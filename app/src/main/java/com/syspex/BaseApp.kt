package com.syspex

import android.app.Application
import android.app.NotificationChannel
import android.app.NotificationManager
import android.content.Context
import android.os.Build
import com.syspex.ui.helper.UploadFileListener
import com.syspex.ui.helper.UploadFileNotification
import net.gotev.uploadservice.UploadServiceConfig
import net.gotev.uploadservice.observer.request.GlobalRequestObserver

class BaseApp : Application() {

    companion object {
        const val notificationChannelID = "TestChannel"
    }

    override fun onCreate() {
        super.onCreate()

        createNotificationChannel()

        UploadServiceConfig.initialize(
            context = this,
            defaultNotificationChannel = notificationChannelID,
            debug = BuildConfig.DEBUG
        )

        UploadServiceConfig.notificationHandlerFactory = { service ->
            UploadFileNotification(service)
        }

        // Upload Listener
        GlobalRequestObserver(this, UploadFileListener())
    }

    private fun createNotificationChannel() {
        if (Build.VERSION.SDK_INT >= 26) {
            val manager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
            val channel = NotificationChannel(notificationChannelID, "Syspex Sync Data", NotificationManager.IMPORTANCE_LOW)
            manager.createNotificationChannel(channel)
        }
    }
}