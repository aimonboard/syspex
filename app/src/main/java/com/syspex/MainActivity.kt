package com.syspex

import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.View
import android.webkit.CookieManager
import android.webkit.CookieSyncManager
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.syspex.data.local.SharedPreferenceData
import com.syspex.data.remote.LoginRequest
import com.syspex.data.retrofit.ApiClient
import com.syspex.ui.PasswordForgot
import kotlinx.android.synthetic.main.main_activity.*
import org.json.JSONArray
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.main_activity)

        checkSaveLogin()

        btn_login.setOnClickListener {
            if (check()) {
                saveLogin()
                login()
            }
        }

        btn_forgot.setOnClickListener {
            val i = Intent(this, PasswordForgot::class.java)
            startActivity(i)
        }
    }

    private fun checkSaveLogin() {
        val loginStatus = SharedPreferenceData.getString(this@MainActivity,1,"")
        val loginToken = SharedPreferenceData.getString(this@MainActivity,2,"")
        val loginSave = SharedPreferenceData.getString(this@MainActivity,3,"")

        Log.d("aim", "Login Status : $loginStatus, Login Save : $loginSave, Token : $loginToken")

        if (loginStatus == "1" && loginToken.isNotEmpty() && loginSave == "1") {
            val i = Intent(this@MainActivity, Menu2::class.java)
            startActivity(i)
        }
    }

    private fun check(): Boolean{
        var cek = 0

        if(user_value.text.toString() == ""){
            user_lay.error = "Please enter your username"; cek++
        } else { user_lay.error = "" }

        if(pass_value.text.toString() == ""){
            pass_lay.error = "Please enter your password"; cek++
        } else { pass_lay.error = "" }

        if (cek == 0){
            return true
        }
        return false
    }

    private fun login() {
        progress_bar.visibility = View.VISIBLE
        btn_login.isEnabled = false

        val params = HashMap<String, String>()
        params["email"] = user_value.text.toString()
        params["password"] = pass_value.text.toString()

        ApiClient.instance.loginRequest(params).enqueue(object: Callback<LoginRequest> {
            override fun onResponse(call: Call<LoginRequest>, response: Response<LoginRequest>?) {
                if (response != null && response.isSuccessful) {

                    val token = response.body()?.data?.token ?: ""
                    val userId = response.body()?.data?.user?.id ?: ""
                    val userCode = response.body()?.data?.user?.userCode ?: ""
                    val userName = response.body()?.data?.user?.userFullname ?: ""
                    val userEmail = response.body()?.data?.user?.email ?: ""
                    val userPhone = response.body()?.data?.user?.phone ?: ""
                    val userSignature = response.body()?.data?.user?.userSignature ?: ""
                    val userRole = response.body()?.data?.user?.role?.roleName ?: ""
                    val userRegion = response.body()?.data?.user?.region?.regionId ?: ""

                    SharedPreferenceData.setString(this@MainActivity, 1, "1")
                    SharedPreferenceData.setString(this@MainActivity, 2, token)
                    SharedPreferenceData.setString(this@MainActivity, 3, "1")

                    SharedPreferenceData.setString(this@MainActivity, 8, userId)
                    SharedPreferenceData.setString(this@MainActivity, 9, userCode)
                    SharedPreferenceData.setString(this@MainActivity, 10, userName)
                    SharedPreferenceData.setString(this@MainActivity, 11, userEmail)
                    SharedPreferenceData.setString(this@MainActivity, 12, userPhone)
                    SharedPreferenceData.setString(this@MainActivity, 13, userRole)
                    SharedPreferenceData.setString(this@MainActivity, 14, userRegion)
                    SharedPreferenceData.setString(this@MainActivity, 1111, userSignature)

                    val jsonArr = JSONArray()
                    response.body()?.data?.user?.userRegion?.forEach {
                        val jsonObj = JSONObject()
                        jsonObj.put("region_id", it?.region?.regionId)
                        jsonObj.put("region_code", it?.region?.regionCode)
                        jsonObj.put("region_name", it?.region?.regionName)
                        jsonArr.put(jsonObj)
                    }
                    SharedPreferenceData.setString(this@MainActivity,18, jsonArr.toString())

                    val i = Intent(this@MainActivity, Menu2::class.java)
                    startActivity(i)

                }
                else {
                    if (response?.code() == 401) {
                        Toast.makeText(this@MainActivity, "Invalid Login", Toast.LENGTH_LONG).show()
                    } else {
                        Toast.makeText(this@MainActivity, "Connection failed", Toast.LENGTH_LONG).show()
                    }
                }
                progress_bar.visibility = View.GONE
                btn_login.isEnabled = true
            }

            override fun onFailure(call: Call<LoginRequest>, t: Throwable) {
                progress_bar.visibility = View.GONE
                Toast.makeText(this@MainActivity, "Connection failed", Toast.LENGTH_LONG).show()
                btn_login.isEnabled = true
            }

        })

    }

    private fun saveLogin() {
        if (remember_me.isChecked) {
            SharedPreferenceData.setString(this@MainActivity, 3, "1")
        } else {
            SharedPreferenceData.setString(this@MainActivity, 3, "0")
        }
    }

//    private fun setCookie() {
//        val url = "https://ssa.syspex.com/news/read/9"
//        val sessionId = "syspex_session=eyJpdiI6Ik9hZXBFcnd1V25KUU81NnNDUWxoR2c9PSIsInZhbHVlIjoiaEIwZWY3VTdzVkRzRys5UGZUQXZcL1oxVHVSK0FrdDJtQlNtWFdaQ2xzNkdka1wvYjg5eWYrUHNcL3RkUktiT0pkMXpMMGoxaG5KbjBpTHpWdTNcL3dRNXdWdTc1ZW4xT2k2cnVESkJHeVUwbGJLQ0JBQ3dzWlwvWEhQSjBUK01raDhVdyIsIm1hYyI6ImY2ZWU1NThiNmJlZjliMzdkMTEzOTM2MzMxNGQwMmEzOGY2M2MwMjMxNmI3ZjY4MTRiZDI0Yjg2NTUxMGNmZTcifQ%3D%3D; expires=Sat, 31-Jul-2021 03:07:40 GMT; Max-Age=2628000; path=/; httponly"
//        // set cookies
//        val cookieManager: CookieManager = CookieManager.getInstance()
//        cookieManager.setAcceptCookie(true)
//        // What is set here is the cookie of a certain url, cookie is sessionId
//        cookieManager.setCookie(url, sessionId)
//    }

}
