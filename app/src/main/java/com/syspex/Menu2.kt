package com.syspex

import android.app.DatePickerDialog
import android.app.ProgressDialog
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import android.view.WindowManager
import android.widget.*
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.appcompat.app.AppCompatActivity
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContentProviderCompat.requireContext
import androidx.core.view.GravityCompat
import androidx.drawerlayout.widget.DrawerLayout
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.NavController
import androidx.navigation.Navigation
import androidx.navigation.ui.setupWithNavController
import androidx.work.WorkManager
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.android.play.core.appupdate.AppUpdateManager
import com.google.android.play.core.appupdate.AppUpdateManagerFactory
import com.google.android.play.core.install.model.AppUpdateType.IMMEDIATE
import com.google.android.play.core.install.model.UpdateAvailability
import com.google.firebase.iid.FirebaseInstanceId
import com.syspex.data.local.SharedPreferenceData
import com.syspex.data.local.database.service_call.ServiceCallViewModel
import com.syspex.data.model.SyncRegionModel
import com.syspex.data.retrofit.ApiClient
import com.syspex.ui.helper.SendData
import com.syspex.ui.helper.SyncActivity
import com.syspex.ui.helper.SyncData
import com.syspex.ui.helper.UploadFile
import kotlinx.android.synthetic.main.activity_menu2.*
import okhttp3.ResponseBody
import org.json.JSONArray
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList

class Menu2 : AppCompatActivity() {
    private lateinit var serviceCallVM: ServiceCallViewModel
    private lateinit var progressDialog: ProgressDialog
    private lateinit var instanceWorkManager: WorkManager
    private lateinit var appUpdateManager: AppUpdateManager
    private lateinit var navController: NavController

    private val updateCode = 453543
    private var format = SimpleDateFormat("yyyy-MM-dd", Locale.getDefault())

    var startDateChoose = Date()
    var endDateChoose = Date()

    companion object {
        var isLoading = MutableLiveData(false)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_menu2)
        window.addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON)

        serviceCallVM = ViewModelProviders.of(this).get(ServiceCallViewModel::class.java)
        progressDialog = ProgressDialog(this)
        instanceWorkManager = WorkManager.getInstance(this)
        appUpdateManager = AppUpdateManagerFactory.create(this)

        setupNavigation()
        getFirebaseToken()
        checkUpdate()

        generateSync(
            onlyMyCall = "1",
            fromDate = startDateChoose,
            toDate = endDateChoose,
            region = "",
            isCustom = false
        )
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == updateCode) {
            // restart update flow when user cancel update
            if (resultCode == RESULT_CANCELED) {
                checkUpdate()
            }
        }
    }

    override fun onResume() {
        super.onResume()
        UploadFile(this@Menu2).startUploadFile()
        SendData(this@Menu2).getParam(null)
    }

    override fun onBackPressed() {
        if(!drawer_layout.isDrawerOpen(GravityCompat.START)) drawer_layout.openDrawer(GravityCompat.START)
        else drawer_layout.closeDrawer(GravityCompat.START)
    }

    private fun setupNavigation() {
        val drawerLayout: DrawerLayout = findViewById(R.id.drawer_layout)
        val toggle = ActionBarDrawerToggle(this, drawerLayout, R.string.app_name, R.string.app_name)
        drawerLayout.addDrawerListener(toggle)
        toggle.syncState()

        navController = Navigation.findNavController(this, R.id.nav_host_fragment)
        nav_view.setupWithNavController(navController)

        nav_view.setNavigationItemSelectedListener {
            when (it.itemId) {
                R.id.menu_home -> {
                    navController.navigate(R.id.nav_home, null)
                    drawer_layout?.closeDrawer(GravityCompat.START)
                    return@setNavigationItemSelectedListener true
                }
                R.id.menu_profile -> {
                    navController.navigate(R.id.nav_profile, null)
                    drawer_layout?.closeDrawer(GravityCompat.START)
                    return@setNavigationItemSelectedListener true
                }
                R.id.menu_news -> {
                    navController.navigate(R.id.nav_news, null)
                    drawer_layout?.closeDrawer(GravityCompat.START)
                    return@setNavigationItemSelectedListener true
                }
                R.id.menu_account -> {
                    navController.navigate(R.id.nav_account, null)
                    drawer_layout?.closeDrawer(GravityCompat.START)
                    return@setNavigationItemSelectedListener true
                }
                R.id.menu_contact -> {
                    navController.navigate(R.id.nav_contact, null)
                    drawer_layout?.closeDrawer(GravityCompat.START)
                    return@setNavigationItemSelectedListener true
                }
                R.id.menu_product -> {
                    navController.navigate(R.id.nav_product, null)
                    drawer_layout?.closeDrawer(GravityCompat.START)
                    return@setNavigationItemSelectedListener true
                }
                R.id.menu_equipment -> {
                    navController.navigate(R.id.nav_equipment, null)
                    drawer_layout?.closeDrawer(GravityCompat.START)
                    return@setNavigationItemSelectedListener true
                }
                R.id.menu_contracts -> {
                    navController.navigate(R.id.nav_agreement, null)
                    drawer_layout?.closeDrawer(GravityCompat.START)
                    return@setNavigationItemSelectedListener true
                }
                R.id.menu_cases -> {
                    navController.navigate(R.id.nav_case, null)
                    drawer_layout?.closeDrawer(GravityCompat.START)
                    return@setNavigationItemSelectedListener true
                }
                R.id.menu_sevice_report -> {
                    navController.navigate(R.id.nav_report, null)
                    drawer_layout?.closeDrawer(GravityCompat.START)
                    return@setNavigationItemSelectedListener true
                }
                R.id.menu_service_call -> {
                    navController.navigate(R.id.nav_call, null)
                    drawer_layout?.closeDrawer(GravityCompat.START)
                    return@setNavigationItemSelectedListener true
                }
                R.id.menu_request_service_call -> {
                    navController.navigate(R.id.nav_call_add_menu, null)
                    drawer_layout?.closeDrawer(GravityCompat.START)
                    return@setNavigationItemSelectedListener true
                }
                R.id.menu_refresh -> {
                    dialogSync()
                    drawer_layout?.closeDrawer(GravityCompat.START)
                    return@setNavigationItemSelectedListener true
                }
                else -> {
                    return@setNavigationItemSelectedListener false
                }
            }
        }
    }

    private fun dialogSync() {
        val view = layoutInflater.inflate(R.layout.dialog_sync_option, null)
        val dialog = BottomSheetDialog(this)
        dialog.setContentView(view)
        val bottomSheetBehavior = BottomSheetBehavior.from<View>(view.parent as View)
        dialog.setOnShowListener { bottomSheetBehavior.state = BottomSheetBehavior.STATE_EXPANDED }

        val txStart = dialog.findViewById<TextView>(R.id.tx_start_date)
        val btnStart = dialog.findViewById<TextView>(R.id.startDate)
        val txEnd = dialog.findViewById<TextView>(R.id.tx_end_date)
        val btnEnd = dialog.findViewById<TextView>(R.id.endDate)
        val spinnerType = dialog.findViewById<Spinner>(R.id.spinner_type)
        val regionLay = dialog.findViewById<ConstraintLayout>(R.id.branch_lay)
        val spinnerRegion = dialog.findViewById<Spinner>(R.id.spinner_region)
        val btnCustom = dialog.findViewById<TextView>(R.id.btn_custom)
        val btnDownload = dialog.findViewById<TextView>(R.id.btn_download)

        val uiFormat = SimpleDateFormat("dd MMM yyyy", Locale.getDefault())

        // spinner type
        val typeData = arrayOf("Related to me only","All")
        val arrayAdapter = ArrayAdapter(this, R.layout.item_spinner_black, typeData)
        spinnerType?.adapter = arrayAdapter

        var customSync = false
        btnCustom?.setOnClickListener {
            if (!customSync) {
                customSync = true

                txStart?.visibility = View.VISIBLE
                btnStart?.visibility = View.VISIBLE

                txEnd?.visibility = View.VISIBLE
                btnEnd?.visibility = View.VISIBLE
            } else {
                customSync = false

                txStart?.visibility = View.GONE
                btnStart?.visibility = View.GONE

                txEnd?.visibility = View.GONE
                btnEnd?.visibility = View.GONE
            }
        }


        // spinner region
        var regionChoose = ""
        val regionString = SharedPreferenceData.getString(this, 18, "")
        val dataRegion = ArrayList<SyncRegionModel>()
        val dataForSpinner = ArrayList<String>()
        val jsonArr = JSONArray(regionString)

        if (jsonArr.length() > 1) {
            for (i in 0 until jsonArr.length()) {
                val jsonObj = jsonArr.getJSONObject(i)
                val regionCode = jsonObj.getString("region_code")
                val regionName = jsonObj.getString("region_name")

                dataForSpinner.add("$regionCode - $regionName")
                dataRegion.add(SyncRegionModel(
                    regionId = jsonObj.getString("region_id"),
                    regionCode = jsonObj.getString("region_code"),
                    regionName = jsonObj.getString("region_name")
                ))
            }

            Log.e("aim", "region data : $dataForSpinner")
            val regionAdapter = ArrayAdapter(this, R.layout.item_spinner_black, dataForSpinner)
            spinnerRegion?.adapter = regionAdapter

            spinnerRegion?.onItemSelectedListener = object : AdapterView.OnItemSelectedListener{
                override fun onNothingSelected(parent: AdapterView<*>?) {}
                override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                    regionChoose = dataRegion[position].regionId
                }
            }
        } else {
            regionLay?.visibility = View.GONE
        }

        btnStart?.setOnClickListener {
            val c = Calendar.getInstance()
            val yearCalendar = c.get(Calendar.YEAR)
            val monthCalendar = c.get(Calendar.MONTH)
            val dayCalendar = c.get(Calendar.DAY_OF_MONTH)

            val dpd = DatePickerDialog(this@Menu2, { view, year, monthOfYear, dayOfMonth ->
                val mCalendar = Calendar.getInstance()
                mCalendar.set(year,monthOfYear,dayOfMonth)
                startDateChoose = mCalendar.time
                btnStart.text = uiFormat.format(startDateChoose)
            }, yearCalendar, monthCalendar, dayCalendar)
            dpd.show()
        }

        btnEnd?.setOnClickListener {
            val c = Calendar.getInstance()
            val yearCalendar = c.get(Calendar.YEAR)
            val monthCalendar = c.get(Calendar.MONTH)
            val dayCalendar = c.get(Calendar.DAY_OF_MONTH)

            val dpd = DatePickerDialog(this@Menu2, { view, year, monthOfYear, dayOfMonth ->
                val mCalendar = Calendar.getInstance()
                mCalendar.set(year,monthOfYear,dayOfMonth)
                endDateChoose = mCalendar.time
                btnEnd.text = uiFormat.format(endDateChoose)
            }, yearCalendar, monthCalendar, dayCalendar)
            dpd.show()
        }


        btnDownload?.setOnClickListener {
            val onlyMyCall = if (spinnerType?.selectedItemPosition == 0) "1" else "0"

            val fromDate = btnStart?.text.toString()
            val toDate = btnEnd?.text.toString()

            val isCustom = fromDate.isNotEmpty() || toDate.isNotEmpty()
            if (isCustom) {
                if (startDateChoose.before(endDateChoose)) {

                    // calculate total day in selected range date
                    val milPerDay = 1000*60*60*24
                    val totalDay = (endDateChoose.time - startDateChoose.time) / milPerDay
                    Log.e("aim", "total day : $totalDay")

                    if (totalDay+1 <= 30) {
                        dialog.dismiss()

                        generateSync(
                            onlyMyCall = onlyMyCall,
                            fromDate = startDateChoose,
                            toDate = endDateChoose,
                            region = regionChoose,
                            isCustom = isCustom
                        )
                    } else {
                        Toast.makeText(
                            this,
                            "Max 30 days",
                            Toast.LENGTH_LONG
                        ).show()
                    }

                } else {
                    Toast.makeText(
                        this,
                        "Start date must be earlier than end date",
                        Toast.LENGTH_LONG
                    ).show()
                }
            } else {
                dialog.dismiss()

                generateSync(
                    onlyMyCall = onlyMyCall,
                    fromDate = startDateChoose,
                    toDate = endDateChoose,
                    region = regionChoose,
                    isCustom = isCustom
                )
            }
        }

        dialog.show()
    }

    private fun generateSync(onlyMyCall: String, fromDate: Date, toDate: Date, region: String, isCustom: Boolean) {
        val lastSync = SharedPreferenceData.getString(this, 6, "")
        Log.e("aim", "last sync : $lastSync, isCustom : $isCustom")

        syncLoading(true)
        SendData(this@Menu2).getParam(null)
        UploadFile(this@Menu2).startUploadFile()
        SyncActivity(this@Menu2).startSyncActivity(null)

        if (lastSync.isEmpty()) {
            // first open apk
            SyncData(
                context = this@Menu2,
                onlyMyCall = onlyMyCall,
                firstSync = "1",
                fromDate = "",
                toDate = "",
                region = region
            ).getEnum()
        } else {
            if (isCustom) {
                // custom sync with start - end date
                SyncData(
                    context = this@Menu2,
                    onlyMyCall = onlyMyCall,
                    firstSync = "1",
                    fromDate = format.format(fromDate),
                    toDate = format.format(toDate),
                    region = region
                ).getEnum()
            } else {
                // increment sync
                SyncData(
                    context = this@Menu2,
                    onlyMyCall = onlyMyCall,
                    firstSync = "0",
                    fromDate = lastSync,
                    toDate = "",
                    region = region
                ).getEnum()
            }
        }
    }

    fun syncLoading(onProgress: Boolean) {
        try {
            if (onProgress) {
                isLoading.value = true
                progressDialog.setMessage("Sync Data ...")
                progressDialog.setCancelable(false)
                progressDialog.show()
            } else {
                isLoading.value = false
                progressDialog.dismiss()

                // update last sync
                val now = format.format(Date())
                SharedPreferenceData.setString(this, 6, now)
            }
        } catch (e: Exception) { e.stackTrace }
    }

    fun syncIteration(page: Int, last: Int) {
        progressDialog.setMessage("Sync Data  $page / $last")
    }

    private fun getFirebaseToken() {
        FirebaseInstanceId.getInstance().instanceId.addOnSuccessListener(this) { instanceIdResult ->
            val token = instanceIdResult.token
            sendFcmToken(token)
            Log.e("aim", "fcm token : $token")
        }

    }

    private fun checkUpdate() {
        appUpdateManager.appUpdateInfo.addOnSuccessListener { appUpdateInfo ->
            if (appUpdateInfo.updateAvailability() == UpdateAvailability.UPDATE_AVAILABLE) {
                appUpdateManager.startUpdateFlowForResult(
                    appUpdateInfo,
                    IMMEDIATE,
                    this,
                    updateCode
                )
            }
        }
    }

    private fun sendFcmToken(fcmToken: String) {
        val token = "Bearer "+SharedPreferenceData.getString(this, 2, "")
        val params = HashMap<String, String>()
        params["fcm_token"] = fcmToken

        ApiClient.instance.sendFcmToken(token, params).enqueue(object : Callback<ResponseBody> {
            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                Log.e("aim", "send fcm token failed : ${t.message}")
            }

            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>?) {
                Log.e("aim", "send fcm token : ${response?.message()}")
            }

        })
    }

}