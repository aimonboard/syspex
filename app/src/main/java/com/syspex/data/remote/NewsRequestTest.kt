package com.syspex.data.remote


import com.google.gson.annotations.SerializedName

data class NewsRequestTest(
    @SerializedName("code")
    val code: String? = "",
    @SerializedName("status")
    val status: String? = "",
    @SerializedName("message")
    val message: String? = "",
    @SerializedName("data")
    val `data`: Data? = Data()
) {
    data class Data(
        @SerializedName("page")
        val page: Int? = 0,
        @SerializedName("last_page")
        val lastPage: Int? = 0,
        @SerializedName("length")
        val length: String? = "",
        @SerializedName("news")
        val news: List<News?>? = listOf()
    ) {
        data class News(
            @SerializedName("id")
            val id: String? = "",
            @SerializedName("title")
            val title: String? = "",
            @SerializedName("thumnail")
            val thumnail: String? = "",
            @SerializedName("content")
            val content: String? = "",
            @SerializedName("plain_content")
            val plainContent: String? = "",
            @SerializedName("region_code")
            val regionCode: String? = "",
            @SerializedName("created_by_user")
            val createdBy: CreatedByUser?,
            @SerializedName("last_modified_by")
            val lastModifiedBy: String? = "",
            @SerializedName("created_at")
            val createdAt: String? = "",
            @SerializedName("updated_at")
            val updatedAt: String? = "",
            @SerializedName("regions")
            val regions: List<Region?>? = listOf()
        ) {

            data class CreatedByUser(
                @SerializedName("created_by")
                val createdBy: Any? = Any(),
                @SerializedName("created_date")
                val createdDate: String? = "",
                @SerializedName("email")
                val email: String? = "",
                @SerializedName("email_verified_at")
                val emailVerifiedAt: Any? = Any(),
                @SerializedName("fcm_token")
                val fcmToken: String? = "",
                @SerializedName("id")
                val id: Int? = 0,
                @SerializedName("is_active")
                val isActive: Int? = 0,
                @SerializedName("last_modified_by")
                val lastModifiedBy: Int? = 0,
                @SerializedName("last_modified_date")
                val lastModifiedDate: String? = "",
                @SerializedName("phone")
                val phone: Any? = Any(),
                @SerializedName("region")
                val region: Region? = Region(),
                @SerializedName("region_id")
                val regionId: String? = "",
                @SerializedName("role")
                val role: Role? = Role(),
                @SerializedName("role_id")
                val roleId: Int? = 0,
                @SerializedName("token_reset")
                val tokenReset: Any? = Any(),
                @SerializedName("user_code")
                val userCode: String? = "",
                @SerializedName("user_first_name")
                val userFirstName: String? = "",
                @SerializedName("user_fullname")
                val userFullname: String? = "",
                @SerializedName("user_last_name")
                val userLastName: String? = "",
                @SerializedName("user_region")
                val userRegion: List<UserRegion?>? = listOf(),
                @SerializedName("user_signature")
                val userSignature: String? = ""
            ) {

                data class Role(
                    @SerializedName("created_at")
                    val createdAt: String? = "",
                    @SerializedName("role_desc")
                    val roleDesc: Any? = Any(),
                    @SerializedName("role_id")
                    val roleId: Int? = 0,
                    @SerializedName("role_name")
                    val roleName: String? = "",
                    @SerializedName("updated_at")
                    val updatedAt: String? = ""
                )

                data class UserRegion(
                    @SerializedName("created_at")
                    val createdAt: String? = "",
                    @SerializedName("region")
                    val region: Region? = Region(),
                    @SerializedName("region_id")
                    val regionId: Int? = 0,
                    @SerializedName("updated_at")
                    val updatedAt: String? = "",
                    @SerializedName("user_id")
                    val userId: Int? = 0,
                    @SerializedName("user_region_id")
                    val userRegionId: Int? = 0
                )
            }

            data class Region(
                @SerializedName("region_id")
                val regionId: String? = "",
                @SerializedName("region_code")
                val regionCode: String? = "",
                @SerializedName("region_name")
                val regionName: String? = "",
                @SerializedName("created_date")
                val createdDate: String? = "",
                @SerializedName("created_by")
                val createdBy: Any? = Any(),
                @SerializedName("last_modified_date")
                val lastModifiedDate: String? = "",
                @SerializedName("last_modified_by")
                val lastModifiedBy: Any? = Any(),
                @SerializedName("timezone")
                val timezone: String? = ""
            )
        }
    }
}