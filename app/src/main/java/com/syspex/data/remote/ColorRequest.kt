package com.syspex.data.remote


import com.google.gson.annotations.SerializedName

data class ColorRequest(
    @SerializedName("code")
    val code: Int? = 0,
    @SerializedName("status")
    val status: Int? = 0,
    @SerializedName("message")
    val message: String? = "",
    @SerializedName("data")
    val `data`: Data? = Data()
) {
    data class Data(
        @SerializedName("status_color")
        val statusColor: List<StatusColor?>? = listOf()
    ) {
        data class StatusColor(
            @SerializedName("table")
            val table: String? = "",
            @SerializedName("status")
            val status: String? = "",
            @SerializedName("bg_color")
            val bgColor: String? = "",
            @SerializedName("color")
            val color: String? = ""
        )
    }
}