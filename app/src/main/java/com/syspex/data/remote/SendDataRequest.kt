package com.syspex.data.remote


import com.google.gson.annotations.SerializedName

data class SendDataRequest(
    @SerializedName("code")
    val code: String = "",
    @SerializedName("status")
    val status: String = "",
    @SerializedName("message")
    val message: String? = "",
    @SerializedName("data")
    val `data`: Data? = Data()
) {
    data class Data(
        @SerializedName("service_report")
        val serviceReport: List<ServiceReport?>? = listOf()
    ) {
        data class ServiceReport(
            @SerializedName("service_report_id")
            val serviceReportId: String = "",
            @SerializedName("region_id")
            val regionId: String? = "",
            @SerializedName("call_id")
            val callId: String = "",
            @SerializedName("service_report_number")
            val serviceReportNumber: String? = "",
            @SerializedName("report_date")
            val reportDate: String? = "",
            @SerializedName("report_status")
            val reportStatus: String = "",
            @SerializedName("field_pic_contact_id")
            val fieldPicContactId: String = "",
            @SerializedName("serviced_by")
            val servicedBy: String = "",
            @SerializedName("internal_memo")
            val internalMemo: String? = "",
            @SerializedName("customer_rating")
            val customerRating: String = "",
            @SerializedName("feedback")
            val feedback: String = "",
            @SerializedName("serviced_by_signature")
            val servicedBySignature: String = "",
            @SerializedName("account_pic_signature")
            val accountPicSignature: String = "",
            @SerializedName("is_chargeable")
            val isChargeable: String = "",
            @SerializedName("employee_name")
            val employeeName: String = "",
            @SerializedName("employee_id")
            val employeeId: String = "",
            @SerializedName("created_date")
            val createdDate: String? = "",
            @SerializedName("created_by")
            val createdBy: String = "",
            @SerializedName("last_modified_date")
            val lastModifiedDate: String? = "",
            @SerializedName("last_modified_by")
            val lastModifiedBy: String = "",
            @SerializedName("service_report_number_temp")
            val serviceReportNumberTemp: String? = "",
            @SerializedName("time_tracking")
            val timeTracking: List<TimeTracking?>? = listOf(),
            @SerializedName("service_report_equipment")
            val serviceReportEquipment: List<ServiceReportEquipment?>? = listOf()
        ) {
            data class TimeTracking(
                @SerializedName("report_id")
                val reportId: String = "",
                @SerializedName("engineer_id")
                val engineerId: String = "",
                @SerializedName("is_lead")
                val isLead: Boolean? = false,
                @SerializedName("created_by")
                val createdBy: String = "",
                @SerializedName("start_date_time")
                val startDateTime: String? = "",
                @SerializedName("end_date_time")
                val endDateTime: String? = "",
                @SerializedName("last_modified_by")
                val lastModifiedBy: String = "",
                @SerializedName("last_modified_date")
                val lastModifiedDate: String? = "",
                @SerializedName("created_date")
                val createdDate: String? = "",
                @SerializedName("service_report_time_tracking_id")
                val serviceReportTimeTrackingId: String = ""
            )

            data class ServiceReportEquipment(
                @SerializedName("report_id")
                val reportId: String = "",
                @SerializedName("equipment_id")
                val equipmentId: String = "",
                @SerializedName("created_by")
                val createdBy: String = "",
                @SerializedName("problem_symptom")
                val problemSymptom: String? = "",
                @SerializedName("status_after_service")
                val statusAfterService: String = "",
                @SerializedName("status_after_service_remarks")
                val statusAfterServiceRemarks: String = "",
                @SerializedName("installation_start_date")
                val installationStartDate: String = "",
                @SerializedName("installation_matters")
                val installationMatters: String = "",
                @SerializedName("checklist_template_id")
                val checklistTemplateId: String = "",
                @SerializedName("last_modified_by")
                val lastModifiedBy: String = "",
                @SerializedName("last_modified_date")
                val lastModifiedDate: String? = "",
                @SerializedName("created_date")
                val createdDate: String? = "",
                @SerializedName("service_report_equipment_id")
                val serviceReportEquipmentId: String = "",
                @SerializedName("service_report_equipment_solutions")
                val serviceReportEquipmentSolutions: List<ServiceReportEquipmentSolution?>? = listOf(),
                @SerializedName("equipment_checklist")
                val equipmentChecklist: List<EquipmentChecklist?>? = listOf(),
                @SerializedName("service_report_sparepart")
                val serviceReportSparepart: List<ServiceReportSparepart?>? = listOf()
            ) {
                data class ServiceReportEquipmentSolution(
                    @SerializedName("report_equipment_id")
                    val reportEquipmentId: String = "",
                    @SerializedName("created_by")
                    val createdBy: String = "",
                    @SerializedName("subject")
                    val subject: String? = "",
                    @SerializedName("problem_cause")
                    val problemCause: String? = "",
                    @SerializedName("solution")
                    val solution: String? = "",
                    @SerializedName("last_modified_by")
                    val lastModifiedBy: String = "",
                    @SerializedName("last_modified_date")
                    val lastModifiedDate: String? = "",
                    @SerializedName("created_date")
                    val createdDate: String? = "",
                    @SerializedName("service_report_equipment_solution_id")
                    val serviceReportEquipmentSolutionId: String = ""
                )

                data class EquipmentChecklist(
                    @SerializedName("report_equipment_id")
                    val reportEquipmentId: String = "",
                    @SerializedName("created_by")
                    val createdBy: String = "",
                    @SerializedName("question_id")
                    val questionId: String = "",
                    @SerializedName("description")
                    val description: String? = "",
                    @SerializedName("status")
                    val status: String = "",
                    @SerializedName("remarks")
                    val remarks: String? = "",
                    @SerializedName("last_modified_by")
                    val lastModifiedBy: String = "",
                    @SerializedName("last_modified_date")
                    val lastModifiedDate: String? = "",
                    @SerializedName("created_date")
                    val createdDate: String? = "",
                    @SerializedName("service_report_equipment_checklist_id")
                    val serviceReportEquipmentChecklistId: String = ""
                )

                data class ServiceReportSparepart(
                    @SerializedName("report_equipment_id")
                    val reportEquipmentId: String = "",
                    @SerializedName("created_by")
                    val createdBy: String = "",
                    @SerializedName("spare_part_id")
                    val sparePartId: String = "",
                    @SerializedName("spare_part_image_number")
                    val sparePartImageNumber: String? = "",
                    @SerializedName("spare_part_number")
                    val sparePartNumber: String? = "",
                    @SerializedName("spare_part_description")
                    val sparePartDescription: String = "",
                    @SerializedName("spare_part_memo")
                    val sparePartMemo: String = "",
                    @SerializedName("spare_part_type")
                    val sparePartType: String = "",
                    @SerializedName("qty")
                    val qty: String = "",
                    @SerializedName("last_modified_by")
                    val lastModifiedBy: String = "",
                    @SerializedName("last_modified_date")
                    val lastModifiedDate: String? = "",
                    @SerializedName("created_date")
                    val createdDate: String? = "",
                    @SerializedName("service_report_sparepart_id")
                    val serviceReportSparepartId: String = ""
                )
            }
        }
    }
}