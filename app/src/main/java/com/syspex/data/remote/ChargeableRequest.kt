package com.syspex.data.remote


import com.google.gson.annotations.SerializedName

data class ChargeableRequest(
    @SerializedName("code")
    val code: String? = "",
    @SerializedName("status")
    val status: String? = "",
    @SerializedName("message")
    val message: String? = "",
    @SerializedName("data")
    val `data`: List<Data?>? = listOf()
) {
    data class Data(
        @SerializedName("key")
        val key: String? = "",
        @SerializedName("value")
        val value: String? = "",
        @SerializedName("is_show")
        val isShow: String? = ""
    )
}