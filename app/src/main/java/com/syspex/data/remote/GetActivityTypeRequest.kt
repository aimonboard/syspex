package com.syspex.data.remote


import com.google.gson.annotations.SerializedName

data class GetActivityTypeRequest(
    @SerializedName("code")
    val code: String? = "",
    @SerializedName("status")
    val status: String? = "",
    @SerializedName("message")
    val message: String? = "",
    @SerializedName("data")
    val `data`: Data? = Data()
) {
    data class Data(
        @SerializedName("activity_type")
        val activityType: List<ActivityType?>? = listOf()
    ) {
        data class ActivityType(
            @SerializedName("activity_type_id")
            val activityTypeId: String? = "",
            @SerializedName("activity_type_code")
            val activityTypeCode: String? = "",
            @SerializedName("activity_type_name")
            val activityTypeName: String? = "",
            @SerializedName("activity_type_color")
            val activityTypeColor: String? = "",
            @SerializedName("activity_related_table")
            val activityRelatedTable: Any? = Any(),
            @SerializedName("created_date")
            val createdDate: String? = "",
            @SerializedName("created_by")
            val createdBy: Any? = Any(),
            @SerializedName("last_modified_date")
            val lastModifiedDate: String? = "",
            @SerializedName("last_modified_by")
            val lastModifiedBy: Any? = Any()
        )
    }
}