package com.syspex.data.remote


import com.google.gson.annotations.SerializedName

data class AttachmentRequest(
    @SerializedName("code")
    val code: String? = "",
    @SerializedName("status")
    val status: String? = "",
    @SerializedName("message")
    val message: String? = "",
    @SerializedName("data")
    val `data`: Data? = Data()
) {
    data class Data(
        @SerializedName("account_attachment_id")
        val accountAttachmentId: String? = "",
        @SerializedName("region_id")
        val regionId: String? = "",
        @SerializedName("account_id")
        val accountId: String? = "",
        @SerializedName("attachment_title")
        val attachmentTitle: String? = "",
        @SerializedName("attachment_body")
        val attachmentBody: String? = "",
        @SerializedName("file_url")
        val fileUrl: String? = "",
        @SerializedName("attachment_owner_id")
        val attachmentOwnerId: String? = "",
        @SerializedName("created_date")
        val createdDate: String? = "",
        @SerializedName("created_by")
        val createdBy: String? = "",
        @SerializedName("last_modified_date")
        val lastModifiedDate: String? = "",
        @SerializedName("last_modified_by")
        val lastModifiedBy: String? = "",
        @SerializedName("attachment_source_table")
        val attachmentSourceTable: String? = "",
        @SerializedName("attachment_source_id")
        val attachmentSourceId: String? = "",
        @SerializedName("created_by_user")
        val createdByUser: CreatedByUser? = CreatedByUser()
    ) {
        data class CreatedByUser(
            @SerializedName("id")
            val id: String? = "",
            @SerializedName("region_id")
            val regionId: String? = "",
            @SerializedName("user_code")
            val userCode: String? = "",
            @SerializedName("user_first_name")
            val userFirstName: String? = "",
            @SerializedName("user_last_name")
            val userLastName: String? = "",
            @SerializedName("email")
            val email: String? = "",
            @SerializedName("email_verified_at")
            val emailVerifiedAt: String? = "",
            @SerializedName("phone")
            val phone: String? = "",
            @SerializedName("role_id")
            val roleId: String? = "",
            @SerializedName("created_date")
            val createdDate: String? = "",
            @SerializedName("created_by")
            val createdBy: String? = "",
            @SerializedName("last_modified_date")
            val lastModifiedDate: String? = "",
            @SerializedName("last_modified_by")
            val lastModifiedBy: String? = "",
            @SerializedName("user_fullname")
            val userFullname: String? = ""
        )
    }
}