package com.syspex.data.remote


import com.google.gson.annotations.SerializedName

data class ContactRequest(
    @SerializedName("code")
    val code: String? = "",
    @SerializedName("status")
    val status: String? = "",
    @SerializedName("message")
    val message: String? = "",
    @SerializedName("data")
    val `data`: Data? = Data()
) {
    data class Data(
        @SerializedName("account_contact")
        val accountContact: AccountContact? = AccountContact()
    ) {
        data class AccountContact(
            @SerializedName("account_contact_id")
            val accountContactId: String? = "",
            @SerializedName("region_id")
            val regionId: String? = "",
            @SerializedName("customer_id")
            val customerId: String? = "",
            @SerializedName("contact_person_name")
            val contactPersonName: String? = "",
            @SerializedName("contact_person_phone")
            val contactPersonPhone: String? = "",
            @SerializedName("contact_person_email")
            val contactPersonEmail: String? = "",
            @SerializedName("contact_person_address")
            val contactPersonAddress: String? = "",
            @SerializedName("contact_person_position")
            val contactPersonPosition: String? = "",
            @SerializedName("contact_type")
            val contactType: String? = "",
            @SerializedName("priority")
            val priority: String? = "",
            @SerializedName("created_date")
            val createdDate: String? = "",
            @SerializedName("created_by")
            val createdBy: String? = "",
            @SerializedName("last_modified_date")
            val lastModifiedDate: String? = "",
            @SerializedName("last_modified_by")
            val lastModifiedBy: String? = ""
        )
    }
}