package com.syspex.data.remote


import com.google.gson.annotations.SerializedName

data class SignAllRequest(
    @SerializedName("code")
    val code: String? = "",
    @SerializedName("status")
    val status: String? = "",
    @SerializedName("message")
    val message: String? = "",
    @SerializedName("data")
    val `data`: Data? = Data()
) {
    data class Data(
        @SerializedName("report")
        val report: Report? = Report()
    ) {
        data class Report(
            @SerializedName("service_report_id")
            val serviceReportId: String? = "",
            @SerializedName("region_id")
            val regionId: String? = "",
            @SerializedName("call_id")
            val callId: String? = "",
            @SerializedName("service_report_number")
            val serviceReportNumber: String? = "",
            @SerializedName("report_date")
            val reportDate: String? = "",
            @SerializedName("report_status")
            val reportStatus: String? = "",
            @SerializedName("field_pic_contact_id")
            val fieldPicContactId: String? = "",
            @SerializedName("serviced_by")
            val servicedBy: String? = "",
            @SerializedName("internal_memo")
            val internalMemo: String? = "",
            @SerializedName("customer_rating")
            val customerRating: String? = "",
            @SerializedName("feedback")
            val feedback: String? = "",
            @SerializedName("serviced_by_signature")
            val servicedBySignature: String? = "",
            @SerializedName("account_pic_signature")
            val accountPicSignature: String? = "",
            @SerializedName("is_chargeable")
            val isChargeable: String? = "",
            @SerializedName("employee_name")
            val employeeName: String? = "",
            @SerializedName("employee_id")
            val employeeId: String? = "",
            @SerializedName("created_date")
            val createdDate: String? = "",
            @SerializedName("created_by")
            val createdBy: String? = "",
            @SerializedName("last_modified_date")
            val lastModifiedDate: String? = "",
            @SerializedName("last_modified_by")
            val lastModifiedBy: String? = "",
            @SerializedName("service_report_number_temp")
            val serviceReportNumberTemp: String? = "",
            @SerializedName("account_signed_at")
            val accountSignedAt: String? = "",
            @SerializedName("deleted_at")
            val deletedAt: String? = "",
            @SerializedName("deleted_by")
            val deletedBy: String? = "",
            @SerializedName("cancellation_note")
            val cancellationNote: String? = "",
            @SerializedName("cp_number")
            val cpNumber: String? = "",
            @SerializedName("cp_name")
            val cpName: String? = "",
            @SerializedName("service_report_time_tracking")
            val serviceReportTimeTracking: List<ServiceReportTimeTracking?>? = listOf(),
            @SerializedName("service_report_equipment")
            val serviceReportEquipment: List<ServiceReportEquipment?>? = listOf()
        ) {
            data class ServiceReportTimeTracking(
                @SerializedName("service_report_time_tracking_id")
                val serviceReportTimeTrackingId: String? = "",
                @SerializedName("report_id")
                val reportId: String? = "",
                @SerializedName("engineer_id")
                val engineerId: String? = "",
                @SerializedName("is_lead")
                val isLead: String? = "",
                @SerializedName("start_date_time")
                val startDateTime: String? = "",
                @SerializedName("end_date_time")
                val endDateTime: String? = "",
                @SerializedName("created_date")
                val createdDate: String? = "",
                @SerializedName("created_by")
                val createdBy: String? = "",
                @SerializedName("last_modified_date")
                val lastModifiedDate: String? = "",
                @SerializedName("last_modified_by")
                val lastModifiedBy: String? = ""
            )

            data class ServiceReportEquipment(
                @SerializedName("service_report_equipment_id")
                val serviceReportEquipmentId: String? = "",
                @SerializedName("report_id")
                val reportId: String? = "",
                @SerializedName("equipment_id")
                val equipmentId: String? = "",
                @SerializedName("problem_symptom")
                val problemSymptom: String? = "",
                @SerializedName("status_after_service")
                val statusAfterService: String? = "",
                @SerializedName("status_after_service_remarks")
                val statusAfterServiceRemarks: String? = "",
                @SerializedName("installation_start_date")
                val installationStartDate: String? = "",
                @SerializedName("installation_matters")
                val installationMatters: String? = "",
                @SerializedName("checklist_template_id")
                val checklistTemplateId: String? = "",
                @SerializedName("created_date")
                val createdDate: String? = "",
                @SerializedName("created_by")
                val createdBy: String? = "",
                @SerializedName("last_modified_date")
                val lastModifiedDate: String? = "",
                @SerializedName("last_modified_by")
                val lastModifiedBy: String? = "",
                @SerializedName("equipment_name")
                val equipmentName: String? = "",
                @SerializedName("equipment_solution")
                val equipmentSolution: List<EquipmentSolution?>? = listOf()
            ) {
                data class EquipmentSolution(
                    @SerializedName("service_report_equipment_solution_id")
                    val serviceReportEquipmentSolutionId: String? = "",
                    @SerializedName("report_equipment_id")
                    val reportEquipmentId: String? = "",
                    @SerializedName("subject")
                    val subject: String? = "",
                    @SerializedName("problem_cause")
                    val problemCause: String? = "",
                    @SerializedName("solution")
                    val solution: String? = "",
                    @SerializedName("created_date")
                    val createdDate: String? = "",
                    @SerializedName("created_by")
                    val createdBy: String? = "",
                    @SerializedName("last_modified_date")
                    val lastModifiedDate: String? = "",
                    @SerializedName("last_modified_by")
                    val lastModifiedBy: String? = ""
                )
            }
        }
    }
}