package com.syspex.data.remote


import com.google.gson.annotations.SerializedName

data class GetActivityRequest(
    @SerializedName("code")
    val code: String? = "",
    @SerializedName("status")
    val status: String? = "",
    @SerializedName("message")
    val message: String? = "",
    @SerializedName("data")
    val `data`: Data? = Data()
) {
    data class Data(
        @SerializedName("page")
        val page: Int? = 0,
        @SerializedName("last_page")
        val lastPage: Int? = 0,
        @SerializedName("from_date")
        val fromDate: String? = "",
        @SerializedName("to_date")
        val toDate: String? = "",
        @SerializedName("length")
        val length: String? = "",
        @SerializedName("activity")
        val activity: List<Activity?>? = listOf()
    ) {
        data class Activity(
            @SerializedName("activity_id")
            val activityId: String? = "",
            @SerializedName("region_id")
            val regionId: String? = "",
            @SerializedName("user_id")
            val userId: String? = "",
            @SerializedName("activity_type")
            val activityType: String? = "",
            @SerializedName("activity_start_time")
            val activityStartTime: String? = "",
            @SerializedName("activity_end_time")
            val activityEndTime: String? = "",
            @SerializedName("activity_subject")
            val activitySubject: String? = "",
            @SerializedName("activity_description")
            val activityDescription: String? = "",
            @SerializedName("activity_source_table")
            val activitySourceTable: String? = "",
            @SerializedName("activity_source_id")
            val activitySourceId: String? = "",
            @SerializedName("created_date")
            val createdDate: String? = "",
            @SerializedName("created_by")
            val createdBy: String? = "",
            @SerializedName("last_modified_date")
            val lastModifiedDate: String? = "",
            @SerializedName("last_modified_by")
            val lastModifiedBy: String? = "",
            @SerializedName("address")
            val address: Any? = Any(),
            @SerializedName("activity_type_text")
            val activityTypeText: ActivityTypeText? = ActivityTypeText(),
            @SerializedName("created_by_detail")
            val createdByDetail: CreatedByDetail? = CreatedByDetail(),
            @SerializedName("last_modified_detail")
            val lastModifiedDetail: LastModifiedDetail? = LastModifiedDetail(),
            @SerializedName("assigned_detail")
            val assignedDetail: AssignedDetail? = AssignedDetail(),
            @SerializedName("call")
            val call: Call? = Call()
        ) {
            data class ActivityTypeText(
                @SerializedName("activity_type_id")
                val activityTypeId: String? = "",
                @SerializedName("activity_type_code")
                val activityTypeCode: String? = "",
                @SerializedName("activity_type_name")
                val activityTypeName: String? = "",
                @SerializedName("activity_type_color")
                val activityTypeColor: String? = "",
                @SerializedName("activity_related_table")
                val activityRelatedTable: Any? = Any(),
                @SerializedName("created_date")
                val createdDate: String? = "",
                @SerializedName("created_by")
                val createdBy: String? = "",
                @SerializedName("last_modified_date")
                val lastModifiedDate: String? = "",
                @SerializedName("last_modified_by")
                val lastModifiedBy: String? = ""
            )

            data class CreatedByDetail(
                @SerializedName("id")
                val id: String? = "",
                @SerializedName("region_id")
                val regionId: String? = "",
                @SerializedName("user_code")
                val userCode: String? = "",
                @SerializedName("user_first_name")
                val userFirstName: String? = "",
                @SerializedName("user_last_name")
                val userLastName: String? = "",
                @SerializedName("email")
                val email: String? = "",
                @SerializedName("email_verified_at")
                val emailVerifiedAt: String? = "",
                @SerializedName("phone")
                val phone: String? = "",
                @SerializedName("role_id")
                val roleId: String? = "",
                @SerializedName("created_date")
                val createdDate: String? = "",
                @SerializedName("created_by")
                val createdBy: String? = "",
                @SerializedName("last_modified_date")
                val lastModifiedDate: String? = "",
                @SerializedName("last_modified_by")
                val lastModifiedBy: String? = "",
                @SerializedName("user_fullname")
                val userFullname: String? = "",
                @SerializedName("fcm_token")
                val fcmToken: String? = "",
                @SerializedName("role")
                val role: Role? = Role(),
                @SerializedName("region")
                val region: Any? = Any(),
                @SerializedName("user_region")
                val userRegion: List<Any?>? = listOf()
            ) {
                data class Role(
                    @SerializedName("role_id")
                    val roleId: String? = "",
                    @SerializedName("role_name")
                    val roleName: String? = "",
                    @SerializedName("created_at")
                    val createdAt: String? = "",
                    @SerializedName("updated_at")
                    val updatedAt: String? = ""
                )
            }

            data class LastModifiedDetail(
                @SerializedName("id")
                val id: String? = "",
                @SerializedName("region_id")
                val regionId: String? = "",
                @SerializedName("user_code")
                val userCode: String? = "",
                @SerializedName("user_first_name")
                val userFirstName: String? = "",
                @SerializedName("user_last_name")
                val userLastName: String? = "",
                @SerializedName("email")
                val email: String? = "",
                @SerializedName("email_verified_at")
                val emailVerifiedAt: String? = "",
                @SerializedName("phone")
                val phone: String? = "",
                @SerializedName("role_id")
                val roleId: String? = "",
                @SerializedName("created_date")
                val createdDate: String? = "",
                @SerializedName("created_by")
                val createdBy: String? = "",
                @SerializedName("last_modified_date")
                val lastModifiedDate: String? = "",
                @SerializedName("last_modified_by")
                val lastModifiedBy: String? = "",
                @SerializedName("user_fullname")
                val userFullname: String? = "",
                @SerializedName("fcm_token")
                val fcmToken: String? = "",
                @SerializedName("role")
                val role: Role? = Role(),
                @SerializedName("region")
                val region: Any? = Any(),
                @SerializedName("user_region")
                val userRegion: List<Any?>? = listOf()
            ) {
                data class Role(
                    @SerializedName("role_id")
                    val roleId: String? = "",
                    @SerializedName("role_name")
                    val roleName: String? = "",
                    @SerializedName("created_at")
                    val createdAt: String? = "",
                    @SerializedName("updated_at")
                    val updatedAt: String? = ""
                )
            }

            data class AssignedDetail(
                @SerializedName("id")
                val id: String? = "",
                @SerializedName("region_id")
                val regionId: String? = "",
                @SerializedName("user_code")
                val userCode: String? = "",
                @SerializedName("user_first_name")
                val userFirstName: String? = "",
                @SerializedName("user_last_name")
                val userLastName: String? = "",
                @SerializedName("email")
                val email: String? = "",
                @SerializedName("email_verified_at")
                val emailVerifiedAt: String? = "",
                @SerializedName("phone")
                val phone: String? = "",
                @SerializedName("role_id")
                val roleId: String? = "",
                @SerializedName("created_date")
                val createdDate: String? = "",
                @SerializedName("created_by")
                val createdBy: String? = "",
                @SerializedName("last_modified_date")
                val lastModifiedDate: String? = "",
                @SerializedName("last_modified_by")
                val lastModifiedBy: String? = "",
                @SerializedName("user_fullname")
                val userFullname: String? = "",
                @SerializedName("fcm_token")
                val fcmToken: String? = "",
                @SerializedName("role")
                val role: Role? = Role(),
                @SerializedName("region")
                val region: Region? = Region(),
                @SerializedName("user_region")
                val userRegion: List<UserRegion?>? = listOf()
            ) {
                data class Role(
                    @SerializedName("role_id")
                    val roleId: String? = "",
                    @SerializedName("role_name")
                    val roleName: String? = "",
                    @SerializedName("created_at")
                    val createdAt: String? = "",
                    @SerializedName("updated_at")
                    val updatedAt: String? = ""
                )

                data class Region(
                    @SerializedName("region_id")
                    val regionId: String? = "",
                    @SerializedName("region_code")
                    val regionCode: String? = "",
                    @SerializedName("region_name")
                    val regionName: String? = "",
                    @SerializedName("created_date")
                    val createdDate: String? = "",
                    @SerializedName("created_by")
                    val createdBy: String? = "",
                    @SerializedName("last_modified_date")
                    val lastModifiedDate: String? = "",
                    @SerializedName("last_modified_by")
                    val lastModifiedBy: String? = "",
                    @SerializedName("timezone")
                    val timezone: String? = ""
                )

                data class UserRegion(
                    @SerializedName("user_region_id")
                    val userRegionId: String? = "",
                    @SerializedName("user_id")
                    val userId: String? = "",
                    @SerializedName("region_id")
                    val regionId: String? = "",
                    @SerializedName("created_at")
                    val createdAt: String? = "",
                    @SerializedName("updated_at")
                    val updatedAt: String? = "",
                    @SerializedName("region")
                    val region: Region? = Region()
                ) {
                    data class Region(
                        @SerializedName("region_id")
                        val regionId: String? = "",
                        @SerializedName("region_code")
                        val regionCode: String? = "",
                        @SerializedName("region_name")
                        val regionName: String? = "",
                        @SerializedName("created_date")
                        val createdDate: String? = "",
                        @SerializedName("created_by")
                        val createdBy: String? = "",
                        @SerializedName("last_modified_date")
                        val lastModifiedDate: String? = "",
                        @SerializedName("last_modified_by")
                        val lastModifiedBy: String? = "",
                        @SerializedName("timezone")
                        val timezone: String? = ""
                    )
                }
            }

            data class Call(
                @SerializedName("service_call_id")
                val serviceCallId: String? = "",
                @SerializedName("region_id")
                val regionId: String? = "",
                @SerializedName("case_id")
                val caseId: String? = "",
                @SerializedName("account_id")
                val accountId: String? = "",
                @SerializedName("service_call_number")
                val serviceCallNumber: String? = "",
                @SerializedName("service_call_subject")
                val serviceCallSubject: String? = "",
                @SerializedName("service_call_description")
                val serviceCallDescription: String? = "",
                @SerializedName("call_status")
                val callStatus: String? = "",
                @SerializedName("start_date")
                val startDate: String? = "",
                @SerializedName("end_date")
                val endDate: String? = "",
                @SerializedName("sales_person_id")
                val salesPersonId: String? = "",
                @SerializedName("case_sales_id")
                val caseSalesId: String? = "",
                @SerializedName("case_account_contact_id")
                val caseAccountContactId: String? = "",
                @SerializedName("case_account_field_pic_contac_id")
                val caseAccountFieldPicContacId: String? = "",
                @SerializedName("call_type")
                val callType: String? = "",
                @SerializedName("lead_engineer_id")
                val leadEngineerId: String? = "",
                @SerializedName("is_chargeable")
                val isChargeable: String? = "",
                @SerializedName("created_date")
                val createdDate: String? = "",
                @SerializedName("created_by")
                val createdBy: String? = "",
                @SerializedName("last_modified_date")
                val lastModifiedDate: String? = "",
                @SerializedName("last_modified_by")
                val lastModifiedBy: String? = "",
                @SerializedName("call_remark")
                val callRemark: String? = "",
                @SerializedName("call_status_remark")
                val callStatusRemark: String? = "",
                @SerializedName("start_time")
                val startTime: String? = "",
                @SerializedName("end_time")
                val endTime: String? = ""
            )
        }
    }
}