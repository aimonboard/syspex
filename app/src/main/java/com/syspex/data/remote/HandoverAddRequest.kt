package com.syspex.data.remote


import com.google.gson.annotations.SerializedName

data class HandoverAddRequest(
    @SerializedName("code")
    val code: String? = "",
    @SerializedName("status")
    val status: String? = "",
    @SerializedName("message")
    val message: String? = "",
    @SerializedName("data")
    val `data`: Data? = Data()
) {
    data class Data(
        @SerializedName("handover")
        val handover: Handover? = Handover(),
        @SerializedName("handover_equipment")
        val handoverEquipment: List<HandoverEquipment?>? = listOf(),
        @SerializedName("handover_accesories")
        val handoverAccesories: List<HandoverAccesory?>? = listOf(),
        @SerializedName("outstanding_matter")
        val outstandingMatter: List<OutstandingMatter?>? = listOf()
    ) {
        data class Handover(
            @SerializedName("created_by")
            val createdBy: String? = "",
            @SerializedName("handover_number")
            val handoverNumber: String? = "",
            @SerializedName("call_id")
            val callId: String? = "",
            @SerializedName("engineer_id")
            val engineerId: String? = "",
            @SerializedName("start_date")
            val startDate: String? = "",
            @SerializedName("end_date")
            val endDate: String? = "",
            @SerializedName("handover_type")
            val handoverType: String? = "",
            @SerializedName("completion_date")
            val completionDate: String? = "",
            @SerializedName("address_id")
            val addressId: String? = "",
            @SerializedName("account_contact_id")
            val accountContactId: String? = "",
            @SerializedName("handover_po_ref_number")
            val handoverPoRefNumber: String? = "",
            @SerializedName("handover_customer_agreed")
            val handoverCustomerAgreed: String? = "",
            @SerializedName("handover_outstanding_matters")
            val handoverOutstandingMatters: Any? = Any(),
            @SerializedName("supplier_pic_name")
            val supplierPicName: String? = "",
            @SerializedName("supplier_pic_position")
            val supplierPicPosition: String? = "",
            @SerializedName("supplier_signature_url")
            val supplierSignatureUrl: String? = "",
            @SerializedName("customer_pic_name")
            val customerPicName: String? = "",
            @SerializedName("customer_pic_position")
            val customerPicPosition: String? = "",
            @SerializedName("customer_signature_url")
            val customerSignatureUrl: String? = "",
            @SerializedName("last_modified_by")
            val lastModifiedBy: String? = "",
            @SerializedName("last_modified_date")
            val lastModifiedDate: String? = "",
            @SerializedName("created_date")
            val createdDate: String? = "",
            @SerializedName("id")
            val id: String? = ""
        )

        data class HandoverEquipment(
            @SerializedName("created_by")
            val createdBy: String? = "",
            @SerializedName("handover_id")
            val handoverId: String? = "",
            @SerializedName("equipment_id")
            val equipmentId: String? = "",
            @SerializedName("warranty_period")
            val warrantyPeriod: String? = "",
            @SerializedName("warranty_end_date")
            val warrantyEndDate: String? = "",
            @SerializedName("last_modified_by")
            val lastModifiedBy: String? = "",
            @SerializedName("last_modified_date")
            val lastModifiedDate: String? = "",
            @SerializedName("created_date")
            val createdDate: String? = "",
            @SerializedName("id")
            val id: String? = ""
        )

        data class HandoverAccesory(
            @SerializedName("created_by")
            val createdBy: String? = "",
            @SerializedName("last_modified_by")
            val lastModifiedBy: String? = "",
            @SerializedName("handover_id")
            val handoverId: String? = "",
            @SerializedName("product_description")
            val productDescription: String? = "",
            @SerializedName("product_number")
            val productNumber: String? = "",
            @SerializedName("qty")
            val qty: String? = "",
            @SerializedName("last_modified_date")
            val lastModifiedDate: String? = "",
            @SerializedName("created_date")
            val createdDate: String? = "",
            @SerializedName("id")
            val id: String? = ""
        )

        data class OutstandingMatter(
            @SerializedName("created_by")
            val createdBy: String? = "",
            @SerializedName("last_modified_by")
            val lastModifiedBy: String? = "",
            @SerializedName("handover_id")
            val handoverId: String? = "",
            @SerializedName("follow_up_desc")
            val followUpDesc: String? = "",
            @SerializedName("follow_up_by")
            val followUpBy: String? = "",
            @SerializedName("target_date")
            val targetDate: String? = "",
            @SerializedName("last_modified_date")
            val lastModifiedDate: String? = "",
            @SerializedName("created_date")
            val createdDate: String? = "",
            @SerializedName("id")
            val id: String? = ""
        )
    }
}