package com.syspex.data.remote


import com.google.gson.annotations.SerializedName

data class AccountRequest(
    @SerializedName("code")
    val code: String? = "",
    @SerializedName("status")
    val status: String? = "",
    @SerializedName("message")
    val message: String? = "",
    @SerializedName("data")
    val `data`: Data? = Data()
) {
    data class Data(
        @SerializedName("account_id")
        val accountId: String? = "",
        @SerializedName("region_id")
        val regionId: String? = "",
        @SerializedName("account_code")
        val accountCode: String? = "",
        @SerializedName("account_name")
        val accountName: String? = "",
        @SerializedName("sales_id")
        val salesId: String? = "",
        @SerializedName("active")
        val active: String? = "",
        @SerializedName("created_date")
        val createdDate: String? = "",
        @SerializedName("created_by")
        val createdBy: String? = "",
        @SerializedName("last_modified_date")
        val lastModifiedDate: String? = "",
        @SerializedName("last_modified_by")
        val lastModifiedBy: Any? = Any(),
        @SerializedName("internal_memo")
        val internalMemo: String? = "",
        @SerializedName("contact")
        val contact: List<GetDataRequest.Data.ServiceCase.Account.Contact?>? = listOf(),
        @SerializedName("address")
        val address: List<GetDataRequest.Data.ServiceCase.Account.Addres?>? = listOf(),
        @SerializedName("sales")
        val sales: GetDataRequest.Data.ServiceCase.Account.Sales? = GetDataRequest.Data.ServiceCase.Account.Sales(),
        @SerializedName("region")
        val region: GetDataRequest.Region? = GetDataRequest.Region(),
        @SerializedName("created_user")
        val createdUser: Any? = Any(),
        @SerializedName("modified_user")
        val modifiedUser: Any? = Any(),
        @SerializedName("attachment")
        val attachment: List<GetDataRequest.Attachment?>? = listOf(),
        @SerializedName("equipments")
        val equipments: List<GetDataRequest.EquipmentAccount?>? = listOf(),
        @SerializedName("agreements")
        val agreements: List<GetDataRequest.Data.ServiceCase.Agreement?>? = listOf()
    )
}