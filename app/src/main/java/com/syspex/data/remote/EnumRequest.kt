package com.syspex.data.remote


import com.google.gson.annotations.SerializedName

data class EnumRequest(
    @SerializedName("code")
    val code: String? = "",
    @SerializedName("status")
    val status: String? = "",
    @SerializedName("message")
    val message: String? = "",
    @SerializedName("data")
    val `data`: Data? = Data()
) {
    data class Data(
        @SerializedName("enum_list")
        val enumList: List<Enum?>? = listOf()
    ) {
        data class Enum(
            @SerializedName("enum_id")
            val enumId: String? = "",
            @SerializedName("enum_slug")
            val enumSlug: String? = "",
            @SerializedName("enum_name")
            val enumName: String? = "",
            @SerializedName("enum_table")
            val enumTable: String? = "",
            @SerializedName("enum_column")
            val enumColumn: String? = "",
            @SerializedName("created_at")
            val createdAt: String? = "",
            @SerializedName("updated_at")
            val updatedAt: String? = ""
        )
    }
}