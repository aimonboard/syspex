package com.syspex.data.remote


import com.google.gson.annotations.SerializedName

data class TrainingAddRequest(
    @SerializedName("code")
    val code: String? = "",
    @SerializedName("status")
    val status: String? = "",
    @SerializedName("message")
    val message: String? = "",
    @SerializedName("data")
    val `data`: Data? = Data()
) {
    data class Data(
        @SerializedName("training")
        val training: Training? = Training(),
        @SerializedName("training_equipment")
        val trainingEquipment: List<TrainingEquipment?>? = listOf(),
        @SerializedName("training_attendees")
        val trainingAttendees: List<TrainingAttendee?>? = listOf()
    ) {
        data class Training(
            @SerializedName("created_by")
            val createdBy: String? = "",
            @SerializedName("training_form_number")
            val trainingFormNumber: String? = "",
            @SerializedName("call_id")
            val callId: String? = "",
            @SerializedName("address_id")
            val addressId: String? = "",
            @SerializedName("training_start_date")
            val trainingStartDate: String? = "",
            @SerializedName("training_end_date")
            val trainingEndDate: String? = "",
            @SerializedName("training_type")
            val trainingType: String? = "",
            @SerializedName("training_detail")
            val trainingDetail: String? = "",
            @SerializedName("engineer_id")
            val engineerId: String? = "",
            @SerializedName("engineer_signature_url")
            val engineerSignatureUrl: String? = "",
            @SerializedName("officer_name")
            val officerName: String? = "",
            @SerializedName("account_contact_id")
            val accountContactId: String? = "",
            @SerializedName("customer_pic_name")
            val customerPicName: String? = "",
            @SerializedName("customer_pic_position")
            val customerPicPosition: String? = "",
            @SerializedName("customer_signature_url")
            val customerSignatureUrl: String? = "",
            @SerializedName("last_modified_by")
            val lastModifiedBy: String? = "",
            @SerializedName("last_modified_date")
            val lastModifiedDate: String? = "",
            @SerializedName("created_date")
            val createdDate: String? = "",
            @SerializedName("id")
            val id: String? = ""
        )

        data class TrainingEquipment(
            @SerializedName("created_by")
            val createdBy: String? = "",
            @SerializedName("training_form_id")
            val trainingFormId: String? = "",
            @SerializedName("equipment_id")
            val equipmentId: String? = "",
            @SerializedName("warranty_period")
            val warrantyPeriod: String? = "",
            @SerializedName("warranty_end_date")
            val warrantyEndDate: String? = "",
            @SerializedName("last_modified_by")
            val lastModifiedBy: String? = "",
            @SerializedName("last_modified_date")
            val lastModifiedDate: String? = "",
            @SerializedName("created_date")
            val createdDate: String? = "",
            @SerializedName("id")
            val id: String? = ""
        )

        data class TrainingAttendee(
            @SerializedName("created_by")
            val createdBy: String? = "",
            @SerializedName("last_modified_by")
            val lastModifiedBy: String? = "",
            @SerializedName("training_form_id")
            val trainingFormId: String? = "",
            @SerializedName("personel_name")
            val personelName: String? = "",
            @SerializedName("designation")
            val designation: String? = "",
            @SerializedName("last_modified_date")
            val lastModifiedDate: String? = "",
            @SerializedName("created_date")
            val createdDate: String? = "",
            @SerializedName("id")
            val id: String? = ""
        )
    }
}