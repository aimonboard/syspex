package com.syspex.data.remote


import com.google.gson.annotations.SerializedName
import com.syspex.data.local.database.product_document.ProductDocumentEntity

data class GetDataRequest(
    @SerializedName("code")
    val code: String? = "",
    @SerializedName("status")
    val status: String? = "",
    @SerializedName("message")
    val message: String? = "",
    @SerializedName("data")
    val `data`: Data? = Data()
) {
    data class Data(
        @SerializedName("page")
        val page: Int? = 0,
        @SerializedName("last_page")
        val lastPage: Int? = 0,
        @SerializedName("from_date")
        val fromDate: String? = "",
        @SerializedName("to_date")
        val toDate: String? = "",
        @SerializedName("length")
        val length: String? = "",
        @SerializedName("activity")
        val activity: Activity? = Activity(),
        @SerializedName("service_case")
        val serviceCase: List<ServiceCase?>? = listOf()
    ) {

        data class Activity(
            @SerializedName("success")
            val success: List<Succes?>? = listOf()
        ) {
            data class Succes(
                @SerializedName("activity_id")
                val activityId: String? = "",
                @SerializedName("region_id")
                val regionId: String? = "",
                @SerializedName("user_id")
                val userId: String? = "",
                @SerializedName("activity_type")
                val activityType: String? = "",
                @SerializedName("activity_start_time")
                val activityStartTime: String? = "",
                @SerializedName("activity_end_time")
                val activityEndTime: String? = "",
                @SerializedName("activity_subject")
                val activitySubject: String? = "",
                @SerializedName("activity_description")
                val activityDescription: String? = "",
                @SerializedName("activity_source_table")
                val activitySourceTable: String? = "",
                @SerializedName("activity_source_id")
                val activitySourceId: String? = "",
                @SerializedName("created_date")
                val createdDate: String? = "",
                @SerializedName("created_by")
                val createdBy: String? = "",
                @SerializedName("last_modified_date")
                val lastModifiedDate: String? = "",
                @SerializedName("last_modified_by")
                val lastModifiedBy: String? = "",
                @SerializedName("address")
                val address: String? = "",
                @SerializedName("activity_type_text")
                val activityTypeText: ActivityTypeText? = ActivityTypeText(),
                @SerializedName("created_by_detail")
                val createdByDetail: CreatedByDetail? = CreatedByDetail(),
                @SerializedName("last_modified_detail")
                val lastModifiedDetail: LastModifiedDetail? = LastModifiedDetail(),
                @SerializedName("assigned_detail")
                val assignedDetail: AssignedDetail? = AssignedDetail(),
                @SerializedName("call")
                val call: Call? = Call()
            ) {
                data class ActivityTypeText(
                    @SerializedName("activity_type_id")
                    val activityTypeId: String? = "",
                    @SerializedName("activity_type_code")
                    val activityTypeCode: String? = "",
                    @SerializedName("activity_type_name")
                    val activityTypeName: String? = "",
                    @SerializedName("activity_type_color")
                    val activityTypeColor: String? = "",
                    @SerializedName("activity_related_table")
                    val activityRelatedTable: String? = "",
                    @SerializedName("created_date")
                    val createdDate: String? = "",
                    @SerializedName("created_by")
                    val createdBy: String? = "",
                    @SerializedName("last_modified_date")
                    val lastModifiedDate: String? = "",
                    @SerializedName("last_modified_by")
                    val lastModifiedBy: String? = ""
                )

                data class CreatedByDetail(
                    @SerializedName("id")
                    val id: String? = "",
                    @SerializedName("region_id")
                    val regionId: String? = "",
                    @SerializedName("user_code")
                    val userCode: String? = "",
                    @SerializedName("user_first_name")
                    val userFirstName: String? = "",
                    @SerializedName("user_last_name")
                    val userLastName: String? = "",
                    @SerializedName("email")
                    val email: String? = "",
                    @SerializedName("email_verified_at")
                    val emailVerifiedAt: String? = "",
                    @SerializedName("phone")
                    val phone: String? = "",
                    @SerializedName("role_id")
                    val roleId: String? = "",
                    @SerializedName("created_date")
                    val createdDate: String? = "",
                    @SerializedName("created_by")
                    val createdBy: String? = "",
                    @SerializedName("last_modified_date")
                    val lastModifiedDate: String? = "",
                    @SerializedName("last_modified_by")
                    val lastModifiedBy: String? = "",
                    @SerializedName("user_fullname")
                    val userFullname: String? = "",
                    @SerializedName("fcm_token")
                    val fcmToken: String? = "",
                    @SerializedName("role")
                    val role: Role? = Role(),
                    @SerializedName("region")
                    val region: Region? = Region(),
                    @SerializedName("user_region")
                    val userRegion: List<Any?>? = listOf()
                ) {
                    data class Role(
                        @SerializedName("role_id")
                        val roleId: String? = "",
                        @SerializedName("role_name")
                        val roleName: String? = "",
                        @SerializedName("created_at")
                        val createdAt: String? = "",
                        @SerializedName("updated_at")
                        val updatedAt: String? = ""
                    )
                }

                data class LastModifiedDetail(
                    @SerializedName("id")
                    val id: String? = "",
                    @SerializedName("region_id")
                    val regionId: String? = "",
                    @SerializedName("user_code")
                    val userCode: String? = "",
                    @SerializedName("user_first_name")
                    val userFirstName: String? = "",
                    @SerializedName("user_last_name")
                    val userLastName: String? = "",
                    @SerializedName("email")
                    val email: String? = "",
                    @SerializedName("email_verified_at")
                    val emailVerifiedAt: String? = "",
                    @SerializedName("phone")
                    val phone: String? = "",
                    @SerializedName("role_id")
                    val roleId: String? = "",
                    @SerializedName("created_date")
                    val createdDate: String? = "",
                    @SerializedName("created_by")
                    val createdBy: Any? = Any(),
                    @SerializedName("last_modified_date")
                    val lastModifiedDate: String? = "",
                    @SerializedName("last_modified_by")
                    val lastModifiedBy: Any? = Any(),
                    @SerializedName("user_fullname")
                    val userFullname: String? = "",
                    @SerializedName("fcm_token")
                    val fcmToken: String? = "",
                    @SerializedName("role")
                    val role: Role? = Role(),
                    @SerializedName("region")
                    val region: Any? = Any(),
                    @SerializedName("user_region")
                    val userRegion: List<Any?>? = listOf()
                ) {
                    data class Role(
                        @SerializedName("role_id")
                        val roleId: String? = "",
                        @SerializedName("role_name")
                        val roleName: String? = "",
                        @SerializedName("created_at")
                        val createdAt: String? = "",
                        @SerializedName("updated_at")
                        val updatedAt: String? = ""
                    )
                }

                data class AssignedDetail(
                    @SerializedName("id")
                    val id: String? = "",
                    @SerializedName("region_id")
                    val regionId: String? = "",
                    @SerializedName("user_code")
                    val userCode: String? = "",
                    @SerializedName("user_first_name")
                    val userFirstName: String? = "",
                    @SerializedName("user_last_name")
                    val userLastName: String? = "",
                    @SerializedName("email")
                    val email: String? = "",
                    @SerializedName("email_verified_at")
                    val emailVerifiedAt: Any? = Any(),
                    @SerializedName("phone")
                    val phone: Any? = Any(),
                    @SerializedName("role_id")
                    val roleId: String? = "",
                    @SerializedName("created_date")
                    val createdDate: String? = "",
                    @SerializedName("created_by")
                    val createdBy: Any? = Any(),
                    @SerializedName("last_modified_date")
                    val lastModifiedDate: String? = "",
                    @SerializedName("last_modified_by")
                    val lastModifiedBy: Any? = Any(),
                    @SerializedName("user_fullname")
                    val userFullname: String? = "",
                    @SerializedName("fcm_token")
                    val fcmToken: String? = "",
                    @SerializedName("role")
                    val role: Role? = Role(),
                    @SerializedName("region")
                    val region: Region? = Region(),
                    @SerializedName("user_region")
                    val userRegion: List<UserRegion?>? = listOf()
                ) {
                    data class Role(
                        @SerializedName("role_id")
                        val roleId: String? = "",
                        @SerializedName("role_name")
                        val roleName: String? = "",
                        @SerializedName("created_at")
                        val createdAt: String? = "",
                        @SerializedName("updated_at")
                        val updatedAt: String? = ""
                    )

                    data class Region(
                        @SerializedName("region_id")
                        val regionId: String? = "",
                        @SerializedName("region_code")
                        val regionCode: String? = "",
                        @SerializedName("region_name")
                        val regionName: String? = "",
                        @SerializedName("created_date")
                        val createdDate: String? = "",
                        @SerializedName("created_by")
                        val createdBy: Any? = Any(),
                        @SerializedName("last_modified_date")
                        val lastModifiedDate: String? = "",
                        @SerializedName("last_modified_by")
                        val lastModifiedBy: Any? = Any(),
                        @SerializedName("timezone")
                        val timezone: String? = ""
                    )

                    data class UserRegion(
                        @SerializedName("user_region_id")
                        val userRegionId: String? = "",
                        @SerializedName("user_id")
                        val userId: String? = "",
                        @SerializedName("region_id")
                        val regionId: String? = "",
                        @SerializedName("created_at")
                        val createdAt: String? = "",
                        @SerializedName("updated_at")
                        val updatedAt: String? = "",
                        @SerializedName("region")
                        val region: Region? = Region()
                    ) {
                        data class Region(
                            @SerializedName("region_id")
                            val regionId: String? = "",
                            @SerializedName("region_code")
                            val regionCode: String? = "",
                            @SerializedName("region_name")
                            val regionName: String? = "",
                            @SerializedName("created_date")
                            val createdDate: String? = "",
                            @SerializedName("created_by")
                            val createdBy: Any? = Any(),
                            @SerializedName("last_modified_date")
                            val lastModifiedDate: String? = "",
                            @SerializedName("last_modified_by")
                            val lastModifiedBy: Any? = Any(),
                            @SerializedName("timezone")
                            val timezone: String? = ""
                        )
                    }
                }

                data class Call(
                    @SerializedName("service_call_id")
                    val serviceCallId: String? = "",
                    @SerializedName("region_id")
                    val regionId: String? = "",
                    @SerializedName("case_id")
                    val caseId: String? = "",
                    @SerializedName("account_id")
                    val accountId: String? = "",
                    @SerializedName("service_call_number")
                    val serviceCallNumber: String? = "",
                    @SerializedName("service_call_subject")
                    val serviceCallSubject: String? = "",
                    @SerializedName("service_call_description")
                    val serviceCallDescription: String? = "",
                    @SerializedName("call_status")
                    val callStatus: String? = "",
                    @SerializedName("start_date")
                    val startDate: String? = "",
                    @SerializedName("end_date")
                    val endDate: String? = "",
                    @SerializedName("sales_person_id")
                    val salesPersonId: String? = "",
                    @SerializedName("case_sales_id")
                    val caseSalesId: String? = "",
                    @SerializedName("case_account_contact_id")
                    val caseAccountContactId: String? = "",
                    @SerializedName("case_account_field_pic_contac_id")
                    val caseAccountFieldPicContacId: String? = "",
                    @SerializedName("call_type")
                    val callType: String? = "",
                    @SerializedName("lead_engineer_id")
                    val leadEngineerId: String? = "",
                    @SerializedName("is_chargeable")
                    val isChargeable: Any? = Any(),
                    @SerializedName("created_date")
                    val createdDate: String? = "",
                    @SerializedName("created_by")
                    val createdBy: String? = "",
                    @SerializedName("last_modified_date")
                    val lastModifiedDate: String? = "",
                    @SerializedName("last_modified_by")
                    val lastModifiedBy: String? = "",
                    @SerializedName("call_remark")
                    val callRemark: Any? = Any(),
                    @SerializedName("call_status_remark")
                    val callStatusRemark: Any? = Any(),
                    @SerializedName("start_time")
                    val startTime: Any? = Any(),
                    @SerializedName("end_time")
                    val endTime: Any? = Any()
                )
            }
        }


        // =========================================================================================
        data class ServiceCase(
            @SerializedName("case_id")
            val caseId: String? = "",
            @SerializedName("region_id")
            val regionId: String? = "",
            @SerializedName("account_id")
            val accountId: String? = "",
            @SerializedName("case_number")
            val caseNumber: String? = "",
            @SerializedName("case_type")
            val caseType: String? = "",
            @SerializedName("case_type_remark")
            val caseTypeRemark: String? = "",
            @SerializedName("case_account_contact_id")
            val caseAccountContactId: String? = "",
            @SerializedName("case_field_account_contact_id")
            val caseFieldAccountContactId: String? = "",
            @SerializedName("salesperson_id")
            val salespersonId: String? = "",
            @SerializedName("case_sales_id")
            val caseSalesId: String? = "",
            @SerializedName("lead_engineer_id")
            val leadEngineerId: String? = "",
            @SerializedName("case_owner_id")
            val caseOwnerId: String? = "",
            @SerializedName("case_status")
            val caseStatus: String? = "",
            @SerializedName("case_status_remark")
            val caseStatusRemark: String? = "",
            @SerializedName("subject")
            val subject: String? = "",
            @SerializedName("description")
            val description: String? = "",
            @SerializedName("address_id")
            val addressId: String? = "",
            @SerializedName("loan_demo_period")
            val loanDemoPeriod: String? = "",
            @SerializedName("agreement_id")
            val agreementId: String? = "",
            @SerializedName("opened_time")
            val openedTime: String? = "",
            @SerializedName("assigned_time")
            val assignedTime: String? = "",
            @SerializedName("proposed_delivery_date")
            val proposedDeliveryDate: String? = "",
            @SerializedName("proposed_installation_date")
            val proposedInstallationDate: String? = "",
            @SerializedName("direct_delivery")
            val directDelivery: String? = "",
            @SerializedName("customer_po")
            val customerPo: String? = "",
            @SerializedName("no_of_free_service")
            val noOfFreeService: String? = "",
            @SerializedName("material_needed")
            val materialNeeded: String? = "",
            @SerializedName("air_utility_supplied_by")
            val airUtilitySuppliedBy: String? = "",
            @SerializedName("special_requirement")
            val specialRequirement: String? = "",
            @SerializedName("warranty_period_supplier")
            val warrantyPeriodSupplier: String? = "",
            @SerializedName("warranty_period_customer")
            val warrantyPeriodCustomer: Any? = Any(),
            @SerializedName("professional_inspection_required")
            val professionalInspectionRequired: String? = "",
            @SerializedName("inspection_remark")
            val inspectionRemark: String? = "",
            @SerializedName("case_color_calendar")
            val caseColorCalendar: Any? = Any(),
            @SerializedName("created_date")
            val createdDate: String? = "",
            @SerializedName("created_by")
            val createdBy: String? = "",
            @SerializedName("last_modified_date")
            val lastModifiedDate: String? = "",
            @SerializedName("last_modified_by")
            val lastModifiedBy: String? = "",
            @SerializedName("due_days")
            val dueDays: String? = "",
            @SerializedName("account")
            val account: Account? = Account(),
            @SerializedName("agreement")
            val agreement: Agreement? = Agreement(),
            @SerializedName("service_call")
            val serviceCall: List<ServiceCall?>? = listOf(),
            @SerializedName("activity")
            val activity: Activity? = Activity(),
            @SerializedName("region")
            val region: Region? = Region(),
            @SerializedName("address")
            val address: Address? = Address(),
            @SerializedName("field_account_contact")
            val fieldAccountContact: FieldAccountContact? = FieldAccountContact(),
            @SerializedName("account_contact")
            val accountContact: AccountContact? = AccountContact(),
            @SerializedName("owner")
            val owner: Owner? = Owner(),
            @SerializedName("lead_engineer")
            val leadEngineer: LeadEngineer? = LeadEngineer(),
            @SerializedName("sales_pic")
            val salesPic: SalesPic? = SalesPic(),
            @SerializedName("account_sales_pic")
            val accountSalesPic: AccountSalesPic? = AccountSalesPic(),
            @SerializedName("created_by_user")
            val createdByUser: CreatedByUser? = CreatedByUser(),
            @SerializedName("modified_by_user")
            val modifiedByUser: ModifiedByUser? = ModifiedByUser(),
            @SerializedName("case_type_text")
            val caseTypeText: CaseTypeText? = CaseTypeText(),
            @SerializedName("case_status_text")
            val caseStatusText: CaseStatusText? = CaseStatusText(),
            @SerializedName("air_utility_supplied")
            val airUtilitySupplied: AirUtilitySupplied? = AirUtilitySupplied(),
            @SerializedName("service_case_equipments")
            val serviceCaseEquipments: List<ServiceCaseEquipment?>? = listOf()
        ) {
            data class Account(
                @SerializedName("account_id")
                val accountId: String? = "",
                @SerializedName("region_id")
                val regionId: String? = "",
                @SerializedName("account_code")
                val accountCode: String? = "",
                @SerializedName("account_name")
                val accountName: String? = "",
                @SerializedName("sales_id")
                val salesId: String? = "",
                @SerializedName("active")
                val active: String? = "",
                @SerializedName("created_date")
                val createdDate: String? = "",
                @SerializedName("created_by")
                val createdBy: String? = "",
                @SerializedName("last_modified_date")
                val lastModifiedDate: String? = "",
                @SerializedName("last_modified_by")
                val lastModifiedBy: String? = "",
                @SerializedName("internal_memo")
                val internalMemo: String? = "",
                @SerializedName("contact")
                val contact: List<Contact?>? = listOf(),
                @SerializedName("address")
                val address: List<Addres?>? = listOf(),
                @SerializedName("sales")
                val sales: Sales? = Sales(),
                @SerializedName("region")
                val region: Region? = Region(),
                @SerializedName("created_user")
                val createdUser: CreatedByUser = CreatedByUser(),
                @SerializedName("modified_user")
                val modifiedUser: ModifiedByUser? = ModifiedByUser(),
                @SerializedName("equipments")
                val equipments: List<EquipmentAccount?>? = listOf(),
                @SerializedName("attachment")
                val attachment: List<Attachment?>? = listOf()
            ) {
                data class Contact(
                    @SerializedName("account_contact_id")
                    val accountContactId: String? = "",
                    @SerializedName("region_id")
                    val regionId: String? = "",
                    @SerializedName("customer_id")
                    val customerId: String? = "",
                    @SerializedName("contact_person_name")
                    val contactPersonName: String? = "",
                    @SerializedName("contact_person_phone")
                    val contactPersonPhone: String? = "",
                    @SerializedName("contact_person_email")
                    val contactPersonEmail: String? = "",
                    @SerializedName("contact_person_address")
                    val contactPersonAddress: String? = "",
                    @SerializedName("contact_person_position")
                    val contactPersonPosition: String? = "",
                    @SerializedName("contact_type")
                    val contactType: String? = "",
                    @SerializedName("priority")
                    val priority: String? = "",
                    @SerializedName("created_date")
                    val createdDate: String? = "",
                    @SerializedName("created_by")
                    val createdBy: String? = "",
                    @SerializedName("last_modified_date")
                    val lastModifiedDate: String? = "",
                    @SerializedName("last_modified_by")
                    val lastModifiedBy: String? = "",
                    @SerializedName("contact_type_text")
                    val contactTypeText: ContactTypeText? = ContactTypeText()
                ) {
                    data class ContactTypeText(
                        @SerializedName("enum_id")
                        val enumId: String? = "",
                        @SerializedName("enum_slug")
                        val enumSlug: String? = "",
                        @SerializedName("enum_name")
                        val enumName: String? = "",
                        @SerializedName("enum_table")
                        val enumTable: String? = "",
                        @SerializedName("enum_column")
                        val enumColumn: String? = "",
                        @SerializedName("created_at")
                        val createdAt: String? = "",
                        @SerializedName("updated_at")
                        val updatedAt: String? = ""
                    )
                }

                data class Addres(
                    @SerializedName("account_address_id")
                    val accountAddressId: String? = "",
                    @SerializedName("region_id")
                    val regionId: String? = "",
                    @SerializedName("account_id")
                    val accountId: String? = "",
                    @SerializedName("address_title")
                    val addressTitle: String? = "",
                    @SerializedName("address_line_1")
                    val addressLine1: String? = "",
                    @SerializedName("address_line_2")
                    val addressLine2: String? = "",
                    @SerializedName("address_line_3")
                    val addressLine3: String? = "",
                    @SerializedName("address_country")
                    val addressCountry: String? = "",
                    @SerializedName("address_city")
                    val addressCity: String? = "",
                    @SerializedName("address_zip_code")
                    val addressZipCode: String? = "",
                    @SerializedName("priority")
                    val priority: String? = "",
                    @SerializedName("created_date")
                    val createdDate: String? = "",
                    @SerializedName("created_by")
                    val createdBy: String? = "",
                    @SerializedName("last_modified_date")
                    val lastModifiedDate: String? = "",
                    @SerializedName("last_modified_by")
                    val lastModifiedBy: String? = "",
                    @SerializedName("full_address")
                    val fullAddress: String? = ""
                )

                data class Sales(
                    @SerializedName("id")
                    val id: String? = "",
                    @SerializedName("region_id")
                    val regionId: String? = "",
                    @SerializedName("user_code")
                    val userCode: String? = "",
                    @SerializedName("user_first_name")
                    val userFirstName: String? = "",
                    @SerializedName("user_last_name")
                    val userLastName: String? = "",
                    @SerializedName("email")
                    val email: String? = "",
                    @SerializedName("email_verified_at")
                    val emailVerifiedAt: Any? = Any(),
                    @SerializedName("phone")
                    val phone: String? = "",
                    @SerializedName("role_id")
                    val roleId: String? = "",
                    @SerializedName("created_date")
                    val createdDate: String? = "",
                    @SerializedName("created_by")
                    val createdBy: String? = "",
                    @SerializedName("last_modified_date")
                    val lastModifiedDate: String? = "",
                    @SerializedName("last_modified_by")
                    val lastModifiedBy: String? = "",
                    @SerializedName("user_fullname")
                    val userFullname: String? = "",
                    @SerializedName("role")
                    val role: Role? = Role(),
                    @SerializedName("region")
                    val region: Region? = Region()
                )
            }

            data class Agreement(
                @SerializedName("agreement_id")
                val agreementId: String? = "",
                @SerializedName("region_id")
                val regionId: String? = "",
                @SerializedName("account_id")
                val accountId: String? = "",
                @SerializedName("account_contact_id")
                val accountContactId: String? = "",
                @SerializedName("agreement_type")
                val agreementType: String? = "",
                @SerializedName("type_text")
                val agreementTypeEnum: TypeText? = TypeText(),
                @SerializedName("agreement_number")
                val agreementNumber: String? = "",
                @SerializedName("agreement_status")
                val agreementStatus: String? = "",
                @SerializedName("agreement_parent_id")
                val agreementParentId: String? = "",
                @SerializedName("internal_memo")
                val internalMemo: String? = "",
                @SerializedName("no_of_service")
                val noOfService: String? = "",
                @SerializedName("agreement_start_date")
                val agreementStartDate: String? = "",
                @SerializedName("agreement_end_date")
                val agreementEndDate: String? = "",
                @SerializedName("installation_delivery_address")
                val installationDeliveryAddress: String? = "",
                @SerializedName("installation_contact_pic")
                val installationContactPic: String? = "",
                @SerializedName("first_visit_date")
                val firstVisitDate: String? = "",
                @SerializedName("created_date")
                val createdDate: String? = "",
                @SerializedName("created_by")
                val createdBy: String? = "",
                @SerializedName("last_modified_date")
                val lastModifiedDate: String? = "",
                @SerializedName("last_modified_by")
                val lastModifiedBy: String? = "",
                @SerializedName("status_text")
                val statusText: StatusText? = StatusText(),
                @SerializedName("equipments")
                val equipments: List<Equipment?>? = listOf(),
                @SerializedName("installation_delivery_address_data")
                val installationDeliveryAddressData: InstallationDeliveryAddressData? = InstallationDeliveryAddressData(),
                @SerializedName("installation_contact_pic_data")
                val installationContactPicData: InstallationContactPicData? = InstallationContactPicData(),
                @SerializedName("created_by_user")
                val createdByUser: CreatedByUser? = CreatedByUser(),
                @SerializedName("modified_by_user")
                val modifiedByUser: ModifiedByUser? = ModifiedByUser()
            ) {
                data class InstallationDeliveryAddressData(
                    @SerializedName("account_address_id")
                    val accountAddressId: String? = "",
                    @SerializedName("region_id")
                    val regionId: String? = "",
                    @SerializedName("account_id")
                    val accountId: String? = "",
                    @SerializedName("address_title")
                    val addressTitle: String? = "",
                    @SerializedName("address_line_1")
                    val addressLine1: String? = "",
                    @SerializedName("address_line_2")
                    val addressLine2: String? = "",
                    @SerializedName("address_line_3")
                    val addressLine3: String? = "",
                    @SerializedName("address_country")
                    val addressCountry: String? = "",
                    @SerializedName("address_city")
                    val addressCity: String? = "",
                    @SerializedName("address_zip_code")
                    val addressZipCode: String? = "",
                    @SerializedName("priority")
                    val priority: String? = "",
                    @SerializedName("created_date")
                    val createdDate: String? = "",
                    @SerializedName("created_by")
                    val createdBy: String? = "",
                    @SerializedName("last_modified_date")
                    val lastModifiedDate: String? = "",
                    @SerializedName("last_modified_by")
                    val lastModifiedBy: String? = "",
                    @SerializedName("full_address")
                    val fullAddress: String? = ""
                )

                data class InstallationContactPicData(
                    @SerializedName("account_contact_id")
                    val accountContactId: String? = "",
                    @SerializedName("region_id")
                    val regionId: String? = "",
                    @SerializedName("customer_id")
                    val customerId: String? = "",
                    @SerializedName("contact_person_name")
                    val contactPersonName: String? = "",
                    @SerializedName("contact_person_phone")
                    val contactPersonPhone: String? = "",
                    @SerializedName("contact_person_email")
                    val contactPersonEmail: String? = "",
                    @SerializedName("contact_person_address")
                    val contactPersonAddress: String? = "",
                    @SerializedName("contact_person_position")
                    val contactPersonPosition: String? = "",
                    @SerializedName("contact_type")
                    val contactType: String? = "",
                    @SerializedName("priority")
                    val priority: String? = "",
                    @SerializedName("created_date")
                    val createdDate: String? = "",
                    @SerializedName("created_by")
                    val createdBy: String? = "",
                    @SerializedName("last_modified_date")
                    val lastModifiedDate: String? = "",
                    @SerializedName("last_modified_by")
                    val lastModifiedBy: String? = ""
                )

                data class TypeText(
                    @SerializedName("enum_id")
                    val enumId: String? = "",
                    @SerializedName("enum_slug")
                    val enumSlug: String? = "",
                    @SerializedName("enum_name")
                    val enumName: String? = "",
                    @SerializedName("enum_table")
                    val enumTable: String? = "",
                    @SerializedName("enum_column")
                    val enumColumn: String? = "",
                    @SerializedName("created_at")
                    val createdAt: String? = "",
                    @SerializedName("updated_at")
                    val updatedAt: String? = ""
                )
            }

            data class ServiceCall(
                @SerializedName("service_call_id")
                val serviceCallId: String? = "",
                @SerializedName("region_id")
                val regionId: String? = "",
                @SerializedName("case_id")
                val caseId: String? = "",
                @SerializedName("account_id")
                val accountId: String? = "",
                @SerializedName("service_call_number")
                val serviceCallNumber: String? = "",
                @SerializedName("service_call_subject")
                val serviceCallSubject: String? = "",
                @SerializedName("service_call_description")
                val serviceCallDescription: String? = "",
                @SerializedName("call_status")
                val callStatus: String? = "",
                @SerializedName("start_date")
                val startDate: String? = "",
                @SerializedName("start_time")
                val startTime: String? = "",
                @SerializedName("end_date")
                val endDate: String? = "",
                @SerializedName("end_time")
                val endTime: String? = "",
                @SerializedName("sales_person_id")
                val salesPersonId: String? = "",
                @SerializedName("case_sales_id")
                val caseSalesId: String? = "",
                @SerializedName("case_account_contact_id")
                val caseAccountContactId: String? = "",
                @SerializedName("case_account_field_pic_contac_id")
                val caseAccountFieldPicContacId: String? = "",
                @SerializedName("call_type")
                val callType: String? = "",
                @SerializedName("lead_engineer_id")
                val leadEngineerId: String? = "",
                @SerializedName("is_chargeable")
                val isChargeable: String? = "",
                @SerializedName("created_date")
                val createdDate: String? = "",
                @SerializedName("created_by")
                val createdBy: String? = "",
                @SerializedName("last_modified_date")
                val lastModifiedDate: String? = "",
                @SerializedName("last_modified_by")
                val lastModifiedBy: String? = "",
                @SerializedName("training")
                val training: List<Training?>? = listOf(),
                @SerializedName("handover")
                val handover: List<Handover?>? = listOf(),
                @SerializedName("installation")
                val installation: List<Installation?>? = listOf(),
                @SerializedName("service_report")
                val serviceReport: List<ServiceReport?>? = listOf(),
                @SerializedName("service_call_equipments")
                val serviceCallEquipments: List<ServiceCallEquipment?>? = listOf(),
                @SerializedName("region")
                val region: Region? = Region(),
                @SerializedName("call_remark")
                val callRemark: String? = "",
                @SerializedName("call_status_remark")
                val callStatusRemark: String? = "",
                @SerializedName("call_status_text")
                val callStatusText: CallStatusText? = CallStatusText(),
                @SerializedName("call_type_text")
                val callTypeText: CallTypeText? = CallTypeText(),
                @SerializedName("account")
                val account: Account? = Account(),
                @SerializedName("created_by_user")
                val createdByUser: CreatedByUser? = CreatedByUser(),
                @SerializedName("modified_by_user")
                val modifiedByUser: ModifiedByUser? = ModifiedByUser(),
                @SerializedName("engineers")
                val engineers: List<Engineer?>? = listOf(),
                @SerializedName("activity")
                val activity: Activity? = Activity(),
                @SerializedName("account_sales_pic")
                val accountSalesPic: AccountSalesPic? = AccountSalesPic(),
                @SerializedName("sales_pic")
                val salesPic: SalesPic? = SalesPic(),
                @SerializedName("account_contact")
                val accountContact: AccountContact? = AccountContact(),
                @SerializedName("field_account_contact")
                val fieldAccountContact: FieldAccountContact? = FieldAccountContact(),
                @SerializedName("lead_engineer")
                val leadEngineer: LeadEngineer? = LeadEngineer()
            ) {

                data class Training(
                    @SerializedName("id")
                    val id: String? = "",
                    @SerializedName("call_id")
                    val callId: String? = "",
                    @SerializedName("address_id")
                    val addressId: String? = "",
                    @SerializedName("training_form_number")
                    val trainingFormNumber: String? = "",
                    @SerializedName("training_type")
                    val trainingType: String? = "",
                    @SerializedName("training_start_date")
                    val trainingStartDate: String? = "",
                    @SerializedName("training_detail")
                    val trainingDetail: String? = "",
                    @SerializedName("engineer_id")
                    val engineerId: String? = "",
                    @SerializedName("engineer_signature_url")
                    val engineerSignatureUrl: String? = "",
                    @SerializedName("customer_pic_name")
                    val customerPicName: String? = "",
                    @SerializedName("customer_pic_position")
                    val customerPicPosition: String? = "",
                    @SerializedName("customer_signature_url")
                    val customerSignatureUrl: String? = "",
                    @SerializedName("officer_name")
                    val officerName: String? = "",
                    @SerializedName("created_date")
                    val createdDate: String? = "",
                    @SerializedName("created_by")
                    val createdBy: String? = "",
                    @SerializedName("last_modified_date")
                    val lastModifiedDate: String? = "",
                    @SerializedName("last_modified_by")
                    val lastModifiedBy: String? = "",
                    @SerializedName("training_end_date")
                    val trainingEndDate: String? = "",
                    @SerializedName("account_contact_id")
                    val accountContactId: String? = ""
                )

                data class Handover(
                    @SerializedName("id")
                    val id: String? = "",
                    @SerializedName("call_id")
                    val callId: String? = "",
                    @SerializedName("address_id")
                    val addressId: String? = "",
                    @SerializedName("account_contact_id")
                    val accountContactId: String? = "",
                    @SerializedName("engineer_id")
                    val engineerId: String? = "",
                    @SerializedName("handover_number")
                    val handoverNumber: String? = "",
                    @SerializedName("handover_type")
                    val handoverType: String? = "",
                    @SerializedName("handover_po_ref_number")
                    val handoverPoRefNumber: String? = "",
                    @SerializedName("handover_customer_agreed")
                    val handoverCustomerAgreed: String? = "",
                    @SerializedName("handover_outstanding_matters")
                    val handoverOutstandingMatters: String? = "",
                    @SerializedName("start_date")
                    val startDate: String? = "",
                    @SerializedName("end_date")
                    val endDate: String? = "",
                    @SerializedName("completion_date")
                    val completionDate: String? = "",
                    @SerializedName("supplier_pic_name")
                    val supplierPicName: String? = "",
                    @SerializedName("supplier_pic_position")
                    val supplierPicPosition: String? = "",
                    @SerializedName("supplier_signature_url")
                    val supplierSignatureUrl: String? = "",
                    @SerializedName("customer_pic_name")
                    val customerPicName: String? = "",
                    @SerializedName("customer_pic_position")
                    val customerPicPosition: String? = "",
                    @SerializedName("customer_signature_url")
                    val customerSignatureUrl: String? = "",
                    @SerializedName("created_date")
                    val createdDate: String? = "",
                    @SerializedName("created_by")
                    val createdBy: String? = "",
                    @SerializedName("last_modified_date")
                    val lastModifiedDate: String? = "",
                    @SerializedName("last_modified_by")
                    val lastModifiedBy: String? = ""
                )

                data class Installation(
                    @SerializedName("id")
                    val id: String? = "",
                    @SerializedName("call_id")
                    val callId: String? = "",
                    @SerializedName("address_id")
                    val addressId: String? = "",
                    @SerializedName("account_contact_id")
                    val accountContactId: String? = "",
                    @SerializedName("engineer_id")
                    val engineerId: String? = "",
                    @SerializedName("handover_number")
                    val handoverNumber: String? = "",
                    @SerializedName("handover_type")
                    val handoverType: String? = "",
                    @SerializedName("handover_po_ref_number")
                    val handoverPoRefNumber: String? = "",
                    @SerializedName("handover_customer_agreed")
                    val handoverCustomerAgreed: String? = "",
                    @SerializedName("handover_outstanding_matters")
                    val handoverOutstandingMatters: String? = "",
                    @SerializedName("start_date")
                    val startDate: String? = "",
                    @SerializedName("end_date")
                    val endDate: String? = "",
                    @SerializedName("completion_date")
                    val completionDate: String? = "",
                    @SerializedName("supplier_pic_name")
                    val supplierPicName: String? = "",
                    @SerializedName("supplier_pic_position")
                    val supplierPicPosition: String? = "",
                    @SerializedName("supplier_signature_url")
                    val supplierSignatureUrl: String? = "",
                    @SerializedName("customer_pic_name")
                    val customerPicName: String? = "",
                    @SerializedName("customer_pic_position")
                    val customerPicPosition: String? = "",
                    @SerializedName("customer_signature_url")
                    val customerSignatureUrl: String? = "",
                    @SerializedName("created_date")
                    val createdDate: String? = "",
                    @SerializedName("created_by")
                    val createdBy: String? = "",
                    @SerializedName("last_modified_date")
                    val lastModifiedDate: String? = "",
                    @SerializedName("last_modified_by")
                    val lastModifiedBy: String? = ""
                )

                data class ServiceReport(
                    @SerializedName("service_report_id")
                    val serviceReportId: String? = "",
                    @SerializedName("region_id")
                    val regionId: String? = "",
                    @SerializedName("call_id")
                    val callId: String? = "",
                    @SerializedName("service_report_number")
                    val serviceReportNumber: String? = "",
                    @SerializedName("report_date")
                    val reportDate: String? = "",
                    @SerializedName("report_status")
                    val reportStatus: String? = "",
                    @SerializedName("field_pic_contact_id")
                    val fieldPicContactId: String? = "",
                    @SerializedName("serviced_by")
                    val servicedBy: String? = "",
                    @SerializedName("internal_memo")
                    val internalMemo: String? = "",
                    @SerializedName("customer_rating")
                    val customerRating: String? = "",
                    @SerializedName("feedback")
                    val feedback: String? = "",
                    @SerializedName("cp_number")
                    val cpNumber: String? = "",
                    @SerializedName("cp_name")
                    val cpName: String? = "",
                    @SerializedName("serviced_by_signature")
                    val servicedBySignature: String? = "",
                    @SerializedName("account_pic_signature")
                    val accountPicSignature: String? = "",
                    @SerializedName("is_chargeable")
                    val isChargeable: String? = "",
                    @SerializedName("employee_name")
                    val employeeName: String? = "",
                    @SerializedName("employee_id")
                    val employeeId: String? = "",
                    @SerializedName("created_date")
                    val createdDate: String? = "",
                    @SerializedName("created_by")
                    val createdBy: String? = "",
                    @SerializedName("last_modified_date")
                    val lastModifiedDate: String? = "",
                    @SerializedName("last_modified_by")
                    val lastModifiedBy: String? = "",
                    @SerializedName("service_report_number_temp")
                    val serviceReportNumberTemp: String? = "",
                    @SerializedName("account_signed_at")
                    val accountSignedAt: String? = "",
                    @SerializedName("region")
                    val region: Region? = Region(),
                    @SerializedName("field_account_contact")
                    val fieldAccountContact: FieldAccountContact? = FieldAccountContact(),
                    @SerializedName("serviced_by_user")
                    val servicedByUser: ServicedByUser? = ServicedByUser(),
                    @SerializedName("created_by_user")
                    val createdByUser: CreatedByUser? = CreatedByUser(),
                    @SerializedName("modified_by_user")
                    val modifiedByUser: ModifiedByUser? = ModifiedByUser(),
                    @SerializedName("service_report_time_tracking")
                    val serviceReportTimeTracking: List<ServiceReportTimeTracking?>? = listOf(),
                    @SerializedName("service_report_equipment")
                    val serviceReportEquipment: List<ServiceReportEquipment?>? = listOf(),
                    @SerializedName("report_status_text")
                    val reportStatusText: ReportStatusText? = ReportStatusText(),
                    @SerializedName("activity")
                    val activity: Activity? = Activity()
                ) {

                    data class ServicedByUser(
                        @SerializedName("id")
                        val id: String? = "",
                        @SerializedName("region_id")
                        val regionId: String? = "",
                        @SerializedName("user_code")
                        val userCode: String? = "",
                        @SerializedName("user_first_name")
                        val userFirstName: String? = "",
                        @SerializedName("user_last_name")
                        val userLastName: String? = "",
                        @SerializedName("email")
                        val email: String? = "",
                        @SerializedName("email_verified_at")
                        val emailVerifiedAt: Any? = Any(),
                        @SerializedName("phone")
                        val phone: String? = "",
                        @SerializedName("role_id")
                        val roleId: String? = "",
                        @SerializedName("created_date")
                        val createdDate: String? = "",
                        @SerializedName("created_by")
                        val createdBy: String? = "",
                        @SerializedName("last_modified_date")
                        val lastModifiedDate: String? = "",
                        @SerializedName("last_modified_by")
                        val lastModifiedBy: String? = "",
                        @SerializedName("user_fullname")
                        val userFullname: String? = "",
                        @SerializedName("role")
                        val role: Role? = Role(),
                        @SerializedName("region")
                        val region: Region? = Region()
                    )

                    data class ServiceReportTimeTracking(
                        @SerializedName("service_report_time_tracking_id")
                        val serviceReportTimeTrackingId: String? = "",
                        @SerializedName("report_id")
                        val reportId: String? = "",
                        @SerializedName("engineer_id")
                        val engineerId: String? = "",
                        @SerializedName("is_lead")
                        val isLead: String? = "",
                        @SerializedName("start_date_time")
                        val startDateTime: String? = "",
                        @SerializedName("end_date_time")
                        val endDateTime: String? = "",
                        @SerializedName("created_date")
                        val createdDate: String? = "",
                        @SerializedName("created_by")
                        val createdBy: String? = "",
                        @SerializedName("last_modified_date")
                        val lastModifiedDate: String? = "",
                        @SerializedName("last_modified_by")
                        val lastModifiedBy: String? = "",
                        @SerializedName("engineer")
                        val engineer: Engineer? = Engineer()
                    )

                    data class ServiceReportEquipment(
                        @SerializedName("service_report_equipment_id")
                        val serviceReportEquipmentId: String? = "",
                        @SerializedName("report_id")
                        val reportId: String? = "",
                        @SerializedName("equipment_id")
                        val equipmentId: String? = "",
                        @SerializedName("problem_symptom")
                        val problemSymptom: String? = "",
                        @SerializedName("status_after_service_text")
                        val statusAfterServiceText: StatusText? = StatusText(),
                        @SerializedName("status_after_service_remarks")
                        val statusAfterServiceRemarks: String? = "",
                        @SerializedName("installation_start_date")
                        val installationStartDate: String? = "",
                        @SerializedName("installation_matters")
                        val installationMatters: String? = "",
                        @SerializedName("checklist_template_id")
                        val checklistTemplateId: String? = "",
                        @SerializedName("created_date")
                        val createdDate: String? = "",
                        @SerializedName("created_by")
                        val createdBy: String? = "",
                        @SerializedName("last_modified_date")
                        val lastModifiedDate: String? = "",
                        @SerializedName("last_modified_by")
                        val lastModifiedBy: String? = "",
                        @SerializedName("equipment_name")
                        val equipmentName: String? = "",
                        @SerializedName("equipment")
                        val equipment: Equipment? = Equipment(),
                        @SerializedName("equipment_image")
                        val equipmentImage: List<EquipmentImage?>? = listOf(),
                        @SerializedName("equipment_solution")
                        val equipmentSolution: List<EquipmentSolution?>? = listOf(),
                        @SerializedName("equipment_checklist")
                        val equipmentChecklist: List<EquipmentChecklist?>? = listOf(),
                        @SerializedName("service_report_sparepart")
                        val serviceReportSparepart: List<ServiceReportSparepart?>? = listOf()
                    ) {

                        data class EquipmentImage(
                            @SerializedName("service_report_equipment_image_id")
                            val serviceReportEquipmentImageId: String? = "",
                            @SerializedName("report_equipment_id")
                            val reportEquipmentId: String? = "",
                            @SerializedName("image_url")
                            val imageUrl: String? = "",
                            @SerializedName("created_date")
                            val createdDate: String? = "",
                            @SerializedName("created_by")
                            val createdBy: String? = "",
                            @SerializedName("last_modified_date")
                            val lastModifiedDate: String? = "",
                            @SerializedName("last_modified_by")
                            val lastModifiedBy: String? = ""
                        )

                        data class EquipmentSolution(
                            @SerializedName("service_report_equipment_solution_id")
                            val serviceReportEquipmentSolutionId: String? = "",
                            @SerializedName("report_equipment_id")
                            val reportEquipmentId: String? = "",
                            @SerializedName("subject")
                            val subject: String? = "",
                            @SerializedName("problem_cause")
                            val problemCause: String? = "",
                            @SerializedName("solution")
                            val solution: String? = "",
                            @SerializedName("created_date")
                            val createdDate: String? = "",
                            @SerializedName("created_by")
                            val createdBy: String? = "",
                            @SerializedName("last_modified_date")
                            val lastModifiedDate: String? = "",
                            @SerializedName("last_modified_by")
                            val lastModifiedBy: String? = "",
                            @SerializedName("solution_image_cause")
                            val causeImage: List<SolutionImage?>? = listOf(),
                            @SerializedName("solution_image_solution")
                            val solutionImage: List<SolutionImage?>? = listOf()
                        ) {
                            data class SolutionImage(
                                @SerializedName("service_report_equipment_solution_image_id")
                                val serviceReportEquipmentSolutionImageId: String? = "",
                                @SerializedName("region_id")
                                val regionId: String? = "",
                                @SerializedName("report_equipment_solution_id")
                                val reportEquipmentSolutionId: String? = "",
                                @SerializedName("status")
                                val status: String? = "",
                                @SerializedName("image_url")
                                val imageUrl: String? = "",
                                @SerializedName("created_date")
                                val createdDate: String? = "",
                                @SerializedName("created_by")
                                val createdBy: String? = "",
                                @SerializedName("last_modified_date")
                                val lastModifiedDate: String? = "",
                                @SerializedName("last_modified_by")
                                val lastModifiedBy: String? = "",
                                @SerializedName("status_text")
                                val statusText: StatusText? = StatusText()
                            )
                        }

                        data class EquipmentChecklist(
                            @SerializedName("service_report_equipment_checklist_id")
                            val serviceReportEquipmentChecklistId: String? = "",
                            @SerializedName("report_equipment_id")
                            val reportEquipmentId: String? = "",
                            @SerializedName("question_id")
                            val questionId: String? = "",
                            @SerializedName("description")
                            val description: String? = "",
                            @SerializedName("status")
                            val status: String? = "",
                            @SerializedName("remarks")
                            val remarks: String? = "",
                            @SerializedName("created_date")
                            val createdDate: String? = "",
                            @SerializedName("created_by")
                            val createdBy: String? = "",
                            @SerializedName("last_modified_date")
                            val lastModifiedDate: String? = "",
                            @SerializedName("last_modified_by")
                            val lastModifiedBy: String? = "",
                            @SerializedName("status_text")
                            val statusText: StatusText? = StatusText(),
                            @SerializedName("checklist_template_question")
                            val checklistTemplateQuestion: ChecklistTemplateQuestion? = ChecklistTemplateQuestion(),
                            @SerializedName("checklist_image")
                            val checklistImage: List<ChecklistImage?>? = listOf()
                        ) {

                            data class ChecklistTemplateQuestion(
                                @SerializedName("id")
                                val id: String? = "",
                                @SerializedName("order_no")
                                val orderNo: String? = "",
                                @SerializedName("section_title")
                                val sectionTitle: String? = "",
                                @SerializedName("description")
                                val description: String? = "",
                                @SerializedName("default_remarks")
                                val defaultRemarks: String? = "",
                                @SerializedName("help_text")
                                val helpText: String? = "",
                                @SerializedName("help_image_url")
                                val helpImageUrl: String? = "",
                                @SerializedName("created_date")
                                val createdDate: String? = "",
                                @SerializedName("created_by")
                                val createdBy: String? = "",
                                @SerializedName("last_modified_date")
                                val lastModifiedDate: String? = "",
                                @SerializedName("last_modified_by")
                                val lastModifiedBy: String? = "",
                                @SerializedName("checklist_template_id")
                                val checklistTemplateId: String? = "",
                                @SerializedName("product_group_checklist_template")
                                val productGroupChecklistTemplate: ProductGroupChecklistTemplate? = ProductGroupChecklistTemplate()
                            ) {
                                data class ProductGroupChecklistTemplate(
                                    @SerializedName("product_group_checklist_template_id")
                                    val productGroupChecklistTemplateId: String? = "",
                                    @SerializedName("region_id")
                                    val regionId: String? = "",
                                    @SerializedName("product_group_id")
                                    val productGroupId: String? = "",
                                    @SerializedName("checklist_type")
                                    val checklistType: String? = "",
                                    @SerializedName("description")
                                    val description: String? = ""
                                )
                            }

                            data class ChecklistImage(
                                @SerializedName("service_report_equipment_checklist_image_id")
                                val serviceReportEquipmentChecklistImageId: String? = "",
                                @SerializedName("report_equipment_checlist_id")
                                val reportEquipmentCheclistId: String? = "",
                                @SerializedName("image_url")
                                val imageUrl: String? = "",
                                @SerializedName("created_date")
                                val createdDate: String? = "",
                                @SerializedName("created_by")
                                val createdBy: String? = "",
                                @SerializedName("last_modified_date")
                                val lastModifiedDate: String? = "",
                                @SerializedName("last_modified_by")
                                val lastModifiedBy: String? = ""
                            )
                        }

                        data class ServiceReportSparepart(
                            @SerializedName("report_equipment_id")
                            val reportEquipmentId: String? = "",
                            @SerializedName("spare_part_id")
                            val sparePartId: String? = "",
                            @SerializedName("spare_part_image_number")
                            val sparePartImageNumber: String? = "",
                            @SerializedName("spare_part_number")
                            val sparePartNumber: String? = "",
                            @SerializedName("spare_part_description")
                            val sparePartDescription: String? = "",
                            @SerializedName("spare_part_memo")
                            val sparePartMemo: String? = "",
                            @SerializedName("spare_part_type")
                            val sparePartType: String? = "",
                            @SerializedName("qty")
                            val qty: String? = "",
                            @SerializedName("created_date")
                            val createdDate: String? = "",
                            @SerializedName("created_by")
                            val createdBy: String? = "",
                            @SerializedName("last_modified_date")
                            val lastModifiedDate: String? = "",
                            @SerializedName("last_modified_by")
                            val lastModifiedBy: String? = "",
                            @SerializedName("service_report_sparepart_id")
                            val serviceReportSparepartId: String? = "",
                            @SerializedName("spare_part_type_text")
                            val sparePartTypeText: SparePartTypeText? = SparePartTypeText(),
                            @SerializedName("sparepart")
                            val sparepart: Sparepart? = Sparepart()
                        ) {
                            data class SparePartTypeText(
                                @SerializedName("enum_id")
                                val enumId: String? = "",
                                @SerializedName("enum_slug")
                                val enumSlug: String? = "",
                                @SerializedName("enum_name")
                                val enumName: String? = "",
                                @SerializedName("enum_table")
                                val enumTable: String? = "",
                                @SerializedName("enum_column")
                                val enumColumn: String? = "",
                                @SerializedName("created_at")
                                val createdAt: String? = "",
                                @SerializedName("updated_at")
                                val updatedAt: String? = ""
                            )

                            data class Sparepart(
                                @SerializedName("part_id")
                                val partId: String? = "",
                                @SerializedName("part_no")
                                val partNo: String? = "",
                                @SerializedName("part_name")
                                val partName: String? = "",
                                @SerializedName("created_date")
                                val createdDate: String? = "",
                                @SerializedName("created_by")
                                val createdBy: String? = "",
                                @SerializedName("last_modified_date")
                                val lastModifiedDate: String? = "",
                                @SerializedName("last_modified_by")
                                val lastModifiedBy: String? = ""
                            )
                        }
                    }

                    data class ReportStatusText(
                        @SerializedName("enum_id")
                        val enumId: String? = "",
                        @SerializedName("enum_slug")
                        val enumSlug: String? = "",
                        @SerializedName("enum_name")
                        val enumName: String? = "",
                        @SerializedName("enum_table")
                        val enumTable: String? = "",
                        @SerializedName("enum_column")
                        val enumColumn: String? = "",
                        @SerializedName("created_at")
                        val createdAt: String? = "",
                        @SerializedName("updated_at")
                        val updatedAt: String? = ""
                    )
                }

                data class ServiceCallEquipment(
                    @SerializedName("service_call_equipment_id")
                    val serviceCallEquipmentId: String? = "",
                    @SerializedName("call_id")
                    val callId: String? = "",
                    @SerializedName("equipment_id")
                    val equipmentId: String? = "",
                    @SerializedName("agreement_id")
                    val agreementId: String? = "",
                    @SerializedName("warranty_start_date")
                    val warrantyStartDate: String? = "",
                    @SerializedName("warranty_end_date")
                    val warrantyEndDate: String? = "",
                    @SerializedName("created_date")
                    val createdDate: String? = "",
                    @SerializedName("created_by")
                    val createdBy: String? = "",
                    @SerializedName("last_modified_date")
                    val lastModifiedDate: String? = "",
                    @SerializedName("last_modified_by")
                    val lastModifiedBy: String? = "",
                    @SerializedName("equipment_name")
                    val equipmentName: String? = "",
                    @SerializedName("equipment")
                    val equipment: Equipment? = Equipment()
                )

                data class CallStatusText(
                    @SerializedName("enum_id")
                    val enumId: String? = "",
                    @SerializedName("enum_slug")
                    val enumSlug: String? = "",
                    @SerializedName("enum_name")
                    val enumName: String? = "",
                    @SerializedName("enum_table")
                    val enumTable: String? = "",
                    @SerializedName("enum_column")
                    val enumColumn: String? = "",
                    @SerializedName("created_at")
                    val createdAt: String? = "",
                    @SerializedName("updated_at")
                    val updatedAt: String? = ""
                )

                data class CallTypeText(
                    @SerializedName("enum_id")
                    val enumId: String? = "",
                    @SerializedName("enum_slug")
                    val enumSlug: String? = "",
                    @SerializedName("enum_name")
                    val enumName: String? = "",
                    @SerializedName("enum_table")
                    val enumTable: String? = "",
                    @SerializedName("enum_column")
                    val enumColumn: String? = "",
                    @SerializedName("created_at")
                    val createdAt: String? = "",
                    @SerializedName("updated_at")
                    val updatedAt: String? = ""
                )

                data class Account(
                    @SerializedName("account_id")
                    val accountId: String? = "",
                    @SerializedName("region_id")
                    val regionId: String? = "",
                    @SerializedName("account_code")
                    val accountCode: String? = "",
                    @SerializedName("account_name")
                    val accountName: String? = "",
                    @SerializedName("sales_id")
                    val salesId: String? = "",
                    @SerializedName("active")
                    val active: String? = "",
                    @SerializedName("created_date")
                    val createdDate: String? = "",
                    @SerializedName("created_by")
                    val createdBy: String? = "",
                    @SerializedName("last_modified_date")
                    val lastModifiedDate: String? = "",
                    @SerializedName("last_modified_by")
                    val lastModifiedBy: String? = "",
                    @SerializedName("internal_memo")
                    val internalMemo: String? = ""
                )
            }

            data class Address(
                @SerializedName("account_address_id")
                val accountAddressId: String? = "",
                @SerializedName("region_id")
                val regionId: String? = "",
                @SerializedName("account_id")
                val accountId: String? = "",
                @SerializedName("address_title")
                val addressTitle: String? = "",
                @SerializedName("address_line_1")
                val addressLine1: String? = "",
                @SerializedName("address_line_2")
                val addressLine2: String? = "",
                @SerializedName("address_line_3")
                val addressLine3: String? = "",
                @SerializedName("address_country")
                val addressCountry: String? = "",
                @SerializedName("address_city")
                val addressCity: String? = "",
                @SerializedName("address_zip_code")
                val addressZipCode: String? = "",
                @SerializedName("priority")
                val priority: String? = "",
                @SerializedName("created_date")
                val createdDate: String? = "",
                @SerializedName("created_by")
                val createdBy: String? = "",
                @SerializedName("last_modified_date")
                val lastModifiedDate: String? = "",
                @SerializedName("last_modified_by")
                val lastModifiedBy: String? = "",
                @SerializedName("full_address")
                val fullAddress: String? = ""
            )

            data class Owner(
                @SerializedName("id")
                val id: String? = "",
                @SerializedName("region_id")
                val regionId: String? = "",
                @SerializedName("user_code")
                val userCode: String? = "",
                @SerializedName("user_first_name")
                val userFirstName: String? = "",
                @SerializedName("user_last_name")
                val userLastName: String? = "",
                @SerializedName("email")
                val email: String? = "",
                @SerializedName("email_verified_at")
                val emailVerifiedAt: Any? = Any(),
                @SerializedName("phone")
                val phone: String? = "",
                @SerializedName("role_id")
                val roleId: String? = "",
                @SerializedName("created_date")
                val createdDate: String? = "",
                @SerializedName("created_by")
                val createdBy: String? = "",
                @SerializedName("last_modified_date")
                val lastModifiedDate: String? = "",
                @SerializedName("last_modified_by")
                val lastModifiedBy: String? = "",
                @SerializedName("user_fullname")
                val userFullname: String? = "",
                @SerializedName("role")
                val role: Role? = Role(),
                @SerializedName("region")
                val region: Region? = Region()
            )

            data class CaseTypeText(
                @SerializedName("enum_id")
                val enumId: String? = "",
                @SerializedName("enum_slug")
                val enumSlug: String? = "",
                @SerializedName("enum_name")
                val enumName: String? = "",
                @SerializedName("enum_table")
                val enumTable: String? = "",
                @SerializedName("enum_column")
                val enumColumn: String? = "",
                @SerializedName("created_at")
                val createdAt: String? = "",
                @SerializedName("updated_at")
                val updatedAt: String? = ""
            )

            data class CaseStatusText(
                @SerializedName("enum_id")
                val enumId: String? = "",
                @SerializedName("enum_slug")
                val enumSlug: String? = "",
                @SerializedName("enum_name")
                val enumName: String? = "",
                @SerializedName("enum_table")
                val enumTable: String? = "",
                @SerializedName("enum_column")
                val enumColumn: String? = "",
                @SerializedName("created_at")
                val createdAt: String? = "",
                @SerializedName("updated_at")
                val updatedAt: String? = ""
            )

            data class AirUtilitySupplied(
                @SerializedName("enum_id")
                val enumId: String? = "",
                @SerializedName("enum_slug")
                val enumSlug: String? = "",
                @SerializedName("enum_name")
                val enumName: String? = "",
                @SerializedName("enum_table")
                val enumTable: String? = "",
                @SerializedName("enum_column")
                val enumColumn: String? = "",
                @SerializedName("created_at")
                val createdAt: String? = "",
                @SerializedName("updated_at")
                val updatedAt: String? = ""
            )

            data class ServiceCaseEquipment(
                @SerializedName("sevice_case_equipment")
                val seviceCaseEquipment: String? = "",
                @SerializedName("case_id")
                val caseId: String? = "",
                @SerializedName("equipment_id")
                val equipmentId: String? = "",
                @SerializedName("agreement_id")
                val agreementId: String? = "",
                @SerializedName("created_date")
                val createdDate: String? = "",
                @SerializedName("created_by")
                val createdBy: String? = "",
                @SerializedName("last_modified_date")
                val lastModifiedDate: String? = "",
                @SerializedName("last_modified_by")
                val lastModifiedBy: String? = "",
                @SerializedName("equipment")
                val equipment: Equipment? = Equipment()
            )
        }
    }

    //============================================================================================== General Used
    data class Activity(
        @SerializedName("activity_id")
        val activityId: String? = "",
        @SerializedName("region_id")
        val regionId: String? = "",
        @SerializedName("user_id")
        val userId: String? = "",
        @SerializedName("activity_type")
        val activityType: String? = "",
        @SerializedName("activity_start_time")
        val activityStartTime: String? = "",
        @SerializedName("activity_end_time")
        val activityEndTime: String? = "",
        @SerializedName("activity_subject")
        val activitySubject: String? = "",
        @SerializedName("activity_description")
        val activityDescription: String? = "",
        @SerializedName("activity_source_table")
        val activitySourceTable: String? = "",
        @SerializedName("activity_source_id")
        val activitySourceId: String? = "",
        @SerializedName("created_date")
        val createdDate: String? = "",
        @SerializedName("created_by")
        val createdBy: String? = "",
        @SerializedName("last_modified_date")
        val lastModifiedDate: String? = "",
        @SerializedName("last_modified_by")
        val lastModifiedBy: String? = "",
        @SerializedName("address")
        val address: Any? = Any(),
        @SerializedName("activity_type_text")
        val activityTypeText: ActivityTypeText? = ActivityTypeText()
    ) {
        data class ActivityTypeText(
            @SerializedName("activity_type_id")
            val activityTypeId: String? = "",
            @SerializedName("activity_type_code")
            val activityTypeCode: String? = "",
            @SerializedName("activity_type_name")
            val activityTypeName: String? = "",
            @SerializedName("activity_type_color")
            val activityTypeColor: String? = "",
            @SerializedName("activity_related_table")
            val activityRelatedTable: Any? = Any(),
            @SerializedName("created_date")
            val createdDate: String? = "",
            @SerializedName("created_by")
            val createdBy: Any? = Any(),
            @SerializedName("last_modified_date")
            val lastModifiedDate: String? = "",
            @SerializedName("last_modified_by")
            val lastModifiedBy: String? = ""
        )
    }

    data class Attachment(
        @SerializedName("account_attachment_id")
        val accountAttachmentId: String? = "",
        @SerializedName("region_id")
        val regionId: String? = "",
        @SerializedName("account_id")
        val accountId: String? = "",
        @SerializedName("attachment_title")
        val attachmentTitle: String? = "",
        @SerializedName("attachment_body")
        val attachmentBody: String? = "",
        @SerializedName("file_url")
        val fileUrl: String? = "",
        @SerializedName("attachment_owner_id")
        val attachmentOwnerId: String? = "",
        @SerializedName("created_date")
        val createdDate: String? = "",
        @SerializedName("created_by_user")
        val createdByUser: CreatedByUser? = CreatedByUser(),
        @SerializedName("last_modified_date")
        val lastModifiedDate: String? = "",
        @SerializedName("modified_by_user")
        val modifiedByUser: ModifiedByUser? = ModifiedByUser(),
        @SerializedName("attachment_source_table")
        val attachmentSourceTable: String? = "",
        @SerializedName("attachment_source_id")
        val attachmentSourceId: String? = ""
    )

    data class Region(
        @SerializedName("region_id")
        val regionId: String? = "",
        @SerializedName("region_code")
        val regionCode: String? = "",
        @SerializedName("region_name")
        val regionName: String? = "",
        @SerializedName("created_date")
        val createdDate: String? = "",
        @SerializedName("created_by")
        val createdBy: String? = "",
        @SerializedName("last_modified_date")
        val lastModifiedDate: String? = "",
        @SerializedName("last_modified_by")
        val lastModifiedBy: String? = ""
    )

    data class StatusText(
        @SerializedName("enum_id")
        val enumId: String? = "",
        @SerializedName("enum_slug")
        val enumSlug: String? = "",
        @SerializedName("enum_name")
        val enumName: String? = "",
        @SerializedName("enum_table")
        val enumTable: String? = "",
        @SerializedName("enum_column")
        val enumColumn: String? = "",
        @SerializedName("created_at")
        val createdAt: String? = "",
        @SerializedName("updated_at")
        val updatedAt: String? = ""
    )

    data class AccountSalesPic(
        @SerializedName("id")
        val id: String? = "",
        @SerializedName("region_id")
        val regionId: String? = "",
        @SerializedName("user_code")
        val userCode: String? = "",
        @SerializedName("user_first_name")
        val userFirstName: String? = "",
        @SerializedName("user_last_name")
        val userLastName: String? = "",
        @SerializedName("email")
        val email: String? = "",
        @SerializedName("email_verified_at")
        val emailVerifiedAt: String? = "",
        @SerializedName("phone")
        val phone: String? = "",
        @SerializedName("role_id")
        val roleId: String? = "",
        @SerializedName("created_date")
        val createdDate: String? = "",
        @SerializedName("created_by")
        val createdBy: String? = "",
        @SerializedName("last_modified_date")
        val lastModifiedDate: String? = "",
        @SerializedName("last_modified_by")
        val lastModifiedBy: String? = "",
        @SerializedName("user_fullname")
        val userFullname: String? = ""
    )

    data class AccountContact(
        @SerializedName("account_contact_id")
        val accountContactId: String? = "",
        @SerializedName("region_id")
        val regionId: String? = "",
        @SerializedName("customer_id")
        val customerId: String? = "",
        @SerializedName("contact_person_name")
        val contactPersonName: String? = "",
        @SerializedName("contact_person_phone")
        val contactPersonPhone: String? = "",
        @SerializedName("contact_person_email")
        val contactPersonEmail: String? = "",
        @SerializedName("contact_person_address")
        val contactPersonAddress: String? = "",
        @SerializedName("contact_person_position")
        val contactPersonPosition: String? = "",
        @SerializedName("contact_type")
        val contactType: String? = "",
        @SerializedName("priority")
        val priority: String? = "",
        @SerializedName("created_date")
        val createdDate: String? = "",
        @SerializedName("created_by")
        val createdBy: String? = "",
        @SerializedName("last_modified_date")
        val lastModifiedDate: String? = "",
        @SerializedName("last_modified_by")
        val lastModifiedBy: String? = ""
    )

    data class LeadEngineer(
        @SerializedName("id")
        val id: String? = "",
        @SerializedName("region_id")
        val regionId: String? = "",
        @SerializedName("user_code")
        val userCode: String? = "",
        @SerializedName("user_first_name")
        val userFirstName: String? = "",
        @SerializedName("user_last_name")
        val userLastName: String? = "",
        @SerializedName("email")
        val email: String? = "",
        @SerializedName("email_verified_at")
        val emailVerifiedAt: String? = "",
        @SerializedName("phone")
        val phone: String? = "",
        @SerializedName("role_id")
        val roleId: String? = "",
        @SerializedName("created_date")
        val createdDate: String? = "",
        @SerializedName("created_by")
        val createdBy: String? = "",
        @SerializedName("last_modified_date")
        val lastModifiedDate: String? = "",
        @SerializedName("last_modified_by")
        val lastModifiedBy: String? = "",
        @SerializedName("user_fullname")
        val userFullname: String? = ""
    )

    data class Engineer(
        @SerializedName("id")
        val id: String? = "",
        @SerializedName("region_id")
        val regionId: String? = "",
        @SerializedName("user_code")
        val userCode: String? = "",
        @SerializedName("user_first_name")
        val userFirstName: String? = "",
        @SerializedName("user_last_name")
        val userLastName: String? = "",
        @SerializedName("email")
        val email: String? = "",
        @SerializedName("email_verified_at")
        val emailVerifiedAt: Any? = Any(),
        @SerializedName("phone")
        val phone: String? = "",
        @SerializedName("role_id")
        val roleId: String? = "",
        @SerializedName("created_date")
        val createdDate: String? = "",
        @SerializedName("created_by")
        val createdBy: String? = "",
        @SerializedName("last_modified_date")
        val lastModifiedDate: String? = "",
        @SerializedName("last_modified_by")
        val lastModifiedBy: String? = "",
        @SerializedName("user_fullname")
        val userFullname: String? = "",
        @SerializedName("role")
        val role: Role? = Role(),
        @SerializedName("region")
        val region: Region? = Region(),
        @SerializedName("pivot")
        val pivot: Pivot? = Pivot()
    ) {
        data class Pivot(
            @SerializedName("call_id")
            val callId: String? = "",
            @SerializedName("engineer_id")
            val engineerId: String? = "",
            @SerializedName("created_date")
            val createdDate: String? = "",
            @SerializedName("last_modified_date")
            val lastModifiedDate: String? = ""
        )
    }

    data class SalesPic(
        @SerializedName("id")
        val id: String? = "",
        @SerializedName("region_id")
        val regionId: String? = "",
        @SerializedName("user_code")
        val userCode: String? = "",
        @SerializedName("user_first_name")
        val userFirstName: String? = "",
        @SerializedName("user_last_name")
        val userLastName: String? = "",
        @SerializedName("email")
        val email: String? = "",
        @SerializedName("email_verified_at")
        val emailVerifiedAt: String? = "",
        @SerializedName("phone")
        val phone: String? = "",
        @SerializedName("role_id")
        val roleId: String? = "",
        @SerializedName("created_date")
        val createdDate: String? = "",
        @SerializedName("created_by")
        val createdBy: String? = "",
        @SerializedName("last_modified_date")
        val lastModifiedDate: String? = "",
        @SerializedName("last_modified_by")
        val lastModifiedBy: String? = "",
        @SerializedName("user_fullname")
        val userFullname: String? = ""
    )

    data class FieldAccountContact(
        @SerializedName("account_contact_id")
        val accountContactId: String? = "",
        @SerializedName("region_id")
        val regionId: String? = "",
        @SerializedName("customer_id")
        val customerId: String? = "",
        @SerializedName("contact_person_name")
        val contactPersonName: String? = "",
        @SerializedName("contact_person_phone")
        val contactPersonPhone: String? = "",
        @SerializedName("contact_person_email")
        val contactPersonEmail: String? = "",
        @SerializedName("contact_person_address")
        val contactPersonAddress: String? = "",
        @SerializedName("contact_person_position")
        val contactPersonPosition: String? = "",
        @SerializedName("contact_type")
        val contactType: String? = "",
        @SerializedName("priority")
        val priority: String? = "",
        @SerializedName("created_date")
        val createdDate: String? = "",
        @SerializedName("created_by")
        val createdBy: String? = "",
        @SerializedName("last_modified_date")
        val lastModifiedDate: String? = "",
        @SerializedName("last_modified_by")
        val lastModifiedBy: String? = ""
    )

    data class CreatedByUser(
        @SerializedName("id")
        val id: String? = "",
        @SerializedName("region_id")
        val regionId: String? = "",
        @SerializedName("user_code")
        val userCode: String? = "",
        @SerializedName("user_first_name")
        val userFirstName: String? = "",
        @SerializedName("user_last_name")
        val userLastName: String? = "",
        @SerializedName("email")
        val email: String? = "",
        @SerializedName("email_verified_at")
        val emailVerifiedAt: String? = "",
        @SerializedName("phone")
        val phone: String? = "",
        @SerializedName("role_id")
        val roleId: String? = "",
        @SerializedName("created_date")
        val createdDate: String? = "",
        @SerializedName("created_by")
        val createdBy: String? = "",
        @SerializedName("last_modified_date")
        val lastModifiedDate: String? = "",
        @SerializedName("last_modified_by")
        val lastModifiedBy: String? = "",
        @SerializedName("user_fullname")
        val userFullname: String? = ""
    )

    data class ModifiedByUser(
        @SerializedName("id")
        val id: String? = "",
        @SerializedName("region_id")
        val regionId: String? = "",
        @SerializedName("user_code")
        val userCode: String? = "",
        @SerializedName("user_first_name")
        val userFirstName: String? = "",
        @SerializedName("user_last_name")
        val userLastName: String? = "",
        @SerializedName("email")
        val email: String? = "",
        @SerializedName("email_verified_at")
        val emailVerifiedAt: String? = "",
        @SerializedName("phone")
        val phone: String? = "",
        @SerializedName("role_id")
        val roleId: String? = "",
        @SerializedName("created_date")
        val createdDate: String? = "",
        @SerializedName("created_by")
        val createdBy: String? = "",
        @SerializedName("last_modified_date")
        val lastModifiedDate: String? = "",
        @SerializedName("last_modified_by")
        val lastModifiedBy: String? = "",
        @SerializedName("user_fullname")
        val userFullname: String? = ""
    )

    data class Role(
        @SerializedName("role_id")
        val roleId: String? = "",
        @SerializedName("role_name")
        val roleName: String? = "",
        @SerializedName("created_at")
        val createdAt: String? = "",
        @SerializedName("updated_at")
        val updatedAt: String? = ""
    )

    // ============================================================================================= Equipment Master
    data class EquipmentAccount(
        @SerializedName("region_id")
        val regionId: String? = "",
        @SerializedName("equipment_id")
        val equipmentId: String? = "",
        @SerializedName("serial_no")
        val serialNo: String? = "",
        @SerializedName("brand")
        val brand: String? = "",
        @SerializedName("account_id")
        val accountId: String? = "",
        @SerializedName("product_id")
        val productId: String? = "",
        @SerializedName("warranty_start")
        val warrantyStart: String? = "",
        @SerializedName("warranty_end")
        val warrantyEnd: String? = "",
        @SerializedName("current_agreement_id")
        val currentAgreementId: String? = "",
        @SerializedName("equipment_status")
        val equipmentStatus: String? = "",
        @SerializedName("equipment_remarks")
        val equipmentRemarks: String? = "",
        @SerializedName("delivery_address_id")
        val deliveryAddressId: String? = "",
        @SerializedName("sales_order_no")
        val salesOrderNo: String? = "",
        @SerializedName("created_date")
        val createdDate: String? = "",
        @SerializedName("created_by")
        val createdBy: String? = "",
        @SerializedName("last_modified_date")
        val lastModifiedDate: String? = "",
        @SerializedName("last_modified_by")
        val lastModifiedBy: String? = "",
        @SerializedName("warranty_status")
        val warrantyStatus: String? = "",
        @SerializedName("current_agreement")
        val currentAgreement: Any? = Any(),
        @SerializedName("status_text")
        val statusText: StatusText? = StatusText(),
        @SerializedName("product")
        val product: Product? = Product()
    ) {

        data class Product(
            @SerializedName("product_id")
            val productId: String? = "",
            @SerializedName("product_code")
            val productCode: String? = "",
            @SerializedName("product_name")
            val productName: String? = "",
            @SerializedName("created_date")
            val createdDate: String? = "",
            @SerializedName("created_by")
            val createdBy: String? = "",
            @SerializedName("last_modified_date")
            val lastModifiedDate: String? = "",
            @SerializedName("last_modified_by")
            val lastModifiedBy: String? = "",
            @SerializedName("product_group_item")
            val productGroupItem: ProductGroupItem? = ProductGroupItem()
        ) {
            data class ProductGroupItem(
                @SerializedName("product_group_item_id")
                val productGroupItemId: String? = "",
                @SerializedName("product_group_id")
                val productGroupId: String? = "",
                @SerializedName("product_id")
                val productId: String? = "",
                @SerializedName("created_date")
                val createdDate: String? = "",
                @SerializedName("created_by")
                val createdBy: String? = "",
                @SerializedName("last_modified_date")
                val lastModifiedDate: String? = "",
                @SerializedName("last_modified_by")
                val lastModifiedBy: String? = "",
                @SerializedName("product_group")
                val productGroup: ProductGroup? = ProductGroup()
            ) {
                data class ProductGroup(
                    @SerializedName("id")
                    val productGroupId: String? = "",
                    @SerializedName("product_group_name")
                    val productGroupName: String? = "",
                    @SerializedName("product_group_tag_id")
                    val productGroupTagId: String? = "",
                    @SerializedName("machine_brand")
                    val machineBrand: String? = "",
                    @SerializedName("machine_supplier")
                    val machineSupplier: String? = "",
                    @SerializedName("machine_model")
                    val machineModel: String? = "",
                    @SerializedName("machine_class_category")
                    val machineClassCategory: String? = "",
                    @SerializedName("voltage")
                    val voltage: String? = "",
                    @SerializedName("current")
                    val current: String? = "",
                    @SerializedName("power")
                    val power: String? = "",
                    @SerializedName("phase")
                    val phase: String? = "",
                    @SerializedName("frequency")
                    val frequency: String? = "",
                    @SerializedName("air_supply_needed")
                    val airSupplyNeeded: String? = "",
                    @SerializedName("air_pressure")
                    val airPressure: String? = "",
                    @SerializedName("created_date")
                    val createdDate: String? = "",
                    @SerializedName("created_by")
                    val createdBy: String? = "",
                    @SerializedName("last_modified_date")
                    val lastModifiedDate: String? = "",
                    @SerializedName("last_modified_by")
                    val lastModifiedBy: String? = "",
                    @SerializedName("product_group_technical_documents")
                    val productGroupDocument: List<ProductGroupTechnicalDocuments?>? = listOf(),
                    @SerializedName("product_group_spare_part")
                    val productGroupSparePart: List<ProductGroupSparePart?>? = listOf(),
                    @SerializedName("product_group_checklist_template")
                    val productGroupChecklistTemplate: List<ProductGroupChecklistTemplate?>? = listOf()
                ) {
                    data class ProductGroupTechnicalDocuments(
                        @SerializedName("product_group_technical_document_id")
                        val productGroupTechnicalDocumentId: String? = "",
                        @SerializedName("product_group_id")
                        val productGroupId: String? = "",
                        @SerializedName("title")
                        val title: String? = "",
                        @SerializedName("url")
                        val url: String? = "",
                        @SerializedName("created_date")
                        val createdDate: String? = "",
                        @SerializedName("created_by")
                        val createdBy: String? = "",
                        @SerializedName("last_modified_date")
                        val lastModifiedDate: String? = "",
                        @SerializedName("last_modified_by")
                        val lastModifiedBy: String? = ""
                    )

                    data class ProductGroupSparePart(
                        @SerializedName("product_group_spare_part_id")
                        val productGroupSparePartId: String? = "",
                        @SerializedName("product_group_id")
                        val productGroupId: String? = "",
                        @SerializedName("part_id")
                        val partId: String? = "",
                        @SerializedName("technical_drawing_id")
                        val technicalDrawingId: String? = "",
                        @SerializedName("image_number")
                        val imageNumber: String? = "",
                        @SerializedName("created_date")
                        val createdDate: String? = "",
                        @SerializedName("created_by")
                        val createdBy: String? = "",
                        @SerializedName("last_modified_date")
                        val lastModifiedDate: String? = "",
                        @SerializedName("last_modified_by")
                        val lastModifiedBy: String? = "",
                        @SerializedName("spare_part")
                        val sparePart: SparePart? = SparePart(),
                        @SerializedName("technical_drawing")
                        val technicalDrawing: TechnicalDrawing? = TechnicalDrawing()
                    ) {
                        data class SparePart(
                            @SerializedName("part_id")
                            val partId: String? = "",
                            @SerializedName("part_no")
                            val partNo: String? = "",
                            @SerializedName("part_name")
                            val partName: String? = "",
                            @SerializedName("created_date")
                            val createdDate: String? = "",
                            @SerializedName("created_by")
                            val createdBy: String? = "",
                            @SerializedName("last_modified_date")
                            val lastModifiedDate: String? = "",
                            @SerializedName("last_modified_by")
                            val lastModifiedBy: String? = ""
                        )

                        data class TechnicalDrawing(
                            @SerializedName("product_group_technical_drawing_id")
                            val productGroupTechnicalDrawingId: String? = "",
                            @SerializedName("product_group_id")
                            val productGroupId: String? = "",
                            @SerializedName("title")
                            val title: String? = "",
                            @SerializedName("page")
                            val page: String? = "",
                            @SerializedName("url")
                            val url: String? = "",
                            @SerializedName("created_date")
                            val createdDate: String? = "",
                            @SerializedName("created_by")
                            val createdBy: String? = "",
                            @SerializedName("last_modified_date")
                            val lastModifiedDate: String? = "",
                            @SerializedName("last_modified_by")
                            val lastModifiedBy: String? = ""
                        )
                    }

                    data class ProductGroupChecklistTemplate(
                        @SerializedName("product_group_checklist_template_id")
                        val productGroupChecklistTemplateId: String? = "",
                        @SerializedName("region_id")
                        val regionId: String? = "",
                        @SerializedName("product_group_id")
                        val productGroupId: String? = "",
                        @SerializedName("checklist_type")
                        val checklistType: String? = "",
                        @SerializedName("description")
                        val description: String? = "",
                        @SerializedName("created_date")
                        val createdDate: String? = "",
                        @SerializedName("created_by")
                        val createdBy: String? = "",
                        @SerializedName("last_modified_date")
                        val lastModifiedDate: String? = "",
                        @SerializedName("last_modified_by")
                        val lastModifiedBy: String? = "",
                        @SerializedName("checklist_type_text")
                        val checklistTypeText: ChecklistTypeText? = ChecklistTypeText(),
                        @SerializedName("checklist_template_questions")
                        val checklistTemplateQuestions: List<ChecklistTemplateQuestions?>? = listOf()
                    ) {
                        data class ChecklistTypeText(
                            @SerializedName("enum_id")
                            val enumId: String? = "",
                            @SerializedName("enum_slug")
                            val enumSlug: String? = "",
                            @SerializedName("enum_name")
                            val enumName: String? = "",
                            @SerializedName("enum_table")
                            val enumTable: String? = "",
                            @SerializedName("enum_column")
                            val enumColumn: String? = "",
                            @SerializedName("created_at")
                            val createdAt: String? = "",
                            @SerializedName("updated_at")
                            val updatedAt: String? = ""
                        )

                        data class ChecklistTemplateQuestions(
                            @SerializedName("id")
                            val id: String? = "",
                            @SerializedName("order_no")
                            val orderNo: String? = "",
                            @SerializedName("section_title")
                            val sectionTitle: String? = "",
                            @SerializedName("description")
                            val description: String? = "",
                            @SerializedName("default_remarks")
                            val defaultRemarks: String? = "",
                            @SerializedName("help_text")
                            val helpText: String? = "",
                            @SerializedName("help_image_url")
                            val helpImageUrl: String? = "",
                            @SerializedName("created_date")
                            val createdDate: String? = "",
                            @SerializedName("created_by")
                            val createdBy: String? = "",
                            @SerializedName("last_modified_date")
                            val lastModifiedDate: String? = "",
                            @SerializedName("last_modified_by")
                            val lastModifiedBy: String? = "",
                            @SerializedName("checklist_template_id")
                            val checklistTemplateId: String? = ""
                        )
                    }
                }
            }
        }
    }

    // ============================================================================================= Equipment Child
    data class Equipment(
        @SerializedName("region_id")
        val regionId: String? = "",
        @SerializedName("equipment_id")
        val equipmentId: String? = "",
        @SerializedName("serial_no")
        val serialNo: String? = "",
        @SerializedName("brand")
        val brand: String? = "",
        @SerializedName("account_id")
        val accountId: String? = "",
        @SerializedName("product_id")
        val productId: String? = "",
        @SerializedName("warranty_start")
        val warrantyStart: String? = "",
        @SerializedName("warranty_end")
        val warrantyEnd: String? = "",
        @SerializedName("current_agreement_id")
        val currentAgreementId: String? = "",
        @SerializedName("equipment_status")
        val equipmentStatus: String? = "",
        @SerializedName("equipment_remarks")
        val equipmentRemarks: String? = "",
        @SerializedName("delivery_address_id")
        val deliveryAddressId: String? = "",
        @SerializedName("sales_order_no")
        val salesOrderNo: String? = "",
        @SerializedName("created_date")
        val createdDate: String? = "",
        @SerializedName("created_by")
        val createdBy: String? = "",
        @SerializedName("last_modified_date")
        val lastModifiedDate: String? = "",
        @SerializedName("last_modified_by")
        val lastModifiedBy: String? = "",
        @SerializedName("warranty_status")
        val warrantyStatus: String? = "",
        @SerializedName("status_text")
        val statusText: StatusText? = StatusText(),
        @SerializedName("product")
        val product: Product? = Product(),
        @SerializedName("pivot")
        val pivot: Pivot? = Pivot()
    ) {

        data class Product(
            @SerializedName("product_id")
            val productId: String? = "",
            @SerializedName("product_code")
            val productCode: String? = "",
            @SerializedName("product_name")
            val productName: String? = "",
            @SerializedName("created_date")
            val createdDate: String? = "",
            @SerializedName("created_by")
            val createdBy: String? = "",
            @SerializedName("last_modified_date")
            val lastModifiedDate: String? = "",
            @SerializedName("last_modified_by")
            val lastModifiedBy: String? = ""
        )

        // Only for agreement equipment
        data class Pivot(
            @SerializedName("agreement_id")
            val agreementId: String? = "",
            @SerializedName("equipment_id")
            val equipmentId: String? = "",
            @SerializedName("created_date")
            val createdDate: String? = "",
            @SerializedName("last_modified_date")
            val lastModifiedDate: String? = ""
        )

    }
}