package com.syspex.data.remote


import com.google.gson.annotations.SerializedName

data class NotifRequest(
    @SerializedName("code")
    val code: String? = "",
    @SerializedName("status")
    val status: String? = "",
    @SerializedName("message")
    val message: String? = "",
    @SerializedName("data")
    val `data`: Data? = Data()
) {
    data class Data(
        @SerializedName("page")
        val page: Int? = 0,
        @SerializedName("last_page")
        val lastPage: Int? = 0,
        @SerializedName("length")
        val length: String? = "",
        @SerializedName("notification")
        val notification: List<Notification?>? = listOf()
    ) {
        data class Notification(
            @SerializedName("notification_id")
            val notificationId: String? = "",
            @SerializedName("region_id")
            val regionId: String? = "",
            @SerializedName("user_id")
            val userId: String? = "",
            @SerializedName("notification_title")
            val notificationTitle: String? = "",
            @SerializedName("notification_body")
            val notificationBody: String? = "",
            @SerializedName("notification_source")
            val notificationSource: String? = "",
            @SerializedName("notification_source_id")
            val notificationSourceId: String? = "",
            @SerializedName("read_time")
            val readTime: Any? = Any(),
            @SerializedName("created_date")
            val createdDate: String? = "",
            @SerializedName("created_by")
            val createdBy: Any? = Any(),
            @SerializedName("last_modified_date")
            val lastModifiedDate: String? = "",
            @SerializedName("last_modified_by")
            val lastModifiedBy: Any? = Any()
        )
    }
}