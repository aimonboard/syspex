package com.syspex.data.remote


import com.google.gson.annotations.SerializedName

data class SearchEquipmentOnlineRequest(
    @SerializedName("code")
    val code: String? = "",
    @SerializedName("status")
    val status: String? = "",
    @SerializedName("message")
    val message: String? = "",
    @SerializedName("data")
    val `data`: Data = Data()
) {
    data class Data(
        @SerializedName("page")
        val page: Int? = 0,
        @SerializedName("last_page")
        val lastPage: Int? = 0,
        @SerializedName("length")
        val length: String? = "",
        @SerializedName("equipment")
        val equipment: List<Equipment> = listOf()
    ) {
        data class Equipment(
            @SerializedName("region_id")
            val regionId: String? = "",
            @SerializedName("equipment_id")
            val equipmentId: String? = "",
            @SerializedName("serial_no")
            val serialNo: String? = "",
            @SerializedName("brand")
            val brand: String? = "",
            @SerializedName("account_id")
            val accountId: String? = "",
            @SerializedName("product_id")
            val productId: String? = "",
            @SerializedName("warranty_start")
            val warrantyStart: String? = "",
            @SerializedName("warranty_end")
            val warrantyEnd: String? = "",
            @SerializedName("current_agreement_id")
            val currentAgreementId: String? = "",
            @SerializedName("equipment_status")
            val equipmentStatus: String? = "",
            @SerializedName("equipment_remarks")
            val equipmentRemarks: String? = "",
            @SerializedName("delivery_address_id")
            val deliveryAddressId: String? = "",
            @SerializedName("sales_order_no")
            val salesOrderNo: String? = "",
            @SerializedName("created_date")
            val createdDate: String? = "",
            @SerializedName("created_by")
            val createdBy: Any? = Any(),
            @SerializedName("last_modified_date")
            val lastModifiedDate: String? = "",
            @SerializedName("last_modified_by")
            val lastModifiedBy: Any? = Any(),
            @SerializedName("is_sap")
            val isSap: String? = "",
            @SerializedName("warranty_status")
            val warrantyStatus: String? = "",
            @SerializedName("current_agreement")
            val currentAgreement: Any? = Any(),
            @SerializedName("status_text")
            val statusText: StatusText? = StatusText(),
            @SerializedName("region")
            val region: Region? = Region(),
            @SerializedName("account")
            val account: Account? = Account(),
            @SerializedName("product")
            val product: Product? = Product()
        ) {
            data class StatusText(
                @SerializedName("enum_id")
                val enumId: String? = "",
                @SerializedName("enum_slug")
                val enumSlug: String? = "",
                @SerializedName("enum_name")
                val enumName: String? = "",
                @SerializedName("enum_table")
                val enumTable: String? = "",
                @SerializedName("enum_column")
                val enumColumn: String? = "",
                @SerializedName("created_at")
                val createdAt: String? = "",
                @SerializedName("updated_at")
                val updatedAt: String? = ""
            )

            data class Region(
                @SerializedName("region_id")
                val regionId: String? = "",
                @SerializedName("region_code")
                val regionCode: String? = "",
                @SerializedName("region_name")
                val regionName: String? = "",
                @SerializedName("created_date")
                val createdDate: String? = "",
                @SerializedName("created_by")
                val createdBy: Any? = Any(),
                @SerializedName("last_modified_date")
                val lastModifiedDate: String? = "",
                @SerializedName("last_modified_by")
                val lastModifiedBy: String? = "",
                @SerializedName("timezone")
                val timezone: String? = ""
            )

            data class Account(
                @SerializedName("account_id")
                val accountId: String? = "",
                @SerializedName("region_id")
                val regionId: String? = "",
                @SerializedName("account_code")
                val accountCode: String? = "",
                @SerializedName("account_name")
                val accountName: String? = "",
                @SerializedName("sales_id")
                val salesId: String? = "",
                @SerializedName("active")
                val active: String? = "",
                @SerializedName("created_date")
                val createdDate: String? = "",
                @SerializedName("created_by")
                val createdBy: Any? = Any(),
                @SerializedName("last_modified_date")
                val lastModifiedDate: String? = "",
                @SerializedName("last_modified_by")
                val lastModifiedBy: Any? = Any(),
                @SerializedName("internal_memo")
                val internalMemo: String? = "",
                @SerializedName("region")
                val region: Region? = Region()
            ) {
                data class Region(
                    @SerializedName("region_id")
                    val regionId: String? = "",
                    @SerializedName("region_code")
                    val regionCode: String? = "",
                    @SerializedName("region_name")
                    val regionName: String? = "",
                    @SerializedName("created_date")
                    val createdDate: String? = "",
                    @SerializedName("created_by")
                    val createdBy: Any? = Any(),
                    @SerializedName("last_modified_date")
                    val lastModifiedDate: String? = "",
                    @SerializedName("last_modified_by")
                    val lastModifiedBy: String? = "",
                    @SerializedName("timezone")
                    val timezone: String? = ""
                )
            }

            data class Product(
                @SerializedName("product_id")
                val productId: String? = "",
                @SerializedName("product_code")
                val productCode: String? = "",
                @SerializedName("product_name")
                val productName: String? = "",
                @SerializedName("created_date")
                val createdDate: String? = "",
                @SerializedName("created_by")
                val createdBy: Any? = Any(),
                @SerializedName("last_modified_date")
                val lastModifiedDate: String? = "",
                @SerializedName("last_modified_by")
                val lastModifiedBy: Any? = Any(),
                @SerializedName("region_id")
                val regionId: String? = "",
                @SerializedName("is_sap")
                val isSap: String? = ""
            )
        }
    }
}