package com.syspex.data.remote

data class NewsModel(
    val id: String,
    val title: String,
    val date: String,
    val time: String
)