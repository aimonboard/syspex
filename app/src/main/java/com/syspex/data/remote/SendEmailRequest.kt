package com.syspex.data.remote


import com.google.gson.annotations.SerializedName

data class SendEmailRequest(
    @SerializedName("code")
    val code: String? = "",
    @SerializedName("status")
    val status: String? = "",
    @SerializedName("message")
    val message: String? = "",
    @SerializedName("data")
    val `data`: List<Data?>? = listOf()
) {
    data class Data(
        @SerializedName("data")
        val `data`: Data? = Data()
    ) {
        data class Data(
            @SerializedName("report_id")
            val reportId: String? = ""
        )
    }
}