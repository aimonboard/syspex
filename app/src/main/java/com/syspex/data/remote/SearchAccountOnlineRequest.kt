package com.syspex.data.remote


import com.google.gson.annotations.SerializedName

data class SearchAccountOnlineRequest(
    @SerializedName("code")
    val code: String? = "",
    @SerializedName("status")
    val status: String? = "",
    @SerializedName("message")
    val message: String? = "",
    @SerializedName("data")
    val `data`: Data = Data()
) {
    data class Data(
        @SerializedName("keyword")
        val keyword: String? = "",
        @SerializedName("page")
        val page: Int? = 0,
        @SerializedName("last_page")
        val lastPage: Int? = 0,
        @SerializedName("length")
        val length: String? = "",
        @SerializedName("account")
        val account: List<Account> = listOf()
    ) {
        data class Account(
            @SerializedName("account_id")
            val accountId: String? = "",
            @SerializedName("region_id")
            val regionId: String? = "",
            @SerializedName("account_code")
            val accountCode: String? = "",
            @SerializedName("account_name")
            val accountName: String? = "",
            @SerializedName("sales_id")
            val salesId: String? = "",
            @SerializedName("active")
            val active: String? = "",
            @SerializedName("address")
            val address: List<Addres?>? = listOf()
        ) {
            data class Addres(
                @SerializedName("account_address_id")
                val accountAddressId: String? = "",
                @SerializedName("region_id")
                val regionId: String? = "",
                @SerializedName("account_id")
                val accountId: String? = "",
                @SerializedName("address_title")
                val addressTitle: String? = "",
                @SerializedName("address_line_1")
                val addressLine1: String? = "",
                @SerializedName("address_line_2")
                val addressLine2: String? = "",
                @SerializedName("address_line_3")
                val addressLine3: String? = "",
                @SerializedName("address_country")
                val addressCountry: String? = "",
                @SerializedName("address_city")
                val addressCity: String? = "",
                @SerializedName("address_zip_code")
                val addressZipCode: String? = "",
                @SerializedName("priority")
                val priority: String? = "",
                @SerializedName("full_address")
                val fullAddress: String? = ""
            )
        }
    }
}