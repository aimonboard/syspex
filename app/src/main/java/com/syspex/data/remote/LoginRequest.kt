package com.syspex.data.remote


import com.google.gson.annotations.SerializedName

data class LoginRequest(
    @SerializedName("code")
    val code: String? = "",
    @SerializedName("status")
    val status: String? = "",
    @SerializedName("message")
    val message: String? = "",
    @SerializedName("data")
    val `data`: Data? = Data()
) {
    data class Data(
        @SerializedName("token")
        val token: String? = "",
        @SerializedName("user")
        val user: User? = User()
    ) {
        data class User(
            @SerializedName("id")
            val id: String? = "",
            @SerializedName("region_id")
            val regionId: String? = "",
            @SerializedName("user_code")
            val userCode: String? = "",
            @SerializedName("user_first_name")
            val userFirstName: String? = "",
            @SerializedName("user_last_name")
            val userLastName: String? = "",
            @SerializedName("email")
            val email: String? = "",
            @SerializedName("email_verified_at")
            val emailVerifiedAt: String? = "",
            @SerializedName("phone")
            val phone: String? = "",
            @SerializedName("role_id")
            val roleId: String? = "",
            @SerializedName("created_date")
            val createdDate: String? = "",
            @SerializedName("created_by")
            val createdBy: String? = "",
            @SerializedName("last_modified_date")
            val lastModifiedDate: String? = "",
            @SerializedName("last_modified_by")
            val lastModifiedBy: String? = "",
            @SerializedName("user_fullname")
            val userFullname: String? = "",
            @SerializedName("user_signature")
            val userSignature: String? = "",
            @SerializedName("role")
            val role: Role? = Role(),
            @SerializedName("region")
            val region: Region? = Region(),
            @SerializedName("user_region")
            val userRegion: List<UserRegion?>? = listOf(),
        ) {
            data class Role(
                @SerializedName("role_id")
                val roleId: String? = "",
                @SerializedName("role_name")
                val roleName: String? = "",
                @SerializedName("created_at")
                val createdAt: String? = "",
                @SerializedName("updated_at")
                val updatedAt: String? = ""
            )

            data class Region(
                @SerializedName("region_id")
                val regionId: String? = "",
                @SerializedName("region_code")
                val regionCode: String? = "",
                @SerializedName("region_name")
                val regionName: String? = "",
                @SerializedName("created_date")
                val createdDate: String? = "",
                @SerializedName("created_by")
                val createdBy: String? = "",
                @SerializedName("last_modified_date")
                val lastModifiedDate: String? = "",
                @SerializedName("last_modified_by")
                val lastModifiedBy: String? = ""
            )

            data class UserRegion(
                @SerializedName("created_at")
                val createdAt: String? = "",
                @SerializedName("region")
                val region: Region? = Region(),
                @SerializedName("region_id")
                val regionId: String? = "",
                @SerializedName("updated_at")
                val updatedAt: String? = "",
                @SerializedName("user_id")
                val userId: String? = "",
                @SerializedName("user_region_id")
                val userRegionId: String? = ""
            ) {
                data class Region(
                    @SerializedName("created_by")
                    val createdBy: Any? = Any(),
                    @SerializedName("created_date")
                    val createdDate: String? = "",
                    @SerializedName("last_modified_by")
                    val lastModifiedBy: String? = "",
                    @SerializedName("last_modified_date")
                    val lastModifiedDate: String? = "",
                    @SerializedName("region_code")
                    val regionCode: String? = "",
                    @SerializedName("region_id")
                    val regionId: String? = "",
                    @SerializedName("region_name")
                    val regionName: String? = "",
                    @SerializedName("timezone")
                    val timezone: String? = ""
                )
            }
        }
    }
}