package com.syspex.data.model

data class SendEmailModel (
    val name: String,
    val email: String
)