package com.syspex.data.model

data class AccessoriesModel (
    val productNumber: String,
    val description: String,
    val quantity: String
)