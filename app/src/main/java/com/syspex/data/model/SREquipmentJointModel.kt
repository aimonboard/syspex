package com.syspex.data.model

import androidx.room.PrimaryKey
import androidx.room.TypeConverters
import com.syspex.data.local.database.ConverterImage

data class SREquipmentJointModel (
    // ============================================================================================= Report Equipment
    val serviceReportEquipmentId: String? = "",
    val serviceReportEquipmentCallId: String? = "",
    val reportId: String? = "",
    val equipmentId: String? = "",
    val problemSymptom: String? = "",
    val statusAfterServiceEnumId: String? = "",
    val statusAfterServiceEnumName: String? = "",
    val statusAfterServiceRemarks: String? = "",
    val installationStartDate: String? = "",
    val installationMatters: String? = "",
    val checklistTemplateId: String? = "",
    val createdDate: String? = "",
    val createdBy: String? = "",
    val lastModifiedDate: String? = "",
    val lastModifiedBy: String? = "",
    val equipmentName: String? = "",
    val regionId: String? = "",
    val serialNo: String? = "",
    val brand: String? = "",
    val warrantyStartDate: String? = "",
    val warrantyEndDate: String? = "",
    val equipmentStatus: String? = "",
    val equipmentRemarks: String? = "",
    val deliveryAddressId: String? = "",
    val salesOrderNo: String? = "",
    val warrantyStatus: String? = "",
    val accountId: String? = "",
    val accountName: String? = "",
    val agreementId: String? = "",
    val agreementNumber: String? = "",
    val agreementEnd: String? = "",
    val statusTextEnumId: String? = "",
    val statusTextEnumText: String? = "",
    val product_id: String? = "",
    val product_name: String,

    // ============================================================================================= Problem
    val serviceReportEquipmentImageId: String ? = "",
    val reportEquipmentId_problem: String? = "",
    val problemImage: List<ConverterImageModel>? = null,

    // ============================================================================================= Cause & solution
    val serviceReportEquipmentSolutionId: String? = "",
    val reportEquipmentId_solution: String? = "",
    val subject: String? = "",
    val problemCause: String? = "",
    val solution: String? = "",
    val causeImage: List<ConverterImageModel>? = null,
    val solutionImage: List<ConverterImageModel>? = null,

    // ============================================================================================= Sparepart
    val service_report_sparepart_id: String? = "",
    val reportEquipmentId_sparepart: String? = "",
    val spare_part_image_number: String? = "",
    val spare_part_number: String? = "",
    val spare_part_description: String? = "",
    val spare_part_memo: String? = "",
    val enum_id: String? = "",
    val enum_name: String? = "",
    val part_id: String? = "",
    val part_no: String? = "",
    val part_name: String? = "",
    val qty: String? = "",

    // ============================================================================================= Master Checklist Count
    val questionId: String? = "",
    val templateId: String? = "",

    // ============================================================================================= Report Checklist Count
    val service_report_equipment_checklist_id: String? = "",
    val report_equipment_id: String? = "",
    val question_id: String? = "",
    val checklist_status_id: String? = "",
    val checklist_status_Name: String? = ""
)