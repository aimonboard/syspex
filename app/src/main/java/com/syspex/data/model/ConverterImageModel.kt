package com.syspex.data.model

data class ConverterImageModel (
    val fileName: String,
    val fileUri: String
)