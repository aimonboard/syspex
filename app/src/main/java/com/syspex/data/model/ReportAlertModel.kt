package com.syspex.data.model

data class ReportAlertModel (
    val serviceCallId: String? = "",
    val serviceCallNumber: String? = "",
    val startDate: String? = "",
    val endDate: String? = "",
    val reportLocalId: String? = ""
)