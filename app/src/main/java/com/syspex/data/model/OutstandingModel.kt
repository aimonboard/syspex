package com.syspex.data.model

data class OutstandingModel (
    val things: String,
    val follow: String,
    val date: String
)