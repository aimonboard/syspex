package com.syspex.data.model

import com.syspex.data.remote.GetDataRequest

data class SCListModel (
    val serviceCallId: String,
    val serviceCallNumber: String,
    val callStatusEnumText: String,
    val serviceCallDescription: String,
    val account_name: String,
    val equipment: List<GetDataRequest.Equipment>,
    val startDate: String,
    val engineer: List<GetDataRequest.Engineer>
)