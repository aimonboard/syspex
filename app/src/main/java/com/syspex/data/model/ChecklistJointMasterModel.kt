package com.syspex.data.model

data class ChecklistJointMasterModel (
    val productGroupChecklistTemplateId: String,
    val regionId: String? = "",
    val productGroupId: String? = "",
    val template_description: String? = "",
    val checklistTypeTextEnumId: String? = "",
    val checklistTypeTextEnumName: String? = "",

    val questionId: String? = "",
    val orderNo: String? = "",
    val sectionTitle: String? = "",
    val question_description: String? = "",
    val defaultRemarks: String? = "",
    val helpText: String? = "",
    val helpImageUrl: String? = "",
    val checklistTemplateId: String? = ""
)