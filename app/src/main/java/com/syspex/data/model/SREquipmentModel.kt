package com.syspex.data.model

import com.syspex.data.local.database.pcs_problem.ProblemEntity
import com.syspex.data.local.database.pcs_solution.SolutionEntity
import com.syspex.data.local.database.product_checklist_question.ChecklistQuestionEntity
import com.syspex.data.local.database.product_sparepart.SparepartEntity
import com.syspex.data.local.database.service_report_equipment_checklist.ServiceReportEquipmentChecklistEntity
import com.syspex.data.local.database.service_report_equipment_checklist_question.ServiceReportEquipmentChecklistQuestionEntity
import com.syspex.data.local.database.service_report_sparepart.ServiceReportSparepartEntity

data class SREquipmentModel (
    val serviceReportEquipmentId: String,
    val reportId: String,
    val equipmentId: String,
    val problemSymptom: String,
    val statusAfterServiceEnumId: String,
    val statusAfterServiceEnumName: String,
    val statusAfterServiceRemarks: String,
    val installationStartDate: String,
    val installationMatters: String,
    val checklistTemplateId: String,
    val createdDate: String,
    val createdBy: String,
    val lastModifiedDate: String,
    val lastModifiedBy: String,
    val equipmentName: String,
    val regionId: String,
    val serialNo: String,
    val brand: String,
    val warrantyStartDate: String,
    val warrantyEndDate: String,
    val equipmentRemarks: String,
    val deliveryAddressId: String,
    val salesOrderNo: String,
    val warrantyStatus: String,
    val accountId: String,
    val accountName: String,
    val agreementId: String,
    val agreementNumber: String,
    val agreementEnd: String,
    val statusTextEnumId: String,
    val statusTextEnumText: String,
    val productId: String,
    val productName: String,

    val problemImage: MutableList<ProblemEntity>,
    val solution: MutableList<SolutionEntity>,
    val sparepart: MutableList<ServiceReportSparepartEntity>,
    val questionMaster: MutableList<ChecklistQuestionEntity>,
    val questionReport: MutableList<ServiceReportEquipmentChecklistEntity>,

    val isAttach: Boolean,
    val isUpload: Boolean
)