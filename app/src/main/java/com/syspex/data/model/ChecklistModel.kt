package com.syspex.data.model

import com.syspex.data.local.database.global_enum.EnumEntity
import com.syspex.data.local.database.product_checklist_question.ChecklistQuestionEntity
import com.syspex.data.local.database.service_report_equipment_checklist.ServiceReportEquipmentChecklistEntity
import com.syspex.data.local.database.service_report_equipment_checklist_question.ServiceReportEquipmentChecklistQuestionEntity

data class ChecklistModel (
    val productGroupChecklistTemplateId: String,
    val regionId: String,
    val productGroupId: String,
    val template_description: String,
    val checklistTypeTextEnumId: String,
    val checklistTypeTextEnumName: String,

    val question: MutableList<ServiceReportEquipmentChecklistEntity>,

    val enumData: MutableList<EnumEntity>
)