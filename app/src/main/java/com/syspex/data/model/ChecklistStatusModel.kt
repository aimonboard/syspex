package com.syspex.data.model

data class ChecklistStatusModel (
    val questionId: String = "",
    var enumId: String = "",
    var enumText: String = "",
    var remark: String = ""
)