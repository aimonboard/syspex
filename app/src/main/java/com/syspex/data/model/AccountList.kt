package com.syspex.data.model

import androidx.room.TypeConverters
import com.syspex.data.local.database.account.AccountConverterAddress
import com.syspex.data.local.database.service_call.ServiceCallConverterEquipment
import com.syspex.data.remote.GetDataRequest

data class AccountList (
    // Account Entity
    val accountId: String? = "",
    val account_code: String? = "",
    val account_name: String? = "",
    @TypeConverters(AccountConverterAddress::class)
    val address: List<GetDataRequest.Data.ServiceCase.Account.Addres?>? = null,

    // Call Entity
    val startDate: String? = "",
    val callStatusEnumText: String? = "",
    val callTypeEnumText: String? = "",
    @TypeConverters(ServiceCallConverterEquipment::class)
    val equipment: List<GetDataRequest.Data.ServiceCase.ServiceCall.ServiceCallEquipment?>? = null
)