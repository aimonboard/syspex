package com.syspex.data.model

import com.syspex.data.remote.GetDataRequest

data class AddEquipmentJointModel (
    val reportId: String? = "",
    val serviceReportNumber: String? = "",
    val equipmentId: String? = ""
)