package com.syspex.data.model

import androidx.room.PrimaryKey
import androidx.room.TypeConverters
import com.syspex.data.local.database.ConverterImage

data class EquipmentListModel (
    // ============================================================================================= Report Equipment
    val equipmentId: String? = "",
    val serialNo: String? = "",
    val statusTextEnumId: String? = "",
    val statusTextEnumText: String? = "",
    val product_name: String? = "",
    val accountName: String? = "",
    val warrantyEndDate: String? = "",
    val agreementEnd: String? = "",
    val reportLocalId: String? = "",
    val serviceReportNumber: String? = "",
    val createdDate: String? = "",
    val reportStatusEnumId: String? = "",
    val reportStatusEnumText: String? = "",
    val servicedByUserName: String? = "",
    val callTypeEnumId: String? = "",
    val callTypeEnumText: String? = ""
)