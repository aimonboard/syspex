package com.syspex.data.model

data class ApiLogModel (
    val requestUrl: String,
    val requestHeader: String,
    val requestBody: String,
    val response: String
)