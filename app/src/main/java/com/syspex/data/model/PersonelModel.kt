package com.syspex.data.model

data class PersonelModel (
    val personelName: String,
    val designation: String
)