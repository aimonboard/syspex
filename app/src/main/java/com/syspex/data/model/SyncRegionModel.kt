package com.syspex.data.model

data class SyncRegionModel (
    val regionId: String = "",
    val regionCode: String =  "",
    val regionName: String = ""
)