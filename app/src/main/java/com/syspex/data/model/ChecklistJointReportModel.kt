package com.syspex.data.model

data class ChecklistJointReportModel (
    val serviceReportEquipmentChecklistId: String? = "",
    val reportEquipmentId: String? = "",
    val questionId: String? = "",
    val template_description: String? = "",
    val remarks: String? = "",
    val statusTemplateEnumId: String? = "",
    val statusTemplateEnumName: String? = "",

    val id: String? = "",
    val orderNo: String? = "",
    val sectionTitle: String? = "",
    val question_description: String? = "",
    val defaultRemarks: String? = "",
    val helpText: String? = "",
    val helpImageUrl: String? = "",
    val statusQuestionEnumId: String? = "",
    val statusQuestionEnumName: String? = "",
    val checklistTemplateId: String? = "",
    val checklistImage: List<ConverterImageModel>? = null
)