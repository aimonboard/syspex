package com.syspex.data.model

data class SpinnerModel (
    val spinnerId: String,
    val spinnerName: String
)