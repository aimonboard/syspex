package com.syspex.data.model

import com.syspex.data.remote.GetDataRequest

data class SRListModel (
    val reportLocalId: String,
    val serviceReportNumber: String,
    val reportStatusEnumText: String,
//    val serviceReportDescription: String,
    val account_name: String,
    val equipment: List<GetDataRequest.Data.ServiceCase.ServiceCall.ServiceReport.ServiceReportEquipment>,
    val reportDate: String,
    val servicedByUserName: String
)