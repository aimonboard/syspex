package com.syspex.data.local.database.account_address

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData

class AccountAddressViewModel constructor(application: Application) : AndroidViewModel(application) {
    private val repository : AccountAddressRepository = AccountAddressRepository(application)

    fun getAll(): LiveData<List<AccountAddressEntity>> {
        return repository.getAll()
    }

    fun getByAccountId(accountId: String): LiveData<List<AccountAddressEntity>> {
        return repository.getByAccountId(accountId)
    }

    fun insert(data: AccountAddressEntity){
        repository.insert(data)
    }

    fun updateFlag(entity: AccountAddressEntity){
        repository.updateFlag(entity)
    }

}