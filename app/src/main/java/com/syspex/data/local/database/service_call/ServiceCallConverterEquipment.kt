package com.syspex.data.local.database.service_call

import androidx.room.TypeConverter
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.syspex.data.remote.GetDataRequest
import java.lang.reflect.Type
import java.util.*


class ServiceCallConverterEquipment {
    var gson = Gson()

    @TypeConverter
    fun stringToSomeObjectList(data: String?): List<GetDataRequest.Data.ServiceCase.ServiceCall.ServiceCallEquipment?>? {
        if (data == null) {
            return Collections.emptyList()
        }
        val listType: Type =
            object : TypeToken<List<GetDataRequest.Data.ServiceCase.ServiceCall.ServiceCallEquipment?>?>() {}.type
        return gson.fromJson<List<GetDataRequest.Data.ServiceCase.ServiceCall.ServiceCallEquipment?>>(data, listType)
    }

    @TypeConverter
    fun someObjectListToString(someObjects: List<GetDataRequest.Data.ServiceCase.ServiceCall.ServiceCallEquipment?>?): String? {
        return gson.toJson(someObjects)
    }


}