package com.syspex.data.local.database.service_report

import android.app.Application
import androidx.lifecycle.LiveData
import com.syspex.data.local.database.AppDatabase
import com.syspex.data.local.database.GlobalDao
import com.syspex.data.model.AddEquipmentJointModel
import com.syspex.data.model.ReportTestModel
import com.syspex.data.model.SRListModel
import com.syspex.data.remote.GetDataRequest

class ServiceReportRepository (application: Application) {

    lateinit var dao : GlobalDao

    init {
        val database = AppDatabase.getInstance(application)
        if (database != null) {
            dao = database.dao()
        }
    }

    fun getAll(searchText: String, filterArray: ArrayList<String>) : LiveData<List<ServiceReportEntity>> {
        return dao.reportAll(searchText, filterArray)
    }

//    fun getTest(filterArray: ArrayList<String>) : LiveData<List<ReportTestModel>> {
//        return dao.reportTest(filterArray)
//    }

    fun search(number: String, accountName: String) : LiveData<List<ServiceReportEntity>> {
        return dao.reportSearch(number, accountName)
    }

    fun reportJoinReportEquipment(callId: String) : LiveData<List<AddEquipmentJointModel>> {
        return dao.reportJoinReportEquipment(callId)
    }

    fun getByReportId(reportId: String) : LiveData<List<ServiceReportEntity>> {
        return dao.reportByReportId(reportId)
    }

    fun getByTempNumber(tempNumber: String) : LiveData<List<ServiceReportEntity>> {
        return dao.reportByTempNumber(tempNumber)
    }

    fun getByCallId(callId: String) : LiveData<List<ServiceReportEntity>> {
        return dao.reportByCallId(callId)
    }

    fun getByCaseId(caseId: String) : LiveData<List<ServiceReportEntity>> {
        return dao.reportByCaseId(caseId)
    }

    fun getByAccountId(accountId: String) : LiveData<List<ServiceReportEntity>> {
        return dao.reportByAccountId(accountId)
    }

    fun reportEquipmentRelated(equipmentId: String) : LiveData<List<ServiceReportEntity>> {
        return dao.reportEquipmentRelated(equipmentId)
    }

    fun getList() : LiveData<List<SRListModel>> {
        return dao.reportList()
    }

    fun insert(entity: ServiceReportEntity) {
        val thread = object : Thread() {
            override fun run() {
                dao.reportInsert(entity)
            }
        }
        thread.start()
    }

    fun updateFeedback(reportId: String, rating: String, feedback: String, fileName: String, fileImage: String, cpNumber: String, cpName: String, isUplaod: Boolean) {
        val thread = object : Thread() {
            override fun run() {
                dao.reportFeedback(reportId, rating, feedback, fileName, fileImage, cpNumber, cpName, isUplaod)
            }
        }
        thread.start()
    }

    fun updateContact(reportId: String, picId: String, picName: String, picPhone: String, isUplaod: Boolean) {
        val thread = object : Thread() {
            override fun run() {
                dao.reportUpdateContact(reportId, picId, picName, picPhone, isUplaod)
            }
        }
        thread.start()
    }

    fun updateMaster(reportId: String, memo: String, isChargeable: String, picId: String, picName: String, picPhone: String, isUplaod: Boolean) {
        val thread = object : Thread() {
            override fun run() {
                dao.reportUpdateMaster(reportId, memo, isChargeable, picId, picName, picPhone, isUplaod)
            }
        }
        thread.start()
    }

    fun updateFlag(reportId: String, isUpload: Boolean) {
        val thread = object : Thread() {
            override fun run() {
                dao.reportUpdateFlag(reportId, isUpload)
            }
        }
        thread.start()
    }

    fun deleteById(reportId: String) {
        val thread = object : Thread() {
            override fun run() {
                dao.reportDeleteById(reportId)
            }
        }
        thread.start()
    }

    fun delete(entity: ServiceReportEntity) {
        val thread = object : Thread() {
            override fun run() {
                dao.reportDelete(entity)
            }
        }
        thread.start()
    }

    fun deleteAll() {
        val thread = object : Thread() {
            override fun run() {
                dao.reportDeleteAll()
            }
        }
        thread.start()
    }

}