package com.syspex.data.local

import android.content.Context

class SharedPreferenceData {
    companion object {

        private const val PREF_FILE = "syspex_data"

        private fun generateKey(keyCode: Int): String? {
            return when (keyCode) {
                // Login: String
                1 -> "login_status"
                2 -> "login_token"
                3 -> "login_save"

                // Sync Data: Int
                4 -> "current_page"
                5 -> "last_page"
                6 -> "last_sync"

                // Recycler orientation: Boolean
                7 -> "recycler_orientation"

                // Profile: String
                8 -> "user_id"
                9 -> "user_code"
                10 -> "user_name"
                11 -> "user_email"
                12 -> "user_phone"
                13 -> "user_role"
                14 -> "user_region"

                // Profile: String
                1111 -> "user_signature"

                // Report Number: Int
                15 -> "report_number"

                // FCM - case id : String
                16 -> "case_id_fcm"

                // Alarm Manager - report : Boolean
                17 -> "report_alert"

                // user multi region (JSON array)
                18 -> "user_region"

                // List Filter
                9991 -> "filter_case"
                9992 -> "filter_call"
                9993 -> "filter_report"
                9994 -> "filter_agreement"
                9995 -> "filter_equipment"

                else -> null
            }
        }

        fun setArrayString(context: Context?, key: Int, value: Set<String>) {
            val settings = context?.getSharedPreferences(PREF_FILE, Context.MODE_PRIVATE)
            val editor = settings?.edit()
            editor?.putStringSet(generateKey(key), value)
            editor?.apply()
        }
        fun getArrayString(context: Context?, key: Int, defValue: Set<String>): Set<String> {
            val settings = context?.getSharedPreferences(PREF_FILE, Context.MODE_PRIVATE)
            return settings?.getStringSet(generateKey(key), defValue) ?: defValue
        }

        fun setString(context: Context?, key: Int, value: String) {
            val settings = context?.getSharedPreferences(PREF_FILE, Context.MODE_PRIVATE)
            val editor = settings?.edit()
            editor?.putString(generateKey(key), value)
            editor?.apply()
        }
        fun getString(context: Context?, key: Int, defValue: String): String {
            val settings = context?.getSharedPreferences(PREF_FILE, Context.MODE_PRIVATE)
            return settings?.getString(generateKey(key), defValue) ?: defValue
        }


        fun setInt(context: Context?, key: Int, value: Int) {
            val settings = context?.getSharedPreferences(PREF_FILE, Context.MODE_PRIVATE)
            val editor = settings?.edit()
            editor?.putInt(generateKey(key), value)
            editor?.apply()
        }
        fun getInt(context: Context?, key: Int, defValue: Int): Int {
            val settings = context?.getSharedPreferences(PREF_FILE, Context.MODE_PRIVATE)
            return settings?.getInt(generateKey(key), defValue) ?: defValue
        }

        fun setBoolean(context: Context?, key: Int, value: Boolean) {
            val settings = context?.getSharedPreferences(PREF_FILE, Context.MODE_PRIVATE)
            val editor = settings?.edit()
            editor?.putBoolean(generateKey(key), value)
            editor?.apply()
        }
        fun getBoolean(context: Context?, key: Int, defValue: Boolean): Boolean {
            val settings = context?.getSharedPreferences(PREF_FILE, Context.MODE_PRIVATE)
            return settings?.getBoolean(generateKey(key), defValue) ?: defValue
        }

    }
}