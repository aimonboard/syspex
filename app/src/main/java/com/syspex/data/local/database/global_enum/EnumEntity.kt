package com.syspex.data.local.database.global_enum

import androidx.room.Entity
import androidx.room.PrimaryKey
import androidx.room.TypeConverters
import com.syspex.data.local.database.ConverterEquipment
import com.syspex.data.remote.GetDataRequest

@Entity(tableName = "table_enum")
data class EnumEntity (
    @PrimaryKey(autoGenerate = false)
    val enum_id: String,
    val enum_slug: String,
    val enum_name: String,
    val enum_table: String,
    val enum_column: String,
    val created_at: String,
    val updated_at: String
)