package com.syspex.data.local.database.service_report_equipment_checklist

import android.app.Application
import androidx.lifecycle.LiveData
import com.syspex.data.local.database.AppDatabase
import com.syspex.data.local.database.GlobalDao
import com.syspex.data.local.database.service_call.ServiceCallEntity
import com.syspex.data.model.ConverterImageModel
import com.syspex.data.model.SCListModel
import com.syspex.data.model.SRListModel
import com.syspex.data.remote.GetDataRequest

class ServiceReportEquipmentChecklistRepository constructor(application: Application) {

    lateinit var dao : GlobalDao

    init {
        val database = AppDatabase.getInstance(application)
        if (database != null) {
            dao = database.dao()
        }
    }

    fun getByReportEquipmentId(reportEquipmentId: String): LiveData<List<ServiceReportEquipmentChecklistEntity>> {
        return dao.reportChecklistByReportEquipment(reportEquipmentId)
    }

    fun insert(entity: ServiceReportEquipmentChecklistEntity) {
        val thread = object : Thread() {
            override fun run() {
                dao.reportChecklistInsert(entity)
            }
        }
        thread.start()
    }

    fun insertAll(entity: ArrayList<ServiceReportEquipmentChecklistEntity>) {
        val thread = object : Thread() {
            override fun run() {
                dao.reportChecklistInsertAll(entity)
            }
        }
        thread.start()
    }

    fun updateStatus(questionId: String, enumId: String, enumName: String, remark: String) {
        val thread = object : Thread() {
            override fun run() {
                dao.reportChecklistUpdateStatus(questionId, enumId, enumName, remark)
            }
        }
        thread.start()
    }
//
    fun updateImage(questionId: String, checklistImage: ArrayList<ConverterImageModel>) {
        val thread = object : Thread() {
            override fun run() {
                dao.reportChecklistUpdateImage(questionId, checklistImage)
            }
        }
        thread.start()
    }

    fun deleteById(reportEquipmentId: String) {
        val thread = object : Thread() {
            override fun run() {
                dao.reportChecklistDeleteById(reportEquipmentId)
            }
        }
        thread.start()
    }

}