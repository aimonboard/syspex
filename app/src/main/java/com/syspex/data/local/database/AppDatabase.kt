package com.syspex.data.local.database

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.syspex.data.local.database.account.AccountConverterAddress
import com.syspex.data.local.database.account.AccountConverterContact
import com.syspex.data.local.database.account.AccountEntity
import com.syspex.data.local.database.account_address.AccountAddressEntity
import com.syspex.data.local.database.account_attachment.AccountAttachmentEntity
import com.syspex.data.local.database.account_contact.AccountContactEntity
import com.syspex.data.local.database.account_equipment.AccountEquipmentEntity
import com.syspex.data.local.database.activity.ActivityEntity
import com.syspex.data.local.database.activity_type.ActivityTypeEntity
import com.syspex.data.local.database.agreement.AgreementEntity
import com.syspex.data.local.database.agreement_equipment.AgreementEquipmentEntity
import com.syspex.data.local.database.chargeable_type.ChargeableEntity
import com.syspex.data.local.database.email.EmailEntity
import com.syspex.data.local.database.file.FileEntity
import com.syspex.data.local.database.global_color.ColorEntity
import com.syspex.data.local.database.global_enum.EnumEntity
import com.syspex.data.local.database.pcs_problem.ProblemEntity
import com.syspex.data.local.database.pcs_solution.SolutionEntity
import com.syspex.data.local.database.product_group.ProductEntity
import com.syspex.data.local.database.product_checklist_question.ChecklistQuestionEntity
import com.syspex.data.local.database.product_checklist_template.ChecklistTemplateEntity
import com.syspex.data.local.database.product_document.ProductDocumentEntity
import com.syspex.data.local.database.product_group_item.ProductGroupItemEntity
import com.syspex.data.local.database.product_sparepart.SparepartEntity
import com.syspex.data.local.database.service_call.*
import com.syspex.data.local.database.service_call_completion.ServiceCallCompletionEntity
import com.syspex.data.local.database.service_case.ServiceCaseEntity
import com.syspex.data.local.database.service_call_equipment.ServiceCallEquipmentEntity
import com.syspex.data.local.database.service_call_handover.ServiceCallHandoverEntity
import com.syspex.data.local.database.service_call_training.ServiceCallTrainingEntity
import com.syspex.data.local.database.service_case.ServiceCaseConverterEquipment
import com.syspex.data.local.database.service_case_equipment.ServiceCaseEquipmentEntity
import com.syspex.data.local.database.service_report.ServiceReportConverterEquipment
import com.syspex.data.local.database.service_report.ServiceReportConverterTimeTracking
import com.syspex.data.local.database.service_report.ServiceReportEntity
import com.syspex.data.local.database.service_report_equipment.ServiceReportEquipmentEntity
import com.syspex.data.local.database.service_report_equipment_checklist.ServiceReportEquipmentChecklistEntity
import com.syspex.data.local.database.service_report_equipment_checklist_question.ServiceReportEquipmentChecklistQuestionEntity
import com.syspex.data.local.database.service_report_equipment_checklist_template.ServiceReportEquipmentChecklistTemplateEntity
import com.syspex.data.local.database.service_report_sparepart.ServiceReportSparepartEntity
import com.syspex.data.local.database.service_report_time_tracking.ServiceReportTimeTrackingEntity

@Database(
    entities = [
        ServiceCaseEntity::class,
        ServiceCaseEquipmentEntity::class,
        AccountEntity::class,
        AccountContactEntity::class,
        AccountAddressEntity::class,
        AccountAttachmentEntity::class,
        AccountEquipmentEntity::class,
        AgreementEntity::class,
        AgreementEquipmentEntity::class,
        ServiceCallEntity::class,
        ServiceCallTrainingEntity::class,
        ServiceCallHandoverEntity::class,
        ServiceCallCompletionEntity::class,
        ServiceCallEquipmentEntity::class,
        ServiceReportEntity::class,
        ServiceReportEquipmentEntity::class,
        ServiceReportEquipmentChecklistEntity::class,
        ServiceReportEquipmentChecklistTemplateEntity::class,
        ServiceReportEquipmentChecklistQuestionEntity::class,
        ServiceReportSparepartEntity::class,
        ServiceReportTimeTrackingEntity::class,
        ProductGroupItemEntity::class,
        ProductDocumentEntity::class,
        ProductEntity::class,
        ProblemEntity::class,
        SolutionEntity::class,
        SparepartEntity::class,
        ChecklistTemplateEntity::class,
        ChecklistQuestionEntity::class,
        EmailEntity::class,
        FileEntity::class,
        EnumEntity::class,
        ChargeableEntity::class,
        ColorEntity::class,
        ActivityEntity::class,
        ActivityTypeEntity::class
    ],
    version = 75,
    exportSchema = false
)

@TypeConverters(
    ConverterEquipment::class,
    ConverterAttachment::class,
    AccountConverterContact::class,
    AccountConverterAddress::class,
    ServiceCallConverterEquipment::class,
    ServiceCallConverterEngineer::class,
    ServiceCaseConverterEquipment::class,
    ServiceReportConverterTimeTracking::class,
    ServiceReportConverterEquipment::class,
    ConverterImage::class
)

abstract class AppDatabase : RoomDatabase() {

    abstract fun dao(): GlobalDao

    companion object {
        var instance: AppDatabase? = null
        fun getInstance(context: Context): AppDatabase? {
            if (instance == null) {
                synchronized(AppDatabase::class) {
                    instance = Room.databaseBuilder(context.applicationContext,
                        AppDatabase::class.java, "syspex")
                        .fallbackToDestructiveMigration()
                        .build()
                }
            }
            return instance
        }
    }
}