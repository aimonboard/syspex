package com.syspex.data.local.database.account_attachment

import android.app.Application
import androidx.lifecycle.LiveData
import com.syspex.data.local.database.AppDatabase
import com.syspex.data.local.database.GlobalDao

class AccountAttachmentRepository constructor(application: Application) {

    lateinit var dao : GlobalDao

    init {
        val database = AppDatabase.getInstance(application)
        if (database != null) {
            dao = database.dao()
        }
    }

    fun getAll() : LiveData<List<AccountAttachmentEntity>> {
        return dao.accountAttachmentAll()
    }

    fun accountAttachment(accountId: String) : LiveData<List<AccountAttachmentEntity>> {
        return dao.accountAttachment(accountId)
    }

    fun accountAttachmentSection (accountId: String, sourceId: String, sourceTable: String) : LiveData<List<AccountAttachmentEntity>> {
        return dao.accountAttachmentSection(accountId, sourceId, sourceTable)
    }

    fun insert(entity: AccountAttachmentEntity) {
        val thread = object : Thread() {
            override fun run() {
                dao.accountAttachmentInsert(entity)
            }
        }
        thread.start()
    }

    fun updateFlag(entity: AccountAttachmentEntity) {
        val thread = object : Thread() {
            override fun run() {
                dao.accountAttachmentUpdate(entity)
            }
        }
        thread.start()
    }

}