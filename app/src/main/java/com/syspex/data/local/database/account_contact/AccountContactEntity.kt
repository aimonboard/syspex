package com.syspex.data.local.database.account_contact

import androidx.room.Entity
import androidx.room.PrimaryKey
import androidx.room.TypeConverters
import com.syspex.data.local.database.ConverterEquipment
import com.syspex.data.remote.GetDataRequest

@Entity(tableName = "table_account_contact")
data class AccountContactEntity (
    @PrimaryKey(autoGenerate = false)
    val account_contact_id: String,
    val region_id: String,
    val account_id: String,
    val account_name: String,
    val contact_person_name: String,
    val contact_person_phone: String,
    val contact_person_email: String,
    val contact_person_address: String,
    val contact_person_position: String,
    val contact_type: String,
    val priority: String,
    val created_date: String,
    val created_by: String,
    val last_modified_date: String,
    val last_modified_by: String

)