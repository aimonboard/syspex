package com.syspex.data.local.database.product_group_item

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "table_product_group_item")
data class ProductGroupItemEntity (
    @PrimaryKey(autoGenerate = false)
    val product_group_item_id: String,
    val product_group_id: String,
    val product_id: String
)