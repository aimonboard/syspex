package com.syspex.data.local.database.product_checklist_question

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import com.syspex.data.local.database.product_checklist_template.ChecklistTemplateEntity
import com.syspex.data.local.database.service_report.ServiceReportEntity
import com.syspex.data.model.ChecklistJointMasterModel
import com.syspex.data.model.SCListModel
import com.syspex.data.model.SRListModel
import com.syspex.data.remote.GetDataRequest

class ChecklistQuestionViewModel constructor(application: Application) : AndroidViewModel(application) {
    private val repository : ChecklistQuestionRepository = ChecklistQuestionRepository(application)

    fun getByTemplate(templateId: String): LiveData<List<ChecklistQuestionEntity>> {
        return repository.getByTemplate(templateId)
    }

    fun insert(data: ChecklistQuestionEntity){
        repository.insert(data)
    }

    fun deleteAll(){
        repository.deleteAll()
    }

}