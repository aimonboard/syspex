package com.syspex.data.local.database.service_call

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import com.syspex.data.local.database.service_report.ServiceReportEntity
import com.syspex.data.model.ReportAlertModel
import com.syspex.data.model.SCListModel
import com.syspex.data.model.SRListModel
import com.syspex.data.remote.GetDataRequest

class ServiceCallViewModel constructor(application: Application) : AndroidViewModel(application) {
    private val repository : ServiceCallRepository = ServiceCallRepository(application)

    fun alert(startDateTime: String, endDateTime: String) : LiveData<List<ReportAlertModel>> {
        return repository.alert(startDateTime, endDateTime)
    }

    fun getAll(searchText: String, filterArray: ArrayList<String>): LiveData<List<ServiceCallEntity>> {
        return repository.getAll(searchText, filterArray)
    }

    fun search(textSearch: String): LiveData<List<ServiceCallEntity>> {
        return repository.search(number = textSearch, accountName = textSearch, subject = textSearch)
    }

    fun getByAccountId(accountId: String): LiveData<List<ServiceCallEntity>> {
        return repository.getByAccountId(accountId)
    }

    fun getByCallId(callId: String): LiveData<List<ServiceCallEntity>> {
        return repository.getByCallId(callId)
    }

    fun getByCallIdBackground(callId: String): List<ServiceCallEntity> {
        return repository.getByCallIdBackground(callId)
    }

    fun getByCaseId(caseId: String): LiveData<List<ServiceCallEntity>> {
        return repository.getByCaseId(caseId)
    }

    fun callEquipmentRelated (equipmentId: String): LiveData<List<ServiceCallEntity>> {
        return repository.callEquipmentRelated(equipmentId)
    }

    fun getList(): LiveData<List<SCListModel>> {
        return repository.getList()
    }

    fun getReportByCallId(callId: String): LiveData<List<SRListModel>> {
        return repository.getReportByCallId(callId)
    }

    fun updateEquipment(id: String, data: List<GetDataRequest.Equipment>) {
        repository.updateEquipment(id, data)
    }


    fun insert(data: ServiceCallEntity){
        repository.insert(data)
    }

    fun delete(data: ServiceCallEntity){
        repository.delete(data)
    }

    fun deleteAll(){
        repository.deleteAll()
    }

    fun updateFlag(callId: String, isUpload: Boolean) {
        repository.updateFlag(callId, isUpload)
    }

}