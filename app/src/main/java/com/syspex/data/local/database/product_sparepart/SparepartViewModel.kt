package com.syspex.data.local.database.product_sparepart

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import com.syspex.data.local.database.service_report.ServiceReportEntity
import com.syspex.data.model.SCListModel
import com.syspex.data.model.SRListModel
import com.syspex.data.remote.GetDataRequest

class SparepartViewModel constructor(application: Application) : AndroidViewModel(application) {
    private val repository : SparepartRepository = SparepartRepository(application)

    fun getByProductGroupId(productGroupId: String): LiveData<List<SparepartEntity>> {
        return repository.getByProductGroupId(productGroupId)
    }

    fun insert(data: SparepartEntity){
        repository.insert(data)
    }

}