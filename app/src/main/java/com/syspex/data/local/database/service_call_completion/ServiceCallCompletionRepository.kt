package com.syspex.data.local.database.service_call_completion

import android.app.Application
import androidx.lifecycle.LiveData
import com.syspex.data.local.database.AppDatabase
import com.syspex.data.local.database.GlobalDao
import com.syspex.data.local.database.service_report_equipment.ServiceReportEquipmentEntity
import com.syspex.data.model.EquipmentListModel
import com.syspex.data.model.SCListModel
import com.syspex.data.model.SRListModel
import com.syspex.data.remote.GetDataRequest

class ServiceCallCompletionRepository constructor(application: Application) {

    lateinit var dao : GlobalDao

    init {
        val database = AppDatabase.getInstance(application)
        if (database != null) {
            dao = database.dao()
        }
    }

    fun getAll() : LiveData<List<ServiceCallCompletionEntity>> {
        return dao.callCompletionAll()
    }

    fun getByCallId(callId: String) : LiveData<List<ServiceCallCompletionEntity>> {
        return dao.callCompletionByCallId(callId)
    }

    fun insert(entity: ServiceCallCompletionEntity) {
        val thread = object : Thread() {
            override fun run() {
                dao.callCompletionInsert(entity)
            }
        }
        thread.start()
    }

    fun delete(entity: ServiceCallCompletionEntity) {
        val thread = object : Thread() {
            override fun run() {
                dao.callCompletionDelete(entity)
            }
        }
        thread.start()
    }

}