package com.syspex.data.local.database.activity

import android.app.Application
import androidx.lifecycle.LiveData
import com.syspex.data.local.database.AppDatabase
import com.syspex.data.local.database.GlobalDao
import com.syspex.data.remote.GetDataRequest

class ActivityRepository constructor(application: Application) {

    lateinit var dao : GlobalDao
    lateinit var data : LiveData<List<ActivityEntity>>

    init {
        val database = AppDatabase.getInstance(application)
        if (database != null) {
            dao = database.dao()
            data = dao.activityAll()
        }
    }

    fun getAll() : LiveData<List<ActivityEntity>> {
        return data
    }

    fun getById(activityId: String) : LiveData<List<ActivityEntity>> {
        return dao.activityById(activityId)
    }

    fun getByCase(caseId: String) : LiveData<List<ActivityEntity>> {
        return dao.activityByCaseId(caseId)
    }

    fun getByAccount(accountId: String) : LiveData<List<ActivityEntity>> {
        return dao.activityByAccountId(accountId)
    }

    fun getByCall(callId: String) : LiveData<List<ActivityEntity>> {
        return dao.activityByCallId(callId)
    }

    fun insert(entity: ActivityEntity) {
        val thread = object : Thread() {
            override fun run() {
                dao.activityInsert(entity)
            }
        }
        thread.start()
    }

}