package com.syspex.data.local.database.service_report_equipment_checklist_question

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import com.syspex.data.local.database.product_checklist_template.ChecklistTemplateEntity
import com.syspex.data.local.database.service_report.ServiceReportEntity
import com.syspex.data.model.ConverterImageModel
import com.syspex.data.model.SCListModel
import com.syspex.data.model.SRListModel
import com.syspex.data.remote.GetDataRequest

class ServiceReportEquipmentChecklistQuestionViewModel constructor(application: Application) : AndroidViewModel(application) {
    private val repository : ServiceReportEquipmentChecklistQuestionRepository = ServiceReportEquipmentChecklistQuestionRepository(application)

    fun getAll(): LiveData<List<ServiceReportEquipmentChecklistQuestionEntity>> {
        return repository.getAll()
    }

    fun insert(data: ServiceReportEquipmentChecklistQuestionEntity){
        repository.insert(data)
    }

    fun updateStatus(questionId: String, enumId: String, enumName: String, isUpload:Boolean){
        repository.updateStatus(questionId, enumId, enumName, isUpload)
    }

    fun updateImage(questionId: String, checklistImage: ArrayList<ConverterImageModel>, isUpload: Boolean){
        repository.updateImage(questionId, checklistImage, isUpload)
    }

    fun deleteAll(){
        repository.deleteAll()
    }

}