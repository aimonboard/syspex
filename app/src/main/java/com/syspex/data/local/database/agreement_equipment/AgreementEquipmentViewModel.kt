package com.syspex.data.local.database.agreement_equipment

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import com.syspex.data.model.EquipmentListModel

class AgreementEquipmentViewModel constructor(application: Application) : AndroidViewModel(application) {
    private val repository : AgreementEquipmentRepository = AgreementEquipmentRepository(application)
    private val data : LiveData<List<AgreementEquipmentEntity>>

    init {
        data = repository.getAll()
    }

    fun getAll(): LiveData<List<AgreementEquipmentEntity>> {
        return data
    }

    fun getByAgreementId(agreementId: String): LiveData<List<EquipmentListModel>> {
        return repository.getByAgreementId(agreementId)
    }

    fun insert(data: AgreementEquipmentEntity){
        repository.insert(data)
    }

    fun delete(data: AgreementEquipmentEntity){
        repository.delete(data)
    }

    fun deleteAll(){
        repository.deleteAll()
    }

}