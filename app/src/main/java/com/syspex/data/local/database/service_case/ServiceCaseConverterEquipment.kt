package com.syspex.data.local.database.service_case

import androidx.room.TypeConverter
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.syspex.data.remote.GetDataRequest
import java.lang.reflect.Type
import java.util.*


class ServiceCaseConverterEquipment {
    var gson = Gson()

    @TypeConverter
    fun stringToSomeObjectList(data: String?): List<GetDataRequest.Data.ServiceCase.ServiceCaseEquipment?>? {
        if (data == null) {
            return Collections.emptyList()
        }
        val listType: Type =
            object : TypeToken<List<GetDataRequest.Data.ServiceCase.ServiceCaseEquipment?>?>() {}.type
        return gson.fromJson<List<GetDataRequest.Data.ServiceCase.ServiceCaseEquipment?>>(data, listType)
    }

    @TypeConverter
    fun someObjectListToString(someObjects: List<GetDataRequest.Data.ServiceCase.ServiceCaseEquipment?>?): String? {
        return gson.toJson(someObjects)
    }


}