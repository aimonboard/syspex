package com.syspex.data.local.database.file

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "table_file")
data class FileEntity (
    @PrimaryKey(autoGenerate = true)
    val id: Int,

    val uri: String,
    val name: String,
    val section: String
)