package com.syspex.data.local.database.pcs_solution

import android.net.Uri
import androidx.room.Entity
import androidx.room.PrimaryKey
import androidx.room.TypeConverters
import com.google.gson.annotations.SerializedName
import com.syspex.data.local.database.ConverterEquipment
import com.syspex.data.local.database.ConverterImage
import com.syspex.data.model.ConverterImageModel
import com.syspex.data.remote.GetDataRequest

@Entity(tableName = "table_solution")
data class SolutionEntity (
    @PrimaryKey(autoGenerate = false)
    val serviceReportEquipmentSolutionId: String,
    val reportEquipmentId_solution: String,
    val subject: String,
    val problemCause: String,
    val solution: String,
    val createdDate: String,
    val createdBy: String,
    val lastModifiedDate: String,
    val lastModifiedBy: String,

    @TypeConverters(ConverterImage::class)
    val causeImage: List<ConverterImageModel>? = null,

    @TypeConverters(ConverterImage::class)
    val solutionImage: List<ConverterImageModel>? = null,

    val isUpload: Boolean,
    val isLocal: Boolean
)