package com.syspex.data.local.database.agreement_equipment

import android.app.Application
import androidx.lifecycle.LiveData
import com.syspex.data.local.database.AppDatabase
import com.syspex.data.local.database.GlobalDao
import com.syspex.data.model.EquipmentListModel

class AgreementEquipmentRepository constructor(application: Application) {

    lateinit var dao : GlobalDao
    lateinit var data : LiveData<List<AgreementEquipmentEntity>>

    init {
        val database = AppDatabase.getInstance(application)
        if (database != null) {
            dao = database.dao()
            data = dao.agreementEquipmentAll()
        }
    }

    fun getAll() : LiveData<List<AgreementEquipmentEntity>> {
        return data
    }

    fun getByAgreementId(getByAgreementId: String) : LiveData<List<EquipmentListModel>> {
        return dao.agreementEquipmentByAgreementId(getByAgreementId)
    }

    fun insert(entity: AgreementEquipmentEntity) {
        val thread = object : Thread() {
            override fun run() {
                dao.agreementEquipmentInsert(entity)
            }
        }
        thread.start()
    }

    fun delete(entity: AgreementEquipmentEntity) {
        val thread = object : Thread() {
            override fun run() {
                dao.agreementEquipmentDelete(entity)
            }
        }
        thread.start()
    }

    fun deleteAll() {
        val thread = object : Thread() {
            override fun run() {
                dao.agreementEquipmentDeleteAll()
            }
        }
        thread.start()
    }

}