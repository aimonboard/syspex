package com.syspex.data.local.database.activity_type

import android.app.Application
import androidx.lifecycle.LiveData
import com.syspex.data.local.database.AppDatabase
import com.syspex.data.local.database.GlobalDao
import com.syspex.data.remote.GetDataRequest

class ActivityTypeRepository constructor(application: Application) {

    lateinit var dao : GlobalDao
    lateinit var data : LiveData<List<ActivityTypeEntity>>

    init {
        val database = AppDatabase.getInstance(application)
        if (database != null) {
            dao = database.dao()
            data = dao.activityTypeAll()
        }
    }

    fun getAll() : LiveData<List<ActivityTypeEntity>> {
        return data
    }

    fun insert(entity: ActivityTypeEntity) {
        val thread = object : Thread() {
            override fun run() {
                dao.activityTypeInsert(entity)
            }
        }
        thread.start()
    }

    fun deleteAll() {
        val thread = object : Thread() {
            override fun run() {
                dao.activityTypeDeleteAll()
            }
        }
        thread.start()
    }

}