package com.syspex.data.local.database.agreement

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import com.syspex.data.remote.GetDataRequest

class AgreementViewModel constructor(application: Application) : AndroidViewModel(application) {
    private val repository : AgreementRepository = AgreementRepository(application)

    fun getAll(searchText: String, filterArray: ArrayList<String>): LiveData<List<AgreementEntity>> {
        return repository.getAll(searchText, filterArray)
    }

    fun saearch(textSearch: String): LiveData<List<AgreementEntity>> {
        return repository.search(textSearch)
    }

    fun getById(agreementId: String): LiveData<List<AgreementEntity>> {
        return repository.getById(agreementId)
    }

    fun getByEquipmentId(equipmentId: String): LiveData<List<AgreementEntity>> {
        return repository.getByEquipmentId(equipmentId)
    }

    fun getByAccountId(accountId: String): LiveData<List<AgreementEntity>> {
        return repository.getByAccountId(accountId)
    }

    fun insert(data: AgreementEntity){
        repository.insert(data)
    }

    fun delete(data: AgreementEntity){
        repository.delete(data)
    }

    fun deleteAll(){
        repository.deleteAll()
    }

}