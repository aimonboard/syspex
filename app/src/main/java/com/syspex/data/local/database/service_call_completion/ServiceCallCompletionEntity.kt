package com.syspex.data.local.database.service_call_completion

import androidx.room.Entity
import androidx.room.PrimaryKey
import androidx.room.TypeConverters
import com.google.gson.annotations.SerializedName
import com.syspex.data.local.database.ConverterEquipment
import com.syspex.data.remote.GetDataRequest

@Entity(tableName = "table_service_call_completion")
data class ServiceCallCompletionEntity (
    @PrimaryKey(autoGenerate = false)
    val completionId: String,
    val callId: String,
    val addressId: String,
    val accountContactId: String,
    val engineerId: String,
    val handoverNumber: String,
    val handoverType: String,
    val handoverPoRefNumber: String,
    val handoverCustomerAgreed: String,
    val handoverOutstandingMatters: String,
    val startDate: String,
    val endDate: String,
    val completionDate: String,
    val supplierPicName: String,
    val supplierPicPosition: String,
    val supplierSignatureUrl: String,
    val customerPicName: String,
    val customerPicPosition: String,
    val customerSignatureUrl: String,
    val createdDate: String,
    val createdBy: String,
    val lastModifiedDate: String,
    val lastModifiedBy: String

)