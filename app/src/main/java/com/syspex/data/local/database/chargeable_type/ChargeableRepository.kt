package com.syspex.data.local.database.chargeable_type

import android.app.Application
import androidx.lifecycle.LiveData
import com.syspex.data.local.database.AppDatabase
import com.syspex.data.local.database.GlobalDao

class ChargeableRepository constructor(application: Application) {

    lateinit var dao : GlobalDao

    init {
        val database = AppDatabase.getInstance(application)
        if (database != null) {
            dao = database.dao()
        }
    }

    fun getAll() : LiveData<List<ChargeableEntity>> {
        return dao.chargeableAll()
    }

    fun insert(entity: ChargeableEntity) {
        val thread = object : Thread() {
            override fun run() {
                dao.chargeableInsert(entity)
            }
        }
        thread.start()
    }
}