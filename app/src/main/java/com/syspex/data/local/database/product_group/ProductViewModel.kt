package com.syspex.data.local.database.product_group

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData

class ProductViewModel constructor(application: Application) : AndroidViewModel(application) {
    private val repository : ProductRepository = ProductRepository(application)

    fun getAll(textSearch: String): LiveData<List<ProductEntity>> {
        return repository.getAll(textSearch)
    }

    fun productGroupJoinProduct(productId: String): LiveData<List<ProductEntity>> {
        return repository.productGroupJoinProduct(productId)
    }

    fun getById(productGroupId: String): LiveData<List<ProductEntity>> {
        return repository.getById(productGroupId)
    }

    fun insert(data: ProductEntity){
        repository.insert(data)
    }

    fun delete(data: ProductEntity){
        repository.delete(data)
    }

    fun deleteAll(){
        repository.deleteAll()
    }

}