package com.syspex.data.local.database.global_enum

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import com.syspex.data.local.database.service_report.ServiceReportEntity
import com.syspex.data.model.SCListModel
import com.syspex.data.model.SRListModel
import com.syspex.data.remote.GetDataRequest

class EnumViewModel constructor(application: Application) : AndroidViewModel(application) {
    private val repository : EnumRepository = EnumRepository(application)

    fun getByTableName(tableName: String): LiveData<List<EnumEntity>> {
        return repository.getByTableName(tableName)
    }

    fun getByTableAndColumn(tableName: String, columnName: String): LiveData<List<EnumEntity>> {
        return repository.getByTableAndColumn(tableName, columnName)
    }

    fun insert(data: EnumEntity){
        repository.insert(data)
    }

}