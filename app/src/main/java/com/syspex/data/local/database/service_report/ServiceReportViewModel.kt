package com.syspex.data.local.database.service_report

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import com.syspex.data.model.AddEquipmentJointModel
import com.syspex.data.model.ReportTestModel
import com.syspex.data.model.SRListModel
import com.syspex.data.remote.GetDataRequest

class ServiceReportViewModel constructor(application: Application) : AndroidViewModel(application) {
    private val repository : ServiceReportRepository = ServiceReportRepository(application)

    fun getAll(searchText: String, filterArray: ArrayList<String>): LiveData<List<ServiceReportEntity>> {
        return repository.getAll(searchText, filterArray)
    }

//    fun getTest(filterArray: ArrayList<String>): LiveData<List<ReportTestModel>> {
//        return repository.getTest(filterArray)
//    }

    fun search(searchText: String): LiveData<List<ServiceReportEntity>> {
        return repository.search(number = searchText, accountName = searchText)
    }

    fun reportJoinReportEquipment(callId: String) : LiveData<List<AddEquipmentJointModel>> {
        return repository.reportJoinReportEquipment(callId)
    }

    fun getByReportId(reportId: String): LiveData<List<ServiceReportEntity>> {
        return repository.getByReportId(reportId)
    }

    fun getByTempNumber(tempNumber: String): LiveData<List<ServiceReportEntity>> {
        return repository.getByTempNumber(tempNumber)
    }

    fun getByCallId(callId: String): LiveData<List<ServiceReportEntity>> {
        return repository.getByCallId(callId)
    }

    fun getByCaseId(caseId: String): LiveData<List<ServiceReportEntity>> {
        return repository.getByCaseId(caseId)
    }

    fun getByAccountId(accountId: String): LiveData<List<ServiceReportEntity>> {
        return repository.getByAccountId(accountId)
    }

    fun reportEquipmentRelated(equipmentId: String): LiveData<List<ServiceReportEntity>> {
        return repository.reportEquipmentRelated(equipmentId)
    }

    fun getList(): LiveData<List<SRListModel>> {
        return repository.getList()
    }

    fun insert(data: ServiceReportEntity){
        repository.insert(data)
    }

    fun updateFeedback(reportId: String, rating: String, feedback: String, fileName: String, fileImage: String, cpNumber: String, cpName: String, isUplaod: Boolean) {
        repository.updateFeedback(reportId, rating, feedback, fileName, fileImage, cpNumber, cpName, isUplaod)
    }

    fun updateContact(reportId: String, picId: String, picName: String, picPhone: String, isUplaod: Boolean) {
        repository.updateContact(reportId, picId, picName, picPhone, isUplaod)
    }

    fun updateMaster(reportId: String, memo: String, isChargeable: String, picId: String, picName: String, picPhone: String, isUplaod: Boolean) {
        repository.updateMaster(reportId, memo, isChargeable, picId, picName, picPhone, isUplaod)
    }

    fun updateFlag(reportId: String, isUpload: Boolean){
        repository.updateFlag(reportId, isUpload)
    }

    fun deleteById(reportId: String){
        repository.deleteById(reportId)
    }

    fun delete(data: ServiceReportEntity){
        repository.delete(data)
    }

    fun deleteAll(){
        repository.deleteAll()
    }

}