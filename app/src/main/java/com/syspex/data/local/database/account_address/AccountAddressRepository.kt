package com.syspex.data.local.database.account_address

import android.app.Application
import androidx.lifecycle.LiveData
import com.syspex.data.local.database.AppDatabase
import com.syspex.data.local.database.GlobalDao

class AccountAddressRepository constructor(application: Application) {

    lateinit var dao : GlobalDao

    init {
        val database = AppDatabase.getInstance(application)
        if (database != null) {
            dao = database.dao()
        }
    }

    fun getAll() : LiveData<List<AccountAddressEntity>> {
        return dao.accountAddressAll()
    }

    fun getByAccountId(accountId: String) : LiveData<List<AccountAddressEntity>> {
        return dao.accountAddressByAccountId(accountId)
    }

    fun insert(entity: AccountAddressEntity) {
        val thread = object : Thread() {
            override fun run() {
                dao.accountAddressInsert(entity)
            }
        }
        thread.start()
    }

    fun updateFlag(entity: AccountAddressEntity) {
        val thread = object : Thread() {
            override fun run() {
                dao.accountAddressUpdate(entity)
            }
        }
        thread.start()
    }

}