package com.syspex.data.local.database.pcs_problem

import android.app.Application
import androidx.lifecycle.LiveData
import com.syspex.data.local.database.AppDatabase
import com.syspex.data.local.database.GlobalDao
import com.syspex.data.local.database.service_call_equipment.ServiceCallEquipmentEntity
import com.syspex.data.model.SCListModel
import com.syspex.data.model.SRListModel
import com.syspex.data.remote.GetDataRequest

class ProblemRepository constructor(application: Application) {

    lateinit var dao : GlobalDao

    init {
        val database = AppDatabase.getInstance(application)
        if (database != null) {
            dao = database.dao()
        }
    }

    fun getByReportEquipment(reportEquipmentId: String) : LiveData<List<ProblemEntity>> {
        return dao.problemByreportEquipment(reportEquipmentId)
    }

    fun insert(entity: ProblemEntity) {
        val thread = object : Thread() {
            override fun run() {
                dao.problemInsert(entity)
            }
        }
        thread.start()
    }

    fun update(entity: ProblemEntity){
        val thread = object : Thread() {
            override fun run() {
                dao.problemUpdate(entity)
            }
        }
        thread.start()
    }
}