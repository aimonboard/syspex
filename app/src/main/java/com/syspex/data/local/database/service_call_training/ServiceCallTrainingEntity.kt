package com.syspex.data.local.database.service_call_training

import androidx.room.Entity
import androidx.room.PrimaryKey
import androidx.room.TypeConverters
import com.google.gson.annotations.SerializedName
import com.syspex.data.local.database.ConverterEquipment
import com.syspex.data.remote.GetDataRequest

@Entity(tableName = "table_service_call_training")
data class ServiceCallTrainingEntity (
    @PrimaryKey(autoGenerate = false)
    val trainingId: String,
    val callId: String,
    val addressId: String,
    val trainingFormNumber: String,
    val trainingType: String,
    val trainingStartDate: String,
    val trainingDetail: String,
    val engineerId: String,
    val engineerSignatureUrl: String,
    val customerPicName: String,
    val customerPicPosition: String,
    val customerSignatureUrl: String,
    val officerName: String,
    val createdDate: String,
    val createdBy: String,
    val lastModifiedDate: String,
    val lastModifiedBy: String,
    val trainingEndDate: String,
    val accountContactId: String

)