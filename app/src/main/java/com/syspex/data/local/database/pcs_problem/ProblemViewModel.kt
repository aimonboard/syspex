package com.syspex.data.local.database.pcs_problem

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import com.syspex.data.local.database.service_call_equipment.ServiceCallEquipmentEntity
import com.syspex.data.local.database.service_report.ServiceReportEntity
import com.syspex.data.model.SCListModel
import com.syspex.data.model.SRListModel
import com.syspex.data.remote.GetDataRequest

class ProblemViewModel constructor(application: Application) : AndroidViewModel(application) {
    private val repository : ProblemRepository = ProblemRepository(application)

    fun getByReportEquipment(reportEquipmentId: String): LiveData<List<ProblemEntity>> {
        return repository.getByReportEquipment(reportEquipmentId)
    }

    fun insert(data: ProblemEntity){
        repository.insert(data)
    }

    fun update(data: ProblemEntity) {
        repository.update(data)
    }

}