package com.syspex.data.local.database.pcs_problem

import android.net.Uri
import androidx.room.Entity
import androidx.room.PrimaryKey
import androidx.room.TypeConverters
import com.syspex.data.local.database.ConverterEquipment
import com.syspex.data.local.database.ConverterImage
import com.syspex.data.model.ConverterImageModel
import com.syspex.data.remote.GetDataRequest

@Entity(tableName = "table_problem")
data class ProblemEntity (
    @PrimaryKey(autoGenerate = false)
    val reportEquipmentId_problem: String,
    val problemImage: List<ConverterImageModel>? = null
)