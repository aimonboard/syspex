package com.syspex.data.local.database.account_attachment

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData

class AccountAttachmentViewModel constructor(application: Application) : AndroidViewModel(application) {
    private val repository : AccountAttachmentRepository = AccountAttachmentRepository(application)

    fun getAll(): LiveData<List<AccountAttachmentEntity>> {
        return repository.getAll()
    }

    fun accountAttachment(accountId: String): LiveData<List<AccountAttachmentEntity>> {
        return repository.accountAttachment(accountId)
    }

    fun accountAttachmentSection(accountId: String, sourceId: String, sourceTable: String): LiveData<List<AccountAttachmentEntity>> {
        return repository.accountAttachmentSection(accountId, sourceId, sourceTable)
    }

    fun insert(data: AccountAttachmentEntity){
        repository.insert(data)
    }

    fun updateFlag(entity: AccountAttachmentEntity){
        repository.updateFlag(entity)
    }

}