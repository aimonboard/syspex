package com.syspex.data.local.database.email

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "table_email")
data class EmailEntity (
    @PrimaryKey(autoGenerate = true)
    val emailId: Int,
    val reportId: String,
    val to: String,
    val other: String,
    val attachment: String
)