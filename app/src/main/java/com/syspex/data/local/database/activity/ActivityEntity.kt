package com.syspex.data.local.database.activity

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "table_activity")
data class ActivityEntity (
    @PrimaryKey(autoGenerate = false)
    val activityId: String,
    val caseId: String,
    val accountId: String,
    val serviceCallId: String,
    val regionId: String,
    val userId: String,
    val activityType: String,
    val activityStartTime: String,
    val activityEndTime: String,
    val activitySubject: String,
    val activityDescription: String,
    val activitySourceTable: String,
    val activitySourceId: String,
    val createdDate: String,
    val createdBy: String,
    val lastModifiedDate: String,
    val lastModifiedBy: String,
    val activityTypeId: String,
    val activityTypeCode: String,
    val activityTypeName: String,
    val activityTypeColor: String,
    val isLocal: Boolean
)