package com.syspex.data.local.database.service_report_equipment_checklist

import androidx.room.Entity
import androidx.room.PrimaryKey
import androidx.room.TypeConverters
import com.google.gson.annotations.SerializedName
import com.syspex.data.local.database.ConverterEquipment
import com.syspex.data.local.database.ConverterImage
import com.syspex.data.model.ConverterImageModel
import com.syspex.data.remote.GetDataRequest

@Entity(tableName = "table_service_report_equipment_checklist")
data class ServiceReportEquipmentChecklistEntity (
    @PrimaryKey(autoGenerate = false)
    val service_report_equipment_checklist_id: String,
    val report_equipment_id: String,
    val description: String,
    val remarks: String,
    val created_date: String,
    val created_by: String,
    val last_modified_date: String,
    val last_modified_by: String,

    val checklist_status_id: String,
    val checklist_status_Name: String,

    val question_id: String,
    val question_order_no: String,
    val question_section: String,
    val question_desc: String,
    val question_default_remarks: String,
    val question_help_text: String,
    val question_help_image_url: String,
    val checklist_template_id: String,

    val product_group_checklist_template_id: String,
    val region_id: String,
    val product_group_id: String,
    val checklist_type: String,
    val template_desc: String,

    @TypeConverters(ConverterImage::class)
    val checklistImage: List<ConverterImageModel>? = null,

    val isLocal: Boolean
)