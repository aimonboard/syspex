package com.syspex.data.local.database.service_report_equipment_checklist

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import com.syspex.data.local.database.product_checklist_template.ChecklistTemplateEntity
import com.syspex.data.local.database.service_report.ServiceReportEntity
import com.syspex.data.local.database.service_report_equipment.ServiceReportEquipmentRepository
import com.syspex.data.model.ConverterImageModel
import com.syspex.data.model.SCListModel
import com.syspex.data.model.SRListModel
import com.syspex.data.remote.GetDataRequest

class ServiceReportEquipmentChecklistViewModel constructor(application: Application) : AndroidViewModel(application) {
    private val repository : ServiceReportEquipmentChecklistRepository = ServiceReportEquipmentChecklistRepository(application)

    fun getByReportEquipmentId(reportEquipmentId: String): LiveData<List<ServiceReportEquipmentChecklistEntity>> {
        return repository.getByReportEquipmentId(reportEquipmentId)
    }

    fun insert(data: ServiceReportEquipmentChecklistEntity){
        repository.insert(data)
    }

    fun insertAll(data: ArrayList<ServiceReportEquipmentChecklistEntity>){
        repository.insertAll(data)
    }

    fun updateStatus(questionId: String, enumId: String, enumName: String, remark: String){
        repository.updateStatus(questionId, enumId, enumName, remark)
    }

    fun updateImage(questionId: String, checklistImage: ArrayList<ConverterImageModel>){
        repository.updateImage(questionId, checklistImage)
    }

    fun deleteById(reportEquipmentId: String){
        repository.deleteById(reportEquipmentId)
    }

}