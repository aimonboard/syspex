package com.syspex.data.local.database.file

import android.app.Application
import androidx.lifecycle.LiveData
import com.syspex.data.local.database.AppDatabase
import com.syspex.data.local.database.GlobalDao

class FileRepository constructor(application: Application) {

    lateinit var dao : GlobalDao
    lateinit var data : LiveData<List<FileEntity>>

    init {
        val database = AppDatabase.getInstance(application)
        if (database != null) {
            dao = database.dao()
            data = dao.fileAll()
        }
    }

    fun getAll() : LiveData<List<FileEntity>> {
        return data
    }

    fun getByName(fileName: String) : LiveData<List<FileEntity>> {
        return dao.fileGetByName(fileName)
    }

    fun insert(entity: FileEntity) {
        val thread = object : Thread() {
            override fun run() {
                dao.fileInsert(entity)
            }
        }
        thread.start()
    }

    fun delete(entity: FileEntity) {
        val thread = object : Thread() {
            override fun run() {
                dao.fileDelete(entity)
            }
        }
        thread.start()
    }

    fun deleteAll() {
        val thread = object : Thread() {
            override fun run() {
                dao.fileDeleteAll()
            }
        }
        thread.start()
    }

}