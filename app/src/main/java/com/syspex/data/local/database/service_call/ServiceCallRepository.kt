package com.syspex.data.local.database.service_call

import android.app.Application
import androidx.lifecycle.LiveData
import com.syspex.data.local.database.AppDatabase
import com.syspex.data.local.database.GlobalDao
import com.syspex.data.model.ReportAlertModel
import com.syspex.data.model.SCListModel
import com.syspex.data.model.SRListModel
import com.syspex.data.remote.GetDataRequest

class ServiceCallRepository constructor(application: Application) {

    lateinit var dao : GlobalDao

    init {
        val database = AppDatabase.getInstance(application)
        if (database != null) {
            dao = database.dao()
        }
    }

    fun alert(startDateTime: String, endDateTime: String) : LiveData<List<ReportAlertModel>> {
        return dao.callTodayAlert(startDateTime, endDateTime)
    }

    fun getAll(searchText: String, filterArray: ArrayList<String>) : LiveData<List<ServiceCallEntity>> {
        return dao.callAll(searchText, filterArray)
    }

    fun search(number: String, accountName: String, subject: String): LiveData<List<ServiceCallEntity>> {
        return dao.callSearch(number, accountName, subject)
    }

    fun getByAccountId(accountId: String) : LiveData<List<ServiceCallEntity>> {
        return dao.callByAccountId(accountId)
    }

    fun getByCallId(callId: String) : LiveData<List<ServiceCallEntity>> {
        return dao.callByCallId(callId)
    }

    fun getByCallIdBackground(callId: String) : List<ServiceCallEntity> {
        return dao.callByCallIdBackground(callId)
    }

    fun getByCaseId(caseId: String) : LiveData<List<ServiceCallEntity>> {
        return dao.callByCaseId(caseId)
    }

    fun callEquipmentRelated(equipmentId: String) : LiveData<List<ServiceCallEntity>> {
        return dao.callEquipmentRelated(equipmentId)
    }

    fun getList() : LiveData<List<SCListModel>> {
        return dao.callList()
    }

    fun getReportByCallId(callId: String) : LiveData<List<SRListModel>> {
        return dao.callReportByCallId(callId)
    }

    fun updateEquipment(id: String, data: List<GetDataRequest.Equipment>) {
        val thread = object : Thread() {
            override fun run() {
                dao.callUpdateEquipment(id, data)
            }
        }
        thread.start()
    }

    fun insert(entity: ServiceCallEntity) {
        val thread = object : Thread() {
            override fun run() {
                dao.callInsert(entity)
            }
        }
        thread.start()
    }

    fun delete(entity: ServiceCallEntity) {
        val thread = object : Thread() {
            override fun run() {
                dao.callDelete(entity)
            }
        }
        thread.start()
    }

    fun deleteAll() {
        val thread = object : Thread() {
            override fun run() {
                dao.callDeleteAll()
            }
        }
        thread.start()
    }

    fun updateFlag(callId: String, isUpload: Boolean) {
        val thread = object : Thread() {
            override fun run() {
                dao.callUpdateFlag(callId, isUpload)
            }
        }
        thread.start()

    }

}