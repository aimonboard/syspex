package com.syspex.data.local.database

import androidx.room.TypeConverter
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.syspex.data.model.ConverterImageModel
import com.syspex.data.remote.GetDataRequest
import java.lang.reflect.Type
import java.util.*

class ConverterImage {
    var gson = Gson()

    @TypeConverter
    fun stringToSomeObjectList(data: String?): List<ConverterImageModel?>? {
        if (data == null) {
            return Collections.emptyList()
        }
        val listType: Type =
            object : TypeToken<List<ConverterImageModel?>?>() {}.type
        return gson.fromJson<List<ConverterImageModel?>>(data, listType)
    }

    @TypeConverter
    fun someObjectListToString(someObjects: List<ConverterImageModel?>?): String? {
        return gson.toJson(someObjects)
    }


}