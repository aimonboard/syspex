package com.syspex.data.local.database.account

import android.app.Application
import androidx.lifecycle.LiveData
import com.syspex.data.local.database.AppDatabase
import com.syspex.data.local.database.GlobalDao
import com.syspex.data.model.AccountList
import com.syspex.data.remote.GetDataRequest

class AccountRepository constructor(application: Application) {

    lateinit var dao : GlobalDao

    init {
        val database = AppDatabase.getInstance(application)
        if (database != null) {
            dao = database.dao()
        }
    }

    fun getAll() : LiveData<List<AccountEntity>> {
        return dao.accountAll()
    }

    fun getList(searchText: String) : LiveData<List<AccountList>> {
        return dao.accountList(searchText)
    }

    fun search(code: String, accountName: String) : LiveData<List<AccountList>> {
        return dao.accountSearch(code, accountName)
    }

    fun getByAccountId(accountId: String) : LiveData<List<AccountEntity>> {
        return dao.accountByAccountId(accountId)
    }

    fun insert(entity: AccountEntity) {
        val thread = object : Thread() {
            override fun run() {
                dao.accountInsert(entity)
            }
        }
        thread.start()
    }

    fun delete(entity: AccountEntity) {
        val thread = object : Thread() {
            override fun run() {
                dao.accountDelete(entity)
            }
        }
        thread.start()
    }

    fun deleteAll() {
        val thread = object : Thread() {
            override fun run() {
                dao.accountDeleteAll()
            }
        }
        thread.start()
    }

}