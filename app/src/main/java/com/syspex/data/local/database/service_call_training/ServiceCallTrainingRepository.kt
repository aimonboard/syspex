package com.syspex.data.local.database.service_call_training

import android.app.Application
import androidx.lifecycle.LiveData
import com.syspex.data.local.database.AppDatabase
import com.syspex.data.local.database.GlobalDao
import com.syspex.data.local.database.service_report_equipment.ServiceReportEquipmentEntity
import com.syspex.data.model.EquipmentListModel
import com.syspex.data.model.SCListModel
import com.syspex.data.model.SRListModel
import com.syspex.data.remote.GetDataRequest

class ServiceCallTrainingRepository constructor(application: Application) {

    lateinit var dao : GlobalDao

    init {
        val database = AppDatabase.getInstance(application)
        if (database != null) {
            dao = database.dao()
        }
    }

    fun getAll() : LiveData<List<ServiceCallTrainingEntity>> {
        return dao.callTrainingAll()
    }

    fun getByCallId(callId: String) : LiveData<List<ServiceCallTrainingEntity>> {
        return dao.callTrainingByCallId(callId)
    }

    fun insert(entity: ServiceCallTrainingEntity) {
        val thread = object : Thread() {
            override fun run() {
                dao.callTrainingInsert(entity)
            }
        }
        thread.start()
    }

    fun delete(entity: ServiceCallTrainingEntity) {
        val thread = object : Thread() {
            override fun run() {
                dao.callTrainingDelete(entity)
            }
        }
        thread.start()
    }

}