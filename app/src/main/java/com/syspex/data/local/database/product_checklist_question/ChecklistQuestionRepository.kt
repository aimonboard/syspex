package com.syspex.data.local.database.product_checklist_question

import android.app.Application
import androidx.lifecycle.LiveData
import com.syspex.data.local.database.AppDatabase
import com.syspex.data.local.database.GlobalDao
import com.syspex.data.local.database.product_checklist_template.ChecklistTemplateEntity
import com.syspex.data.model.SCListModel
import com.syspex.data.model.SRListModel
import com.syspex.data.remote.GetDataRequest

class ChecklistQuestionRepository constructor(application: Application) {

    lateinit var dao : GlobalDao

    init {
        val database = AppDatabase.getInstance(application)
        if (database != null) {
            dao = database.dao()
        }
    }

    fun getByTemplate(templateId: String) : LiveData<List<ChecklistQuestionEntity>> {
        return dao.checklistQuestionByTemplate(templateId)
    }

    fun insert(entity: ChecklistQuestionEntity) {
        val thread = object : Thread() {
            override fun run() {
                dao.checklistQuestionInsert(entity)
            }
        }
        thread.start()
    }

    fun deleteAll() {
        val thread = object : Thread() {
            override fun run() {
                dao.checklistQuestionByDeleteAll()
            }
        }
        thread.start()
    }

}