package com.syspex.data.local.database.pcs_solution

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import com.syspex.data.local.database.service_call_equipment.ServiceCallEquipmentEntity
import com.syspex.data.local.database.service_report.ServiceReportEntity
import com.syspex.data.model.SCListModel
import com.syspex.data.model.SRListModel
import com.syspex.data.remote.GetDataRequest

class SolutionViewModel constructor(application: Application) : AndroidViewModel(application) {
    private val repository : SolutionRepository = SolutionRepository(application)

    fun getByReportEquipment(reportEquipmentId: String): LiveData<List<SolutionEntity>> {
        return repository.getByReportEquipment(reportEquipmentId)
    }

    fun getBySolutionId(solutionId: String): LiveData<List<SolutionEntity>> {
        return repository.getBySolutionId(solutionId)
    }


    fun insert(data: SolutionEntity){
        repository.insert(data)
    }

    fun update(data: SolutionEntity) {
        repository.update(data)
    }

    fun deleteByReportEquipmentId(reportEquipmentId: String) {
        repository.deleteByReportEquipmentId(reportEquipmentId)
    }

    fun delete(data: SolutionEntity){
        repository.delete(data)
    }

    fun deleteAll(){
        repository.deleteAll()
    }

}