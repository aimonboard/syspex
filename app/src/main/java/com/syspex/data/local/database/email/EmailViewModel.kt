package com.syspex.data.local.database.email

import android.app.Application
import androidx.lifecycle.AndroidViewModel

class EmailViewModel constructor(application: Application) : AndroidViewModel(application) {
    private val repository : EmailRepository = EmailRepository(application)

    fun getAll(): List<EmailEntity> {
        return repository.getAll()
    }

    fun insert(data: EmailEntity){
        repository.insert(data)
    }

    fun deleteById(emailId: Int){
        repository.deleteById(emailId)
    }

}