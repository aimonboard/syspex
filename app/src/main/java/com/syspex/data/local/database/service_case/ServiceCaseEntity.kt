package com.syspex.data.local.database.service_case

import androidx.room.Entity
import androidx.room.PrimaryKey
import androidx.room.TypeConverters
import com.syspex.data.local.database.ConverterAttachment
import com.syspex.data.local.database.ConverterEquipment
import com.syspex.data.local.database.service_call.ServiceCallConverterEngineer
import com.syspex.data.local.database.service_call.ServiceCallConverterEquipment
import com.syspex.data.remote.GetDataRequest

@Entity(tableName = "table_service_case")
data class ServiceCaseEntity (
    @PrimaryKey (autoGenerate = false)
    val caseId: String,

    val accountId: String,
    val accountName: String,

    val agreementId: String,
    val agreementType: String,

    val regionId: String,
    val caseNumber: String,

    val caseTypeEnumId: String,
    val caseTypeEnumName: String,

    val caseTypeRemark: String,

    val caseAccountContactId: String,
    val caseAccountContactName: String,
    val caseAccountContactPhone: String,

    val caseFieldAccountContactId: String,
    val caseFieldAccountContactName: String,
    val caseFieldAccountContactPhone: String,

    val accountSalesId: String,
    val accountSalesName: String,
    val caseSalesId: String,
    val caseSalesName: String,

    val leadEngineerId: String,
    val leadEngineerName: String,

    val caseOwnerId: String,
    val caseOwnerName: String,

    val caseStatusEnumId: String,
    val caseStatusEnumName: String,
    val colorBackground: String? = "",
    val colorStatusLabel: String? = "",
    val colorStatusLabelText: String? = "",

    val caseStatusRemark: String,
    val subject: String,
    val description: String,

    val addressId: String,
    val addressText: String,

    val loanDemoPeriod: String,
    val openedTime: String,
    val assignedTime: String,
    val proposedDeliveryDate: String,
    val proposedInstallationDate: String,
    val directDelivery: String,
    val customerPo: String,
    val noOfFreeService: String,
    val materialNeeded: String,
    val airUtilitySuppliedBy: String,
    val specialRequirement: String,
    val warrantyPeriodSupplier: String,
    val warrantyPeriodCustomer: String,
    val professionalInspectionRequired: String,
    val inspectionRemark: String,
    val caseColorCalendar: String,
    val createdDate: String,
    val createdBy: String,
    val lastModifiedDate: String,
    val lastModifiedBy: String,

    @TypeConverters(ServiceCaseConverterEquipment::class)
    val equipment: List<GetDataRequest.Data.ServiceCase.ServiceCaseEquipment?>? = null,

    @TypeConverters(ServiceCallConverterEngineer::class)
    val engineer: List<GetDataRequest.Engineer?>? = null,

    val isUpload: Boolean
)