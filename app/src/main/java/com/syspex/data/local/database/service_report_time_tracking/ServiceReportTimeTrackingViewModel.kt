package com.syspex.data.local.database.service_report_time_tracking

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import com.syspex.data.local.database.service_report.ServiceReportEntity
import com.syspex.data.model.SCListModel
import com.syspex.data.model.SRListModel
import com.syspex.data.remote.GetDataRequest

class ServiceReportTimeTrackingViewModel constructor(application: Application) : AndroidViewModel(application) {
    private val repository : ServiceReportTimeTrackingRepository = ServiceReportTimeTrackingRepository(application)

    fun getByReportId(reportId: String): LiveData<List<ServiceReportTimeTrackingEntity>> {
        return repository.getByReportId(reportId)
    }

    fun getByCallId(callId: String): LiveData<List<ServiceReportTimeTrackingEntity>> {
        return repository.getByCallId(callId)
    }

    fun getByCallIdBackground(callId: String): List<ServiceReportTimeTrackingEntity> {
        return repository.getByCallIdBackground(callId)
    }

    fun insert(data: ServiceReportTimeTrackingEntity){
        repository.insert(data)
    }

    fun delete(data: ServiceReportTimeTrackingEntity){
        repository.delete(data)
    }

    fun deleteById(id: String){
        repository.deleteById(id)
    }

    fun deleteAll(){
        repository.deleteAll()
    }

}