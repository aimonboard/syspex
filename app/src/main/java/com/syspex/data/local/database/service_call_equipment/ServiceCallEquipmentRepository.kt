package com.syspex.data.local.database.service_call_equipment

import android.app.Application
import androidx.lifecycle.LiveData
import com.syspex.data.local.database.AppDatabase
import com.syspex.data.local.database.GlobalDao
import com.syspex.data.local.database.service_report_equipment.ServiceReportEquipmentEntity
import com.syspex.data.model.EquipmentListModel
import com.syspex.data.model.SCListModel
import com.syspex.data.model.SRListModel
import com.syspex.data.remote.GetDataRequest

class ServiceCallEquipmentRepository constructor(application: Application) {

    lateinit var dao : GlobalDao

    init {
        val database = AppDatabase.getInstance(application)
        if (database != null) {
            dao = database.dao()
        }
    }

    fun getAll() : LiveData<List<ServiceCallEquipmentEntity>> {
        return dao.callEquipmentAll()
    }

    fun checkAttach(callId: String, equipmentId: String): LiveData<List<ServiceCallEquipmentEntity>> {
        return dao.callEquipmentCheckAttach(callId, equipmentId)
    }

    fun getEquipmentList(callId: String, isAttachToCall: Boolean) : LiveData<List<EquipmentListModel>> {
        return dao.callEquipmentList(callId, isAttachToCall)
    }

    fun getAttachToCall(callId: String, isAttachToCall: Boolean) : LiveData<List<ServiceCallEquipmentEntity>> {
        return dao.callEquipmentAttachToCall(callId, isAttachToCall)
    }

    fun getAttachToReport(callId: String, isAttachToCall: Boolean, isAttachToReport: Boolean) : LiveData<List<ServiceCallEquipmentEntity>> {
        return dao.callEquipmentAttachToReport(callId, isAttachToCall, isAttachToReport)
    }

    fun equipmentAdd(callId: String, reportId:String, searchText: String) : LiveData<List<ServiceCallEquipmentEntity>> {
        return dao.callEquipmentAdd(callId, reportId, searchText)
    }

    fun insert(entity: ServiceCallEquipmentEntity) {
        val thread = object : Thread() {
            override fun run() {
                dao.callEquipmentInsert(entity)
            }
        }
        thread.start()
    }

    fun updateFlagCall(callId: String, equipmentId: String, isAttachToCall: Boolean) {
        val thread = object : Thread() {
            override fun run() {
                dao.callEquipmentUpdateFlagCall(callId, equipmentId, isAttachToCall)
            }
        }
        thread.start()
    }

    fun updateFlagReport(callId: String, equipmentId: String, isAttachToReport: Boolean) {
        val thread = object : Thread() {
            override fun run() {
                dao.callEquipmentUpdateFlagReport(callId, equipmentId, isAttachToReport)
            }
        }
        thread.start()
    }

    fun delete(entity: ServiceCallEquipmentEntity) {
        val thread = object : Thread() {
            override fun run() {
                dao.callEquipmentDelete(entity)
            }
        }
        thread.start()
    }

    fun deleteById(callEquipmentId: String) {
        val thread = object : Thread() {
            override fun run() {
                dao.callEquipmentDeleteById(callEquipmentId)
            }
        }
        thread.start()
    }

}