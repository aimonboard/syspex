package com.syspex.data.local.database.product_document

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import com.syspex.data.local.database.service_report.ServiceReportEntity
import com.syspex.data.model.SCListModel
import com.syspex.data.model.SRListModel
import com.syspex.data.remote.GetDataRequest

class ProductDocumentViewModel constructor(application: Application) : AndroidViewModel(application) {
    private val repository : ProductDocumentRepository = ProductDocumentRepository(application)

    fun getByProductGroupId(productGroupId: String): LiveData<List<ProductDocumentEntity>> {
        return repository.getByProductGroupId(productGroupId)
    }

    fun insert(data: ProductDocumentEntity){
        repository.insert(data)
    }

}