package com.syspex.data.local.database.account_attachment

import androidx.room.Entity
import androidx.room.PrimaryKey
import androidx.room.TypeConverters
import com.syspex.data.local.database.ConverterEquipment
import com.syspex.data.remote.GetDataRequest

@Entity(tableName = "table_account_attachment")
data class AccountAttachmentEntity (
    @PrimaryKey(autoGenerate = false)
    val accountAttachmentId: String,
    val regionId: String,
    val accountId: String,
    val attachmentTitle: String,
    val attachmentBody: String,
    val fileUrl: String,
    val attachmentOwnerId: String,
    val createdDate: String,
    val createdBy: String,
    val lastModifiedDate: String,
    val lastModifiedBy: String,
    val attachmentSourceTable: String,
    val attachmentSourceId: String,
    val statusEnumId: String,
    val statusEnumText: String,
    val createdById: String,
    val createdByName: String
)