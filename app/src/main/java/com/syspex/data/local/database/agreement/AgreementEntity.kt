package com.syspex.data.local.database.agreement

import androidx.room.Entity
import androidx.room.PrimaryKey
import androidx.room.TypeConverters
import com.google.gson.annotations.SerializedName
import com.syspex.data.local.database.ConverterEquipment
import com.syspex.data.remote.GetDataRequest

@Entity(tableName = "table_agreement")
data class AgreementEntity (
    @PrimaryKey (autoGenerate = false)
    val agreementId: String,

    val caseId: String,

    val accountId: String,
    val accountName: String,

    val callId: String,

    val regionId: String,
    val accountContactId: String,

    val agreementTypeEnumId: String,
    val agreementTypeEnumName: String,

    val agreementNumber: String,

    val agreementStatusEnumId: String,
    val agreementStatusEnumText: String,
    val colorBackground: String? = "",
    val colorStatusLabel: String? = "",
    val colorStatusLabelText: String? = "",

    val installationDeliveryAddressId: String,
    val installationDeliveryAddressText: String,

    val installationContactPicId: String,
    val installationContactPicName: String,
    val installationContactPicPhone: String,

    val agreementParentId: String,
    val internalMemo: String,
    val noOfService: String,
    val agreementStartDate: String,
    val agreementEndDate: String,
    val installationDeliveryAddress: String,
    val installationContactPic: String,
    val firstVisitDate: String,
    val createdDate: String,
    val createdBy: String,
    val lastModifiedDate: String,
    val lastModifiedBy: String,

    @TypeConverters(ConverterEquipment::class)
    val equipment: List<GetDataRequest.Equipment?>? = null
)