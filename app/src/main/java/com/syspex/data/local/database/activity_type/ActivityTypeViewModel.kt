package com.syspex.data.local.database.activity_type

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import com.syspex.data.remote.GetDataRequest

class ActivityTypeViewModel constructor(application: Application) : AndroidViewModel(application) {
    private val repository : ActivityTypeRepository = ActivityTypeRepository(application)
    private val data : LiveData<List<ActivityTypeEntity>>

    init {
        data = repository.getAll()
    }

    fun getAll(): LiveData<List<ActivityTypeEntity>> {
        return data
    }

    fun insert(data: ActivityTypeEntity){
        repository.insert(data)
    }

    fun deleteAll(){
        repository.deleteAll()
    }

}