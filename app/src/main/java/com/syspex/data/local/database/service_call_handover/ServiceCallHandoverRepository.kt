package com.syspex.data.local.database.service_call_handover

import android.app.Application
import androidx.lifecycle.LiveData
import com.syspex.data.local.database.AppDatabase
import com.syspex.data.local.database.GlobalDao
import com.syspex.data.local.database.service_report_equipment.ServiceReportEquipmentEntity
import com.syspex.data.model.EquipmentListModel
import com.syspex.data.model.SCListModel
import com.syspex.data.model.SRListModel
import com.syspex.data.remote.GetDataRequest

class ServiceCallHandoverRepository constructor(application: Application) {

    lateinit var dao : GlobalDao

    init {
        val database = AppDatabase.getInstance(application)
        if (database != null) {
            dao = database.dao()
        }
    }

    fun getAll() : LiveData<List<ServiceCallHandoverEntity>> {
        return dao.callHandoverAll()
    }

    fun getByCallId(callId: String) : LiveData<List<ServiceCallHandoverEntity>> {
        return dao.callHandoverByCallId(callId)
    }

    fun insert(entity: ServiceCallHandoverEntity) {
        val thread = object : Thread() {
            override fun run() {
                dao.callHandoverInsert(entity)
            }
        }
        thread.start()
    }

    fun delete(entity: ServiceCallHandoverEntity) {
        val thread = object : Thread() {
            override fun run() {
                dao.callHandoverDelete(entity)
            }
        }
        thread.start()
    }

}