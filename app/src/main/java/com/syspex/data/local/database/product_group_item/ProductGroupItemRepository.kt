package com.syspex.data.local.database.product_group_item

import android.app.Application
import androidx.lifecycle.LiveData
import com.syspex.data.local.database.AppDatabase
import com.syspex.data.local.database.GlobalDao

class ProductGroupItemRepository constructor(application: Application) {

    lateinit var dao : GlobalDao

    init {
        val database = AppDatabase.getInstance(application)
        if (database != null) {
            dao = database.dao()
        }
    }

    fun getAll() : LiveData<List<ProductGroupItemEntity>> {
        return dao.productGroupItemAll()
    }

    fun getByProductId(productId: String) : LiveData<List<ProductGroupItemEntity>> {
        return dao.productGroupItemByProductId(productId)
    }

    fun insert(entity: ProductGroupItemEntity) {
        val thread = object : Thread() {
            override fun run() {
                dao.productGroupItemInsert(entity)
            }
        }
        thread.start()
    }

}