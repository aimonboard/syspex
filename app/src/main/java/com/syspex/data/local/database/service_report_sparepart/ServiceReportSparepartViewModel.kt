package com.syspex.data.local.database.service_report_sparepart

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import com.syspex.data.local.database.service_report.ServiceReportEntity
import com.syspex.data.model.SCListModel
import com.syspex.data.model.SRListModel
import com.syspex.data.remote.GetDataRequest

class ServiceReportSparepartViewModel constructor(application: Application) : AndroidViewModel(application) {
    private val repository : ServiceReportSparepartRepository = ServiceReportSparepartRepository(application)

    fun getAll(): LiveData<List<ServiceReportSparepartEntity>> {
        return repository.getAll()
    }

    fun getByReportEquipmentId(reportEquipmentId: String): LiveData<List<ServiceReportSparepartEntity>> {
        return repository.getByReportEquipmentId(reportEquipmentId)
    }

    fun insertCheck(reportEquipmentId: String, partId: String, enumId: String): LiveData<List<ServiceReportSparepartEntity>> {
        return repository.insertCheck(reportEquipmentId, partId, enumId)
    }

    fun insert(data: ServiceReportSparepartEntity){
        repository.insert(data)
    }

    fun update(data: ServiceReportSparepartEntity){
        repository.update(data)
    }

    fun deleteByReportEquipmentId(reportEquipmentId: String){
        repository.deleteByReportEquipmentId(reportEquipmentId)
    }

    fun delete(data: ServiceReportSparepartEntity){
        repository.delete(data)
    }

}