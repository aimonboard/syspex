package com.syspex.data.local.database.service_report_time_tracking

import androidx.room.Entity
import androidx.room.PrimaryKey
import androidx.room.TypeConverters
import com.syspex.data.local.database.ConverterEquipment
import com.syspex.data.remote.GetDataRequest

@Entity(tableName = "table_service_report_time_tracking")
data class ServiceReportTimeTrackingEntity (
    @PrimaryKey(autoGenerate = false)
    val timeTrackingId: String,
    val callId: String,
    val reportId: String,
    val engineerId: String,
    val engineerName: String,
    val isLead: String,
    val start: String,
    val end: String,
    val isUpload: Boolean,
    val isLocal: Boolean
)