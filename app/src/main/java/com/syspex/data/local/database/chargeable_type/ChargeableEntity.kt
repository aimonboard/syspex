package com.syspex.data.local.database.chargeable_type

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "table_chargeable")
data class ChargeableEntity (
    @PrimaryKey(autoGenerate = false)
    val key: String,
    val value: String,
    val isShow: String
)