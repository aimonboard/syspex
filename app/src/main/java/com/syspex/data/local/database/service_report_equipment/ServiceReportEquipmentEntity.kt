package com.syspex.data.local.database.service_report_equipment

import androidx.room.Entity
import androidx.room.PrimaryKey
import androidx.room.TypeConverters
import com.syspex.data.local.database.ConverterEquipment
import com.syspex.data.remote.GetDataRequest

@Entity(tableName = "table_service_report_equipment")
data class ServiceReportEquipmentEntity (
    @PrimaryKey(autoGenerate = false)
    val serviceReportEquipmentId: String,
    val serviceReportEquipmentCallId: String,
    val reportId: String,
    val equipmentId: String,
    val problemSymptom: String,
    val statusAfterServiceEnumId: String,
    val statusAfterServiceEnumName: String,
    val statusAfterServiceRemarks: String,
    val installationStartDate: String,
    val installationMatters: String,
    val checklistTemplateId: String,
    val createdDate: String,
    val createdBy: String,
    val lastModifiedDate: String,
    val lastModifiedBy: String,
    val equipmentName: String,

    val regionId: String,
    val serialNo: String,
    val brand: String,
    val warrantyStartDate: String,
    val warrantyEndDate: String,
    val equipmentRemarks: String,
    val deliveryAddressId: String,
    val salesOrderNo: String,
    val warrantyStatus: String,

    val accountId: String,
    val accountName: String,

    val agreementId: String,
    val agreementNumber: String,
    val agreementEnd: String,

    val statusTextEnumId: String,
    val statusTextEnumText: String,

    val product_id: String,
    val product_name: String,

    val isAttach: Boolean,
    val isUpload: Boolean,
    val isLocal: Boolean

)