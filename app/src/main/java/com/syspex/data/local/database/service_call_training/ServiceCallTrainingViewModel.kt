package com.syspex.data.local.database.service_call_training

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import com.syspex.data.local.database.service_report.ServiceReportEntity
import com.syspex.data.model.EquipmentListModel
import com.syspex.data.model.SCListModel
import com.syspex.data.model.SRListModel
import com.syspex.data.remote.GetDataRequest

class ServiceCallTrainingViewModel constructor(application: Application) : AndroidViewModel(application) {
    private val repository : ServiceCallTrainingRepository = ServiceCallTrainingRepository(application)

    fun getAll(): LiveData<List<ServiceCallTrainingEntity>> {
        return repository.getAll()
    }

    fun getByCallId(callId: String): LiveData<List<ServiceCallTrainingEntity>> {
        return repository.getByCallId(callId)
    }

    fun insert(data: ServiceCallTrainingEntity){
        repository.insert(data)
    }

    fun delete(data: ServiceCallTrainingEntity){
        repository.delete(data)
    }

}