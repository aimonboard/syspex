package com.syspex.data.local.database.agreement

import android.app.Application
import androidx.lifecycle.LiveData
import com.syspex.data.local.database.AppDatabase
import com.syspex.data.local.database.GlobalDao
import com.syspex.data.remote.GetDataRequest

class AgreementRepository constructor(application: Application) {

    lateinit var dao : GlobalDao

    init {
        val database = AppDatabase.getInstance(application)
        if (database != null) {
            dao = database.dao()
        }
    }

    fun getAll(searchText: String, filterArray: ArrayList<String>) : LiveData<List<AgreementEntity>> {
        return dao.agreementAll(searchText, filterArray)
    }

    fun search(number: String) : LiveData<List<AgreementEntity>> {
        return dao.agreementSearch(number)
    }

    fun getById(agreementId: String) : LiveData<List<AgreementEntity>> {
        return dao.agreementById(agreementId)
    }

    fun getByEquipmentId(equipmentId: String) : LiveData<List<AgreementEntity>> {
        return dao.agreementByEquipmentId(equipmentId)
    }

    fun getByAccountId(accountId: String) : LiveData<List<AgreementEntity>> {
        return dao.agreementByAccountId(accountId)
    }

    fun insert(entity: AgreementEntity) {
        val thread = object : Thread() {
            override fun run() {
                dao.agreementInsert(entity)
            }
        }
        thread.start()
    }

    fun delete(entity: AgreementEntity) {
        val thread = object : Thread() {
            override fun run() {
                dao.agreementDelete(entity)
            }
        }
        thread.start()
    }

    fun deleteAll() {
        val thread = object : Thread() {
            override fun run() {
                dao.agreementDeleteAll()
            }
        }
        thread.start()
    }

}