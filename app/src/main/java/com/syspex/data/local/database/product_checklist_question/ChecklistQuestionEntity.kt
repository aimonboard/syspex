package com.syspex.data.local.database.product_checklist_question

import androidx.room.Entity
import androidx.room.PrimaryKey
import androidx.room.TypeConverters
import com.syspex.data.local.database.ConverterEquipment
import com.syspex.data.remote.GetDataRequest

@Entity(tableName = "table_product_checklist_question")
data class ChecklistQuestionEntity (
    @PrimaryKey(autoGenerate = false)
    val questionId: String,
    val orderNo: String,
    val sectionTitle: String,
    val question_description: String,
    val defaultRemarks: String,
    val helpText: String,
    val helpImageUrl: String,
    val createdDate: String,
    val createdBy: String,
    val lastModifiedDate: String,
    val lastModifiedBy: String,
    val templateId: String
)