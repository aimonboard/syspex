package com.syspex.data.local.database.account_equipment

import android.app.Application
import androidx.lifecycle.LiveData
import com.syspex.data.local.database.AppDatabase
import com.syspex.data.local.database.GlobalDao
import com.syspex.data.model.EquipmentListModel
import com.syspex.data.model.SCListModel
import com.syspex.data.model.SRListModel
import com.syspex.data.remote.GetDataRequest

class AccountEquipmentRepository constructor(application: Application) {

    lateinit var dao : GlobalDao

    init {
        val database = AppDatabase.getInstance(application)
        if (database != null) {
            dao = database.dao()
        }
    }

    fun getList(textSearch: String, filterArray: ArrayList<String>) : LiveData<List<EquipmentListModel>> {
        return dao.accountEquipmentList(number = textSearch, name = textSearch, filterArray)
    }

    fun search(textSearch: String) : LiveData<List<EquipmentListModel>> {
        return dao.accountEquipmentSearch(number = textSearch, productName = textSearch, accountName = textSearch)
    }

    fun getAll() : LiveData<List<AccountEquipmentEntity>> {
        return dao.accountEquipmentAll()
    }

    fun getByEquipmentId(equipmentId: String) : LiveData<List<AccountEquipmentEntity>> {
        return dao.accountEquipmentByEquipmentId(equipmentId)
    }

    fun getByAgreementId(agreementId: String) : LiveData<List<AccountEquipmentEntity>> {
        return dao.accountEquipmentByAgreementId(agreementId)
    }

    fun getByAccountId(accountId: String) : LiveData<List<EquipmentListModel>> {
        return dao.accountEquipmentByAccountId(accountId)
    }

    fun getByAccount(accountId: String) : LiveData<List<AccountEquipmentEntity>> {
        return dao.accountEquipmentByAccount(accountId)
    }

    fun equipmentAdd(accountId: String, caseId: String, searchText: String) : LiveData<List<AccountEquipmentEntity>> {
        return dao.accountEquipmentAdd(accountId, caseId, searchText)
    }

    fun insert(entity: AccountEquipmentEntity) {
        val thread = object : Thread() {
            override fun run() {
                dao.accountEquipmentInsert(entity)
            }
        }
        thread.start()
    }

    fun updateFlag(id: String, flag: Boolean) {
        val thread = object : Thread() {
            override fun run() {
                dao.accountEquipmentUpdateFlag(id, flag)
            }
        }
        thread.start()
    }

    fun delete(entity: AccountEquipmentEntity) {
        val thread = object : Thread() {
            override fun run() {
                dao.accountEquipmentDelete(entity)
            }
        }
        thread.start()
    }

    fun deleteAll() {
        val thread = object : Thread() {
            override fun run() {
                dao.accountEquipmentDeleteAll()
            }
        }
        thread.start()
    }

}