package com.syspex.data.local.database.service_call_equipment

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import com.syspex.data.local.database.service_report.ServiceReportEntity
import com.syspex.data.model.EquipmentListModel
import com.syspex.data.model.SCListModel
import com.syspex.data.model.SRListModel
import com.syspex.data.remote.GetDataRequest

class ServiceCallEquipmentViewModel constructor(application: Application) : AndroidViewModel(application) {
    private val repository : ServiceCallEquipmentRepository = ServiceCallEquipmentRepository(application)

    fun getAll(): LiveData<List<ServiceCallEquipmentEntity>> {
        return repository.getAll()
    }

    fun checkAttach(callId: String, equipmentId: String): LiveData<List<ServiceCallEquipmentEntity>> {
        return repository.checkAttach(callId, equipmentId)
    }

    fun getEquipmentList(callId: String, isAttachToCall: Boolean): LiveData<List<EquipmentListModel>> {
        return repository.getEquipmentList(callId, isAttachToCall)
    }

    fun getAttachToCall(callId: String, isAttachToCall: Boolean): LiveData<List<ServiceCallEquipmentEntity>> {
        return repository.getAttachToCall(callId, isAttachToCall)
    }

    fun getAttachToReport(callId: String, isAttachToCall: Boolean, isAttachToReport: Boolean): LiveData<List<ServiceCallEquipmentEntity>> {
        return repository.getAttachToReport(callId, isAttachToCall, isAttachToReport)
    }

    fun equipmentAdd(callId: String, reportId: String, searchText: String): LiveData<List<ServiceCallEquipmentEntity>> {
        return repository.equipmentAdd(callId, reportId, searchText)
    }

    fun insert(data: ServiceCallEquipmentEntity){
        repository.insert(data)
    }

    fun updateFlagCall(callId: String, equipmentId: String, isAttachToCall: Boolean){
        repository.updateFlagCall(callId, equipmentId, isAttachToCall)
    }

    fun updateFlagReport(callId: String, equipmentId: String, isAttachToReport: Boolean){
        repository.updateFlagReport(callId, equipmentId, isAttachToReport)
    }

    fun delete(data: ServiceCallEquipmentEntity){
        repository.delete(data)
    }

    fun deleteById(callEquipmentId: String){
        repository.deleteById(callEquipmentId)
    }

}