package com.syspex.data.local.database

import androidx.room.TypeConverter
import java.text.SimpleDateFormat
import java.util.*

class DateConverter {

    val sdf = SimpleDateFormat("dd-MM-yyyy HH:mm", Locale.getDefault())

    @TypeConverter
    fun toDateTime(value: String?): Date? {
        return value?.let {
            return sdf.parse(value)
        }
    }

    @TypeConverter
    fun fromDateTime(date: Date?): String? {
        return date?.let {
            return date.toString()
        }
    }
}