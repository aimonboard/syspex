package com.syspex.data.local.database.service_call_handover

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import com.syspex.data.local.database.service_report.ServiceReportEntity
import com.syspex.data.model.EquipmentListModel
import com.syspex.data.model.SCListModel
import com.syspex.data.model.SRListModel
import com.syspex.data.remote.GetDataRequest

class ServiceCallHandoverViewModel constructor(application: Application) : AndroidViewModel(application) {
    private val repository : ServiceCallHandoverRepository = ServiceCallHandoverRepository(application)

    fun getAll(): LiveData<List<ServiceCallHandoverEntity>> {
        return repository.getAll()
    }

    fun getByCallId(callId: String): LiveData<List<ServiceCallHandoverEntity>> {
        return repository.getByCallId(callId)
    }

    fun insert(data: ServiceCallHandoverEntity){
        repository.insert(data)
    }

    fun delete(data: ServiceCallHandoverEntity){
        repository.delete(data)
    }

}