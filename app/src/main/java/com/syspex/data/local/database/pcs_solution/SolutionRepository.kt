package com.syspex.data.local.database.pcs_solution

import android.app.Application
import androidx.lifecycle.LiveData
import com.syspex.data.local.database.AppDatabase
import com.syspex.data.local.database.GlobalDao
import com.syspex.data.local.database.service_call_equipment.ServiceCallEquipmentEntity
import com.syspex.data.model.SCListModel
import com.syspex.data.model.SRListModel
import com.syspex.data.remote.GetDataRequest

class SolutionRepository constructor(application: Application) {

    lateinit var dao : GlobalDao

    init {
        val database = AppDatabase.getInstance(application)
        if (database != null) {
            dao = database.dao()
        }
    }

    fun getByReportEquipment(reportEquipmentId: String) : LiveData<List<SolutionEntity>> {
        return dao.solutionByReportEquipment(reportEquipmentId)
    }

    fun getBySolutionId(solutionId: String) : LiveData<List<SolutionEntity>> {
        return dao.solutionBySolutionId(solutionId)
    }

    fun insert(entity: SolutionEntity) {
        val thread = object : Thread() {
            override fun run() {
                dao.solutionInsert(entity)
            }
        }
        thread.start()
    }

    fun update(entity: SolutionEntity){
        val thread = object : Thread() {
            override fun run() {
                dao.solutionUpdate(entity)
            }
        }
        thread.start()
    }

    fun deleteByReportEquipmentId(reportEquipmentId: String) {
        val thread = object : Thread() {
            override fun run() {
                dao.solutionByReportEquipment(reportEquipmentId)
            }
        }
        thread.start()
    }

    fun delete(entity: SolutionEntity) {
        val thread = object : Thread() {
            override fun run() {
                dao.solutionDelete(entity)
            }
        }
        thread.start()
    }

    fun deleteAll() {
        val thread = object : Thread() {
            override fun run() {
                dao.solutionDeleteAll()
            }
        }
        thread.start()
    }
}