package com.syspex.data.local.database.product_sparepart

import android.app.Application
import androidx.lifecycle.LiveData
import com.syspex.data.local.database.AppDatabase
import com.syspex.data.local.database.GlobalDao
import com.syspex.data.model.SCListModel
import com.syspex.data.model.SRListModel
import com.syspex.data.remote.GetDataRequest

class SparepartRepository constructor(application: Application) {

    lateinit var dao : GlobalDao

    init {
        val database = AppDatabase.getInstance(application)
        if (database != null) {
            dao = database.dao()
        }
    }

    fun getByProductGroupId(productGroupId: String) : LiveData<List<SparepartEntity>> {
        return dao.sparepartByProductGroupId(productGroupId)
    }

    fun insert(entity: SparepartEntity) {
        val thread = object : Thread() {
            override fun run() {
                dao.sparepartInsert(entity)
            }
        }
        thread.start()
    }

}