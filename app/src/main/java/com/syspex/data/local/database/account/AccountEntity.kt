package com.syspex.data.local.database.account

import androidx.room.Entity
import androidx.room.PrimaryKey
import androidx.room.TypeConverters
import com.syspex.data.local.database.ConverterAttachment
import com.syspex.data.local.database.ConverterEquipment
import com.syspex.data.remote.GetDataRequest

@Entity(tableName = "table_account")
data class AccountEntity (
    @PrimaryKey (autoGenerate = false)
    val accountId: String,

    val caseId: String,
    val agreementId: String,
    val callId: String,

    val account_code: String,
    val account_name: String,
    val active: String,
    val created_date: String,
    val created_by: String,
    val last_modified_date: String,
    val last_modified_by: String,
    val internal_memo: String,
    val created_user: String,
    val modified_user: String,

    val sales_id: String,
    val sales_name: String,
    val sales_email: String,
    val sales_phone: String,

    val region_id: String,
    val region_name: String,

    @TypeConverters(AccountConverterContact::class)
    val contact: List<GetDataRequest.Data.ServiceCase.Account.Contact?>? = null,

    @TypeConverters(AccountConverterAddress::class)
    val address: List<GetDataRequest.Data.ServiceCase.Account.Addres?>? = null
)