package com.syspex.data.local.database.service_report_equipment

import android.app.Application
import androidx.lifecycle.LiveData
import com.syspex.data.local.database.AppDatabase
import com.syspex.data.local.database.GlobalDao
import com.syspex.data.model.SREquipmentJointModel

class ServiceReportEquipmentRepository constructor(application: Application) {

    lateinit var dao : GlobalDao

    init {
        val database = AppDatabase.getInstance(application)
        if (database != null) {
            dao = database.dao()
        }
    }

    fun checkAttach(callId: String, equipmentId: String, isAttach: Boolean) : LiveData<List<ServiceReportEquipmentEntity>> {
        return dao.reportEquipmentCheckAttach(callId, equipmentId, isAttach)
    }

    fun getByReportId(reportId: String) : LiveData<List<ServiceReportEquipmentEntity>> {
        return dao.reportEquipmentByReportId(reportId)
    }

    fun getByReportEquipmentId(reportEquipmentId: String) : LiveData<List<ServiceReportEquipmentEntity>> {
        return dao.reportEquipmentByReportEquipmentId(reportEquipmentId)
    }

    fun getEquipmentAllByCallId(callId: String, isAttach: Boolean) : LiveData<List<SREquipmentJointModel>> {
        return dao.reportEquipmentAllByCallId(callId, isAttach)
    }

    fun getEquipmentDetail(reportId: String) : LiveData<List<SREquipmentJointModel>> {
        return dao.reportEquipmentDetail(reportId)
    }

    fun insert(entity: ServiceReportEquipmentEntity) {
        val thread = object : Thread() {
            override fun run() {
                dao.reportEquipmentInsert(entity)
            }
        }
        thread.start()
    }

    fun editTemplateId(reportEquipmentId: String, templateId: String) {
        val thread = object : Thread() {
            override fun run() {
                dao.reportEquipmentEditTemplateId(reportEquipmentId, templateId)
            }
        }
        thread.start()
    }

    fun edit(reportEquipmentId: String, statusAfterServiceEnumId: String, statusAfterServiceEnumName: String, statusAfterServiceRemarks: String, problem: String, isUpload: Boolean) {
        val thread = object : Thread() {
            override fun run() {
                dao.reportEquipmentEdit(reportEquipmentId, statusAfterServiceEnumId, statusAfterServiceEnumName, statusAfterServiceRemarks, problem, isUpload)
            }
        }
        thread.start()
    }

    fun updateFlag(reportEquipmentId: String, isUpload: Boolean) {
        val thread = object : Thread() {
            override fun run() {
                dao.reportEquipmentUpdateFlag(reportEquipmentId, isUpload)
            }
        }
        thread.start()
    }

    fun delete(entity: ServiceReportEquipmentEntity) {
        val thread = object : Thread() {
            override fun run() {
                dao.reportEquipmentDelete(entity)
            }
        }
        thread.start()
    }

    fun deleteById(reportEquipmentId: String) {
        val thread = object : Thread() {
            override fun run() {
                dao.reportEquipmentDeleteById(reportEquipmentId)
            }
        }
        thread.start()
    }

    fun deleteByReportId(reportId: String) {
        val thread = object : Thread() {
            override fun run() {
                dao.reportEquipmentDeleteByReportId(reportId)
            }
        }
        thread.start()
    }


}