package com.syspex.data.local.database.global_color

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "table_color")
data class ColorEntity (
    @PrimaryKey(autoGenerate = true)
    val colorId: Int,
    val colorTable: String,
    val colorBackground: String,
    val colorStatusLabel: String,
    val colorStatusLabelText: String,
    val colorStatus: String
)