package com.syspex.data.local.database

import androidx.lifecycle.LiveData
import androidx.room.*
import com.syspex.data.local.database.account.AccountEntity
import com.syspex.data.local.database.account_address.AccountAddressEntity
import com.syspex.data.local.database.account_attachment.AccountAttachmentEntity
import com.syspex.data.local.database.account_contact.AccountContactEntity
import com.syspex.data.local.database.account_equipment.AccountEquipmentEntity
import com.syspex.data.local.database.activity.ActivityEntity
import com.syspex.data.local.database.activity_type.ActivityTypeEntity
import com.syspex.data.local.database.agreement.AgreementEntity
import com.syspex.data.local.database.agreement_equipment.AgreementEquipmentEntity
import com.syspex.data.local.database.chargeable_type.ChargeableEntity
import com.syspex.data.local.database.email.EmailEntity
import com.syspex.data.local.database.file.FileEntity
import com.syspex.data.local.database.global_color.ColorEntity
import com.syspex.data.local.database.global_enum.EnumEntity
import com.syspex.data.local.database.pcs_problem.ProblemEntity
import com.syspex.data.local.database.pcs_solution.SolutionEntity
import com.syspex.data.local.database.product_group.ProductEntity
import com.syspex.data.local.database.product_checklist_question.ChecklistQuestionEntity
import com.syspex.data.local.database.product_checklist_template.ChecklistTemplateEntity
import com.syspex.data.local.database.product_document.ProductDocumentEntity
import com.syspex.data.local.database.product_group_item.ProductGroupItemEntity
import com.syspex.data.local.database.product_sparepart.SparepartEntity
import com.syspex.data.local.database.service_call.ServiceCallEntity
import com.syspex.data.local.database.service_call_completion.ServiceCallCompletionEntity
import com.syspex.data.local.database.service_call_equipment.ServiceCallEquipmentEntity
import com.syspex.data.local.database.service_call_handover.ServiceCallHandoverEntity
import com.syspex.data.local.database.service_call_training.ServiceCallTrainingEntity
import com.syspex.data.local.database.service_case.ServiceCaseEntity
import com.syspex.data.local.database.service_case_equipment.ServiceCaseEquipmentEntity
import com.syspex.data.local.database.service_report.ServiceReportEntity
import com.syspex.data.local.database.service_report_equipment.ServiceReportEquipmentEntity
import com.syspex.data.local.database.service_report_equipment_checklist.ServiceReportEquipmentChecklistEntity
import com.syspex.data.local.database.service_report_equipment_checklist_question.ServiceReportEquipmentChecklistQuestionEntity
import com.syspex.data.local.database.service_report_equipment_checklist_template.ServiceReportEquipmentChecklistTemplateEntity
import com.syspex.data.local.database.service_report_sparepart.ServiceReportSparepartEntity
import com.syspex.data.local.database.service_report_time_tracking.ServiceReportTimeTrackingEntity
import com.syspex.data.model.*
import com.syspex.data.remote.GetDataRequest

@Dao
interface GlobalDao {
    // --------------------------------------------------------------------------------------------- Enum
    @Query("SELECT * from table_enum WHERE enum_table=:tableName ORDER BY enum_name ASC")
    fun enumByTableName(tableName: String): LiveData<List<EnumEntity>>

    @Query("SELECT * from table_enum WHERE enum_table=:tableName AND enum_column=:colummName ORDER BY enum_name ASC")
    fun enumByTableAndCoulumn(tableName: String, colummName: String): LiveData<List<EnumEntity>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun enumInsert(data: EnumEntity)

    // ============================================================================================= Color
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun colorInsert(data: ColorEntity)

    // --------------------------------------------------------------------------------------------- Activity
    @Query("SELECT * from table_activity WHERE activitySourceTable == 'Call' ORDER BY strftime('%Y-%m-%d %H:%M:%S', createdDate) DESC")
    fun activityAll(): LiveData<List<ActivityEntity>>

    @Query("SELECT * from table_activity WHERE activityId=:activityId ORDER BY strftime('%Y-%m-%d %H:%M:%S', createdDate) DESC")
    fun activityById(activityId: String): LiveData<List<ActivityEntity>>

    @Query("SELECT * from table_activity WHERE caseId=:caseId ORDER BY strftime('%Y-%m-%d %H:%M:%S', createdDate) DESC")
    fun activityByCaseId(caseId: String): LiveData<List<ActivityEntity>>

    @Query("SELECT * from table_activity WHERE accountId=:accountId ORDER BY strftime('%Y-%m-%d %H:%M:%S', createdDate) DESC")
    fun activityByAccountId(accountId: String): LiveData<List<ActivityEntity>>

    @Query("SELECT * from table_activity WHERE serviceCallId=:callId ORDER BY strftime('%Y-%m-%d %H:%M:%S', createdDate) DESC")
    fun activityByCallId(callId: String): LiveData<List<ActivityEntity>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun activityInsert(data: ActivityEntity)

    @Query("DELETE FROM table_activity WHERE isLocal=:isLocal")
    fun activityDeleteLocal(isLocal: Boolean)

    @Query("SELECT * from table_activity WHERE isLocal=:isLocal")
    fun activityUpload(isLocal: Boolean): List<ActivityEntity>

    // --------------------------------------------------------------------------------------------- Activity Type
    @Query("SELECT * from table_activity_type")
    fun activityTypeAll(): LiveData<List<ActivityTypeEntity>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun activityTypeInsert(data: ActivityTypeEntity)

    @Query("DELETE FROM table_activity_type")
    fun activityTypeDeleteAll()






    // --------------------------------------------------------------------------------------------- Service Case
    @Query("SELECT * from table_service_case LEFT JOIN table_color ON table_service_case.caseStatusEnumName = table_color.colorStatus AND table_color.colorTable = 'Case' WHERE table_service_case.caseStatusEnumId IN (:filterArray) AND caseNumber LIKE '%' || :searchText || '%' ORDER BY datetime(createdDate) DESC LIMIT 100")
    fun caseAll(searchText: String, filterArray: ArrayList<String>): LiveData<List<ServiceCaseEntity>>

    @Query("SELECT * from table_service_case LEFT JOIN table_color ON table_service_case.caseStatusEnumName = table_color.colorStatus AND table_color.colorTable = 'Case' WHERE caseNumber LIKE '%' || :number || '%' OR accountName LIKE '%' || :accountName || '%' OR subject LIKE '%' || :subject || '%' ORDER BY strftime('%Y-%m-%d %H:%M:%S', createdDate) DESC LIMIT 3")
    fun caseSearch(number: String, accountName: String, subject: String): LiveData<List<ServiceCaseEntity>>

    @Query("SELECT * from table_service_case LEFT JOIN table_color ON table_service_case.caseStatusEnumName = table_color.colorStatus AND table_color.colorTable = 'Case' WHERE caseId =:caseId")
    fun caseByCaseId(caseId: String): LiveData<List<ServiceCaseEntity>>

    @Query("SELECT * from table_service_case LEFT JOIN table_color ON table_service_case.caseStatusEnumName = table_color.colorStatus AND table_color.colorTable = 'Case' WHERE accountId =:accountId")
    fun caseByAccountId(accountId: String): LiveData<List<ServiceCaseEntity>>

    @Query("SELECT * from table_service_case LEFT JOIN table_color ON table_service_case.caseStatusEnumName = table_color.colorStatus AND table_color.colorTable = 'Case' WHERE agreementId =:agreementId")
    fun caseByAgreementId(agreementId: String): LiveData<List<ServiceCaseEntity>>

    @Query("SELECT * from table_service_case LEFT JOIN table_service_case_equipment ON table_service_case.caseId = table_service_case_equipment.caseId LEFT JOIN table_color ON table_service_case.caseStatusEnumName = table_color.colorStatus AND table_color.colorTable = 'Case' WHERE table_service_case_equipment.equipmentId =:equipmentId")
    fun caseEquipmentRelated(equipmentId: String): LiveData<List<ServiceCaseEntity>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun caseInsert(data: ServiceCaseEntity) : Long

    @Update
    fun caseUpdate(data: ServiceCaseEntity) : Int

    @Delete
    fun caseDelete(data: ServiceCaseEntity)

    @Query("DELETE FROM table_service_case")
    fun caseDeleteAll()

    @Query("UPDATE table_service_case SET isUpload=:isUplaod WHERE caseId=:caseId")
    fun caseUpdateFlag(caseId: String, isUplaod: Boolean)

    @Query("SELECT table_service_case.caseId from table_service_case WHERE isUpload=:isUplaod")
    fun caseUpload(isUplaod: Boolean): List<String>

    // --------------------------------------------------------------------------------------------- Service Case Equipment
    @Query("SELECT * from table_service_case_equipment")
    fun caseEquipmentall(): LiveData<List<ServiceCaseEquipmentEntity>>

    @Query("SELECT * FROM table_service_case_equipment WHERE caseId=:caseId AND equipmentId=:equipmentId")
    fun caseEquipmentCheckAttach(caseId: String, equipmentId: String): LiveData<List<ServiceCaseEquipmentEntity>>

    @Query("SELECT " +
            "table_service_case_equipment.equipmentId," +
            "table_service_case_equipment.serialNo," +
            "table_service_case_equipment.statusTextEnumId," +
            "table_service_case_equipment.statusTextEnumText," +
            "table_service_case_equipment.product_name," +
            "table_service_case_equipment.accountName," +
            "table_service_case_equipment.warrantyEndDate," +
            "table_service_case_equipment.agreementEnd," +
            "table_service_report.reportLocalId," +
            "table_service_report.serviceReportNumber," +
            "table_service_report.createdDate," +
            "table_service_report.reportStatusEnumId," +
            "table_service_report.reportStatusEnumText," +
            "table_service_report.servicedByUserName," +
            "table_service_call.callTypeEnumId," +
            "table_service_call.callTypeEnumText " +
            "FROM table_service_case_equipment " +
            "LEFT JOIN table_service_report_equipment ON table_service_case_equipment.equipmentId = table_service_report_equipment.equipmentId " +
            "LEFT JOIN table_service_report ON table_service_report_equipment.reportId = table_service_report.reportLocalId " +
            "LEFT JOIN table_service_call ON table_service_report.callId = table_service_call.serviceCallId " +
            "WHERE table_service_case_equipment.caseId =:caseId AND table_service_case_equipment.isAttach=:isAttach " +
            "GROUP BY table_service_case_equipment.equipmentId " +
            "ORDER BY strftime('%Y-%m-%d %H:%M:%S', table_service_case_equipment.createdDate) DESC")
    fun caseEquipmentByCaseId(caseId: String, isAttach: Boolean): LiveData<List<EquipmentListModel>>

    @Query("SELECT * from table_service_case_equipment WHERE table_service_case_equipment.caseId=:caseId AND table_service_case_equipment.equipmentId NOT IN (SELECT DISTINCT table_service_call_equipment.equipmentId FROM table_service_call_equipment WHERE table_service_call_equipment.callId=:callId) AND serialNo LIKE '%' || :searchText || '%' ORDER BY table_service_case_equipment.serialNo DESC")
    fun caseEquipmentAdd(caseId: String, callId:String, searchText: String): LiveData<List<ServiceCaseEquipmentEntity>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun caseEquipmentInsert(data: ServiceCaseEquipmentEntity)

    @Query("UPDATE table_service_case_equipment SET isAttach=:data WHERE equipmentId=:id")
    fun caseEquipmentUpdateFlag(id: String, data: Boolean)

    @Delete
    fun caseEquipmentDelete(data: ServiceCaseEquipmentEntity)

    @Query("Delete FROM table_service_case_equipment WHERE isLocal=:isLocal")
    fun caseEquipmentDeleteLocal(isLocal: Boolean)

    @Query("DELETE FROM table_service_case_equipment")
    fun  caseEquipmentDeleteAll()

    @Query("SELECT table_service_case_equipment.equipmentId from table_service_case_equipment WHERE caseId=:caseId")
    fun caseEquipmentUpload(caseId: String): List<String>

    @Query("Delete FROM table_service_case_equipment WHERE caseId=:caseId")
    fun caseEquipmentDeleteByCaseId(caseId: String)

    // --------------------------------------------------------------------------------------------- Account
    @Query("SELECT " +
            "table_account.accountId, " +
            "table_account.account_code, " +
            "table_account.account_name, " +
            "table_account.address, " +
            "(SELECT table_service_call.startDate FROM table_service_call WHERE accountId = table_account.accountId ORDER BY strftime('%Y-%m-%d %H:%M:%S', startdate) DESC LIMIT 1) AS startDate, " +
            "(SELECT table_service_call.callStatusEnumText FROM table_service_call WHERE accountId = table_account.accountId ORDER BY strftime('%Y-%m-%d %H:%M:%S', startdate) DESC LIMIT 1) AS callStatusEnumText, " +
            "(SELECT table_service_call.callTypeEnumText FROM table_service_call WHERE accountId = table_account.accountId ORDER BY strftime('%Y-%m-%d %H:%M:%S', startdate) DESC LIMIT 1) AS callTypeEnumText, " +
            "(SELECT table_service_call.equipment FROM table_service_call WHERE accountId = table_account.accountId ORDER BY strftime('%Y-%m-%d %H:%M:%S', startdate) DESC LIMIT 1) AS equipment " +
            "FROM table_account " +
            "WHERE account_code LIKE '%' || :code || '%' OR account_name LIKE '%' || :accountName || '%' ORDER BY strftime('%Y-%m-%d %H:%M:%S', created_date) DESC LIMIT 3")
    fun accountSearch(code: String, accountName: String): LiveData<List<AccountList>>

    @Query("SELECT " +
            "table_account.accountId, " +
            "table_account.account_code, " +
            "table_account.account_name, " +
            "table_account.address, " +
            "(SELECT table_service_call.startDate FROM table_service_call WHERE accountId = table_account.accountId ORDER BY strftime('%Y-%m-%d %H:%M:%S', startdate) DESC LIMIT 1) AS startDate, " +
            "(SELECT table_service_call.callStatusEnumText FROM table_service_call WHERE accountId = table_account.accountId ORDER BY strftime('%Y-%m-%d %H:%M:%S', startdate) DESC LIMIT 1) AS callStatusEnumText, " +
            "(SELECT table_service_call.callTypeEnumText FROM table_service_call WHERE accountId = table_account.accountId ORDER BY strftime('%Y-%m-%d %H:%M:%S', startdate) DESC LIMIT 1) AS callTypeEnumText, " +
            "(SELECT table_service_call.equipment FROM table_service_call WHERE accountId = table_account.accountId ORDER BY strftime('%Y-%m-%d %H:%M:%S', startdate) DESC LIMIT 1) AS equipment " +
            "FROM table_account " +
            "WHERE table_account.account_name LIKE '%' || :searchText || '%' " +
            "ORDER BY strftime('%Y-%m-%d %H:%M:%S', created_date) DESC LIMIT 100")
    fun accountList(searchText: String): LiveData<List<AccountList>>

    @Query("SELECT * from table_account")
    fun accountAll(): LiveData<List<AccountEntity>>

    // For batch uplopad
    @Query("SELECT * from table_account")
    fun accountAllOnce(): List<AccountEntity>

    @Query("SELECT * from table_account WHERE accountId =:accountId")
    fun accountByAccountId(accountId: String): LiveData<List<AccountEntity>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun accountInsert(data: AccountEntity)

    @Update(onConflict = OnConflictStrategy.REPLACE)
    fun accountUpdate(data: AccountEntity)

    @Delete
    fun accountDelete(data: AccountEntity)

    @Query("DELETE FROM table_account")
    fun accountDeleteAll()

    // --------------------------------------------------------------------------------------------- Account Contact
    @Query("SELECT * from table_account_contact WHERE account_name LIKE '%' || :accountName || '%' OR contact_person_name LIKE '%' || :contactName || '%' ORDER BY account_name ASC LIMIT 100")
    fun accountContactSearch(accountName: String, contactName: String): LiveData<List<AccountContactEntity>>

    @Query("SELECT * from table_account_contact WHERE account_id=:accountId AND contact_person_name LIKE '%' || :contactName || '%' ORDER BY contact_person_name ASC")
    fun accountContactSearchInAccountDetail(accountId: String, contactName: String): LiveData<List<AccountContactEntity>>

    @Query("SELECT * from table_account_contact")
    fun accountContactAll(): LiveData<List<AccountContactEntity>>

    @Query("SELECT * from table_account_contact WHERE account_contact_id =:contactId")
    fun accountContactById(contactId: String): LiveData<List<AccountContactEntity>>

    @Query("SELECT * from table_account_contact WHERE account_id =:accountId")
    fun accountContactByAccountId(accountId: String): LiveData<List<AccountContactEntity>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun accountContactInsert(data: AccountContactEntity)

    @Update(onConflict = OnConflictStrategy.REPLACE)
    fun accountContactUpdate(data: AccountContactEntity)

    @Query("DELETE from table_account_contact WHERE account_id=:accountId")
    fun accountContactDeleteByAccountId(accountId: String)

    // --------------------------------------------------------------------------------------------- Account Address
    @Query("SELECT * from table_account_address")
    fun accountAddressAll(): LiveData<List<AccountAddressEntity>>

    @Query("SELECT * from table_account_address WHERE account_id =:accountId")
    fun accountAddressByAccountId(accountId: String): LiveData<List<AccountAddressEntity>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun accountAddressInsert(data: AccountAddressEntity)

    @Update(onConflict = OnConflictStrategy.REPLACE)
    fun accountAddressUpdate(data: AccountAddressEntity)

    @Query("DELETE from table_account_address WHERE account_id=:accountId")
    fun accountAddressDeleteByAccountId(accountId: String)

    // --------------------------------------------------------------------------------------------- Account Attachment
    @Query("SELECT * from table_account_attachment")
    fun accountAttachmentAll(): LiveData<List<AccountAttachmentEntity>>

    @Query("SELECT * from table_account_attachment WHERE accountId=:accountId")
    fun accountAttachment(accountId: String): LiveData<List<AccountAttachmentEntity>>

    @Query("SELECT * from table_account_attachment WHERE accountId =:accountId AND attachmentSourceId=:sourceId AND attachmentSourceTable=:sourceTable")
    fun accountAttachmentSection(accountId: String, sourceId: String, sourceTable: String): LiveData<List<AccountAttachmentEntity>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun accountAttachmentInsert(data: AccountAttachmentEntity)

    @Update(onConflict = OnConflictStrategy.REPLACE)
    fun accountAttachmentUpdate(data: AccountAttachmentEntity)

    @Query("DELETE from table_account_attachment WHERE accountId=:accountId")
    fun accountAttachmentDeleteByAccountId(accountId: String)

    // --------------------------------------------------------------------------------------------- Account Equipment
    @Query("SELECT " +
            "table_account_equipment.equipmentId," +
            "table_account_equipment.serialNo," +
            "table_account_equipment.statusTextEnumId," +
            "table_account_equipment.statusTextEnumText," +
            "table_account_equipment.product_name," +
            "table_account_equipment.accountName," +
            "table_account_equipment.warrantyEndDate," +
            "table_account_equipment.agreementEnd," +
            "table_service_report.reportLocalId," +
            "table_service_report.serviceReportNumber," +
            "table_service_report.createdDate," +
            "table_service_report.reportStatusEnumId," +
            "table_service_report.reportStatusEnumText," +
            "table_service_report.servicedByUserName," +
            "table_service_call.callTypeEnumId," +
            "table_service_call.callTypeEnumText " +
            "FROM table_account_equipment " +
            "LEFT JOIN table_service_report_equipment ON table_account_equipment.equipmentId = table_service_report_equipment.equipmentId " +
            "LEFT JOIN table_service_report ON table_service_report_equipment.reportId = table_service_report.reportLocalId " +
            "LEFT JOIN table_service_call ON table_service_report.callId = table_service_call.serviceCallId " +
            "WHERE table_account_equipment.statusTextEnumId IN (:filterArray) AND " +
            "table_account_equipment.serialNo LIKE '%' || :number || '%' OR " +
            "table_account_equipment.product_name LIKE '%' || :name || '%' " +
            "GROUP BY table_account_equipment.equipmentId LIMIT 100")
    fun accountEquipmentList(number: String, name: String, filterArray: ArrayList<String>): LiveData<List<EquipmentListModel>>

    @Query("SELECT " +
            "table_account_equipment.equipmentId," +
            "table_account_equipment.serialNo," +
            "table_account_equipment.statusTextEnumId," +
            "table_account_equipment.statusTextEnumText," +
            "table_account_equipment.product_name," +
            "table_account_equipment.accountName," +
            "table_account_equipment.warrantyEndDate," +
            "table_account_equipment.agreementEnd," +
            "table_service_report.reportLocalId," +
            "table_service_report.serviceReportNumber," +
            "table_service_report.createdDate," +
            "table_service_report.reportStatusEnumId," +
            "table_service_report.reportStatusEnumText," +
            "table_service_report.servicedByUserName," +
            "table_service_call.callTypeEnumId," +
            "table_service_call.callTypeEnumText " +
            "FROM table_account_equipment " +
            "LEFT JOIN table_service_report_equipment ON table_account_equipment.equipmentId = table_service_report_equipment.equipmentId " +
            "LEFT JOIN table_service_report ON table_service_report_equipment.reportId = table_service_report.reportLocalId " +
            "LEFT JOIN table_service_call ON table_service_report.callId = table_service_call.serviceCallId " +
            "WHERE table_account_equipment.accountId =:accountId " +
            "GROUP BY table_account_equipment.equipmentId")
    fun accountEquipmentByAccountId(accountId: String): LiveData<List<EquipmentListModel>>

    @Query("SELECT * from table_account_equipment WHERE accountId=:accountId")
    fun accountEquipmentByAccount(accountId: String): LiveData<List<AccountEquipmentEntity>>

    @Query("SELECT " +
            "table_account_equipment.equipmentId," +
            "table_account_equipment.serialNo," +
            "table_account_equipment.statusTextEnumId," +
            "table_account_equipment.statusTextEnumText," +
            "table_account_equipment.product_name," +
            "table_account_equipment.accountName," +
            "table_account_equipment.warrantyEndDate," +
            "table_account_equipment.agreementEnd," +
            "table_service_report.reportLocalId," +
            "table_service_report.serviceReportNumber," +
            "table_service_report.createdDate," +
            "table_service_report.reportStatusEnumId," +
            "table_service_report.reportStatusEnumText," +
            "table_service_report.servicedByUserName," +
            "table_service_call.callTypeEnumId," +
            "table_service_call.callTypeEnumText " +
            "FROM table_account_equipment " +
            "LEFT JOIN table_service_report_equipment ON table_account_equipment.equipmentId = table_service_report_equipment.equipmentId " +
            "LEFT JOIN table_service_report ON table_service_report_equipment.reportId = table_service_report.reportLocalId " +
            "LEFT JOIN table_service_call ON table_service_report.callId = table_service_call.serviceCallId " +
            "WHERE " +
            "table_account_equipment.serialNo LIKE '%' || :number || '%' OR " +
            "table_account_equipment.product_name LIKE '%' || :productName || '%' OR " +
            "table_account_equipment.accountName LIKE '%' || :accountName || '%' " +
            "GROUP BY table_account_equipment.equipmentId LIMIT 3")
    fun accountEquipmentSearch(number: String, productName: String, accountName: String): LiveData<List<EquipmentListModel>>

    @Query("SELECT * from table_account_equipment ORDER BY strftime('%Y-%m-%d %H:%M:%S', equipmentId) DESC")
    fun accountEquipmentAll(): LiveData<List<AccountEquipmentEntity>>

    @Query("SELECT * from table_account_equipment WHERE equipmentId=:equipmentId")
    fun accountEquipmentByEquipmentId(equipmentId: String): LiveData<List<AccountEquipmentEntity>>

    @Query("SELECT * from table_account_equipment WHERE agreementId=:agreementId")
    fun accountEquipmentByAgreementId(agreementId: String): LiveData<List<AccountEquipmentEntity>>

    @Query("SELECT * from table_account_equipment WHERE table_account_equipment.accountId=:accountId AND table_account_equipment.equipmentId NOT IN (SELECT DISTINCT table_service_case_equipment.equipmentId FROM table_service_case_equipment WHERE table_service_case_equipment.caseId=:caseId) AND serialNo LIKE '%' || :searchText || '%' ORDER BY table_account_equipment.serialNo DESC")
    fun accountEquipmentAdd(accountId: String, caseId:String, searchText: String): LiveData<List<AccountEquipmentEntity>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun accountEquipmentInsert(data: AccountEquipmentEntity)

    @Query("UPDATE table_account_equipment SET isAttach=:flag WHERE equipmentId=:id")
    fun accountEquipmentUpdateFlag(id: String, flag: Boolean)

    @Delete
    fun accountEquipmentDelete(data: AccountEquipmentEntity)

    @Query("DELETE FROM table_account_equipment")
    fun  accountEquipmentDeleteAll()

    // --------------------------------------------------------------------------------------------- Product Group Item
    @Query("SELECT * from table_product_group_item")
    fun productGroupItemAll(): LiveData<List<ProductGroupItemEntity>>

    @Query("SELECT * from table_product_group_item WHERE product_id=:productId")
    fun productGroupItemByProductId(productId: String): LiveData<List<ProductGroupItemEntity>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun productGroupItemInsert(data: ProductGroupItemEntity)

    // --------------------------------------------------------------------------------------------- Product Group
    @Query("SELECT * from table_product WHERE product_group_name LIKE '%' || :textSearch || '%' LIMIT 100")
    fun productGroupAll(textSearch: String): LiveData<List<ProductEntity>>

    @Query("SELECT * from table_product LEFT JOIN table_product_group_item ON table_product.product_group_id = table_product_group_item.product_group_id WHERE table_product_group_item.product_id=:productId")
    fun productGroupJoinProduct(productId: String): LiveData<List<ProductEntity>>

    @Query("SELECT * from table_product WHERE product_group_id=:productGroupId")
    fun productGroupById(productGroupId: String): LiveData<List<ProductEntity>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun productGroupInsert(data: ProductEntity)

    @Delete
    fun productGroupDelete(data: ProductEntity)

    @Query("DELETE FROM table_product")
    fun  productGroupDeleteAll()

    // --------------------------------------------------------------------------------------------- Product Document
    @Query("SELECT * from table_product_document WHERE product_group_id=:productGroupId")
    fun documentByProductGroupId(productGroupId: String): LiveData<List<ProductDocumentEntity>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun documentInsert(data: ProductDocumentEntity)

    // --------------------------------------------------------------------------------------------- SparePart
    @Query("SELECT * from table_product_sparepart WHERE product_group_id=:productGroupId")
    fun sparepartByProductGroupId(productGroupId: String): LiveData<List<SparepartEntity>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun sparepartInsert(data: SparepartEntity)

    // --------------------------------------------------------------------------------------------- Checklist Template
    @Query("SELECT * FROM table_product_checklist_template")
    fun  checklistAll(): LiveData<List<ChecklistTemplateEntity>>

    @Query("SELECT * from table_product_checklist_template WHERE productGroupId=:productGroupId")
    fun checklistTemplateByProductGroupId(productGroupId: String): LiveData<List<ChecklistTemplateEntity>>

    @Query("SELECT * from table_product_checklist_template WHERE productGroupId=:productGroupId AND checklistTypeTextEnumName=:checklistTypeText")
    fun checklistTemplateByProductGroupIdAndType(productGroupId: String, checklistTypeText: String): LiveData<List<ChecklistTemplateEntity>>

    @Query("SELECT * from table_product_checklist_template LEFT JOIN table_product_checklist_question ON table_product_checklist_template.productGroupChecklistTemplateId = table_product_checklist_question.templateId  WHERE productGroupId=:productGroupId AND checklistTypeTextEnumName=:checklistTypeText")
    fun checklistTemplateJointQuestion(productGroupId: String, checklistTypeText: String): LiveData<List<ChecklistJointMasterModel>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun checklistTemplateInsert(data: ChecklistTemplateEntity)

    @Query("DELETE FROM table_product_checklist_template")
    fun  checklistTemplateDeleteAll()

    // --------------------------------------------------------------------------------------------- Checklist Question
    @Query("SELECT * FROM table_product_checklist_question WHERE templateId=:templateId")
    fun checklistQuestionByTemplate(templateId: String): LiveData<List<ChecklistQuestionEntity>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun checklistQuestionInsert(data: ChecklistQuestionEntity)

    @Query("DELETE FROM table_product_checklist_question")
    fun  checklistQuestionByDeleteAll()

    // --------------------------------------------------------------------------------------------- Agreement
    @Query("SELECT * from table_agreement WHERE agreementStatusEnumId IN (:filterArray) AND agreementNumber LIKE '%' || :searchText || '%' LIMIT 100")
    fun agreementAll(searchText: String, filterArray: ArrayList<String>): LiveData<List<AgreementEntity>>

    @Query("SELECT * from table_agreement WHERE agreementNumber LIKE '%' || :number || '%' ORDER BY strftime('%Y-%m-%d %H:%M:%S', createdDate) DESC LIMIT 3")
    fun agreementSearch(number: String): LiveData<List<AgreementEntity>>

    @Query("SELECT * from table_agreement WHERE agreementId =:agreementId")
    fun agreementById(agreementId: String): LiveData<List<AgreementEntity>>

    @Query("SELECT * from table_agreement LEFT JOIN table_agreement_equipment ON table_agreement.agreementId = table_agreement_equipment.agreementId WHERE table_agreement_equipment.equipmentId =:equipmentId")
    fun agreementByEquipmentId(equipmentId: String): LiveData<List<AgreementEntity>>

    @Query("SELECT * from table_agreement WHERE accountId =:accountId")
    fun agreementByAccountId(accountId: String): LiveData<List<AgreementEntity>>

    // For batch uplopad
    @Query("SELECT * from table_agreement")
    fun agreementAllOnce(): List<AgreementEntity>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun agreementInsert(data: AgreementEntity)

    @Update(onConflict = OnConflictStrategy.REPLACE)
    fun agreementUpdate(data: AgreementEntity)

    @Delete
    fun agreementDelete(data: AgreementEntity)

    @Query("DELETE FROM table_agreement")
    fun agreementDeleteAll()

    // --------------------------------------------------------------------------------------------- Agreement Equipment
    @Query("SELECT * from table_agreement_equipment")
    fun agreementEquipmentAll(): LiveData<List<AgreementEquipmentEntity>>

    @Query("SELECT " +
            "table_agreement_equipment.equipmentId," +
            "table_agreement_equipment.serialNo," +
            "table_agreement_equipment.statusTextEnumId," +
            "table_agreement_equipment.statusTextEnumText," +
            "table_agreement_equipment.product_name," +
            "table_agreement_equipment.accountName," +
            "table_agreement_equipment.warrantyEndDate," +
            "table_agreement_equipment.agreementEnd," +
            "table_service_report.reportLocalId," +
            "table_service_report.serviceReportNumber," +
            "table_service_report.createdDate," +
            "table_service_report.reportStatusEnumId," +
            "table_service_report.reportStatusEnumText," +
            "table_service_report.servicedByUserName," +
            "table_service_call.callTypeEnumId," +
            "table_service_call.callTypeEnumText " +
            "FROM table_agreement_equipment " +
            "LEFT JOIN table_service_report_equipment ON table_agreement_equipment.equipmentId = table_service_report_equipment.equipmentId " +
            "LEFT JOIN table_service_report ON table_service_report_equipment.reportId = table_service_report.reportLocalId " +
            "LEFT JOIN table_service_call ON table_service_report.callId = table_service_call.serviceCallId " +
            "WHERE table_agreement_equipment.agreementId =:agreementId " +
            "GROUP BY table_agreement_equipment.equipmentId")
    fun agreementEquipmentByAgreementId(agreementId: String): LiveData<List<EquipmentListModel>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun agreementEquipmentInsert(data: AgreementEquipmentEntity)

    @Delete
    fun agreementEquipmentDelete(data: AgreementEquipmentEntity)

    @Query("DELETE FROM table_agreement_equipment")
    fun agreementEquipmentDeleteAll()

    // --------------------------------------------------------------------------------------------- Service Call
    @Query("SELECT table_service_call.serviceCallId, table_service_call.serviceCallNumber, table_service_call.startDate, table_service_call.endDate FROM table_service_call WHERE table_service_call.startDate <= :endDateTime AND table_service_call.endDate >= :startDateTime")
    fun callTodayAlert(startDateTime: String, endDateTime: String): LiveData<List<ReportAlertModel>>

    @Query("SELECT * from table_service_call LEFT JOIN table_color ON table_service_call.callStatusEnumText = table_color.colorStatus AND table_color.colorTable = 'Call' WHERE table_service_call.callStatusEnumId IN (:filterArray) AND table_service_call.serviceCallNumber LIKE '%' || :searchTExt || '%'  ORDER BY strftime('%Y-%m-%d %H:%M:%S', createdDate) DESC LIMIT 100")
    fun callAll(searchTExt: String, filterArray: ArrayList<String>): LiveData<List<ServiceCallEntity>>

    @Query("SELECT * from table_service_call LEFT JOIN table_color ON table_service_call.callStatusEnumText = table_color.colorStatus AND table_color.colorTable = 'Call' WHERE serviceCallNumber LIKE '%' || :number || '%' OR accountName LIKE '%' || :accountName || '%' OR serviceCallSubject LIKE '%' || :subject || '%' ORDER BY strftime('%Y-%m-%d %H:%M:%S', createdDate) DESC LIMIT 3")
    fun callSearch(number: String, accountName: String, subject: String): LiveData<List<ServiceCallEntity>>

    // For batch uplopad
    @Query("SELECT * from table_service_call")
    fun callAllOnce(): List<ServiceCallEntity>

    @Query("SELECT * from table_service_call LEFT JOIN table_color ON table_service_call.callStatusEnumText = table_color.colorStatus AND table_color.colorTable = 'Call' WHERE accountId =:accountId")
    fun callByAccountId(accountId: String): LiveData<List<ServiceCallEntity>>

    @Query("SELECT * from table_service_call LEFT JOIN table_color ON table_service_call.callStatusEnumText = table_color.colorStatus AND table_color.colorTable = 'Call' WHERE serviceCallId =:callId")
    fun callByCallId(callId: String): LiveData<List<ServiceCallEntity>>

    @Query("SELECT * from table_service_call LEFT JOIN table_color ON table_service_call.callStatusEnumText = table_color.colorStatus AND table_color.colorTable = 'Call' WHERE serviceCallId =:callId")
    fun callByCallIdBackground(callId: String): List<ServiceCallEntity>

    @Query("SELECT * from table_service_call LEFT JOIN table_color ON table_service_call.callStatusEnumText = table_color.colorStatus AND table_color.colorTable = 'Call' WHERE caseId =:caseId ORDER BY strftime('%Y-%m-%d %H:%M:%S', createdDate) DESC")
    fun callByCaseId(caseId: String): LiveData<List<ServiceCallEntity>>

    @Query("SELECT * from table_service_call LEFT JOIN table_service_call_equipment ON table_service_call.serviceCallId = table_service_call_equipment.callId LEFT JOIN table_color ON table_service_call.callStatusEnumText = table_color.colorStatus AND table_color.colorTable = 'Call' WHERE table_service_call_equipment.equipmentId =:equipmentId")
    fun callEquipmentRelated(equipmentId: String): LiveData<List<ServiceCallEntity>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun callInsert(data: ServiceCallEntity)

    @Update(onConflict = OnConflictStrategy.REPLACE)
    fun callUpdate(data: ServiceCallEntity)

    @Query("UPDATE table_service_call SET equipment=:data WHERE serviceCallId=:id")
    fun callUpdateEquipment(id: String, data: List<GetDataRequest.Equipment>)

    @Delete
    fun callDelete(data: ServiceCallEntity)

    @Query("DELETE FROM table_service_call")
    fun  callDeleteAll()

    @Query("SELECT table_service_call.serviceCallId, table_service_call.serviceCallNumber, table_service_call.callStatusEnumText, table_service_call.serviceCallDescription, table_service_call.startDate, table_service_call.equipment, table_service_call.engineer, table_account.account_name FROM table_service_call LEFT JOIN table_account ON table_account.accountId = table_service_call.accountId ORDER BY serviceCallId DESC")
    fun callList(): LiveData<List<SCListModel>>

    @Query("SELECT table_service_report.reportLocalId, table_service_report.serviceReportNumber, table_service_report.reportStatusEnumText, table_service_report.reportDate, table_service_report.equipment, table_service_report.servicedByUserName, table_account.account_name FROM table_service_report LEFT JOIN table_account ON table_account.accountId = table_service_report.accountId WHERE table_service_report.callId =:callId")
    fun callReportByCallId(callId: String): LiveData<List<SRListModel>>

    @Query("UPDATE table_service_call SET isUpload=:isUplaod WHERE serviceCallId=:serviceCallId")
    fun callUpdateFlag(serviceCallId: String, isUplaod: Boolean)

    @Query("SELECT table_service_call.serviceCallId from table_service_call WHERE isUpload=:isUplaod")
    fun callUpload(isUplaod: Boolean): List<String>

    // --------------------------------------------------------------------------------------------- Service Call Training
    @Query("SELECT * from table_service_call_training")
    fun callTrainingAll(): LiveData<List<ServiceCallTrainingEntity>>

    @Query("SELECT * from table_service_call_training WHERE callId=:callId")
    fun callTrainingByCallId(callId: String): LiveData<List<ServiceCallTrainingEntity>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun callTrainingInsert(data: ServiceCallTrainingEntity)

    @Update(onConflict = OnConflictStrategy.REPLACE)
    fun callTrainingUpdate(data: ServiceCallTrainingEntity)

    @Delete
    fun callTrainingDelete(data: ServiceCallTrainingEntity)

    @Query("DELETE FROM table_service_call_training")
    fun callTrainingDeleteAll()

    @Query("DELETE from table_service_call_training WHERE callId=:callId")
    fun callTrainingDeleteByCallId(callId: String)

    // --------------------------------------------------------------------------------------------- Service Call Handover
    @Query("SELECT * from table_service_call_handover")
    fun callHandoverAll(): LiveData<List<ServiceCallHandoverEntity>>

    @Query("SELECT * from table_service_call_handover WHERE callId=:callId")
    fun callHandoverByCallId(callId: String): LiveData<List<ServiceCallHandoverEntity>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun callHandoverInsert(data: ServiceCallHandoverEntity)

    @Update(onConflict = OnConflictStrategy.REPLACE)
    fun callHandoverUpdate(data: ServiceCallHandoverEntity)

    @Delete
    fun callHandoverDelete(data: ServiceCallHandoverEntity)

    @Query("DELETE from table_service_call_handover WHERE callId=:callId")
    fun callHandoverDeleteByCallId(callId: String)

    @Query("DELETE FROM table_service_call_handover")
    fun callHandoverDeleteAll()

    // --------------------------------------------------------------------------------------------- Service Call Completion
    @Query("SELECT * from table_service_call_completion")
    fun callCompletionAll(): LiveData<List<ServiceCallCompletionEntity>>

    @Query("SELECT * from table_service_call_completion WHERE callId=:callId")
    fun callCompletionByCallId(callId: String): LiveData<List<ServiceCallCompletionEntity>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun callCompletionInsert(data: ServiceCallCompletionEntity)

    @Update(onConflict = OnConflictStrategy.REPLACE)
    fun callCompletionUpdate(data: ServiceCallCompletionEntity)

    @Delete
    fun callCompletionDelete(data: ServiceCallCompletionEntity)

    @Query("DELETE from table_service_call_completion WHERE callId=:callId")
    fun callCompletionDeleteByCallId(callId: String)

    @Query("DELETE FROM table_service_call_completion")
    fun callCompletionDeleteAll()

    // --------------------------------------------------------------------------------------------- Service Call Equipment
    @Query("SELECT * FROM table_service_call_equipment")
    fun callEquipmentAll(): LiveData<List<ServiceCallEquipmentEntity>>

    @Query("SELECT * FROM table_service_call_equipment WHERE callId=:callId AND equipmentId=:equipmentId")
    fun callEquipmentCheckAttach(callId: String, equipmentId: String): LiveData<List<ServiceCallEquipmentEntity>>

    @Query("SELECT " +
            "table_service_call_equipment.equipmentId," +
            "table_service_call_equipment.serialNo," +
            "table_service_call_equipment.statusTextEnumId," +
            "table_service_call_equipment.statusTextEnumText," +
            "table_service_call_equipment.product_name," +
            "table_service_call_equipment.accountName," +
            "table_service_call_equipment.warrantyEndDate," +
            "table_service_call_equipment.agreementEnd," +
            "table_service_report.reportLocalId," +
            "table_service_report.serviceReportNumber," +
            "table_service_report.createdDate," +
            "table_service_report.reportStatusEnumId," +
            "table_service_report.reportStatusEnumText," +
            "table_service_report.servicedByUserName," +
            "table_service_call.callTypeEnumId," +
            "table_service_call.callTypeEnumText " +
            "FROM table_service_call_equipment " +
            "LEFT JOIN table_service_report_equipment ON table_service_call_equipment.equipmentId = table_service_report_equipment.equipmentId " +
            "LEFT JOIN table_service_report ON table_service_report_equipment.reportId = table_service_report.reportLocalId " +
            "LEFT JOIN table_service_call ON table_service_report.callId = table_service_call.serviceCallId " +
            "WHERE table_service_call_equipment.callId =:callId AND table_service_call_equipment.isAttachToCall=:isAttachToCall " +
            "GROUP BY table_service_call_equipment.equipmentId " +
            "ORDER BY strftime('%Y-%m-%d %H:%M:%S', table_service_call_equipment.createdDate) DESC")
    fun callEquipmentList(callId: String, isAttachToCall: Boolean): LiveData<List<EquipmentListModel>>

    @Query("SELECT * from table_service_call_equipment WHERE callId=:callId AND isAttachToCall=:isAttachToCall ORDER BY strftime('%Y-%m-%d %H:%M:%S', createdDate) DESC")
    fun callEquipmentAttachToCall(callId: String, isAttachToCall: Boolean): LiveData<List<ServiceCallEquipmentEntity>>

    @Query("SELECT * from table_service_call_equipment WHERE callId=:callId AND isAttachToCall=:isAttachToCall AND isAttachToReport=:isAttachToReport ORDER BY strftime('%Y-%m-%d %H:%M:%S', createdDate) DESC")
    fun callEquipmentAttachToReport(callId: String, isAttachToCall: Boolean, isAttachToReport: Boolean): LiveData<List<ServiceCallEquipmentEntity>>

    @Query("SELECT * from table_service_call_equipment WHERE table_service_call_equipment.callId=:callId AND table_service_call_equipment.equipmentId NOT IN (SELECT DISTINCT table_service_report_equipment.equipmentId FROM table_service_report_equipment WHERE table_service_report_equipment.reportId=:reportId) AND serialNo LIKE '%' || :searchText || '%' ORDER BY table_service_call_equipment.serialNo DESC")
    fun callEquipmentAdd(callId:String, reportId:String, searchText: String): LiveData<List<ServiceCallEquipmentEntity>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun callEquipmentInsert(data: ServiceCallEquipmentEntity)

    @Query("UPDATE table_service_call_equipment SET isAttachToCall=:isAttachToCall WHERE callId=:callId AND equipmentId=:equipmentId")
    fun callEquipmentUpdateFlagCall(callId: String, equipmentId: String, isAttachToCall: Boolean)

    @Query("UPDATE table_service_call_equipment SET isAttachToReport=:isAttachToReport WHERE callId=:callId AND equipmentId=:equipmentId")
    fun callEquipmentUpdateFlagReport(callId: String, equipmentId: String, isAttachToReport: Boolean)

    @Delete
    fun callEquipmentDelete(data: ServiceCallEquipmentEntity)

    @Query("DELETE FROM table_service_call_equipment WHERE serviceCallEquipmentId=:callEquipmentId")
    fun  callEquipmentDeleteById(callEquipmentId: String)

    @Query("DELETE FROM table_service_call_equipment WHERE callId=:callId")
    fun  callEquipmentDeleteByCallId(callId: String)

    @Query("SELECT table_service_call_equipment.equipmentId from table_service_call_equipment WHERE callId=:callId")
    fun callEquipmentUpload(callId: String): List<String>

    @Query("Delete FROM table_service_call_equipment WHERE isLocal=:isLocal")
    fun callEquipmentDeleteLocal(isLocal: Boolean)

    // --------------------------------------------------------------------------------------------- Service Report
    @Query("SELECT table_service_call.serviceCallId, table_service_call.serviceCallNumber, table_service_call.startDate, table_service_call.endDate, table_service_report.reportLocalId FROM table_service_call LEFT JOIN table_service_report ON table_service_call.serviceCallId = table_service_report.callId WHERE table_service_call.startDate <= :endDateTime AND table_service_call.endDate >= :startDateTime GROUP BY table_service_call.serviceCallId")
    fun reportTodayAlert(startDateTime: String, endDateTime: String): List<ReportAlertModel>

    @Query("SELECT * from table_service_report LEFT JOIN table_color ON table_service_report.reportStatusEnumText = table_color.colorStatus AND table_color.colorTable = 'Report' WHERE table_service_report.reportStatusEnumId IN (:filterArray) AND table_service_report.serviceReportNumberTemp LIKE '%' || :searchText || '%' ORDER BY strftime('%Y-%m-%d %H:%M:%S', createdDate) DESC LIMIT 100")
    fun reportAll(searchText: String, filterArray: ArrayList<String>): LiveData<List<ServiceReportEntity>>

//    @Query("SELECT table_service_report.*, table_service_call.engineer FROM table_service_report LEFT JOIN table_service_call ON table_service_call.serviceCallId = table_service_report.callId LEFT JOIN table_color ON table_service_report.reportStatusEnumText = table_color.colorStatus AND table_color.colorTable = 'Report' WHERE table_service_report.reportStatusEnumId IN (:filterArray) ORDER BY strftime('%Y-%m-%d %H:%M:%S', createdDate) DESC")
//    fun reportTest(filterArray: ArrayList<String>): LiveData<List<ReportTestModel>>

    @Query("SELECT * from table_service_report LEFT JOIN table_color ON table_service_report.reportStatusEnumText = table_color.colorStatus AND table_color.colorTable = 'Report' WHERE serviceReportNumber LIKE '%' || :number || '%' OR accountName LIKE '%' || :accountName || '%' ORDER BY strftime('%Y-%m-%d %H:%M:%S', createdDate) DESC LIMIT 3")
    fun reportSearch(number: String, accountName: String): LiveData<List<ServiceReportEntity>>

    @Query("SELECT table_service_report.serviceReportNumber ,table_service_report_equipment.reportId, table_service_report_equipment.equipmentId FROM table_service_report LEFT JOIN table_service_report_equipment ON table_service_report.reportLocalId = table_service_report_equipment.reportId  WHERE callId =:callId")
    fun reportJoinReportEquipment(callId: String): LiveData<List<AddEquipmentJointModel>>

    @Query("SELECT * from table_service_report WHERE reportLocalId =:reportId")
    fun reportByReportId(reportId: String): LiveData<List<ServiceReportEntity>>

    @Query("SELECT * from table_service_report WHERE serviceReportNumberTemp =:tempNumber")
    fun reportByTempNumber(tempNumber: String): LiveData<List<ServiceReportEntity>>

    @Query("SELECT * from table_service_report LEFT JOIN table_color ON table_service_report.reportStatusEnumText = table_color.colorStatus AND table_color.colorTable = 'Report' WHERE callId =:callId")
    fun reportByCallId(callId: String): LiveData<List<ServiceReportEntity>>

    @Query("SELECT * from table_service_report LEFT JOIN table_color ON table_service_report.reportStatusEnumText = table_color.colorStatus AND table_color.colorTable = 'Report' WHERE caseId =:caseId")
    fun reportByCaseId(caseId: String): LiveData<List<ServiceReportEntity>>

    @Query("SELECT * from table_service_report LEFT JOIN table_service_report_equipment ON table_service_report.reportLocalId = table_service_report_equipment.reportId LEFT JOIN table_color ON table_service_report.reportStatusEnumText = table_color.colorStatus AND table_color.colorTable = 'Report' WHERE table_service_report_equipment.equipmentId =:equipmentId ORDER BY strftime('%Y-%m-%d %H:%M:%S', table_service_report.createdDate) DESC")
    fun reportEquipmentRelated(equipmentId: String): LiveData<List<ServiceReportEntity>>

    @Query("SELECT * from table_service_report LEFT JOIN table_color ON table_service_report.reportStatusEnumText = table_color.colorStatus AND table_color.colorTable = 'Report' WHERE accountId =:accountId")
    fun reportByAccountId(accountId: String): LiveData<List<ServiceReportEntity>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun reportInsert(data: ServiceReportEntity)

    @Update(onConflict = OnConflictStrategy.REPLACE)
    fun reportUpdate(data: ServiceReportEntity)

    @Query("UPDATE table_service_report SET equipment=:data WHERE reportLocalId=:id")
    fun reportUpdateEquipment(id: String, data: String)

    @Query("UPDATE table_service_report SET customerRating=:rating, feedback=:feedback, accountPicSignatureFileName=:fileName, accountPicSignatureFileImage=:fileImage, cpNumber=:cpNumber, cpName=:cpName, isUpload=:isUplaod WHERE reportLocalId=:reportId")
    fun reportFeedback(reportId: String, rating: String, feedback: String, fileName: String, fileImage: String, cpNumber: String, cpName: String, isUplaod: Boolean)

    @Query("UPDATE table_service_report SET fieldAccountContactId=:picId, fieldAccountContactName=:picName, fieldAccountContactPhone=:picPhone, isUpload=:isUplaod WHERE reportLocalId=:reportId")
    fun reportUpdateContact(reportId: String, picId: String, picName: String, picPhone: String, isUplaod: Boolean)

    @Query("UPDATE table_service_report SET internalMemo=:memo, isChargeable=:isChargeable, isUpload=:isUplaod, fieldAccountContactId=:picId, fieldAccountContactName=:picName, fieldAccountContactPhone=:picPhone WHERE reportLocalId=:reportId")
    fun reportUpdateMaster(reportId: String, memo: String, isChargeable: String,
                           picId: String, picName: String, picPhone: String, isUplaod: Boolean)

    @Query("UPDATE table_service_report SET isUpload=:isUplaod WHERE reportLocalId=:reportId")
    fun reportUpdateFlag(reportId: String, isUplaod: Boolean)

    @Query("DELETE FROM table_service_report WHERE reportLocalId=:reportId")
    fun reportDeleteById(reportId: String)

    @Delete
    fun reportDelete(data: ServiceReportEntity)

    @Query("DELETE FROM table_service_report")
    fun reportDeleteAll()

    @Query("SELECT table_service_report.reportLocalId, table_service_report.serviceReportNumber, table_service_report.reportStatusEnumText, table_service_report.reportDate, table_service_report.equipment, table_service_report.servicedByUserName, table_account.account_name FROM table_service_report LEFT JOIN table_account ON table_account.accountId = table_service_report.accountId ORDER BY strftime('%Y-%m-%d %H:%M:%S', createdDate) DESC")
    fun reportList(): LiveData<List<SRListModel>>

    @Query("SELECT * from table_service_report WHERE isUpload=:isUplaod")
    fun reportUpload(isUplaod: Boolean): List<ServiceReportEntity>

    @Query("UPDATE table_service_report SET reportLocalId=:localId, reportServerId=:serverId, serviceReportNumber=:reportNumber, isUpload=:isUpload WHERE serviceReportNumberTemp=:tempNumber")
    fun reportUploadFinish(localId: String, serverId: String, reportNumber: String, tempNumber: String, isUpload: Boolean)

    @Query("Delete FROM table_service_report WHERE isLocal=:isLocal")
    fun reportDeleteLocal(isLocal: Boolean)


    // --------------------------------------------------------------------------------------------- Service Report Equipment
    @Query("SELECT * FROM table_service_report_equipment WHERE serviceReportEquipmentCallId=:callId AND equipmentId=:equipmentId AND isAttach=:isAttach")
    fun reportEquipmentCheckAttach(callId: String, equipmentId: String, isAttach: Boolean): LiveData<List<ServiceReportEquipmentEntity>>

    @Query("SELECT * from table_service_report_equipment WHERE reportId=:reportId")
    fun reportEquipmentByReportId(reportId: String): LiveData<List<ServiceReportEquipmentEntity>>

    @Query("SELECT * from table_service_report_equipment WHERE serviceReportEquipmentId=:reportEquipmentId")
    fun reportEquipmentByReportEquipmentId(reportEquipmentId: String): LiveData<List<ServiceReportEquipmentEntity>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun reportEquipmentInsert(data: ServiceReportEquipmentEntity)

    @Query("UPDATE table_service_report_equipment SET checklistTemplateId=:templateId WHERE serviceReportEquipmentId=:reportEquipmentId")
    fun reportEquipmentEditTemplateId(reportEquipmentId: String, templateId: String)

    @Query("UPDATE table_service_report_equipment SET statusAfterServiceEnumId=:statusAfterServiceEnumId, statusAfterServiceEnumName=:statusAfterServiceEnumName, statusAfterServiceRemarks=:statusAfterServiceRemarks, problemSymptom=:problem, isUpload=:isUpload  WHERE serviceReportEquipmentId=:reportEquipmentId")
    fun reportEquipmentEdit(reportEquipmentId: String, statusAfterServiceEnumId: String, statusAfterServiceEnumName: String, statusAfterServiceRemarks: String, problem: String, isUpload: Boolean)

    @Query("UPDATE table_service_report_equipment SET isUpload=:isAttach WHERE serviceReportEquipmentId=:reportEquipmentId")
    fun reportEquipmentUpdateFlag(reportEquipmentId: String, isAttach: Boolean)

    @Delete
    fun reportEquipmentDelete(data: ServiceReportEquipmentEntity)

    @Query("DELETE FROM table_service_report_equipment WHERE serviceReportEquipmentId=:reportEquipmentId")
    fun  reportEquipmentDeleteById(reportEquipmentId: String)

    @Query("SELECT * FROM table_service_report_equipment LEFT JOIN table_service_report_equipment_checklist ON table_service_report_equipment.serviceReportEquipmentId = table_service_report_equipment_checklist.report_equipment_id LEFT JOIN table_product_checklist_question ON table_service_report_equipment.checklistTemplateId = table_product_checklist_question.templateId LEFT JOIN table_report_sparepart ON table_service_report_equipment.serviceReportEquipmentId = table_report_sparepart.reportEquipmentId_sparepart LEFT JOIN table_problem ON table_service_report_equipment.serviceReportEquipmentId = table_problem.reportEquipmentId_problem LEFT JOIN table_solution ON table_service_report_equipment.serviceReportEquipmentId = table_solution.reportEquipmentId_solution WHERE table_service_report_equipment.serviceReportEquipmentCallId=:callId AND table_service_report_equipment.isAttach=:isAttach")
    fun reportEquipmentAllByCallId(callId: String, isAttach: Boolean): LiveData<List<SREquipmentJointModel>>

    @Query("SELECT * FROM table_service_report_equipment LEFT JOIN table_service_report_equipment_checklist ON table_service_report_equipment.serviceReportEquipmentId = table_service_report_equipment_checklist.report_equipment_id LEFT JOIN table_product_checklist_question ON table_service_report_equipment.checklistTemplateId = table_product_checklist_question.templateId LEFT JOIN table_report_sparepart ON table_service_report_equipment.serviceReportEquipmentId = table_report_sparepart.reportEquipmentId_sparepart LEFT JOIN table_problem ON table_service_report_equipment.serviceReportEquipmentId = table_problem.reportEquipmentId_problem LEFT JOIN table_solution ON table_service_report_equipment.serviceReportEquipmentId = table_solution.reportEquipmentId_solution WHERE table_service_report_equipment.reportId=:reportId")
    fun reportEquipmentDetail(reportId: String): LiveData<List<SREquipmentJointModel>>

    @Query("SELECT * from table_service_report_equipment WHERE reportId=:reportId")
    fun reportEquipmentUpload(reportId: String): List<ServiceReportEquipmentEntity>

    @Query("Delete FROM table_service_report_equipment WHERE isLocal=:isLocal")
    fun reportEquipmentDeleteLocal(isLocal: Boolean)

    @Query("Delete FROM table_service_report_equipment WHERE reportId=:reportId")
    fun reportEquipmentDeleteByReportId(reportId: String)


    // --------------------------------------------------------------------------------------------- Service Report Time Tracking
    @Query("SELECT * from table_service_report_time_tracking WHERE reportId=:reportId ORDER BY strftime('%Y-%m-%d %H:%M:%S', start) DESC")
    fun reportTimeTrackingByReportId(reportId: String): LiveData<List<ServiceReportTimeTrackingEntity>>

    @Query("SELECT * from table_service_report_time_tracking WHERE callId=:callId ORDER BY strftime('%Y-%m-%d %H:%M:%S', start) DESC")
    fun reportTimeTrackingByCallId(callId: String): LiveData<List<ServiceReportTimeTrackingEntity>>

    @Query("SELECT * from table_service_report_time_tracking WHERE callId=:callId ORDER BY strftime('%Y-%m-%d %H:%M:%S', start) DESC")
    fun reportTimeTrackingByCallIdBackground(callId: String): List<ServiceReportTimeTrackingEntity>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun reportTimeTrackingInsert(data: ServiceReportTimeTrackingEntity)

    @Update(onConflict = OnConflictStrategy.REPLACE)
    fun reportTimeTrackingUpdate(data: ServiceReportTimeTrackingEntity)

    @Delete
    fun reportTimeTrackingDelete(data: ServiceReportTimeTrackingEntity)

    @Query("DELETE FROM table_service_report_time_tracking")
    fun  reportTimeTrackingDeleteAll()

    @Query("SELECT * from table_service_report_time_tracking WHERE reportId=:reportId")
    fun reportTimeTrackingUpload(reportId:String): List<ServiceReportTimeTrackingEntity>

    @Query("Delete FROM table_service_report_time_tracking WHERE isLocal=:isLocal")
    fun reportTimeTrackingDeleteLocal(isLocal: Boolean)

    @Query("Delete FROM table_service_report_time_tracking WHERE timeTrackingId=:id")
    fun reportTimeTrackingDeleteById(id: String)

    @Query("DELETE from table_service_report_time_tracking WHERE reportId=:reportId")
    fun reportTimeTrackingDeleteByReportId(reportId: String)

    // --------------------------------------------------------------------------------------------- Problem Image
    @Query("SELECT * from table_problem WHERE reportEquipmentId_problem=:reportEquipmentId")
    fun problemByreportEquipment(reportEquipmentId: String): LiveData<List<ProblemEntity>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun problemInsert(data: ProblemEntity)

    @Update(onConflict = OnConflictStrategy.REPLACE)
    fun problemUpdate(data: ProblemEntity)

    @Query("SELECT * from table_problem WHERE reportEquipmentId_problem=:reportEquipmentId")
    fun problemUpload(reportEquipmentId: String): List<ProblemEntity>

    @Query("DELETE from table_problem WHERE reportEquipmentId_problem=:reportEquipmentId")
    fun problemDeleteByReportEquipmentId(reportEquipmentId: String)

    // --------------------------------------------------------------------------------------------- Solution
    @Query("SELECT * from table_solution WHERE reportEquipmentId_solution=:reportEquipmentId")
    fun solutionByReportEquipment(reportEquipmentId: String): LiveData<List<SolutionEntity>>

    @Query("SELECT * from table_solution WHERE serviceReportEquipmentSolutionId=:solutionId")
    fun solutionBySolutionId(solutionId: String): LiveData<List<SolutionEntity>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun solutionInsert(data: SolutionEntity)

    @Update(onConflict = OnConflictStrategy.REPLACE)
    fun solutionUpdate(data: SolutionEntity)

    @Delete
    fun solutionDelete(data: SolutionEntity)

    @Query("DELETE FROM table_solution")
    fun solutionDeleteAll()

    @Query("SELECT * from table_solution WHERE reportEquipmentId_solution=:reportEquipmentId")
    fun solutionUpload(reportEquipmentId: String): List<SolutionEntity>

    @Query("DELETE FROM table_solution WHERE reportEquipmentId_solution=:reportEquipmnetId")
    fun solutionDeleteByReportEquipmnetId(reportEquipmnetId: String)

    @Query("DELETE FROM table_solution WHERE isLocal=:isLocal")
    fun solutionDeleteLocal(isLocal: Boolean)

    // ============================================================================================= Report Equipment Checklist
    @Query("SELECT * from table_service_report_equipment_checklist WHERE report_equipment_id=:reportEquipmentId ORDER BY question_section ASC")
    fun reportChecklistByReportEquipment(reportEquipmentId: String): LiveData<List<ServiceReportEquipmentChecklistEntity>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun reportChecklistInsert(data: ServiceReportEquipmentChecklistEntity)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun reportChecklistInsertAll(data: ArrayList<ServiceReportEquipmentChecklistEntity>)

    @Query("UPDATE table_service_report_equipment_checklist SET checklist_status_id=:enumId, checklist_status_Name=:enumName, remarks=:remark WHERE question_id=:questionId")
    fun reportChecklistUpdateStatus(questionId: String, enumId: String, enumName: String, remark: String)

    @Query("UPDATE table_service_report_equipment_checklist SET checklistImage=:checklistImage WHERE question_id=:questionId")
    fun reportChecklistUpdateImage(questionId: String, checklistImage: ArrayList<ConverterImageModel>)

    @Query("SELECT * from table_service_report_equipment_checklist WHERE report_equipment_id=:reportEquipmentId")
    fun reportChecklistUpload(reportEquipmentId: String): List<ServiceReportEquipmentChecklistEntity>

    @Query("DELETE FROM table_service_report_equipment_checklist WHERE report_equipment_id=:reportEquipmentId")
    fun reportChecklistDeleteByReportEquipmentId(reportEquipmentId: String)

    @Query("DELETE FROM table_service_report_equipment_checklist WHERE report_equipment_id=:reportEquipmentId")
    fun reportChecklistDeleteById(reportEquipmentId: String)

    @Query("DELETE FROM table_service_report_equipment_checklist WHERE isLocal=:isLocal")
    fun reportChecklistDeleteLocal(isLocal: Boolean)

    // --------------------------------------------------------------------------------------------- Report Checklist Template (delete)
    @Query("SELECT * FROM table_service_report_equipment_checklist_template")
    fun  reportChecklistAll(): LiveData<List<ServiceReportEquipmentChecklistTemplateEntity>>

    @Query("SELECT * from table_service_report_equipment_checklist_template LEFT JOIN table_service_report_equipment_checklist_question ON table_service_report_equipment_checklist_template.templateId = table_service_report_equipment_checklist_question.checklistTemplateId WHERE reportEquipmentId_template=:reportEquipmentId")
    fun reportChecklistTemplateByReportEquipment(reportEquipmentId: String): LiveData<List<ChecklistJointReportModel>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun reportChecklistTemplateInsert(data: ServiceReportEquipmentChecklistTemplateEntity)

    @Query("DELETE FROM table_service_report_equipment_checklist_template")
    fun  reportChecklistTemplateDeleteAll()

    @Query("DELETE FROM table_service_report_equipment_checklist_template WHERE isLocal=:isLocal")
    fun reportChecklistTemplateDeleteLocal(isLocal: Boolean)

    // --------------------------------------------------------------------------------------------- Report Checklist Question (delete)
    @Query("SELECT * FROM table_service_report_equipment_checklist_question")
    fun reportChecklistQuestionAll(): LiveData<List<ServiceReportEquipmentChecklistQuestionEntity>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun reportChecklistQuestionInsert(data: ServiceReportEquipmentChecklistQuestionEntity)

    @Query("UPDATE table_service_report_equipment_checklist_question SET statusQuestionEnumId=:enumId, statusQuestionEnumName=:enumName, isUpload=:isUpload WHERE id=:questionId")
    fun  reportChecklistQuestionUpdateStatus(questionId: String, enumId: String, enumName: String, isUpload: Boolean)

    @Query("UPDATE table_service_report_equipment_checklist_question SET checklistImage=:checklistImage, isUpload=:isUpload WHERE id=:questionId")
    fun  reportChecklistQuestionUpdateImage(questionId: String, checklistImage: ArrayList<ConverterImageModel>, isUpload: Boolean)

    @Query("DELETE FROM table_service_report_equipment_checklist_question")
    fun  reportChecklistQuestionDeleteAll()

    @Query("SELECT * from table_service_report_equipment_checklist_question WHERE isUpload=:isUpload")
    fun reportChecklistQuestionUpload(isUpload: Boolean): List<ServiceReportEquipmentChecklistQuestionEntity>

    @Query("DELETE FROM table_service_report_equipment_checklist_question WHERE isLocal=:isLocal")
    fun reportChecklistQuestionDeleteLocal(isLocal: Boolean)

    // --------------------------------------------------------------------------------------------- Report Sparepart
    @Query("SELECT * FROM table_report_sparepart")
    fun reportSparepartAll(): LiveData<List<ServiceReportSparepartEntity>>

    @Query("SELECT * FROM table_report_sparepart WHERE reportEquipmentId_sparepart=:reportEquipmentId")
    fun  reportSparepartByReportEquipment(reportEquipmentId: String) : LiveData<List<ServiceReportSparepartEntity>>

    @Query("SELECT * FROM table_report_sparepart WHERE reportEquipmentId_sparepart=:reportEquipmentId AND part_id=:partId AND enum_id=:enumId")
    fun  reportSparepartInsertCheck(reportEquipmentId: String, partId: String, enumId: String) : LiveData<List<ServiceReportSparepartEntity>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun reportSparepartInsert(data: ServiceReportSparepartEntity)

    @Update(onConflict = OnConflictStrategy.REPLACE)
    fun reportSparepartUpdate(data: ServiceReportSparepartEntity)

    @Query("UPDATE table_report_sparepart SET enum_id=:enumId, enum_name=:enumName, isUpload=:isUpload WHERE service_report_sparepart_id=:reportSparepartId")
    fun  reportSparepartUpdateStatus(reportSparepartId: String, enumId: String, enumName: String, isUpload: Boolean)

    @Delete
    fun reportSparepartDelete(data: ServiceReportSparepartEntity)

    @Query("DELETE FROM table_report_sparepart")
    fun  reportSparepartDeleteAll()

    @Query("SELECT * from table_report_sparepart WHERE reportEquipmentId_sparepart=:reportEquipmentId")
    fun reportSparepartUpload(reportEquipmentId: String): List<ServiceReportSparepartEntity>

    @Query("DELETE FROM table_report_sparepart WHERE reportEquipmentId_sparepart=:reportEquipmentId")
    fun reportSparepartDeleteByReportEquipmentId(reportEquipmentId: String)

    @Query("DELETE FROM table_report_sparepart WHERE isLocal=:isLocal")
    fun reportSparepartDeleteLocal(isLocal: Boolean)

    // ============================================================================================= Email
    // For batch uplopad
    @Query("SELECT * from table_email")
    fun emailAll(): List<EmailEntity>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun emailInsert(data: EmailEntity)

    @Query("DELETE FROM table_email WHERE emailId =:emailId")
    fun emailDeleteById(emailId: Int)

    // ============================================================================================= File
    @Query("SELECT * from table_file")
    fun fileAll(): LiveData<List<FileEntity>>

    @Query("SELECT * from table_file WHERE name =:fileName")
    fun fileGetByName(fileName: String): LiveData<List<FileEntity>>

    @Query("SELECT * from table_file WHERE name =:fileName")
    fun fileGetByNameThread(fileName: String): List<FileEntity>

    // For batch uplopad
    @Query("SELECT * from table_file WHERE section != 'pdf' ")
    fun fileGetUpload(): List<FileEntity>

    @Query("DELETE FROM table_file WHERE name =:name")
    fun fileDeleteByName(name: String)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun fileInsert(data: FileEntity)

    @Update(onConflict = OnConflictStrategy.REPLACE)
    fun fileUpdate(data: FileEntity)

    @Delete
    fun fileDelete(data: FileEntity)

    @Query("DELETE FROM table_file")
    fun fileDeleteAll()

    // ============================================================================================= Chargeable Type
    // For batch uplopad
    @Query("SELECT * from table_chargeable ORDER BY `key` ASC")
    fun chargeableAll(): LiveData<List<ChargeableEntity>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun chargeableInsert(data: ChargeableEntity)

}