package com.syspex.data.local.database.account

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import com.syspex.data.model.AccountList
import com.syspex.data.remote.GetDataRequest

class AccountViewModel constructor(application: Application) : AndroidViewModel(application) {
    private val repository : AccountRepository = AccountRepository(application)
    private val data : LiveData<List<AccountEntity>>

    init {
        data = repository.getAll()
    }

    fun search(textSearch: String): LiveData<List<AccountList>> {
        return repository.search(code = textSearch, accountName = textSearch)
    }

    fun getAll(): LiveData<List<AccountEntity>> {
        return data
    }

    fun getList(searchText: String): LiveData<List<AccountList>> {
        return repository.getList(searchText)
    }

    fun getByAccountId(accountId: String): LiveData<List<AccountEntity>> {
        return repository.getByAccountId(accountId)
    }

    fun insert(data: AccountEntity){
        repository.insert(data)
    }

    fun delete(data: AccountEntity){
        repository.delete(data)
    }

    fun deleteAll(){
        repository.deleteAll()
    }

}