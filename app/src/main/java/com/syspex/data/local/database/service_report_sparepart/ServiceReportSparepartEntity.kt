package com.syspex.data.local.database.service_report_sparepart

import androidx.room.Entity
import androidx.room.PrimaryKey
import androidx.room.TypeConverters
import com.syspex.data.local.database.ConverterEquipment
import com.syspex.data.remote.GetDataRequest

@Entity(tableName = "table_report_sparepart")
data class ServiceReportSparepartEntity (
    @PrimaryKey(autoGenerate = false)
    val service_report_sparepart_id: String,
    val reportEquipmentId_sparepart: String,
    val spare_part_image_number: String,
    val spare_part_number: String,
    val spare_part_description: String,
    val spare_part_memo: String,

    val enum_id: String,
    val enum_name: String,

    val part_id: String,
    val part_no: String,
    val part_name: String,
    val qty: String,

    val isUpload: Boolean,
    val isLocal: Boolean


)