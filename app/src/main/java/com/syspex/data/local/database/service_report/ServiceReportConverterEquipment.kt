package com.syspex.data.local.database.service_report

import androidx.room.TypeConverter
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.syspex.data.remote.GetDataRequest
import java.lang.reflect.Type
import java.util.*


class ServiceReportConverterEquipment {
    var gson = Gson()

    @TypeConverter
    fun stringToSomeObjectList(data: String?): List<GetDataRequest.Data.ServiceCase.ServiceCall.ServiceReport.ServiceReportEquipment?>? {
        if (data == null) {
            return Collections.emptyList()
        }
        val listType: Type =
            object : TypeToken<List<GetDataRequest.Data.ServiceCase.ServiceCall.ServiceReport.ServiceReportEquipment?>?>() {}.type
        return gson.fromJson<List<GetDataRequest.Data.ServiceCase.ServiceCall.ServiceReport.ServiceReportEquipment?>>(data, listType)
    }

    @TypeConverter
    fun someObjectListToString(someObjects: List<GetDataRequest.Data.ServiceCase.ServiceCall.ServiceReport.ServiceReportEquipment?>?): String? {
        return gson.toJson(someObjects)
    }


}