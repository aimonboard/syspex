package com.syspex.data.local.database.product_group

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "table_product")
data class ProductEntity (
    @PrimaryKey(autoGenerate = false)
    val product_group_id: String,
    val product_group_name: String,
    val product_group_tag_id: String,
    val machine_brand: String,
    val machine_supplier: String,
    val machine_model: String,
    val machine_class_category: String,
    val voltage: String,
    val current: String,
    val power: String,
    val phase: String,
    val frequency: String,
    val air_supply_needed: String,
    val air_pressure: String,
    val product_id: String,
    val created_date: String,
    val created_by: String,
    val last_modified_date: String,
    val last_modified_by: String
)