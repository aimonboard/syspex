package com.syspex.data.local.database.service_case

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData

class ServiceCaseViewModel constructor(application: Application) : AndroidViewModel(application) {
    private val repository : ServiceCaseRepository = ServiceCaseRepository(application)

    fun getAll(searchText: String, filterArray: ArrayList<String>): LiveData<List<ServiceCaseEntity>> {
        return repository.getAll(searchText, filterArray)
    }

    fun search(searchText: String): LiveData<List<ServiceCaseEntity>> {
        return repository.search(number = searchText, accountName = searchText, subject = searchText)
    }

    fun getByCaseId(caseId: String): LiveData<List<ServiceCaseEntity>> {
        return repository.getByCaseId(caseId)
    }

    fun getByAccountId(accountId: String): LiveData<List<ServiceCaseEntity>> {
        return repository.getByAccountId(accountId)
    }

    fun getByAgreementId(agreementId: String): LiveData<List<ServiceCaseEntity>> {
        return repository.getByAgreementId(agreementId)
    }

    fun caseEquipmentRelated(equipmentId: String): LiveData<List<ServiceCaseEntity>> {
        return repository.caseEquipmentRelated(equipmentId)
    }

    fun insert(data: ServiceCaseEntity){
        repository.insert(data)
    }

    fun delete(data: ServiceCaseEntity){
        repository.delete(data)
    }

    fun deleteAll(){
        repository.deleteAll()
    }

    fun updateFlag(caseId: String, isUpload: Boolean) {
        repository.updateFlag(caseId, isUpload)
    }

}