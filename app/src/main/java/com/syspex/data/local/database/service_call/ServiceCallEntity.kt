package com.syspex.data.local.database.service_call

import androidx.room.Entity
import androidx.room.PrimaryKey
import androidx.room.TypeConverters
import com.syspex.data.local.database.ConverterEquipment
import com.syspex.data.remote.GetDataRequest

@Entity(tableName = "table_service_call")
data class ServiceCallEntity (
    @PrimaryKey (autoGenerate = false)
    val serviceCallId: String,
    val caseId: String,
    val serviceCallNumber: String,
    val serviceCallSubject: String,
    val serviceCallDescription: String,
    val startDate: String,
    val endDate: String,
    val salesPersonId: String,
    val caseSalesId: String,
    val caseAccountContactId: String,
    val caseAccountFieldPicContacId: String,
    val isChargeable: String,
    val createdDate: String,
    val lastModifiedDate: String,
    val lastModifiedBy: String,

    val regionId: String,
    val regionName: String,

    val callRemark: String,
    val callStatusRemark: String,

    val callStatusEnumId: String,
    val callStatusEnumText: String,
    val colorBackground: String? = "",
    val colorStatusLabel: String? = "",
    val colorStatusLabelText: String? = "",

    val callTypeEnumId: String,
    val callTypeEnumText: String,

    val accountId: String,
    val accountName: String,

    val createdByUserId: String,
    val createdByUserName: String,

    val modifiedByUserId: String,
    val modifiedByUserName: String,

    val accountSalesPicId: String,
    val accountSalesPicName: String,

    val salesPicId: String,
    val salesPicName: String,

    val accountContactId: String,
    val accountContactName: String,
    val accountContactPhone: String,
    val accountContactEmail: String,

    val fieldAccountContactId: String,
    val fieldAccountContactName: String,
    val fieldAccountContactPhone: String,
    val fieldAccountContactEmail: String,

    val leadEngineerId: String,
    val leadEngineerName: String,

    @TypeConverters(ServiceCallConverterEquipment::class)
    val equipment: List<GetDataRequest.Data.ServiceCase.ServiceCall.ServiceCallEquipment?>? = null,

    @TypeConverters(ServiceCallConverterEngineer::class)
    val engineer: List<GetDataRequest.Engineer?>? = null,

    val isUpload: Boolean

)