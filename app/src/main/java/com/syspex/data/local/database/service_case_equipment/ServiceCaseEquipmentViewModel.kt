package com.syspex.data.local.database.service_case_equipment

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import com.syspex.data.local.database.service_report.ServiceReportEntity
import com.syspex.data.model.EquipmentListModel
import com.syspex.data.model.SCListModel
import com.syspex.data.model.SRListModel
import com.syspex.data.remote.GetDataRequest

class ServiceCaseEquipmentViewModel constructor(application: Application) : AndroidViewModel(application) {
    private val repository : ServiceCaseEquipmentRepository = ServiceCaseEquipmentRepository(application)

    fun all(): LiveData<List<ServiceCaseEquipmentEntity>> {
        return repository.all()
    }

    fun getByCaseId(caseId: String, isAttach: Boolean): LiveData<List<EquipmentListModel>> {
        return repository.getByCaseId(caseId, isAttach)
    }

    fun checkAttach(caseId: String, equipmentId: String): LiveData<List<ServiceCaseEquipmentEntity>> {
        return repository.checkAttach(caseId, equipmentId)
    }

    fun equipmentAdd(caseId: String, callId: String, searchText: String): LiveData<List<ServiceCaseEquipmentEntity>> {
        return repository.equipmentAdd(caseId, callId, searchText)
    }

    fun insert(data: ServiceCaseEquipmentEntity){
        repository.insert(data)
    }

    fun updateFlag(id: String, flag: Boolean){
        repository.updateFlag(id, flag)
    }

    fun delete(data: ServiceCaseEquipmentEntity){
        repository.delete(data)
    }

    fun deleteAll(){
        repository.deleteAll()
    }

}