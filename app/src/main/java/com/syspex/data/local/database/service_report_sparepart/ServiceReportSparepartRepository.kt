package com.syspex.data.local.database.service_report_sparepart

import android.app.Application
import androidx.lifecycle.LiveData
import com.syspex.data.local.database.AppDatabase
import com.syspex.data.local.database.GlobalDao
import com.syspex.data.model.SCListModel
import com.syspex.data.model.SRListModel
import com.syspex.data.remote.GetDataRequest

class ServiceReportSparepartRepository constructor(application: Application) {

    lateinit var dao : GlobalDao

    init {
        val database = AppDatabase.getInstance(application)
        if (database != null) {
            dao = database.dao()
        }
    }

    fun getAll() : LiveData<List<ServiceReportSparepartEntity>> {
        return dao.reportSparepartAll()
    }

    fun getByReportEquipmentId(reportEquipmentId: String) : LiveData<List<ServiceReportSparepartEntity>> {
        return dao.reportSparepartByReportEquipment(reportEquipmentId)
    }

    fun insertCheck(reportEquipmentId: String, partId: String, enumId: String) : LiveData<List<ServiceReportSparepartEntity>> {
        return dao.reportSparepartInsertCheck(reportEquipmentId, partId, enumId)
    }

    fun insert(entity: ServiceReportSparepartEntity) {
        val thread = object : Thread() {
            override fun run() {
                dao.reportSparepartInsert(entity)
            }
        }
        thread.start()
    }

    fun update(entity: ServiceReportSparepartEntity) {
        val thread = object : Thread() {
            override fun run() {
                dao.reportSparepartUpdate(entity)
            }
        }
        thread.start()
    }

    fun deleteByReportEquipmentId(reportEquipmentId: String) {
        val thread = object : Thread() {
            override fun run() {
                dao.reportSparepartDeleteByReportEquipmentId(reportEquipmentId)
            }
        }
        thread.start()
    }

    fun delete(entity: ServiceReportSparepartEntity) {
        val thread = object : Thread() {
            override fun run() {
                dao.reportSparepartDelete(entity)
            }
        }
        thread.start()
    }

    fun updateStatus(reportSparepartId: String, enumId: String, enumName: String, isUpload: Boolean) {
        val thread = object : Thread() {
            override fun run() {
                dao.reportSparepartUpdateStatus(reportSparepartId, enumId, enumName, isUpload)
            }
        }
        thread.start()
    }

}