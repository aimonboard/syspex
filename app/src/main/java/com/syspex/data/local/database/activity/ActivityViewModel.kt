package com.syspex.data.local.database.activity

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import com.syspex.data.remote.GetDataRequest

class ActivityViewModel constructor(application: Application) : AndroidViewModel(application) {
    private val repository : ActivityRepository = ActivityRepository(application)
    private val data : LiveData<List<ActivityEntity>>

    init {
        data = repository.getAll()
    }

    fun getAll(): LiveData<List<ActivityEntity>> {
        return data
    }

    fun getById(activityId: String) : LiveData<List<ActivityEntity>> {
        return repository.getById(activityId)
    }

    fun getBycase(caseId: String) : LiveData<List<ActivityEntity>> {
        return repository.getByCase(caseId)
    }

    fun getByAccount(accountId: String) : LiveData<List<ActivityEntity>> {
        return repository.getByAccount(accountId)
    }

    fun getByCall(callId: String) : LiveData<List<ActivityEntity>> {
        return repository.getByCall(callId)
    }

    fun insert(data: ActivityEntity){
        repository.insert(data)
    }

}