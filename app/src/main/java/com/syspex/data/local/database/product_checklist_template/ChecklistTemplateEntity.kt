package com.syspex.data.local.database.product_checklist_template

import androidx.room.Entity
import androidx.room.PrimaryKey
import androidx.room.TypeConverters
import com.syspex.data.local.database.ConverterEquipment
import com.syspex.data.remote.GetDataRequest

@Entity(tableName = "table_product_checklist_template")
data class ChecklistTemplateEntity (
    @PrimaryKey(autoGenerate = false)
    val productGroupChecklistTemplateId: String,
    val regionId: String,
    val productGroupId: String,
    val template_description: String,
    val createdDate: String,
    val createdBy: String,
    val lastModifiedDate: String,
    val lastModifiedBy: String,
    val checklistTypeTextEnumId: String,
    val checklistTypeTextEnumName: String
)