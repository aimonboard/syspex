package com.syspex.data.local.database.service_case_equipment

import android.app.Application
import androidx.lifecycle.LiveData
import com.syspex.data.local.database.AppDatabase
import com.syspex.data.local.database.GlobalDao
import com.syspex.data.model.EquipmentListModel
import com.syspex.data.model.SCListModel
import com.syspex.data.model.SRListModel
import com.syspex.data.remote.GetDataRequest

class ServiceCaseEquipmentRepository constructor(application: Application) {

    lateinit var dao : GlobalDao

    init {
        val database = AppDatabase.getInstance(application)
        if (database != null) {
            dao = database.dao()
        }
    }

    fun all() : LiveData<List<ServiceCaseEquipmentEntity>> {
        return dao.caseEquipmentall()
    }

    fun getByCaseId(caseId: String, isAttach: Boolean) : LiveData<List<EquipmentListModel>> {
        return dao.caseEquipmentByCaseId(caseId, isAttach)
    }

    fun checkAttach(caseId: String, equipmentId: String) : LiveData<List<ServiceCaseEquipmentEntity>> {
        return dao.caseEquipmentCheckAttach(caseId, equipmentId)
    }

    fun equipmentAdd(caseId: String, callId: String, searchText: String) : LiveData<List<ServiceCaseEquipmentEntity>> {
        return dao.caseEquipmentAdd(caseId, callId, searchText)
    }

    fun insert(entity: ServiceCaseEquipmentEntity) {
        val thread = object : Thread() {
            override fun run() {
                dao.caseEquipmentInsert(entity)
            }
        }
        thread.start()
    }

    fun updateFlag(id: String, flag: Boolean) {
        val thread = object : Thread() {
            override fun run() {
                dao.caseEquipmentUpdateFlag(id, flag)
            }
        }
        thread.start()
    }

    fun delete(entity: ServiceCaseEquipmentEntity) {
        val thread = object : Thread() {
            override fun run() {
                dao.caseEquipmentDelete(entity)
            }
        }
        thread.start()
    }

    fun deleteAll() {
        val thread = object : Thread() {
            override fun run() {
                dao.caseEquipmentDeleteAll()
            }
        }
        thread.start()
    }

}