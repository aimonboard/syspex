package com.syspex.data.local.database.service_call

import androidx.room.TypeConverter
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.syspex.data.remote.GetDataRequest
import java.lang.reflect.Type
import java.util.*


class ServiceCallConverterEngineer {
    var gson = Gson()

    @TypeConverter
    fun stringToSomeObjectList(data: String?): List<GetDataRequest.Engineer?>? {
        if (data == null) {
            return Collections.emptyList()
        }
        val listType: Type =
            object : TypeToken<List<GetDataRequest.Engineer?>?>() {}.type
        return gson.fromJson<List<GetDataRequest.Engineer?>>(data, listType)
    }

    @TypeConverter
    fun someObjectListToString(someObjects: List<GetDataRequest.Engineer?>?): String? {
        return gson.toJson(someObjects)
    }


}