package com.syspex.data.local.database.product_checklist_template

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import com.syspex.data.model.ChecklistJointMasterModel

class ChecklistTemplateViewModel constructor(application: Application) : AndroidViewModel(application) {
    private val repository : ChecklistTemplateRepository = ChecklistTemplateRepository(application)

    fun getAll(): LiveData<List<ChecklistTemplateEntity>> {
        return repository.getAll()
    }

    fun getTemplateByProductGroupId(productGroupId: String): LiveData<List<ChecklistTemplateEntity>> {
        return repository.getTemplateByProductGroupId(productGroupId)
    }

    fun getTemplateByProductGroupIdAndType(productGroupId: String, checklistTypeText: String): LiveData<List<ChecklistTemplateEntity>> {
        return repository.getTemplateByProductGroupIdAndType(productGroupId, checklistTypeText)
    }

    fun getTemplateJointQuestion(productGroupId: String, checklistTypeText: String): LiveData<List<ChecklistJointMasterModel>> {
        return repository.getTemplateJointQuestion(productGroupId, checklistTypeText)
    }

    fun insert(data: ChecklistTemplateEntity){
        repository.insert(data)
    }

    fun deleteAll(){
        repository.deleteAll()
    }

}