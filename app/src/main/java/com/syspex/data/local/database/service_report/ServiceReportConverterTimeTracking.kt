package com.syspex.data.local.database.service_report

import androidx.room.TypeConverter
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.syspex.data.remote.GetDataRequest
import java.lang.reflect.Type
import java.util.*


class ServiceReportConverterTimeTracking {
    var gson = Gson()

    @TypeConverter
    fun stringToSomeObjectList(data: String?): List<GetDataRequest.Data.ServiceCase.ServiceCall.ServiceReport.ServiceReportTimeTracking?>? {
        if (data == null) {
            return Collections.emptyList()
        }
        val listType: Type =
            object : TypeToken<List<GetDataRequest.Data.ServiceCase.ServiceCall.ServiceReport.ServiceReportTimeTracking?>?>() {}.type
        return gson.fromJson<List<GetDataRequest.Data.ServiceCase.ServiceCall.ServiceReport.ServiceReportTimeTracking?>>(data, listType)
    }

    @TypeConverter
    fun someObjectListToString(someObjects: List<GetDataRequest.Data.ServiceCase.ServiceCall.ServiceReport.ServiceReportTimeTracking?>?): String? {
        return gson.toJson(someObjects)
    }


}