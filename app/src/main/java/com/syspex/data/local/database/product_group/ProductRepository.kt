package com.syspex.data.local.database.product_group

import android.app.Application
import androidx.lifecycle.LiveData
import com.syspex.data.local.database.AppDatabase
import com.syspex.data.local.database.GlobalDao

class ProductRepository constructor(application: Application) {

    lateinit var dao : GlobalDao

    init {
        val database = AppDatabase.getInstance(application)
        if (database != null) {
            dao = database.dao()
        }
    }

    fun getAll(textSearch: String) : LiveData<List<ProductEntity>> {
        return dao.productGroupAll(textSearch)
    }

    fun productGroupJoinProduct(productId: String) : LiveData<List<ProductEntity>> {
        return dao.productGroupJoinProduct(productId)
    }

    fun getById(productGroupId: String) : LiveData<List<ProductEntity>> {
        return dao.productGroupById(productGroupId)
    }

    fun insert(entity: ProductEntity) {
        val thread = object : Thread() {
            override fun run() {
                dao.productGroupInsert(entity)
            }
        }
        thread.start()
    }

    fun delete(entity: ProductEntity) {
        val thread = object : Thread() {
            override fun run() {
                dao.productGroupDelete(entity)
            }
        }
        thread.start()
    }

    fun deleteAll() {
        val thread = object : Thread() {
            override fun run() {
                dao.productGroupDeleteAll()
            }
        }
        thread.start()
    }

}