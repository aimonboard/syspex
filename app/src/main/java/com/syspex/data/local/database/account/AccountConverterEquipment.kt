package com.syspex.data.local.database.account

import androidx.room.TypeConverter
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.syspex.data.remote.GetDataRequest
import java.lang.reflect.Type
import java.util.*


class AccountConverterEquipment {
    var gson = Gson()

    @TypeConverter
    fun stringToSomeObjectList(data: String?): List<GetDataRequest.EquipmentAccount?>? {
        if (data == null) {
            return Collections.emptyList()
        }
        val listType: Type =
            object : TypeToken<List<GetDataRequest.EquipmentAccount?>?>() {}.type
        return gson.fromJson<List<GetDataRequest.EquipmentAccount?>>(data, listType)
    }

    @TypeConverter
    fun someObjectListToString(someObjects: List<GetDataRequest.EquipmentAccount?>?): String? {
        return gson.toJson(someObjects)
    }


}