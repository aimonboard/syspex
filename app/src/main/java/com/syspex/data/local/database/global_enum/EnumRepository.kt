package com.syspex.data.local.database.global_enum

import android.app.Application
import androidx.lifecycle.LiveData
import com.syspex.data.local.database.AppDatabase
import com.syspex.data.local.database.GlobalDao
import com.syspex.data.model.SCListModel
import com.syspex.data.model.SRListModel
import com.syspex.data.remote.GetDataRequest

class EnumRepository constructor(application: Application) {

    lateinit var dao : GlobalDao

    init {
        val database = AppDatabase.getInstance(application)
        if (database != null) {
            dao = database.dao()
        }
    }

    fun getByTableName(tableName: String) : LiveData<List<EnumEntity>> {
        return dao.enumByTableName(tableName)
    }

    fun getByTableAndColumn(tableName: String, columnName: String) : LiveData<List<EnumEntity>> {
        return dao.enumByTableAndCoulumn(tableName, columnName)
    }

    fun insert(entity: EnumEntity) {
        val thread = object : Thread() {
            override fun run() {
                dao.enumInsert(entity)
            }
        }
        thread.start()
    }
}