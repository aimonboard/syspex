package com.syspex.data.local.database.email

import android.app.Application
import androidx.lifecycle.LiveData
import com.syspex.data.local.database.AppDatabase
import com.syspex.data.local.database.GlobalDao

class EmailRepository constructor(application: Application) {

    lateinit var dao : GlobalDao

    init {
        val database = AppDatabase.getInstance(application)
        if (database != null) {
            dao = database.dao()
        }
    }

    fun getAll() : List<EmailEntity> {
        return dao.emailAll()
    }

    fun insert(entity: EmailEntity) {
        val thread = object : Thread() {
            override fun run() {
                dao.emailInsert(entity)
            }
        }
        thread.start()
    }

    fun deleteById(emailId: Int) {
        val thread = object : Thread() {
            override fun run() {
                dao.emailDeleteById(emailId)
            }
        }
        thread.start()
    }

}