package com.syspex.data.local.database.account_address

import androidx.room.Entity
import androidx.room.PrimaryKey
import androidx.room.TypeConverters
import com.syspex.data.local.database.ConverterEquipment
import com.syspex.data.remote.GetDataRequest

@Entity(tableName = "table_account_address")
data class AccountAddressEntity (
    @PrimaryKey(autoGenerate = false)
    val account_address_id: String,
    val region_id: String,
    val account_id: String,
    val address_title: String,
    val address_line_1: String,
    val address_line_2: String,
    val address_line_3: String,
    val address_country: String,
    val address_city: String,
    val address_zip_code: String,
    val priority: String,
    val created_date: String,
    val created_by: String,
    val last_modified_date: String,
    val last_modified_by: String,
    val full_address: String
)