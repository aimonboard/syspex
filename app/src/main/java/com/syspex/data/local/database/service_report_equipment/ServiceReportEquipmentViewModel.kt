package com.syspex.data.local.database.service_report_equipment

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import com.syspex.data.model.SREquipmentJointModel

class ServiceReportEquipmentViewModel constructor(application: Application) : AndroidViewModel(application) {
    private val repository : ServiceReportEquipmentRepository = ServiceReportEquipmentRepository(application)

    fun checkAttach(callId: String, equipmentId: String, isAttach: Boolean): LiveData<List<ServiceReportEquipmentEntity>> {
        return repository.checkAttach(callId, equipmentId, isAttach)
    }

    fun getByReportId(reportId: String): LiveData<List<ServiceReportEquipmentEntity>> {
        return repository.getByReportId(reportId)
    }

    fun getByReportEquipmentId(reportEquipmentId: String): LiveData<List<ServiceReportEquipmentEntity>> {
        return repository.getByReportEquipmentId(reportEquipmentId)
    }

    fun getEquipmentAllByCallId(callId: String, isAttach: Boolean): LiveData<List<SREquipmentJointModel>> {
        return repository.getEquipmentAllByCallId(callId, isAttach)
    }

    fun getEquipmentDetail(reportId: String): LiveData<List<SREquipmentJointModel>> {
        return repository.getEquipmentDetail(reportId)
    }

    fun insert(data: ServiceReportEquipmentEntity){
        repository.insert(data)
    }

    fun editTemplateId(reportEquipmentId: String, templateId: String){
        repository.editTemplateId(reportEquipmentId, templateId)
    }

    fun edit(reportEquipmentId: String, statusAfterServiceEnumId: String, statusAfterServiceEnumName: String, statusAfterServiceRemarks: String, problem: String, isUpload: Boolean){
        repository.edit(reportEquipmentId, statusAfterServiceEnumId, statusAfterServiceEnumName, statusAfterServiceRemarks, problem, isUpload)
    }

    fun updateFlag(reportEquipmentId: String, isUpload: Boolean){
        repository.updateFlag(reportEquipmentId, isUpload)
    }

    fun delete(data: ServiceReportEquipmentEntity){
        repository.delete(data)
    }

    fun deleteById(reportEquipmentId: String){
        repository.deleteById(reportEquipmentId)
    }

    fun deleteByReportId(reportId: String){
        repository.deleteByReportId(reportId)
    }

}