package com.syspex.data.local.database.service_report

import androidx.room.Entity
import androidx.room.PrimaryKey
import androidx.room.TypeConverters
import com.syspex.data.local.database.service_call.ServiceCallConverterEngineer
import com.syspex.data.remote.GetDataRequest

@Entity(tableName = "table_service_report")
data class ServiceReportEntity (
    @PrimaryKey (autoGenerate = false)
    val reportLocalId: String,
    val reportServerId: String,
    val caseId: String,
    val accountId: String,
    val accountName: String,
    val callId: String,
    val serviceReportNumber: String,
    val reportDate: String,
    val internalMemo: String,
    val customerRating: String,
    val feedback: String,
    val cpNumber: String,
    val cpName: String,
    val servicedBySignatureFileName: String,
    val servicedBySignatureFileImage: String,
    val accountPicSignatureFileName: String,
    val accountPicSignatureFileImage: String,
    val accountPicSignatureAt: String,
    val isChargeable: String,
    val employeeId: String,
    val employeeName: String,
    val createdDate: String,
    val lastModifiedDate: String,
    val serviceReportNumberTemp: String,

    val regionId: String,
    val regionName: String,

    val fieldAccountContactId: String,
    val fieldAccountContactName: String,
    val fieldAccountContactPhone: String,

    val servicedByUserId: String,
    val servicedByUserName: String,

    val createdByUserId: String,
    val createdByUserName: String,

    val modifiedByUserId: String,
    val modifiedByUserName: String,

    val reportStatusEnumId: String,
    val reportStatusEnumText: String,

    val reportTypeEnumId: String,
    val reportTypeEnumText: String,

    val colorBackground: String? = "",
    val colorStatusLabel: String? = "",
    val colorStatusLabelText: String? = "",

    @TypeConverters(ServiceReportConverterTimeTracking::class)
    val timeTracking: List<GetDataRequest.Data.ServiceCase.ServiceCall.ServiceReport.ServiceReportTimeTracking?>? = null,

    @TypeConverters(ServiceCallConverterEngineer::class)
    val engineer: List<GetDataRequest.Engineer?>? = null,

    @TypeConverters(ServiceReportConverterEquipment::class)
    val equipment: List<GetDataRequest.Data.ServiceCase.ServiceCall.ServiceReport.ServiceReportEquipment?>? = null,

    val isUpload: Boolean,
    val isLocal: Boolean

)