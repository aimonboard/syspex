package com.syspex.data.local.database.account_contact

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData

class AccountContactViewModel constructor(application: Application) : AndroidViewModel(application) {
    private val repository : AccountContactRepository = AccountContactRepository(application)

    fun search(searchText: String): LiveData<List<AccountContactEntity>> {
        return repository.search(accountName=searchText, contactName=searchText)
    }

    fun searchInAccountDetail(accountId: String, contactName: String): LiveData<List<AccountContactEntity>> {
        return repository.searchInAccountDetail(accountId, contactName)
    }

    fun getAll(): LiveData<List<AccountContactEntity>> {
        return repository.getAll()
    }

    fun getById(contactId: String): LiveData<List<AccountContactEntity>> {
        return repository.getById(contactId)
    }

    fun getByAccountId(accountId: String): LiveData<List<AccountContactEntity>> {
        return repository.getByAccountId(accountId)
    }

    fun insert(data: AccountContactEntity){
        repository.insert(data)
    }

    fun updateFlag(entity: AccountContactEntity){
        repository.updateFlag(entity)
    }

}