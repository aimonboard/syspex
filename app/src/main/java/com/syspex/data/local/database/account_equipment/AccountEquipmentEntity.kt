package com.syspex.data.local.database.account_equipment

import androidx.room.Entity
import androidx.room.PrimaryKey
import androidx.room.TypeConverters
import com.syspex.data.local.database.ConverterEquipment
import com.syspex.data.remote.GetDataRequest

@Entity(tableName = "table_account_equipment")
data class AccountEquipmentEntity (
    @PrimaryKey(autoGenerate = false)
    val equipmentId: String,
    val regionId: String,
    val serialNo: String,
    val brand: String,
    val warrantyStartDate: String,
    val warrantyEndDate: String,
    val equipmentRemarks: String,
    val deliveryAddressId: String,
    val salesOrderNo: String,
    val warrantyStatus: String,

    val accountId: String,
    val accountName: String,

    val agreementId: String,
    val agreementNumber: String,
    val agreementEnd: String,

    val statusTextEnumId: String,
    val statusTextEnumText: String,

    val product_id: String,
    val product_name: String,

    val isAttach: Boolean

)