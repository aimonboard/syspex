package com.syspex.data.local.database.service_report_time_tracking

import android.app.Application
import androidx.lifecycle.LiveData
import com.syspex.data.local.database.AppDatabase
import com.syspex.data.local.database.GlobalDao
import com.syspex.data.model.SCListModel
import com.syspex.data.model.SRListModel
import com.syspex.data.remote.GetDataRequest

class ServiceReportTimeTrackingRepository constructor(application: Application) {

    lateinit var dao : GlobalDao

    init {
        val database = AppDatabase.getInstance(application)
        if (database != null) {
            dao = database.dao()
        }
    }

    fun getByReportId(reportId: String) : LiveData<List<ServiceReportTimeTrackingEntity>> {
        return dao.reportTimeTrackingByReportId(reportId)
    }

    fun getByCallId(callId: String) : LiveData<List<ServiceReportTimeTrackingEntity>> {
        return dao.reportTimeTrackingByCallId(callId)
    }

    fun getByCallIdBackground(callId: String) : List<ServiceReportTimeTrackingEntity> {
        return dao.reportTimeTrackingByCallIdBackground(callId)
    }

    fun insert(entity: ServiceReportTimeTrackingEntity) {
        val thread = object : Thread() {
            override fun run() {
                dao.reportTimeTrackingInsert(entity)
            }
        }
        thread.start()
    }

    fun delete(entity: ServiceReportTimeTrackingEntity) {
        val thread = object : Thread() {
            override fun run() {
                dao.reportTimeTrackingDelete(entity)
            }
        }
        thread.start()
    }

    fun deleteById(id: String) {
        val thread = object : Thread() {
            override fun run() {
                dao.reportTimeTrackingDeleteById(id)
            }
        }
        thread.start()
    }

    fun deleteAll() {
        val thread = object : Thread() {
            override fun run() {
                dao.reportTimeTrackingDeleteAll()
            }
        }
        thread.start()
    }

}