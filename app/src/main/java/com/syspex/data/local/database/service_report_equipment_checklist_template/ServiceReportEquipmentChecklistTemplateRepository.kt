package com.syspex.data.local.database.service_report_equipment_checklist_template

import android.app.Application
import androidx.lifecycle.LiveData
import com.syspex.data.local.database.AppDatabase
import com.syspex.data.local.database.GlobalDao
import com.syspex.data.model.ChecklistJointMasterModel
import com.syspex.data.model.ChecklistJointReportModel

class ServiceReportEquipmentChecklistTemplateRepository constructor(application: Application) {

    lateinit var dao : GlobalDao

    init {
        val database = AppDatabase.getInstance(application)
        if (database != null) {
            dao = database.dao()
        }
    }

    fun getAll() : LiveData<List<ServiceReportEquipmentChecklistTemplateEntity>> {
        return dao.reportChecklistAll()
    }

    fun getByReportEquipmnet(reportEquipmentId: String) : LiveData<List<ChecklistJointReportModel>> {
        return dao.reportChecklistTemplateByReportEquipment(reportEquipmentId)
    }

    fun insert(entity: ServiceReportEquipmentChecklistTemplateEntity) {
        val thread = object : Thread() {
            override fun run() {
                dao.reportChecklistTemplateInsert(entity)
            }
        }
        thread.start()
    }

    fun deleteAll() {
        val thread = object : Thread() {
            override fun run() {
                dao.reportChecklistTemplateDeleteAll()
            }
        }
        thread.start()
    }

}