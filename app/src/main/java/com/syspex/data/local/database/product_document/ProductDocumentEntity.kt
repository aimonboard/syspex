package com.syspex.data.local.database.product_document

import androidx.room.Entity
import androidx.room.PrimaryKey
import androidx.room.TypeConverters
import com.syspex.data.local.database.ConverterEquipment
import com.syspex.data.remote.GetDataRequest

@Entity(tableName = "table_product_document")
data class ProductDocumentEntity (
    @PrimaryKey(autoGenerate = false)
    val product_group_technical_document_id: String,
    val product_group_id: String,
    val title: String,
    val url: String,
    val created_date: String,
    val created_by: String,
    val last_modified_date: String,
    val last_modified_by: String
)