package com.syspex.data.local.database.product_sparepart

import androidx.room.Entity
import androidx.room.PrimaryKey
import androidx.room.TypeConverters
import com.syspex.data.local.database.ConverterEquipment
import com.syspex.data.remote.GetDataRequest

@Entity(tableName = "table_product_sparepart")
data class SparepartEntity (
    @PrimaryKey(autoGenerate = false)
    val product_group_spare_part_id: String,
    val product_group_id: String,
    val image_number: String,

    val part_id: String,
    val part_no: String,
    val part_name: String,

    val technical_drawing_id: String,
    val title: String,
    val page: String,
    val url: String
)