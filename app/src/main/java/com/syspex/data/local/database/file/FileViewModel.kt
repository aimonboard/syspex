package com.syspex.data.local.database.file

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import com.syspex.data.local.database.file.FileEntity
import com.syspex.data.local.database.file.FileRepository

class FileViewModel constructor(application: Application) : AndroidViewModel(application) {
    private val repository : FileRepository =
        FileRepository(application)
    private val data : LiveData<List<FileEntity>>

    init {
        data = repository.getAll()
    }

    fun getAll(): LiveData<List<FileEntity>> {
        return data
    }

    fun getByName(fileName: String): LiveData<List<FileEntity>> {
        return repository.getByName(fileName)
    }

    fun insert(data: FileEntity){
        repository.insert(data)
    }

    fun delete(data: FileEntity){
        repository.delete(data)
    }

    fun deleteAll(){
        repository.deleteAll()
    }

}