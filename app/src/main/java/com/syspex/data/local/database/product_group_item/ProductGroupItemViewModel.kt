package com.syspex.data.local.database.product_group_item

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData

class ProductGroupItemViewModel constructor(application: Application) : AndroidViewModel(application) {
    private val repository : ProductGroupItemRepository = ProductGroupItemRepository(application)

    fun getAll(): LiveData<List<ProductGroupItemEntity>> {
        return repository.getAll()
    }

    fun getByProductId(productId: String): LiveData<List<ProductGroupItemEntity>> {
        return repository.getByProductId(productId)
    }

    fun insert(data: ProductGroupItemEntity){
        repository.insert(data)
    }

}