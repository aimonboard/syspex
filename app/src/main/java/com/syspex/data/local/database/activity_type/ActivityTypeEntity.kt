package com.syspex.data.local.database.activity_type

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "table_activity_type")
data class ActivityTypeEntity (
    @PrimaryKey(autoGenerate = false)
    val activity_type_id: String,
    val activity_type_code: String,
    val activity_type_name: String,
    val activity_type_color: String
)