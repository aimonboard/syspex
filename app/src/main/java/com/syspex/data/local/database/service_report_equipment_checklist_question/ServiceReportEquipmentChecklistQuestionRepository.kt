package com.syspex.data.local.database.service_report_equipment_checklist_question

import android.app.Application
import androidx.lifecycle.LiveData
import com.syspex.data.local.database.AppDatabase
import com.syspex.data.local.database.GlobalDao
import com.syspex.data.local.database.service_call.ServiceCallEntity
import com.syspex.data.model.ConverterImageModel
import com.syspex.data.model.SCListModel
import com.syspex.data.model.SRListModel
import com.syspex.data.remote.GetDataRequest

class ServiceReportEquipmentChecklistQuestionRepository constructor(application: Application) {

    lateinit var dao : GlobalDao

    init {
        val database = AppDatabase.getInstance(application)
        if (database != null) {
            dao = database.dao()
        }
    }

    fun getAll(): LiveData<List<ServiceReportEquipmentChecklistQuestionEntity>> {
        return dao.reportChecklistQuestionAll()
    }

    fun insert(entity: ServiceReportEquipmentChecklistQuestionEntity) {
        val thread = object : Thread() {
            override fun run() {
                dao.reportChecklistQuestionInsert(entity)
            }
        }
        thread.start()
    }

    fun updateStatus(questionId: String, enumId: String, enumName: String, isUpload: Boolean) {
        val thread = object : Thread() {
            override fun run() {
                dao.reportChecklistQuestionUpdateStatus(questionId, enumId, enumName, isUpload)
            }
        }
        thread.start()
    }

    fun updateImage(questionId: String, checklistImage: ArrayList<ConverterImageModel>, isUpload: Boolean) {
        val thread = object : Thread() {
            override fun run() {
                dao.reportChecklistQuestionUpdateImage(questionId, checklistImage, isUpload)
            }
        }
        thread.start()
    }

    fun deleteAll() {
        val thread = object : Thread() {
            override fun run() {
                dao.reportChecklistQuestionDeleteAll()
            }
        }
        thread.start()
    }

}