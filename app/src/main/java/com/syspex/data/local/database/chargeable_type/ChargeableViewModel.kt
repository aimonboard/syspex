package com.syspex.data.local.database.chargeable_type

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData

class ChargeableViewModel constructor(application: Application) : AndroidViewModel(application) {
    private val repository : ChargeableRepository = ChargeableRepository(application)

    fun getAll(): LiveData<List<ChargeableEntity>> {
        return repository.getAll()
    }

    fun insert(data: ChargeableEntity){
        repository.insert(data)
    }
}