package com.syspex.data.local.database.account_equipment

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import com.syspex.data.local.database.service_report.ServiceReportEntity
import com.syspex.data.model.EquipmentListModel
import com.syspex.data.model.SCListModel
import com.syspex.data.model.SRListModel
import com.syspex.data.remote.GetDataRequest

class AccountEquipmentViewModel constructor(application: Application) : AndroidViewModel(application) {
    private val repository : AccountEquipmentRepository = AccountEquipmentRepository(application)

    fun getList(textSearch: String, filterArray: ArrayList<String>): LiveData<List<EquipmentListModel>> {
        return repository.getList(textSearch, filterArray)
    }

    fun search(textSearch: String): LiveData<List<EquipmentListModel>> {
        return repository.search(textSearch)
    }

    fun getAll(): LiveData<List<AccountEquipmentEntity>> {
        return repository.getAll()
    }

    fun getByEquipmentId(equipmentId: String): LiveData<List<AccountEquipmentEntity>> {
        return repository.getByEquipmentId(equipmentId)
    }

    fun getByAgreementId(agreementId: String): LiveData<List<AccountEquipmentEntity>> {
        return repository.getByAgreementId(agreementId)
    }

    fun getByAccountId(accountId: String): LiveData<List<EquipmentListModel>> {
        return repository.getByAccountId(accountId)
    }

    fun getByAccount(accountId: String): LiveData<List<AccountEquipmentEntity>> {
        return repository.getByAccount(accountId)
    }

    fun equipmentAdd(accountId: String, caseId: String, searchText: String): LiveData<List<AccountEquipmentEntity>> {
        return repository.equipmentAdd(accountId, caseId, searchText)
    }

    fun insert(data: AccountEquipmentEntity){
        repository.insert(data)
    }

    fun updateFlag(id: String, flag: Boolean){
        repository.updateFlag(id, flag)
    }

    fun delete(data: AccountEquipmentEntity){
        repository.delete(data)
    }

    fun deleteAll(){
        repository.deleteAll()
    }

}