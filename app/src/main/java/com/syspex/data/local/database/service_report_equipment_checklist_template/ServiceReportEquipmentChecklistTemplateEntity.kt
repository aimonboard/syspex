package com.syspex.data.local.database.service_report_equipment_checklist_template

import androidx.room.Entity
import androidx.room.PrimaryKey
import androidx.room.TypeConverters
import com.syspex.data.local.database.ConverterEquipment
import com.syspex.data.remote.GetDataRequest

@Entity(tableName = "table_service_report_equipment_checklist_template")
data class ServiceReportEquipmentChecklistTemplateEntity (
    @PrimaryKey(autoGenerate = false)
    val serviceReportEquipmentChecklistId: String,
    val reportEquipmentId_template: String,
    val templateId: String,
    val template_description: String,
    val remarks: String,
    val createdDate: String,
    val createdBy: String,
    val lastModifiedDate: String,
    val lastModifiedBy: String,
    val statusTemplateEnumId: String,
    val statusTemplateEnumName: String,

    val isLocal: Boolean

)