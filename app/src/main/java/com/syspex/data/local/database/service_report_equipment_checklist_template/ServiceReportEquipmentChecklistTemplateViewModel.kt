package com.syspex.data.local.database.service_report_equipment_checklist_template

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import com.syspex.data.model.ChecklistJointMasterModel
import com.syspex.data.model.ChecklistJointReportModel

class ServiceReportEquipmentChecklistTemplateViewModel constructor(application: Application) : AndroidViewModel(application) {
    private val repository : ServiceReportEquipmentChecklistTemplateRepository = ServiceReportEquipmentChecklistTemplateRepository(application)

    fun getAll(): LiveData<List<ServiceReportEquipmentChecklistTemplateEntity>> {
        return repository.getAll()
    }

    fun getTemplateJointQuestion(reportEquipmentId: String): LiveData<List<ChecklistJointReportModel>> {
        return repository.getByReportEquipmnet(reportEquipmentId)
    }

    fun insert(data: ServiceReportEquipmentChecklistTemplateEntity){
        repository.insert(data)
    }

    fun deleteAll(){
        repository.deleteAll()
    }

}