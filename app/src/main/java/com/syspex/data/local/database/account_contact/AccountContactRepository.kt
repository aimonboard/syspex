package com.syspex.data.local.database.account_contact

import android.app.Application
import androidx.lifecycle.LiveData
import com.syspex.data.local.database.AppDatabase
import com.syspex.data.local.database.GlobalDao

class AccountContactRepository constructor(application: Application) {

    lateinit var dao : GlobalDao

    init {
        val database = AppDatabase.getInstance(application)
        if (database != null) {
            dao = database.dao()
        }
    }

    fun search(accountName: String, contactName: String) : LiveData<List<AccountContactEntity>> {
        return dao.accountContactSearch(accountName = accountName, contactName = contactName)
    }

    fun searchInAccountDetail(accountId: String, contactName: String) : LiveData<List<AccountContactEntity>> {
        return dao.accountContactSearchInAccountDetail(accountId, contactName)
    }

    fun getAll() : LiveData<List<AccountContactEntity>> {
        return dao.accountContactAll()
    }

    fun getById(contactId: String) : LiveData<List<AccountContactEntity>> {
        return dao.accountContactById(contactId)
    }

    fun getByAccountId(accountId: String) : LiveData<List<AccountContactEntity>> {
        return dao.accountContactByAccountId(accountId)
    }

    fun insert(entity: AccountContactEntity) {
        val thread = object : Thread() {
            override fun run() {
                dao.accountContactInsert(entity)
            }
        }
        thread.start()
    }

    fun updateFlag(entity: AccountContactEntity) {
        val thread = object : Thread() {
            override fun run() {
                dao.accountContactUpdate(entity)
            }
        }
        thread.start()
    }

}