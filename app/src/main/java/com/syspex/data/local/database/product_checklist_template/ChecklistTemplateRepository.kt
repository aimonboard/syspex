package com.syspex.data.local.database.product_checklist_template

import android.app.Application
import androidx.lifecycle.LiveData
import com.syspex.data.local.database.AppDatabase
import com.syspex.data.local.database.GlobalDao
import com.syspex.data.model.ChecklistJointMasterModel

class ChecklistTemplateRepository constructor(application: Application) {

    lateinit var dao : GlobalDao

    init {
        val database = AppDatabase.getInstance(application)
        if (database != null) {
            dao = database.dao()
        }
    }

    fun getAll() : LiveData<List<ChecklistTemplateEntity>> {
        return dao.checklistAll()
    }

    fun getTemplateByProductGroupId(productGroupId: String) : LiveData<List<ChecklistTemplateEntity>> {
        return dao.checklistTemplateByProductGroupId(productGroupId)
    }

    fun getTemplateByProductGroupIdAndType(productGroupId: String, checklistTypeText: String) : LiveData<List<ChecklistTemplateEntity>> {
        return dao.checklistTemplateByProductGroupIdAndType(productGroupId, checklistTypeText)
    }

    fun getTemplateJointQuestion(productGroupId: String, checklistTypeText: String) : LiveData<List<ChecklistJointMasterModel>> {
        return dao.checklistTemplateJointQuestion(productGroupId, checklistTypeText)
    }

    fun insert(entity: ChecklistTemplateEntity) {
        val thread = object : Thread() {
            override fun run() {
                dao.checklistTemplateInsert(entity)
            }
        }
        thread.start()
    }

    fun deleteAll() {
        val thread = object : Thread() {
            override fun run() {
                dao.checklistTemplateDeleteAll()
            }
        }
        thread.start()
    }

}