package com.syspex.data.local.database.service_report_equipment_checklist_question

import androidx.room.Entity
import androidx.room.PrimaryKey
import androidx.room.TypeConverters
import com.google.gson.annotations.SerializedName
import com.syspex.data.local.database.ConverterEquipment
import com.syspex.data.local.database.ConverterImage
import com.syspex.data.model.ConverterImageModel
import com.syspex.data.remote.GetDataRequest

@Entity(tableName = "table_service_report_equipment_checklist_question")
data class ServiceReportEquipmentChecklistQuestionEntity (
    @PrimaryKey(autoGenerate = false)
    val id: String,
    val orderNo: String,
    val sectionTitle: String,
    val question_description: String,
    val defaultRemarks: String,
    val helpText: String,
    val helpImageUrl: String,
    val statusQuestionEnumId: String,
    val statusQuestionEnumName: String,
    val createdDate: String,
    val createdBy: String,
    val lastModifiedDate: String,
    val lastModifiedBy: String,
    val checklistTemplateId: String,
    val checklistImage: List<ConverterImageModel>? = null,

    val isUpload: Boolean,
    val islocal: Boolean
)