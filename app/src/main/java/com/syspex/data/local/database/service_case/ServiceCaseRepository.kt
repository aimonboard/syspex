package com.syspex.data.local.database.service_case

import android.app.Application
import androidx.lifecycle.LiveData
import com.syspex.data.local.database.AppDatabase
import com.syspex.data.local.database.GlobalDao

class ServiceCaseRepository constructor(application: Application) {

    lateinit var dao : GlobalDao

    init {
        val database = AppDatabase.getInstance(application)
        if (database != null) {
            dao = database.dao()
        }
    }

    fun getAll(searchText: String, filterArray: ArrayList<String>) : LiveData<List<ServiceCaseEntity>> {
        return dao.caseAll(searchText, filterArray)
    }

    fun search(number: String, accountName: String, subject: String) : LiveData<List<ServiceCaseEntity>> {
        return dao.caseSearch(number, accountName, subject)
    }

    fun getByCaseId(caseId: String) : LiveData<List<ServiceCaseEntity>> {
        return dao.caseByCaseId(caseId)
    }

    fun getByAccountId(accountId: String) : LiveData<List<ServiceCaseEntity>> {
        return dao.caseByAccountId(accountId)
    }

    fun getByAgreementId(agreementId: String) : LiveData<List<ServiceCaseEntity>> {
        return dao.caseByAgreementId(agreementId)
    }

    fun caseEquipmentRelated(equipmentId: String) : LiveData<List<ServiceCaseEntity>> {
        return dao.caseEquipmentRelated(equipmentId)
    }

    fun insert(entity: ServiceCaseEntity) {
        val thread = object : Thread() {
            override fun run() {
                dao.caseInsert(entity)
            }
        }
        thread.start()
    }

    fun delete(entity: ServiceCaseEntity) {
        val thread = object : Thread() {
            override fun run() {
                dao.caseDelete(entity)
            }
        }
        thread.start()
    }

    fun deleteAll() {
        val thread = object : Thread() {
            override fun run() {
                dao.caseDeleteAll()
            }
        }
        thread.start()
    }

    fun updateFlag(caseId: String, isUpload: Boolean) {
        val thread = object : Thread() {
            override fun run() {
                dao.caseUpdateFlag(caseId, isUpload)
            }
        }
        thread.start()
    }

}