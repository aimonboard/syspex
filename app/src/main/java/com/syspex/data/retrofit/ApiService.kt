package com.syspex.data.retrofit

import com.syspex.data.remote.*
import okhttp3.MultipartBody
import okhttp3.RequestBody
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.http.*


interface ApiService {

    @Headers("Accept: application/json")
    @FormUrlEncoded
    @POST("api/login")
    fun loginRequest(
        @FieldMap body: HashMap<String, String>
    ): Call<LoginRequest>

    @Headers("Accept: application/json")
    @FormUrlEncoded
    @POST("api/save-profile")
    fun updateProfile(
        @Header("Authorization") token: String,
        @FieldMap body: HashMap<String, String>
    ): Call<LoginRequest>

    @Headers("Accept: application/json")
    @FormUrlEncoded
    @POST("api/save-password")
    fun updatePassword(
        @Header("Authorization") token: String,
        @FieldMap body: HashMap<String, String>
    ): Call<ResponseBody>

    @Headers("Accept: application/json")
    @FormUrlEncoded
    @POST("api/forgot-password")
    fun forgotPassword(
        @FieldMap body: HashMap<String, String>
    ): Call<ResponseBody>

    @Headers("Accept: application/json")
    @POST("api/logout")
    fun logout(
        @Header("Authorization") token: String
    ): Call<ResponseBody>

    @Headers("Accept: application/json")
    @Multipart
    @POST("api/profile/upload-signature")
    fun uploadUserSignature(
        @Header("Authorization") token: String,
        @Part file: MultipartBody.Part
    ): Call<LoginRequest>

    @Headers("Accept: application/json")
    @GET("api/get-notification")
    fun getNotif(
        @Header("Authorization") token: String,
        @Query("page") page: Int
    ): Call<NotifRequest>

    @Headers("Accept: application/json")
    @GET("api/get-news")
    fun getNews(
        @Header("Authorization") token: String,
        @Query("page") page: Int
    ): Call<NewsRequestTest>

    @Headers("Accept: application/json")
    @POST("api/get-enum-list")
    fun getEnum(
        @Header("Authorization") token: String
    ): Call<EnumRequest>

    @Headers("Accept: application/json")
    @POST("api/get-status-color")
    fun getColor(
        @Header("Authorization") token: String
    ): Call<ColorRequest>

    @Headers("Accept: application/json")
    @GET("api/get-chargeable-type")
    fun getChargeable(
        @Header("Authorization") token: String
    ): Call<ChargeableRequest>

    @Headers("Accept: application/json")
    @GET("api/sync-data-v2")
    fun getData(
        @Header("Authorization") token: String,
        @Query("page") page: Int,
        @Query("only_my_call") onlyMyCall: String,
        @Query("first_sync") firstSync: String,
        @Query("from_date") fromDate: String?,
        @Query("to_date") toDate: String?,
        @Query("region_id") regionId: String?,
    ): Call<GetDataRequest>

    @Headers("Accept: application/json")
    @GET("api/get-activity")
    fun getActivity(
        @Header("Authorization") token: String,
        @Query("page") page: Int
    ): Call<GetActivityRequest>

    @Headers("Accept: application/json")
    @POST("api/get-activity-type")
    fun getActivityType(
        @Header("Authorization") token: String
    ): Call<GetActivityTypeRequest>

    @Headers("Accept: application/json")
    @GET("api/get-equipment")
    fun searchEquipment(
        @Header("Authorization") token: String,
        @Query("page") page: Int,
        @Query("keyword") searchText: String
    ): Call<SearchEquipmentOnlineRequest>

    @Headers("Accept: application/json")
    @GET("api/get-account")
    fun searchAccount(
        @Header("Authorization") token: String,
        @Query("page") page: Int,
        @Query("keyword") searchText: String
    ): Call<SearchAccountOnlineRequest>

    @FormUrlEncoded
    @Headers("Accept: application/json")
    @POST("api/account/get-by-id")
    fun accountRequest(
        @Header("Authorization") token: String,
        @FieldMap body: HashMap<String, Any>
    ): Call<AccountRequest>

    @FormUrlEncoded
    @Headers("Accept: application/json")
    @POST("api/case/get-all-by-equipment")
    fun syncEquipment(
        @Header("Authorization") token: String,
        @FieldMap body: HashMap<String, Any>
    ): Call<GetDataRequest>

    @FormUrlEncoded
    @Headers("Accept: application/json")
    @POST("api/export-data")
    fun sendData(
        @Header("Authorization") token: String,
        @FieldMap body: HashMap<String, Any>
    ): Call<SendDataRequest>

    @FormUrlEncoded
    @Headers("Accept: application/json")
    @POST("api/export-data-v2")
    fun sendDataV2(
        @Header("Authorization") token: String,
        @FieldMap body: HashMap<String, Any>
    ): Call<GetDataRequest>

    @FormUrlEncoded
    @Headers("Accept: application/json")
    @POST("api/service-report/sign-all")
    fun signAllReport(
        @Header("Authorization") token: String,
        @FieldMap body: HashMap<String, String>
    ): Call<SignAllRequest>

    @FormUrlEncoded
    @Headers("Accept: application/json")
    @POST("api/get-case-by-id")
    fun getByCaseId(
        @Header("Authorization") token: String,
        @FieldMap body: HashMap<String, Any>
    ): Call<GetDataRequest>

    @FormUrlEncoded
    @Headers("Accept: application/json")
    @POST("api/service-call/request")
    fun callRequest(
        @Header("Authorization") token: String,
        @FieldMap body: HashMap<String, Any>
    ): Call<ResponseBody>

    @FormUrlEncoded
    @Headers("Accept: application/json")
    @POST("api/service-report/cancel")
    fun cancelReport(
        @Header("Authorization") token: String,
        @FieldMap body: HashMap<String, Any>
    ): Call<ResponseBody>

    @FormUrlEncoded
    @Headers("Accept: application/json")
    @POST("api/email/report")
    fun sendEmail(
        @Header("Authorization") token: String,
        @FieldMap body: HashMap<String, Any>
    ): Call<SendEmailRequest>

    @FormUrlEncoded
    @Headers("Accept: application/json")
    @POST("api/account-contact/save")
    fun sendContact(
        @Header("Authorization") token: String,
        @FieldMap body: HashMap<String, Any>
    ): Call<ContactRequest>

    @Multipart
    @Headers("Accept: application/json")
    @POST("api/service-call/add-training")
    fun sendTraining(
        @Header("Authorization") token: String,
        @PartMap body: HashMap<String, RequestBody>
    ): Call<TrainingAddRequest>

    @Multipart
    @Headers("Accept: application/json")
    @POST("api/service-call/add-handover")
    fun sendHandover(
        @Header("Authorization") token: String,
        @PartMap body: HashMap<String, RequestBody>
    ): Call<HandoverAddRequest>

    @Multipart
    @Headers("Accept: application/json")
    @POST("api/account-attachment/save")
    fun sendAttachment(
        @Header("Authorization") token: String,
        @PartMap body: HashMap<String, RequestBody>
    ): Call<AttachmentRequest>

    @Headers("Accept: application/json")
    @GET
    fun download(
        @Url url: String
    ): Call<ResponseBody>

    @Headers("Accept: application/json")
    @GET
    fun downloadWithToken(
        @Header("Authorization") token: String,
        @Url url: String
    ): Call<ResponseBody>

    @FormUrlEncoded
    @Headers("Accept: application/json")
    @POST("api/update-fcm-token")
    fun sendFcmToken(
        @Header("Authorization") token: String,
        @FieldMap body: HashMap<String, String>
    ): Call<ResponseBody>

}