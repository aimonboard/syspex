package com.syspex.ui

import android.annotation.SuppressLint
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import androidx.core.widget.NestedScrollView
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.tabs.TabLayout
import com.livinglifetechway.quickpermissions_kotlin.runWithPermissions
import com.syspex.Menu2
import com.syspex.R
import com.syspex.data.local.database.product_checklist_template.ChecklistTemplateViewModel
import com.syspex.data.local.database.product_document.ProductDocumentViewModel
import com.syspex.data.local.database.product_group.ProductViewModel
import com.syspex.data.local.database.product_sparepart.SparepartEntity
import com.syspex.data.local.database.product_sparepart.SparepartViewModel
import com.syspex.ui.adapter.*
import com.syspex.ui.helper.DownloadPdf
import com.syspex.ui.helper.FunHelper
import kotlinx.android.synthetic.main.activity_product_detail.*
import kotlinx.android.synthetic.main.activity_product_detail.back_link
import kotlinx.android.synthetic.main.activity_product_detail.document_lay
import kotlinx.android.synthetic.main.activity_product_detail.head_checklist
import kotlinx.android.synthetic.main.activity_product_detail.head_document
import kotlinx.android.synthetic.main.activity_product_detail.head_information
import kotlinx.android.synthetic.main.activity_product_detail.machine_brand_value
import kotlinx.android.synthetic.main.activity_product_detail.machine_class_value
import kotlinx.android.synthetic.main.activity_product_detail.machine_model_value
import kotlinx.android.synthetic.main.activity_product_detail.recycler_check_list
import kotlinx.android.synthetic.main.activity_product_detail.recycler_document
import kotlinx.android.synthetic.main.activity_product_detail.scroll_lay
import kotlinx.android.synthetic.main.activity_product_detail.tab_lay
import kotlinx.android.synthetic.main.activity_product_detail.technical_air_pressure_value
import kotlinx.android.synthetic.main.activity_product_detail.technical_air_supply_value
import kotlinx.android.synthetic.main.activity_product_detail.technical_information_value
import kotlinx.android.synthetic.main.activity_product_detail.technical_product_value
import kotlinx.android.synthetic.main.activity_product_detail.technical_supplier_value
import kotlinx.android.synthetic.main.activity_product_detail.technical_tag_value
import kotlinx.android.synthetic.main.activity_product_detail.warning_document

class ProductDetail : AppCompatActivity() {

    private lateinit var productVM : ProductViewModel
    private lateinit var sparepartVM : SparepartViewModel
    private lateinit var checklistVM : ChecklistTemplateViewModel
    private lateinit var productDocumentVM: ProductDocumentViewModel

    private val sparepartAdapter = SparePartMasterAdapter()
    private val checklistAdapter = ChecklistMasterAdapter()
    private val documentAdapter = TechnicalDocumentAdapter()

    private var isScrolling = false
    private var productGroupId = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_product_detail)
        productVM = ViewModelProviders.of(this).get(ProductViewModel::class.java)
        sparepartVM = ViewModelProviders.of(this).get(SparepartViewModel::class.java)
        checklistVM = ViewModelProviders.of(this).get(ChecklistTemplateViewModel::class.java)
        productDocumentVM = ViewModelProviders.of(this).get(ProductDocumentViewModel::class.java)

        generateView()
        buttonListener()
        setupAdapter()
        documentAdapterListener()
        generateTab()
        tabListener()

    }

    private fun generateView() {
        if (intent.hasExtra("productGroupId")) {
            productGroupId = intent.getStringExtra("productGroupId") ?: ""
            header_name.text = intent.getStringExtra("productGroupName")

            productObserver()
            sparepartObserver()
            checklistObserver()
            documentObserver()
        }
    }

    private fun buttonListener() {
        back_link.setOnClickListener {
            onBackPressed()
        }

        btn_home.setOnClickListener {
            val intent = Intent(this, Menu2::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_NEW_TASK)
            startActivity(intent)
        }
    }

    private fun generateTab() {
        tab_lay.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener{
            override fun onTabUnselected(tab: TabLayout.Tab?) { }
            override fun onTabReselected(tab: TabLayout.Tab?) { }

            override fun onTabSelected(tab: TabLayout.Tab?) {
                if (!isScrolling) {
                    when (tab_lay.selectedTabPosition) {
                        0 -> scroll_lay.smoothScrollTo(0, head_information.top)
                        1 -> scroll_lay.smoothScrollTo(0, head_sparepart.top)
                        2 -> scroll_lay.smoothScrollTo(0, head_checklist.top)
                        3 -> scroll_lay.smoothScrollTo(0, head_document.top)
                    }
                }
            }

        })
    }

    private fun tabListener() {
        scroll_lay.setOnScrollChangeListener { v: NestedScrollView?, x: Int, y: Int, oldX: Int, oldY: Int ->
            if (y >= head_information.top && y < head_sparepart.top){
                isScrolling = true
                tab_lay.getTabAt(0)?.select()
                isScrolling = false
            }
            else if (y >= head_sparepart.top && y < head_checklist.top) {
                isScrolling = true
                tab_lay.getTabAt(1)?.select()
                isScrolling = false
            }
            else if (y >= head_checklist.top && y < head_document.top) {
                isScrolling = true
                tab_lay.getTabAt(2)?.select()
                isScrolling = false
            }
            else if (y > head_document.top) {
                isScrolling = true
                tab_lay.getTabAt(3)?.select()
                isScrolling = false
            }
        }
    }

    private fun setupAdapter() {
        recycler_sparepart.layoutManager = LinearLayoutManager(this, RecyclerView.VERTICAL, false)
        recycler_sparepart.adapter = sparepartAdapter

        recycler_check_list.layoutManager = LinearLayoutManager(this, RecyclerView.VERTICAL, false)
        recycler_check_list.adapter = checklistAdapter

        recycler_document.layoutManager = LinearLayoutManager(this, RecyclerView.VERTICAL, false)
        recycler_document.adapter = documentAdapter
    }

    private fun documentAdapterListener() {
        documentAdapter.setEventHandler(object : TechnicalDocumentAdapter.RecyclerClickListener {
            override fun isClicked(fileUrl: String) {
                showPdf(fileUrl)
            }
        })
    }

    @SuppressLint("SetTextI18n")
    private fun productObserver() {
        productVM.getById(productGroupId).observe(this, {
            if (it.isNotEmpty()) {
                technical_product_value.text = it.first().product_group_name.ifEmpty { "-" }

                val voltage = it.first().voltage.ifEmpty { "-" }
                val current = it.first().current.ifEmpty { "-" }
                val power = it.first().power.ifEmpty { "-" }
                val phase = it.first().phase.ifEmpty { "-" }
                val frequency = it.first().frequency.ifEmpty { "-" }
                technical_information_value.text = "$voltage | $current | $power | $phase | $frequency"

                technical_air_supply_value.text = it.first().air_supply_needed.ifEmpty { "-" }
                technical_air_pressure_value.text = it.first().air_pressure.ifEmpty { "-" }
                machine_brand_value.text = it.first().machine_brand.ifEmpty { "-" }
                machine_model_value.text = it.first().machine_model.ifEmpty { "-" }
                machine_class_value.text = it.first().machine_class_category.ifEmpty { "-" }
                technical_supplier_value.text = it.first().machine_supplier.ifEmpty { "-" }
                technical_tag_value.text = it.first().product_group_tag_id.ifEmpty { "-" }
            } else {
                document_lay.visibility = View.GONE
                warning_document.visibility = View.VISIBLE
            }
        })
    }

    @SuppressLint("SetTextI18n")
    private fun sparepartObserver() {
        sparepartVM.getByProductGroupId(productGroupId).observe(this, {
            tx_sparepart_count.text = ""

            if (it.isNotEmpty()) {
                tx_sparepart_count.text = "(${it.size})"
                sparepart_lay.visibility = View.VISIBLE
                warning_sparepart.visibility = View.GONE

                sparepartAdapter.setList(it)
            } else {
                sparepart_lay.visibility = View.GONE
                warning_sparepart.visibility = View.VISIBLE
            }
        })
    }

    @SuppressLint("SetTextI18n")
    private fun checklistObserver() {
        checklistVM.getTemplateByProductGroupId(productGroupId).observe(this, {
            tx_checklist_count.text = ""

            if (it.isNotEmpty()) {
                tx_checklist_count.text = "(${it.size})"
                checklist_lay.visibility = View.VISIBLE
                warning_checklist.visibility = View.GONE

                checklistAdapter.setList(it)
            } else {
                checklist_lay.visibility = View.GONE
                warning_checklist.visibility = View.VISIBLE
            }
        })
    }

    @SuppressLint("SetTextI18n")
    private fun documentObserver() {
        productDocumentVM.getByProductGroupId(productGroupId).observe(this, { documentData->
            tx_document_count.text = ""

            if (documentData.isNotEmpty()) {
                tx_document_count.text = "(${documentData.size})"
                document_lay.visibility = View.VISIBLE
                warning_document.visibility = View.GONE

                documentAdapter.setList(documentData)
            } else {
                document_lay.visibility = View.GONE
                warning_document.visibility = View.VISIBLE
            }
        })
    }

    private fun showPdf(fileUrl: String) = runWithPermissions( FunHelper.storageWritePermission) {
        DownloadPdf(this, fileUrl).downloadNow(false)
    }

}