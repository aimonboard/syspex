package com.syspex.ui

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.AdapterView
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.core.widget.NestedScrollView
import androidx.fragment.app.FragmentActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.android.material.tabs.TabLayout
import com.syspex.R
import com.syspex.data.local.database.global_enum.EnumViewModel
import com.syspex.data.local.database.pcs_problem.ProblemEntity
import com.syspex.data.local.database.pcs_problem.ProblemViewModel
import com.syspex.data.local.database.pcs_solution.SolutionEntity
import com.syspex.data.local.database.pcs_solution.SolutionViewModel
import com.syspex.data.local.database.product_checklist_question.ChecklistQuestionViewModel
import com.syspex.data.local.database.product_sparepart.SparepartViewModel
import com.syspex.data.local.database.service_report.ServiceReportViewModel
import com.syspex.data.local.database.service_report_equipment.ServiceReportEquipmentEntity
import com.syspex.data.local.database.service_report_equipment.ServiceReportEquipmentViewModel
import com.syspex.data.local.database.service_report_equipment_checklist.ServiceReportEquipmentChecklistViewModel
import com.syspex.data.local.database.service_report_sparepart.ServiceReportSparepartEntity
import com.syspex.data.local.database.service_report_sparepart.ServiceReportSparepartViewModel
import com.syspex.data.model.ConverterImageModel
import com.syspex.data.model.SpinnerModel
import com.syspex.ui.adapter.SRCauseSolutionAdapter
import com.syspex.ui.adapter.SRImageProblemAdapter
import com.syspex.ui.adapter.SRSparePartAdapter
import com.syspex.ui.custom.SpinnerAdapter
import com.syspex.ui.helper.FunHelper
import com.syspex.ui.helper.SendData
import kotlinx.android.synthetic.main.equipment_edit_activity.*

class EquipmentEdit : AppCompatActivity() {

    companion object {
        var problemImage = ArrayList<ConverterImageModel>()
    }

    private lateinit var reportVM: ServiceReportViewModel
    private lateinit var reportEquipmentVM: ServiceReportEquipmentViewModel
    private lateinit var checklistMaster: ChecklistQuestionViewModel
    private lateinit var checklistReport: ServiceReportEquipmentChecklistViewModel
    private lateinit var sparepartVM: ServiceReportSparepartViewModel
    private lateinit var problemVM: ProblemViewModel
    private lateinit var solutionVM: SolutionViewModel
    private lateinit var enumVM: EnumViewModel

    private val problemAdapter = SRImageProblemAdapter(true)
    private val causeSolutionAdapter = SRCauseSolutionAdapter()
    private val sparepartAdapter = SRSparePartAdapter()

    private var isScrolling = false
    private var reportId = ""
    private var reportEquipmentId = ""
    private var equipmentId = ""
    private var productId = ""
    private var checklistTemplateId = ""

    private val spinnerData = mutableListOf<SpinnerModel>()
    private var spinnerStatusPosition = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.equipment_edit_activity)

        reportVM = ViewModelProviders.of(this).get(ServiceReportViewModel::class.java)
        reportEquipmentVM = ViewModelProviders.of(this).get(ServiceReportEquipmentViewModel::class.java)
        checklistMaster = ViewModelProviders.of(this).get(ChecklistQuestionViewModel::class.java)
        checklistReport = ViewModelProviders.of(this).get(ServiceReportEquipmentChecklistViewModel::class.java)
        sparepartVM = ViewModelProviders.of(this).get(ServiceReportSparepartViewModel::class.java)
        problemVM = ViewModelProviders.of(this).get(ProblemViewModel::class.java)
        solutionVM = ViewModelProviders.of(this).get(SolutionViewModel::class.java)
        enumVM = ViewModelProviders.of(this).get(EnumViewModel::class.java)

        generateView()
        buttonListener()
        generateTab()
        setupAdapter()
        problemImageAdapterListener()
        causeSolutionAdapterListener()
        scrollListener()

    }

    // Return from dialog upload file
    override fun onResume() {
        super.onResume()
        problemImage.forEach {
            problemAdapter.addData(it)
        }
        problemImage.clear()
    }

    override fun onBackPressed() {
        dialogConfirmation()
    }

    private fun generateView() {
        if (intent.hasExtra("reportEquipmentId")) {
            reportId = intent.getStringExtra("reportId") ?: ""
            reportEquipmentId = intent.getStringExtra("reportEquipmentId") ?: ""
            equipmentId = intent.getStringExtra("equipmentId") ?: ""
            productId = intent.getStringExtra("productId") ?: ""
            checklistTemplateId = intent.getStringExtra("checklistTemplateId") ?: ""

            val hasChecklist = intent.getBooleanExtra("hasChecklist", false)
            if (hasChecklist) {
                checklist_lay.visibility = View.VISIBLE
                warning_checklist.visibility = View.GONE
            } else {
                checklist_lay.visibility = View.GONE
                warning_checklist.visibility = View.VISIBLE
            }

            reportEquipmentObserver()
            problemObserver()
            solutionObserver()
            checklistObserver()
            sparepartObserver()
        }
    }

    private fun buttonListener() {
        back_link?.setOnClickListener {
            onBackPressed()
        }

        btn_problem_image?.setOnClickListener {
            DialogUploadFile.fileSection = "problem"
            DialogUploadFile.equipmentId = equipmentId
            DialogUploadFile().show((it.context as FragmentActivity).supportFragmentManager,DialogUploadFile().tag)
        }

        btn_add_cause?.setOnClickListener {
            saveData()

            val i = Intent(this, CauseAdd::class.java)
            i.putExtra("reportEquipmentId", reportEquipmentId)
            i.putExtra("equipmentId", equipmentId)
            startActivity(i)
        }

        btn_add_sparepart?.setOnClickListener {
            saveData()

            val i = Intent(this, SparePartAdd::class.java)
            i.putExtra("reportId", reportId)
            i.putExtra("reportEquipmentId", reportEquipmentId)
            i.putExtra("productId", productId)
            startActivity(i)

            Log.e("aim","report id: $reportId, reportequipment id: $reportEquipmentId, product id: $productId")
        }

        btn_add_checklist?.setOnClickListener {
            saveData()

            val i = Intent(this, ChecklistConfirm::class.java)
            i.putExtra("reportId", reportId)
            i.putExtra("reportEquipmentId", reportEquipmentId)
            i.putExtra("equipmentId", equipmentId)
            i.putExtra("productId", productId)
            startActivity(i)
        }

        btn_update_all.setOnClickListener {
            dialogUpdate()
        }

        btn_save.setOnClickListener {
//            if (equipmentEmptyCheck()) {
//            }
//            saveData()
            saveData()
            finish()
        }
    }

    private fun generateTab() {
        tab_lay.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener{
            override fun onTabUnselected(tab: TabLayout.Tab?) { }
            override fun onTabReselected(tab: TabLayout.Tab?) { }

            override fun onTabSelected(tab: TabLayout.Tab?) {
                if (!isScrolling) {
                    when (tab_lay.selectedTabPosition) {
                        0 -> scroll_lay.smoothScrollTo(0, head_symptom.top)
                        1 -> scroll_lay.smoothScrollTo(0, head_cause.top)
                        2 -> scroll_lay.smoothScrollTo(0, head_sparepart.top)
                        3 -> scroll_lay.smoothScrollTo(0, head_checklist.top)
                    }
                }
            }

        })
    }

    private fun setupAdapter() {
        recycler_problem_image.layoutManager = LinearLayoutManager(this, RecyclerView.HORIZONTAL, false)
        recycler_problem_image.adapter = problemAdapter

        recycler_cause_solution.layoutManager = LinearLayoutManager(this, RecyclerView.VERTICAL, false)
        recycler_cause_solution.adapter = causeSolutionAdapter
    }

    private fun problemImageAdapterListener() {
        problemAdapter.registerAdapterDataObserver(object : RecyclerView.AdapterDataObserver() {
            override fun onChanged() {
                super.onChanged()
                if (problemAdapter.itemCount == 0) {
                    divider_problem.visibility = View.GONE
                } else {
                    divider_problem.visibility = View.VISIBLE
                }
            }
        })
    }

    private fun causeSolutionAdapterListener() {
        causeSolutionAdapter.setEventHandler(object : SRCauseSolutionAdapter.RecyclerClickListener {
            override fun isCauseSolutionDeleted(data: SolutionEntity) {
                deleteDialog(data)
            }
        })
    }

    private fun scrollListener() {
        scroll_lay.setOnScrollChangeListener { v: NestedScrollView?, x: Int, y: Int, oldX: Int, oldY: Int ->
            if (y >= head_symptom.top && y <= head_symptom.bottom) {
                isScrolling = true
                tab_lay.getTabAt(0)?.select()
                isScrolling = false
            } else if (y >= head_cause.top && y <= head_cause.bottom) {
                isScrolling = true
                tab_lay.getTabAt(1)?.select()
                isScrolling = false
            } else if (y >= head_sparepart.top && y <= head_sparepart.bottom) {
                isScrolling = true
                tab_lay.getTabAt(2)?.select()
                isScrolling = false
            } else if (y >= head_checklist.top && y <= head_checklist.bottom) {
                isScrolling = true
                tab_lay.getTabAt(3)?.select()
                isScrolling = false
            }
        }
    }

    private fun reportEquipmentObserver() {
        reportEquipmentVM.getByReportEquipmentId(reportEquipmentId).observe(this, Observer { reportEquipment->
            if (reportEquipment.isNotEmpty()) {
                var selectedPosDB = 0
                enumVM.getByTableName("service_report_equipment").observe(this, Observer { enumData->
                    if (enumData.isNotEmpty()) {
                        enumData.forEachIndexed { index, enumEntity ->
                            spinnerData.add(SpinnerModel(enumEntity.enum_id, enumEntity.enum_name))
                            if (enumEntity.enum_id == reportEquipment.first().statusAfterServiceEnumId) {
                                selectedPosDB = index
                            }
                        }

                        spinnerStatus(selectedPosDB)
                    }
                })

                status_remark_value.setText(reportEquipment.first().statusAfterServiceRemarks)
                problem_value.setText(reportEquipment.first().problemSymptom)
            }
        })
    }

    private fun spinnerStatus(selectedPosDB: Int) {
        val arrayAdapter = SpinnerAdapter(this, R.layout.item_spinner_black, spinnerData)
        spinner_status?.adapter = arrayAdapter

        spinner_status?.onItemSelectedListener = object : AdapterView.OnItemSelectedListener{
            override fun onNothingSelected(parent: AdapterView<*>?) { }
            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                spinnerStatusPosition = position
            }
        }

        spinner_status.setSelection(selectedPosDB)
    }

    private fun deleteDialog(data: SolutionEntity) {
        AlertDialog.Builder(this)
            .setMessage("Delete Cause and Solution ?")
            .setPositiveButton(android.R.string.yes) { dialog, whichButton ->
                solutionVM.delete(data)
            }
            .setNegativeButton(android.R.string.no, null).show()
    }

    private fun problemObserver() {
        problemVM.getByReportEquipment(reportEquipmentId).observe(this, Observer { problem->
            problem.forEach { problemData->
                problemData.problemImage?.forEach {
                    problemAdapter.addData(it)
                }
            }
        })
    }

    private fun solutionObserver() {
        Log.e("aim", "reportEquipmentId: $reportEquipmentId")
        solutionVM.getByReportEquipment(reportEquipmentId).observe(this, Observer { solution->
            Log.e("aim", "pcs : $solution")
            causeSolutionAdapter.setList(solution, equipmentId, true)
        })
    }

    @SuppressLint("SetTextI18n")
    private fun checklistObserver() {
        checklistReport.getByReportEquipmentId(reportEquipmentId).observe(this, { reportCount->
            val test = reportCount.filter { it.checklist_status_id.isNotEmpty() }
            tx_checklist.text = "${test.size}/${reportCount.size} has been checked."
        })
    }

    private fun sparepartObserver() {
        Log.d("aim", "equipmentId: $equipmentId")
        recycler_sparepart?.layoutManager = LinearLayoutManager(this, RecyclerView.VERTICAL, false)
        recycler_sparepart?.adapter = sparepartAdapter

        sparepartVM.getByReportEquipmentId(reportEquipmentId).observe(this, Observer { sparePart->
            sparepartAdapter.setList(sparePart)
            if (sparePart.isNotEmpty()) {
                tx_sparepart.visibility = View.GONE
            } else {
                tx_sparepart.visibility = View.VISIBLE
            }
        })
    }

    private fun equipmentEmptyCheck(): Boolean{
        var cek = true

        if(problem_value.text.isEmpty()) cek = false

        if (!cek){
            Toast.makeText(this, "Please fill all columns", Toast.LENGTH_LONG).show()
            return cek
        }

        return cek
    }

    private fun problemEmptyCheck(): Boolean{
        var cek = true

        if(problem_value.text.toString() == "") cek = false

        if (!cek){
            Toast.makeText(this, "Please fill problem columns", Toast.LENGTH_LONG).show()
            return cek
        }

        return cek
    }

    @SuppressLint("SetTextI18n")
    private fun dialogConfirmation() {
        val view = layoutInflater.inflate(R.layout.dialog_confirmation,null)
        val dialog = BottomSheetDialog(this)
        dialog.setContentView(view)

        val txTitle = dialog.findViewById<TextView>(R.id.tx_title)
        val txBody = dialog.findViewById<TextView>(R.id.tx_body)
        val buttonYes = dialog.findViewById<TextView>(R.id.btn_yes)
        val buttonNo = dialog.findViewById<TextView>(R.id.btn_no)

        txTitle?.text = "Save Data"
        txBody?.text = "Leave page without saving data?"
        buttonYes?.text = "Save Data"
        buttonNo?.text = "Leave page"

        buttonYes?.setOnClickListener {
            saveData()
            dialog.dismiss()
            finish()
        }

        buttonNo?.setOnClickListener {
            dialog.dismiss()
            finish()
        }

        dialog.show()
    }

    @SuppressLint("SetTextI18n")
    private fun dialogUpdate() {
        val view = layoutInflater.inflate(R.layout.dialog_confirmation,null)
        val dialog = BottomSheetDialog(this)
        dialog.setContentView(view)

        val txTitle = dialog.findViewById<TextView>(R.id.tx_title)
        val txBody = dialog.findViewById<TextView>(R.id.tx_body)
        val buttonYes = dialog.findViewById<TextView>(R.id.btn_yes)
        val buttonNo = dialog.findViewById<TextView>(R.id.btn_no)

        txTitle?.text = "Update All Equipment"
        txBody?.text = "Are you sure to update all equipment in this report?"
        buttonYes?.text = "Update"
        buttonNo?.text = "Cancel"

        buttonYes?.setOnClickListener {
            prepareUpdateAll()
        }

        buttonNo?.setOnClickListener {
            dialog.dismiss()
        }

        dialog.show()
    }

    private fun saveData() {
        reportVM.updateFlag(reportId, false)

        val selectedSpinner = spinner_status.adapter.getItem(spinnerStatusPosition) as SpinnerModel
        reportEquipmentVM.edit(reportEquipmentId, selectedSpinner.spinnerId, selectedSpinner.spinnerName, status_remark_value.text.toString(), problem_value.text.toString(), false)

        problemVM.insert(ProblemEntity(
            reportEquipmentId ?: "",
            problemAdapter.getAllItem()
        ))
    }

    private fun prepareUpdateAll() {
        reportVM.updateFlag(reportId, false)
        getDataForCopy()
    }

    private fun getDataForCopy() {
//        var masterSolution = mutableListOf<SolutionEntity>()
//        var masterSparepart = mutableListOf<ServiceReportSparepartEntity>()

        val solutionLive = solutionVM.getByReportEquipment(reportEquipmentId)
        solutionLive.observe(this, { it ->
            solutionLive.removeObservers(this)
//            masterSolution = it.toMutableList()
            pasteData(it.toMutableList())
        })

//        val sparepartLive = sparepartVM.getByReportEquipmentId(reportEquipmentId)
//        sparepartLive.observe(this, {
//            sparepartLive.removeObservers(this)
//            sparepartState = true
//            masterSparepart = it.toMutableList()
//
//            if (solutionState && sparepartState) {
//                pasteData(
//                    masterSolution,
//                    masterSparepart
//                )
//            }
//        })
    }

    private fun pasteData(
        masterSolution: MutableList<SolutionEntity>
    ) {
        val reportEquipmentLive = reportEquipmentVM.getByReportId(reportId)
        reportEquipmentLive.observe(this, { it->
            reportEquipmentLive.removeObservers(this)

            it.forEach { reportEquipment->
                // Delete old data
                solutionVM.deleteByReportEquipmentId(reportEquipment.serviceReportEquipmentId)
//                sparepartVM.deleteByReportEquipmentId(reportEquipment.serviceReportEquipmentId)

                val selectedSpinner = spinner_status.adapter.getItem(spinnerStatusPosition) as SpinnerModel
                reportEquipmentVM.edit(
                    reportEquipment.serviceReportEquipmentId,
                    selectedSpinner.spinnerId,
                    selectedSpinner.spinnerName,
                    status_remark_value.text.toString(),
                    problem_value.text.toString(),
                    false
                )

                problemVM.insert(
                    ProblemEntity(
                        reportEquipment.serviceReportEquipmentId,
                        problemAdapter.getAllItem()
                    )
                )

                masterSolution.forEach {solutionData->
                    solutionVM.insert(SolutionEntity(
                        FunHelper.randomString("") ?: "",
                        reportEquipment.serviceReportEquipmentId ?: "",
                        solutionData.subject ?: "",
                        solutionData.problemCause ?: "",
                        solutionData.solution ?: "",
                        solutionData.createdDate ?: "",
                        solutionData.createdBy ?: "",
                        solutionData.lastModifiedDate ?: "",
                        solutionData.lastModifiedBy ?: "",
                        solutionData.causeImage ?: emptyList(),
                        solutionData.solutionImage ?: emptyList(),
                        false,
                        true,
                    ))
                }

//                masterSparepart.forEach { sparepartData->
//                    sparepartVM.insert(ServiceReportSparepartEntity(
//                        FunHelper.randomString("") ?: "",
//                        reportEquipment.serviceReportEquipmentId ?: "",
//                        sparepartData.spare_part_image_number ?: "",
//                        sparepartData.spare_part_number ?: "",
//                        sparepartData.spare_part_description ?: "",
//                        sparepartData.spare_part_memo ?: "",
//                        sparepartData.enum_id ?: "",
//                        sparepartData.enum_name ?: "",
//                        sparepartData.part_id ?: "",
//                        sparepartData.part_no ?: "",
//                        sparepartData.part_name ?: "",
//                        sparepartData.qty ?: "",
//                        false,
//                        true,
//                    ))
//                }
            }
        })

        finish()
    }
}