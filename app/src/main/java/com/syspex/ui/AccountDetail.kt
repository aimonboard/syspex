package com.syspex.ui

import android.annotation.SuppressLint
import android.app.ProgressDialog
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.core.widget.NestedScrollView
import androidx.lifecycle.ViewModelProviders
import com.google.android.material.tabs.TabLayout
import com.livinglifetechway.quickpermissions_kotlin.runWithPermissions
import com.syspex.Menu2
import com.syspex.R
import com.syspex.data.local.database.account.AccountViewModel
import com.syspex.data.local.database.account_attachment.AccountAttachmentViewModel
import com.syspex.data.local.database.account_contact.AccountContactViewModel
import com.syspex.data.local.database.account_equipment.AccountEquipmentViewModel
import com.syspex.data.local.database.activity.ActivityViewModel
import com.syspex.data.local.database.agreement.AgreementViewModel
import com.syspex.data.local.database.service_call.ServiceCallViewModel
import com.syspex.data.local.database.service_case.ServiceCaseViewModel
import com.syspex.data.local.database.service_report.ServiceReportViewModel
import com.syspex.ui.adapter.*
import com.syspex.ui.helper.DownloadPdf
import com.syspex.ui.helper.FunHelper
import kotlinx.android.synthetic.main.account_detail_activity.*
import java.net.URL

class AccountDetail : AppCompatActivity() {

    private lateinit var accountVM: AccountViewModel
    private lateinit var attachmentVM: AccountAttachmentViewModel
    private lateinit var contactVM: AccountContactViewModel
    private lateinit var agreementVM: AgreementViewModel
    private lateinit var caseVM: ServiceCaseViewModel
    private lateinit var callVM: ServiceCallViewModel
    private lateinit var reportVM: ServiceReportViewModel
    private lateinit var accountEquipmentVM: AccountEquipmentViewModel
    private lateinit var activityVM: ActivityViewModel

    private val contactAdapter = ContactAdapter()
    private val agreementAdapter = AgreementAdapter()
    private val caseAdapter = CaseMyAdapter()
    private val callAdapter = SCMyCallAdapter()
    private val reportAdapter = SRMyReportAdapter()
    private val equipmentAdapter = EquipmentListAdapter()
    private val activityAdapter = ActivityListAdapter()
    private val attachmentAdapter = AttachmentAdapter()

    private var isScrolling = false
    private var accountId = ""
    private var accountName = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.account_detail_activity)

        accountVM = ViewModelProviders.of(this).get(AccountViewModel::class.java)
        attachmentVM = ViewModelProviders.of(this).get(AccountAttachmentViewModel::class.java)
        contactVM = ViewModelProviders.of(this).get(AccountContactViewModel::class.java)
        agreementVM = ViewModelProviders.of(this).get(AgreementViewModel::class.java)
        caseVM = ViewModelProviders.of(this).get(ServiceCaseViewModel::class.java)
        callVM = ViewModelProviders.of(this).get(ServiceCallViewModel::class.java)
        reportVM = ViewModelProviders.of(this).get(ServiceReportViewModel::class.java)
        accountEquipmentVM = ViewModelProviders.of(this).get(AccountEquipmentViewModel::class.java)
        activityVM = ViewModelProviders.of(this).get(ActivityViewModel::class.java)

        generateView()
        buttonListener()
        generateTab()
        scrollListener()
        setupAdapter()
        attachmentAdapterListener()

    }

    private fun generateView() {
        if (intent.hasExtra("id")) {
            accountId = intent.getStringExtra("id") ?: ""
            accountName = intent.getStringExtra("name") ?: ""
            header_name?.text = intent.getStringExtra("name") ?: ""

            accountObserver()
            contactObserver()
            agreementObserver()
            caseObserver()
            callObserver()
            reportObserver()
            equipmentObserver()
            activityObserver()
            attachmnetObserver()

        }
    }

    private fun buttonListener(){
        back_link.setOnClickListener {
            onBackPressed()
        }

//        btn_search.setOnClickListener {
//            val i = Intent(it.context, Search::class.java)
//            startActivity(i)
//        }

        btn_home.setOnClickListener {
            val intent = Intent(this, Menu2::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_NEW_TASK)
            startActivity(intent)
        }

        btn_location.setOnClickListener {
            FunHelper.direction(it.context, address_value.text.toString())
        }

        btn_add_contact.setOnClickListener {
            val i = Intent(it.context, ContactAdd::class.java)
            i.putExtra("accountId",accountId)
            i.putExtra("accountName",accountName)
            startActivity(i)
        }

        btn_add_attachment.setOnClickListener {
            val i = Intent(this, AttachmentAdd::class.java)
            i.putExtra("accountId", accountId)
            i.putExtra("sourceId", "-")
            i.putExtra("sourceTable","-")
            startActivity(i)
        }
    }

    private fun generateTab() {
        tab_lay.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener{
            override fun onTabUnselected(tab: TabLayout.Tab?) { }
            override fun onTabReselected(tab: TabLayout.Tab?) { }

            override fun onTabSelected(tab: TabLayout.Tab?) {
                if (!isScrolling) {
                    when (tab_lay.selectedTabPosition) {
                        0 -> scroll_lay.smoothScrollTo(0, head_information.top)
                        1 -> scroll_lay.smoothScrollTo(0, head_contact.top)
                        2 -> scroll_lay.smoothScrollTo(0, head_agreement.top)
                        3 -> scroll_lay.smoothScrollTo(0, head_case.top)
                        4 -> scroll_lay.smoothScrollTo(0, head_service_call.top)
                        5 -> scroll_lay.smoothScrollTo(0, head_equipment.top)
                        6 -> scroll_lay.smoothScrollTo(0, head_service_report.top)
                        7 -> scroll_lay.smoothScrollTo(0, head_activity.top)
                        8 -> scroll_lay.smoothScrollTo(0, head_attachment.top)
                    }
                }
            }

        })
    }

    private fun scrollListener() {
        scroll_lay.setOnScrollChangeListener { v: NestedScrollView?, x: Int, y: Int, oldX: Int, oldY: Int ->
            if (y >= head_information.top && y <= head_information.bottom){
                isScrolling = true
                tab_lay.getTabAt(0)?.select()
                isScrolling = false
            }
            else if (y >= head_contact.top && y < head_agreement.top) {
                isScrolling = true
                tab_lay.getTabAt(1)?.select()
                isScrolling = false
            }
            else if (y >= head_agreement.top && y < head_case.top) {
                isScrolling = true
                tab_lay.getTabAt(2)?.select()
                isScrolling = false
            }
            else if (y >= head_case.top && y < head_service_call.top) {
                isScrolling = true
                tab_lay.getTabAt(3)?.select()
                isScrolling = false
            }
            else if (y >= head_service_call.top && y < head_equipment.top) {
                isScrolling = true
                tab_lay.getTabAt(4)?.select()
                isScrolling = false
            }
            else if (y >= head_equipment.top && y < head_service_report.top) {
                isScrolling = true
                tab_lay.getTabAt(5)?.select()
                isScrolling = false
            }
            else if (y >= head_service_report.top && y < head_activity.top) {
                isScrolling = true
                tab_lay.getTabAt(6)?.select()
                isScrolling = false
            }
            else if (y >= head_activity.top && y < head_attachment.top) {
                isScrolling = true
                tab_lay.getTabAt(7)?.select()
                isScrolling = false
            }
            else if (y >= head_attachment.top) {
                isScrolling = true
                tab_lay.getTabAt(8)?.select()
                isScrolling = false
            }
        }
    }

    private fun setupAdapter() {
        FunHelper.setUpAdapter(this, recycler_contact).adapter = contactAdapter
        FunHelper.setUpAdapter(this, recycler_agreement).adapter = agreementAdapter
        FunHelper.setUpAdapter(this, recycler_case).adapter = caseAdapter
        FunHelper.setUpAdapter(this, recycler_service_call).adapter = callAdapter
        FunHelper.setUpAdapter(this, recycler_service_report).adapter = reportAdapter
        FunHelper.setUpAdapter(this, recycler_equipment).adapter = equipmentAdapter
        FunHelper.setUpAdapter(this, recycler_activity).adapter = activityAdapter
        FunHelper.setUpAdapter(this, recycler_attachment).adapter = attachmentAdapter
    }

    private fun attachmentAdapterListener() {
        attachmentAdapter.setEventHandler(object : AttachmentAdapter.RecyclerClickListener {
            override fun isClicked(fileUrl: String) {
                val progressDialog = ProgressDialog(this@AccountDetail)
                progressDialog.setMessage("Open Attachment ...")
                progressDialog.setCancelable(false)
                progressDialog.show()

                if (fileUrl.isNotEmpty()) {
                    val thread = object : Thread() {
                        override fun run() {
                            var fileExtension = ""
                            try {
                                val connection = URL(fileUrl).openConnection()
                                fileExtension = connection.getHeaderField("Content-Type")
                                Log.e("aim", "extention : $fileExtension")
                            } catch (e: Exception) { }

                            runOnUiThread {
                                if (fileExtension.contains("pdf")) {
                                    progressDialog.dismiss()
                                    showPdf(fileUrl)
                                } else if (fileExtension.contains("jpg") ||
                                    fileExtension.contains("jpeg") ||
                                    fileExtension.contains("png")) {

                                    progressDialog.dismiss()
                                    val i = Intent(this@AccountDetail, ImagePreview::class.java)
                                    i.putExtra("image", fileUrl)
                                    startActivity(i)
                                } else {
                                    progressDialog.dismiss()
                                    Toast.makeText(
                                        this@AccountDetail,
                                        "Cant open attachment file",
                                        Toast.LENGTH_LONG
                                    ).show()
                                }
                            }
                        }
                    }
                    thread.start()
                } else {
                    Toast.makeText(
                        this@AccountDetail,
                        "Attachment file not available",
                        Toast.LENGTH_LONG
                    ).show()
                }
            }

        })
    }

    @SuppressLint("SetTextI18n")
    private fun accountObserver() {
        accountVM.getByAccountId(accountId).observe(this, {
            if (it.isNotEmpty()) {
                address_value.text = it.first().address?.first()?.fullAddress ?: "-"
                code_value.text = it.first().account_code.ifEmpty { "-" }
                sales_value.text = it.first().sales_name.ifEmpty { "-" }
                brach_value.text = it.first().region_id.ifEmpty { "-" }
                created_value.text = FunHelper.uiDateFormat(it.first().created_date)
                modify_date.text = FunHelper.uiDateFormat(it.first().last_modified_date)
            }
        })
    }

    @SuppressLint("SetTextI18n")
    private fun contactObserver() {
        contactVM.getByAccountId(accountId).observe(this, {
            contact_count.text = ""

            if (it.isNotEmpty()) {
                contact_count.text = "(${it.size})"
                recycler_contact.visibility = View.VISIBLE
                warning_contact.visibility = View.GONE
                contactAdapter.setList(it)
            } else {
                recycler_contact.visibility = View.GONE
                warning_contact.visibility = View.VISIBLE
            }
        })
    }

    @SuppressLint("SetTextI18n")
    private fun agreementObserver() {
        agreementVM.getByAccountId(accountId).observe(this, {
            agreement_count.text = ""

            if (it.isNotEmpty()) {
                agreement_count.text = "(${it.size})"
                recycler_agreement.visibility = View.VISIBLE
                warning_agreement.visibility = View.GONE
                agreementAdapter.setList(it)
            } else {
                recycler_agreement.visibility = View.GONE
                warning_agreement.visibility = View.VISIBLE
            }
        })
    }

    @SuppressLint("SetTextI18n")
    private fun caseObserver() {
        caseVM.getByAccountId(accountId).observe(this, {
            case_count.text = ""

            if (it.isNotEmpty()) {
                case_count.text = "(${it.size})"
                recycler_case.visibility = View.VISIBLE
                warning_case.visibility = View.GONE
                caseAdapter.setList(it, true)
            } else {
                recycler_case.visibility = View.GONE
                warning_case.visibility = View.VISIBLE
            }
        })
    }

    @SuppressLint("SetTextI18n")
    private fun callObserver() {
        callVM.getByAccountId(accountId).observe(this, {
            service_call_count.text = ""

            if (it.isNotEmpty()) {
                service_call_count.text = "(${it.size})"
                recycler_service_call.visibility = View.VISIBLE
                warning_call.visibility = View.GONE
                callAdapter.setList(it, true)
            } else {
                recycler_service_call.visibility = View.GONE
                warning_call.visibility = View.VISIBLE
            }
        })
    }

    @SuppressLint("SetTextI18n")
    private fun reportObserver() {
        reportVM.getByAccountId(accountId).observe(this, {
            service_report_count.text = ""

            if (it.isNotEmpty()) {
                service_report_count.text = "(${it.size})"
                recycler_service_report.visibility = View.VISIBLE
                warning_report.visibility = View.GONE
                reportAdapter.setList(it, true)
            } else {
                recycler_service_report.visibility = View.GONE
                warning_report.visibility = View.VISIBLE
            }
        })
    }

    @SuppressLint("SetTextI18n")
    private fun equipmentObserver(){
        accountEquipmentVM.getByAccountId(accountId).observe(this, {
            equipment_count.text = ""

            if (it.isNotEmpty()) {
                equipment_count.text = "(${it.size})"
                recycler_equipment.visibility = View.VISIBLE
                warning_equipment.visibility = View.GONE
                equipmentAdapter.setList(it, true)
            } else {
                recycler_equipment.visibility = View.GONE
                warning_equipment.visibility = View.VISIBLE
            }
        })
    }

    @SuppressLint("SetTextI18n")
    private fun activityObserver() {
        activityVM.getByAccount(accountId).observe(this, {
            activity_count.text = ""

            if (it.isNotEmpty()) {
                activity_count.text = "(${it.size})"
                recycler_activity.visibility = View.VISIBLE
                warning_activity.visibility = View.GONE
                activityAdapter.setList(it)
            } else {
                recycler_activity.visibility = View.GONE
                warning_activity.visibility = View.VISIBLE
            }
        })
    }

    @SuppressLint("SetTextI18n")
    private fun attachmnetObserver() {
        attachmentVM.accountAttachment(accountId)
            .observe(this, { attachmentData->
            tx_attachment_count.text = ""

            if (attachmentData.isNotEmpty()) {
                tx_attachment_count.text = "(${attachmentData.size})"
                recycler_attachment.visibility = View.VISIBLE
                warning_attachment.visibility = View.GONE
                attachmentAdapter.setList(attachmentData)
            } else {
                recycler_attachment.visibility = View.GONE
                warning_attachment.visibility = View.VISIBLE
            }
        })
    }

    private fun showPdf(fileUrl: String) = runWithPermissions( FunHelper.storageWritePermission) {
        DownloadPdf(this, fileUrl).downloadNow(false)
    }



}
