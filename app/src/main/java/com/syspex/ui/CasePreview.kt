package com.syspex.ui

import android.annotation.SuppressLint
import android.app.ProgressDialog
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.livinglifetechway.quickpermissions_kotlin.runWithPermissions
import com.syspex.R
import com.syspex.data.local.SharedPreferenceData
import com.syspex.data.local.database.account_attachment.AccountAttachmentViewModel
import com.syspex.data.local.database.account_contact.AccountContactViewModel
import com.syspex.data.local.database.activity.ActivityViewModel
import com.syspex.data.local.database.agreement.AgreementViewModel
import com.syspex.data.local.database.service_call.ServiceCallViewModel
import com.syspex.data.local.database.service_case.ServiceCaseViewModel
import com.syspex.data.local.database.service_case_equipment.ServiceCaseEquipmentViewModel
import com.syspex.data.local.database.service_report.ServiceReportViewModel
import com.syspex.ui.adapter.*
import com.syspex.ui.helper.DownloadPdf
import com.syspex.ui.helper.FunHelper
import kotlinx.android.synthetic.main.case_preview_fragment.*
import java.net.URL

class CasePreview : BottomSheetDialogFragment() {

    companion object {
        var caseId = ""
    }

    private lateinit var caseVM: ServiceCaseViewModel
    private lateinit var caseEquipmentVM: ServiceCaseEquipmentViewModel
    private lateinit var accountContactVM: AccountContactViewModel
    private lateinit var attachmentVM: AccountAttachmentViewModel
    private lateinit var agreementVM: AgreementViewModel
    private lateinit var callVM: ServiceCallViewModel
    private lateinit var reportVM: ServiceReportViewModel
    private lateinit var activityVM: ActivityViewModel

    private val agreementAdapter = AgreementAdapter()
    private val serviceCallAdapter = SCMyCallAdapter()
    private val reportAdapter = SRMyReportAdapter()
    private val equipmentAdapter = EquipmentListAdapter()
    private val activityAdapter = ActivityListAdapter()
    private val attachmentAdapter = AttachmentAdapter()

    private var accountId = ""
    private var accountName = ""

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.case_preview_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        caseVM = ViewModelProviders.of(this).get(ServiceCaseViewModel::class.java)
        caseEquipmentVM = ViewModelProviders.of(this).get(ServiceCaseEquipmentViewModel::class.java)
        accountContactVM = ViewModelProviders.of(this).get(AccountContactViewModel::class.java)
        attachmentVM = ViewModelProviders.of(this).get(AccountAttachmentViewModel::class.java)
        agreementVM = ViewModelProviders.of(this).get(AgreementViewModel::class.java)
        callVM = ViewModelProviders.of(this).get(ServiceCallViewModel::class.java)
        reportVM = ViewModelProviders.of(this).get(ServiceReportViewModel::class.java)
        activityVM = ViewModelProviders.of(this).get(ActivityViewModel::class.java)

        setupAdapter()
        attachmentAdapterListener()
        setupObserver()
        buttonListener()

    }

    private fun buttonListener() {
        btn_location.setOnClickListener {
            FunHelper.direction(requireContext(), info_address.text.toString())
        }

        btn_phone1.setOnClickListener {
            FunHelper.phoneCall(requireContext(), account_contact_phone.text.toString())
        }

        btn_phone2.setOnClickListener {
            FunHelper.phoneCall(requireContext(), account_pic_contact_phone.text.toString())
        }

        btn_edit_contact.setOnClickListener {
            val i = Intent(requireContext(), ContactAccount::class.java)
            i.putExtra("accountId", accountId)
            i.putExtra("accountName", accountName)
            startActivity(i)
        }

        btn_add_call.setOnClickListener {
            val i = Intent(requireContext(), ServiceCallAddCase::class.java)
            i.putExtra("caseId", caseId)
            i.putExtra("accountId", accountId)
            startActivity(i)
        }

        btn_add_attachment.setOnClickListener {
            val i = Intent(requireContext(), AttachmentAdd::class.java)
            i.putExtra("accountId", accountId)
            i.putExtra("sourceId", caseId)
            i.putExtra("sourceTable","Case")
            startActivity(i)
        }
    }

    private fun setupAdapter() {
        FunHelper.setUpAdapter(requireContext(), recycler_agreement).adapter = agreementAdapter
        FunHelper.setUpAdapter(requireContext(), recycler_service_call).adapter = serviceCallAdapter
        FunHelper.setUpAdapter(requireContext(), recycler_service_report).adapter = reportAdapter
        FunHelper.setUpAdapter(requireContext(), recycler_equipment).adapter = equipmentAdapter
        FunHelper.setUpAdapter(requireContext(), recycler_activity).adapter = activityAdapter
        FunHelper.setUpAdapter(requireContext(), recycler_attachment).adapter = attachmentAdapter
    }

    private fun attachmentAdapterListener() {
        attachmentAdapter.setEventHandler(object : AttachmentAdapter.RecyclerClickListener {
            override fun isClicked(fileUrl: String) {
                val progressDialog = ProgressDialog(requireContext())
                progressDialog.setMessage("Open Attachment ...")
                progressDialog.setCancelable(false)
                progressDialog.show()

                if (fileUrl.isNotEmpty()) {
                    val thread = object : Thread() {
                        override fun run() {
                            var fileExtension = ""
                            try {
                                val connection = URL(fileUrl).openConnection()
                                fileExtension = connection.getHeaderField("Content-Type")
                                Log.e("aim", "extention : $fileExtension")
                            } catch (e: Exception) { }

                            requireActivity().runOnUiThread {
                                if (fileExtension.contains("pdf")) {
                                    progressDialog.dismiss()
                                    showPdf(fileUrl)
                                } else if (fileExtension.contains("jpg") ||
                                    fileExtension.contains("jpeg") ||
                                    fileExtension.contains("png")) {

                                    progressDialog.dismiss()
                                    val i = Intent(requireContext(), ImagePreview::class.java)
                                    i.putExtra("image", fileUrl)
                                    startActivity(i)
                                } else {
                                    progressDialog.dismiss()
                                    Toast.makeText(
                                        requireContext(),
                                        "Cant open attachment file",
                                        Toast.LENGTH_LONG
                                    ).show()
                                }
                            }
                        }
                    }
                    thread.start()
                } else {
                    Toast.makeText(
                        requireContext(),
                        "Attachment file not available",
                        Toast.LENGTH_LONG
                    ).show()
                }
            }

        })
    }

    @SuppressLint("SetTextI18n")
    private fun setupObserver(){
        caseVM.getByCaseId(caseId).observe(this, {
            if (it.isNotEmpty()) {
                // header
                info_case_number.text = it.first().caseNumber.ifEmpty { "-" }
                sub_title.text = it.first().caseTypeEnumName.ifEmpty { "-" }
                case_status.text = it.first().caseStatusEnumName.ifEmpty { "-" }

                //info
                info_account_name.text = it.first().accountName.ifEmpty { "-" }
                info_address.text = it.first().addressText.ifEmpty { "-" }

                // Account contact
                getContact(
                    it.first().caseAccountContactId,
                    account_contact_name,
                    account_contact_phone,
                    account_contact_type
                )

                // PIC contact
                getContact(
                    it.first().caseFieldAccountContactId,
                    account_pic_contact_name,
                    account_pic_contact_phone,
                    account_pic_contact_type
                )

                case_subject_value.text = it.first().subject.ifEmpty { "-" }
                case_desc_value.text = it.first().description.ifEmpty { "-" }
                info_agreement_type.text = it.first().agreementType.ifEmpty { "-" }

                info_case_owner.text = it.first().caseOwnerName.ifEmpty { "-" }
                info_technical_pic.text = it.first().leadEngineerName.ifEmpty { "-" }
                account_sales_value.text = it.first().accountSalesName.ifEmpty { "-" }
                case_sales_value.text = it.first().caseSalesName.ifEmpty { "-" }

                // ================================================================================= Installation / Demo
                if (it.first().caseTypeEnumName == "Installation" || it.first().caseTypeEnumName == "Demo") {
                    installation_lay.visibility = View.VISIBLE
                    warning_installation.visibility = View.GONE

                    installation_delivery_value.text =
                        FunHelper.uiDateFormat(it.first().proposedDeliveryDate)
                    installation_install_value.text =
                        FunHelper.uiDateFormat(it.first().proposedInstallationDate)
                    installation_po_value.text = it.first().customerPo.ifEmpty { "-" }
                    installation_supply_direct_value.text =
                        if (it.first().directDelivery == "1") "Yes" else "No"
                    installation_material_value.text =
                        if (it.first().materialNeeded == "1") "Yes" else "No"
                    isntallation_free_value.text = it.first().noOfFreeService.ifEmpty { "-" }
                } else {
                    installation_lay.visibility = View.GONE
                    warning_installation.visibility = View.VISIBLE
                }

                // ================================================================================= Attachment
                accountId = it.first().accountId
                accountName = it.first().accountName
                attachmnetObserver(accountId)

                // ================================================================================= Agreement
                val agreementId = it.first().agreementId
                agreementObserver(agreementId)

                // ================================================================================= My Case
                val engineerId = SharedPreferenceData.getString(requireContext(), 8, "")
                if (it.first().leadEngineerId == engineerId) {
                    btn_edit_contact.visibility = View.VISIBLE
                    btn_add_call.visibility = View.VISIBLE
                    btn_add_attachment.visibility = View.VISIBLE
                } else {
                    btn_edit_contact.visibility = View.GONE
                    btn_add_call.visibility = View.GONE
                    btn_add_attachment.visibility = View.GONE
                }

            }
        })

        callVM.getByCaseId(caseId).observe(this, {
            tx_service_call_count.text = ""
            serviceCallAdapter.clear()

            if (it.isNotEmpty()) {
                tx_service_call_count.text = "(${it.size})"
                recycler_service_call.visibility = View.VISIBLE
                warning_call.visibility = View.GONE

                serviceCallAdapter.setList(it, false)
            } else {
                recycler_service_call.visibility = View.GONE
                warning_call.visibility = View.VISIBLE
            }
        })

        reportVM.getByCaseId(caseId).observe(this, {
            tx_service_report_count.text = ""
            reportAdapter.clear()

            if (it.isNotEmpty()) {
                tx_service_report_count.text = "(${it.size})"
                recycler_service_report.visibility = View.VISIBLE
                warning_report.visibility = View.GONE

                reportAdapter.setList(it, false)
            } else {
                recycler_service_report.visibility = View.GONE
                warning_report.visibility = View.VISIBLE
            }
        })

        caseEquipmentVM.getByCaseId(caseId, true).observe(this, Observer {
            tx_equipment_count.text = ""
            equipmentAdapter.reset()

            if (it.isNotEmpty()) {
                tx_equipment_count.text = "(${it.size})"
                recycler_equipment.visibility = View.VISIBLE
                warning_equipment.visibility = View.GONE

                it.forEach { equipmentData ->
                    equipmentAdapter.addData(equipmentData, false)
                }
            } else {
                recycler_equipment.visibility = View.GONE
                warning_equipment.visibility = View.VISIBLE
            }
        })

        activityVM.getBycase(caseId).observe(this, {
            activity_count.text = ""

            if (it.isNotEmpty()) {
                activity_count.text = "(${it.size})"
                recycler_activity.visibility = View.VISIBLE
                warning_activity.visibility = View.GONE

                activityAdapter.setList(it)
            } else {
                recycler_activity.visibility = View.GONE
                warning_activity.visibility = View.VISIBLE
            }
        })
    }

    @SuppressLint("SetTextI18n")
    private fun agreementObserver(agreementId: String) {
        agreementVM.getById(agreementId).observe(this, { agreementData ->
            tx_agreement_count.text = ""

            if (agreementData.isNotEmpty()) {
                tx_agreement_count.text = "(${agreementData.size})"
                recycler_agreement.visibility = View.VISIBLE
                warning_agreement_1234.visibility = View.GONE

                agreementAdapter.setList(agreementData)
            } else {
                recycler_agreement.visibility = View.GONE
                warning_agreement_1234.visibility = View.VISIBLE
            }
        })
    }

    @SuppressLint("SetTextI18n")
    private fun attachmnetObserver(accountId: String) {
        attachmentVM.accountAttachmentSection(accountId, caseId, "Case")
            .observe(this, { attachmentData ->
                tx_attachment_count.text = ""

                if (attachmentData.isNotEmpty()) {
                    tx_attachment_count.text = "(${attachmentData.size})"
                    recycler_attachment.visibility = View.VISIBLE
                    warning_attachment.visibility = View.GONE

                    attachmentAdapter.setList(attachmentData)
                } else {
                    recycler_attachment.visibility = View.GONE
                    warning_attachment.visibility = View.VISIBLE
                }
            })
    }

    private fun getContact(contactId: String, name: TextView, phone: TextView, type: TextView) {
        accountContactVM.getById(contactId).observe(this, {
            if (it.isNotEmpty()) {
                name.text = it.first().contact_person_name.ifEmpty { "-" }
                phone.text = it.first().contact_person_phone.ifEmpty { "-" }
                type.text = it.first().contact_type.ifEmpty { "-" }
            }
        })
    }

    private fun showPdf(fileUrl: String) = runWithPermissions(FunHelper.storageWritePermission) {
        DownloadPdf(requireContext(), fileUrl).downloadNow(false)
    }

}
