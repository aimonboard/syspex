package com.syspex.ui

import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CheckBox
import android.widget.TextView
import androidx.core.view.GravityCompat
import androidx.core.widget.NestedScrollView
import androidx.drawerlayout.widget.DrawerLayout
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.android.material.tabs.TabLayout
import com.syspex.Menu2
import com.syspex.R
import com.syspex.data.local.SharedPreferenceData
import com.syspex.data.local.database.global_enum.EnumViewModel
import com.syspex.data.local.database.service_call.ServiceCallEntity
import com.syspex.data.local.database.service_call.ServiceCallViewModel
import com.syspex.ui.adapter.FilterAdapter
import com.syspex.ui.adapter.SCMyCallAdapter
import com.syspex.ui.adapter.SCOtherCallAdapter
import com.syspex.ui.adapter.SCTeamCallAdapter
import com.syspex.ui.helper.KeyboardHelper
import kotlinx.android.synthetic.main.service_call_fragment.*
import kotlinx.android.synthetic.main.service_call_fragment.btn_filter
import kotlinx.android.synthetic.main.service_call_fragment.btn_menu
import kotlinx.android.synthetic.main.service_call_fragment.btn_search
import kotlinx.android.synthetic.main.service_call_fragment.header_name
import kotlinx.android.synthetic.main.service_call_fragment.search_lay
import kotlinx.android.synthetic.main.service_call_fragment.search_value
import kotlinx.android.synthetic.main.shimmer_layout.*

class ServiceCall : Fragment() {

    private lateinit var serviceCallVM: ServiceCallViewModel
    private lateinit var enumVM: EnumViewModel

    private var myCallAdapter = SCMyCallAdapter()
    private var teamCallAdapter = SCTeamCallAdapter()
    private var otherCallAdapter = SCOtherCallAdapter()
    private var filterAdapter = FilterAdapter()

    private var isScrolling = false
    private var otherCallVisible = false
    private var tabArray = arrayListOf(0,1)
    private var filterCheckedId = ArrayList<String>()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.service_call_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        serviceCallVM = ViewModelProviders.of(this).get(ServiceCallViewModel::class.java)
        enumVM = ViewModelProviders.of(this).get(EnumViewModel::class.java)

        setupAdapter()
        buttonListener()
        tabListener()
        scrollListener()

        syncObserver()
        filterObserver()
        searchListener()
        filterListener()

    }

    private fun buttonListener() {
        val drawerLayout: DrawerLayout = requireActivity().findViewById(R.id.drawer_layout)
        btn_menu?.setOnClickListener {
            if(!drawerLayout.isDrawerOpen(GravityCompat.START))
                drawerLayout.openDrawer(GravityCompat.START)
        }

        btn_search.setOnClickListener {
            if (header_name.visibility == View.VISIBLE) {
                header_name.visibility = View.GONE
                search_lay.visibility = View.VISIBLE
                btn_search.setImageResource(R.drawable.ic_close)

                KeyboardHelper.openSoftKeyboard(requireContext(),search_value)
            } else {
                header_name.visibility = View.VISIBLE
                search_lay.visibility = View.GONE
                btn_search.setImageResource(R.drawable.ic_search_small)

                KeyboardHelper.hideSoftKeyboard(requireContext(),search_value)

                search_value.setText("")
            }
        }

        btn_filter?.setOnClickListener {
            filterDialog()
        }

        swipe_container.setOnRefreshListener {
            syncObserver()
        }
    }

    private fun setupAdapter() {
        recycler_my?.layoutManager = LinearLayoutManager(activity, RecyclerView.VERTICAL, false)
        recycler_my?.adapter = myCallAdapter

        recycler_team?.layoutManager = LinearLayoutManager(activity, RecyclerView.VERTICAL, false)
        recycler_team?.adapter = teamCallAdapter

        recycler_other?.layoutManager = LinearLayoutManager(activity, RecyclerView.VERTICAL, false)
        recycler_other?.adapter = otherCallAdapter
    }

    private fun tabListener() {
        tab_lay.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener{
            override fun onTabUnselected(tab: TabLayout.Tab?) { }
            override fun onTabReselected(tab: TabLayout.Tab?) { }

            override fun onTabSelected(tab: TabLayout.Tab?) {
                if (!isScrolling) {
                    when (tab_lay.selectedTabPosition) {
                        0 -> scroll_lay.smoothScrollTo(0, dynamicTabSelected(0))
                        1 -> scroll_lay.smoothScrollTo(0, dynamicTabSelected(1))
                        2 -> scroll_lay.smoothScrollTo(0, dynamicTabSelected(2))
                    }
                }
            }
        })
    }

    private fun dynamicTabSelected(position: Int) : Int {
        return when(tabArray[position]) {
            0 -> head_my.top
            1 -> head_team.top
            else -> head_other.top
        }
    }

    private fun scrollListener() {
        scroll_lay.setOnScrollChangeListener { v: NestedScrollView?, x: Int, y: Int, oldX: Int, oldY: Int ->
            swipe_container.isEnabled = y == 0

            if (y >= head_my.top && y <= recycler_my.bottom && recycler_my.visibility == View.VISIBLE) {
                isScrolling = true
                tab_lay.getTabAt(tabArray.indexOf(0))?.select()
                isScrolling = false
            } else if (y >= head_team.top && y <= recycler_team.bottom && recycler_team.visibility == View.VISIBLE) {
                isScrolling = true
                tab_lay.getTabAt(tabArray.indexOf(1))?.select()
                isScrolling = false
            } else if (y >= head_other.top && y <= recycler_other.bottom && recycler_other.visibility == View.VISIBLE) {
                isScrolling = true
                tab_lay.getTabAt(tabArray.indexOf(2))?.select()
                isScrolling = false
            }
        }
    }

    private fun filterDialog() {
        val view = layoutInflater.inflate(R.layout.dialog_list_filter,null)
        val dialog = BottomSheetDialog(requireContext())
        dialog.setContentView(view)
        val bottomSheetBehavior = BottomSheetBehavior.from<View>(view.parent as View)
        dialog.setOnShowListener { bottomSheetBehavior.state = BottomSheetBehavior.STATE_EXPANDED }

        val recyclerFilter = dialog.findViewById<RecyclerView>(R.id.recycler_filter)
        val checkboxOtherCall = dialog.findViewById<CheckBox>(R.id.checkbox_other_call)
        val btnClear = dialog.findViewById<TextView>(R.id.btn_clear)

        recyclerFilter?.layoutManager = GridLayoutManager(activity,2)
        recyclerFilter?.adapter = filterAdapter
        filterAdapter.restore(filterCheckedId)

        checkboxOtherCall?.visibility = if (otherCallAdapter.itemCount > 0) View.VISIBLE else View.GONE

        checkboxOtherCall?.isChecked = otherCallVisible
        filterOtherCallVisibility(otherCallVisible)

        checkboxOtherCall?.setOnCheckedChangeListener { buttonView, isChecked ->
            filterOtherCallVisibility(isChecked)
        }

        btnClear?.setOnClickListener {
            filterCheckedId.clear()
            SharedPreferenceData.setArrayString(requireContext(),9992, emptySet())
            recyclerFilter?.layoutManager = GridLayoutManager(activity,2)
            recyclerFilter?.adapter = filterAdapter

            syncObserver()
            btn_filter.setImageResource(R.drawable.ic_filter_disable)
        }

        dialog.show()
    }

    private fun filterOtherCallVisibility(isChecked: Boolean) {
        if (isChecked) {
            otherCallVisible = true
            if (otherCallAdapter.itemCount > 0) {
                head_other.visibility = View.VISIBLE
                recycler_other.visibility = View.VISIBLE

                if (tabArray.size == 2) {
                    tabArray.add(2,2)
                    tab_lay.addTab(tab_lay.newTab().setText("Other Call"), 2)
                }
            } else {
                head_other?.visibility = View.GONE
                recycler_other?.visibility = View.GONE
            }
        } else {
            otherCallVisible = false
            head_other.visibility = View.GONE
            recycler_other.visibility = View.GONE

            if (tabArray.size == 3) {
                tabArray.removeAt(2)
                tab_lay?.removeTabAt(2)
            }
        }
    }

    private fun filterObserver() {
        val enumLive = enumVM.getByTableAndColumn("service_call","call_status")
        enumLive.observe(viewLifecycleOwner, Observer {
            enumLive.removeObservers(viewLifecycleOwner)
            filterAdapter.clear()

            if (it.isNotEmpty()) {
                it.forEach { enumData->
                    filterAdapter.addData(enumData)
                }

                val savedFilter = restoreFilter()
                when {
                    it.size == savedFilter.size -> {
                        btn_filter.setImageResource(R.drawable.ic_filter_disable)
                    }
                    savedFilter.isEmpty() -> {
                        btn_filter.setImageResource(R.drawable.ic_filter_disable)
                    }
                    savedFilter.isNotEmpty() -> {
                        btn_filter.setImageResource(R.drawable.ic_filter_enable)
                    }
                }
            }
        })
    }

    private fun restoreFilter(): Set<String> {
        // Restore filter from shared preference
        val savedFilter = SharedPreferenceData.getArrayString(requireContext(),9992, emptySet())
        if (savedFilter.isNotEmpty()) {
            filterCheckedId.clear()
            savedFilter.forEach { restoreFilter->
                filterCheckedId.add(restoreFilter)
            }
        }
        return savedFilter
    }

    private fun filterListener() {
        filterAdapter.setEventHandler(object: FilterAdapter.RecyclerClickListener {
            override fun isChecked(checkedId: ArrayList<String>, checkedAll: Boolean) {
                filterCheckedId = checkedId
                SharedPreferenceData.setArrayString(requireContext(),9992,filterCheckedId.toSet())

                if (!checkedAll) {
                    btn_filter.setImageResource(R.drawable.ic_filter_enable)
                } else if (checkedAll) {
                    btn_filter.setImageResource(R.drawable.ic_filter_disable)
                }
                syncObserver()
            }
        })
    }

    private fun searchListener() {
        search_value.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) { }
            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) { }

            override fun afterTextChanged(p0: Editable?) {
                if (p0 != null) {
                    syncObserver()
                }
            }
        })
    }

    private fun syncObserver() {
        Menu2.isLoading.observe(viewLifecycleOwner, {
            Log.e("aim", "sync proses $it")
            if (it == false) {
                callObserver()
            }
        })
    }

    private fun callObserver() {
        restoreFilter()
        shimmer_container.visibility = View.VISIBLE
        val callObserver = serviceCallVM.getAll(search_value.text.toString(), filterCheckedId)
        callObserver.observe(viewLifecycleOwner, {
            callObserver.removeObservers(viewLifecycleOwner)

            val myCallData = ArrayList<ServiceCallEntity>()
            val teamCallData = ArrayList<ServiceCallEntity>()
            val otherCallData = ArrayList<ServiceCallEntity>()

            swipe_container.isRefreshing = false
            shimmer_container.visibility = View.GONE

            val engineerId = SharedPreferenceData.getString(requireContext(), 8, "")
            it.forEach { callData ->
                if (!callData.engineer.isNullOrEmpty()) {
                    var teamCall = false

                    callData.engineer.forEach { engineerData ->
                        if (engineerData != null) {
                            if (engineerData.id == engineerId) {
                                teamCall = true
                            }
                        }
                    }

                    when {
                        callData.leadEngineerId == engineerId -> {
                            myCallData.add(callData)
                        }
                        teamCall -> {
                            teamCallData.add(callData)
                        }
                        else -> {
                            otherCallData.add(callData)
                        }
                    }
                } else {
                    otherCallData.add(callData)
                }
            }

            myCallAdapter.setList(myCallData, true)
            teamCallAdapter.setList(teamCallData)
            otherCallAdapter.setList(otherCallData)

            visibilityListener()
        })
    }

    private fun visibilityListener() {
        tab_lay.removeAllTabs()
        tabArray = arrayListOf(0,1)
        tab_lay.addTab(tab_lay.newTab().setText("My Call"), 0)
        tab_lay.addTab(tab_lay.newTab().setText("Join Call"), 1)
//        tab_lay.addTab(tab_lay.newTab().setText("Other Call"), 2)

//        if (otherCallAdapter.itemCount == 0) {
//            head_other?.visibility = View.GONE
//            recycler_other?.visibility = View.GONE
//            if (tabArray.size == 3) {
//                tabArray.removeAt(2)
//                tab_lay?.removeTabAt(2)
//            }
//        } else {
//            head_other?.visibility = View.VISIBLE
//            recycler_other?.visibility = View.VISIBLE
//        }

        if (teamCallAdapter.itemCount == 0) {
            head_team?.visibility = View.GONE
            recycler_team?.visibility = View.GONE
            tabArray.removeAt(1)
            tab_lay?.removeTabAt(1)
        } else {
            head_team?.visibility = View.VISIBLE
            recycler_team?.visibility = View.VISIBLE
        }

        if (myCallAdapter.itemCount == 0) {
            head_my?.visibility = View.GONE
            recycler_my?.visibility = View.GONE
            tabArray.removeAt(0)
            tab_lay?.removeTabAt(0)
        } else {
            head_my?.visibility = View.VISIBLE
            recycler_my?.visibility = View.VISIBLE
        }

        if (myCallAdapter.itemCount > 0 || teamCallAdapter.itemCount > 0 || otherCallAdapter.itemCount > 0) {
            tab_lay.visibility = View.VISIBLE
        } else {
            tab_lay.visibility = View.GONE
        }
    }

}
