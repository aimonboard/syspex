package com.syspex.ui

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.View
import android.widget.AdapterView
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.syspex.R
import com.syspex.data.local.database.global_enum.EnumViewModel
import com.syspex.data.local.database.product_group_item.ProductGroupItemViewModel
import com.syspex.data.local.database.product_sparepart.SparepartEntity
import com.syspex.data.local.database.product_sparepart.SparepartViewModel
import com.syspex.data.local.database.service_report.ServiceReportViewModel
import com.syspex.data.local.database.service_report_equipment.ServiceReportEquipmentViewModel
import com.syspex.data.local.database.service_report_sparepart.ServiceReportSparepartEntity
import com.syspex.data.local.database.service_report_sparepart.ServiceReportSparepartViewModel
import com.syspex.data.model.SpinnerModel
import com.syspex.ui.adapter.SparePartAddAdapter
import com.syspex.ui.custom.AutoTextItemNumberAdapter
import com.syspex.ui.custom.AutoTextPartNumberAdapter
import com.syspex.ui.custom.SpinnerAdapter
import com.syspex.ui.helper.FunHelper
import kotlinx.android.synthetic.main.spare_part_add_activity.*
import kotlinx.android.synthetic.main.sub_header.back_link
import kotlinx.android.synthetic.main.sub_header.header_name
import java.lang.Exception

class SparePartAdd : AppCompatActivity() {

    private lateinit var enumVM: EnumViewModel
    private lateinit var productGroupItemVM: ProductGroupItemViewModel
    private lateinit var masterSparepartVM: SparepartViewModel
    private lateinit var reportVM: ServiceReportViewModel
    private lateinit var reportEquipmentVM: ServiceReportEquipmentViewModel
    private lateinit var reportSparepartVM: ServiceReportSparepartViewModel

    private val adapter = SparePartAddAdapter()
    private val spinnerStatus = ArrayList<SpinnerModel>()
    private var spinnerStatusPos = 0

    private lateinit var editItem: ServiceReportSparepartEntity

    private var selectedItem :SparepartEntity? = null

    private var reportId = ""
    private var reportEquipmentId = ""
    private var productId = ""
    private var qty = 1

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.spare_part_add_activity)
        header_name.text = "Add Sparepart"

        enumVM = ViewModelProviders.of(this).get(EnumViewModel::class.java)
        productGroupItemVM = ViewModelProviders.of(this).get(ProductGroupItemViewModel::class.java)
        masterSparepartVM = ViewModelProviders.of(this).get(SparepartViewModel::class.java)
        reportVM = ViewModelProviders.of(this).get(ServiceReportViewModel::class.java)
        reportEquipmentVM = ViewModelProviders.of(this).get(ServiceReportEquipmentViewModel::class.java)
        reportSparepartVM = ViewModelProviders.of(this).get(ServiceReportSparepartViewModel::class.java)

        generateView()
        generateAdapter()
        reportSparepartObserver()
        spinnerStatus()
        handlingQty()
        buttonListener()

    }

    private fun generateView() {
        if (intent.hasExtra("reportId")) {
            reportId = intent.getStringExtra("reportId")!!
            reportEquipmentId = intent.getStringExtra("reportEquipmentId")!!
            productId = intent.getStringExtra("productId")!!

            Log.e("aim","report id: $reportId, reportequipment id: $reportEquipmentId, product id: $productId")
            masterSparepartObserver()
        }
    }

    private fun buttonListener() {
        back_link.setOnClickListener {
            onBackPressed()
        }

        btn_edit_cancel.setOnClickListener {
            handlingVisibilityInsert()
            clearForm()
        }

        btn_edit_save.setOnClickListener {
            if (checkValue()) {
                testEdit()
                Log.e("aim", "sparepart : $selectedItem")
            }
        }

        btn_down.setOnClickListener {
            if (recycler_sparepart.visibility == View.VISIBLE) {
                btn_down.rotation = 180f
                recycler_sparepart.visibility = View.GONE
            } else {
                btn_down.rotation = 0f
                recycler_sparepart.visibility = View.VISIBLE
            }
        }

        btn_submit.setOnClickListener {
            if (checkValue()) {
                testInsert()
                Log.e("aim", "sparepart : $selectedItem")
            }
        }
    }

    private fun checkValue(): Boolean{
        var cek = 0

//        if(part_test.text.toString().isEmpty()){
//            part_err.text = "*Required"; cek++
//        } else { part_err.text = "" }

        if(desc_value.text.toString().isEmpty()){
            desc_err.text = "*Required"; cek++
        } else { desc_err.text = "" }

        if(quantity_value.text.toString() == "0"){
            Toast.makeText(this, "Minimum quantity 1",Toast.LENGTH_LONG).show(); cek++
        }

        if (cek == 0){
            return true
        }
        return false
    }

    private fun handlingVisibilityEdit() {
        tx_sparepart.visibility = View.GONE
        btn_down.visibility = View.GONE
        recycler_sparepart.visibility = View.GONE
        tx_sparepart_add.text = "Edit Sparepart"
        btn_edit_cancel.visibility = View.VISIBLE
        btn_edit_save.visibility = View.VISIBLE
        btn_submit.visibility = View.GONE
    }

    private fun handlingVisibilityInsert() {
        tx_sparepart.visibility = View.VISIBLE
        btn_down.visibility = View.VISIBLE
        recycler_sparepart.visibility = View.VISIBLE
        tx_sparepart_add.text = "Add Sparepart"
        btn_edit_cancel.visibility = View.GONE
        btn_edit_save.visibility = View.GONE
        btn_submit.visibility = View.VISIBLE
    }

    private fun generateAdapter() {
        recycler_sparepart?.layoutManager = LinearLayoutManager(this, RecyclerView.VERTICAL, false)
        recycler_sparepart?.adapter = adapter

        adapter.setEventHandler(object: SparePartAddAdapter.RecyclerClickListener {
            override fun isDelete(position: Int, data: ServiceReportSparepartEntity) {
                deleteDialog(position, data)

            }

            override fun isEdit(position: Int, data: ServiceReportSparepartEntity) {
                handlingVisibilityEdit()
                editItem = data

                desc_value.setText(data.spare_part_description)
                remark_value_7454.setText(data.spare_part_memo)
                quantity_value.text = data.qty
                qty = data.qty.toInt()

                handlingQty()

                item_test.setText(data.spare_part_image_number)
                part_test.setText(data.spare_part_number)

                spinnerStatus.forEachIndexed { index, spinnerModel ->
                    if (spinnerModel.spinnerId == data.enum_id) {
                        spinner_sparepart.setSelection(index)
                    }
                }
            }

        })
    }

    private fun masterSparepartObserver() {
        productGroupItemVM.getByProductId(productId).observe(this, Observer { productGroupData->
            if (productGroupData.isNotEmpty()) {

                val productGroupId = productGroupData.first().product_group_id

                masterSparepartVM.getByProductGroupId(productGroupId).observe(this, Observer { sparepartList->
                    if (sparepartList.isNotEmpty()) {
                        val sparepartImage = ArrayList<String>()
                        val sparepartPage = ArrayList<String>()

                        val sparepartSorted = sparepartList.sortedBy { it.page }
                        sparepartSorted.forEach { sparepart->
                            if (!sparepartPage.contains(sparepart.page)) {
                                sparepartPage.add(sparepart.page)
                                sparepartImage.add(sparepart.url)
                            }
                        }

                        handlingPageImage(sparepartPage, sparepartImage, sparepartList)

                    }
                })

            }
        })
    }

    private fun reportSparepartObserver() {
        reportSparepartVM.getAll().observe(this, Observer {
            Log.e("aim","sparepart all : $it")
        })

        reportSparepartVM.getByReportEquipmentId(reportEquipmentId).observe(this, { sparepart->
            adapter.setList(sparepart, spinnerStatus)
            sparepart.forEach {
                Log.e("aim","part id: ${it.part_id}, part no: ${it.part_no}, status: ${it.enum_name}")
            }
        })
    }

    private fun spinnerStatus(){
        enumVM.getByTableName("service_report_sparepart").observe(this, Observer {
            it.forEach {  enumData->
                spinnerStatus.add(SpinnerModel(enumData.enum_id, enumData.enum_name))
                val arrayAdapter = SpinnerAdapter(this, R.layout.item_spinner_black, spinnerStatus)
                spinner_sparepart?.adapter = arrayAdapter

                spinner_sparepart?.onItemSelectedListener = object : AdapterView.OnItemSelectedListener{
                    override fun onNothingSelected(parent: AdapterView<*>?) { }
                    override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                        spinnerStatusPos = position
                    }
                }
            }
        })
    }

    private fun handlingPageImage(page: ArrayList<String>, sparepartImage: ArrayList<String>, sparepart: List<SparepartEntity>) {
        var currentPage = 1

        // Firt Load (page 1)
        try {
            handlingPageData(page[0], sparepart)
            Glide.with(this).load(sparepartImage[currentPage - 1]).into(image_lay)

            if (sparepartImage.size == 1) {
                btn_left.visibility = View.GONE
                btn_right.visibility = View.GONE
            } else  if (sparepartImage.size >= 1) {
                btn_left.visibility = View.GONE
            }

        } catch (e: Exception) { }

        image_lay.setOnClickListener {
//            DialogImagePreview.helpText = ""
//            DialogImagePreview.helpImage = imageUrl
//            DialogImagePreview().show((it.context as FragmentActivity).supportFragmentManager,DialogImagePreview().tag)

            Log.e("aim", "image send : ${sparepartImage[currentPage - 1]}")
            val i = Intent(this, ImagePreview::class.java)
            i.putExtra("image", sparepartImage[currentPage - 1])
            startActivity(i)
        }

        btn_left.setOnClickListener {
            Log.e("aim", "page : $currentPage")
            if (currentPage >= 0) {
                currentPage--
                try {
                    handlingPageData(page[currentPage - 1], sparepart)
                    Glide.with(this).load(sparepartImage[currentPage - 1]).into(image_lay)
                } catch (e: Exception) { }

                if (currentPage == 1) {
                    btn_left.visibility = View.GONE
                    btn_right.visibility = View.VISIBLE
                } else {
                    btn_left.visibility = View.VISIBLE
                    btn_right.visibility = View.VISIBLE
                }

            }
        }

        btn_right.setOnClickListener {
            Log.e("aim", "page : $currentPage")
            if (currentPage <= sparepartImage.lastIndex) {
                currentPage++
                try {
                    handlingPageData(page[currentPage - 1], sparepart)
                    Glide.with(this).load(sparepartImage[currentPage - 1]).into(image_lay)
                } catch (e: Exception) {
                    Log.e("aim", "Sparepart page : $e")
                }

                if (currentPage == sparepartImage.size) {
                    btn_right.visibility = View.GONE
                    btn_left.visibility = View.VISIBLE
                } else {
                    btn_right.visibility = View.VISIBLE
                    btn_left.visibility = View.VISIBLE
                }

            }
        }
    }

    private fun handlingPageData(currentPage: String, sparepart: List<SparepartEntity>) {

        var dataPerPage = sparepart.filter { it.page == currentPage }
        dataPerPage = dataPerPage.sortedBy { it.image_number }

        // ========================================================================================= Item Number
        item_test.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) { }
            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) { }

            override fun afterTextChanged(p0: Editable?) {
                if (p0 != null) {
                    val dataFilter = dataPerPage.filter {
                        it.image_number == p0.toString()
                    }

                    if (dataFilter.isNotEmpty()) {
                        selectedItem = dataFilter.first()
                        part_test.setText(dataFilter.first().part_no)
                        desc_value.setText(dataFilter.first().part_name)

                        Log.e("aim", "selected sparepart : ${dataFilter.first()}")
                    } else {
                        selectedItem = null
//                        part_test.setText("")
//                        desc_value.setText("")
                    }
                }
            }
        })


        // ========================================================================================= Part Number
//        part_test.addTextChangedListener(object : TextWatcher {
//            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) { }
//            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) { }
//
//            override fun afterTextChanged(p0: Editable?) {
//                if (p0 != null) {
//                    dataPerPage.forEach {
//                        if (it.part_no == p0.toString()) {
//                            selectedItem = it
//                            item_test.setText(it.image_number)
//                        } else {
//                            item_test.setText("N/A")
//                        }
//                    }
//                }
//            }
//        })

    }

    private fun handlingQty() {
        quantity_value.text = "$qty"

        btn_minus.setOnClickListener {
            Log.e("aim","minus click")
            if (qty > 1) {
                qty--
                quantity_value.text = "$qty"
            }
        }

        btn_plus.setOnClickListener {
            Log.e("aim","plus click")
            qty++
            quantity_value.text = qty.toString()
        }
    }

    private fun deleteDialog(position: Int, entity: ServiceReportSparepartEntity) {
        AlertDialog.Builder(this)
            .setMessage("Delete this item ?")
            .setPositiveButton(android.R.string.yes) { dialog, whichButton ->
//                adapter.notifyItemRemoved(position)
                testDelete(entity)
            }
            .setNegativeButton(android.R.string.no, null)
            .show()
    }

    private fun testInsert() {
        if (selectedItem != null) {
            // Master sparepart
            val check = reportSparepartVM.insertCheck(
                reportEquipmentId,
                selectedItem?.part_id ?: "",
                spinnerStatus[spinnerStatusPos].spinnerId
            )
            check.observe(this, Observer {
                check.removeObservers(this)
                if (it.isEmpty()) {
                    val data = ServiceReportSparepartEntity(
                        FunHelper.randomString("") ?: "",
                        reportEquipmentId ?: "",
                        item_test.text.toString() ?: "",
                        part_test.text.toString() ?: "",
                        desc_value.text.toString() ?: "",
                        remark_value_7454.text.toString() ?: "",
                        spinnerStatus[spinnerStatusPos].spinnerId ?: "",
                        spinnerStatus[spinnerStatusPos].spinnerName ?: "",
                        selectedItem?.part_id ?: "",
                        part_test.text.toString() ?: "",
                        selectedItem?.part_name ?: "",
                        qty.toString() ?: "",
                        false,
                        true
                    )
                    reportSparepartVM.insert(data)

                    clearForm()
                    updateFlagUpload()
                }
                else {
                    Toast.makeText(this, "Data already exist", Toast.LENGTH_LONG).show()
                }
            })
        }
        else {
            // Other sparepart part id null
            val data = ServiceReportSparepartEntity(
                FunHelper.randomString("") ?: "",
                reportEquipmentId ?: "",
                item_test.text.toString() ?: "",
                part_test.text.toString() ?: "",
                desc_value.text.toString() ?: "",
                remark_value_7454.text.toString() ?: "",
                spinnerStatus[spinnerStatusPos].spinnerId ?: "",
                spinnerStatus[spinnerStatusPos].spinnerName ?: "",
                selectedItem?.part_id ?: "",
                part_test.text.toString() ?: "",
                selectedItem?.part_name ?: "",
                qty.toString() ?: "",
                false,
                true
            )
            reportSparepartVM.insert(data)

            clearForm()
            updateFlagUpload()
        }
    }

    private fun testEdit() {
        if (selectedItem != null) {
            val check = reportSparepartVM.insertCheck(
                reportEquipmentId,
                selectedItem?.part_id ?: "",
                spinnerStatus[spinnerStatusPos].spinnerId
            )
            check.observe(this, Observer { dataCheck ->
                check.removeObservers(this)
                if (dataCheck.isEmpty()) {
                    // Update all column
                    val data = ServiceReportSparepartEntity(
                        editItem.service_report_sparepart_id ?: "",
                        reportEquipmentId ?: "",
                        item_test.text.toString() ?: "",
                        part_test.text.toString() ?: "",
                        desc_value.text.toString() ?: "",
                        remark_value_7454.text.toString() ?: "",
                        spinnerStatus[spinnerStatusPos].spinnerId ?: "",
                        spinnerStatus[spinnerStatusPos].spinnerName ?: "",
                        selectedItem?.part_id ?: "",
                        part_test.text.toString() ?: "",
                        selectedItem?.part_name ?: "",
                        qty.toString() ?: "",
                        false,
                        true
                    )
                    reportSparepartVM.update(data)

                    handlingVisibilityInsert()
                    clearForm()
                    updateFlagUpload()
                }
                else {
                    Toast.makeText(this, "Data already exist", Toast.LENGTH_LONG).show()
                }
            })
        }
        else {
            val data = ServiceReportSparepartEntity(
                editItem.service_report_sparepart_id ?: "",
                reportEquipmentId ?: "",
                item_test.text.toString() ?: "",
                part_test.text.toString() ?: "",
                desc_value.text.toString() ?: "",
                remark_value_7454.text.toString() ?: "",
                spinnerStatus[spinnerStatusPos].spinnerId ?: "",
                spinnerStatus[spinnerStatusPos].spinnerName ?: "",
                selectedItem?.part_id ?: "",
                part_test.text.toString() ?: "",
                selectedItem?.part_name ?: "",
                qty.toString() ?: "",
                false,
                true
            )
            reportSparepartVM.update(data)

            handlingVisibilityInsert()
            clearForm()
            updateFlagUpload()
        }
    }

    private fun testDelete(entity: ServiceReportSparepartEntity) {
        reportSparepartVM.delete(entity)
        updateFlagUpload()
    }

    private fun updateFlagUpload() {
        reportVM.updateFlag(reportId,false)
        reportEquipmentVM.updateFlag(reportEquipmentId, false)
    }

    private fun clearForm() {
        item_test.setText("")
        part_test.setText("")
        desc_value.setText("")
        remark_value_7454.setText("")

        part_err.text = ""
        desc_err.text = ""

        qty = 1
        quantity_value.text = qty.toString()
    }

}
