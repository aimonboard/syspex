package com.syspex.ui

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.webkit.*
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.syspex.Menu2
import com.syspex.R
import kotlinx.android.synthetic.main.activity_news_detail.*
import kotlinx.android.synthetic.main.activity_news_detail.swipe_container


class NewsDetail : AppCompatActivity() {

    private var fcmStatus = false
    var contentId = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_news_detail)

        generateView()
        buttonListener()

    }

    override fun onBackPressed() {
        super.onBackPressed()
        if (fcmStatus) {
            val intent = Intent(this, Menu2::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_NEW_TASK)
            startActivity(intent)
            finish()
        }
    }

    private fun generateView() {
        header_name.text = intent.getStringExtra("title") ?: "News Detail"
        when {
            // From fcm
            intent.hasExtra("news_id") -> {
                fcmStatus = true
                contentId = intent.getStringExtra("news_id") ?: ""
            }
            // From notif page
            intent.hasExtra("notif_news_id") -> {
                contentId = intent.getStringExtra("notif_news_id") ?: ""
            }
            // Normal flow
            intent.hasExtra("id") -> {
                contentId = intent.getStringExtra("id") ?: ""
            }
        }

        if (contentId.isNotEmpty()) {
            loadContent()
        } else {
            Toast.makeText(this,"News not found",Toast.LENGTH_LONG).show()
        }
    }

    private fun buttonListener() {
        back_link.setOnClickListener {
            onBackPressed()
        }

        swipe_container.setOnRefreshListener {
            if (contentId.isNotEmpty()) {
                loadContent()
            } else {
                Toast.makeText(this,"News not found",Toast.LENGTH_LONG).show()
            }
        }
    }

    @SuppressLint("SetJavaScriptEnabled", "SetTextI18n")
    private fun loadContent() {
        swipe_container.isRefreshing = true
        val url = "https://ssa.syspex.com/news/read/$contentId"

        webView.fitsSystemWindows = true
        webView.settings.javaScriptEnabled = true
        webView.settings.loadWithOverviewMode = true
        webView.settings.useWideViewPort = true
        webView.settings.builtInZoomControls = true
        webView.settings.displayZoomControls = false
        webView.webViewClient = object : WebViewClient() {
            override fun onPageFinished(view: WebView, url: String) {
                swipe_container?.isRefreshing = false
            }
        }

        webView.loadUrl(url)
        Log.e("aim","news url : $url")

    }

}