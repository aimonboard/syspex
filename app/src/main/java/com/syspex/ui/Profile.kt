package com.syspex.ui

import android.annotation.SuppressLint
import android.app.ProgressDialog
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import androidx.core.view.GravityCompat
import androidx.drawerlayout.widget.DrawerLayout
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import com.bumptech.glide.Glide
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.syspex.BuildConfig
import com.syspex.MainActivity
import com.syspex.R
import com.syspex.data.local.SharedPreferenceData
import com.syspex.data.local.database.AppDatabase
import com.syspex.data.local.database.file.FileViewModel
import com.syspex.data.remote.LoginRequest
import com.syspex.data.retrofit.ApiClient
import com.syspex.ui.helper.FunHelper
import kotlinx.android.synthetic.main.profile_fragment.*
import okhttp3.ResponseBody
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import kotlin.Exception

class Profile : Fragment() {

    companion object {
        var profileImage = ""
    }

    private lateinit var fileViewModel: FileViewModel

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.profile_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        fileViewModel = ViewModelProviders.of(this).get(FileViewModel::class.java)

        generateView()
        buttonListener()

    }

    // Return from dialog upload file
    override fun onResume() {
        super.onResume()
        if (profileImage.isNotEmpty()) {
            try {
                Glide.with(this).load(profile_image).into(profile_image)
                profileImage = ""
            } catch (e: Exception) { }
        }
    }

    @SuppressLint("SetTextI18n")
    private fun generateView() {
        val versionName = BuildConfig.VERSION_NAME.split(" / ")
        version_code_value.text = "Version ${versionName[0]}"
        version_date_value.text = versionName[1]

        profile_name.text = SharedPreferenceData.getString(context,10,"")
        profile_job.text = SharedPreferenceData.getString(context,13,"")
        phone_value.text = SharedPreferenceData.getString(context, 12, "").ifEmpty { "-" }
        email_value.text = SharedPreferenceData.getString(context, 11, "").ifEmpty { "-" }


//        try {
//            Glide.with(this)
//                .load("https://www.telegraph.co.uk/content/dam/education/SPARK/stem-awards/stem-hq/female-engineer-getty.jpg")
//                .into(profile_image)
//        } catch (e: Exception) { }
    }

    private fun buttonListener() {
        val drawerLayout: DrawerLayout = requireActivity().findViewById(R.id.drawer_layout)
        btn_menu?.setOnClickListener {
            if(!drawerLayout.isDrawerOpen(GravityCompat.START))
                drawerLayout.openDrawer(GravityCompat.START)
        }

        switch_recycler.isChecked = SharedPreferenceData.getBoolean(requireContext(),7,false)
        switch_recycler.setOnCheckedChangeListener { _, b ->
            if (b) {
                SharedPreferenceData.setBoolean(requireContext(),7,true)
            } else {
                SharedPreferenceData.setBoolean(requireContext(),7,false)
            }
        }

        profile_image.setOnClickListener {
//            Toast.makeText(requireContext(),"Under construction", Toast.LENGTH_LONG).show()
//            DialogUploadFile.fileSection = "profile"
//            DialogUploadFile().show((requireContext() as FragmentActivity).supportFragmentManager,DialogUploadFile().tag)
        }

        btn_edit_profile.setOnClickListener {
            dialogEditProfile()
        }

        btn_signature.setOnClickListener {
            val i = Intent(requireContext(), EngineerSignature::class.java)
            startActivity(i)
        }

        btn_change_pass.setOnClickListener {
            val i = Intent(requireContext(), PasswordChange::class.java)
            requireContext().startActivity(i)
        }

        btn_out.setOnClickListener {
            logout()
//            clearLocalData()
        }
    }

    private fun dialogEditProfile() {
        val view = layoutInflater.inflate(R.layout.dialog_edit_profile,null)
        val dialog = BottomSheetDialog(requireContext())
        dialog.setContentView(view)

        val phoneText = dialog.findViewById<EditText>(R.id.phone_value)
        val emailText = dialog.findViewById<EditText>(R.id.email_value)
        val buttonSave = dialog.findViewById<TextView>(R.id.btn_save)

        phoneText?.setText(SharedPreferenceData.getString(requireContext(),12,""))
        emailText?.setText(SharedPreferenceData.getString(requireContext(),11,""))

        buttonSave?.setOnClickListener {
            editProfile(
                phoneText?.text.toString(),
                emailText?.text.toString()
            )
            dialog.dismiss()
        }

        dialog.show()
    }

    private fun editProfile(phone: String, email: String) {
        val progressDialog = ProgressDialog(requireContext())
        progressDialog.setMessage("Update Profile ...")
        progressDialog.setCancelable(false)
        progressDialog.show()

        val token = "Bearer "+ SharedPreferenceData.getString(requireContext(),2, "")
        val userId = SharedPreferenceData.getString(requireContext(),8, "")

        val params = HashMap<String, String>()
        params["email"] = email
        params["phone"] = phone

        ApiClient.instance.updateProfile(token, params).enqueue(object: Callback<LoginRequest> {
            override fun onFailure(call: Call<LoginRequest>, t: Throwable) {
                progressDialog.dismiss()
                Toast.makeText(requireContext(), "Update profile failed", Toast.LENGTH_LONG).show()
            }

            override fun onResponse(call: Call<LoginRequest>, response: Response<LoginRequest>) {
                progressDialog.dismiss()
                if (response.isSuccessful) {
                    Toast.makeText(requireContext(), "Profile updated", Toast.LENGTH_LONG).show()

                    SharedPreferenceData.setString(requireContext(), 8,
                        response.body()?.data?.user?.id ?: "")
                    SharedPreferenceData.setString(requireContext(), 9,
                        response.body()?.data?.user?.userCode ?: "")
                    SharedPreferenceData.setString(requireContext(), 10,
                        response.body()?.data?.user?.userFullname ?: "")
                    SharedPreferenceData.setString(requireContext(), 11,
                        response.body()?.data?.user?.email ?: "")
                    SharedPreferenceData.setString(requireContext(), 12,
                        response.body()?.data?.user?.phone ?: "")
                    SharedPreferenceData.setString(requireContext(), 13,
                        response.body()?.data?.user?.role?.roleName ?: "")
                    SharedPreferenceData.setString(requireContext(), 14,
                        response.body()?.data?.user?.region?.regionName ?: "")
                    SharedPreferenceData.setString(requireContext(), 1111,
                        response.body()?.data?.user?.userSignature ?: "")

                    phone_value.text = response.body()?.data?.user?.phone ?: ""
                    email_value.text = response.body()?.data?.user?.email ?: ""
                }
                else {
                    if (response.code() == 401) {
                        FunHelper.clearAllData(requireContext())
                        Toast.makeText(requireContext(), "Session end", Toast.LENGTH_LONG)
                            .show()
                    }
                    else {
                        FunHelper.firebaseApiLog(
                            userId ?: "",
                            response.raw().request().url().toString() ?: "",
                            response.raw().request().headers().toString() ?: "",
                            params.toString() ?: "",
                            response.raw().toString() ?: ""
                        )
                        Toast.makeText(requireContext(), response.raw().toString(), Toast.LENGTH_LONG).show()
                    }
                }
            }
        })
    }

    private fun logout() {
        val progressDialog = ProgressDialog(requireContext())
        progressDialog.setMessage("Logout ...")
        progressDialog.setCancelable(false)
        progressDialog.show()

        val token = "Bearer "+ SharedPreferenceData.getString(requireContext(),2, "")
        val userId = SharedPreferenceData.getString(requireContext(),8, "")

        ApiClient.instance.logout(token).enqueue(object: Callback<ResponseBody> {
            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                progressDialog.dismiss()
                Toast.makeText(requireContext(), "Logout failed", Toast.LENGTH_LONG).show()
            }

            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                progressDialog.dismiss()
                if (response.isSuccessful) {
                    clearLocalData()
                }
                else {
                    if (response.code() == 401) {
                        FunHelper.clearAllData(requireContext())
                        Toast.makeText(requireContext(), "Session end", Toast.LENGTH_LONG).show()
                    }
                    else {
                        FunHelper.firebaseApiLog(
                            userId ?: "",
                            response.raw().request().url().toString() ?: "",
                            response.raw().request().headers().toString() ?: "",
                            "",
                            response.raw().toString() ?: ""
                        )
                        Toast.makeText(requireContext(), response.raw().toString(), Toast.LENGTH_LONG).show()
                    }
                }
            }
        })
    }

    private fun clearLocalData() {
        val appDatabase = AppDatabase.getInstance(requireContext())!!
        val thread = object : Thread() {
            override fun run() {
                appDatabase.clearAllTables()
            }
        }
        thread.start()

        SharedPreferenceData.setString(context, 1, "0")
        SharedPreferenceData.setString(context, 2, "")
        SharedPreferenceData.setString(context, 3, "0")
        SharedPreferenceData.setInt(context, 4, 1)
        SharedPreferenceData.setString(context, 6, "")

        // Profile
        SharedPreferenceData.setString(context, 8, "")
        SharedPreferenceData.setString(context, 9, "")
        SharedPreferenceData.setString(context, 10, "")
        SharedPreferenceData.setString(context, 11, "")
        SharedPreferenceData.setString(context, 12, "")
        SharedPreferenceData.setString(context, 13, "")
        SharedPreferenceData.setString(context, 14, "")
        SharedPreferenceData.setString(context, 1111, "")


        val intent = Intent(context, MainActivity::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_NEW_TASK)
        startActivity(intent)
        activity?.finish()
    }

}
