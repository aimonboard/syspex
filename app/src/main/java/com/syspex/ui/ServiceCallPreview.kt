package com.syspex.ui

import android.annotation.SuppressLint
import android.app.ProgressDialog
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.livinglifetechway.quickpermissions_kotlin.runWithPermissions
import com.syspex.R
import com.syspex.data.local.SharedPreferenceData
import com.syspex.data.local.database.account.AccountViewModel
import com.syspex.data.local.database.account_attachment.AccountAttachmentViewModel
import com.syspex.data.local.database.account_contact.AccountContactViewModel
import com.syspex.data.local.database.activity.ActivityViewModel
import com.syspex.data.local.database.service_call.ServiceCallViewModel
import com.syspex.data.local.database.service_call_completion.ServiceCallCompletionViewModel
import com.syspex.data.local.database.service_call_equipment.ServiceCallEquipmentViewModel
import com.syspex.data.local.database.service_call_handover.ServiceCallHandoverViewModel
import com.syspex.data.local.database.service_call_training.ServiceCallTrainingViewModel
import com.syspex.data.local.database.service_case.ServiceCaseViewModel
import com.syspex.data.local.database.service_report.ServiceReportViewModel
import com.syspex.data.local.database.service_report_equipment.ServiceReportEquipmentViewModel
import com.syspex.data.local.database.service_report_time_tracking.ServiceReportTimeTrackingViewModel
import com.syspex.ui.adapter.*
import com.syspex.ui.helper.DownloadPdf
import com.syspex.ui.helper.FunHelper
import kotlinx.android.synthetic.main.service_call_preview_fragment.*
import kotlinx.android.synthetic.main.service_call_preview_fragment.account_contact_name
import kotlinx.android.synthetic.main.service_call_preview_fragment.account_contact_phone
import kotlinx.android.synthetic.main.service_call_preview_fragment.account_contact_type
import kotlinx.android.synthetic.main.service_call_preview_fragment.account_pic_contact_name
import kotlinx.android.synthetic.main.service_call_preview_fragment.account_pic_contact_phone
import kotlinx.android.synthetic.main.service_call_preview_fragment.account_pic_contact_type
import kotlinx.android.synthetic.main.service_call_preview_fragment.account_sales_pic
import kotlinx.android.synthetic.main.service_call_preview_fragment.activity_count
import kotlinx.android.synthetic.main.service_call_preview_fragment.additional_form_lay
import kotlinx.android.synthetic.main.service_call_preview_fragment.address_value
import kotlinx.android.synthetic.main.service_call_preview_fragment.agreement_type_value
import kotlinx.android.synthetic.main.service_call_preview_fragment.btn_add_attachment
import kotlinx.android.synthetic.main.service_call_preview_fragment.btn_add_complete
import kotlinx.android.synthetic.main.service_call_preview_fragment.btn_add_equipment
import kotlinx.android.synthetic.main.service_call_preview_fragment.btn_add_handover
import kotlinx.android.synthetic.main.service_call_preview_fragment.btn_add_report
import kotlinx.android.synthetic.main.service_call_preview_fragment.btn_add_training
import kotlinx.android.synthetic.main.service_call_preview_fragment.btn_edit_contact
import kotlinx.android.synthetic.main.service_call_preview_fragment.btn_location
import kotlinx.android.synthetic.main.service_call_preview_fragment.btn_phone1
import kotlinx.android.synthetic.main.service_call_preview_fragment.btn_phone2
import kotlinx.android.synthetic.main.service_call_preview_fragment.btn_send_pdf
import kotlinx.android.synthetic.main.service_call_preview_fragment.btn_service_case
import kotlinx.android.synthetic.main.service_call_preview_fragment.btn_sign_all
import kotlinx.android.synthetic.main.service_call_preview_fragment.case_desc_value
import kotlinx.android.synthetic.main.service_call_preview_fragment.case_owner_value
import kotlinx.android.synthetic.main.service_call_preview_fragment.case_sales_pic_value
import kotlinx.android.synthetic.main.service_call_preview_fragment.case_status
import kotlinx.android.synthetic.main.service_call_preview_fragment.case_subject_value
import kotlinx.android.synthetic.main.service_call_preview_fragment.case_title
import kotlinx.android.synthetic.main.service_call_preview_fragment.company_value
import kotlinx.android.synthetic.main.service_call_preview_fragment.installation_air_supply_value
import kotlinx.android.synthetic.main.service_call_preview_fragment.installation_customer_warranty_value
import kotlinx.android.synthetic.main.service_call_preview_fragment.installation_delivery_value
import kotlinx.android.synthetic.main.service_call_preview_fragment.installation_demo_value
import kotlinx.android.synthetic.main.service_call_preview_fragment.installation_inspec_value
import kotlinx.android.synthetic.main.service_call_preview_fragment.installation_install_value
import kotlinx.android.synthetic.main.service_call_preview_fragment.installation_lay
import kotlinx.android.synthetic.main.service_call_preview_fragment.installation_material_value
import kotlinx.android.synthetic.main.service_call_preview_fragment.installation_po_value
import kotlinx.android.synthetic.main.service_call_preview_fragment.installation_pro_value
import kotlinx.android.synthetic.main.service_call_preview_fragment.installation_special_requirement_value
import kotlinx.android.synthetic.main.service_call_preview_fragment.installation_supply_direct_value
import kotlinx.android.synthetic.main.service_call_preview_fragment.installation_warranty_value
import kotlinx.android.synthetic.main.service_call_preview_fragment.isntallation_free_value
import kotlinx.android.synthetic.main.service_call_preview_fragment.recycler_activity
import kotlinx.android.synthetic.main.service_call_preview_fragment.recycler_attachment
import kotlinx.android.synthetic.main.service_call_preview_fragment.recycler_completion
import kotlinx.android.synthetic.main.service_call_preview_fragment.recycler_equipment
import kotlinx.android.synthetic.main.service_call_preview_fragment.recycler_handover
import kotlinx.android.synthetic.main.service_call_preview_fragment.recycler_service_report
import kotlinx.android.synthetic.main.service_call_preview_fragment.recycler_time_tracking
import kotlinx.android.synthetic.main.service_call_preview_fragment.recycler_training
import kotlinx.android.synthetic.main.service_call_preview_fragment.sub_title
import kotlinx.android.synthetic.main.service_call_preview_fragment.technical_pic_value
import kotlinx.android.synthetic.main.service_call_preview_fragment.time_tracking_lay
import kotlinx.android.synthetic.main.service_call_preview_fragment.tx_attachment_count
import kotlinx.android.synthetic.main.service_call_preview_fragment.tx_equipment_count
import kotlinx.android.synthetic.main.service_call_preview_fragment.tx_report_count
import kotlinx.android.synthetic.main.service_call_preview_fragment.tx_time_tracking_count
import kotlinx.android.synthetic.main.service_call_preview_fragment.warning_activity
import kotlinx.android.synthetic.main.service_call_preview_fragment.warning_additional
import kotlinx.android.synthetic.main.service_call_preview_fragment.warning_attachment
import kotlinx.android.synthetic.main.service_call_preview_fragment.warning_completion
import kotlinx.android.synthetic.main.service_call_preview_fragment.warning_equipment
import kotlinx.android.synthetic.main.service_call_preview_fragment.warning_handover
import kotlinx.android.synthetic.main.service_call_preview_fragment.warning_installation
import kotlinx.android.synthetic.main.service_call_preview_fragment.warning_report
import kotlinx.android.synthetic.main.service_call_preview_fragment.warning_time_tracking
import kotlinx.android.synthetic.main.service_call_preview_fragment.warning_training
import java.net.URL

class ServiceCallPreview : BottomSheetDialogFragment() {

    companion object {
        var callId = ""
    }

    private lateinit var caseVM: ServiceCaseViewModel
    private lateinit var accountVM: AccountViewModel
    private lateinit var accountContactVM: AccountContactViewModel
    private lateinit var callVM: ServiceCallViewModel
    private lateinit var callTrainingVM: ServiceCallTrainingViewModel
    private lateinit var callHandoverVM: ServiceCallHandoverViewModel
    private lateinit var callCompletionVM: ServiceCallCompletionViewModel
    private lateinit var callEquipmentVM: ServiceCallEquipmentViewModel
    private lateinit var reportVM: ServiceReportViewModel
    private lateinit var reportEquipmentVM: ServiceReportEquipmentViewModel
    private lateinit var timeTrackingVM: ServiceReportTimeTrackingViewModel
    private lateinit var activityVM: ActivityViewModel
    private lateinit var attachmentVM: AccountAttachmentViewModel

    private val reportAdapter = SRMyReportAdapter()
    private val timeTrackingAdapter = SRMyTimeTrackingAdapter()
    private val equipmentAdapter = EquipmentListAdapter()
    private val activityAdapter = ActivityListAdapter()
    private val attachmentAdapter = AttachmentAdapter()
    private val trainingAdapter = CallTrainingAdapter()
    private val handoverAdapter = CallHandoverAdapter()
    private val completionAdapter = CallCompletionAdapter()

    private var caseId = ""
    private var caseNumber = ""
    private var casePo = ""
    private var callNumber = ""
    private var accountId = ""
    private var accountName = ""
    private var addressId = ""
    private var addressText = ""
    private var leadEngineerId = ""
    private var leadEngineerName = ""
    private var picId = ""
    private var picName = ""

    // Sign all report
    private var timeTrackingAdded = false
    private var problemAdded = false
    private var solutionAdded = false

    // Send email get from call
    private var accountContact = ""
    private var accountFieldPic = ""

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.service_call_preview_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        caseVM = ViewModelProviders.of(this).get(ServiceCaseViewModel::class.java)
        accountVM = ViewModelProviders.of(this).get(AccountViewModel::class.java)
        accountContactVM = ViewModelProviders.of(this).get(AccountContactViewModel::class.java)
        callVM = ViewModelProviders.of(this).get(ServiceCallViewModel::class.java)
        callTrainingVM = ViewModelProviders.of(this).get(ServiceCallTrainingViewModel::class.java)
        callHandoverVM = ViewModelProviders.of(this).get(ServiceCallHandoverViewModel::class.java)
        callCompletionVM = ViewModelProviders.of(this).get(ServiceCallCompletionViewModel::class.java)
        callEquipmentVM = ViewModelProviders.of(this).get(ServiceCallEquipmentViewModel::class.java)
        reportVM = ViewModelProviders.of(this).get(ServiceReportViewModel::class.java)
        reportEquipmentVM = ViewModelProviders.of(this).get(ServiceReportEquipmentViewModel::class.java)
        timeTrackingVM = ViewModelProviders.of(this).get(ServiceReportTimeTrackingViewModel::class.java)
        activityVM = ViewModelProviders.of(this).get(ActivityViewModel::class.java)
        attachmentVM = ViewModelProviders.of(this).get(AccountAttachmentViewModel::class.java)

        buttonListener()
        setupAdapter()
        trainingAdapterListener()
        handoverAdapterListener()
        completionAdapterListener()
        attachmentAdapterListener()
        setupObserver()
    }

    private fun buttonListener() {
        btn_service_case.setOnClickListener {
            val i = Intent(requireContext(), CaseDetail::class.java)
            i.putExtra("id", caseId)
            i.putExtra("number", caseNumber)
            startActivity(i)
        }

        btn_location.setOnClickListener {
            FunHelper.direction(requireContext(), address_value.text.toString())
        }

        btn_phone1.setOnClickListener {
            FunHelper.phoneCall(requireContext(), account_contact_phone.text.toString())
        }

        btn_phone2.setOnClickListener {
            FunHelper.phoneCall(requireContext(), account_pic_contact_phone.text.toString())
        }

        btn_edit_contact.setOnClickListener {
            val i = Intent(requireContext(), ContactAccount::class.java)
            i.putExtra("accountId", accountId)
            i.putExtra("accountName", accountName)
            startActivity(i)
        }

        btn_add_report.setOnClickListener {
            val i = Intent(requireContext(), ServiceReportAdd::class.java)
            i.putExtra("caseId", caseId)
            i.putExtra("callId", callId)
            i.putExtra("callNumber", callNumber)
            startActivity(i)
        }

        btn_add_equipment.setOnClickListener {
            val i = Intent(requireContext(), EquipmentAdd::class.java)
            i.putExtra("section", "call")
            i.putExtra("caseId", caseId)
            i.putExtra("callId", callId)
            i.putExtra("accountId", accountId)
            startActivity(i)
        }

        btn_add_training.setOnClickListener {
            // trained by default by login id
            val loginId = SharedPreferenceData.getString(requireContext(),8,"")
            val loginName = SharedPreferenceData.getString(requireContext(),10,"")

            val i = Intent(requireContext(), TrainingAdd::class.java)
            i.putExtra("callId", callId)
            i.putExtra("accountId", accountId)
            i.putExtra("accountName", accountName)
            i.putExtra("addressId", addressId)
            i.putExtra("addressText", addressText)
            i.putExtra("engineerId", loginId)
            i.putExtra("engineerName", loginName)
            i.putExtra("picId", picId)
            i.putExtra("picName", picName)
            startActivity(i)
        }

        btn_add_handover.setOnClickListener {
            val i = Intent(requireContext(), HandoverAdd::class.java)
            i.putExtra("section", "handover")
            i.putExtra("casePo", casePo)
            i.putExtra("callId", callId)
            i.putExtra("accountId", accountId)
            i.putExtra("accountName", accountName)
            i.putExtra("addressId", addressId)
            i.putExtra("addressText", addressText)
            i.putExtra("engineerId", leadEngineerId)
            i.putExtra("engineerName", leadEngineerName)
            i.putExtra("picId", picId)
            i.putExtra("picName", picName)
            startActivity(i)
        }

        btn_add_complete.setOnClickListener {
            val i = Intent(requireContext(), HandoverAdd::class.java)
            i.putExtra("section", "installation")
            i.putExtra("casePo", casePo)
            i.putExtra("callId", callId)
            i.putExtra("accountId", accountId)
            i.putExtra("accountName", accountName)
            i.putExtra("addressId", addressId)
            i.putExtra("addressText", addressText)
            i.putExtra("engineerId", leadEngineerId)
            i.putExtra("engineerName", leadEngineerName)
            i.putExtra("picId", picId)
            i.putExtra("picName", picName)
            startActivity(i)
        }

        btn_sign_all.setOnClickListener {
            if (timeTrackingAdded && problemAdded && solutionAdded) {
                val i = Intent(requireContext(), FeedbackEdit::class.java)
                i.putExtra("callId", callId)
                i.putExtra("accountId", accountId)
                i.putExtra("picId", picId)
                i.putExtra("picName", picName)
                startActivity(i)
            } else if (!timeTrackingAdded) {
                Toast.makeText(requireContext(), "Insert time tracking first", Toast.LENGTH_LONG).show()
            } else if (!problemAdded) {
                Toast.makeText(
                    requireContext(),
                    "Insert problem for all report equipment",
                    Toast.LENGTH_LONG
                ).show()
            } else if (!solutionAdded) {
                Toast.makeText(
                    requireContext(),
                    "Insert solution for all report equipment",
                    Toast.LENGTH_LONG
                ).show()
            }
        }

        btn_send_pdf.setOnClickListener {
            val i = Intent(requireContext(), SendEmail::class.java)
            i.putExtra("callId", callId)
            i.putExtra("accountContact", accountContact)
            i.putExtra("accountFieldPic", accountFieldPic)
            startActivity(i)
        }

        btn_add_attachment.setOnClickListener {
            val i = Intent(requireContext(), AttachmentAdd::class.java)
            i.putExtra("accountId", accountId)
            i.putExtra("sourceId", callId)
            i.putExtra("sourceTable","Call")
            startActivity(i)
        }


    }

    private fun setupAdapter() {
        FunHelper.setUpAdapter(requireContext(), recycler_service_report).adapter = reportAdapter

        recycler_time_tracking?.layoutManager = LinearLayoutManager(requireContext(), RecyclerView.VERTICAL, false)
        recycler_time_tracking?.adapter = timeTrackingAdapter

        FunHelper.setUpAdapter(requireContext(), recycler_equipment).adapter = equipmentAdapter
        FunHelper.setUpAdapter(requireContext(), recycler_activity).adapter = activityAdapter
        FunHelper.setUpAdapter(requireContext(), recycler_attachment).adapter = attachmentAdapter

        recycler_training?.layoutManager = LinearLayoutManager(requireContext(), RecyclerView.VERTICAL, false)
        recycler_training?.adapter = trainingAdapter

        recycler_handover?.layoutManager = LinearLayoutManager(requireContext(), RecyclerView.VERTICAL, false)
        recycler_handover?.adapter = handoverAdapter

        recycler_completion?.layoutManager = LinearLayoutManager(requireContext(), RecyclerView.VERTICAL, false)
        recycler_completion?.adapter = completionAdapter
    }

    private fun trainingAdapterListener() {
        trainingAdapter.setEventHandler(object : CallTrainingAdapter.RecyclerClickListener{
            override fun isClicked(fileUrl: String) {
                showPdf(fileUrl, true)
            }
        })
    }

    private fun handoverAdapterListener() {
        handoverAdapter.setEventHandler(object : CallHandoverAdapter.RecyclerClickListener {
            override fun isClicked(fileUrl: String) {
                showPdf(fileUrl, true)
            }
        })
    }

    private fun completionAdapterListener() {
        completionAdapter.setEventHandler(object : CallCompletionAdapter.RecyclerClickListener {
            override fun isClicked(fileUrl: String) {
                showPdf(fileUrl, true)
            }
        })
    }

    private fun attachmentAdapterListener() {
        attachmentAdapter.setEventHandler(object : AttachmentAdapter.RecyclerClickListener {
            override fun isClicked(fileUrl: String) {
                val progressDialog = ProgressDialog(requireContext())
                progressDialog.setMessage("Open Attachment ...")
                progressDialog.setCancelable(false)
                progressDialog.show()

                if (fileUrl.isNotEmpty()) {
                    val thread = object : Thread() {
                        override fun run() {
                            var fileExtension = ""
                            try {
                                val connection = URL(fileUrl).openConnection()
                                fileExtension = connection.getHeaderField("Content-Type")
                                Log.e("aim", "extention : $fileExtension")
                            } catch (e: Exception) { }

                            activity?.runOnUiThread {
                                if (fileExtension.contains("pdf")) {
                                    progressDialog.dismiss()
                                    showPdf(fileUrl, false)
                                } else if (fileExtension.contains("jpg") ||
                                    fileExtension.contains("jpeg") ||
                                    fileExtension.contains("png")) {

                                    progressDialog.dismiss()
                                    val i = Intent(requireContext(), ImagePreview::class.java)
                                    i.putExtra("image", fileUrl)
                                    startActivity(i)
                                } else {
                                    progressDialog.dismiss()
                                    Toast.makeText(
                                        requireContext(),
                                        "Cant open attachment file",
                                        Toast.LENGTH_LONG
                                    ).show()
                                }
                            }
                        }
                    }
                    thread.start()
                } else {
                    Toast.makeText(
                        requireContext(),
                        "Attachment file not available",
                        Toast.LENGTH_LONG
                    ).show()
                }
            }

        })
    }

    @SuppressLint("SetTextI18n")
    private fun setupObserver(){
        val engineerId = SharedPreferenceData.getString(requireContext(), 8, "")

        callVM.getByCallId(callId).observe(viewLifecycleOwner, Observer { callData->
            if (callData.isNotEmpty()) {

                // ================================================================================= Call Status
                val callStatus = callData.first().callStatusEnumText
                if (callStatus == "Closed" || callStatus == "Customer Signed" || callStatus == "Canceled"
                    || callStatus == "Completed" || callStatus == "Suspended"
                ) {
                    btn_edit_contact.visibility = View.GONE
                    btn_add_report.visibility = View.GONE
                    btn_sign_all.visibility = View.GONE
                    btn_add_equipment.visibility = View.GONE
                    btn_add_attachment.visibility = View.GONE
                } else {
                    btn_edit_contact.visibility = View.VISIBLE
                    btn_add_report.visibility = View.VISIBLE
                    btn_sign_all.visibility = View.VISIBLE
                    btn_add_equipment.visibility = View.VISIBLE
                    btn_add_attachment.visibility = View.VISIBLE
                }

                // ================================================================================= My Call
                val itsMyCall = ArrayList<String>()
                callData.first().engineer?.forEach {
                    if (it?.id ?: "" == engineerId) {
                        itsMyCall.add(it?.id ?: "")
                    }
                }
                if (itsMyCall.isEmpty()) {
                    btn_edit_contact.visibility = View.GONE
                    btn_add_report.visibility = View.GONE
                    btn_add_equipment.visibility = View.GONE
                    btn_add_training.visibility = View.GONE
                    btn_add_handover.visibility = View.GONE
                    btn_add_complete.visibility = View.GONE
                    btn_sign_all.visibility = View.GONE
                    btn_send_pdf.visibility = View.GONE
                    btn_add_attachment.visibility = View.GONE
                }

                caseId = callData.first().caseId
                callNumber = callData.first().serviceCallNumber
                picId = callData.first().accountContactId
                picName = callData.first().accountContactName
                accountContact = callData.first().accountContactEmail
                accountFieldPic = callData.first().fieldAccountContactEmail

                // Account contact
                getContact(
                    callData.first().caseAccountContactId,
                    account_contact_name,
                    account_contact_phone,
                    account_contact_type
                )

                // PIC contact
                getContact(
                    callData.first().caseAccountFieldPicContacId,
                    account_pic_contact_name,
                    account_pic_contact_phone,
                    account_pic_contact_type
                )

                caseVM.getByCaseId(caseId).observe(viewLifecycleOwner, Observer { caseData->
                    if (caseData.isNotEmpty()) {

                        // ========================================================================= Additional Form
                        if (caseData.first().caseTypeEnumName == "Installation") {
                            additional_form_lay.visibility = View.VISIBLE
                            warning_additional.visibility = View.GONE
                        } else {
                            additional_form_lay.visibility = View.GONE
                            warning_additional.visibility = View.VISIBLE
                        }

                        accountId = caseData.first().accountId
                        accountName = caseData.first().accountName
                        caseNumber = caseData.first().caseNumber
                        casePo = caseData.first().customerPo
                        addressId = caseData.first().addressId
                        addressText = caseData.first().addressText

                        btn_service_case.text = "Service Case : ${caseData.first().caseNumber}"


                        company_value.text = callData.first().accountName.ifEmpty { "-" }
                        address_value.text = caseData.first().addressText.ifEmpty { "-" }

                        attachmentObserver()

                        // case information
                        information_case_type_value.text = caseData.first().caseTypeEnumName.ifEmpty { "-" }
                        information_status_value.text = callData.first().callStatusEnumText.ifEmpty { "-" }
                        information_event_start_value.text = FunHelper.uiDateFormat(callData.first().startDate)
                        information_event_end_value.text = FunHelper.uiDateFormat(callData.first().endDate)
                        information_call_subject_value.text = callData.first().serviceCallSubject.ifEmpty { "-" }
                        information_call_desc_value.text = callData.first().serviceCallDescription.ifEmpty { "-" }
                        information_lead_value.text = callData.first().leadEngineerName.ifEmpty { "-" }

                        // case layout
                        case_title.text = "Case ${caseData.first().caseNumber}"
                        case_status.text = caseData.first().caseStatusEnumName.ifEmpty { "-" }
                        sub_title.text = caseData.first().caseTypeEnumName.ifEmpty { "-" }
                        case_subject_value.text = caseData.first().subject.ifEmpty { "-" }
                        case_desc_value.text = caseData.first().description.ifEmpty { "-" }
                        agreement_type_value.text = caseData.first().agreementType.ifEmpty { "-" }
                        account_sales_pic.text = caseData.first().accountSalesName.ifEmpty { "-" }
                        case_sales_pic_value.text = caseData.first().caseSalesName.ifEmpty { "-" }
                        case_owner_value.text = caseData.first().caseOwnerName.ifEmpty { "-" }
                        technical_pic_value.text = caseData.first().leadEngineerName.ifEmpty { "-" }

                        // ========================================================================= Installation
                        if (caseData.first().caseTypeEnumName == "Installation" || caseData.first().caseTypeEnumName == "Demo") {
                            installation_lay.visibility = View.VISIBLE
                            warning_installation.visibility = View.GONE

                            installation_delivery_value.text = FunHelper.uiDateFormat(caseData.first().proposedDeliveryDate)
                            installation_install_value.text = FunHelper.uiDateFormat(caseData.first().proposedInstallationDate)
                            installation_po_value.text = caseData.first().customerPo.ifEmpty { "-" }
                            installation_supply_direct_value.text = if (caseData.first().directDelivery == "1") "Yes" else "No"
                            installation_material_value.text = if (caseData.first().materialNeeded == "1") "Yes" else "No"
                            isntallation_free_value.text = caseData.first().noOfFreeService.ifEmpty { "-" }
                            installation_special_requirement_value.text = if (caseData.first().specialRequirement == "1") "Yes" else "No"
                            installation_demo_value.text = caseData.first().loanDemoPeriod.ifEmpty { "-" }
                            installation_air_supply_value.text = caseData.first().airUtilitySuppliedBy.ifEmpty { "-" }
                            installation_warranty_value.text = caseData.first().warrantyPeriodSupplier.ifEmpty { "-" }
                            installation_customer_warranty_value.text = caseData.first().warrantyPeriodCustomer.ifEmpty { "-" }
                            installation_pro_value.text = if (caseData.first().professionalInspectionRequired == "1") "Yes" else "No"
                            installation_inspec_value.text = caseData.first().inspectionRemark.ifEmpty { "-" }
                        } else {
                            installation_lay.visibility = View.GONE
                            warning_installation.visibility = View.VISIBLE
                        }
                    }
                })

                // header
                leadEngineerId = callData.first().leadEngineerId
                leadEngineerName = callData.first().leadEngineerName

                reportObserver()
                checkBeforeSignAll()

            }
        })

        callTrainingVM.getByCallId(callId).observe(viewLifecycleOwner, {
            if (it.isNotEmpty()) {
                warning_training.visibility = View.GONE
                recycler_training.visibility = View.VISIBLE

                trainingAdapter.setList(it)
            } else {
                warning_training.visibility = View.VISIBLE
                recycler_training.visibility = View.GONE
            }
        })

        callHandoverVM.getByCallId(callId).observe(viewLifecycleOwner, {
            if (it.isNotEmpty()) {
                warning_handover.visibility = View.GONE
                recycler_handover.visibility = View.VISIBLE

                handoverAdapter.setList(it)
            } else {
                warning_handover.visibility = View.VISIBLE
                recycler_handover.visibility = View.GONE
            }
        })

        callCompletionVM.getByCallId(callId).observe(viewLifecycleOwner, {
            if (it.isNotEmpty()) {
                warning_completion.visibility = View.GONE
                recycler_completion.visibility = View.VISIBLE

                completionAdapter.setList(it)
            } else {
                warning_completion.visibility = View.VISIBLE
                recycler_completion.visibility = View.GONE
            }
        })

        callEquipmentVM.getEquipmentList(callId, true).observe(viewLifecycleOwner, { equipmentData->
            tx_equipment_count.text = ""
            equipmentAdapter.reset()

            if (equipmentData.isNotEmpty()) {
                tx_equipment_count.text = "(${equipmentData.size})"
                recycler_equipment.visibility = View.VISIBLE
                warning_equipment.visibility = View.GONE

                equipmentAdapter.setList(equipmentData, false)
            } else {
                recycler_equipment.visibility = View.GONE
                warning_equipment.visibility = View.VISIBLE
            }
        })

        timeTrackingVM.getByCallId(callId).observe(viewLifecycleOwner, Observer { timeTracking->
            tx_time_tracking_count.text = ""
            timeTrackingAdapter.clear()

            if (timeTracking.isNotEmpty()) {
                val timeTrackingCount = "(${timeTracking.size})"
                tx_time_tracking_count.text = timeTrackingCount
                warning_time_tracking.visibility = View.GONE
                time_tracking_lay.visibility = View.VISIBLE

                timeTrackingAdded = true
                timeTrackingAdapter.setList(timeTracking, engineerId, false)
            } else {
                timeTrackingAdded = false
                warning_time_tracking.visibility = View.VISIBLE
                time_tracking_lay.visibility = View.GONE
            }
        })

        activityVM.getByCall(callId).observe(viewLifecycleOwner, {
            activity_count.text = ""

            if (it.isNotEmpty()) {
                activity_count.text = "(${it.size})"
                recycler_activity.visibility = View.VISIBLE
                warning_activity.visibility = View.GONE

                activityAdapter.setList(it)
            } else {
                recycler_activity.visibility = View.GONE
                warning_activity.visibility = View.VISIBLE
            }
        })
    }

    @SuppressLint("SetTextI18n")
    private fun reportObserver() {
        reportVM.getByCallId(callId).observe(viewLifecycleOwner, {
            tx_report_count.text = ""
            reportAdapter.clear()

            if (it.isNotEmpty()) {
                tx_report_count.text =  "(${it.size})"
                recycler_service_report.visibility = View.VISIBLE
                warning_report.visibility = View.GONE

                reportAdapter.setList(it, false)
            } else {
                btn_sign_all.visibility = View.GONE
                recycler_service_report.visibility = View.GONE
                warning_report.visibility = View.VISIBLE
            }
        })
    }

    private fun attachmentObserver() {
        Log.e("aim","accountId: $accountId, sourceId: $callId, table: Call")
        attachmentVM.accountAttachmentSection(accountId, callId, "Call")
            .observe(viewLifecycleOwner, { attachmentData ->
                tx_attachment_count.text = ""

                if (attachmentData.isNotEmpty()) {
                    tx_attachment_count.text = "(${attachmentData.size})"
                    recycler_attachment.visibility = View.VISIBLE
                    warning_attachment.visibility = View.GONE

                    attachmentAdapter.setList(attachmentData)
                } else {
                    recycler_attachment.visibility = View.GONE
                    warning_attachment.visibility = View.VISIBLE
                }
            })
    }

    private fun getContact(contactId: String, name: TextView, phone: TextView, type: TextView) {
        accountContactVM.getById(contactId).observe(viewLifecycleOwner, {
            if (it.isNotEmpty()) {
                name.text = it.first().contact_person_name.ifEmpty { "-" }
                phone.text = it.first().contact_person_phone.ifEmpty { "-" }
                type.text = it.first().contact_type.ifEmpty { "-" }
            }
        })
    }

    private fun checkBeforeSignAll() {
        var problemCount = 0
        var solutionCount = 0
        reportEquipmentVM.getEquipmentAllByCallId(callId, true).observe(viewLifecycleOwner, { equipment->
            if (equipment.isNotEmpty()) {
                val equipmentSize = equipment.size

                equipment.forEach {
                    if (!it.problemSymptom.isNullOrEmpty()) {
                        problemCount++
                    }

                    if (!it.solution.isNullOrEmpty()) {
                        solutionCount++
                    }
                }

                problemAdded = equipmentSize == problemCount
                solutionAdded = equipmentSize == solutionCount
            }
        })
    }

    private fun showPdf(fileUrl: String, withToken: Boolean) = runWithPermissions(FunHelper.storageWritePermission) {
        if (withToken) {
            DownloadPdf(requireContext(), fileUrl).downloadWithToken()
        } else {
            DownloadPdf(requireContext(), fileUrl).downloadNow(false)
        }
    }

}
