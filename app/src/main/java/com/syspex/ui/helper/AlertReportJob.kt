package com.syspex.ui.helper

import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.media.RingtoneManager
import android.os.Build
import android.util.Log
import android.widget.Toast
import androidx.core.app.JobIntentService
import androidx.core.app.NotificationCompat
import com.syspex.R
import com.syspex.data.local.database.AppDatabase
import java.text.SimpleDateFormat
import java.util.*


class AlertReportJob : JobIntentService() {

    /* Give the Job a Unique Id */
    private val jobId: Int = 1000
    fun enqueueWork(context: Context?, work: Intent) {
        if (context != null) {
            enqueueWork(context, AlertReportJob::class.java, jobId, work)
        }
    }

    override fun onHandleWork(intent: Intent) {
//        checkAvailableReport()
//        reportNotification(1244, "test background")


        Log.e("aim", "Sending notification")
        val nm = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        val sender = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT)
        nm.notify(4350, NotificationUtils.getInstance(this).getNotification(sender,"Reminder", "Test Background"))

    }

    private fun checkAvailableReport() {

//        Toast.makeText(this, "masuk job intent", Toast.LENGTH_LONG).show()

        val dao = AppDatabase.getInstance(this)!!.dao()
        val sdf = SimpleDateFormat("yyyy-MM-dd", Locale.getDefault())
//        val startDateTime = "${sdf.format(Date())} 00:01"
//        val endDateTime = "${sdf.format(Date())} 23:59"

        val startDateTime = "2020-10-11 00:01"
        val endDateTime = "2020-10-11 23:59"

        Log.e("aim", "report start : $startDateTime, end : $endDateTime")

        val thread = object : Thread() {
            override fun run() {
                dao.reportTodayAlert(startDateTime, endDateTime).forEachIndexed { index, it ->
                    Log.e("aim", "report today : $it")
                    if (it.reportLocalId == null) {
                        val chanelId = index+1
                        val content = "You have open service call ${it.serviceCallNumber ?: "-"} " +
                                "overdue with no service report"
                        reportNotification(chanelId, content)
                    }
                }
            }
        }
        thread.start()
    }

    private fun reportNotification(notificationId: Int, content: String) {
        val intent = Intent(this, AlertReportJob::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
        val pendingIntent = PendingIntent.getActivity(
            this, 0, intent,
            PendingIntent.FLAG_ONE_SHOT
        )

        val chanelId = "Default"
        val soundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION)
        val builder= NotificationCompat.Builder(this, chanelId)
            .setPriority(NotificationCompat.PRIORITY_HIGH)
            .setCategory(NotificationCompat.CATEGORY_ALARM)
            .setStyle(NotificationCompat.BigTextStyle().bigText(content))
            .setSmallIcon(R.mipmap.ic_launcher_round)
            .setContentTitle("Reminder")
            .setContentText(content)
            .setAutoCancel(true)
            .setSound(soundUri)
            .setContentIntent(pendingIntent)

        val manager = this.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val channel = NotificationChannel(
                chanelId,
                "Default channel",
                NotificationManager.IMPORTANCE_HIGH
            )
            channel.lockscreenVisibility = NotificationCompat.VISIBILITY_PUBLIC
            manager.createNotificationChannel(channel)
        }

        manager.notify(notificationId, builder.build())
    }
}