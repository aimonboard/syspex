package com.syspex.ui.helper

import androidx.recyclerview.widget.RecyclerView
import com.syspex.data.local.database.service_report_equipment_checklist.ServiceReportEquipmentChecklistEntity
import com.syspex.data.model.ConverterImageModel

interface ChecklistInterface {
    fun onQuestionAddImage(questionId: String, image: ArrayList<ConverterImageModel>)
    fun onQuestionDeleteImage(questionId: String, image: ArrayList<ConverterImageModel>)
    fun onQuestionStatusChange(questionId: String, enumId: String, enumName: String, remark: String)
}