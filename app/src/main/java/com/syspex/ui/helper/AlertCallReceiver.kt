package com.syspex.ui.helper

import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.media.RingtoneManager
import android.os.Build
import android.widget.Toast
import androidx.core.app.NotificationCompat
import com.syspex.R
import java.util.*

class AlertCallReceiver : BroadcastReceiver() {

    override fun onReceive(context: Context?, intent: Intent?) {
        Toast.makeText(context, "alert call receiver", Toast.LENGTH_LONG).show()

        if (context != null) {
            val callId = intent?.getStringExtra("callId") ?: "0000"
            val callNumber = intent?.getStringExtra("callNumber") ?: "0000"
            val start = intent?.getStringExtra("start") ?: "00:00"
            val content = "Today service call : $callNumber - 2 hours remaining before $start"
            callNotification(context, content)
        }
    }

    // ============================================================================================= Report Alert
    private fun callNotification(context: Context, content: String) {
        val intent = Intent(context, AlertCallReceiver::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
        val pendingIntent = PendingIntent.getActivity(
            context, 0, intent,
            PendingIntent.FLAG_ONE_SHOT
        )

        val chanelId = "Default"
        val soundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION)
        val builder= NotificationCompat.Builder(context, chanelId)
            .setPriority(NotificationCompat.PRIORITY_HIGH)
            .setStyle(NotificationCompat.BigTextStyle().bigText(content))
            .setSmallIcon(R.mipmap.ic_launcher_round)
            .setContentTitle("Reminder")
            .setContentText(content)
            .setAutoCancel(true)
            .setSound(soundUri)
            .setContentIntent(pendingIntent)

        val manager = context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val channel = NotificationChannel(
                chanelId,
                "Default channel",
                NotificationManager.IMPORTANCE_DEFAULT
            )
            manager.createNotificationChannel(channel)
        }

        val r = Random()
        val randomNotificationId = String.format("%04d", Integer.valueOf(r.nextInt(1001))).toInt()
        manager.notify(randomNotificationId, builder.build())
    }
}