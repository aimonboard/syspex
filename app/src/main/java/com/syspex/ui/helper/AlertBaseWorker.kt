package com.syspex.ui.helper

import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.os.Build
import android.util.Log
import androidx.work.*
import com.syspex.Menu2
import com.syspex.data.local.database.AppDatabase
import java.text.SimpleDateFormat
import java.util.*
import java.util.concurrent.TimeUnit

class AlertBaseWorker(context: Context, param: WorkerParameters) : Worker(context, param) {
    override fun doWork(): Result {
        val type = inputData.getString("type") ?: ""

        Log.e("aim", "do work")

        when (type) {
            "call" -> {
                callNotification()
            }
            "report" -> {
                reportNotification()
    //            reCreateAlertReport()
            }
            else -> {
                Log.e("aim", "type not found")
            }
        }

        return Result.success()
    }

    private fun callNotification() {
        val workId = inputData.getInt("workId",0)
        val callId = inputData.getString("callId") ?: ""
        val callNumber = inputData.getString("callNumber") ?: ""
        val start = inputData.getString("start") ?: ""
        val text = "Today service call : $callNumber - 2 hours remaining before $start"

        val intent = Intent(applicationContext, Menu2::class.java)
        intent.putExtra("callId", callId)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
        val pendingIntent = PendingIntent.getActivity(
            applicationContext, 0, intent,
            PendingIntent.FLAG_ONE_SHOT
        )

        val nm = applicationContext.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        val sender = PendingIntent.getActivity(applicationContext, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT)
        nm.notify(workId, NotificationUtils.getInstance(applicationContext).getNotification(sender,"Reminder", text))
    }

    private fun reportNotification() {
        val dao = AppDatabase.getInstance(applicationContext)!!.dao()
        val sdf = SimpleDateFormat("yyyy-MM-dd", Locale.getDefault())
//        val startDateTime = "${sdf.format(Date())} 00:01"
//        val endDateTime = "${sdf.format(Date())} 23:59"

        val startDateTime = "2020-10-11 00:01"
        val endDateTime = "2020-10-11 23:59"
        Log.e("aim", "report start : $startDateTime, end : $endDateTime")

        dao.reportTodayAlert(startDateTime, endDateTime).forEachIndexed { index, it ->
            Log.e("aim", "report today : $it")
            if (it.reportLocalId == null) {
                val notificationId = index+1
                val text = "You have open service call ${it.serviceCallNumber ?: "-"} " +
                        "overdue with no service report"

                val intent = Intent(applicationContext, Menu2::class.java)
//                intent.putExtra("reportId", it.reportLocalId ?: "")
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                val sender = PendingIntent.getActivity(applicationContext, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT)

                val nm = applicationContext.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
                nm.notify(notificationId, NotificationUtils.getInstance(applicationContext).getNotification(sender,"Reminder", text))
            }
        }
    }

    private fun reCreateAlertReport() {
        val c = Calendar.getInstance()
        c[Calendar.HOUR_OF_DAY] = 21
        c[Calendar.MINUTE] = 50
        c[Calendar.SECOND] = 0
//        if (c.before(Calendar.getInstance())) { c.add(Calendar.DATE, 1) }

        val data = Data.Builder()
            .putString("type", "report")
            .putInt("workId", FunHelper.alertReportRequest)
            .build()

        val constraints = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            Constraints.Builder()
                .setRequiresStorageNotLow(false)
                .setRequiresBatteryNotLow(false)
                .setRequiresCharging(false)
                .setRequiresDeviceIdle(false)
                .build()
        } else {
            Constraints.Builder()
                .setRequiresStorageNotLow(false)
                .setRequiresBatteryNotLow(false)
                .setRequiresCharging(false)
                .build()
        }

        val workId = data.getInt("workId",0)
        val notificationWork =
            OneTimeWorkRequest.Builder(AlertBaseWorker::class.java)
                .setInitialDelay(c.timeInMillis, TimeUnit.MILLISECONDS)
                .setConstraints(constraints)
                .setInputData(data)
                .build()

        val instanceWorkManager = WorkManager.getInstance(applicationContext)
        instanceWorkManager.beginUniqueWork(workId.toString(), ExistingWorkPolicy.REPLACE, notificationWork)
            .enqueue()
    }
}