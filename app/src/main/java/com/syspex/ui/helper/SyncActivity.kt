package com.syspex.ui.helper

import android.content.Context
import android.util.Log
import android.widget.Toast
import com.syspex.data.local.SharedPreferenceData
import com.syspex.data.local.database.AppDatabase
import com.syspex.data.local.database.activity.ActivityEntity
import com.syspex.data.remote.GetActivityRequest
import com.syspex.data.retrofit.ApiClient
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class SyncActivity(val context: Context) {

    private val dao = AppDatabase.getInstance(context)!!.dao()
    private var listener: SendDataInterface? = null
    private val token = "Bearer "+ SharedPreferenceData.getString(context,2, "")
    private val userId = SharedPreferenceData.getString(context,8, "")

    fun startSyncActivity(mListener: SendDataInterface?) {
        this.listener = mListener
        getActivity(1)
    }

    private fun getActivity(loadPage: Int) {
        ApiClient.instance.getActivity(token, loadPage).enqueue(object: Callback<GetActivityRequest> {
            override fun onFailure(call: Call<GetActivityRequest>, t: Throwable) {
                Log.e("aim", "export Err : $t")
                Toast.makeText(context, "Connection failed", Toast.LENGTH_LONG).show()
                listener?.sendDataCallback(false)
            }
            override fun onResponse(call: Call<GetActivityRequest>, response: Response<GetActivityRequest>) {
                if (response.isSuccessful) {
                    val page= response.body()?.data?.page ?: 1
                    val lastPage= response.body()?.data?.lastPage ?: 1
                    iterationHandling(page, lastPage)

                    insertActivity(response)
                }
                else {
                    FunHelper.firebaseApiLog(
                        userId ?: "",
                        response.raw().request().url().toString() ?: "",
                        response.raw().request().headers().toString() ?: "",
                        "",
                        response.raw().toString() ?: ""
                    )
                    Toast.makeText(context, response.raw().toString(), Toast.LENGTH_LONG).show()
                }
            }
        })
    }

    private fun insertActivity (response: Response<GetActivityRequest>) {
        val thread = object : Thread() {
            override fun run() {
                response.body()?.data?.activity?.forEach {
                    dao.activityInsert(ActivityEntity(
                        it?.activityId ?: "",
                        it?.call?.caseId ?: "",
                        it?.call?.accountId ?: "",
                        it?.call?.serviceCallId ?: "",
                        it?.regionId ?: "",
                        it?.userId ?: "",
                        it?.activityType ?: "",
                        it?.activityStartTime ?: "",
                        it?.activityEndTime ?: "",
                        it?.activitySubject ?: "",
                        it?.activityDescription ?: "",
                        it?.activitySourceTable ?: "",
                        it?.activitySourceId ?: "",
                        it?.createdDate ?: "",
                        it?.createdByDetail?.userFullname ?: "",
                        it?.lastModifiedDate ?: "",
                        it?.lastModifiedDetail?.userFullname ?: "",
                        it?.activityTypeText?.activityTypeId ?: "",
                        it?.activityTypeText?.activityTypeCode ?: "",
                        it?.activityTypeText?.activityTypeName ?: "",
                        it?.activityTypeText?.activityTypeColor ?: "",
                        false
                    ))
                }
            }
        }
        thread.start()
    }

    private fun iterationHandling(page: Int, lastPage: Int) {
        if (page < lastPage) {
            getActivity(page+1)
        } else if (page >= lastPage) {
            listener?.sendDataCallback(true)
        }
    }

}