package com.syspex.ui.helper

import android.annotation.SuppressLint
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.app.ProgressDialog
import android.content.Context
import android.content.Intent
import android.media.RingtoneManager
import android.os.Build
import android.util.Log
import androidx.core.app.NotificationCompat
import com.google.firebase.FirebaseApp
import com.google.firebase.iid.FirebaseInstanceId
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import com.syspex.R
import com.syspex.data.local.SharedPreferenceData
import com.syspex.ui.CaseDetail
import com.syspex.ui.NewsDetail
import com.syspex.ui.ServiceCallDetail
import com.syspex.ui.ServiceReportDetail


class FirebaseService : FirebaseMessagingService() {
    @SuppressLint("LongLogTag")
    override fun onMessageReceived(remoteMessage: RemoteMessage) {

        Log.e("aim", "fcm notif: ${remoteMessage.notification?.body}, fcm data: ${remoteMessage.data}")

        if (remoteMessage.notification != null) {
            showNotificationForeground(
                remoteMessage.notification?.title,
                remoteMessage.notification?.body,
                remoteMessage.data
            )

            val caseId = remoteMessage.data["case_id"] ?: ""
            SharedPreferenceData.setString(this, 16, caseId)

//            SyncDataFcm(this).getByCaseId(caseId,"case", null)
        }
    }

    override fun onNewToken(p0: String) {
        super.onNewToken(p0)
        Log.e("aim", "fcm onNewToken : $p0")
    }

    private fun showNotificationForeground(title: String?, body: String?, data: Map<String, String>) {
        val intent = when(data["goto"]) {
            "Case" -> Intent(this, CaseDetail::class.java)
            "Call" -> Intent(this, ServiceCallDetail::class.java)
            "News" -> Intent(this, NewsDetail::class.java)
            else -> Intent(this, FirebaseService::class.java)
        }

        intent.putExtra("case_id", data["case_id"] ?: "")
        intent.putExtra("call_id", data["call_id"] ?: "")
        intent.putExtra("news_id", data["id"] ?: "")
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)

        val pendingIntent = PendingIntent.getActivity(
            this, 0, intent,
            PendingIntent.FLAG_ONE_SHOT
        )

        val channelId = "Default"
        val soundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION)
        val builder= NotificationCompat.Builder(this, channelId)
            .setStyle(NotificationCompat.BigTextStyle().bigText(body))
            .setSmallIcon(R.drawable.ic_notification)
            .setContentTitle(title)
            .setContentText(body)
            .setAutoCancel(true)
            .setSound(soundUri)
            .setContentIntent(pendingIntent)

        val manager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val channel = NotificationChannel(
                channelId,
                "Default channel",
                NotificationManager.IMPORTANCE_DEFAULT
            )
            manager.createNotificationChannel(channel)
        }
        manager.notify(0, builder.build())
    }

}