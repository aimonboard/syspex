package com.syspex.ui.helper

import android.app.*
import android.content.Intent
import android.content.IntentFilter
import android.os.Build
import android.os.IBinder
import com.syspex.Menu2


class AlertReportService : Service() {

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
//        fourgroundService()
        registerReceiver(AlertReportReceiver(), IntentFilter("com.syspex.alert.report.action"))
        return START_STICKY
    }

    override fun onBind(intent: Intent?): IBinder? {
        return null
    }

    override fun onLowMemory() {  // rem this if you want it always----
        stopSelf()
        super.onLowMemory()
    }

    private fun fourgroundService() {
        val pendingIntent = PendingIntent.getActivity(
            this, 1, Intent(
                this,
                Menu2::class.java
            ), 0
        )

        /*Handle Android O Notifs as they need channel when targeting 28th SDK*/

        /*Handle Android O Notifs as they need channel when targeting 28th SDK*/
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val notificationManager = getSystemService(NOTIFICATION_SERVICE) as NotificationManager
            val notificationChannel = NotificationChannel(
                "download_check_channel_id",
                "Channel name",
                NotificationManager.IMPORTANCE_LOW
            )
            notificationManager.createNotificationChannel(notificationChannel)
            val builder = Notification.Builder(this.baseContext, notificationChannel.id)
                .setContentTitle("Hi! I'm service")
                .setContentIntent(pendingIntent)
                .setOngoing(true)
            val notification = builder.build()
            startForeground("StackOverflow".length, notification)
        }
    }
}