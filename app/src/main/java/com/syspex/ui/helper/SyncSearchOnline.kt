package com.syspex.ui.helper

import android.content.Context
import android.util.Log
import android.widget.Toast
import com.syspex.data.local.SharedPreferenceData
import com.syspex.data.local.database.AppDatabase
import com.syspex.data.local.database.account.AccountEntity
import com.syspex.data.local.database.account_address.AccountAddressEntity
import com.syspex.data.local.database.account_attachment.AccountAttachmentEntity
import com.syspex.data.local.database.account_contact.AccountContactEntity
import com.syspex.data.local.database.account_equipment.AccountEquipmentEntity
import com.syspex.data.local.database.agreement.AgreementEntity
import com.syspex.data.local.database.product_checklist_question.ChecklistQuestionEntity
import com.syspex.data.local.database.product_checklist_template.ChecklistTemplateEntity
import com.syspex.data.local.database.product_document.ProductDocumentEntity
import com.syspex.data.local.database.product_group.ProductEntity
import com.syspex.data.local.database.product_group_item.ProductGroupItemEntity
import com.syspex.data.local.database.product_sparepart.SparepartEntity
import com.syspex.data.remote.AccountRequest
import com.syspex.data.remote.GetDataRequest
import com.syspex.data.retrofit.ApiClient
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.text.SimpleDateFormat
import java.util.*

class SyncSearchOnline(val context: Context) {

    private var listener: SyncSearchInterface? = null
    private val dao = AppDatabase.getInstance(context)!!.dao()
    private val token = "Bearer "+ SharedPreferenceData.getString(context,2, "")
    private val userId = SharedPreferenceData.getString(context,8, "")

    private var accountId = ""
    private var equipmentId = ""
    private var onlySyncAccount = false
    private var accountDone = false
    private var equipmentDone = false

    fun getAccountOnline(_accountId: String, mListener: SyncSearchInterface) {
        listener = mListener
        accountId = _accountId
        onlySyncAccount = true

        getAccountDetail()
    }

    fun getEquipmentOnline(_accountId: String, _equipmentId: String, mListener: SyncSearchInterface) {
        listener = mListener
        accountId = _accountId
        equipmentId = _equipmentId
        onlySyncAccount = false

        getAccountDetail()
        getDetailEquipment()
    }

    private fun getAccountDetail() {
        val params = HashMap<String, Any>()
        params["account_id"] = accountId
        Log.e("aim","get detail account : $params")

        ApiClient.instance.accountRequest(token, params).enqueue(object : Callback<AccountRequest> {
            override fun onFailure(call: Call<AccountRequest>, t: Throwable) {
                accountDone = false
                handlingCallBack(onlySyncAccount)

                Log.e("aim", "sync equipment : $t")
                Toast.makeText(context, "Err : ${t.message}", Toast.LENGTH_LONG).show()
            }

            override fun onResponse(call: Call<AccountRequest>,response: Response<AccountRequest>) {
                if (!response.isSuccessful) {
                    if (response.code() == 401) {
                        FunHelper.clearAllData(context)
                        Toast.makeText(context, "Session end", Toast.LENGTH_LONG).show()
                    } else {
                        accountDone = false
                        handlingCallBack(onlySyncAccount)

                        FunHelper.firebaseApiLog(
                            userId ?: "",
                            response.raw().request().url().toString() ?: "",
                            response.raw().request().headers().toString() ?: "",
                            params.toString() ?: "",
                            response.raw().toString() ?: ""
                        )
                        Toast.makeText(context, response.raw().toString(), Toast.LENGTH_LONG).show()
                    }
                } else {
                    insertAccount(response)
                    accountDone = true
                    handlingCallBack(onlySyncAccount)
                }
            }
        })
    }

    private fun getDetailEquipment() {
        val params = HashMap<String, Any>()
        params["equipment_id"] = equipmentId
        params["page"] = "1"
        params["limit"] = "5"
        params["last_record_date"] = ""
        Log.e("aim","sync equipment : $params")

        ApiClient.instance.syncEquipment(token, params).enqueue(object : Callback<GetDataRequest> {
            override fun onFailure(call: Call<GetDataRequest>, t: Throwable) {
                equipmentDone = false
                handlingCallBack(onlySyncAccount)

                Log.e("aim", "sync equipment : $t")
                Toast.makeText(context, "Err : ${t.message}", Toast.LENGTH_LONG).show()
            }

            override fun onResponse(call: Call<GetDataRequest>,response: Response<GetDataRequest>) {
                if (!response.isSuccessful) {
                    if (response.code() == 401) {
                        FunHelper.clearAllData(context)
                        Toast.makeText(context, "Session end", Toast.LENGTH_LONG).show()
                    } else {
                        equipmentDone = false
                        handlingCallBack(onlySyncAccount)

                        FunHelper.firebaseApiLog(
                            userId ?: "",
                            response.raw().request().url().toString() ?: "",
                            response.raw().request().headers().toString() ?: "",
                            params.toString() ?: "",
                            response.raw().toString() ?: ""
                        )
                        Toast.makeText(context, response.raw().toString(), Toast.LENGTH_LONG).show()
                    }
                } else {
                    insertEquipment(response)
                    equipmentDone = true
                    handlingCallBack(onlySyncAccount)
                }
            }
        })
    }

    private fun insertAccount(response: Response<AccountRequest>) {
        val thread = object : Thread() {
            override fun run() {
                val accountID = response.body()?.data?.accountId
                val accountName = response.body()?.data?.accountName ?: ""
                val regionCode = response.body()?.data?.region?.regionCode ?: ""

                // ================================================================================= Insert Account
                dao.accountInsert(
                    AccountEntity(
                        response.body()?.data?.accountId ?: "",
                        "",
                        "",
                        "",
                        response.body()?.data?.accountCode ?: "",
                        response.body()?.data?.accountName ?: "",
                        response.body()?.data?.active ?: "",
                        response.body()?.data?.createdDate ?: "",
                        response.body()?.data?.createdBy ?: "",
                        response.body()?.data?.lastModifiedDate ?: "",
                        "",
                        response.body()?.data?.internalMemo ?: "",
                        "",
                        "",
                        response.body()?.data?.sales?.id ?: "",
                        response.body()?.data?.sales?.userFullname ?: "",
                        response.body()?.data?.sales?.email ?: "",
                        response.body()?.data?.sales?.phone ?: "",
                        response.body()?.data?.region?.regionCode ?: "",
                        response.body()?.data?.region?.regionName ?: "",
                        response.body()?.data?.contact ?: emptyList(),
                        response.body()?.data?.address ?: emptyList()
                    )
                )

                // ============================================================================= Insert Account Contact
                response.body()?.data?.contact?.forEach { contact->
                    dao.accountContactInsert(
                        AccountContactEntity(
                            contact?.accountContactId ?: "",
                            contact?.regionId ?: "",
                            contact?.customerId ?: "",
                            accountName,
                            contact?.contactPersonName ?: "",
                            contact?.contactPersonPhone ?: "",
                            contact?.contactPersonEmail ?: "",
                            contact?.contactPersonAddress ?: "",
                            contact?.contactPersonPosition ?: "",
                            contact?.contactTypeText?.enumName ?: "",
                            contact?.priority ?: "",
                            contact?.createdDate ?: "",
                            contact?.createdBy ?: "",
                            contact?.lastModifiedDate ?: "",
                            contact?.lastModifiedBy ?: ""
                        )
                    )
                }

                // ============================================================================= Insert Account Address
                response.body()?.data?.address?.forEach { address->
                    dao.accountAddressInsert(
                        AccountAddressEntity(
                            address?.accountAddressId ?: "",
                            address?.regionId ?: "",
                            address?.accountId ?: "",
                            address?.addressTitle ?: "",
                            address?.addressLine1 ?: "",
                            address?.addressLine2 ?: "",
                            address?.addressLine3 ?: "",
                            address?.addressCountry ?: "",
                            address?.addressCity ?: "",
                            address?.addressZipCode ?: "",
                            address?.priority ?: "",
                            address?.createdDate ?: "",
                            address?.createdBy ?: "",
                            address?.lastModifiedDate ?: "",
                            address?.lastModifiedBy ?: "",
                            address?.fullAddress ?: ""
                        )
                    )
                }

                // ============================================================================= Insert Account Attachment
                response.body()?.data?.attachment?.forEach { attachment->
                    dao.accountAttachmentInsert(
                        AccountAttachmentEntity(
                            attachment?.accountAttachmentId ?: "",
                            attachment?.regionId ?: "",
                            attachment?.accountId ?: "",
                            attachment?.attachmentTitle ?: "",
                            attachment?.attachmentBody ?: "",
                            attachment?.fileUrl ?: "",
                            attachment?.attachmentOwnerId ?: "",
                            attachment?.createdDate ?: "",
                            attachment?.createdByUser?.id ?: "",
                            attachment?.lastModifiedDate ?: "",
                            attachment?.modifiedByUser?.userFullname ?: "",
                            attachment?.attachmentSourceTable ?: "",
                            attachment?.attachmentSourceId ?: "",
                            "",
                            "",
                            attachment?.createdByUser?.id ?: "",
                            attachment?.createdByUser?.userFullname ?: ""
                        )
                    )
                }

                // ============================================================================= Insert Account Equipment
                response.body()?.data?.equipments?.forEach { equipment ->
                    // Insert selected online equipment
                    if (equipment?.equipmentId ?: "" == equipmentId) {
                        dao.accountEquipmentInsert(
                            AccountEquipmentEntity(
                                equipment?.equipmentId ?: "",
                                equipment?.regionId ?: "",
                                equipment?.serialNo ?: "",
                                equipment?.brand ?: "",
                                equipment?.warrantyStart ?: "",
                                equipment?.warrantyEnd ?: "",
                                equipment?.equipmentRemarks ?: "",
                                equipment?.deliveryAddressId ?: "",
                                equipment?.salesOrderNo ?: "",
                                equipment?.warrantyStatus ?: "",
                                accountID ?: "",
                                accountName,
                                equipment?.currentAgreementId ?: "",
                                "",
                                "",
                                equipment?.statusText?.enumId ?: "",
                                equipment?.statusText?.enumName ?: "",
                                equipment?.product?.productId ?: "",
                                equipment?.product?.productName ?: "",
                                false
                            )
                        )

                        // ========================================================================= Insert Product Group Item
                        dao.productGroupItemInsert(
                            ProductGroupItemEntity(
                                equipment?.product?.productGroupItem?.productGroupItemId ?: "",
                                equipment?.product?.productGroupItem?.productGroupId ?: "",
                                equipment?.product?.productGroupItem?.productId ?: ""
                            )
                        )


                        // ========================================================================= Insert Product Group
                        dao.productGroupInsert(
                            ProductEntity(
                                equipment?.product?.productGroupItem?.productGroup?.productGroupId
                                    ?: "",
                                equipment?.product?.productGroupItem?.productGroup?.productGroupName
                                    ?: "",
                                equipment?.product?.productGroupItem?.productGroup?.productGroupTagId
                                    ?: "",
                                equipment?.product?.productGroupItem?.productGroup?.machineBrand
                                    ?: "",
                                equipment?.product?.productGroupItem?.productGroup?.machineSupplier
                                    ?: "",
                                equipment?.product?.productGroupItem?.productGroup?.machineModel
                                    ?: "",
                                equipment?.product?.productGroupItem?.productGroup?.machineClassCategory
                                    ?: "",
                                equipment?.product?.productGroupItem?.productGroup?.voltage ?: "",
                                equipment?.product?.productGroupItem?.productGroup?.current ?: "",
                                equipment?.product?.productGroupItem?.productGroup?.power ?: "",
                                equipment?.product?.productGroupItem?.productGroup?.phase ?: "",
                                equipment?.product?.productGroupItem?.productGroup?.frequency ?: "",
                                equipment?.product?.productGroupItem?.productGroup?.airSupplyNeeded
                                    ?: "",
                                equipment?.product?.productGroupItem?.productGroup?.airPressure
                                    ?: "",
                                equipment?.product?.productId ?: "",
                                equipment?.product?.productGroupItem?.productGroup?.createdDate
                                    ?: "",
                                equipment?.product?.productGroupItem?.productGroup?.createdBy ?: "",
                                equipment?.product?.productGroupItem?.productGroup?.lastModifiedDate
                                    ?: "",
                                ""
                            )
                        )

                        // ========================================================================= Insert Product Document
                        equipment?.product?.productGroupItem?.productGroup?.productGroupDocument?.forEach { document ->
                            dao.documentInsert(
                                ProductDocumentEntity(
                                    document?.productGroupTechnicalDocumentId ?: "",
                                    document?.productGroupId ?: "",
                                    document?.title ?: "",
                                    document?.url ?: "",
                                    document?.createdDate ?: "",
                                    document?.createdBy ?: "",
                                    document?.lastModifiedDate ?: "",
                                    document?.lastModifiedBy ?: ""
                                )
                            )
                        }

                        // ========================================================================= Insert Sparepart
                        equipment?.product?.productGroupItem?.productGroup?.productGroupSparePart?.forEach { sparepart ->
                            dao.sparepartInsert(
                                SparepartEntity(
                                    sparepart?.productGroupSparePartId ?: "",
                                    sparepart?.productGroupId ?: "",
                                    sparepart?.imageNumber ?: "",
                                    sparepart?.partId ?: "",
                                    sparepart?.sparePart?.partNo ?: "",
                                    sparepart?.sparePart?.partName ?: "",
                                    sparepart?.technicalDrawing?.productGroupTechnicalDrawingId
                                        ?: "",
                                    sparepart?.technicalDrawing?.title ?: "",
                                    sparepart?.technicalDrawing?.page ?: "",
                                    sparepart?.technicalDrawing?.url ?: ""
                                )
                            )
                        }

                        equipment?.product?.productGroupItem?.productGroup?.productGroupChecklistTemplate?.forEach { checklistTemplate ->
                            // ===================================================================== Checklist Template
                            dao.checklistTemplateInsert(
                                ChecklistTemplateEntity(
                                    checklistTemplate?.productGroupChecklistTemplateId ?: "",
                                    checklistTemplate?.regionId ?: "",
                                    checklistTemplate?.productGroupId ?: "",
                                    checklistTemplate?.description ?: "",
                                    checklistTemplate?.createdDate ?: "",
                                    checklistTemplate?.createdBy ?: "",
                                    checklistTemplate?.lastModifiedDate ?: "",
                                    checklistTemplate?.lastModifiedBy ?: "",
                                    checklistTemplate?.checklistTypeText?.enumId ?: "",
                                    checklistTemplate?.checklistTypeText?.enumName ?: ""
                                )
                            )

                            // ===================================================================== Checklist Question
                            checklistTemplate?.checklistTemplateQuestions?.forEach { question ->
                                dao.checklistQuestionInsert(
                                    ChecklistQuestionEntity(
                                        question?.id ?: "",
                                        question?.orderNo ?: "",
                                        question?.sectionTitle ?: "",
                                        question?.description ?: "",
                                        question?.defaultRemarks ?: "",
                                        question?.helpText ?: "",
                                        question?.helpImageUrl ?: "",
                                        question?.createdDate ?: "",
                                        question?.createdBy ?: "",
                                        question?.lastModifiedDate ?: "",
                                        question?.lastModifiedBy ?: "",
                                        question?.checklistTemplateId ?: ""
                                    )
                                )
                            }


                        }
                    }
                }

                // ============================================================================= Insert Account Agreement
                response.body()?.data?.agreements?.forEach { agreement ->
                    dao.agreementInsert(
                        AgreementEntity(
                            agreement?.agreementId ?: "",
                            "",
                            "",
                            accountName,
                            "",
                            regionCode,
                            agreement?.accountContactId ?: "",
                            agreement?.agreementTypeEnum?.enumId ?: "",
                            agreement?.agreementTypeEnum?.enumName ?: "",
                            agreement?.agreementNumber ?: "",
                            agreement?.statusText?.enumId ?: "",
                            agreement?.statusText?.enumName ?: "",
                            "",
                            "",
                            "",
                            agreement?.installationDeliveryAddressData?.accountAddressId ?: "",
                            agreement?.installationDeliveryAddressData?.fullAddress ?: "",
                            agreement?.installationContactPicData?.accountContactId ?: "",
                            agreement?.installationContactPicData?.contactPersonName ?: "",
                            agreement?.installationContactPicData?.contactPersonPhone ?: "",
                            agreement?.agreementParentId ?: "",
                            agreement?.internalMemo ?: "",
                            agreement?.noOfService ?: "",
                            agreement?.agreementStartDate ?: "",
                            agreement?.agreementEndDate ?: "",
                            agreement?.installationDeliveryAddressData?.fullAddress ?: "",
                            agreement?.installationContactPicData?.contactPersonName ?: "",
                            agreement?.firstVisitDate ?: "",
                            agreement?.createdDate ?: "",
                            agreement?.createdByUser?.userFullname ?: "",
                            agreement?.lastModifiedDate ?: "",
                            agreement?.modifiedByUser?.userFullname ?: "",
                            agreement?.equipments ?: emptyList()
                        )
                    )
                }
            }
        }
        thread.start()
    }

    private fun insertEquipment(response: Response<GetDataRequest>) {
        val thread = object : Thread() {
            override fun run() {
                ReplaceDataBase(context).insertAll(dao, response)
            }
        }
        thread.start()
    }

    private fun handlingCallBack(_onlySyncAccount: Boolean) {
        if (_onlySyncAccount) {
            listener?.syncSearchCallback(accountDone)
        } else {
            listener?.syncSearchCallback(accountDone && equipmentDone)
        }
    }

    interface SyncSearchInterface {
        fun syncSearchCallback(requestSuccess: Boolean)
    }

}