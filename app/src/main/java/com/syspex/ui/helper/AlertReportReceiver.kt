package com.syspex.ui.helper

import android.app.AlarmManager
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.media.RingtoneManager
import android.os.AsyncTask
import android.os.Build
import android.util.Log
import android.widget.Toast
import androidx.core.app.JobIntentService.enqueueWork
import androidx.core.app.NotificationCompat
import com.syspex.R
import com.syspex.data.local.database.AppDatabase
import java.text.SimpleDateFormat
import java.util.*


class AlertReportReceiver : BroadcastReceiver() {

    override fun onReceive(context: Context?, intent: Intent?) {
        Toast.makeText(context, "alert report receiver", Toast.LENGTH_LONG).show()
        checkAvailableReport(context!!)

        if (context != null && intent != null) {
//            reprotAlert(context)

//            val comp = ComponentName(
//                context.packageName,
//                AlertReportJob::class.java.name
//            )
//            AlertReportJob().enqueueWork(context, intent.setComponent(comp))

//            val notifyIntent = Intent(context, AlertReportJob::class.java)
//            notifyIntent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
//            enqueueWork(context, AlertReportJob::class.java, 65223, notifyIntent)

//            val i = Intent(context, AlertJobService::class.java)
//            context.startService(i)

//            val pendingResult: PendingResult = goAsync()
//            val asyncTask = Task(context, pendingResult, intent)
//            asyncTask.execute()

        }
    }

    private class Task(val context: Context, val pendingResult: PendingResult, val intent: Intent)
        : AsyncTask<String, Int, String>() {

        override fun doInBackground(vararg params: String?): String {
            val sb = StringBuilder()
            sb.append("Action: ${intent.action}\n")
            sb.append("URI: ${intent.toUri(Intent.URI_INTENT_SCHEME)}\n")

            val notifyIntent = Intent(context, AlertReportJob::class.java)
            notifyIntent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
            enqueueWork(context, AlertReportJob::class.java, 65223, notifyIntent)

            return toString().also { log ->
                Log.d("aim", log)
            }
        }

        override fun onPostExecute(result: String?) {
            super.onPostExecute(result)
            // Must call finish() so the BroadcastReceiver can be recycled.
            pendingResult.finish()
        }
    }


    // ============================================================================================= Report Alert
    private fun reprotAlert(context: Context) {
        // Repeat alert after call, check available report every 20:00
        val c = Calendar.getInstance()
        c[Calendar.HOUR_OF_DAY] = 20
        c[Calendar.MINUTE] = 0
        c[Calendar.SECOND] = 0

        val alarmManager = context.getSystemService(Context.ALARM_SERVICE) as AlarmManager
        val i = Intent(context, AlertReportReceiver::class.java)
        val pendingIntent: PendingIntent = PendingIntent
            .getBroadcast(
                context,
                FunHelper.alertReportRequest,
                i,
                PendingIntent.FLAG_UPDATE_CURRENT
            )
        if (c.before(Calendar.getInstance())) { c.add(Calendar.DATE, 1) }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            alarmManager.setAndAllowWhileIdle(
                AlarmManager.RTC_WAKEUP,
                c.timeInMillis,
                pendingIntent
            )
        } else {
            alarmManager.setExact(AlarmManager.RTC_WAKEUP, c.timeInMillis, pendingIntent)
        }

        checkAvailableReport(context)
    }

    private fun checkAvailableReport(context: Context) {
        val dao = AppDatabase.getInstance(context)!!.dao()
        val sdf = SimpleDateFormat("yyyy-MM-dd", Locale.getDefault())
//        val startDateTime = "${sdf.format(Date())} 00:01"
//        val endDateTime = "${sdf.format(Date())} 23:59"

        val startDateTime = "2020-10-11 00:01"
        val endDateTime = "2020-10-11 23:59"
        Log.e("aim", "report start : $startDateTime, end : $endDateTime")

        val thread = object : Thread() {
            override fun run() {
                dao.reportTodayAlert(startDateTime, endDateTime).forEachIndexed { index, it ->
                    Log.e("aim", "report today : $it")
                    if (it.reportLocalId == null) {
                        val notificationId = index+1
                        val content = "You have open service call ${it.serviceCallNumber ?: "-"} " +
                                "overdue with no service report"
                        reportNotification(context, notificationId, content)
                    }
                }
            }
        }
        thread.start()
    }

    private fun reportNotification(context: Context, notificationId: Int, content: String) {
        val intent = Intent(context, AlertReportReceiver::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
        val pendingIntent = PendingIntent.getActivity(
            context, 0, intent,
            PendingIntent.FLAG_ONE_SHOT
        )

        val chanelId = "Default"
        val soundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION)
        val builder= NotificationCompat.Builder(context, chanelId)
            .setPriority(NotificationCompat.PRIORITY_HIGH)
            .setStyle(NotificationCompat.BigTextStyle().bigText(content))
            .setSmallIcon(R.mipmap.ic_launcher_round)
            .setContentTitle("Reminder")
            .setContentText(content)
            .setAutoCancel(true)
            .setSound(soundUri)
            .setContentIntent(pendingIntent)

        val manager = context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val channel = NotificationChannel(
                chanelId,
                "Default channel",
                NotificationManager.IMPORTANCE_DEFAULT
            )
            manager.createNotificationChannel(channel)
        }

        manager.notify(notificationId, builder.build())
    }
}