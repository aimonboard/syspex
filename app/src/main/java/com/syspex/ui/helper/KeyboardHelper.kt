package com.syspex.ui.helper

import android.content.Context
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.EditText


object KeyboardHelper {
//    fun hideSoftKeyboard(context: Context?, view: View) {
//        if (context == null) {
//            return
//        }
//        view.requestFocus()
//        view.postDelayed(Runnable {
//            val imm: InputMethodManager =
//                (context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager)
//            imm.hideSoftInputFromWindow(view.getWindowToken(), 0)
//        }, 500)
//    }

    fun hideSoftKeyboard(context: Context, editText: EditText) {
        editText.requestFocus()
        editText.postDelayed({
            val imm: InputMethodManager =
                (context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager)
            imm.hideSoftInputFromWindow(editText.windowToken, 0)
        }, 500)
    }

    fun openSoftKeyboard(context: Context, editText: EditText) {
        editText.requestFocus()
        editText.postDelayed({
            val imm: InputMethodManager =
                (context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager)
            imm.showSoftInput(editText, InputMethodManager.SHOW_IMPLICIT)
        }, 500)
    }
}