package com.syspex.ui.helper

import android.content.Context
import android.util.Log
import android.widget.Toast
import com.syspex.data.local.SharedPreferenceData
import com.syspex.data.local.database.AppDatabase
import com.syspex.data.local.database.account.AccountEntity
import com.syspex.data.local.database.account_address.AccountAddressEntity
import com.syspex.data.local.database.account_attachment.AccountAttachmentEntity
import com.syspex.data.local.database.account_contact.AccountContactEntity
import com.syspex.data.local.database.account_equipment.AccountEquipmentEntity
import com.syspex.data.local.database.agreement.AgreementEntity
import com.syspex.data.local.database.agreement_equipment.AgreementEquipmentEntity
import com.syspex.data.local.database.pcs_problem.ProblemEntity
import com.syspex.data.local.database.pcs_solution.SolutionEntity
import com.syspex.data.local.database.product_checklist_question.ChecklistQuestionEntity
import com.syspex.data.local.database.product_checklist_template.ChecklistTemplateEntity
import com.syspex.data.local.database.product_document.ProductDocumentEntity
import com.syspex.data.local.database.product_group.ProductEntity
import com.syspex.data.local.database.product_group_item.ProductGroupItemEntity
import com.syspex.data.local.database.product_sparepart.SparepartEntity
import com.syspex.data.local.database.service_call.ServiceCallEntity
import com.syspex.data.local.database.service_call_completion.ServiceCallCompletionEntity
import com.syspex.data.local.database.service_call_equipment.ServiceCallEquipmentEntity
import com.syspex.data.local.database.service_call_handover.ServiceCallHandoverEntity
import com.syspex.data.local.database.service_call_training.ServiceCallTrainingEntity
import com.syspex.data.local.database.service_case.ServiceCaseEntity
import com.syspex.data.local.database.service_case_equipment.ServiceCaseEquipmentEntity
import com.syspex.data.local.database.service_report.ServiceReportEntity
import com.syspex.data.local.database.service_report_equipment.ServiceReportEquipmentEntity
import com.syspex.data.local.database.service_report_equipment_checklist.ServiceReportEquipmentChecklistEntity
import com.syspex.data.local.database.service_report_sparepart.ServiceReportSparepartEntity
import com.syspex.data.local.database.service_report_time_tracking.ServiceReportTimeTrackingEntity
import com.syspex.data.model.ConverterImageModel
import com.syspex.data.remote.GetDataRequest
import com.syspex.data.retrofit.ApiClient
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class SyncDataFcm(val context: Context) {

    private val dao = AppDatabase.getInstance(context)!!.dao()
    private var listener: SendDataInterface? = null

    fun getByCaseId(id: String, source: String, mListener: SendDataInterface?) {
        this.listener = mListener
        val token = "Bearer "+SharedPreferenceData.getString(context, 2, "")
        val userId = SharedPreferenceData.getString(context, 8, "")

        val params = HashMap<String, Any>()
        when(source) {
            "case" -> params["case_id"] = id
            "call" -> params["call_id"] = id
            "report" -> params["report_id"] = id
        }
        Log.e("aim","sync one : $params")

        ApiClient.instance.getByCaseId(token, params).enqueue(object : Callback<GetDataRequest> {
            override fun onFailure(call: Call<GetDataRequest>, t: Throwable) {
                Log.e("aim", "export Err : $t")
                Toast.makeText(context, "Connection failed", Toast.LENGTH_LONG).show()
                listener?.sendDataCallback(false)
            }

            override fun onResponse(call: Call<GetDataRequest>,response: Response<GetDataRequest>) {
                listener?.sendDataCallback(true)

                if (!response.isSuccessful) {
                    if (response.code() == 401) {
                        FunHelper.clearAllData(context)
                        Toast.makeText(context, "Session end", Toast.LENGTH_LONG).show()
                    } else {
                        FunHelper.firebaseApiLog(
                            userId ?: "",
                            response.raw().request().url().toString() ?: "",
                            response.raw().request().headers().toString() ?: "",
                            params.toString() ?: "",
                            response.raw().toString() ?: ""
                        )
                        Toast.makeText(context, response.raw().toString(), Toast.LENGTH_LONG).show()
                    }
                } else {

                    // Reset Fcm, not empty = need fcm sync in menu
                    SharedPreferenceData.setString(context, 16, "")
                    prepareInsertDatabase(response)

                }
            }
        })
    }

    private fun prepareInsertDatabase(response: Response<GetDataRequest>) {
        val thread = object : Thread() {
            override fun run() {

                ReplaceDataBase(context).insertAll(dao, response)

            }
        }
        thread.start()
    }

}