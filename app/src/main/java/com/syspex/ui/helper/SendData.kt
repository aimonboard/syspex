package com.syspex.ui.helper

import android.content.Context
import android.util.Log
import android.widget.Toast
import com.syspex.data.local.SharedPreferenceData
import com.syspex.data.local.database.AppDatabase
import com.syspex.data.local.database.account.AccountEntity
import com.syspex.data.local.database.account_address.AccountAddressEntity
import com.syspex.data.local.database.account_attachment.AccountAttachmentEntity
import com.syspex.data.local.database.account_contact.AccountContactEntity
import com.syspex.data.local.database.account_equipment.AccountEquipmentEntity
import com.syspex.data.local.database.agreement.AgreementEntity
import com.syspex.data.local.database.agreement_equipment.AgreementEquipmentEntity
import com.syspex.data.local.database.email.EmailEntity
import com.syspex.data.local.database.pcs_problem.ProblemEntity
import com.syspex.data.local.database.pcs_solution.SolutionEntity
import com.syspex.data.local.database.product_checklist_question.ChecklistQuestionEntity
import com.syspex.data.local.database.product_checklist_template.ChecklistTemplateEntity
import com.syspex.data.local.database.product_document.ProductDocumentEntity
import com.syspex.data.local.database.product_group.ProductEntity
import com.syspex.data.local.database.product_group_item.ProductGroupItemEntity
import com.syspex.data.local.database.product_sparepart.SparepartEntity
import com.syspex.data.local.database.service_call.ServiceCallEntity
import com.syspex.data.local.database.service_call_completion.ServiceCallCompletionEntity
import com.syspex.data.local.database.service_call_equipment.ServiceCallEquipmentEntity
import com.syspex.data.local.database.service_call_handover.ServiceCallHandoverEntity
import com.syspex.data.local.database.service_call_training.ServiceCallTrainingEntity
import com.syspex.data.local.database.service_case.ServiceCaseEntity
import com.syspex.data.local.database.service_case_equipment.ServiceCaseEquipmentEntity
import com.syspex.data.local.database.service_report.ServiceReportEntity
import com.syspex.data.local.database.service_report_equipment.ServiceReportEquipmentEntity
import com.syspex.data.local.database.service_report_equipment_checklist.ServiceReportEquipmentChecklistEntity
import com.syspex.data.local.database.service_report_sparepart.ServiceReportSparepartEntity
import com.syspex.data.local.database.service_report_time_tracking.ServiceReportTimeTrackingEntity
import com.syspex.data.model.ConverterImageModel
import com.syspex.data.remote.GetDataRequest
import com.syspex.data.remote.SendEmailRequest
import com.syspex.data.retrofit.ApiClient
import org.json.JSONArray
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class SendData(val context: Context) {

    private val dao = AppDatabase.getInstance(context)!!.dao()
    private var listener: SendDataInterface? = null
    private val params = HashMap<String, Any>()
    private val token = "Bearer "+SharedPreferenceData.getString(context, 2, "")
    private val userId = SharedPreferenceData.getString(context, 8, "")

    fun getParam(mListener: SendDataInterface?) {
        this.listener = mListener

        val thread = object : Thread() {
            override fun run() {

                val userId = SharedPreferenceData.getString(context, 8, "")
                val userName = SharedPreferenceData.getString(context, 10, "")

                // ================================================================================= Activity
                val activityArray = JSONArray()
                dao.activityUpload(true).forEach {
                    val activityObject = JSONObject()

                    if (it.activityId.length < 5) {
                        // edit, id not random uuid
                        activityObject.put("activity_id", it.activityId)
                    }

                    activityObject.put("activity_type", it.activityTypeId)
                    activityObject.put("activity_start_time", it.activityStartTime)
                    activityObject.put("activity_end_time", it.activityEndTime)
                    activityObject.put("activity_subject", it.activitySubject)
                    activityObject.put("activity_description", it.activityDescription)

                    activityArray.put(activityObject)
                }

                if (activityArray.length() > 0) params["activity"] = activityArray

                // ================================================================================= Service Case
                val caseArray = JSONArray()
                dao.caseUpload(false).forEach {
                    val caseObject = JSONObject()
                    caseObject.put("case_id", it)

                    val caseEquipmentArray = JSONArray()
                    dao.caseEquipmentUpload(it).forEachIndexed { index, equipmentId ->
                        caseEquipmentArray.put(index, equipmentId)
                    }
                    caseObject.put("equipments", caseEquipmentArray)

                    caseArray.put(caseObject)
                }

                if (caseArray.length() > 0) params["service_case"] = caseArray

                // ================================================================================= Service Call
                val callArray = JSONArray()
                dao.callUpload(false).forEach {
                    val callObject = JSONObject()
                    callObject.put("call_id", it)

                    val callEquipmentArray = JSONArray()
                    dao.callEquipmentUpload(it).forEachIndexed { index, equipmentId ->
                        callEquipmentArray.put(index, equipmentId)
                    }
                    callObject.put("equipments", callEquipmentArray)

                    callArray.put(callObject)
                }

                if (callArray.length() > 0) params["service_call"] = callArray

                // ================================================================================= Service Report
                val reportAray = JSONArray()
                dao.reportUpload(false).forEach { report ->

                    val reportObject = JSONObject()
                    val reportId = report.reportLocalId

                    if (report.reportServerId.isNotEmpty()) {
                        reportObject.put("service_report_id", report.reportServerId)
                    } else {
                        reportObject.put("service_report_id", null)
                    }

                    reportObject.put("call_id", report.callId)
                    reportObject.put("report_status", report.reportStatusEnumId)
                    reportObject.put("report_date", report.createdDate)
                    reportObject.put("internal_memo", report.internalMemo)
                    reportObject.put("is_chargeable", report.isChargeable)
                    reportObject.put("service_report_number_temp", report.serviceReportNumber)
                    reportObject.put("field_pic_contact_id", report.fieldAccountContactId)
                    reportObject.put("cp_number", report.cpNumber)
                    reportObject.put("cp_name", report.cpName)

                    // ============================================================================= Feedback
                    reportObject.put("customer_rating", report.customerRating) // 5.0
                    reportObject.put("feedback", report.feedback)
                    reportObject.put("employee_id", userId)
                    reportObject.put("employee_name", userName)
                    reportObject.put("serviced_by_signature", report.servicedBySignatureFileName)
                    reportObject.put("account_pic_signature", report.accountPicSignatureFileName)

                    // ============================================================================= Time Tracking
                    val timeTrackingArray = JSONArray()
                    dao.reportTimeTrackingUpload(reportId).forEach { timeTracking->
                        val timeTrackingObject = JSONObject()
                        val start = timeTracking.start.split(" ")
                        val end = timeTracking.end.split(" ")
                        timeTrackingObject.put("aim_timetracking_id", timeTracking.timeTrackingId)
                        timeTrackingObject.put("engineer_id", timeTracking.engineerId)
                        timeTrackingObject.put("date", start[0])
                        timeTrackingObject.put("start_time", start[1])
                        timeTrackingObject.put("end_time", end[1])

                        timeTrackingArray.put(timeTrackingObject)
                    }
                    reportObject.put("time_tracking", timeTrackingArray)

                    // ============================================================================= Report Equipment
                    val reportEquipmentArray = JSONArray()
                    dao.reportEquipmentUpload(reportId).forEach { equipment->

                        val reportEquipmentObject = JSONObject()
                        reportEquipmentObject.put(
                            "aim_report_equipment_id",
                            equipment.serviceReportEquipmentId
                        )
                        reportEquipmentObject.put("equipment_id", equipment.equipmentId)
                        reportEquipmentObject.put("problem_symptom", equipment.problemSymptom)
                        reportEquipmentObject.put(
                            "status_after_service",
                            equipment.statusAfterServiceEnumId
                        )
                        reportEquipmentObject.put(
                            "status_after_service_remarks",
                            equipment.statusAfterServiceRemarks
                        )
                        reportEquipmentObject.put(
                            "installation_start_date",
                            equipment.installationStartDate
                        )
                        reportEquipmentObject.put(
                            "installation_matters",
                            equipment.installationMatters
                        )
                        reportEquipmentObject.put(
                            "checklist_template_id",
                            equipment.checklistTemplateId
                        )

                        // ========================================================================= Problem Image
                        val problemArray = JSONArray()
                        dao.problemUpload(equipment.serviceReportEquipmentId).forEach { problem->
                            problem.problemImage?.forEach { problemImage->
                                if (problemImage.fileName.isNotEmpty()) {
                                    problemArray.put(problemImage.fileName)
                                } else {
                                    problemArray.put(problemImage.fileUri)
                                }
                            }
                        }
                        reportEquipmentObject.put("service_report_equipment_image", problemArray)

                        // ========================================================================= Solution
                        val solutionArray = JSONArray()
                        dao.solutionUpload(equipment.serviceReportEquipmentId).forEach { solution->
                            val solutionObject = JSONObject()
                            solutionObject.put("subject", solution.subject)
                            solutionObject.put("problem_cause", solution.problemCause)
                            solutionObject.put("solution", solution.solution)

                            //  Cause Image
                            val causeImageArray = JSONArray()
                            solution.causeImage?.forEach { causeImage->
                                if (causeImage.fileName.isNotEmpty()) {
                                    causeImageArray.put(causeImage.fileName)
                                } else {
                                    causeImageArray.put(causeImage.fileUri)
                                }
                            }
                            solutionObject.put(
                                "service_report_equipment_cause_image",
                                causeImageArray
                            )

                            // ===================================================================== Solution Image
                            val solutionImageArray = JSONArray()
                            solution.solutionImage?.forEach { solutionImage->
                                if (solutionImage.fileName.isNotEmpty()) {
                                    solutionImageArray.put(solutionImage.fileName)
                                } else {
                                    solutionImageArray.put(solutionImage.fileUri)
                                }
                            }
                            solutionObject.put(
                                "service_report_equipment_solutions_image",
                                solutionImageArray
                            )
                            solutionArray.put(solutionObject)
                        }
                        reportEquipmentObject.put(
                            "service_report_equipment_solutions",
                            solutionArray
                        )

                        // ========================================================================= Checklist
                        val checklistArray = JSONArray()
                        dao.reportChecklistUpload(equipment.serviceReportEquipmentId).forEach { checklist->
//                            if (checklist.checklist_status_id.isNotEmpty() && checklist.checklist_status_Name.isNotEmpty()) {
//                                val checklistObject = JSONObject()
//                                checklistObject.put("question_id", checklist.question_id)
//                                checklistObject.put("description", checklist.question_desc)
//                                checklistObject.put("status", checklist.checklist_status_id)
//                                checklistObject.put("remarks", checklist.question_default_remarks)
//
//                                // Checklist Image
//                                val checklistImageArray = JSONArray()
//                                checklist.checklistImage?.forEach { checklistImage->
//                                    if (checklistImage.fileName.isNotEmpty()) {
//                                        checklistImageArray.put(checklistImage.fileName)
//                                    } else {
//                                        checklistImageArray.put(checklistImage.fileUri)
//                                    }
//                                }
//                                checklistObject.put("service_report_equipment_checklist_image",checklistImageArray)
//                                checklistArray.put(checklistObject)
//                            }


                            val checklistObject = JSONObject()
                            checklistObject.put("question_id", checklist.question_id)
                            checklistObject.put("description", checklist.question_desc)
                            checklistObject.put("status", checklist.checklist_status_id)
                            checklistObject.put("remarks", checklist.remarks)

                            // Checklist Image
                            val checklistImageArray = JSONArray()
                            checklist.checklistImage?.forEach { checklistImage->
                                if (checklistImage.fileName.isNotEmpty()) {
                                    checklistImageArray.put(checklistImage.fileName)
                                } else {
                                    checklistImageArray.put(checklistImage.fileUri)
                                }
                            }
                            checklistObject.put(
                                "service_report_equipment_checklist_image",
                                checklistImageArray
                            )
                            checklistArray.put(checklistObject)

                        }
                        reportEquipmentObject.put("equipment_checklist", checklistArray)

                        // ========================================================================= Sparepart
                        val sparepartArray = JSONArray()
                        dao.reportSparepartUpload(equipment.serviceReportEquipmentId).forEach { sparepart->
                            val sparepartObject = JSONObject()
                            if (sparepart.part_id.isNotEmpty()) {
                                sparepartObject.put("spare_part_id", sparepart.part_id)
                            }
                            sparepartObject.put("spare_part_image_number",sparepart.spare_part_image_number)
                            sparepartObject.put("spare_part_number",sparepart.spare_part_number)
                            sparepartObject.put("spare_part_description",sparepart.spare_part_description)
                            sparepartObject.put("spare_part_memo", sparepart.spare_part_memo)
                            sparepartObject.put("spare_part_type", sparepart.enum_id)
                            sparepartObject.put("qty", sparepart.qty)

                            sparepartArray.put(sparepartObject)
                        }
                        reportEquipmentObject.put("service_report_sparepart", sparepartArray)

                        reportEquipmentArray.put(reportEquipmentObject)
                    }
                    reportObject.put("service_report_equipment", reportEquipmentArray)
                    reportAray.put(reportObject)
                }

                if (reportAray.length() > 0) params["service_report"] = reportAray


                val paramString = params.toString()
                val maxLogSize = 1000
                for (i in 0..paramString.length / maxLogSize) {
                    val start = i * maxLogSize
                    var end = (i + 1) * maxLogSize
                    end = if (end > paramString.length) paramString.length else end
                    Log.e("aim", paramString.substring(start, end))
                }

                if (activityArray.length() > 0 || caseArray.length() > 0 || callArray.length() > 0 || reportAray.length() > 0) {
                    sendData()
                } else {
                    Log.e("aim", "Send data abort")
                    listener?.sendDataCallback(true)
                }

                // ================================================================================= Send Email
                dao.emailAll().forEach {
                    sendEmail(it)
                }

            }
        }
        thread.start()
    }

    fun sendData() {
        ApiClient.instance.sendDataV2(token, params).enqueue(object : Callback<GetDataRequest> {
            override fun onFailure(call: Call<GetDataRequest>, t: Throwable) {
                Log.e("aim", "export Err : $t")
                listener?.sendDataCallback(false)
            }

            override fun onResponse(call: Call<GetDataRequest>,response: Response<GetDataRequest>) {
                listener?.sendDataCallback(true)

                if (response.isSuccessful) {
                    prepareInsertDatabase(response)
                }
                else {
                    if (response.code() == 401) {
                        FunHelper.clearAllData(context)
                        Toast.makeText(context, "Session end", Toast.LENGTH_LONG).show()
                    } else {
                        FunHelper.firebaseApiLog(
                            userId ?: "",
                            response.raw().request().url().toString() ?: "",
                            response.raw().request().headers().toString() ?: "",
                            params.toString() ?: "",
                            response.raw().toString() ?: ""
                        )
                        Toast.makeText(context, response.raw().toString(), Toast.LENGTH_LONG).show()
                    }

                }
            }
        })
    }

    private fun prepareInsertDatabase(response: Response<GetDataRequest>) {
        val thread = object : Thread() {
            override fun run() {

                deleteLocalData()
                ReplaceDataBase(context).insertAll(dao, response)

            }
        }
        thread.start()
    }

    private fun deleteLocalData() {
        // ================================================================================= Delete Local DB
        val thread = object : Thread() {
            override fun run() {
                val body = JSONObject(params as Map<String, Any>)

                if (body.has("service_case")) {
                    val caseArray = body.getJSONArray("service_case")
                    for (i in 0 until caseArray.length()) {
                        val caseObject = caseArray.getJSONObject(i)

                        val caseId = caseObject.getString("case_id")
                        dao.caseEquipmentDeleteByCaseId(caseId)
                    }
                }

                if (body.has("service_call")) {
                    val callArray = body.getJSONArray("service_call")
                    for (i in 0 until callArray.length()) {
                        val callObject = callArray.getJSONObject(i)

                        val callId = callObject.getString("call_id")
                        dao.callEquipmentDeleteByCallId(callId)
                    }
                }

                if (body.has("service_report")) {
                    val reportArray = body.getJSONArray("service_report")
                    for (i in 0 until reportArray.length()) {
                        val reportObject = reportArray.getJSONObject(i)

                        val timeTrackingArray = reportObject.getJSONArray("time_tracking")
                        for (x in 0 until timeTrackingArray.length()) {
                            val timeTrackingObject = timeTrackingArray.getJSONObject(x)
                            val timeTrackingId = timeTrackingObject.getString("aim_timetracking_id")
                            dao.reportTimeTrackingDeleteById(timeTrackingId)
                        }

                        val reportEquipmentArray = reportObject.getJSONArray("service_report_equipment")
                        for (j in 0 until reportEquipmentArray.length()) {
                            val reportEquipmentObjects = reportEquipmentArray.getJSONObject(j)
                            val reportEquipmentId =
                                reportEquipmentObjects.getString("aim_report_equipment_id")

                            dao.reportEquipmentDeleteById(reportEquipmentId)
                            dao.reportSparepartDeleteByReportEquipmentId(reportEquipmentId)
                            dao.reportChecklistDeleteByReportEquipmentId(reportEquipmentId)
                            dao.solutionDeleteByReportEquipmnetId(reportEquipmentId)
                        }
                    }
                }

                dao.reportSparepartDeleteLocal(true)
                dao.reportChecklistDeleteLocal(true)
                dao.solutionDeleteLocal(true)
                dao.reportTimeTrackingDeleteLocal(true)
                dao.reportEquipmentDeleteLocal(true)
                dao.reportDeleteLocal(true)
                dao.activityDeleteLocal(true)
            }
        }
        thread.start()
    }

    private fun sendEmail(dataEmail: EmailEntity) {
        val token = "Bearer "+ SharedPreferenceData.getString(context,2, "")

        val params = HashMap<String, Any>()
        params["report_id"] = JSONArray(dataEmail.reportId)
        params["to"] = JSONArray(dataEmail.to)
        params["others"] = JSONArray(dataEmail.other)
        params["additional_attachment"] = JSONArray(dataEmail.attachment)
        Log.e("aim","send email : $params")

        ApiClient.instance.sendEmail(token, params).enqueue(object: Callback<SendEmailRequest> {
            override fun onFailure(call: Call<SendEmailRequest>, t: Throwable) { }
            override fun onResponse(call: Call<SendEmailRequest>, response: Response<SendEmailRequest>) {
                if (response.isSuccessful) {
                    val thread = object : Thread() {
                        override fun run() {
                            dao.emailDeleteById(dataEmail.emailId)
                        }
                    }
                    thread.start()
                }
                else {
                    if (response.code() == 401) {
                        FunHelper.clearAllData(context)
                        Toast.makeText(context, "Session end", Toast.LENGTH_LONG).show()
                    } else {
                        FunHelper.firebaseApiLog(
                            userId ?: "",
                            response.raw().request().url().toString() ?: "",
                            response.raw().request().headers().toString() ?: "",
                            params.toString() ?: "",
                            response.raw().toString() ?: ""
                        )
                        Toast.makeText(context, response.raw().toString(), Toast.LENGTH_LONG).show()
                    }
                }
            }
        })
    }

}