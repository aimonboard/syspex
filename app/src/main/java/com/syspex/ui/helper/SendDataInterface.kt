package com.syspex.ui.helper

interface SendDataInterface {
    fun sendDataCallback(isSuccess: Boolean)
}