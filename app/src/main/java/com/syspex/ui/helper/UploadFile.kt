package com.syspex.ui.helper

import android.content.Context
import android.util.Log
import com.syspex.data.local.SharedPreferenceData
import com.syspex.data.local.database.AppDatabase
import com.syspex.data.local.database.file.FileEntity
import com.syspex.data.model.ConverterImageModel
import net.gotev.uploadservice.protocols.multipart.MultipartUploadRequest
import java.util.*

class UploadFile(val context: Context) {

    val dao = AppDatabase.getInstance(context)!!.dao()

    // ============================================================================================= Insert
    fun saveDB(fileData: ArrayList<ConverterImageModel>, fileSection: String) {
        val thread = object : Thread() {
            override fun run() {
                fileData.forEach {
                    val data = FileEntity(
                        0,
                        it.fileUri ?: "",
                        it.fileName ?: "",
                        fileSection ?: ""
                    )

                    dao.fileInsert(data)
                }
                startUploadFile()
            }
        }
        thread.start()
    }

    // ============================================================================================= Upload
    fun startUploadFile() {
        val thread = object : Thread() {
            override fun run() {
                dao.fileGetUpload().forEachIndexed { i: Int, fileEntity: FileEntity ->
                    uploadService(fileEntity.name, fileEntity.uri, fileEntity.section)
                    Log.d("aim", "masuk DB: ${fileEntity.uri}, name: ${fileEntity.name}, section: ${fileEntity.section}")
                }
            }
        }
        thread.start()
    }

    private fun uploadService(fileName: String, fileUri: String, fileSection: String) {
        val extension = fileName.split(".")

        val url ="${FunHelper.BASE_URL}api/upload-file"
        val header = "Bearer ${SharedPreferenceData.getString(context,2,"")}"

        val test = MultipartUploadRequest(context, url)
            .setMethod("POST")
            .addHeader("Authorization",header)
            .addParameter("file_name", fileName)
            .addParameter("file_extension", extension[extension.lastIndex])
            .addFileToUpload(fileUri, "myfile")
            .setUploadID(fileName)
//            .setAutoDeleteFilesAfterSuccessfulUpload(true)
            .setMaxRetries(0)

        test.startUpload()
    }

}