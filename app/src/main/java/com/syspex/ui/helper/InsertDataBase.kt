package com.syspex.ui.helper

import android.content.Context
import com.syspex.data.local.database.AppDatabase
import com.syspex.data.local.database.GlobalDao
import com.syspex.data.local.database.account.AccountEntity
import com.syspex.data.local.database.account_address.AccountAddressEntity
import com.syspex.data.local.database.account_attachment.AccountAttachmentEntity
import com.syspex.data.local.database.account_contact.AccountContactEntity
import com.syspex.data.local.database.account_equipment.AccountEquipmentEntity
import com.syspex.data.local.database.agreement.AgreementEntity
import com.syspex.data.local.database.agreement_equipment.AgreementEquipmentEntity
import com.syspex.data.local.database.pcs_problem.ProblemEntity
import com.syspex.data.local.database.pcs_solution.SolutionEntity
import com.syspex.data.local.database.product_checklist_question.ChecklistQuestionEntity
import com.syspex.data.local.database.product_checklist_template.ChecklistTemplateEntity
import com.syspex.data.local.database.product_document.ProductDocumentEntity
import com.syspex.data.local.database.product_group.ProductEntity
import com.syspex.data.local.database.product_group_item.ProductGroupItemEntity
import com.syspex.data.local.database.product_sparepart.SparepartEntity
import com.syspex.data.local.database.service_call.ServiceCallEntity
import com.syspex.data.local.database.service_call_completion.ServiceCallCompletionEntity
import com.syspex.data.local.database.service_call_equipment.ServiceCallEquipmentEntity
import com.syspex.data.local.database.service_call_handover.ServiceCallHandoverEntity
import com.syspex.data.local.database.service_call_training.ServiceCallTrainingEntity
import com.syspex.data.local.database.service_case.ServiceCaseEntity
import com.syspex.data.local.database.service_case_equipment.ServiceCaseEquipmentEntity
import com.syspex.data.local.database.service_report.ServiceReportEntity
import com.syspex.data.local.database.service_report_equipment.ServiceReportEquipmentEntity
import com.syspex.data.local.database.service_report_equipment_checklist.ServiceReportEquipmentChecklistEntity
import com.syspex.data.local.database.service_report_sparepart.ServiceReportSparepartEntity
import com.syspex.data.local.database.service_report_time_tracking.ServiceReportTimeTrackingEntity
import com.syspex.data.model.ConverterImageModel
import com.syspex.data.remote.GetDataRequest
import retrofit2.Response

class InsertDataBase(val context: Context) {

    fun insertAll(dao: GlobalDao, response: Response<GetDataRequest>) {

        response.body()?.data?.serviceCase?.forEach {
            val caseID = it?.caseId
            val accountID = it?.account?.accountId
            val accountName = it?.account?.accountName
            val agreementID = it?.agreement?.agreementId
            val agreementNumber = it?.agreement?.agreementNumber
            val agreementType = it?.agreement?.agreementTypeEnum?.enumName
            val agreementEnd = it?.agreement?.agreementEndDate
            val regionCode = it?.region?.regionCode

            dao.caseInsert(
                ServiceCaseEntity(
                    it?.caseId ?: "",
                    accountID ?: "",
                    accountName ?: "",
                    it?.agreementId ?: "",
                    agreementType ?: "",
                    regionCode ?: "",
                    it?.caseNumber ?: "",
                    it?.caseTypeText?.enumId ?: "",
                    it?.caseTypeText?.enumName ?: "",
                    it?.caseTypeRemark ?: "",
                    it?.accountContact?.accountContactId ?: "",
                    it?.accountContact?.contactPersonName ?: "",
                    it?.accountContact?.contactPersonPhone ?: "",
                    it?.fieldAccountContact?.accountContactId ?: "",
                    it?.fieldAccountContact?.contactPersonName ?: "",
                    it?.fieldAccountContact?.contactPersonPhone ?: "",
                    it?.accountSalesPic?.id ?: "",
                    it?.accountSalesPic?.userFullname ?: "",
                    it?.salesPic?.id ?: "",
                    it?.salesPic?.userFullname ?: "",
                    it?.leadEngineer?.id ?: "",
                    it?.leadEngineer?.userFullname ?: "",
                    it?.owner?.id ?: "",
                    it?.owner?.userFullname ?: "",
                    it?.caseStatusText?.enumId ?: "",
                    it?.caseStatusText?.enumName ?: "",
                    "",
                    "",
                    "",
                    it?.caseStatusRemark ?: "",
                    it?.subject ?: "",
                    it?.description ?: "",
                    it?.address?.accountAddressId ?: "",
                    it?.address?.fullAddress ?: "",
                    it?.loanDemoPeriod ?: "",
                    it?.openedTime ?: "",
                    it?.assignedTime ?: "",
                    it?.proposedDeliveryDate ?: "",
                    it?.proposedInstallationDate ?: "",
                    it?.directDelivery ?: "",
                    it?.customerPo ?: "",
                    it?.noOfFreeService ?: "",
                    it?.materialNeeded ?: "",
                    it?.airUtilitySuppliedBy ?: "",
                    it?.specialRequirement ?: "",
                    it?.warrantyPeriodSupplier ?: "",
                    "",
                    it?.professionalInspectionRequired ?: "",
                    it?.inspectionRemark ?: "",
                    "",
                    it?.createdDate ?: "",
                    it?.createdByUser?.userFullname ?: "",
                    it?.lastModifiedDate ?: "",
                    it?.modifiedByUser?.userFullname ?: "",
                    it?.serviceCaseEquipments ?: emptyList(),
                    emptyList(),
                    true
                )
            )

            // ============================================================================= Insert Service Case Equipment
            it?.serviceCaseEquipments?.forEach { equipment->
                dao.caseEquipmentInsert(
                    ServiceCaseEquipmentEntity(
                        equipment?.seviceCaseEquipment ?: "",
                        equipment?.caseId ?: "",
                        equipment?.equipmentId ?: "",
                        equipment?.createdDate ?: "",
                        equipment?.createdBy ?: "",
                        equipment?.lastModifiedDate ?: "",
                        equipment?.lastModifiedBy ?: "",
                        equipment?.equipment?.regionId ?: "",
                        equipment?.equipment?.serialNo ?: "",
                        equipment?.equipment?.brand ?: "",
                        equipment?.equipment?.warrantyStart ?: "",
                        equipment?.equipment?.warrantyEnd ?: "",
                        equipment?.equipment?.equipmentRemarks ?: "",
                        equipment?.equipment?.deliveryAddressId ?: "",
                        equipment?.equipment?.salesOrderNo ?: "",
                        equipment?.equipment?.warrantyStatus ?: "",
                        accountID ?: "",
                        accountName ?: "",
                        equipment?.agreementId ?: "",
                        agreementNumber ?: "",
                        agreementEnd ?: "",
                        equipment?.equipment?.statusText?.enumId ?: "",
                        equipment?.equipment?.statusText?.enumName ?: "",
                        equipment?.equipment?.product?.productId ?: "",
                        equipment?.equipment?.product?.productName ?: "",
                        true,
                        false
                    )
                )
            }

            // ============================================================================= Insert Account
            if (it?.account != null) {
                dao.accountInsert(
                    AccountEntity(
                        it.account.accountId ?: "",
                        caseID ?: "",
                        agreementID ?: "",
                        "",
                        it.account.accountCode ?: "",
                        it.account.accountName ?: "",
                        it.account.active ?: "",
                        it.account.createdDate ?: "",
                        it.account.createdBy ?: "",
                        it.lastModifiedDate ?: "",
                        "",
                        it.account.internalMemo ?: "",
                        "",
                        "",
                        it.account.sales?.id ?: "",
                        it.account.sales?.userFullname ?: "",
                        it.account.sales?.email ?: "",
                        it.account.sales?.phone ?: "",
                        regionCode ?: "",
                        it.account.region?.regionName ?: "",
                        it.account.contact ?: emptyList(),
                        it.account.address ?: emptyList()
                    )
                )
            }

            // ============================================================================= Insert Account Contact
            it?.account?.contact?.forEach { contact->
                dao.accountContactInsert(
                    AccountContactEntity(
                        contact?.accountContactId ?: "",
                        contact?.regionId ?: "",
                        contact?.customerId ?: "",
                        accountName ?: "",
                        contact?.contactPersonName ?: "",
                        contact?.contactPersonPhone ?: "",
                        contact?.contactPersonEmail ?: "",
                        contact?.contactPersonAddress ?: "",
                        contact?.contactPersonPosition ?: "",
                        contact?.contactTypeText?.enumName ?: "",
                        contact?.priority ?: "",
                        contact?.createdDate ?: "",
                        contact?.createdBy ?: "",
                        contact?.lastModifiedDate ?: "",
                        contact?.lastModifiedBy ?: ""
                    )
                )
            }

            // ============================================================================= Insert Account Address
            it?.account?.address?.forEach { address->
                dao.accountAddressInsert(
                    AccountAddressEntity(
                        address?.accountAddressId ?: "",
                        address?.regionId ?: "",
                        address?.accountId ?: "",
                        address?.addressTitle ?: "",
                        address?.addressLine1 ?: "",
                        address?.addressLine2 ?: "",
                        address?.addressLine3 ?: "",
                        address?.addressCountry ?: "",
                        address?.addressCity ?: "",
                        address?.addressZipCode ?: "",
                        address?.priority ?: "",
                        address?.createdDate ?: "",
                        address?.createdBy ?: "",
                        address?.lastModifiedDate ?: "",
                        address?.lastModifiedBy ?: "",
                        address?.fullAddress ?: ""
                    )
                )
            }

            // ============================================================================= Insert Account Attachment
            it?.account?.attachment?.forEach { attachment->
                dao.accountAttachmentInsert(
                    AccountAttachmentEntity(
                        attachment?.accountAttachmentId ?: "",
                        attachment?.regionId ?: "",
                        attachment?.accountId ?: "",
                        attachment?.attachmentTitle ?: "",
                        attachment?.attachmentBody ?: "",
                        attachment?.fileUrl ?: "",
                        attachment?.attachmentOwnerId ?: "",
                        attachment?.createdDate ?: "",
                        attachment?.createdByUser?.id ?: "",
                        attachment?.lastModifiedDate ?: "",
                        attachment?.modifiedByUser?.userFullname ?: "",
                        attachment?.attachmentSourceTable ?: "",
                        attachment?.attachmentSourceId ?: "",
                        "",
                        "",
                        attachment?.createdByUser?.id ?: "",
                        attachment?.createdByUser?.userFullname ?: ""
                    )
                )
            }

            // ============================================================================= Insert Account Equipment
            it?.account?.equipments?.forEach { equipment->
                dao.accountEquipmentInsert(
                    AccountEquipmentEntity(
                        equipment?.equipmentId ?: "",
                        equipment?.regionId ?: "",
                        equipment?.serialNo ?: "",
                        equipment?.brand ?: "",
                        equipment?.warrantyStart ?: "",
                        equipment?.warrantyEnd ?: "",
                        equipment?.equipmentRemarks ?: "",
                        equipment?.deliveryAddressId ?: "",
                        equipment?.salesOrderNo ?: "",
                        equipment?.warrantyStatus ?: "",
                        accountID ?: "",
                        accountName ?: "",
                        equipment?.currentAgreementId ?: "",
                        agreementNumber ?: "",
                        agreementEnd ?: "",
                        equipment?.statusText?.enumId ?: "",
                        equipment?.statusText?.enumName ?: "",
                        equipment?.product?.productId ?: "",
                        equipment?.product?.productName ?: "",
                        false
                    )
                )

                // ========================================================================= Insert Product Group Item
                dao.productGroupItemInsert(
                    ProductGroupItemEntity(
                        equipment?.product?.productGroupItem?.productGroupItemId ?: "",
                        equipment?.product?.productGroupItem?.productGroupId ?: "",
                        equipment?.product?.productGroupItem?.productId ?: ""
                    )
                )


                // ========================================================================= Insert Product Group
                dao.productGroupInsert(
                    ProductEntity(
                        equipment?.product?.productGroupItem?.productGroup?.productGroupId
                            ?: "",
                        equipment?.product?.productGroupItem?.productGroup?.productGroupName
                            ?: "",
                        equipment?.product?.productGroupItem?.productGroup?.productGroupTagId
                            ?: "",
                        equipment?.product?.productGroupItem?.productGroup?.machineBrand
                            ?: "",
                        equipment?.product?.productGroupItem?.productGroup?.machineSupplier
                            ?: "",
                        equipment?.product?.productGroupItem?.productGroup?.machineModel
                            ?: "",
                        equipment?.product?.productGroupItem?.productGroup?.machineClassCategory
                            ?: "",
                        equipment?.product?.productGroupItem?.productGroup?.voltage ?: "",
                        equipment?.product?.productGroupItem?.productGroup?.current ?: "",
                        equipment?.product?.productGroupItem?.productGroup?.power ?: "",
                        equipment?.product?.productGroupItem?.productGroup?.phase ?: "",
                        equipment?.product?.productGroupItem?.productGroup?.frequency ?: "",
                        equipment?.product?.productGroupItem?.productGroup?.airSupplyNeeded
                            ?: "",
                        equipment?.product?.productGroupItem?.productGroup?.airPressure
                            ?: "",
                        equipment?.product?.productId ?: "",
                        equipment?.product?.productGroupItem?.productGroup?.createdDate
                            ?: "",
                        equipment?.product?.productGroupItem?.productGroup?.createdBy ?: "",
                        equipment?.product?.productGroupItem?.productGroup?.lastModifiedDate
                            ?: "",
                        ""
                    )
                )

                // ========================================================================= Insert Product Document
                equipment?.product?.productGroupItem?.productGroup?.productGroupDocument?.forEach { document->
                    dao.documentInsert(
                        ProductDocumentEntity(
                            document?.productGroupTechnicalDocumentId ?: "",
                            document?.productGroupId ?: "",
                            document?.title ?: "",
                            document?.url ?: "",
                            document?.createdDate ?: "",
                            document?.createdBy ?: "",
                            document?.lastModifiedDate ?: "",
                            document?.lastModifiedBy ?: ""
                        )
                    )
                }

                // ========================================================================= Insert Sparepart
                equipment?.product?.productGroupItem?.productGroup?.productGroupSparePart?.forEach { sparepart->
                    dao.sparepartInsert(
                        SparepartEntity(
                            sparepart?.productGroupSparePartId ?: "",
                            sparepart?.productGroupId ?: "",
                            sparepart?.imageNumber ?: "",
                            sparepart?.partId ?: "",
                            sparepart?.sparePart?.partNo ?: "",
                            sparepart?.sparePart?.partName ?: "",
                            sparepart?.technicalDrawing?.productGroupTechnicalDrawingId ?: "",
                            sparepart?.technicalDrawing?.title ?: "",
                            sparepart?.technicalDrawing?.page ?: "",
                            sparepart?.technicalDrawing?.url ?: ""
                        )
                    )
                }

                equipment?.product?.productGroupItem?.productGroup?.productGroupChecklistTemplate?.forEach { checklistTemplate->
                    // ===================================================================== Checklist Template
                    dao.checklistTemplateInsert(
                        ChecklistTemplateEntity(
                            checklistTemplate?.productGroupChecklistTemplateId ?: "",
                            checklistTemplate?.regionId ?: "",
                            checklistTemplate?.productGroupId ?: "",
                            checklistTemplate?.description ?: "",
                            checklistTemplate?.createdDate ?: "",
                            checklistTemplate?.createdBy ?: "",
                            checklistTemplate?.lastModifiedDate ?: "",
                            checklistTemplate?.lastModifiedBy ?: "",
                            checklistTemplate?.checklistTypeText?.enumId ?: "",
                            checklistTemplate?.checklistTypeText?.enumName ?: ""
                        )
                    )

                    // ===================================================================== Checklist Question
                    checklistTemplate?.checklistTemplateQuestions?.forEach { question ->
                        dao.checklistQuestionInsert(
                            ChecklistQuestionEntity(
                                question?.id ?: "",
                                question?.orderNo ?: "",
                                question?.sectionTitle ?: "",
                                question?.description ?: "",
                                question?.defaultRemarks ?: "",
                                question?.helpText ?: "",
                                question?.helpImageUrl ?: "",
                                question?.createdDate ?: "",
                                question?.createdBy ?: "",
                                question?.lastModifiedDate ?: "",
                                question?.lastModifiedBy ?: "",
                                question?.checklistTemplateId ?: ""
                            )
                        )
                    }


                }
            }

            // ============================================================================= Insert Agreement
            if (it?.agreement != null) {
                dao.agreementInsert(
                    AgreementEntity(
                        it.agreement.agreementId ?: "",
                        caseID ?: "",
                        accountID ?: "",
                        accountName ?: "",
                        "",
                        regionCode ?: "",
                        it.agreement.accountContactId ?: "",
                        it.agreement.agreementTypeEnum?.enumId ?: "",
                        it.agreement.agreementTypeEnum?.enumName ?: "",
                        it.agreement.agreementNumber ?: "",
                        it.agreement.statusText?.enumId ?: "",
                        it.agreement.statusText?.enumName ?: "",
                        "",
                        "",
                        "",
                        it.agreement.installationDeliveryAddressData?.accountAddressId ?: "",
                        it.agreement.installationDeliveryAddressData?.fullAddress ?: "",
                        it.agreement.installationContactPicData?.accountContactId ?: "",
                        it.agreement.installationContactPicData?.contactPersonName ?: "",
                        it.agreement.installationContactPicData?.contactPersonPhone ?: "",
                        it.agreement.agreementParentId ?: "",
                        it.agreement.internalMemo ?: "",
                        it.agreement.noOfService ?: "",
                        it.agreement.agreementStartDate ?: "",
                        it.agreement.agreementEndDate ?: "",
                        it.agreement.installationDeliveryAddressData?.fullAddress ?: "",
                        it.agreement.installationContactPicData?.contactPersonName ?: "",
                        it.agreement.firstVisitDate ?: "",
                        it.agreement.createdDate ?: "",
                        it.createdByUser?.userFullname ?: "",
                        it.agreement.lastModifiedDate ?: "",
                        it.modifiedByUser?.userFullname ?: "",
                        it.agreement.equipments ?: emptyList()
                    )
                )
            }

            // ============================================================================= Insert Agreement Equipment
            it?.agreement?.equipments?.forEach { equipment ->
                dao.agreementEquipmentInsert(
                    AgreementEquipmentEntity(
                        equipment?.equipmentId ?: "",
                        equipment?.regionId ?: "",
                        equipment?.serialNo ?: "",
                        equipment?.brand ?: "",
                        equipment?.warrantyStart ?: "",
                        equipment?.warrantyEnd ?: "",
                        equipment?.equipmentRemarks ?: "",
                        equipment?.deliveryAddressId ?: "",
                        equipment?.salesOrderNo ?: "",
                        equipment?.warrantyStatus ?: "",
                        accountID ?: "",
                        accountName ?: "",
                        equipment?.pivot?.agreementId ?: "",
                        agreementNumber ?: "",
                        agreementEnd ?: "",
                        equipment?.statusText?.enumId ?: "",
                        equipment?.statusText?.enumName ?: "",
                        equipment?.product?.productId ?: "",
                        equipment?.product?.productName ?: "",
                    )
                )
            }

            // ============================================================================= Insert Service Call
            it?.serviceCall?.forEach { serviceCall->

                val callId = serviceCall?.serviceCallId ?: ""

                val startDate = serviceCall?.startDate ?: ""
                val startTime = serviceCall?.startTime ?: ""
                val startDateTime = if (startDate.isNotEmpty() && startTime.isNotEmpty()) {
                    "$startDate $startTime"
                } else { "" }

                val endDate = serviceCall?.endDate ?: ""
                val endTime = serviceCall?.endTime ?: ""
                val endDateTime = if (endDate.isNotEmpty() && endTime.isNotEmpty()) {
                    "$endDate $endTime"
                } else { "" }

                dao.callInsert(
                    ServiceCallEntity(
                        serviceCall?.serviceCallId ?: "",
                        serviceCall?.caseId ?: "",
                        serviceCall?.serviceCallNumber ?: "",
                        serviceCall?.serviceCallSubject ?: "",
                        serviceCall?.serviceCallDescription ?: "",
                        startDateTime,
                        endDateTime,
                        serviceCall?.salesPersonId ?: "",
                        serviceCall?.caseSalesId ?: "",
                        serviceCall?.caseAccountContactId ?: "",
                        serviceCall?.caseAccountFieldPicContacId ?: "",
                        serviceCall?.isChargeable ?: "",
                        serviceCall?.createdDate ?: "",
                        serviceCall?.lastModifiedDate ?: "",
                        "",
                        serviceCall?.region?.regionId ?: "",
                        serviceCall?.region?.regionName ?: "",
                        serviceCall?.callRemark ?: "",
                        serviceCall?.callStatusRemark ?: "",
                        serviceCall?.callStatusText?.enumId ?: "",
                        serviceCall?.callStatusText?.enumName ?: "",
                        "",
                        "",
                        "",
                        serviceCall?.callTypeText?.enumId ?: "",
                        serviceCall?.callTypeText?.enumName ?: "",
                        serviceCall?.account?.accountId ?: "",
                        serviceCall?.account?.accountName ?: "",
                        serviceCall?.createdByUser?.id ?: "",
                        serviceCall?.createdByUser?.userFullname ?: "",
                        serviceCall?.modifiedByUser?.id ?: "",
                        serviceCall?.modifiedByUser?.userFullname ?: "",
                        serviceCall?.accountSalesPic?.id ?: "",
                        serviceCall?.accountSalesPic?.userFullname ?: "",
                        serviceCall?.salesPic?.id ?: "",
                        serviceCall?.salesPic?.userFullname ?: "",
                        serviceCall?.accountContact?.accountContactId ?: "",
                        serviceCall?.accountContact?.contactPersonName ?: "",
                        serviceCall?.accountContact?.contactPersonPhone ?: "",
                        serviceCall?.accountContact?.contactPersonEmail ?: "",
                        serviceCall?.fieldAccountContact?.accountContactId ?: "",
                        serviceCall?.fieldAccountContact?.contactPersonName ?: "",
                        serviceCall?.fieldAccountContact?.contactPersonPhone ?: "",
                        serviceCall?.fieldAccountContact?.contactPersonEmail ?: "",
                        serviceCall?.leadEngineer?.id ?: "",
                        serviceCall?.leadEngineer?.userFullname ?: "",
                        serviceCall?.serviceCallEquipments ?: emptyList(),
                        serviceCall?.engineers ?: emptyList(),
                        true
                    )
                )

                // ========================================================================= Insert Service Call Training
                serviceCall?.training?.forEach { trainingData->
                    dao.callTrainingInsert(
                        ServiceCallTrainingEntity(
                            trainingData?.id ?: "",
                            trainingData?.callId ?: "",
                            trainingData?.addressId ?: "",
                            trainingData?.trainingFormNumber ?: "",
                            trainingData?.trainingType ?: "",
                            trainingData?.trainingStartDate ?: "",
                            trainingData?.trainingDetail ?: "",
                            trainingData?.engineerId ?: "",
                            trainingData?.engineerSignatureUrl ?: "",
                            trainingData?.customerPicName ?: "",
                            trainingData?.customerPicPosition ?: "",
                            trainingData?.customerSignatureUrl ?: "",
                            trainingData?.officerName ?: "",
                            trainingData?.createdDate ?: "",
                            trainingData?.createdBy ?: "",
                            trainingData?.lastModifiedDate ?: "",
                            trainingData?.lastModifiedBy ?: "",
                            trainingData?.trainingEndDate ?: "",
                            trainingData?.accountContactId ?: ""
                        )
                    )
                }

                // ========================================================================= Insert Service Call Handover
                serviceCall?.handover?.forEach { handoverData->
                    dao.callHandoverInsert(
                        ServiceCallHandoverEntity(
                            handoverData?.id ?: "",
                            handoverData?.callId ?: "",
                            handoverData?.addressId ?: "",
                            handoverData?.accountContactId ?: "",
                            handoverData?.engineerId ?: "",
                            handoverData?.handoverNumber ?: "",
                            handoverData?.handoverType ?: "",
                            handoverData?.handoverPoRefNumber ?: "",
                            handoverData?.handoverCustomerAgreed ?: "",
                            handoverData?.handoverOutstandingMatters ?: "",
                            handoverData?.startDate ?: "",
                            handoverData?.endDate ?: "",
                            handoverData?.completionDate ?: "",
                            handoverData?.supplierPicName ?: "",
                            handoverData?.supplierPicPosition ?: "",
                            handoverData?.supplierSignatureUrl ?: "",
                            handoverData?.customerPicName ?: "",
                            handoverData?.customerPicPosition ?: "",
                            handoverData?.customerSignatureUrl ?: "",
                            handoverData?.createdDate ?: "",
                            handoverData?.createdBy ?: "",
                            handoverData?.lastModifiedDate ?: "",
                            handoverData?.lastModifiedBy ?: ""
                        )
                    )
                }

                // ========================================================================= Insert Service Call Completion
                serviceCall?.installation?.forEach { completionData->
                    dao.callCompletionInsert(
                        ServiceCallCompletionEntity(
                            completionData?.id ?: "",
                            completionData?.callId ?: "",
                            completionData?.addressId ?: "",
                            completionData?.accountContactId ?: "",
                            completionData?.engineerId ?: "",
                            completionData?.handoverNumber ?: "",
                            completionData?.handoverType ?: "",
                            completionData?.handoverPoRefNumber ?: "",
                            completionData?.handoverCustomerAgreed ?: "",
                            completionData?.handoverOutstandingMatters ?: "",
                            completionData?.startDate ?: "",
                            completionData?.endDate ?: "",
                            completionData?.completionDate ?: "",
                            completionData?.supplierPicName ?: "",
                            completionData?.supplierPicPosition ?: "",
                            completionData?.supplierSignatureUrl ?: "",
                            completionData?.customerPicName ?: "",
                            completionData?.customerPicPosition ?: "",
                            completionData?.customerSignatureUrl ?: "",
                            completionData?.createdDate ?: "",
                            completionData?.createdBy ?: "",
                            completionData?.lastModifiedDate ?: "",
                            completionData?.lastModifiedBy ?: ""
                        )
                    )
                }

                // ========================================================================= Insert Service Call Equipment
                serviceCall?.serviceCallEquipments?.forEach { callEquipment ->
                    dao.callEquipmentInsert(
                        ServiceCallEquipmentEntity(
                            callEquipment?.serviceCallEquipmentId ?: "",
                            callEquipment?.callId ?: "",
                            callEquipment?.equipmentId ?: "",
                            callEquipment?.createdDate ?: "",
                            callEquipment?.createdBy ?: "",
                            callEquipment?.lastModifiedDate ?: "",
                            callEquipment?.lastModifiedBy ?: "",
                            callEquipment?.equipmentName ?: "",
                            callEquipment?.equipment?.regionId ?: "",
                            callEquipment?.equipment?.serialNo ?: "",
                            callEquipment?.equipment?.brand ?: "",
                            callEquipment?.equipment?.warrantyStart ?: "",
                            callEquipment?.equipment?.warrantyEnd ?: "",
                            callEquipment?.equipment?.equipmentRemarks ?: "",
                            callEquipment?.equipment?.deliveryAddressId ?: "",
                            callEquipment?.equipment?.salesOrderNo ?: "",
                            callEquipment?.equipment?.warrantyStatus ?: "",
                            accountID ?: "",
                            accountName ?: "",
                            callEquipment?.agreementId ?: "",
                            agreementNumber ?: "",
                            agreementEnd ?: "",
                            callEquipment?.equipment?.statusText?.enumId ?: "",
                            callEquipment?.equipment?.statusText?.enumName ?: "",
                            callEquipment?.equipment?.product?.productId ?: "",
                            callEquipment?.equipment?.product?.productName ?: "",
                            true,
                            false,
                            true,
                            false
                        )
                    )
                }

                // ========================================================================= Insert Service Report
                serviceCall?.serviceReport?.forEach { report ->

                    var fileName = report?.accountPicSignature ?: ""
                    fileName = if (fileName.isNotEmpty()) {
                        val split = fileName.split("/service-report-signature/")
                        split[1]
                    } else { "" }

                    // can add report equipment when already added in canceled report
                    val reportCanceled = report?.reportStatusText?.enumName ?: "" != "Canceled"

                    dao.reportInsert(
                        ServiceReportEntity(
                            report?.serviceReportId ?: "",
                            report?.serviceReportId ?: "",
                            caseID ?: "",
                            accountID ?: "",
                            accountName ?: "",
                            report?.callId ?: "",
                            report?.serviceReportNumber ?: "",
                            report?.reportDate ?: "",
                            report?.internalMemo ?: "",
                            report?.customerRating ?: "",
                            report?.feedback ?: "",
                            report?.cpNumber ?: "",
                            report?.cpName ?: "",
                            "",
                            report?.servicedBySignature ?: "",
                            fileName,
                            report?.accountPicSignature ?: "",
                            report?.accountSignedAt ?: "",
                            report?.isChargeable ?: "",
                            report?.employeeId ?: "",
                            report?.employeeName ?: "",
                            report?.createdDate ?: "",
                            report?.lastModifiedDate ?: "",
                            report?.serviceReportNumberTemp ?: "",
                            report?.region?.regionId ?: "",
                            report?.region?.regionName ?: "",
                            report?.fieldAccountContact?.accountContactId ?: "",
                            report?.fieldAccountContact?.contactPersonName ?: "",
                            report?.fieldAccountContact?.contactPersonPhone ?: "",
                            report?.servicedByUser?.id ?: "",
                            report?.servicedByUser?.userFullname ?: "",
                            report?.createdByUser?.id ?: "",
                            report?.createdByUser?.userFullname ?: "",
                            report?.modifiedByUser?.id ?: "",
                            report?.modifiedByUser?.userFullname ?: "",
                            report?.reportStatusText?.enumId ?: "",
                            report?.reportStatusText?.enumName ?: "",
                            serviceCall.callTypeText?.enumId ?: "",
                            serviceCall.callTypeText?.enumName ?: "",
                            "",
                            "",
                            "",
                            report?.serviceReportTimeTracking ?: emptyList(),
                            serviceCall.engineers ?: emptyList(),
                            report?.serviceReportEquipment ?: emptyList(),
                            true,
                            false
                        )
                    )

                    // ===================================================================== Insert Service Report Time tracking
                    report?.serviceReportTimeTracking?.forEach { timeTracking->
                        dao.reportTimeTrackingInsert(
                            ServiceReportTimeTrackingEntity(
                                timeTracking?.serviceReportTimeTrackingId ?: "",
                                callId,
                                timeTracking?.reportId ?: "",
                                timeTracking?.engineer?.id ?: "",
                                timeTracking?.engineer?.userFullname ?: "",
                                timeTracking?.isLead ?: "",
                                timeTracking?.startDateTime ?: "",
                                timeTracking?.endDateTime ?: "",
                                true,
                                false
                            )
                        )
                    }

                    // ===================================================================== Insert Service Report Equipment
                    report?.serviceReportEquipment?.forEach { srEquipment->
                        dao.reportEquipmentInsert(
                            ServiceReportEquipmentEntity(
                                srEquipment?.serviceReportEquipmentId ?: "",
                                callId,
                                srEquipment?.reportId ?: "",
                                srEquipment?.equipmentId ?: "",
                                srEquipment?.problemSymptom ?: "",
                                srEquipment?.statusAfterServiceText?.enumId ?: "",
                                srEquipment?.statusAfterServiceText?.enumName ?: "",
                                srEquipment?.statusAfterServiceRemarks ?: "",
                                srEquipment?.installationStartDate ?: "",
                                srEquipment?.installationMatters ?: "",
                                "1",
                                srEquipment?.createdDate ?: "",
                                srEquipment?.createdBy ?: "",
                                srEquipment?.lastModifiedDate ?: "",
                                srEquipment?.lastModifiedBy ?: "",
                                srEquipment?.equipmentName ?: "",
                                srEquipment?.equipment?.regionId ?: "",
                                srEquipment?.equipment?.serialNo ?: "",
                                srEquipment?.equipment?.brand ?: "",
                                srEquipment?.equipment?.warrantyStart ?: "",
                                srEquipment?.equipment?.warrantyEnd ?: "",
                                srEquipment?.equipment?.equipmentRemarks ?: "",
                                srEquipment?.equipment?.deliveryAddressId ?: "",
                                srEquipment?.equipment?.salesOrderNo ?: "",
                                srEquipment?.equipment?.warrantyStatus ?: "",
                                accountID ?: "",
                                accountName ?: "",
                                srEquipment?.equipment?.currentAgreementId ?: "",
                                agreementNumber ?: "",
                                agreementEnd ?: "",
                                srEquipment?.equipment?.statusText?.enumId ?: "",
                                srEquipment?.equipment?.statusText?.enumName ?: "",
                                srEquipment?.equipment?.product?.productId ?: "",
                                srEquipment?.equipment?.product?.productName ?: "",
                                reportCanceled ?: true,
                                true,
                                false
                            )
                        )

                        // ================================================================= Problem Image
                        val problemImage = ArrayList<ConverterImageModel>()
                        srEquipment?.equipmentImage?.forEach { problem->
                            if (problem != null) {
                                problemImage.add(
                                    ConverterImageModel(
                                        "",
                                        problem.imageUrl ?: ""
                                    )
                                )
                            }
                        }

                        dao.problemInsert(
                            ProblemEntity(
                                srEquipment?.serviceReportEquipmentId ?: "",
                                problemImage
                            )
                        )


                        // ================================================================= Cuse & Solution
                        srEquipment?.equipmentSolution?.forEach { solution->

                            val causeImage = ArrayList<ConverterImageModel>()
                            val solutionImage = ArrayList<ConverterImageModel>()

                            solution?.causeImage?.forEach { dataImage->
                                causeImage.add(
                                    ConverterImageModel(
                                        "",
                                        dataImage?.imageUrl ?: ""
                                    )
                                )
                            }

                            solution?.solutionImage?.forEach { dataImage->
                                solutionImage.add(
                                    ConverterImageModel(
                                        "",
                                        dataImage?.imageUrl ?: ""
                                    )
                                )
                            }

                            dao.solutionInsert(
                                SolutionEntity(
                                    solution?.serviceReportEquipmentSolutionId ?: "",
                                    solution?.reportEquipmentId ?: "",
                                    solution?.subject ?: "",
                                    solution?.problemCause ?: "",
                                    solution?.solution ?: "",
                                    solution?.createdDate ?: "",
                                    solution?.createdBy ?: "",
                                    solution?.lastModifiedDate ?: "",
                                    solution?.lastModifiedBy ?: "",
                                    causeImage,
                                    solutionImage,
                                    true,
                                    false
                                )
                            )
                        }

                        // ================================================================= Insert Service Report Checklist
                        srEquipment?.equipmentChecklist?.forEach { template->
                            val questionImage = ArrayList<ConverterImageModel>()
                            template?.checklistImage?.forEach { image->
                                questionImage.add(
                                    ConverterImageModel(
                                        "",
                                        image?.imageUrl ?: ""
                                    )
                                )
                            }

                            if (template?.checklistTemplateQuestion != null) {
                                dao.reportChecklistInsert(
                                    ServiceReportEquipmentChecklistEntity(
                                        template.serviceReportEquipmentChecklistId ?: "",
                                        template.reportEquipmentId ?: "",
                                        template.description ?: "",
                                        template.remarks ?: "",
                                        template.createdDate ?: "",
                                        template.createdBy ?: "",
                                        template.lastModifiedDate ?: "",
                                        template.lastModifiedBy ?: "",
                                        template.statusText?.enumId ?: "",
                                        template.statusText?.enumName ?: "",
                                        template.checklistTemplateQuestion.id ?: "",
                                        template.checklistTemplateQuestion.orderNo ?: "",
                                        template.checklistTemplateQuestion.sectionTitle
                                            ?: "",
                                        template.checklistTemplateQuestion.description
                                            ?: "",
                                        template.checklistTemplateQuestion.defaultRemarks
                                            ?: "",
                                        template.checklistTemplateQuestion.helpText ?: "",
                                        template.checklistTemplateQuestion.helpImageUrl
                                            ?: "",
                                        template.checklistTemplateQuestion.checklistTemplateId
                                            ?: "",
                                        template.checklistTemplateQuestion.productGroupChecklistTemplate?.productGroupChecklistTemplateId
                                            ?: "",
                                        template.checklistTemplateQuestion.productGroupChecklistTemplate?.regionId
                                            ?: "",
                                        template.checklistTemplateQuestion.productGroupChecklistTemplate?.productGroupId
                                            ?: "",
                                        template.checklistTemplateQuestion.productGroupChecklistTemplate?.checklistType
                                            ?: "",
                                        template.checklistTemplateQuestion.productGroupChecklistTemplate?.description
                                            ?: "",
                                        questionImage,
                                        false
                                    )
                                )
                            }

                        }

                        // ================================================================= Insert Service Report Sparepart
                        srEquipment?.serviceReportSparepart?.forEach { sparepart->
                            dao.reportSparepartInsert(
                                ServiceReportSparepartEntity(
                                    sparepart?.serviceReportSparepartId ?: "",
                                    sparepart?.reportEquipmentId ?: "",
                                    sparepart?.sparePartImageNumber ?: "",
                                    sparepart?.sparePartNumber ?: "",
                                    sparepart?.sparePartDescription ?: "",
                                    sparepart?.sparePartMemo ?: "",
                                    sparepart?.sparePartTypeText?.enumId ?: "",
                                    sparepart?.sparePartTypeText?.enumName ?: "",
                                    sparepart?.sparepart?.partId ?: "",
                                    sparepart?.sparepart?.partNo ?: "",
                                    sparepart?.sparepart?.partName ?: "",
                                    sparepart?.qty ?: "",
                                    true,
                                    false
                                )
                            )
                        }
                    }
                }
            }
        }

    }

}