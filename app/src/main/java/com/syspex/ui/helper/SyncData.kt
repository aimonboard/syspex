package com.syspex.ui.helper

import android.content.Context
import android.os.Handler
import android.os.Looper
import android.util.Log
import android.widget.Toast
import com.syspex.Menu2
import com.syspex.data.local.SharedPreferenceData
import com.syspex.data.local.database.AppDatabase
import com.syspex.data.local.database.account.AccountEntity
import com.syspex.data.local.database.account_address.AccountAddressEntity
import com.syspex.data.local.database.account_attachment.AccountAttachmentEntity
import com.syspex.data.local.database.account_contact.AccountContactEntity
import com.syspex.data.local.database.account_equipment.AccountEquipmentEntity
import com.syspex.data.local.database.activity.ActivityEntity
import com.syspex.data.local.database.activity_type.ActivityTypeEntity
import com.syspex.data.local.database.agreement.AgreementEntity
import com.syspex.data.local.database.agreement_equipment.AgreementEquipmentEntity
import com.syspex.data.local.database.chargeable_type.ChargeableEntity
import com.syspex.data.local.database.global_color.ColorEntity
import com.syspex.data.local.database.global_enum.EnumEntity
import com.syspex.data.local.database.pcs_problem.ProblemEntity
import com.syspex.data.local.database.pcs_solution.SolutionEntity
import com.syspex.data.local.database.product_group.ProductEntity
import com.syspex.data.local.database.product_checklist_question.ChecklistQuestionEntity
import com.syspex.data.local.database.product_checklist_template.ChecklistTemplateEntity
import com.syspex.data.local.database.product_document.ProductDocumentEntity
import com.syspex.data.local.database.product_group_item.ProductGroupItemEntity
import com.syspex.data.local.database.product_sparepart.SparepartEntity
import com.syspex.data.local.database.service_call.ServiceCallEntity
import com.syspex.data.local.database.service_call_completion.ServiceCallCompletionEntity
import com.syspex.data.local.database.service_call_equipment.ServiceCallEquipmentEntity
import com.syspex.data.local.database.service_call_handover.ServiceCallHandoverEntity
import com.syspex.data.local.database.service_call_training.ServiceCallTrainingEntity
import com.syspex.data.local.database.service_case.ServiceCaseEntity
import com.syspex.data.local.database.service_case_equipment.ServiceCaseEquipmentEntity
import com.syspex.data.local.database.service_report.ServiceReportEntity
import com.syspex.data.local.database.service_report_equipment.ServiceReportEquipmentEntity
import com.syspex.data.local.database.service_report_equipment_checklist.ServiceReportEquipmentChecklistEntity
import com.syspex.data.local.database.service_report_equipment_checklist_question.ServiceReportEquipmentChecklistQuestionEntity
import com.syspex.data.local.database.service_report_equipment_checklist_template.ServiceReportEquipmentChecklistTemplateEntity
import com.syspex.data.local.database.service_report_sparepart.ServiceReportSparepartEntity
import com.syspex.data.local.database.service_report_time_tracking.ServiceReportTimeTrackingEntity
import com.syspex.data.model.ConverterImageModel
import com.syspex.data.remote.*
import com.syspex.data.retrofit.ApiClient
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList
import kotlin.math.ceil
import kotlin.math.floor

class SyncData (
    private val context: Context,
    private val onlyMyCall: String,
    private val firstSync: String,
    private val fromDate: String,
    private val toDate: String,
    private val region: String,
) {

    private val token = "Bearer "+SharedPreferenceData.getString(context,2, "")
    private val userId = SharedPreferenceData.getString(context,8, "")
    private val dao = AppDatabase.getInstance(context)!!.dao()

    private var stop = false
    private var counter = 1
    private var pagination = 10
    private var lastPage = 0
    private var currentPage = 1
    private var responseCount = 0

    // ============================================================================================= Enum
    fun getEnum() {
        ApiClient.instance.getEnum(token).enqueue(object: Callback<EnumRequest> {
            override fun onFailure(call: Call<EnumRequest>, t: Throwable) {
                try { (context as Menu2).syncLoading(false) }
                catch (e: Exception) { Log.e("aim", "Err :$e") }
                Log.e("aim","enum err : $t")
                Toast.makeText(context,"Connection failed", Toast.LENGTH_LONG).show()
            }
            override fun onResponse(call: Call<EnumRequest>, response: Response<EnumRequest>) {
                if (!response.isSuccessful) {
                    if (response.code() == 401) {
                        FunHelper.clearAllData(context)
                        Toast.makeText(context,"Session end", Toast.LENGTH_LONG).show()
                    } else {
                        FunHelper.firebaseApiLog(
                            userId ?: "",
                            response.raw().request().url().toString() ?: "",
                            response.raw().request().headers().toString() ?: "",
                            "",
                            response.raw().toString() ?: ""
                        )
                    }
                }
                else {

                    getColor()
                    getChargeableType()

                    getActivityType()

                    val thread = object : Thread() {
                        override fun run() {
                            val caseFilter = ArrayList<String>()
                            val callFilter = ArrayList<String>()
                            val reportFilter = ArrayList<String>()
                            val agreementFilter = ArrayList<String>()
                            val equipmentFilter = ArrayList<String>()

                            response.body()?.data?.enumList?.forEach { enumData->
                                dao.enumInsert(EnumEntity(
                                    enumData?.enumId ?: "",
                                    enumData?.enumSlug ?: "",
                                    enumData?.enumName ?: "",
                                    enumData?.enumTable ?: "",
                                    enumData?.enumColumn ?: "",
                                    enumData?.createdAt ?: "",
                                    enumData?.updatedAt ?: ""
                                ))

                                when (enumData?.enumColumn ?: "") {
                                    "case_status" -> caseFilter.add(enumData?.enumId ?: "")
                                    "call_status" -> callFilter.add(enumData?.enumId ?: "")
                                    "report_status" -> reportFilter.add(enumData?.enumId ?: "")
                                    "agreement_status" -> agreementFilter.add(enumData?.enumId ?: "")
                                    "equipment_status" -> equipmentFilter.add(enumData?.enumId ?: "")
                                }
                            }

                            setFilter(caseFilter,callFilter,reportFilter,agreementFilter,equipmentFilter)

                        }
                    }
                    thread.start()
                }
            }

        })
    }

    // ============================================================================================= Set Filter
    private fun setFilter(
        case: ArrayList<String>,
        call: ArrayList<String>,
        report: ArrayList<String>,
        agreement: ArrayList<String>,
        equipment: ArrayList<String>
    ) {

        if (SharedPreferenceData.getArrayString(context,9991, emptySet()).isEmpty())
            SharedPreferenceData.setArrayString(context,9991, case.toSet())

        if (SharedPreferenceData.getArrayString(context,9992, emptySet()).isEmpty())
            SharedPreferenceData.setArrayString(context,9992, call.toSet())

        if (SharedPreferenceData.getArrayString(context,9993, emptySet()).isEmpty())
            SharedPreferenceData.setArrayString(context,9993, report.toSet())

        if (SharedPreferenceData.getArrayString(context,9994, emptySet()).isEmpty())
            SharedPreferenceData.setArrayString(context,9994, agreement.toSet())

        if (SharedPreferenceData.getArrayString(context,9995, emptySet()).isEmpty())
            SharedPreferenceData.setArrayString(context,9995, equipment.toSet())
    }

    // ============================================================================================= Color
    private fun getColor() {
        ApiClient.instance.getColor(token).enqueue(object: Callback<ColorRequest> {
            override fun onFailure(call: Call<ColorRequest>, t: Throwable) { }
            override fun onResponse(call: Call<ColorRequest>, response: Response<ColorRequest>) {
                if (response.isSuccessful) {
                    val thread = object : Thread() {
                        override fun run() {
                            response.body()?.data?.statusColor?.forEachIndexed { index, it->
                                dao.colorInsert(ColorEntity(
                                    index+1,
                                    it?.table ?: "",
                                    it?.bgColor ?: "",
                                    "",
                                    "",
                                    it?.status ?: ""
                                ))
                            }
                        }
                    }
                    thread.start()
                } else {
                    if (response.code() == 401) {
                        FunHelper.clearAllData(context)
                        Toast.makeText(context,"Session end", Toast.LENGTH_LONG).show()
                    } else {
                        FunHelper.firebaseApiLog(
                            userId ?: "",
                            response.raw().request().url().toString() ?: "",
                            response.raw().request().headers().toString() ?: "",
                            "",
                            response.raw().toString() ?: ""
                        )
                    }
                }
            }
        })
    }

    // ============================================================================================= Chargeable Type
    private fun getChargeableType() {
        ApiClient.instance.getChargeable(token).enqueue(object: Callback<ChargeableRequest> {
            override fun onFailure(call: Call<ChargeableRequest>, t: Throwable) { }
            override fun onResponse(call: Call<ChargeableRequest>, response: Response<ChargeableRequest>) {
                if (response.isSuccessful) {
                    val thread = object : Thread() {
                        override fun run() {
                            response.body()?.data?.forEach { it->
                                dao.chargeableInsert(ChargeableEntity(
                                    it?.key ?: "",
                                    it?.value ?: "",
                                    it?.isShow ?: ""
                                ))
                            }
                        }
                    }
                    thread.start()
                } else {
                    if (response.code() == 401) {
                        FunHelper.clearAllData(context)
                        Toast.makeText(context,"Session end", Toast.LENGTH_LONG).show()
                    } else {
                        FunHelper.firebaseApiLog(
                            userId ?: "",
                            response.raw().request().url().toString() ?: "",
                            response.raw().request().headers().toString() ?: "",
                            "",
                            response.raw().toString() ?: ""
                        )
                    }
                }
            }
        })
    }

    // ============================================================================================= Activity Type & start sync
    private fun getActivityType() {
        ApiClient.instance.getActivityType(token).enqueue(object: Callback<GetActivityTypeRequest> {
            override fun onFailure(call: Call<GetActivityTypeRequest>, t: Throwable) { }
            override fun onResponse(call: Call<GetActivityTypeRequest>, response: Response<GetActivityTypeRequest>) {
                if (response.isSuccessful) {
                    for (i in 1..pagination) {
                        getData(i)
                    }

                    val thread = object : Thread() {
                        override fun run() {
                            response.body()?.data?.activityType?.forEach {
                                dao.activityTypeInsert(ActivityTypeEntity(
                                    it?.activityTypeId ?: "",
                                    it?.activityTypeCode ?: "",
                                    it?.activityTypeName ?: "",
                                    it?.activityTypeColor ?: ""
                                ))
                            }
                        }
                    }
                    thread.start()
                } else {
                    if (response.code() == 401) {
                        FunHelper.clearAllData(context)
                        Toast.makeText(context,"Session end", Toast.LENGTH_LONG).show()
                    } else {
                        FunHelper.firebaseApiLog(
                            userId ?: "",
                            response.raw().request().url().toString() ?: "",
                            response.raw().request().headers().toString() ?: "",
                            "",
                            response.raw().toString() ?: ""
                        )
                    }
                }
            }
        })
    }

    // ============================================================================================= Sync Data
    private fun getData(loadPage: Int) {

        Log.e("aim", "parsing start page $loadPage = ${Date()}")
        Log.e("aim", "======= sync param =======\n" +
                "token = $token,\n" +
                "page = $loadPage,\n" +
                "onlyMyCall = $onlyMyCall,\n" +
                "firstSync = $firstSync,\n" +
                "fromDate = $fromDate,\n" +
                "toDate = $toDate,\n" +
                "regionId = $region")

        ApiClient.instance.getData(
            token = token,
            page = loadPage,
            onlyMyCall = onlyMyCall,
            firstSync = firstSync,
            fromDate = fromDate,
            toDate = toDate,
            regionId = region
        ).enqueue(object: Callback<GetDataRequest> {
            override fun onFailure(call: Call<GetDataRequest>, t: Throwable) {
                responseCount++
            }
            override fun onResponse(call: Call<GetDataRequest>, response: Response<GetDataRequest>) {
                responseCount++

                if (response.isSuccessful) {
                    currentPage = response.body()?.data?.page ?: 1
                    lastPage = response.body()?.data?.lastPage ?: 1

//                    Log.e("aim", "parsing end page $loadPage = ${Date()}")
//                    iterationHandling(page, lastPage)

                    shouldCallMore()
                    prepareInsertDatabase(response)

                } else {
                    if (response.code() == 401) {
                        FunHelper.clearAllData(context)
                        Toast.makeText(context,"Session end", Toast.LENGTH_LONG).show()
                    } else {
                        FunHelper.firebaseApiLog(
                            userId ?: "",
                            response.raw().request().url().toString() ?: "",
                            response.raw().request().headers().toString() ?: "",
                            "",
                            response.raw().toString() ?: ""
                        )
                    }
                }
            }
        })
    }

    private fun prepareInsertDatabase(response: Response<GetDataRequest>) {
        val thread = object : Thread() {
            override fun run() {

                ReplaceDataBase(context).insertAll(dao, response)

//                Log.e("aim", "start database page $currentPage = ${Date()}")
//                Log.e("aim", "end database page $currentPage = ${Date()}")

                if (currentPage == lastPage) {
                    dao.reportSparepartDeleteLocal(true)
                    dao.reportChecklistDeleteLocal(true)
                    dao.solutionDeleteLocal(true)
                    dao.reportTimeTrackingDeleteLocal(true)
                    dao.reportEquipmentDeleteLocal(true)
                    dao.reportDeleteLocal(true)
                    dao.activityDeleteLocal(true)
                }

            }
        }
        thread.start()
    }

//    private fun iterationHandling(page: Int, lastPage: Int) {
//        when {
//            page < lastPage -> {
//                SharedPreferenceData.setInt(context, 4, page+1)
//                SharedPreferenceData.setInt(context, 5, lastPage)
//                getData(page+1)
//                try { (context as Menu2).syncIteration(page, lastPage) }
//                catch (e: Exception) { Log.e("aim", "Err :$e") }
//            }
//            page == lastPage -> {
//                val sdf = SimpleDateFormat("dd-MM-yyyy HH:mm", Locale.getDefault())
//                SharedPreferenceData.setInt(context, 4, 1)
//                SharedPreferenceData.setString(context, 6, sdf.format(Date()))
//                try { (context as Menu2).syncLoading(false) }
//                catch (e: Exception) { Log.e("aim", "Err :$e") }
//            }
//            page > lastPage -> {
//                try { (context as Menu2).syncLoading(false) }
//                catch (e: Exception) { Log.e("aim", "Err :$e") }
//            }
//        }
//    }

    private fun shouldCallMore() {
        Log.e("aim","Load more, page: $currentPage last: $lastPage")

        val totalCounter = ceil(lastPage.toDouble()/pagination)
        if (counter >= totalCounter){
            stop = true
        }

        if (lastPage == 0) {
            try { (context as Menu2).syncLoading(false) }
            catch (e: Exception) { Log.e("aim", "Err :$e") }
        } else if (!stop) {

            for (i in 1..pagination) {
                if ((counter * pagination) + i <= lastPage) {
                    getData((counter * pagination) + i)
                }
            }

            counter++

        } else {
            if (responseCount == lastPage) {
                Handler(Looper.getMainLooper()).postDelayed({
                    try {
                        // Update last sync parameter
                        SharedPreferenceData.setString(context, 6, FunHelper.getDate(true))
                        (context as Menu2).syncLoading(false)
                    }
                    catch (e: Exception) { Log.e("aim", "Err :$e") }
                }, 1000*5)
            }
        }
    }

}