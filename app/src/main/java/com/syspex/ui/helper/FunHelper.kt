package com.syspex.ui.helper

import android.Manifest
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.util.Log
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.AutoCompleteTextView
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.LinearSnapHelper
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.snackbar.Snackbar
import com.google.firebase.database.FirebaseDatabase
import com.syspex.MainActivity
import com.syspex.data.local.SharedPreferenceData
import com.syspex.data.local.database.AppDatabase
import com.syspex.data.model.ApiLogModel
import java.text.SimpleDateFormat
import java.util.*

class FunHelper {

    companion object {

        const val BASE_URL = "https://ssa.syspex.com/"
//        const val BASE_URL = "https://syspex-env.eba-fwz33irp.ap-southeast-1.elasticbeanstalk.com/"

        const val storageWritePermission = Manifest.permission.WRITE_EXTERNAL_STORAGE
        const val storageReadPermission = Manifest.permission.READ_EXTERNAL_STORAGE
        const val cameraPermission = Manifest.permission.CAMERA

        const val scanRequest = 11111
        const val alertReportRequest = 1234

        fun autoText (context: Context, text_view: AutoCompleteTextView, data: ArrayList<String>, listener: AutoTextInterface) {
            val adapter = ArrayAdapter(context, android.R.layout.simple_dropdown_item_1line,data)
            text_view.setAdapter(adapter)
            text_view.threshold = 1
            text_view.onItemClickListener = AdapterView.OnItemClickListener{parent,view,position,id->
                val text = parent.getItemAtPosition(position).toString()
                listener.isClicked(text)
            }
            text_view.onFocusChangeListener = View.OnFocusChangeListener{view, b ->
                if(b) { text_view.showDropDown() }
            }
        }

        fun phoneCall (context: Context, phone: String) {
            try {
                var data = phone.replace("(","").replace(")","")
                data = data.replace(" ","").replace("-","")
                if (data.isNotEmpty()) {
                    val intent = Intent(Intent.ACTION_DIAL)
                    intent.data = Uri.parse("tel:$data")
                    context.startActivity(intent)
                } else {
                    Toast.makeText(context,"Data not available",Toast.LENGTH_LONG).show()
                }
            }catch (e: Exception) {
                Toast.makeText(context,"Data not available",Toast.LENGTH_LONG).show()
            }
        }

        fun direction (context: Context, address: String) {
            if (address.isNotEmpty() && address != "-") {
                val map = "http://maps.google.co.in/maps?q=$address"
                val i = Intent(Intent.ACTION_VIEW, Uri.parse(map))
                context.startActivity(i)
            } else {
                Toast.makeText(context,"Data not available",Toast.LENGTH_LONG).show()
            }
        }

        fun snackBar(view: View?, message: String) {
            if (view != null) {
                Snackbar.make(view, message, Snackbar.LENGTH_LONG).show()
            }
        }

        fun randomString (section: String): String {
            val rnd = Random()
            val number = rnd.nextInt(999999)

            String.format("%06d", number)

            when (section) {
                "report" -> "SR-${String.format("%06d", number)}"
                "file" -> "SR-${String.format("%06d", number)}"
            }

            return UUID.randomUUID().toString()
        }

        fun reportNumber(context: Context, callNumber: String, userId: String): String {
            val increment = SharedPreferenceData.getInt(context, 15, 1)
            SharedPreferenceData.setInt(context, 15, increment+1)
            return "$callNumber-$userId-$increment"
        }

        fun setUpAdapter(context: Context, recycler: RecyclerView): RecyclerView {
            val isVerticalList = SharedPreferenceData.getBoolean(context,7,false)

            recycler.layoutManager = if (isVerticalList) {
                    LinearLayoutManager(context, RecyclerView.VERTICAL, false)
                } else {
                    LinearLayoutManager(context, RecyclerView.HORIZONTAL,false)
                }
            if (recycler.onFlingListener == null && !isVerticalList) {
                val snapHelper = LinearSnapHelper()
                snapHelper.attachToRecyclerView(recycler)
            }

            return recycler
        }

        fun getDate(onlyDate: Boolean): String {
            return if (onlyDate) {
                val sdf = SimpleDateFormat("yyyy-MM-dd", Locale.getDefault())
                sdf.format(Date())
            } else {
                val sdf = SimpleDateFormat("yyyy-MM-dd hh:mm", Locale.getDefault())
                sdf.format(Date())
            }

        }

        // dd MMM yyyy HH:mm
        fun uiDateFormat(data: String): String {
            return try {
                when {
                    data.length == 10 -> {
                        val date = SimpleDateFormat("yyyy-MM-dd", Locale.getDefault()).parse(data)
                        val sdf = SimpleDateFormat("dd MMM yyyy", Locale.getDefault())
                        sdf.format(date ?: "-")
                    }
                    data.length > 10 -> {
                        val date = SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault()).parse(data)
                        val sdf = SimpleDateFormat("dd MMM yyyy HH:mm", Locale.getDefault())
                        sdf.format(date ?: "-")
                    }
                    else -> {
                        ""
                    }
                }
            } catch (e: Exception) {
                data
            }
        }

//        fun syncData(context: Context) {
//            UploadFile(context).startUploadFile()
//            SendData(context).getParam()
//        }

        fun clearAllData(context: Context) {
            val appDatabase = AppDatabase.getInstance(context)!!
            val thread = object : Thread() {
                override fun run() {
                    appDatabase.clearAllTables()
                }
            }
            thread.start()

            SharedPreferenceData.setString(context, 1, "0")
            SharedPreferenceData.setString(context, 2, "")
            SharedPreferenceData.setString(context, 3, "0")
            SharedPreferenceData.setInt(context, 4, 1)

            val intent = Intent(context, MainActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_NEW_TASK)
            context.startActivity(intent)
        }

        fun firebaseApiLog(userId: String, requestUrl: String, requestHeader: String, requestBody: String, response: String) {
            val uiFormat = SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault())
            val key = "${uiFormat.format(Date())} - $userId"

            val model = ApiLogModel(
                requestUrl ?: "",
                requestHeader ?: "",
                requestBody ?: "",
                response ?: ""
            )

            FirebaseDatabase.getInstance().reference.child("Api Log").child(key).setValue(model)
                .addOnCompleteListener {
                    Log.e("aim","firebase berhasil")
                }.addOnFailureListener {
                    Log.e("aim","firebase error : $it")
                }
        }

    }

    interface AutoTextInterface {
        fun isClicked(data: String)
    }

}