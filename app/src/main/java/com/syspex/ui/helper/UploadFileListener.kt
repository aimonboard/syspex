package com.syspex.ui.helper

import android.content.Context
import android.util.Log
import com.syspex.data.local.database.AppDatabase
import net.gotev.uploadservice.data.UploadInfo
import net.gotev.uploadservice.exceptions.UploadError
import net.gotev.uploadservice.exceptions.UserCancelledUploadException
import net.gotev.uploadservice.network.ServerResponse
import net.gotev.uploadservice.observer.request.RequestObserverDelegate

class UploadFileListener : RequestObserverDelegate {
    override fun onProgress(context: Context, uploadInfo: UploadInfo) {
        Log.d("aim", "Progress: $uploadInfo")
    }

    override fun onSuccess(context: Context, uploadInfo: UploadInfo, serverResponse: ServerResponse) {
        try {
            val database = AppDatabase.getInstance(context)
            if (database != null) {
                val dao = database.dao()

                val thread = object : Thread() {
                    override fun run() {
                        dao.fileDeleteByName(uploadInfo.uploadId)
                    }
                }
                thread.start()
            }
        } catch (e: Exception) {
            Log.e("aim", "Save DB : $e")
        }
        Log.d("aim", "Success: $serverResponse")
    }

    override fun onError(context: Context, uploadInfo: UploadInfo, exception: Throwable) {
        when (exception) {
            is UserCancelledUploadException -> {
                Log.d("aim", "Error, user cancelled upload: $uploadInfo")
            }

            is UploadError -> {
                Log.d("aim", "Error, upload error: ${exception.serverResponse}")
            }

            else -> {
                Log.d("aim", "Error: $uploadInfo", exception)

            }
        }
    }

    override fun onCompleted(context: Context, uploadInfo: UploadInfo) {
        Log.d("aim", "Completed: $uploadInfo")
    }

    override fun onCompletedWhileNotObserving() {
        Log.d("aim", "Completed while not observing")
    }
}