package com.syspex.ui.helper

import android.app.ProgressDialog
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Environment
import android.util.Base64
import android.util.Log
import android.widget.Toast
import androidx.core.content.FileProvider
import com.syspex.data.local.SharedPreferenceData
import com.syspex.data.local.database.AppDatabase
import com.syspex.data.local.database.file.FileEntity
import com.syspex.data.retrofit.ApiClient
import com.syspex.ui.ReportPdfOffline
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.*

class DownloadPdf(val context: Context, val fileUrl: String) {

    val dao = AppDatabase.getInstance(context)!!.dao()

    fun downloadNow(isReportPdf: Boolean) {
        val progressDialog = ProgressDialog(context)
        progressDialog.setMessage("Download Pdf ...")
        progressDialog.setCancelable(false)
        progressDialog.show()

        ApiClient.instance.download(fileUrl).enqueue(object : Callback<ResponseBody> {
            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                progressDialog.dismiss()
                if (response.isSuccessful) {
                    savePdf(response.body()!!)
                } else {
                    Toast.makeText(context, response.raw().toString(), Toast.LENGTH_LONG).show()
                }
            }

            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                progressDialog.dismiss()
                showOldPdf(isReportPdf)
            }

        })
    }

    // for training, handover, installation form
    fun downloadWithToken() {
        val token = "Bearer "+ SharedPreferenceData.getString(context,2, "")

        val progressDialog = ProgressDialog(context)
        progressDialog.setMessage("Download Pdf ...")
        progressDialog.setCancelable(false)
        progressDialog.show()

        ApiClient.instance.downloadWithToken(token, fileUrl).enqueue(object : Callback<ResponseBody> {
            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                progressDialog.dismiss()
                if (response.isSuccessful) {
                    savePdf(response.body()!!)
                } else {
                    Toast.makeText(context, response.raw().toString(), Toast.LENGTH_LONG).show()
                }
            }

            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                progressDialog.dismiss()
                showOldPdf(false)
            }

        })
    }

    private fun savePdf(body: ResponseBody) {
        val fileNameArray = fileUrl.split("/")
        val index = fileNameArray.lastIndex
        val fileName = if (fileNameArray[index].contains("pdf")) {
            fileNameArray[index]
        } else {
            "${fileNameArray[index]}.pdf"
        }

        val saveDirectory = context.getExternalFilesDir(Environment.DIRECTORY_PICTURES)
        val dataFile = File(saveDirectory, fileName)

        var inputStream: InputStream? = null
        var outputStream: OutputStream? = null

        try {
            val fileReader = ByteArray(4096)
            val fileSize: Long = body.contentLength()
            var fileSizeDownloaded: Long = 0
            inputStream = body.byteStream()
            outputStream = FileOutputStream(dataFile)
            while (true) {
                val read = inputStream.read(fileReader)
                if (read == -1) {
                    break
                }
                outputStream.write(fileReader, 0, read)
                fileSizeDownloaded += read.toLong()
                Log.d("aim", "$fileSizeDownloaded of $fileSize")
            }
            outputStream.flush()

            val fileUri = FileProvider.getUriForFile(
                context,
                "com.syspex.fileprovider",
                dataFile
            )
            Log.e("aim", "file uri : $fileUri")

            val thread = object : Thread() {
                override fun run() {
                    dao.fileInsert(
                        FileEntity(
                            0,
                            fileUri.toString() ?: "",
                            fileUrl ?: "",
                            "pdf"
                        )
                    )
                }
            }
            thread.start()

            val intent = Intent(Intent.ACTION_VIEW)
            intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION)
            intent.setDataAndType(fileUri, "application/pdf")
            context.startActivity(intent)

        } catch (e: java.lang.Exception) {
            Log.e("aim", "Exception occurred " + e.message)
        } finally {
            inputStream?.close()
            outputStream?.close()
        }
    }

    private fun showOldPdf(isReportPdf: Boolean) {
        val thread = object : Thread() {
            override fun run() {
                val fileData = dao.fileGetByNameThread(fileUrl)
                if (fileData.isNotEmpty()) {
                    val fileUri = Uri.parse(fileData.first().uri)

                    val intent = Intent(Intent.ACTION_VIEW)
                    intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION)
                    intent.setDataAndType(fileUri, "application/pdf")
                    context.startActivity(intent)
                }
                else {
                    if (isReportPdf) {
                        // Report pdf offline
                        val reportId = fileUrl.split("/")

                        val i = Intent(context,ReportPdfOffline::class.java)
                        i.putExtra("reportId",reportId[reportId.lastIndex])
                        context.startActivity(i)
                    }
                }
            }
        }
        thread.start()
    }
}