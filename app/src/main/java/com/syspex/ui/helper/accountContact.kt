package com.syspex.ui.helper


import com.google.gson.annotations.SerializedName

data class accountContact(
    @SerializedName("installation_contact_pic_data")
    val installationContactPicData: InstallationContactPicData? = InstallationContactPicData()
) {
    data class InstallationContactPicData(
        @SerializedName("account_contact_id")
        val accountContactId: Int? = 0,
        @SerializedName("region_id")
        val regionId: Int? = 0,
        @SerializedName("customer_id")
        val customerId: Int? = 0,
        @SerializedName("contact_person_name")
        val contactPersonName: String? = "",
        @SerializedName("contact_person_phone")
        val contactPersonPhone: String? = "",
        @SerializedName("contact_person_email")
        val contactPersonEmail: Any? = Any(),
        @SerializedName("contact_person_address")
        val contactPersonAddress: Any? = Any(),
        @SerializedName("contact_person_position")
        val contactPersonPosition: String? = "",
        @SerializedName("contact_type")
        val contactType: Int? = 0,
        @SerializedName("priority")
        val priority: Int? = 0,
        @SerializedName("created_date")
        val createdDate: String? = "",
        @SerializedName("created_by")
        val createdBy: Any? = Any(),
        @SerializedName("last_modified_date")
        val lastModifiedDate: String? = "",
        @SerializedName("last_modified_by")
        val lastModifiedBy: Any? = Any()
    )
}