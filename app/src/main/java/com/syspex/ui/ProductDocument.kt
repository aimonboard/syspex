package com.syspex.ui

import android.app.ProgressDialog
import android.content.ContentResolver
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.os.Environment
import android.util.Log
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContentProviderCompat.requireContext
import androidx.core.content.FileProvider
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.gms.common.util.IOUtils
import com.livinglifetechway.quickpermissions_kotlin.runWithPermissions
import com.syspex.R
import com.syspex.data.local.database.file.FileEntity
import com.syspex.data.local.database.file.FileViewModel
import com.syspex.data.local.database.product_document.ProductDocumentViewModel
import com.syspex.data.local.database.product_group_item.ProductGroupItemViewModel
import com.syspex.data.retrofit.ApiClient
import com.syspex.ui.adapter.TechnicalDocumentAdapter
import com.syspex.ui.helper.DownloadPdf
import com.syspex.ui.helper.FunHelper
import kotlinx.android.synthetic.main.activity_product_document.*
import kotlinx.android.synthetic.main.sub_header.*
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.File
import java.io.FileOutputStream
import java.io.InputStream
import java.io.OutputStream


class ProductDocument : AppCompatActivity() {

    private lateinit var productGroupItemVM: ProductGroupItemViewModel
    private lateinit var documentVM: ProductDocumentViewModel
    private lateinit var fileVM: FileViewModel

    private val adapter = TechnicalDocumentAdapter()
    private var productId = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_product_document)

        productGroupItemVM = ViewModelProviders.of(this).get(ProductGroupItemViewModel::class.java)
        documentVM = ViewModelProviders.of(this).get(ProductDocumentViewModel::class.java)
        fileVM = ViewModelProviders.of(this).get(FileViewModel::class.java)

        header_name.text = "Manual Document"

        setupAdapter()
        generateView()
        buttonListener()
        adapterListener()

    }

    private fun generateView() {
        if (intent.hasExtra("productId")) {
            productId = intent.getStringExtra("productId") ?: ""
            Log.e("aim", "productId : $productId")

            documentObserver()

        }
    }

    private fun buttonListener() {
        back_link.setOnClickListener {
            onBackPressed()
        }
    }

    private fun setupAdapter() {
        recycler_document?.layoutManager = LinearLayoutManager(this, RecyclerView.VERTICAL, false)
        recycler_document?.adapter = adapter
    }

    private fun adapterListener() {
        adapter.setEventHandler(object : TechnicalDocumentAdapter.RecyclerClickListener {
            override fun isClicked(fileUrl: String) {
                showPdf(fileUrl)
            }

        })
    }

    private fun documentObserver() {
        productGroupItemVM.getByProductId(productId).observe(this, {
            if (it.isNotEmpty()) {
                val productGroupId = it.first().product_group_id

                documentVM.getByProductGroupId(productGroupId).observe(this, { dataDocument ->
                    if (dataDocument.isNotEmpty()) {
                        adapter.setList(dataDocument)
                    } else {
                        Toast.makeText(this, "Document not available", Toast.LENGTH_LONG).show()
                    }
                })
            } else {
                Toast.makeText(this, "Document not available", Toast.LENGTH_LONG).show()
            }
        })
    }

    private fun showPdf(fileUrl: String) = runWithPermissions( FunHelper.storageWritePermission) {
//        val fileLive = fileVM.getByName(fileUrl)
//        fileLive.observe(this, Observer {
//            fileLive.removeObservers(this)
//
//            if (it.isNotEmpty()) {
//                val fileUri = Uri.parse(it.first().uri)
//
//                val intent = Intent(Intent.ACTION_VIEW)
//                intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION)
//                intent.setDataAndType(fileUri, "application/pdf")
//                startActivity(intent)
//            } else {
//                DownloadPdf(this, fileUrl).downloadNow()
////                downloadPdf(fileUrl)
//            }
//        })

        DownloadPdf(this, fileUrl).downloadNow(false)
    }
}