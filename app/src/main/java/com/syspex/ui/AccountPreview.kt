package com.syspex.ui

import android.annotation.SuppressLint
import android.app.ProgressDialog
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.lifecycle.ViewModelProviders
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.livinglifetechway.quickpermissions_kotlin.runWithPermissions
import com.syspex.R
import com.syspex.data.local.database.account.AccountViewModel
import com.syspex.data.local.database.account_attachment.AccountAttachmentViewModel
import com.syspex.data.local.database.account_contact.AccountContactViewModel
import com.syspex.data.local.database.account_equipment.AccountEquipmentViewModel
import com.syspex.data.local.database.activity.ActivityViewModel
import com.syspex.data.local.database.agreement.AgreementViewModel
import com.syspex.data.local.database.service_call.ServiceCallViewModel
import com.syspex.data.local.database.service_case.ServiceCaseViewModel
import com.syspex.data.local.database.service_report.ServiceReportViewModel
import com.syspex.ui.adapter.*
import com.syspex.ui.helper.DownloadPdf
import com.syspex.ui.helper.FunHelper
import kotlinx.android.synthetic.main.account_preview_fragment.activity_count
import kotlinx.android.synthetic.main.account_preview_fragment.address_value
import kotlinx.android.synthetic.main.account_preview_fragment.agreement_count
import kotlinx.android.synthetic.main.account_preview_fragment.brach_value
import kotlinx.android.synthetic.main.account_preview_fragment.btn_add_attachment
import kotlinx.android.synthetic.main.account_preview_fragment.btn_location
import kotlinx.android.synthetic.main.account_preview_fragment.case_count
import kotlinx.android.synthetic.main.account_preview_fragment.code_value
import kotlinx.android.synthetic.main.account_preview_fragment.contact_count
import kotlinx.android.synthetic.main.account_preview_fragment.equipment_count
import kotlinx.android.synthetic.main.account_preview_fragment.recycler_activity
import kotlinx.android.synthetic.main.account_preview_fragment.recycler_agreement
import kotlinx.android.synthetic.main.account_preview_fragment.recycler_attachment
import kotlinx.android.synthetic.main.account_preview_fragment.recycler_case
import kotlinx.android.synthetic.main.account_preview_fragment.recycler_contact
import kotlinx.android.synthetic.main.account_preview_fragment.recycler_equipment
import kotlinx.android.synthetic.main.account_preview_fragment.recycler_service_call
import kotlinx.android.synthetic.main.account_preview_fragment.recycler_service_report
import kotlinx.android.synthetic.main.account_preview_fragment.sales_value
import kotlinx.android.synthetic.main.account_preview_fragment.service_call_count
import kotlinx.android.synthetic.main.account_preview_fragment.service_report_count
import kotlinx.android.synthetic.main.account_preview_fragment.tx_attachment_count
import kotlinx.android.synthetic.main.account_preview_fragment.warning_activity
import kotlinx.android.synthetic.main.account_preview_fragment.warning_agreement
import kotlinx.android.synthetic.main.account_preview_fragment.warning_attachment
import kotlinx.android.synthetic.main.account_preview_fragment.warning_call
import kotlinx.android.synthetic.main.account_preview_fragment.warning_case
import kotlinx.android.synthetic.main.account_preview_fragment.warning_contact
import kotlinx.android.synthetic.main.account_preview_fragment.warning_equipment
import kotlinx.android.synthetic.main.account_preview_fragment.warning_report
import java.net.URL

class AccountPreview : BottomSheetDialogFragment() {

    companion object {
        var accountId = ""
    }

    private lateinit var accountVM: AccountViewModel
    private lateinit var attachmentVM: AccountAttachmentViewModel
    private lateinit var contactVM: AccountContactViewModel
    private lateinit var agreementVM: AgreementViewModel
    private lateinit var caseVM: ServiceCaseViewModel
    private lateinit var callVM: ServiceCallViewModel
    private lateinit var reportVM: ServiceReportViewModel
    private lateinit var accountEquipmentVM: AccountEquipmentViewModel
    private lateinit var activityVM: ActivityViewModel

    private val contactAdapter = ContactAdapter()
    private val agreementAdapter = AgreementAdapter()
    private val caseAdapter = CaseMyAdapter()
    private val callAdapter = SCMyCallAdapter()
    private val reportAdapter = SRMyReportAdapter()
    private val equipmentAdapter = EquipmentListAdapter()
    private val activityAdapter = ActivityListAdapter()
    private val attachmentAdapter = AttachmentAdapter()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.account_preview_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        accountVM = ViewModelProviders.of(this).get(AccountViewModel::class.java)
        attachmentVM = ViewModelProviders.of(this).get(AccountAttachmentViewModel::class.java)
        contactVM = ViewModelProviders.of(this).get(AccountContactViewModel::class.java)
        agreementVM = ViewModelProviders.of(this).get(AgreementViewModel::class.java)
        caseVM = ViewModelProviders.of(this).get(ServiceCaseViewModel::class.java)
        callVM = ViewModelProviders.of(this).get(ServiceCallViewModel::class.java)
        reportVM = ViewModelProviders.of(this).get(ServiceReportViewModel::class.java)
        accountEquipmentVM = ViewModelProviders.of(this).get(AccountEquipmentViewModel::class.java)
        activityVM = ViewModelProviders.of(this).get(ActivityViewModel::class.java)

        setupAdapter()
        attachmentAdapterListener()
        buttonListener()
        accountObserver()
        contactObserver()
        agreementObserver()
        caseObserver()
        callObserver()
        reportObserver()
        equipmentObserver()
        activityObserver()
        attachmnetObserver()

    }

    private fun buttonListener() {
        btn_location.setOnClickListener {
            FunHelper.direction(it.context,address_value.text.toString())
        }

        btn_add_attachment.setOnClickListener {
            val i = Intent(requireContext(), AttachmentAdd::class.java)
            i.putExtra("accountId", accountId)
            i.putExtra("sourceId", "-")
            i.putExtra("sourceTable","-")
            startActivity(i)
        }
    }

    private fun setupAdapter() {
        FunHelper.setUpAdapter(requireContext(), recycler_contact).adapter = contactAdapter
        FunHelper.setUpAdapter(requireContext(), recycler_agreement).adapter = agreementAdapter
        FunHelper.setUpAdapter(requireContext(), recycler_case).adapter = caseAdapter
        FunHelper.setUpAdapter(requireContext(), recycler_service_call).adapter = callAdapter
        FunHelper.setUpAdapter(requireContext(), recycler_service_report).adapter = reportAdapter
        FunHelper.setUpAdapter(requireContext(), recycler_equipment).adapter = equipmentAdapter
        FunHelper.setUpAdapter(requireContext(), recycler_activity).adapter = activityAdapter
        FunHelper.setUpAdapter(requireContext(), recycler_attachment).adapter = attachmentAdapter
    }

    private fun attachmentAdapterListener() {
        attachmentAdapter.setEventHandler(object : AttachmentAdapter.RecyclerClickListener {
            override fun isClicked(fileUrl: String) {
                val progressDialog = ProgressDialog(requireContext())
                progressDialog.setMessage("Open Attachment ...")
                progressDialog.setCancelable(false)
                progressDialog.show()

                if (fileUrl.isNotEmpty()) {
                    val thread = object : Thread() {
                        override fun run() {
                            var fileExtension = ""
                            try {
                                val connection = URL(fileUrl).openConnection()
                                fileExtension = connection.getHeaderField("Content-Type")
                                Log.e("aim", "extention : $fileExtension")
                            } catch (e: Exception) { }

                            requireActivity().runOnUiThread {
                                if (fileExtension.contains("pdf")) {
                                    progressDialog.dismiss()
                                    showPdf(fileUrl)
                                } else if (fileExtension.contains("jpg") ||
                                    fileExtension.contains("jpeg") ||
                                    fileExtension.contains("png")) {

                                    progressDialog.dismiss()
                                    val i = Intent(requireContext(), ImagePreview::class.java)
                                    i.putExtra("image", fileUrl)
                                    startActivity(i)
                                } else {
                                    progressDialog.dismiss()
                                    Toast.makeText(
                                        requireContext(),
                                        "Cant open attachment file",
                                        Toast.LENGTH_LONG
                                    ).show()
                                }
                            }
                        }
                    }
                    thread.start()
                } else {
                    Toast.makeText(
                        requireContext(),
                        "Attachment file not available",
                        Toast.LENGTH_LONG
                    ).show()
                }
            }

        })
    }

    @SuppressLint("SetTextI18n")
    private fun accountObserver() {
        accountVM.getByAccountId(accountId).observe(viewLifecycleOwner, {
            if (it.isNotEmpty()) {
                address_value.text = it.first().address?.first()?.fullAddress ?: "-"
                code_value.text = it.first().account_code.ifEmpty { "-" }
                sales_value.text = it.first().sales_name.ifEmpty { "-" }
                brach_value.text = it.first().region_id.ifEmpty { "-" }
            }
        })
    }

    @SuppressLint("SetTextI18n")
    private fun contactObserver() {
        contactVM.getByAccountId(accountId).observe(this, {
            contact_count.text = ""

            if (it.isNotEmpty()) {
                contact_count.text = "(${it.size})"
                recycler_contact.visibility = View.VISIBLE
                warning_contact.visibility = View.GONE
                contactAdapter.setList(it)
            } else {
                recycler_contact.visibility = View.GONE
                warning_contact.visibility = View.VISIBLE
            }
        })
    }

    @SuppressLint("SetTextI18n")
    private fun agreementObserver() {
        agreementVM.getByAccountId(accountId).observe(viewLifecycleOwner, {
            agreement_count.text = ""

            if (it.isNotEmpty()) {
                agreement_count.text = "(${it.size})"
                recycler_agreement.visibility = View.VISIBLE
                warning_agreement.visibility = View.GONE
                agreementAdapter.setList(it)
            } else {
                recycler_agreement.visibility = View.GONE
                warning_agreement.visibility = View.VISIBLE
            }
        })
    }

    @SuppressLint("SetTextI18n")
    private fun caseObserver() {
        caseVM.getByAccountId(accountId).observe(viewLifecycleOwner, {
            case_count.text = ""

            if (it.isNotEmpty()) {
                case_count.text = "(${it.size})"
                recycler_case.visibility = View.VISIBLE
                warning_case.visibility = View.GONE
                caseAdapter.setList(it, false)
            } else {
                recycler_case.visibility = View.GONE
                warning_case.visibility = View.VISIBLE
            }
        })
    }

    @SuppressLint("SetTextI18n")
    private fun callObserver() {
        callVM.getByAccountId(accountId).observe(viewLifecycleOwner, {
            service_call_count.text = ""

            if (it.isNotEmpty()) {
                service_call_count.text = "(${it.size})"
                recycler_service_call.visibility = View.VISIBLE
                warning_call.visibility = View.GONE
                callAdapter.setList(it, false)
            } else {
                recycler_service_call.visibility = View.GONE
                warning_call.visibility = View.VISIBLE
            }
        })
    }

    @SuppressLint("SetTextI18n")
    private fun reportObserver() {
        reportVM.getByAccountId(accountId).observe(viewLifecycleOwner, {
            service_report_count.text = ""

            if (it.isNotEmpty()) {
                service_report_count.text = "(${it.size})"
                recycler_service_report.visibility = View.VISIBLE
                warning_report.visibility = View.GONE
                reportAdapter.setList(it, false)
            } else {
                recycler_service_report.visibility = View.GONE
                warning_report.visibility = View.VISIBLE
            }
        })
    }

    @SuppressLint("SetTextI18n")
    private fun equipmentObserver(){
        accountEquipmentVM.getByAccountId(accountId).observe(viewLifecycleOwner, {
            equipment_count.text = ""

            if (it.isNotEmpty()) {
                equipment_count.text = "(${it.size})"
                recycler_equipment.visibility = View.VISIBLE
                warning_equipment.visibility = View.GONE
                equipmentAdapter.setList(it, false)
            } else {
                recycler_equipment.visibility = View.GONE
                warning_equipment.visibility = View.VISIBLE
            }
        })
    }

    @SuppressLint("SetTextI18n")
    private fun activityObserver() {
        activityVM.getByAccount(accountId).observe(viewLifecycleOwner, {
            activity_count.text = ""

            if (it.isNotEmpty()) {
                activity_count.text = "(${it.size})"
                recycler_activity.visibility = View.VISIBLE
                warning_activity.visibility = View.GONE
                activityAdapter.setList(it)
            } else {
                recycler_activity.visibility = View.GONE
                warning_activity.visibility = View.VISIBLE
            }
        })
    }

    @SuppressLint("SetTextI18n")
    private fun attachmnetObserver() {
        attachmentVM.accountAttachment(accountId)
            .observe(this, { attachmentData->
            tx_attachment_count.text = ""

            if (attachmentData.isNotEmpty()) {
                tx_attachment_count.text = "(${attachmentData.size})"
                recycler_attachment.visibility = View.VISIBLE
                warning_attachment.visibility = View.GONE
                attachmentAdapter.setList(attachmentData)
            } else {
                recycler_attachment.visibility = View.GONE
                warning_attachment.visibility = View.VISIBLE
            }
        })
    }

    private fun showPdf(fileUrl: String) = runWithPermissions( FunHelper.storageWritePermission) {
        DownloadPdf(requireContext(), fileUrl).downloadNow(false)
    }

}
