package com.syspex.ui

import android.annotation.SuppressLint
import android.app.ProgressDialog
import android.graphics.BitmapFactory
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.FragmentActivity
import androidx.lifecycle.ViewModelProviders
import com.syspex.R
import com.syspex.data.local.SharedPreferenceData
import com.syspex.data.local.database.account_attachment.AccountAttachmentEntity
import com.syspex.data.local.database.account_attachment.AccountAttachmentViewModel
import com.syspex.data.model.ConverterImageModel
import com.syspex.data.remote.AttachmentRequest
import com.syspex.data.retrofit.ApiClient
import com.syspex.ui.helper.FunHelper
import kotlinx.android.synthetic.main.activity_attachment_add.*
import kotlinx.android.synthetic.main.sub_header.*
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.InputStream
import java.util.*
import kotlin.collections.ArrayList
import kotlin.collections.set


class AttachmentAdd : AppCompatActivity() {

    companion object {
        var attachmentFile = ArrayList<ConverterImageModel>()
    }

    private lateinit var attachmentVM: AccountAttachmentViewModel
//    private lateinit var fileUpload : File
    private lateinit var uriUpload : Uri
    private lateinit var nameUpload : String

    private var accountId = ""
    private var attachmentId = ""
    private var sourceId = ""
    private var sourceTable = ""

    @SuppressLint("SetTextI18n")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_attachment_add)
        attachmentVM = ViewModelProviders.of(this).get(AccountAttachmentViewModel::class.java)

        generateView()
        buttonListener()

    }

    override fun onResume() {
        super.onResume()
        if (attachmentFile.isNotEmpty()) {
            file_value.visibility = View.VISIBLE
            attachmentFile.forEach {
                file_value.text = it.fileName
            }

            val fileUri = attachmentFile.first().fileUri
            nameUpload = attachmentFile.first().fileName
            uriUpload = Uri.parse(fileUri)

            Log.e(
                "aim",
                "name : ${attachmentFile.first().fileName}, uri: ${attachmentFile.first().fileUri}"
            )

//            val fileUri = attachmentFile.first().fileUri
//            val path = Uri.parse(fileUri).path // "/mnt/sdcard/FileName.mp3"
//            fileUpload = File(URI(fileUri))

            attachmentFile.clear()
        } else {
            file_value.visibility = View.GONE
        }
    }

    private fun generateView() {
        if (intent.hasExtra("accountId")) {
            accountId = intent.getStringExtra("accountId") ?: ""
            attachmentId = intent.getStringExtra("attachmentId") ?: ""
            sourceId = intent.getStringExtra("sourceId") ?: ""
            sourceTable = intent.getStringExtra("sourceTable") ?: ""

            header_name.text = if (sourceTable != "-") {
                "$sourceTable Attachment"
            } else {
                "Account Attachment"
            }
            title_value.setText(intent.getStringExtra("title") ?: "")

            Log.e(
                "aim",
                "accountId: $accountId, attachmentId: $attachmentId, sourceId: $sourceId, sourceTable: $sourceTable"
            )
        }
    }

    @SuppressLint("SetTextI18n")
    private fun buttonListener() {
        back_link.setOnClickListener {
            onBackPressed()
        }

        btn_upload.setOnClickListener {
            DialogUploadFile.fileSection = "attachment"
            DialogUploadFile.equipmentId = ""
            DialogUploadFile().show(
                (it.context as FragmentActivity).supportFragmentManager,
                DialogUploadFile().tag
            )
        }

        btn_save.setOnClickListener {
            if (title_value.text.isNotEmpty() && file_value.text.isNotEmpty()) {
                sendAttachment()
            } else {
                title_err.text = "*Required"
                file_err.text = "*Required"
            }
        }
    }

    private fun sendAttachment() {
        val progressDialog = ProgressDialog(this)
        progressDialog.setMessage("Send Attachment ...")
        progressDialog.setCancelable(false)
        progressDialog.show()

        val token = "Bearer "+ SharedPreferenceData.getString(this, 2, "")
        val userId = SharedPreferenceData.getString(this, 8, "")

        val params = HashMap<String, RequestBody>()
        params["account_attachment_id"] = toRequestBody(attachmentId)
        params["attachment_title"] = toRequestBody(title_value.text.toString())
        params["attachment_body"] = toRequestBody("body")
        params["attachment_source_table"] = toRequestBody(sourceTable)
        params["attachment_source_id"] = toRequestBody(sourceId)
        params["account_id"] = toRequestBody(accountId)

        val inputStream: InputStream? = contentResolver.openInputStream(uriUpload)
//        val bitmap = BitmapFactory.decodeStream(inputStream)
//        inputStream.close()

        val fileParam: RequestBody = RequestBody.create(
            MediaType.parse(
                contentResolver.getType(
                    uriUpload
                )!!
            ), inputStream!!.readBytes()
        )
        params["attachment_file\"; filename=\"$nameUpload"] = fileParam
        Log.e("aim", "params: $params")

        ApiClient.instance.sendAttachment(token, params).enqueue(object :Callback<AttachmentRequest> {
            override fun onFailure(call: Call<AttachmentRequest>, t: Throwable) {
                progressDialog.dismiss()
                Toast.makeText(this@AttachmentAdd, "Send attachment failed", Toast.LENGTH_LONG)
                    .show()
                Log.e("aim", "onFailure ${t.message}")
            }

            override fun onResponse(call: Call<AttachmentRequest>,response: Response<AttachmentRequest>) {
                progressDialog.dismiss()

                if (!response.isSuccessful) {
                    if (response.code() == 401) {
                        FunHelper.clearAllData(this@AttachmentAdd)
                        Toast.makeText(this@AttachmentAdd, "Session end", Toast.LENGTH_LONG)
                            .show()
                    }
                    else {
                        FunHelper.firebaseApiLog(
                            userId ?: "",
                            response.raw().request().url().toString() ?: "",
                            response.raw().request().headers().toString() ?: "",
                            params.toString() ?: "",
                            response.raw().toString() ?: ""
                        )
                        Toast.makeText(this@AttachmentAdd, response.raw().toString(), Toast.LENGTH_LONG).show()
                    }
                } else {
                    attachmentVM.insert(
                        AccountAttachmentEntity(
                            response.body()?.data?.accountAttachmentId ?: "",
                            response.body()?.data?.regionId ?: "",
                            response.body()?.data?.accountId ?: "",
                            response.body()?.data?.attachmentTitle ?: "",
                            response.body()?.data?.attachmentBody ?: "",
                            response.body()?.data?.fileUrl ?: "",
                            response.body()?.data?.attachmentOwnerId ?: "",
                            response.body()?.data?.createdDate ?: "",
                            response.body()?.data?.createdBy ?: "",
                            response.body()?.data?.lastModifiedDate ?: "",
                            response.body()?.data?.lastModifiedBy ?: "",
                            response.body()?.data?.attachmentSourceTable ?: "",
                            response.body()?.data?.attachmentSourceId ?: "",
                            "",
                            "",
                            response.body()?.data?.createdByUser?.id ?: "",
                            response.body()?.data?.createdByUser?.userFullname ?: ""
                        )
                    )

                    Toast.makeText(this@AttachmentAdd, "Attachment sent", Toast.LENGTH_LONG).show()
                    onBackPressed()
                }
            }
        })
    }

    private fun toRequestBody(value: String): RequestBody {
        return RequestBody.create(MultipartBody.FORM, value)
    }

}