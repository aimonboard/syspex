package com.syspex.ui

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import com.google.zxing.Result
import com.livinglifetechway.quickpermissions_kotlin.runWithPermissions
import com.syspex.R
import com.syspex.ui.helper.FunHelper
import innovacia.co.id.trunkey1.helper.CustomViewFinder
import kotlinx.android.synthetic.main.scan_activity.*
import kotlinx.android.synthetic.main.sub_header.*
import me.dm7.barcodescanner.core.IViewFinder
import me.dm7.barcodescanner.zxing.ZXingScannerView


class Scan : AppCompatActivity() , ZXingScannerView.ResultHandler {

    private var mScannerView: ZXingScannerView? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.scan_activity)
        header_name?.text = "Scan"
        back_link.setOnClickListener {
            onBackPressed()
        }

        initScannerView()
    }

    private fun initScannerView() = runWithPermissions(FunHelper.cameraPermission) {
        mScannerView = object : ZXingScannerView(this) {
            override fun createViewFinderView(context: Context?): IViewFinder {
                return CustomViewFinder(context!!)
            }
        }

        if (mScannerView != null) {
            mScannerView!!.startCamera()
            mScannerView!!.setAutoFocus(true)
            mScannerView!!.setResultHandler(this)
            frame_layout_camera.addView(mScannerView)
        }
    }

    override fun onStart() {
        super.onStart()
        if (mScannerView != null) {
            mScannerView!!.startCamera()
            initScannerView()
        }
    }

    override fun handleResult(rawResult: Result?) {
        if (rawResult != null) {
            Log.d("aim", "hasil scan : $rawResult")
            val intent = Intent()
            intent.putExtra("scan_result", rawResult.toString())
            setResult(FunHelper.scanRequest, intent)
            finish()
        }
    }


}