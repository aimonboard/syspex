package com.syspex.ui.receiver

import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.media.RingtoneManager
import android.os.Build
import android.util.Log
import android.widget.Toast
import androidx.core.app.NotificationCompat
import com.syspex.R
import com.syspex.data.local.database.AppDatabase
import com.syspex.ui.helper.AlertReportReceiver
import com.syspex.ui.service.AlarmService
import com.syspex.ui.util.Constants
import java.text.SimpleDateFormat
import java.util.*
import java.util.concurrent.TimeUnit

class AlarmReceiver : BroadcastReceiver() {
    override fun onReceive(context: Context, intent: Intent) {
        Toast.makeText(context, "receiver", Toast.LENGTH_LONG).show()
        val timeInMillis = intent.getLongExtra(Constants.EXTRA_EXACT_ALARM_TIME, 0L)

        when (intent.action) {
            Constants.ACTION_SET_EXACT -> {
                val callId = intent.getStringExtra("callId") ?: "0000"
                val callNumber = intent.getStringExtra("callNumber") ?: "0000"
                val start = intent.getStringExtra("start") ?: "00:00"
                val message = "Today service call : $callNumber - 2 hours remaining before $start"
                buildNotification(context, 0, message)
            }

            Constants.ACTION_SET_REPETITIVE_EXACT -> {
                setRepetitiveAlarm(AlarmService(context))
                checkAvailableReport(context)
            }
        }
    }

    private fun checkAvailableReport(context: Context) {
        val dao = AppDatabase.getInstance(context)!!.dao()
        val sdf = SimpleDateFormat("yyyy-MM-dd", Locale.getDefault())

//        val startDateTime = "${sdf.format(Date())} 00:01"
//        val endDateTime = "${sdf.format(Date())} 23:59"

        val startDateTime = "2020-10-26 00:01"
        val endDateTime = "2020-10-26 23:59"
        Log.e("aim", "report start : $startDateTime, end : $endDateTime")

        val thread = object : Thread() {
            override fun run() {
                dao.reportTodayAlert(startDateTime, endDateTime).forEachIndexed { index, it ->
                    Log.e("aim", "report today : $it")
                    if (it.reportLocalId == null) {
                        val notificationId = index+1
                        val content = "You have open service call ${it.serviceCallNumber ?: "-"} " +
                                "overdue with no service report"
                        buildNotification(context, notificationId, content)
                    }
                }
            }
        }
        thread.start()
    }

    private fun buildNotification(context: Context, notificationId: Int, message: String) {
        val intent = Intent(context, AlertReportReceiver::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
        val pendingIntent = PendingIntent.getActivity(
            context, 0, intent,
            PendingIntent.FLAG_ONE_SHOT
        )

        val chanelId = "Default"
        val soundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION)
        val builder= NotificationCompat.Builder(context, chanelId)
            .setPriority(NotificationCompat.PRIORITY_HIGH)
            .setStyle(NotificationCompat.BigTextStyle().bigText(message))
            .setSmallIcon(R.mipmap.ic_launcher_round)
            .setContentTitle("Reminder")
            .setContentText(message)
            .setAutoCancel(true)
            .setSound(soundUri)
//            .setContentIntent(pendingIntent)

        val manager = context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val channel = NotificationChannel(
                chanelId,
                "Default channel",
                NotificationManager.IMPORTANCE_DEFAULT
            )
            manager.createNotificationChannel(channel)
        }

        manager.notify(notificationId, builder.build())
    }

    private fun setRepetitiveAlarm(alarmService: AlarmService) {
        val cal = Calendar.getInstance().apply {
            this.timeInMillis = timeInMillis + TimeUnit.HOURS.toMillis(1)
        }
        alarmService.reportAlarm(cal.timeInMillis)
    }

    fun getDate(milliSeconds: Long): String {
        // Create a DateFormatter object for displaying date in specified format.
        val formatter = SimpleDateFormat("hh:mm", Locale.getDefault())

        // Create a calendar object that will convert the date and time value in milliseconds to date.
        val calendar = Calendar.getInstance()
        calendar.timeInMillis = milliSeconds
        return formatter.format(calendar.time)
    }

}