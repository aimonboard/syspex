package com.syspex.ui

import android.annotation.SuppressLint
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.FragmentActivity
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.syspex.R
import com.syspex.data.local.SharedPreferenceData
import com.syspex.data.local.database.account_equipment.AccountEquipmentEntity
import com.syspex.data.local.database.account_equipment.AccountEquipmentViewModel
import com.syspex.data.local.database.product_group.ProductViewModel
import com.syspex.data.local.database.product_checklist_question.ChecklistQuestionViewModel
import com.syspex.data.local.database.product_checklist_template.ChecklistTemplateViewModel
import com.syspex.data.local.database.product_group_item.ProductGroupItemViewModel
import com.syspex.data.local.database.service_call.ServiceCallViewModel
import com.syspex.data.local.database.service_call_equipment.ServiceCallEquipmentEntity
import com.syspex.data.local.database.service_call_equipment.ServiceCallEquipmentViewModel
import com.syspex.data.local.database.service_case.ServiceCaseViewModel
import com.syspex.data.local.database.service_case_equipment.ServiceCaseEquipmentEntity
import com.syspex.data.local.database.service_case_equipment.ServiceCaseEquipmentViewModel
import com.syspex.data.local.database.service_report.ServiceReportViewModel
import com.syspex.data.local.database.service_report_equipment.ServiceReportEquipmentEntity
import com.syspex.data.local.database.service_report_equipment.ServiceReportEquipmentViewModel
import com.syspex.data.local.database.service_report_equipment_checklist.ServiceReportEquipmentChecklistEntity
import com.syspex.data.local.database.service_report_equipment_checklist.ServiceReportEquipmentChecklistViewModel
import com.syspex.ui.adapter.*
import com.syspex.ui.helper.FunHelper
import kotlinx.android.synthetic.main.equipment_add_activity.*
import kotlinx.android.synthetic.main.sub_header.*

class EquipmentAdd : AppCompatActivity() {

    private lateinit var caseVM: ServiceCaseViewModel
    private lateinit var caseEquipmentVM: ServiceCaseEquipmentViewModel
    private lateinit var accountEquipmentVM: AccountEquipmentViewModel
    private lateinit var callVM: ServiceCallViewModel
    private lateinit var callEquipmentVM: ServiceCallEquipmentViewModel
    private lateinit var reportVM: ServiceReportViewModel
    private lateinit var reportEquipmentVM: ServiceReportEquipmentViewModel
    private lateinit var productGroupItemVM: ProductGroupItemViewModel
    private lateinit var productVM: ProductViewModel

    private lateinit var checklistTemplateMaster: ChecklistTemplateViewModel
    private lateinit var checklistQuestionMaster: ChecklistQuestionViewModel
    private lateinit var reportChecklistVM: ServiceReportEquipmentChecklistViewModel

    private val accountEquipmentAdapter = AccountEquipmentAdapter()
    private val caseEquipmentAdapter = CaseEquipmentAdapter()
    private val callEquipmentBeforeAdapter = CallEquipmentBeforeAdapter()

    private val callEquipmentAfterAdapter = CallEquipmentAfterAdapter()
    private val reportEquipmentAfterAdapter = ReportEquipmentAfterAdapter()

    private var section = ""
    private var caseId = ""
    private var accountId = ""
    private var callId = ""
    private var reportId = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.equipment_add_activity)
        header_name?.text = "Add New Equipment"

        caseVM = ViewModelProviders.of(this).get(ServiceCaseViewModel::class.java)
        caseEquipmentVM = ViewModelProviders.of(this).get(ServiceCaseEquipmentViewModel::class.java)
        accountEquipmentVM = ViewModelProviders.of(this).get(AccountEquipmentViewModel::class.java)
        callVM = ViewModelProviders.of(this).get(ServiceCallViewModel::class.java)
        callEquipmentVM = ViewModelProviders.of(this).get(ServiceCallEquipmentViewModel::class.java)
        reportVM = ViewModelProviders.of(this).get(ServiceReportViewModel::class.java)
        reportEquipmentVM = ViewModelProviders.of(this).get(ServiceReportEquipmentViewModel::class.java)
        productGroupItemVM = ViewModelProviders.of(this).get(ProductGroupItemViewModel::class.java)
        productVM = ViewModelProviders.of(this).get(ProductViewModel::class.java)

        checklistTemplateMaster = ViewModelProviders.of(this).get(ChecklistTemplateViewModel::class.java)
        checklistQuestionMaster = ViewModelProviders.of(this).get(ChecklistQuestionViewModel::class.java)
        reportChecklistVM = ViewModelProviders.of(this).get(ServiceReportEquipmentChecklistViewModel::class.java)

        generateView()
        buttonListener()
        searchListener()
        adapterListener()

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == FunHelper.scanRequest && data != null) {
            search_value.setText(if (data.hasExtra("scan_result")) {
                data.getStringExtra("scan_result") ?: ""
            } else {
                ""
            })
        }
    }

    @SuppressLint("SetTextI18n")
    private fun generateView() {
        if (intent.hasExtra("section")) {
            section = intent.getStringExtra("section")!!
            when(section) {
                "call" -> {
                    accountId = intent.getStringExtra("accountId")!!
                    caseId = intent.getStringExtra("caseId")!!
                    callId = intent.getStringExtra("callId")!!

                    header_name.text = "Add Call Equipment"
                    head_after.text = "Call Equipment"

                    generateSpinner()
                    callEquipmentAttachObserver()
                }
                "report" -> {
                    accountId = intent.getStringExtra("accountId")!!
                    caseId = intent.getStringExtra("caseId")!!
                    callId = intent.getStringExtra("callId")!!
                    reportId = intent.getStringExtra("reportId")!!

                    header_name.text = "Add Report Equipment"
                    head_after.text = "Report Equipment"

                    generateSpinner()
                    reportEquipmentAttachObserver()
                }
            }
        }
    }

    private fun buttonListener() {
        back_link?.setOnClickListener {
            onBackPressed()
        }

        btn_barcode?.setOnClickListener {
            val i = Intent(it.context, Scan::class.java)
            startActivityForResult(i, FunHelper.scanRequest)
        }

        btn_down?.setOnClickListener {
            if (recycler_after.visibility == View.GONE) {
                btn_down.rotation = 0f
                recycler_after.visibility = View.VISIBLE
            } else {
                btn_down.rotation = 180f
                recycler_after.visibility = View.GONE
            }

        }

        btn_add_other?.setOnClickListener {

        }
    }

    private fun searchListener() {
        search_value.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) { }
            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) { }

            override fun afterTextChanged(p0: Editable?) {
                if (p0 != null) {
                    when (spinner_lay.selectedItem as String) {
                        "Account" -> accountEquipmentObserver()
                        "Case" -> caseEquipmentObserver()
                        "Call" -> callEquipmentObserver()
                    }
                }
            }
        })
    }

    private fun adapterListener() {
        // ========================================================================================= Attatch Equipment
        accountEquipmentAdapter.setEventHandler(object: AccountEquipmentAdapter.RecyclerClickListener {
            override fun attach(position: Int, data: AccountEquipmentEntity) {
                search_value.text.clear()

                if (section == "call") {
                    accountEquipmentAdapter.removeData(data)
                    insertCaseEquipment(data.toCaseEquipment())
                    insertCallEquipment(data.toCallEquipment())
                }
                else {
                    val checkReport = reportEquipmentVM.checkAttach(callId, data.equipmentId, true)
                    checkReport.observe(this@EquipmentAdd, {
                        checkReport.removeObservers(this@EquipmentAdd)
                        if (it.isEmpty()) {
                            accountEquipmentAdapter.removeData(data)
                            insertReportEquipment(data.toReportEquipment())
                        } else {
                            Toast.makeText(this@EquipmentAdd,"Equipment already exist in other report",Toast.LENGTH_LONG).show()
                        }
                    })
                }
            }
        })

        caseEquipmentAdapter.setEventHandler(object: CaseEquipmentAdapter.RecyclerClickListener {
            override fun attach(position: Int, data: ServiceCaseEquipmentEntity) {
                search_value.text.clear()

                if (section == "call") {
                    caseEquipmentAdapter.removeData(data)
                    insertCallEquipment(data.toCallEquipment())
                }
                else {
                    val checkReport = reportEquipmentVM.checkAttach(callId, data.equipmentId, true)
                    checkReport.observe(this@EquipmentAdd, {
                        checkReport.removeObservers(this@EquipmentAdd)
                        if (it.isEmpty()) {
                            caseEquipmentAdapter.removeData(data)
                            insertReportEquipment(data.toReportEquipment())
                        } else {
                            Toast.makeText(this@EquipmentAdd,"Equipment already exist in other report",Toast.LENGTH_LONG).show()
                        }
                    })
                }
            }
        })

        callEquipmentBeforeAdapter.setEventHandler(object: CallEquipmentBeforeAdapter.RecyclerClickListener {
            override fun attach(position: Int, data: ServiceCallEquipmentEntity) {
                search_value.text.clear()

                val checkReport = reportEquipmentVM.checkAttach(callId, data.equipmentId, true)
                checkReport.observe(this@EquipmentAdd, {
                    checkReport.removeObservers(this@EquipmentAdd)
                    if (it.isEmpty()) {
                        callEquipmentBeforeAdapter.removeData(data)
                        insertReportEquipment(data.toReportEquipment())
                    } else {
                        Toast.makeText(this@EquipmentAdd,"Equipment already exist in other report",Toast.LENGTH_LONG).show()
                    }
                })
            }
        })

        // ========================================================================================= Detach Equipment
        callEquipmentAfterAdapter.setEventHandler(object: CallEquipmentAfterAdapter.RecyclerClickListener{
            override fun detach(data: ServiceCallEquipmentEntity) {
                deleteDialog(data.serviceCallEquipmentId,"", data.equipmentId, data.serialNo)
            }
        })

        reportEquipmentAfterAdapter.setEventHandler(object: ReportEquipmentAfterAdapter.RecyclerClickListener{
            override fun detach(data: ServiceReportEquipmentEntity) {
                deleteDialog("", data.serviceReportEquipmentId, "", data.serialNo)
            }
        })
    }

    @SuppressLint("SetTextI18n")
    private fun generateSpinner(){
        val spinnerData =
            if (section == "call") arrayOf("Case", "Account")
            else arrayOf("Call", "Case", "Account")

        val arrayAdapter = ArrayAdapter(this, android.R.layout.simple_spinner_dropdown_item, spinnerData)
        spinner_lay?.adapter = arrayAdapter

        spinner_lay?.onItemSelectedListener = object : AdapterView.OnItemSelectedListener{
            override fun onNothingSelected(parent: AdapterView<*>?) { }

            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                tx_before_count.text = ""

                when (spinnerData[position]){
                    "Account" -> {
                        tx_label.text = "Search Account Equipment"
                        head_before.text = "Account Equipment"
                        accountEquipmentObserver()
                    }
                    "Case" -> {
                        tx_label.text = "Search Case Equipment"
                        head_before.text = "Case Equipment"
                        caseEquipmentObserver()
                    }
                    "Call" -> {
                        tx_label.text = "Search Call Equipment"
                        head_before.text = "Call Equipment"
                        callEquipmentObserver()
                    }
                }
            }
        }
    }

    // ============================================================================================= Before (data master)
    @SuppressLint("SetTextI18n")
    private fun accountEquipmentObserver() {
        recycler_before?.layoutManager = LinearLayoutManager(this, RecyclerView.VERTICAL, false)
        recycler_before?.adapter = accountEquipmentAdapter

        accountEquipmentVM.equipmentAdd(accountId, caseId, search_value.text.toString()).observe(this, {
            tx_before_count.text = ""
            accountEquipmentAdapter.clearData()

            if (it.isNotEmpty()) {
                tx_before_count.text = "(${it.size})"
                accountEquipmentAdapter.setList(it)
            }
        })
    }

    @SuppressLint("SetTextI18n")
    private fun caseEquipmentObserver() {
        recycler_before?.layoutManager = LinearLayoutManager(this, RecyclerView.VERTICAL, false)
        recycler_before?.adapter = caseEquipmentAdapter

        caseEquipmentVM.equipmentAdd(caseId, callId, search_value.text.toString()).observe(this, {
            tx_before_count.text = ""
            caseEquipmentAdapter.clearData()

            if (it.isNotEmpty()) {
                tx_before_count.text = "(${it.size})"
                caseEquipmentAdapter.setList(it)
            }
        })
    }

    @SuppressLint("SetTextI18n")
    private fun callEquipmentObserver() {
        recycler_before?.layoutManager = LinearLayoutManager(this, RecyclerView.VERTICAL, false)
        recycler_before?.adapter = callEquipmentBeforeAdapter

        callEquipmentVM.equipmentAdd(callId, reportId, search_value.text.toString()).observe(this, {
            tx_before_count.text = ""
            callEquipmentBeforeAdapter.clearData()

            if (it.isNotEmpty()) {
                tx_before_count.text = "(${it.size})"
                callEquipmentBeforeAdapter.setList(it)
            }
        })
    }

    // ============================================================================================= After (already attach)
    @SuppressLint("SetTextI18n")
    private fun callEquipmentAttachObserver() {
        recycler_after?.layoutManager = LinearLayoutManager(this, RecyclerView.VERTICAL, false)
        recycler_after?.adapter = callEquipmentAfterAdapter

        callEquipmentVM.getAttachToCall(callId, true).observe(this, {
            tx_after_count.text = ""
            callEquipmentAfterAdapter.clearData()

            if (it.isNotEmpty()) {
                tx_after_count.text = "(${it.size})"
                callEquipmentAfterAdapter.setList(it)
            }
        })

    }

    @SuppressLint("SetTextI18n")
    private fun reportEquipmentAttachObserver() {
        recycler_after?.layoutManager = LinearLayoutManager(this, RecyclerView.VERTICAL, false)
        recycler_after?.adapter = reportEquipmentAfterAdapter

        reportEquipmentVM.getByReportId(reportId).observe(this, {
            tx_after_count.text = ""
            reportEquipmentAfterAdapter.clearData()

            if (it.isNotEmpty()) {
                tx_after_count.text = "(${it.size})"
                reportEquipmentAfterAdapter.setList(it)
            }
        })
    }

    // ============================================================================================= Insert Equipment
    private fun insertCaseEquipment(equipment: ServiceCaseEquipmentEntity) {
//        caseEquipmentVM.insert(equipment)

        // Insert when equipment no exist
        val checkCase = caseEquipmentVM.checkAttach(caseId, equipment.equipmentId)
        checkCase.observe(this, {
            checkCase.removeObservers(this)
            if (it.isEmpty()) {
                caseEquipmentVM.insert(equipment)
            }
        })

        caseVM.updateFlag(caseId, false)
        callVM.updateFlag(callId, false)
        reportVM.updateFlag(reportId, false)
    }

    private fun insertCallEquipment(equipment: ServiceCallEquipmentEntity) {
//        callEquipmentVM.insert(equipment)

        // Insert when equipment no exist
        val checkCall = callEquipmentVM.checkAttach(callId, equipment.equipmentId)
        checkCall.observe(this, {
            checkCall.removeObservers(this)
            if (it.isEmpty()) {
                callEquipmentVM.insert(equipment)
            }
        })

        caseVM.updateFlag(caseId, false)
        callVM.updateFlag(callId, false)
        reportVM.updateFlag(reportId, false)
    }

    private fun insertReportEquipment(equipment: ServiceReportEquipmentEntity) {
        // Insert equipment
        reportEquipmentVM.insert(equipment)

        // Insert Checklist
        getCallType(equipment.product_id, equipment.serviceReportEquipmentId)

        caseVM.updateFlag(caseId, false)
        callVM.updateFlag(callId, false)
        reportVM.updateFlag(reportId, false)
    }

    // ============================================================================================= Delete Equipment
    private fun deleteDialog(callEquipmentId: String, reportEquipmentId: String, equipmentId: String, equipmentNumber: String) {
        AlertDialog.Builder(this)
            .setMessage("Delete equipment $equipmentNumber ?")
            .setPositiveButton(android.R.string.yes) { dialog, whichButton ->
                if (section == "call") {
                    deleteCallEquipment(callEquipmentId, equipmentId)
                } else {
                    deleteReportEquipment(reportEquipmentId)
                }
            }
            .setNegativeButton(android.R.string.no, null).show()
    }

    private fun deleteCallEquipment(callEquipmentId: String, equipmentId: String) {
        val reportLive = reportVM.reportJoinReportEquipment(callId)
        reportLive.observe(this, {
            reportLive.removeObservers(this)

            var reportIdCompare = ""
            var reportNumberCompare = ""
            var equipmentIdCompare = ""

            it.forEach { equipmentData->
                if (equipmentData.equipmentId ?: "" == equipmentId) {
                    reportIdCompare = equipmentData.reportId ?: ""
                    equipmentIdCompare = equipmentData.equipmentId ?: ""
                    reportNumberCompare = equipmentData.serviceReportNumber ?: ""
                }
            }

            if (reportIdCompare.isNotEmpty() && reportNumberCompare.isNotEmpty() && equipmentIdCompare.isNotEmpty()) {
                DialogEquipmentAdd.reportId = reportIdCompare
                DialogEquipmentAdd.reportNumber = reportNumberCompare
                DialogEquipmentAdd().show((this as FragmentActivity).supportFragmentManager, DialogEquipmentAdd().tag)
            } else {
                callEquipmentVM.deleteById(callEquipmentId)
                accountEquipmentObserver()
                caseEquipmentObserver()
                callEquipmentAttachObserver()

                caseVM.updateFlag(caseId, false)
                callVM.updateFlag(callId, false)
                reportVM.updateFlag(reportId, false)
                deleteRefreshData()
            }

        })

    }

    private fun deleteReportEquipment(reportEquipmentId: String) {
        reportEquipmentVM.deleteById(reportEquipmentId)
        reportChecklistVM.deleteById(reportEquipmentId)

        accountEquipmentObserver()
        caseEquipmentObserver()
        callEquipmentAttachObserver()
        reportEquipmentAttachObserver()

        caseVM.updateFlag(caseId, false)
        callVM.updateFlag(callId, false)
        reportVM.updateFlag(reportId, false)
        deleteRefreshData()
    }

    private fun deleteRefreshData() {
        search_value.text.clear()

        when (spinner_lay.selectedItem as String) {
            "Account" -> accountEquipmentObserver()
            "Case" -> caseEquipmentObserver()
            "Call" -> callEquipmentObserver()
        }
    }

    // ============================================================================================= Insert Checklist
    private fun getCallType(productId: String, reportEquipmnetId: String) {
        Log.e("aim", "productId : $productId")

        callVM.getByCallId(callId).observe(this, { callData->
            if (callData.isNotEmpty()) {
                Log.e("aim", "call type : ${callData.first().callTypeEnumText}")

                if (callData.first().callTypeEnumText == "Installation" || callData.first().callTypeEnumText == "Maintenance") {
                    insertChecklist(productId, reportEquipmnetId, callData.first().callTypeEnumText)
                }
            }
        })
    }

    // Hardcode Maintenance
    private fun insertChecklist(productId: String, reportEquipmnetId: String, checklistTypeText: String) {
        productGroupItemVM.getByProductId(productId).observe(this, {

            if (it.isNotEmpty()) {

                val productGroupId = it.first().product_group_id

                checklistTemplateMaster.getTemplateByProductGroupIdAndType(productGroupId, checklistTypeText).observe(this, { templateArray ->

                    templateArray.forEach { templateData ->

                        Log.e(
                            "aim",
                            "template type : ${templateData.checklistTypeTextEnumName}"
                        )

                        val userId = SharedPreferenceData.getString(this, 8, "")
                        val nowDate = FunHelper.getDate(false)

                        reportEquipmentVM.editTemplateId(
                            reportEquipmnetId,
                            templateData.productGroupChecklistTemplateId
                        )

                        checklistQuestionMaster.getByTemplate(templateData.productGroupChecklistTemplateId)
                            .observe(this, { questionArray ->

                                questionArray.forEach { questionData ->

                                    Log.e(
                                        "aim",
                                        "question section : ${questionData.sectionTitle}"
                                    )

                                    reportChecklistVM.insert(
                                        ServiceReportEquipmentChecklistEntity(
                                            FunHelper.randomString("") ?: "",
                                            reportEquipmnetId ?: "",
                                            "",
                                            "",
                                            nowDate ?: "",
                                            userId ?: "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            questionData.questionId ?: "",
                                            questionData.orderNo ?: "",
                                            questionData.sectionTitle ?: "",
                                            questionData.question_description ?: "",
                                            questionData.defaultRemarks ?: "",
                                            questionData.helpText ?: "",
                                            questionData.helpImageUrl ?: "",
                                            questionData.templateId ?: "",
                                            templateData.productGroupChecklistTemplateId ?: "",
                                            templateData.regionId ?: "",
                                            templateData.productGroupId ?: "",
                                            templateData.checklistTypeTextEnumId ?: "",
                                            templateData.checklistTypeTextEnumName ?: "",
                                            emptyList(),
                                            true
                                        )
                                    )
                                }
                            })
                    }

                })

            }

        })
    }

    // ============================================================================================= Model Converter
    private fun AccountEquipmentEntity.toCaseEquipment() = ServiceCaseEquipmentEntity(
        seviceCaseEquipment = FunHelper.randomString(""),
        caseId = caseId,
        equipmentId = equipmentId,
        createdDate = "",
        createdBy = "",
        lastModifiedDate = "",
        lastModifiedBy = "",
        regionId = regionId,
        serialNo = serialNo,
        brand = brand,
        warrantyStartDate = warrantyStartDate,
        warrantyEndDate = warrantyEndDate,
        equipmentRemarks = equipmentRemarks,
        deliveryAddressId = deliveryAddressId,
        salesOrderNo = salesOrderNo,
        warrantyStatus = warrantyStatus,
        accountId = accountId,
        accountName = accountName,
        agreementId = agreementId,
        agreementNumber = agreementNumber,
        agreementEnd = agreementEnd,
        statusTextEnumId = statusTextEnumId,
        statusTextEnumText = statusTextEnumText,
        product_id = product_id,
        product_name = product_name,
        isAttach = true,
        isLocal = true
    )
    private fun AccountEquipmentEntity.toCallEquipment() = ServiceCallEquipmentEntity(
        serviceCallEquipmentId = FunHelper.randomString(""),
        callId = callId,
        equipmentId = equipmentId,
        createdDate = "",
        createdBy = "",
        lastModifiedDate = "",
        lastModifiedBy = "",
        equipmentName = "",
        regionId = regionId,
        serialNo = serialNo,
        brand = brand,
        warrantyStartDate = warrantyStartDate,
        warrantyEndDate = warrantyEndDate,
        equipmentRemarks = equipmentRemarks,
        deliveryAddressId = deliveryAddressId,
        salesOrderNo = salesOrderNo,
        warrantyStatus = warrantyStatus,
        accountId = accountId,
        accountName = accountName,
        agreementId = agreementId,
        agreementNumber = agreementNumber,
        agreementEnd = agreementEnd,
        statusTextEnumId = statusTextEnumId,
        statusTextEnumText = statusTextEnumText,
        product_id = product_id,
        product_name = product_name,
        isAttachToCall = true,
        isAttachToReport = false,
        isUpload = false,
        isLocal = true
    )
    private fun AccountEquipmentEntity.toReportEquipment() = ServiceReportEquipmentEntity(
        serviceReportEquipmentId = FunHelper.randomString(""),
        serviceReportEquipmentCallId = callId,
        reportId = reportId,
        equipmentId = equipmentId,
        problemSymptom = "",
        statusAfterServiceEnumId = "",
        statusAfterServiceEnumName = "",
        statusAfterServiceRemarks = "",
        installationStartDate = "",
        installationMatters = "",
        checklistTemplateId = "",
        createdDate = "",
        createdBy = "",
        lastModifiedDate = "",
        lastModifiedBy = "",
        equipmentName = "",
        regionId = regionId,
        serialNo = serialNo,
        brand = brand,
        warrantyStartDate = warrantyStartDate,
        warrantyEndDate = warrantyEndDate,
        equipmentRemarks = equipmentRemarks,
        deliveryAddressId = deliveryAddressId,
        salesOrderNo = salesOrderNo,
        warrantyStatus = warrantyStatus,
        accountId = accountId,
        accountName = accountName,
        agreementId = agreementId,
        agreementNumber = agreementNumber,
        agreementEnd = agreementEnd,
        statusTextEnumId = statusTextEnumId,
        statusTextEnumText = statusTextEnumText,
        product_id = product_id,
        product_name = product_name,
        isAttach = true,
        isUpload = false,
        isLocal = true
    )

    private fun ServiceCaseEquipmentEntity.toCallEquipment() = ServiceCallEquipmentEntity(
        serviceCallEquipmentId = FunHelper.randomString(""),
        callId = callId,
        equipmentId = equipmentId,
        createdDate = "",
        createdBy = "",
        lastModifiedDate = "",
        lastModifiedBy = "",
        equipmentName = "",
        regionId = regionId,
        serialNo = serialNo,
        brand = brand,
        warrantyStartDate = warrantyStartDate,
        warrantyEndDate = warrantyEndDate,
        equipmentRemarks = equipmentRemarks,
        deliveryAddressId = deliveryAddressId,
        salesOrderNo = salesOrderNo,
        warrantyStatus = warrantyStatus,
        accountId = accountId,
        accountName = accountName,
        agreementId = agreementId,
        agreementNumber = agreementNumber,
        agreementEnd = agreementEnd,
        statusTextEnumId = statusTextEnumId,
        statusTextEnumText = statusTextEnumText,
        product_id = product_id,
        product_name = product_name,
        isAttachToCall = true,
        isAttachToReport = false,
        isUpload = false,
        isLocal = true
    )
    private fun ServiceCaseEquipmentEntity.toReportEquipment() = ServiceReportEquipmentEntity(
        serviceReportEquipmentId = FunHelper.randomString(""),
        serviceReportEquipmentCallId = callId,
        reportId = reportId,
        equipmentId = equipmentId,
        problemSymptom = "",
        statusAfterServiceEnumId = "",
        statusAfterServiceEnumName = "",
        statusAfterServiceRemarks = "",
        installationStartDate = "",
        installationMatters = "",
        checklistTemplateId = "",
        createdDate = "",
        createdBy = "",
        lastModifiedDate = "",
        lastModifiedBy = "",
        equipmentName = "",
        regionId = regionId,
        serialNo = serialNo,
        brand = brand,
        warrantyStartDate = warrantyStartDate,
        warrantyEndDate = warrantyEndDate,
        equipmentRemarks = equipmentRemarks,
        deliveryAddressId = deliveryAddressId,
        salesOrderNo = salesOrderNo,
        warrantyStatus = warrantyStatus,
        accountId = accountId,
        accountName = accountName,
        agreementId = agreementId,
        agreementNumber = agreementNumber,
        agreementEnd = agreementEnd,
        statusTextEnumId = statusTextEnumId,
        statusTextEnumText = statusTextEnumText,
        product_id = product_id,
        product_name = product_name,
        isAttach = true,
        isUpload = false,
        isLocal = true
    )

    private fun ServiceCallEquipmentEntity.toReportEquipment() = ServiceReportEquipmentEntity(
        serviceReportEquipmentId = FunHelper.randomString(""),
        serviceReportEquipmentCallId = callId,
        reportId = reportId,
        equipmentId = equipmentId,
        problemSymptom = "",
        statusAfterServiceEnumId = "",
        statusAfterServiceEnumName = "",
        statusAfterServiceRemarks = "",
        installationStartDate = "",
        installationMatters = "",
        checklistTemplateId = "",
        createdDate = "",
        createdBy = "",
        lastModifiedDate = "",
        lastModifiedBy = "",
        equipmentName = "",
        regionId = regionId,
        serialNo = serialNo,
        brand = brand,
        warrantyStartDate = warrantyStartDate,
        warrantyEndDate = warrantyEndDate,
        equipmentRemarks = equipmentRemarks,
        deliveryAddressId = deliveryAddressId,
        salesOrderNo = salesOrderNo,
        warrantyStatus = warrantyStatus,
        accountId = accountId,
        accountName = accountName,
        agreementId = agreementId,
        agreementNumber = agreementNumber,
        agreementEnd = agreementEnd,
        statusTextEnumId = statusTextEnumId,
        statusTextEnumText = statusTextEnumText,
        product_id = product_id,
        product_name = product_name,
        isAttach = true,
        isUpload = false,
        isLocal = true
    )

}
