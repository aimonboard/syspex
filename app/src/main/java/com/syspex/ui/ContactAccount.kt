package com.syspex.ui

import android.annotation.SuppressLint
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.syspex.R
import com.syspex.data.local.database.account_contact.AccountContactViewModel
import com.syspex.ui.adapter.ContactAdapter
import com.syspex.ui.helper.KeyboardHelper
import kotlinx.android.synthetic.main.activity_contact_account.*
import kotlinx.android.synthetic.main.activity_contact_account.btn_search
import kotlinx.android.synthetic.main.activity_contact_account.header_name
import kotlinx.android.synthetic.main.activity_contact_account.search_lay
import kotlinx.android.synthetic.main.activity_contact_account.search_value
import kotlinx.android.synthetic.main.shimmer_layout.*

class ContactAccount : AppCompatActivity() {

    private lateinit var contactVM: AccountContactViewModel
    private val contactAdapter = ContactAdapter()

    private var accountId = ""
    private var accountName = ""

    @SuppressLint("SetTextI18n")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_contact_account)
        contactVM = ViewModelProviders.of(this).get(AccountContactViewModel::class.java)

        setupAdapter()
        searchListener()
        generateView()
        buttonListener()

    }

    private fun generateView() {
        if (intent.hasExtra("accountId")) {
            accountId = intent.getStringExtra("accountId") ?: ""
            accountName = intent.getStringExtra("accountName") ?: ""

            setupObserver("")
        }
    }

    private fun buttonListener() {
        btn_back.setOnClickListener {
            onBackPressed()
        }

        btn_search?.setOnClickListener {
            if (header_name.visibility == View.VISIBLE) {
                header_name.visibility = View.GONE
                search_lay.visibility = View.VISIBLE
                btn_search.setImageResource(R.drawable.ic_close)

                KeyboardHelper.openSoftKeyboard(this,search_value)
            } else {
                header_name.visibility = View.VISIBLE
                search_lay.visibility = View.GONE
                btn_search.setImageResource(R.drawable.ic_search_small)

                KeyboardHelper.hideSoftKeyboard(this,search_value)
            }
        }

        btn_add.setOnClickListener {
            val i = Intent(this, ContactAdd::class.java)
            i.putExtra("accountId", accountId)
            i.putExtra("accountName", accountName)
            startActivity(i)
        }
    }

    private fun setupAdapter() {
        recycler_contact?.layoutManager = LinearLayoutManager(this, RecyclerView.VERTICAL, false)
        recycler_contact?.adapter = contactAdapter
    }

    private fun searchListener() {
        search_value.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) { }
            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) { }

            override fun afterTextChanged(p0: Editable?) {
                if (p0 != null) {
                    setupObserver(p0.toString())
                }
            }
        })
    }

    private fun setupObserver(searchText: String) {
        shimmer_container.visibility = View.VISIBLE
        contactVM.searchInAccountDetail(accountId, searchText).observe(this, {
            shimmer_container.visibility = View.GONE
            contactAdapter.setList(it)
        })
    }
}