package com.syspex.ui

import android.annotation.SuppressLint
import android.app.ProgressDialog
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.AutoCompleteTextView
import android.widget.Toast
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.syspex.R
import com.syspex.data.local.SharedPreferenceData
import com.syspex.data.local.database.account.AccountViewModel
import com.syspex.data.local.database.email.EmailEntity
import com.syspex.data.local.database.email.EmailViewModel
import com.syspex.data.local.database.service_call.ServiceCallViewModel
import com.syspex.data.local.database.service_report.ServiceReportViewModel
import com.syspex.data.local.database.service_report_time_tracking.ServiceReportTimeTrackingViewModel
import com.syspex.data.model.SendEmailModel
import com.syspex.data.remote.SendEmailRequest
import com.syspex.data.retrofit.ApiClient
import com.syspex.ui.adapter.CallAddReportAdapter
import com.syspex.ui.custom.AutoTextEmailAdapter
import com.syspex.ui.helper.FunHelper
import com.syspex.ui.helper.SendData
import com.syspex.ui.helper.SendDataInterface
import com.syspex.ui.helper.SendDataReport
import kotlinx.android.synthetic.main.send_email_activity.*
import kotlinx.android.synthetic.main.sub_header.*
import org.json.JSONArray
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class SendEmail : AppCompatActivity() {

    private lateinit var callVM: ServiceCallViewModel
    private lateinit var accountVM: AccountViewModel
    private lateinit var reportVM: ServiceReportViewModel
    private lateinit var timeTrackingVM: ServiceReportTimeTrackingViewModel
    private lateinit var emailVM: EmailViewModel
    private lateinit var progressDialog: ProgressDialog

    private val reportAdapter = CallAddReportAdapter()

    private var emailAll = ArrayList<SendEmailModel>()
    private var reportData = ArrayList<String>()

    private var emailValue1 : SendEmailModel? = null
    private var emailValue2 : SendEmailModel? = null
    private var emailValue3 : SendEmailModel? = null
    private var emailValue4 : SendEmailModel? = null

    private var callId = ""
//    private var reportId = ""

    @SuppressLint("SetTextI18n")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.send_email_activity)
        header_name.text = "Send Email"

        callVM = ViewModelProviders.of(this).get(ServiceCallViewModel::class.java)
        accountVM = ViewModelProviders.of(this).get(AccountViewModel::class.java)
        reportVM = ViewModelProviders.of(this).get(ServiceReportViewModel::class.java)
        timeTrackingVM = ViewModelProviders.of(this).get(ServiceReportTimeTrackingViewModel::class.java)
        emailVM = ViewModelProviders.of(this).get(EmailViewModel::class.java)

        progressDialog = ProgressDialog(this)
        progressDialog.setMessage("Send Email ...")
        progressDialog.setCancelable(false)

        generateView()
        setupAdapter()
        buttonListener()

        setupAutoTextAdapter()
        textChangeListener()

    }

    private fun generateView() {
        if (intent.hasExtra("callId")) {
            callId = intent.getStringExtra("callId") ?: ""
            val reportId = intent.getStringExtra("reportId") ?: ""

            val accountContact = intent.getStringExtra("accountContact") ?: ""
            text1.setText(accountContact)
            text1.dismissDropDown()

            val accountFieldPic = intent.getStringExtra("accountFieldPic") ?: ""
            text2.setText(accountFieldPic)
            text2.dismissDropDown()

            if (reportId.isEmpty()) {
                // from call detail
                reportObserver()
                reportAdapterListener()

                tx_report.visibility = View.VISIBLE
                report_lay.visibility = View.VISIBLE
            } else {
                // from report detail
                tx_report.visibility = View.GONE
                report_lay.visibility = View.GONE

                reportData.add(reportId)
                checkbox_checklist.isChecked = false
                checkbox_training.isChecked = false
                checkbox_handover.isChecked = false
                checkbox_completion_letter.isChecked = false
            }

            callObserver()
        }
    }

    private fun buttonListener() {
        back_link.setOnClickListener {
            onBackPressed()
        }

        btn_send_email.setOnClickListener {
            if (formValidation()) {
                sendData()
            } else {
                Toast.makeText(this,"Insert at least one email", Toast.LENGTH_LONG).show()
            }
        }

    }

    private fun formValidation(): Boolean {
        if (text1.text.toString().isNotEmpty()) {
            return true
        }
        if (text2.text.toString().isNotEmpty()) {
            return true
        }
        if (text3.text.toString().isNotEmpty()) {
            return true
        }
        if (text4.text.toString().isNotEmpty()) {
            return true
        }
        return false
    }

    private fun setupAdapter() {
        recycler_report.layoutManager = LinearLayoutManager(this, RecyclerView.VERTICAL, false)
        recycler_report.adapter = reportAdapter
    }

    private fun reportAdapterListener() {
        reportAdapter.setEventHandler(object : CallAddReportAdapter.RecyclerClickListener{
            override fun isAdd(reportId: String) {
                reportData.add(reportId)
            }

            override fun isRemove(reportId: String) {
                reportData.remove(reportId)
            }

        })
    }

    private fun reportObserver() {
        reportVM.getByCallId(callId).observe(this, {
            if (it.isNotEmpty()) {
                recycler_report.visibility = View.VISIBLE
                warning_report.visibility = View.GONE

                reportAdapter.setList(it)
            } else {
                recycler_report.visibility = View.GONE
                warning_report.visibility = View.VISIBLE
            }
        })
    }

    private fun callObserver() {
        callVM.getByCallId(callId).observe(this, { callData->
            if (callData.isNotEmpty()) {
                val accountId = callData.first().accountId
                accountVM.getByAccountId(accountId).observe(this, { accountData->
                    if (accountData.isNotEmpty()) {
                        accountData.first().contact?.forEach {
                            val name = it?.contactPersonName ?: ""
                            val email = it?.contactPersonEmail ?: ""

                            if (email.isNotEmpty()) {
                                emailAll.add(SendEmailModel(name, email))
                            }
                        }
                    }

                    Log.e("aim","email : $emailAll")

                })
            }
        })
    }

    private fun setupAutoTextAdapter() {
        autoTextAdapter(text1, 1)
        autoTextAdapter(text2, 2)
        autoTextAdapter(text3, 3)
        autoTextAdapter(text4, 4)
    }

    private fun autoTextAdapter (text_view: AutoCompleteTextView, emailValue: Int) {
        val adapter = AutoTextEmailAdapter(this, R.layout.item_spinner_black, emailAll)
        text_view.setAdapter(adapter)
        text_view.threshold = 1
        text_view.onItemClickListener = AdapterView.OnItemClickListener{ parent, view, position, id->
            val data = parent.getItemAtPosition(position) as SendEmailModel
            text_view.setText(data.email)
            emailAll.remove(data)
//            emailSelected.add(data)

            when (emailValue) {
                1 -> emailValue1 = data
                2 -> emailValue2 = data
                3 -> emailValue3 = data
                4 -> emailValue4 = data
            }

        }
        text_view.onFocusChangeListener = View.OnFocusChangeListener{ view, b ->
            if(b && text_view.text.isEmpty()) { text_view.showDropDown() }
        }
    }

    private fun textChangeListener() {
        text1?.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) { }
            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) { }

            override fun afterTextChanged(p0: Editable?) {
                if (p0 != null && emailValue1 != null) {
                    if (!emailAll.contains(emailValue1!!) && p0.toString() == "") {
                        emailAll.add(emailValue1!!)
//                        emailSelected.remove(emailValue1!!)
                        setupAutoTextAdapter()
                    }
                }
            }
        })

        text2?.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) { }
            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) { }

            override fun afterTextChanged(p0: Editable?) {
                if (p0 != null && emailValue2 != null) {
                    if (!emailAll.contains(emailValue2!!) && p0.toString() == "") {
                        emailAll.add(emailValue2!!)
//                        emailSelected.remove(emailValue2!!)
                        setupAutoTextAdapter()
                    }
                }
            }
        })

        text3?.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) { }
            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) { }

            override fun afterTextChanged(p0: Editable?) {
                if (p0 != null && emailValue3 != null) {
                    if (!emailAll.contains(emailValue3!!) && p0.toString() == "") {
                        emailAll.add(emailValue3!!)
//                        emailSelected.remove(emailValue3!!)
                        setupAutoTextAdapter()
                    }
                }
            }
        })

        text4?.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) { }
            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) { }

            override fun afterTextChanged(p0: Editable?) {
                if (p0 != null && emailValue4 != null) {
                    if (!emailAll.contains(emailValue4!!) && p0.toString() == "") {
                        emailAll.add(emailValue4!!)
//                        emailSelected.remove(emailValue4!!)
                        setupAutoTextAdapter()
                    }
                }
            }
        })
    }

//    private fun timeTrackingCheck() {
//        val timeTrackingLive = timeTrackingVM.getByReportId(reportId)
//        timeTrackingLive.observe(this, {
//            timeTrackingLive.removeObservers(this)
//            if (it.isEmpty()) {
//                Toast.makeText(this, "Send email failed, insert time tracking",Toast.LENGTH_LONG).show()
//            } else {
//                sendData()
//            }
//        })
//    }

    private fun sendData() {
        progressDialog.show()

        SendDataReport(this).getParam(callId, "call", object : SendDataInterface {
            override fun sendDataCallback(isSuccess: Boolean) {
                runOnUiThread {
                    if (isSuccess) {
                        sendReportEmail()
                    } else {
                        progressDialog.dismiss()
                    }
                }
            }
        })
    }


    private fun sendReportEmail() {
        val token = "Bearer "+ SharedPreferenceData.getString(this,2, "")
        val userId = SharedPreferenceData.getString(this,8, "")

        val reportArray = JSONArray()
        reportData.forEach {
            reportArray.put(it)
        }

        val emailArray = JSONArray()
        if (text1.text.toString().isNotEmpty()) {
            emailArray.put(text1.text.toString().trim())
        }
        if (text2.text.toString().isNotEmpty()) {
            emailArray.put(text2.text.toString().trim())
        }
        if (text3.text.toString().isNotEmpty()) {
            emailArray.put(text3.text.toString().trim())
        }
        if (text4.text.toString().isNotEmpty()) {
            emailArray.put(text4.text.toString().trim())
        }
        if (checkbox_my_email.isChecked) {
            val myEmail = SharedPreferenceData.getString(this,11,"")
            if (myEmail.isNotEmpty()) {
                emailArray.put(myEmail.trim())
            }
        }

        val otherArray = JSONArray()
        if (checkbox_call_owner.isChecked) {
            otherArray.put("call_created_by")
        }
        if (checkbox_pic.isChecked) {
            otherArray.put("case_technical_pic")
        }
        if (checkbox_admin.isChecked) {
            otherArray.put("technical_admin")
        }

        val attachment = JSONArray()
        if (checkbox_checklist.isChecked) {
            attachment.put("checklist")
        }
        if (checkbox_training.isChecked) {
            attachment.put("training")
        }
        if (checkbox_handover.isChecked) {
            attachment.put("handover")
        }
        if (checkbox_completion_letter.isChecked) {
            attachment.put("installation_completion")
        }

        val params = HashMap<String, Any>()
        params["report_id"] = reportArray
        params["to"] = emailArray
        params["others"] = otherArray
        params["additional_attachment"] = attachment
        Log.e("aim","send email : $params")

        ApiClient.instance.sendEmail(token, params).enqueue(object: Callback<SendEmailRequest> {
            override fun onFailure(call: Call<SendEmailRequest>, t: Throwable) {
                progressDialog.dismiss()
                Toast.makeText(this@SendEmail, "Email saved, waiting for connection",Toast.LENGTH_LONG).show()

                Log.e("aim", "message : ${t.message}, stackTrace: ${t.stackTrace}")

                emailVM.insert(
                    EmailEntity(
                        0,
                        reportArray.toString() ?: "",
                        emailArray.toString() ?: "",
                        otherArray.toString() ?: "",
                        attachment.toString() ?: "",
                    )
                )
            }
            override fun onResponse(call: Call<SendEmailRequest>, response: Response<SendEmailRequest>) {
                progressDialog.dismiss()
                if (response.isSuccessful) {
                    Toast.makeText(this@SendEmail, "Email sent",Toast.LENGTH_LONG).show()
                    onBackPressed()
                } else {
                    if (response.code() == 401) {
                        FunHelper.clearAllData(this@SendEmail)
                        Toast.makeText(this@SendEmail, "Session end", Toast.LENGTH_LONG).show()
                    }
                    else {
                        FunHelper.firebaseApiLog(
                            userId ?: "",
                            response.raw().request().url().toString() ?: "",
                            response.raw().request().headers().toString() ?: "",
                            params.toString() ?: "",
                            response.raw().toString() ?: ""
                        )

                        Toast.makeText(this@SendEmail, response.raw().toString(), Toast.LENGTH_LONG).show()
                    }
                }
            }
        })
    }

}
