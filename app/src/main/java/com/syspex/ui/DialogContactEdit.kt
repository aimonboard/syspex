package com.syspex.ui

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.syspex.R
import kotlinx.android.synthetic.main.dialog_contact_edit.*


class DialogContactEdit : BottomSheetDialogFragment() {

    private val spinnerData = arrayOf("Contact type A","Contact type B")

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,savedInstanceState: Bundle?): View? {
        // Set dialog initial state when shown
        dialog?.setOnShowListener {
            val bottomSheetDialog = it as BottomSheetDialog
            val sheetInternal: View = bottomSheetDialog.findViewById(com.google.android.material.R.id.design_bottom_sheet)!!
            BottomSheetBehavior.from(sheetInternal).state = BottomSheetBehavior.STATE_EXPANDED
        }

        return inflater.inflate(R.layout.dialog_contact_edit, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        spinnerContact()
        generateView()

        buttonListener()

    }

    private fun generateView() {
        name_value.setText(arguments?.getString("name"))
        email_value.setText(arguments?.getString("email"))
        phone_value.setText(arguments?.getString("phone"))
        id_value.setText(arguments?.getString("employee_id"))

        val index = spinnerData.indexOf(arguments?.getString("contact_type"))
        Log.d("aim", "selected index: $index")
        spinner_value.setSelection(index)
    }

    private fun buttonListener() {
        btn_save.setOnClickListener {
            if (checkValue()) {
                activity?.onBackPressed()
            }
        }
    }

    private fun spinnerContact(){
        val arrayAdapter = ArrayAdapter(requireContext(), android.R.layout.simple_spinner_dropdown_item, spinnerData)
        spinner_value?.adapter = arrayAdapter

        spinner_value?.onItemSelectedListener = object : AdapterView.OnItemSelectedListener{
            override fun onNothingSelected(parent: AdapterView<*>?) { }
            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {

                when (position){
                    0 -> {

                    }
                    1 -> {

                    }
                }
            }
        }
    }

    private fun checkValue(): Boolean{
        var cek = 0

        if(name_value.text.toString().isEmpty()){
            name_err.error = "*Required"; cek++
        } else { name_err.error = "" }

        if(email_value.text.toString().isEmpty()){
            email_err.error = "*Required"; cek++
        } else { email_err.error = "" }

        if(phone_value.text.toString().isEmpty()){
            phone_err.error = "*Required"; cek++
        } else { phone_err.error = "" }

        if(id_value.text.toString().isEmpty()){
            id_err.error = "*Required"; cek++
        } else { id_err.error = "" }

        if (cek == 0){
            return true
        }
        return false
    }

}
