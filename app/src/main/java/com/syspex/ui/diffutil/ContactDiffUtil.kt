package com.syspex.ui.diffutil

import androidx.annotation.Nullable
import androidx.recyclerview.widget.DiffUtil
import com.syspex.data.local.database.account_contact.AccountContactEntity
import com.syspex.data.local.database.service_call.ServiceCallEntity

class ContactDiffUtil (private val oldList: List<AccountContactEntity>, private val newList: List<AccountContactEntity>) : DiffUtil.Callback() {

    override fun getOldListSize(): Int = oldList.size

    override fun getNewListSize(): Int = newList.size

    override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        return oldList[oldItemPosition].account_contact_id === newList[newItemPosition].account_contact_id
    }

    override fun areContentsTheSame(oldPosition: Int, newPosition: Int): Boolean {
        val (_, value, name) = oldList[oldPosition]
        val (_, value1, name1) = newList[newPosition]

        return name == name1 && value == value1
    }

    @Nullable
    override fun getChangePayload(oldPosition: Int, newPosition: Int): Any? {
        return super.getChangePayload(oldPosition, newPosition)
    }
}