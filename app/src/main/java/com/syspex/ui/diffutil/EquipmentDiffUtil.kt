package com.syspex.ui.diffutil

import androidx.annotation.Nullable
import androidx.recyclerview.widget.DiffUtil
import com.syspex.data.model.AccountList
import com.syspex.data.model.EquipmentListModel

class EquipmentDiffUtil (private val oldList: List<EquipmentListModel>, private val newList: List<EquipmentListModel>) : DiffUtil.Callback() {

    override fun getOldListSize(): Int = oldList.size

    override fun getNewListSize(): Int = newList.size

    override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        return oldList[oldItemPosition].equipmentId === newList[newItemPosition].equipmentId
    }

    override fun areContentsTheSame(oldPosition: Int, newPosition: Int): Boolean {
        val (_, value, name) = oldList[oldPosition]
        val (_, value1, name1) = newList[newPosition]

        return name == name1 && value == value1
    }

    @Nullable
    override fun getChangePayload(oldPosition: Int, newPosition: Int): Any? {
        return super.getChangePayload(oldPosition, newPosition)
    }
}