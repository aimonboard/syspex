package com.syspex.ui.diffutil

import androidx.annotation.Nullable
import androidx.recyclerview.widget.DiffUtil
import com.syspex.data.local.database.service_call.ServiceCallEntity

class CallDiffUtil (private val oldList: List<ServiceCallEntity>, private val newList: List<ServiceCallEntity>) : DiffUtil.Callback() {

    override fun getOldListSize(): Int = oldList.size

    override fun getNewListSize(): Int = newList.size

    override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        return oldList[oldItemPosition].serviceCallId === newList[newItemPosition].serviceCallId
    }

    override fun areContentsTheSame(oldPosition: Int, newPosition: Int): Boolean {
        val (_, value, name) = oldList[oldPosition]
        val (_, value1, name1) = newList[newPosition]

        return name == name1 && value == value1
    }

    @Nullable
    override fun getChangePayload(oldPosition: Int, newPosition: Int): Any? {
        return super.getChangePayload(oldPosition, newPosition)
    }
}