package com.syspex.ui

import android.app.Activity
import android.content.ContentResolver
import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.Matrix
import android.hardware.camera2.CameraCharacteristics
import android.hardware.camera2.CameraManager
import android.net.Uri
import android.os.Bundle
import android.os.Environment
import android.provider.MediaStore
import android.provider.OpenableColumns
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.content.FileProvider
import androidx.exifinterface.media.ExifInterface
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.livinglifetechway.quickpermissions_kotlin.runWithPermissions
import com.syspex.R
import com.syspex.data.model.ConverterImageModel
import com.syspex.ui.custom.CustomCamera
import com.syspex.ui.helper.FunHelper
import com.syspex.ui.helper.UploadFile
import kotlinx.android.synthetic.main.dialog_upload_file_fragment.*
import java.io.*
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList


class DialogUploadFile : BottomSheetDialogFragment() {

    private val choseImageCode = 2121
    private val cameraRequest = 2222

    private lateinit var cameraUri: Uri
    private var tempImageLocation = ""

    companion object {
        // use when access this modul
        var fileSection = ""
        var equipmentId = ""
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.dialog_upload_file_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        btn_photo.setOnClickListener {
            openCamera()
//            customCamera()
        }

        btn_file.setOnClickListener {
            openFile()
        }

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        val fileData = ArrayList<ConverterImageModel>()

        if (requestCode == choseImageCode && resultCode == Activity.RESULT_OK && data != null) {
            if (data.clipData != null) {
                // Multiple Chooser File
                for (i in 0 until data.clipData!!.itemCount) {
                    val fileUri = data.clipData!!.getItemAt(i).uri
                    val fileName = getFileName(fileUri)
                    val format = fileName.split(".")

                    if (checkExtension(fileName)) {
                        val internalData = copyFileFromUri(
                            fileUri,
                            "${imageNameServer()}.${format[format.lastIndex]}"
                        )
                        if (internalData != null) {
                            val uriInternalData = FileProvider.getUriForFile(
                                requireContext(),
                                "com.syspex.fileprovider",
                                internalData
                            )

                            fileData.add(
                                ConverterImageModel(
                                    "${imageNameServer()}.${format[format.lastIndex]}",
                                    uriInternalData.toString()
                                )
                            )
                        }
                    } else {
                        Toast.makeText(context, "Choose .jpg or .pdf file", Toast.LENGTH_LONG).show()
                    }
                }
            }
            else {
                // Single Chooser File
                val fileUri = data.data!!
                val fileName = getFileName(fileUri)
                val format = fileName.split(".")

                if (checkExtension(fileName)) {
                    val internalData = copyFileFromUri(
                        fileUri,
                        "${imageNameServer()}.${format[format.lastIndex]}"
                    )
                    if (internalData != null) {
                        val uriInternalData = FileProvider.getUriForFile(
                            requireContext(),
                            "com.syspex.fileprovider",
                            internalData
                        )

                        fileData.add(
                            ConverterImageModel(
                                "${imageNameServer()}.${format[format.lastIndex]}",
                                uriInternalData.toString()
                            )
                        )
                    }
                } else {
                    Toast.makeText(context, "Choose .jpg or .pdf file", Toast.LENGTH_LONG).show()
                }

            }

            imageCallBackRoute(fileData)

        }

        // Camera
        else if (requestCode == cameraRequest && resultCode == Activity.RESULT_OK) {

            val compressUri = FileProvider.getUriForFile(
                requireContext(),
                "com.syspex.fileprovider",
                imageCompression(
                    rotateImage()
                )
            )

//            copyExif(cameraUri.path.toString(), compressUri.path.toString())

            fileData.add(
                ConverterImageModel(
                    getFileName(compressUri),
                    compressUri.toString()
                )
            )

            Log.e("aim", "test compres: $fileData")

            imageCallBackRoute(fileData)

        }

        // Save DB and Upload
        if (fileSection != "attachment") {
            val uploadService = UploadFile(requireContext())
            uploadService.saveDB(fileData, fileSection)
        }

        dismiss()

    }

    private fun openFile() = runWithPermissions(
        FunHelper.storageReadPermission,
        FunHelper.storageWritePermission
    ) {
        when (fileSection) {
            "profile" -> {
                val intent = Intent(Intent.ACTION_OPEN_DOCUMENT).apply {
                    putExtra(Intent.EXTRA_ALLOW_MULTIPLE, false)
                    addCategory(Intent.CATEGORY_OPENABLE)
                    flags =
                        (Intent.FLAG_GRANT_READ_URI_PERMISSION and Intent.FLAG_GRANT_PERSISTABLE_URI_PERMISSION)
                    type = "*/*"
                }
                startActivityForResult(intent, choseImageCode)
            }
            "attachment" -> {
                val intent = Intent(Intent.ACTION_OPEN_DOCUMENT).apply {
                    putExtra(Intent.EXTRA_ALLOW_MULTIPLE, false)
                    addCategory(Intent.CATEGORY_OPENABLE)
                    flags =
                        (Intent.FLAG_GRANT_READ_URI_PERMISSION and Intent.FLAG_GRANT_PERSISTABLE_URI_PERMISSION)
                    type = "*/*"
                }
                startActivityForResult(intent, choseImageCode)
            }
            else -> {
                val intent = Intent(Intent.ACTION_OPEN_DOCUMENT).apply {
                    putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true)
                    addCategory(Intent.CATEGORY_OPENABLE)
                    flags = (Intent.FLAG_GRANT_READ_URI_PERMISSION and Intent.FLAG_GRANT_PERSISTABLE_URI_PERMISSION)
                    type = "*/*"
                }
                startActivityForResult(intent, choseImageCode)
            }
        }
    }

    private fun openCamera() = runWithPermissions(
        FunHelper.cameraPermission,
        FunHelper.storageWritePermission
    ) {
        Intent(MediaStore.ACTION_IMAGE_CAPTURE).also { takePictureIntent ->
            // Ensure that there's a camera activity to handle the intent
            takePictureIntent.resolveActivity(requireContext().packageManager)?.also {
                // Create the File where the photo should go
                val photoFile: File? = try {
                    createImageFile(imageNameServer())
                } catch (ex: IOException) { null }
                // Continue only if the File was successfully created
                photoFile?.also {
                    cameraUri = FileProvider.getUriForFile(
                        requireContext(),
                        "com.syspex.fileprovider",
                        it
                    )

                    takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, cameraUri)
                    startActivityForResult(takePictureIntent, cameraRequest)
                }
            }
        }
    }

    private fun customCamera() = runWithPermissions(
        FunHelper.cameraPermission,
        FunHelper.storageWritePermission
    ) {
        val intent = Intent(requireContext(), CustomCamera::class.java)
        startActivity(intent)
    }

    private fun testCamera () {
        val manager= requireContext().getSystemService(Context.CAMERA_SERVICE) as CameraManager
        try {
            val cameraId: String = manager.cameraIdList[0]
            val characteristics: CameraCharacteristics = manager.getCameraCharacteristics(cameraId)
            var orientation = characteristics.get(CameraCharacteristics.SENSOR_ORIENTATION) ?: 0
        } catch (e: java.lang.Exception) {
        }
    }

    @Throws(IOException::class)
    private fun createImageFile(prefix: String): File {
        // Create an image file name
        val timeStamp: String = SimpleDateFormat("yyyyMMdd_HHmmss", Locale.getDefault()).format(Date())
        val storageDir: File? = requireContext().getExternalFilesDir(Environment.DIRECTORY_PICTURES)
        val tempFile = File.createTempFile(
            prefix, /* prefix */
            ".jpg", /* suffix */
            storageDir /* directory */
        )

       tempImageLocation = tempFile.absolutePath

        return tempFile
    }

    @Throws(IOException::class)
    private fun rotateImage(): Bitmap {
        val bitmap = MediaStore.Images.Media.getBitmap(context?.contentResolver, cameraUri)
        val ei = ExifInterface(tempImageLocation)
        val orientation = ei.getAttributeInt(
            ExifInterface.TAG_ORIENTATION,
            ExifInterface.ORIENTATION_UNDEFINED
        )

        val matrix = Matrix()
        when (orientation) {
            ExifInterface.ORIENTATION_ROTATE_90 -> matrix.setRotate(90f)
            ExifInterface.ORIENTATION_ROTATE_180 -> matrix.setRotate(180f)
            ExifInterface.ORIENTATION_ROTATE_270 -> matrix.setRotate(270f)
        }

        return Bitmap.createBitmap(bitmap, 0, 0, bitmap.width, bitmap.height, matrix, true)
    }

    private fun imageCompression(bitmap: Bitmap): File {
//        val bitmap = MediaStore.Images.Media.getBitmap(context?.contentResolver, cameraUri)
        val storageDir = requireContext().getExternalFilesDir(Environment.DIRECTORY_PICTURES)
        val dataFile = File(storageDir, getFileName(cameraUri))

        return try {
            dataFile.createNewFile()

            //Convert bitmap to byte array
            val bos = ByteArrayOutputStream()
            bitmap.compress(Bitmap.CompressFormat.JPEG, 10, bos)
            val bitmapdata = bos.toByteArray()

            //write the bytes in file
            val fos = FileOutputStream(dataFile)
            fos.write(bitmapdata)
            fos.flush()
            fos.close()

            dataFile
        } catch (e: java.lang.Exception) {
            dataFile
        }
    }


    private fun getFileName(uri: Uri): String {
        var result: String? = null
        if (uri.scheme == "content") {
            val cursor = context?.contentResolver?.query(uri, null, null, null, null)
            try {
                if (cursor != null && cursor.moveToFirst()) {
                    result = cursor.getString(cursor.getColumnIndex(OpenableColumns.DISPLAY_NAME))
                }
            } catch (e: Exception) { }
            cursor?.close()
        }
        if (result == null) {
            result = uri.path
            val cut = result!!.lastIndexOf('/')
            if (cut != -1) {
                result = result.substring(cut + 1)
            }
        }
        return result
    }

    private fun copyFileFromUri(fileUri: Uri, fileName: String): File? {
        val inputStream: InputStream?
        val outputStream: OutputStream?
        var dataFile : File? = null

        try {
            val content: ContentResolver = requireContext().contentResolver
            inputStream = content.openInputStream(fileUri)
            val saveDirectory = requireContext().getExternalFilesDir(Environment.DIRECTORY_PICTURES)
            dataFile = File(saveDirectory, fileName)
            outputStream = FileOutputStream(dataFile)
            val buffer = ByteArray(1024)
            var bytesRead = 0
            while (inputStream!!.read(buffer, 0, buffer.size).also { bytesRead = it } >= 0) {
                outputStream.write(buffer, 0, buffer.size)
            }
        } catch (e: java.lang.Exception) {
            Log.e("aim", "Exception occurred " + e.message)
        }
        return dataFile
    }

    private fun checkExtension(fileName: String): Boolean {
        val isAllowedExtension: Boolean
        val extension = fileName.split(".")

        isAllowedExtension = when (extension[extension.lastIndex].toLowerCase(Locale.getDefault())) {
            "jpg" -> true
            "jpeg" -> true
            "pdf" -> true
            "png" -> true
            else -> false
        }

        return isAllowedExtension
    }

    private fun imageNameServer(): String {
        val uuid = FunHelper.randomString("")
        when (fileSection) {
            "profile" -> {
                return "UNKNOWN-$equipmentId-XXX-$uuid"
            }
            "problem" -> {
                return "SRE-$equipmentId-P-$uuid"
            }
            "cause" -> {
                return "SRE-$equipmentId-C-$uuid"
            }
            "solution" -> {
                return "SRE-$equipmentId-S-$uuid"
            }
            "checklist" -> {
                return "SRE-$equipmentId-CH-$uuid"
            }
            "attachment" -> {
                return "Attachment-$uuid"
            }
            else -> {
                return "UNKNOWN-$equipmentId-XXX-$uuid"
            }
        }
    }

    private fun imageCallBackRoute(fileData: ArrayList<ConverterImageModel>) {
        when (fileSection) {
            "profile" -> {
                fileData.forEach {
                    Profile.profileImage = it.fileUri
                }
            }
            "problem" -> {
                fileData.forEach {
                    EquipmentEdit.problemImage.add(it)
                }
            }
            "cause" -> {
                fileData.forEach {
                    CauseAdd.causeImage.add(it)
                }
            }
            "solution" -> {
                fileData.forEach {
                    CauseAdd.solutionImage.add(it)
                }
            }
            "checklist" -> {
                fileData.forEach {
                    ChecklistConfirm.checklistImage.add(it)
                }
            }
            "attachment" -> {
                fileData.forEach {
                    AttachmentAdd.attachmentFile.add(it)
                }
            }
        }
    }

}
