package com.syspex.ui

import android.app.Activity
import android.content.Intent
import android.content.res.Resources
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.CalendarView
import android.widget.NumberPicker
import android.widget.TimePicker
import androidx.appcompat.app.AppCompatActivity
import com.syspex.R
import kotlinx.android.synthetic.main.date_time_picker_activity.*
import java.text.DecimalFormat
import java.text.SimpleDateFormat
import java.util.*


class DateTimePicker : AppCompatActivity() {

    private var timeSelected = ""
    private var dateSelected = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.date_time_picker_activity)

        generateView()
        generateListener()

    }

    private fun generateView() {
        if (intent.hasExtra("title")) {
            tx_title.text = intent.getStringExtra("title")
        }
    }

    private fun generateListener() {

        time_value?.setIs24HourView(true)

        val interval = 15
        val formater = DecimalFormat("00")
        val numValues = 60 / interval
        val displayedValues = arrayOfNulls<String>(numValues)
        for (i in 0 until numValues) {
            displayedValues[i] = formater.format(i * interval)
        }

        var minutePicker: NumberPicker? = null
        val minuteNumberPicker = time_value?.findViewById<NumberPicker>(
            Resources.getSystem().getIdentifier(
                "minute",
                "id",
                "android"
            )
        )
        if (minuteNumberPicker != null) {
            minutePicker = minuteNumberPicker
            minutePicker.minValue = 0
            minutePicker.maxValue = numValues - 1
            minutePicker.displayedValues = displayedValues
        }

        time_value?.setOnTimeChangedListener{ timePicker: TimePicker, hour: Int, minute: Int ->
            if (minutePicker != null) {
                timeSelected = "${formater.format(hour)}:${formater.format(minutePicker.value * interval)}:00"
            }
        }

        date_value.setOnDateChangeListener{ calendarView: CalendarView, year: Int, month: Int, date: Int ->
            dateSelected = "$year-${DecimalFormat("00").format(month+1)}-${DecimalFormat("00").format(date)}"
            Log.d("aim", "dateSelected : $dateSelected")
        }

        btn_set.setOnClickListener {
            val rightNow = Calendar.getInstance()
            val hour = rightNow[Calendar.HOUR_OF_DAY]
            val sdf = SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault())
            val now = sdf.format(Date()).split(" ")

            if (minutePicker != null && timeSelected.isEmpty()) {
                timeSelected = "${formater.format(hour)}:${formater.format(minutePicker.value * interval)}:00"
            }

            val finalResult = "${dateSelected.ifEmpty { now[0] }} $timeSelected"
            Log.e("aim","datetime : $finalResult")

            val intent = Intent()
            intent.putExtra("dateTime", finalResult)
            setResult(Activity.RESULT_OK, intent)
            finish()

        }
    }
}
