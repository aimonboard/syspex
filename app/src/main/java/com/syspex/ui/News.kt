package com.syspex.ui

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.core.view.GravityCompat
import androidx.drawerlayout.widget.DrawerLayout
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.syspex.R
import com.syspex.data.local.SharedPreferenceData
import com.syspex.data.remote.NewsRequestTest
import com.syspex.data.retrofit.ApiClient
import com.syspex.ui.adapter.NewsAdapter
import com.syspex.ui.helper.FunHelper
import kotlinx.android.synthetic.main.news_fragment.*
import kotlinx.android.synthetic.main.news_fragment.swipe_container
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class News : Fragment() {

    private val newsAdapter = NewsAdapter()

    private var page = 1
    private var lastPage = 1

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.news_fragment, container, false)
    }

    override fun onStart() {
        super.onStart()

        buttonListener()
        setupAdapter()
        getNews()
        recyclerScrollListener()
    }

    private fun buttonListener(){
        swipe_container.setOnRefreshListener {
            page = 1
            newsAdapter.clear()
            getNews()
        }

        val drawerLayout: DrawerLayout = requireActivity().findViewById(R.id.drawer_layout)
        btn_menu?.setOnClickListener {
            if(!drawerLayout.isDrawerOpen(GravityCompat.START))
                drawerLayout.openDrawer(GravityCompat.START)
        }
    }

    private fun setupAdapter() {
        recycler_news?.layoutManager = LinearLayoutManager(context, RecyclerView.VERTICAL, false)
        recycler_news?.adapter = newsAdapter
    }

    private fun recyclerScrollListener() {
        recycler_news?.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                val linearLayoutManager = recyclerView.layoutManager as LinearLayoutManager
                val countItem = linearLayoutManager.itemCount
                val lastVisiblePosition = linearLayoutManager.findLastCompletelyVisibleItemPosition()
                val isLastPosition = countItem.minus(1) == lastVisiblePosition

                swipe_container.isEnabled = linearLayoutManager.findFirstCompletelyVisibleItemPosition() == 0

                if(isLastPosition && page < lastPage && newsAdapter.itemCount > 0){
                    page++
                    getNews()
                }
            }
        })
    }

    private fun getNews() {
        swipe_container.isRefreshing = true
        val token = "Bearer "+ SharedPreferenceData.getString(requireContext(),2, "")
        val userId = SharedPreferenceData.getString(requireContext(),8, "")

        ApiClient.instance.getNews(token, page).enqueue(object: Callback<NewsRequestTest> {
            override fun onFailure(call: Call<NewsRequestTest>, t: Throwable) {
                swipe_container?.isRefreshing = false
                Toast.makeText(requireContext(),"Connection failed", Toast.LENGTH_LONG).show()
            }
            override fun onResponse(call: Call<NewsRequestTest>, response: Response<NewsRequestTest>) {
                swipe_container?.isRefreshing = false
                if (!response.isSuccessful) {
                    if (response.code() == 401) {
                        FunHelper.clearAllData(requireContext())
                        Toast.makeText(requireContext(),"Session end", Toast.LENGTH_LONG).show()
                    } else {
                        FunHelper.firebaseApiLog(
                            userId ?: "",
                            response.raw().request().url().toString() ?: "",
                            response.raw().request().headers().toString() ?: "",
                            "",
                            response.raw().toString() ?: ""
                        )
                        Toast.makeText(requireContext(), response.raw().toString(), Toast.LENGTH_LONG).show()
                    }
                }
                else {

                    page = response.body()?.data?.page ?: 1
                    lastPage = response.body()?.data?.lastPage ?: 1

                    response.body()?.data?.news?.forEach {
                        if (it != null) {
                            newsAdapter.addData(it)
                        }
                    }
                }
            }
        })
    }

}
