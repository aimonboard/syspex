package com.syspex.ui

import android.annotation.SuppressLint
import android.os.Bundle
import android.util.Log
import android.view.View
import android.webkit.WebSettings
import android.webkit.WebView
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProviders
import com.syspex.R
import com.syspex.data.local.database.account_contact.AccountContactViewModel
import com.syspex.data.local.database.pcs_problem.ProblemEntity
import com.syspex.data.local.database.pcs_solution.SolutionEntity
import com.syspex.data.local.database.product_checklist_question.ChecklistQuestionEntity
import com.syspex.data.local.database.service_call.ServiceCallViewModel
import com.syspex.data.local.database.service_case.ServiceCaseViewModel
import com.syspex.data.local.database.service_report.ServiceReportViewModel
import com.syspex.data.local.database.service_report_equipment.ServiceReportEquipmentEntity
import com.syspex.data.local.database.service_report_equipment.ServiceReportEquipmentViewModel
import com.syspex.data.local.database.service_report_equipment_checklist.ServiceReportEquipmentChecklistEntity
import com.syspex.data.local.database.service_report_sparepart.ServiceReportSparepartEntity
import com.syspex.data.local.database.service_report_time_tracking.ServiceReportTimeTrackingViewModel
import com.syspex.data.model.SREquipmentModel
import com.syspex.ui.helper.FunHelper
import kotlinx.android.synthetic.main.activity_report_pdf_offline.*
import kotlinx.android.synthetic.main.activity_report_pdf_offline.back_link
import java.text.SimpleDateFormat


class ReportPdfOffline : AppCompatActivity() {

    private lateinit var caseVM: ServiceCaseViewModel
    private lateinit var callVM: ServiceCallViewModel
    private lateinit var reportVM: ServiceReportViewModel
    private lateinit var timeTrackingVM: ServiceReportTimeTrackingViewModel
    private lateinit var reportEquipmentVM: ServiceReportEquipmentViewModel
    private lateinit var accountContactVM: AccountContactViewModel

    private var accountSection = ""
    private var contactSection = ""
    private var callSection = ""
    private var equipmentSection = ""
    private var chargeSection = ""
    private var feedbackSection = ""
    private var signatureSection = ""
    private var timeTrackingSection = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_report_pdf_offline)

        caseVM = ViewModelProviders.of(this).get(ServiceCaseViewModel::class.java)
        callVM = ViewModelProviders.of(this).get(ServiceCallViewModel::class.java)
        reportVM = ViewModelProviders.of(this).get(ServiceReportViewModel::class.java)
        timeTrackingVM = ViewModelProviders.of(this).get(ServiceReportTimeTrackingViewModel::class.java)
        reportEquipmentVM = ViewModelProviders.of(this).get(ServiceReportEquipmentViewModel::class.java)
        accountContactVM = ViewModelProviders.of(this).get(AccountContactViewModel::class.java)

        generateView()
        buttonListener()

    }

    private fun generateView() {
        if (intent.hasExtra("reportId")) {
            Log.e("aim", "report id : ${intent.getStringExtra("reportId")}")
            getReport(intent.getStringExtra("reportId") ?: "")
        }
    }

    private fun buttonListener() {
        back_link.setOnClickListener {
            onBackPressed()
        }
    }

    private fun getReport(reportId: String) {
        reportVM.getByReportId(reportId).observe(this, {
            if (it.isNotEmpty()) {
                val chargeValue = when (it.first().isChargeable) {
                    "0" -> "Non Chargeable"
                    "1" -> "Chargeable"
                    "2" -> "FoC"
                    "3" -> "To be Adviced"
                    "4" -> "As per Agreement"
                    else -> "-"
                }
                chargeSection =
                    "<td>" +
                    "  <p class=\"bold\">Service Charge Type</p>" +
                    "</td>" +
                    "<td>" +
                    "  <p>$chargeValue</p>" +
                    "</td>" +
                    "<td>" +
                    "  <p class=\"bold\">Remark</p>" +
                    "</td>" +
                    "<td>" +
                    "  <p>-</p>" +
                    "</td>"

                feedbackSection =
                    "<tr class=\"border-top\">" +
                    "    <td>" +
                    "        <p>How would you rate the overall service provided?</p>" +
                    "    </td>" +
                    "    <td>" +
                    "        <p>${it.first().customerRating.ifEmpty { "-" }} Star</p>" +
                    "    </td>" +
                    "</tr>" +
                    "<tr>" +
                    "    <td colspan=\"2\">" +
                    "        <p class=\"font-9 bold\">Comment</p>" +
                    "        <p>${it.first().feedback.ifEmpty { "-" }}</p>" +
                    "    </td>" +
                    "</tr>"

                signatureSection =
                    "<div class=\"row\">" +
                    "    <div class=\"col-xs-12 section-header\">" +
                    "        <div class=\"row\">" +
                    "            <div class=\"col-xs-6\">" +
                    "                <p class=\"font-10 bold\">SIGNATURE</p>" +
                    "            </div>" +
                    "            <div class=\"col-xs-6\">" +
                    "                <p class=\"font-10\">Date: ${FunHelper.uiDateFormat(it.first().accountPicSignatureAt)}</p>" +
                    "            </div>" +
                    "        </div>" +
                    "    </div>" +

                    "    <div class=\"col-xs-12\">" +
                    "        <table class=\"table table-borderless\">" +
                    "            <tbody class=\"font-9_5\">" +
                    "                <tr class=\"border-top no-border-bottom font-9 bold\">" +
                    "                    <td>Serviced by,</td>" +
                    "                    <td>Service acknowledged by,</td>" +
                    "                </tr>" +
                    "                <tr class=\"no-border-top\">" +
                    "                    <td>" +
                    "                        <img src=\"./Service Report PDF - Detail_files/201211072236-EngineerSignature-49.jpg\" class=\"sign-img\">" +
                    "                        <p class=\"pt-5 mt-10 sign-name border-top-p mw-50\">${it.first().servicedByUserName.ifEmpty { "-" }}</p>" +
                    "                    </td>" +
                    "                    <td>" +
                    "                        <img src=\"./Service Report PDF - Detail_files/SRSIGN-1608344932305.jpg\" class=\"sign-img\">" +
                    "                        <p class=\"pt-5 mt-10 border-top-p mw-50\">" +
                    "                            <span class=\"sign-name\">${it.first().cpName.ifEmpty { "-" }}</span> " +
                    "                            <span class=\"sign-id\">(${it.first().cpNumber.ifEmpty { "-" }})</span>" +
                    "                        </p>" +
                    "                    </td>" +
                    "                </tr>" +
                    "            </tbody>" +
                    "        </table>" +
                    "    </div>" +
                    "</div>" +
                    "</div>"


                getCase(it.first().caseId)
                getCall(it.first().callId)
                getReportEquipment(reportId, it.first().reportTypeEnumText)
                getTimeTracking(reportId)
                loadContent()


            }
        })
    }

    private fun getCase(caseId: String) {
        caseVM.getByCaseId(caseId).observe(this, {
            if (it.isNotEmpty()) {
                accountSection =
                    "<td>" +
                    "    <p class=\"font-9 bold\">Customer Name</p>" +
                    "    <p class=\"font-9_5\">${it.first().accountName.ifEmpty { "-" }}</p>" +
                    "</td>" +
                    "<td colspan=\"2\">" +
                    "    <p class=\"font-9 bold\">Address</p>" +
                    "    <p class=\"font-9_5\">${it.first().addressText.ifEmpty { "-" }}</p>" +
                    "</td>"

                getContact(it.first().caseAccountContactId)
                loadContent()
            }
        })
    }

    private fun getContact(contactId: String) {
        accountContactVM.getById(contactId).observe(this, {
            if (it.isNotEmpty()) {
                contactSection =
                    "<td>" +
                    "    <p class=\"font-9 bold\">Contact Person Name</p>" +
                    "    <p class=\"font-9_5\">${it.first().contact_person_name.ifEmpty { "-" }}</p>" +
                    "</td>" +
                    "<td>" +
                    "    <p class=\"font-9 bold\">Contact Number</p>" +
                    "    <p class=\"font-9_5\">${it.first().contact_person_phone.ifEmpty { "-" }}</p>" +
                    "</td>" +
                    "<td>" +
                    "    <p class=\"font-9 bold\">Contact Email</p>" +
                    "    <p class=\"font-9_5\">${it.first().contact_person_email.ifEmpty { "-" }}</p>" +
                    "</td>"

                loadContent()
            }
        })
    }

    private fun getCall(callId: String) {
        callVM.getByCallId(callId).observe(this, {
            if (it.isNotEmpty()) {
                callSection =
                    "<td>" +
                    "    <p class=\"font-9 bold\">Service Call Subject</p>" +
                    "    <p class=\"font-9_5\">${it.first().serviceCallSubject.ifEmpty { "-" }}</p>" +
                    " </td>" +
                    " <td colspan=\"2\">" +
                    "    <p class=\"font-9 bold\">Service Type</p>" +
                    "    <p class=\"font-9_5\">${it.first().callTypeEnumText.ifEmpty { "-" }}</p>" +
                    " </td>" +
                    " <td>" +
                    "    <p class=\"font-9 bold\">Serviced By</p>" +
                    "    <p class=\"font-9_5\">${it.first().leadEngineerName.ifEmpty { "-" }}</p>" +
                    " </td>"

                loadContent()
            }
        })
    }

    private fun getReportEquipment(reportId: String, reportType: String) {
        reportEquipmentVM.getEquipmentDetail(reportId).observe(this, { reportEquipmentMain ->
            if (reportEquipmentMain.isNotEmpty()) {

                val equipmnetList = mutableListOf<ServiceReportEquipmentEntity>()
                val problemList = mutableListOf<ProblemEntity>()
                val solutionList = mutableListOf<SolutionEntity>()
                val sparepartList = mutableListOf<ServiceReportSparepartEntity>()
                val questionMaster = mutableListOf<ChecklistQuestionEntity>()
                val questionReport = mutableListOf<ServiceReportEquipmentChecklistEntity>()

                reportEquipmentMain.forEach { equipment ->

                    // ============================================================================= Report Equipment
                    val equipmentData = ServiceReportEquipmentEntity(
                        equipment.serviceReportEquipmentId ?: "",
                        equipment.serviceReportEquipmentCallId ?: "",
                        equipment.reportId ?: "",
                        equipment.equipmentId ?: "",
                        equipment.problemSymptom ?: "",
                        equipment.statusAfterServiceEnumId ?: "",
                        equipment.statusAfterServiceEnumName ?: "",
                        equipment.statusAfterServiceRemarks ?: "",
                        equipment.installationStartDate ?: "",
                        equipment.installationMatters ?: "",
                        equipment.checklistTemplateId ?: "",
                        "",
                        "",
                        "",
                        "",
                        equipment.equipmentName ?: "",
                        equipment.regionId ?: "",
                        equipment.serialNo ?: "",
                        equipment.brand ?: "",
                        equipment.warrantyStartDate ?: "",
                        equipment.warrantyEndDate ?: "",
                        equipment.equipmentRemarks ?: "",
                        equipment.deliveryAddressId ?: "",
                        equipment.salesOrderNo ?: "",
                        equipment.warrantyStatus ?: "",
                        equipment.accountId ?: "",
                        equipment.accountName ?: "",
                        equipment.agreementId ?: "",
                        equipment.agreementNumber ?: "",
                        equipment.agreementEnd ?: "",
                        equipment.statusTextEnumId ?: "",
                        equipment.statusTextEnumText ?: "",
                        equipment.product_id ?: "",
                        equipment.product_name,
                        false,
                        false,
                        false
                    )
                    if (!equipmnetList.contains(equipmentData)) {
                        equipmnetList.add(equipmentData)
                    }

                    // ============================================================================= Problem Image
                    val problemData = ProblemEntity(
                        equipment.reportEquipmentId_problem ?: "",
                        equipment.problemImage ?: emptyList()
                    )
                    if (!problemList.contains(problemData)) {
                        problemList.add(problemData)
                    }

                    // ============================================================================= Cause Solution
                    val solutionData = SolutionEntity(
                        equipment.serviceReportEquipmentSolutionId ?: "",
                        equipment.reportEquipmentId_solution ?: "",
                        equipment.subject ?: "",
                        equipment.problemCause ?: "",
                        equipment.solution ?: "",
                        "",
                        "",
                        "",
                        "",
                        equipment.causeImage ?: emptyList(),
                        equipment.solutionImage ?: emptyList(),
                        false,
                        false
                    )
                    if (!solutionList.contains(solutionData)) {
                        solutionList.add(solutionData)
                    }

                    // ============================================================================= Sparepart
                    val sparepartData = ServiceReportSparepartEntity(
                        equipment.service_report_sparepart_id ?: "",
                        equipment.reportEquipmentId_sparepart ?: "",
                        equipment.spare_part_image_number ?: "",
                        equipment.spare_part_number ?: "",
                        equipment.spare_part_description ?: "",
                        equipment.spare_part_memo ?: "",
                        equipment.enum_id ?: "",
                        equipment.enum_name ?: "",
                        equipment.part_id ?: "",
                        equipment.part_no ?: "",
                        equipment.part_name ?: "",
                        equipment.qty ?: "",
                        false,
                        false
                    )
                    if (!sparepartList.contains(sparepartData)) {
                        sparepartList.add(sparepartData)
                    }

                    // ============================================================================= Master Checklist Count
                    val questionDataMaster = ChecklistQuestionEntity(
                        equipment.questionId ?: "",
                        "",
                        "",
                        "",
                        "",
                        "",
                        "",
                        "",
                        "",
                        "",
                        "",
                        equipment.templateId ?: ""
                    )
                    if (!questionMaster.contains(questionDataMaster)) {
                        questionMaster.add(questionDataMaster)
                    }

                    // ============================================================================= Report Checklist Count
                    val questionReportData = ServiceReportEquipmentChecklistEntity(
                        "",
                        equipment.report_equipment_id ?: "",
                        "",
                        "",
                        "",
                        "",
                        "",
                        "",
                        equipment.checklist_status_id ?: "",
                        equipment.checklist_status_Name ?: "",
                        equipment.question_id ?: "",
                        "",
                        "",
                        "",
                        "",
                        "",
                        "",
                        "",
                        "",
                        "",
                        "",
                        "",
                        "",
                        emptyList(),
                        false
                    )
                    if (!questionReport.contains(questionReportData)) {
                        questionReport.add(questionReportData)
                    }

                }

                equipmnetList.forEachIndexed { index, equipment ->

                    val problemFilter =
                        problemList.filter { it.reportEquipmentId_problem == equipment.serviceReportEquipmentId }
                    val solutionFilter =
                        solutionList.filter { it.reportEquipmentId_solution == equipment.serviceReportEquipmentId }
                    val sparepartFilter =
                        sparepartList.filter { it.reportEquipmentId_sparepart == equipment.serviceReportEquipmentId }
                    val checklistMasterFilter =
                        questionMaster.filter { it.templateId == equipment.checklistTemplateId }
                    val checklistReportFilter =
                        questionReport.filter { it.report_equipment_id == equipment.serviceReportEquipmentId }

                    var solutionSection = ""
                    solutionFilter.forEachIndexed { indexSolution, solutionData->
                        solutionSection +=
                            "      <tr class=\"border-top\">" +
                            "        <td colspan=\"3\">" +
                            "          <p class=\"font-10\">" +
                            "             <span class=\"bold\">Issue/Subject (${indexSolution+1}/${solutionFilter.size}) : </span>${solutionData.subject}" +
                            "          </p>" +
                            "        </td>" +
                            "      </tr>" +

                            "      <!-- problem cause and solution -->" +
                            "      <tr>" +
                            "        <td>" +
                            "          <p class=\"font-9 bold\">Problem Cause</p>" +
                            "          <p class=\"font-9_5 pre-line\">${solutionData.problemCause.ifEmpty { "-" }}</p>" +
                            "          <div class=\"row\"></div>" +
                            "        </td>" +
                            "        <td colspan=\"2\">" +
                            "          <p class=\"font-9 bold\">Solution/Action Taken</p>" +
                            "          <p class=\"font-9_5 pre-line\">${solutionData.solution.ifEmpty { "-" }}</p>" +
                            "          <div class=\"row\"></div>" +
                            "        </td>" +
                            "      </tr>"
                    }

                    var sparepartSection = ""
                    if (sparepartFilter.isEmpty()) {
                        sparepartSection =
                            "<tr>" +
                            "  <td colspan=\"5\" class=\"text-center\">No Data</td>" +
                            "</tr>"
                    } else {
                        sparepartFilter.forEachIndexed { indexSparepart, sparepartData ->
                            sparepartSection +=
                                "<tr>" +
                                "  <td>${indexSparepart+1}</td>" +
                                "  <td>${sparepartData.part_no.ifEmpty { "-" }}</td>" +
                                "  <td>${sparepartData.spare_part_description.ifEmpty { "-" }}</td>" +
                                "  <td>${sparepartData.qty.ifEmpty { "-" }}</td>" +
                                "  <td>${sparepartData.enum_name.ifEmpty { "-" }}</td>" +
                                "</tr>"
                        }
                    }

                    equipmentSection +=
                        "      <!-- table service detail start -->" +
                        "      <div class=\"row\">" +
                        "        <div class=\"col-xs-12\">" +
                        "          <table class=\"table table-borderless\">" +
                        "            <!-- equipment detail -->" +
                        "            <tbody><tr class=\"gray border-top\">" +
                        "              <td>" +
                        "                <p class=\"font-9 bold\">Equipment Detail#${index+1}</p>" +
                        "                <p class=\"font-9_5\">${equipment.product_name.ifEmpty { "-" }}</p>" +
                        "              </td>" +
                        "              <td width=\"28%\">" +
                        "                <p class=\"font-9 bold\">Serial Number</p>" +
                        "                <p class=\"font-9_5\">${equipment.serialNo.ifEmpty { "-" }}</p>" +
                        "              </td>" +
                        "              <td width=\"28%\">" +
                        "                <p class=\"font-9 bold\">Warranty Expire Date</p>" +
                        "                <p class=\"font-9_5\">${FunHelper.uiDateFormat(equipment.warrantyEndDate).ifEmpty { "-" }}</p>" +
                        "              </td>" +
                        "            </tr>" +

                        "            <!-- agreement detail -->" +
                        "            <tr>" +
                        "              <td>" +
                        "                <p class=\"font-9 bold\">Type</p>" +
                        "                <p class=\"font-9_5\">$reportType</p>" +
                        "              </td>" +
                        "              <td>" +
                        "                <p class=\"font-9 bold\">Agreement No.</p>" +
                        "                <p class=\"font-9_5\">${equipment.agreementNumber.ifEmpty { "-" }}</p>" +
                        "              </td>" +
                        "              <td>" +
                        "                <p class=\"font-9 bold\">Agreement Expire Date</p>" +
                        "                <p class=\"font-9_5\">${FunHelper.uiDateFormat(equipment.agreementEnd).ifEmpty { "-" }}</p>" +
                        "              </td>" +
                        "            </tr>" +

                        "            <!-- status -->" +
                        "            <tr>" +
                        "              <td>" +
                        "                <p class=\"font-9 bold\">Status After Serviced</p>" +
                        "                <p class=\"font-9_5\">${equipment.statusAfterServiceEnumName.ifEmpty { "-" }}</p>" +
                        "              </td>" +
                        "              <td colspan=\"2\">" +
                        "                <p class=\"font-9 bold\">Status Remarks</p>" +
                        "                <p class=\"font-9_5\">${equipment.statusAfterServiceRemarks.ifEmpty { "-" }}</p>" +
                        "              </td>" +
                        "            </tr>" +

                        "            <!-- problem symptom -->" +
                        "            <tr class=\"border-top\">" +
                        "              <td>" +
                        "                <p class=\"font-9 bold\">Problem Symptom</p>" +
                        "                <p class=\"font-9_5 pre-line\">${equipment.problemSymptom.ifEmpty { "-" }}</p>" +
                        "              </td>" +
                        "              <td colspan=\"2\">" +
                        "                <div class=\"row\">" +
                        "                </div>" +
                        "              </td>" +
                        "            </tr>" +

                        "            $solutionSection" +

                        "          </tbody></table>" +
                        "        </div>" +
                        "      </div>" +

                        "      <!-- Sparepart -->" +
                        "      <div class=\"row\">" +
                        "        <div class=\"col-xs-12\">" +
                        "          <p class=\"font-10 bold\">Parts Supplied / Replaced / To Be Quoted</p>" +
                        "        </div>" +
                        "        <div class=\"col-xs-12\">" +
                        "          <table class=\"table table-borderless\">" +
                        "            <thead>" +
                        "              <tr class=\"border-top border-bottom\">" +
                        "                <th>No</th>" +
                        "                <th>Part No</th>" +
                        "                <th>Part Description</th>" +
                        "                <th>Qty</th>" +
                        "                <th>Type</th>" +
                        "              </tr>" +
                        "            </thead>" +
                        "            <tbody>" +
                        "                <tr>" +
                        "                  $sparepartSection" +
                        "                </tr>" +
                        "            </tbody>" +
                        "          </table>" +
                        "        </div>" +
                        "      </div>"

                    loadContent()
                }
            }
        })
    }

    @SuppressLint("SimpleDateFormat")
    private fun getTimeTracking(reportId: String) {
        val format = SimpleDateFormat("yyyy-MM-dd HH:mm")

        timeTrackingVM.getByReportId(reportId).observe(this, {
            if (it.isNotEmpty()) {
                var timeTrackingTotal : Long = 0
                var timeTrackingLoop = ""

                it.forEach { timeTrackingData ->
                    var diffHours : Long = 0
                    val dateStart = timeTrackingData.start.split(" ")
                    val start = timeTrackingData.start.split(" ")
                    val end = timeTrackingData.end.split(" ")

                    Log.e("aim", "start : ${timeTrackingData.start} end : ${timeTrackingData.end}")

                    try {
                        val d1 = format.parse(timeTrackingData.start)
                        val d2 = format.parse(timeTrackingData.end)

                        Log.e("aim", "diff : ${d1.time} - ${d2.time} = ${d1.time - d2.time}")

                        val diff = d2!!.time - d1!!.time
                        diffHours = (diff / (60 * 60 * 1000) % 24)
                        timeTrackingTotal += diffHours
                    } catch (e: Exception) {
                        e.stackTrace
                    }

                    timeTrackingLoop +=
                        "<tr>" +
                        "  <td>${FunHelper.uiDateFormat(dateStart.first())}</td>" +
                        "  <td>${start.last()}</td>" +
                        "  <td>${end.last()}</td>" +
                        "  <td>$diffHours</td>" +
                        "</tr>"
                }

                timeTrackingSection =
                    "<div class=\"row\">" +
                    "  <div class=\"col-xs-12 section-header\">" +
                    "    <p class=\"font-10 bold pull-left\">SUMMARY TIME TRACKING</p>" +
                    "    <p class=\"font-10 pull-right\">Total Hours : $timeTrackingTotal</p>" +
                    "  </div>" +
                    "  <div class=\"col-xs-12\">" +
                    "    <table class=\"table table-borderless\">" +
                    "      <thead>" +
                    "        <tr class=\"border-top border-bottom font-9\">" +
                    "          <th>Date Start</th>" +
                    "          <th>Time Start</th>" +
                    "          <th>Time End </th>" +
                    "          <th>Hours</th>" +
                    "        </tr>" +
                    "      </thead>" +
                    "      <tbody class=\"font-9_5\">" +
                    "              $timeTrackingLoop" +
                    "       </tbody>" +
                    "    </table>" +
                    "  </div>" +
                    "</div>"

                loadContent()
            }
        })
    }

    @SuppressLint("SetJavaScriptEnabled")
    private fun loadContent() {
        val headHtml =
            "<!DOCTYPE html>" +
            "<html lang=\"en\"><head><meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">" +
            "<meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">" +
            "<meta name=\"viewport\" content=\"width=device-width, initial-scale=1, shrink-to-fit=no\">" +
            "<meta name=\"description\" content=\"\">" +
            "<meta name=\"author\" content=\"\">" +
            "<title>Service Report PDF - Detail" +
            "</title>" +
            "<link rel=\"stylesheet\" type=\"text/css\" href=\"./Service Report PDF - Detail_files/bootstrap_3.4.min.css\">" +
            "<link rel=\"stylesheet\" type=\"text/css\" href=\"./Service Report PDF - Detail_files/bootstrap-theme-3.4.min.css\">" +
            "<link rel=\"stylesheet\" type=\"text/css\" href=\"./Service Report PDF - Detail_files/font-awesome.min.css\">" +
            "<style type=\"text/css\">" +
            "  .bold {" +
            "    font-weight: bold;" +
            "  }" +
            "  .arial {" +
            "    font-family: Arial, Helvetica, sans-serif;" +
            "  }" +
            "  .font-9 {" +
            "    font-size: 9pt;" +
            "  }" +
            "  .font-9_5 {" +
            "    font-size: 9.5pt;" +
            "  }" +
            "  .font-10 {" +
            "    font-size: 10pt;" +
            "  }" +
            "  .font-14 {" +
            "    font-size: 14pt;" +
            "  }" +
            "  .gray {" +
            "    background-color: #EAEAEA;" +
            "  }" +
            "  .width-25 {" +
            "    width: 25%;" +
            "  }" +
            "  .pt-5 {" +
            "    padding-top: 5px;" +
            "  }" +
            "  .mw-50 {" +
            "    max-width: 50%;" +
            "  }" +
            "  .mt-10 {" +
            "    margin-top: 10px;" +
            "  }" +
            "  .no-border-bottom td," +
            "  .no-border-bottom th {" +
            "    border-bottom: 0 !important;" +
            "  }" +
            "  .no-border-top td," +
            "  .no-border-top th {" +
            "    border-top: 0 !important;" +
            "  }" +
            "  .detail-table {" +
            "    max-width: 70%;" +
            "    min-width: 400px;" +
            "  }" +
            "  .table-borderless tr td, .table-borderless tr th {" +
            "    border: 1px solid #E1E0E0;" +
            "  }" +
            "  .border-top td," +
            "  .border-top th {" +
            "    border-top: 2px solid #000 !important;" +
            "  }" +
            "  .border-bottom th {" +
            "    border-bottom: 2px solid #000 !important;" +
            "  }" +
            "  .border-top-p {" +
            "    border-top: 1px solid #000;" +
            "  }" +
            "  .table-bordered-rounded {" +
            "    min-width: 600px;" +
            "    border: 1px solid #D9DDE0;" +
            "    border-radius: 10px;" +
            "    border-spacing: 0;" +
            "    border-collapse: separate;" +
            "  }" +
            "  .table-bordered-rounded thead th {" +
            "    border-top: none;" +
            "    border-bottom: none;" +
            "    background: #FBFBFB;" +
            "    color: #636D77;" +
            "  }" +
            "  .table-bordered-rounded thead th:first-child{" +
            "    border-top-left-radius: 10px;" +
            "  }" +
            "  .table-bordered-rounded thead th:last-child{" +
            "    border-top-right-radius: 10px;" +
            "  }" +
            "  .table-bordered-rounded tbody {" +
            "    background: #fff;" +
            "  }" +
            "  .table-bordered-rounded tbody tr:last-child td:first-child {" +
            "    border-bottom-left-radius: 10px;" +
            "  }" +
            "  .table-bordered-rounded tbody tr:last-child td:last-child {" +
            "    border-bottom-right-radius: 10px;" +
            "  }" +
            "  .total-time {" +
            "    vertical-align: bottom !important;" +
            "    text-align: center;" +
            "  }" +
            "  .total-time-content {" +
            "    font-weight: bold;" +
            "  }" +
            "  .total-time-content p {" +
            "    margin-bottom: 0;" +
            "  }" +
            "  .total-time-content p:last-child {" +
            "    font-size: 12px;" +
            "  }" +
            "  .total-time-content hr {" +
            "    margin-top: 5px;" +
            "    margin-bottom: 5px;" +
            "  }" +
            "  .time-tracking-table thead th:last-child {" +
            "    border-left: 1px solid #D9DDE0;" +
            "    border-top-left-radius: 10px;" +
            "  }" +
            "  .time-tracking-table tbody tr:first-child td:last-child {" +
            "    border-left: 1px solid #D9DDE0;" +
            "    border-bottom-left-radius: 10px;" +
            "    border-bottom-right-radius: 10px;" +
            "  }" +
            "  .sign-name {" +
            "    text-decoration: underline;" +
            "  }" +
            "  .sign-img {" +
            "    width: 100px;" +
            "  }" +
            "  .sign-id {" +
            "    color: #636D77;" +
            "    font-size: 12px;" +
            "  }" +
            "  .pre-line{" +
            "    white-space: pre-line;" +
            "  }" +
            "  .fit-content-width{" +
            "    min-width: fit-content;" +
            "  }" +
            "</style>" +
            "</head>"


        val masterHtml =
            headHtml +
            // ===================================================================================== Body
            "<body id=\"page-top\">" +
            "<div id=\"content-wrapper\" class=\"container-fluid\">" +
            "  <div class=\"container-fluid\">" +
            "    <div class=\"row\">" +
            "      <div class=\"col-xs-12\">" +
            "        <p class=\"font-14 arial bold text-center\">SERVICE REPORT</p>" +
            "      </div>" +
            "    </div>" +

            "    <!-- table customer detail start -->" +
            "    <div class=\"row\">" +
            "      <div class=\"col-xs-12\">" +
            "        <p class=\"font-10 bold\">CUSTOMER DETAILS</p>" +
            "      </div>" +
            "      <div class=\"col-xs-12\">" +
            "        <table class=\"table table-borderless\">" +
            "          <tbody><tr class=\"border-top\">" +
            "            $accountSection" +
            "          </tr>" +
            "          <tr>" +
            "            $contactSection" +
            "          </tr>" +
            "        </tbody></table>" +
            "      </div>" +
            "    </div>" +

            "    <!-- table service detail start -->" +
            "    <div class=\"row\">" +
            "      <div class=\"col-xs-12\">" +
            "        <p class=\"font-10 bold\">SERVICE DETAIL</p>" +
            "      </div>" +
            "      <div class=\"col-xs-12\">" +
            "        <table class=\"table table-borderless\">" +
            "          <tbody><tr class=\"border-top\">" +
            "            $callSection" +
            "          </tr>" +
            "        </tbody></table>" +
            "      </div>" +
            "    </div>" +

            "    $equipmentSection" +

            "    $timeTrackingSection" +

            "    <!-- table service charge start -->" +
            "    <div class=\"row\">" +
            "      <div class=\"col-xs-12 section-header\">" +
            "        <p class=\"font-10 bold\">SERVICE CHARGE</p>" +
            "      </div>" +
            "      <div class=\"col-xs-12\">" +
            "        <table class=\"table table-borderless\">" +
            "          <tbody class=\"font-9_5\">" +
            "            <tr class=\"border-top\">" +
            "            $chargeSection" +
            "            </tr>" +
            "          </tbody>" +
            "        </table>" +
            "      </div>" +
            "    </div>" +
            "    <!-- table service charge end -->" +
            "    <!-- table customer feedback start -->" +
            "    <div class=\"row\">" +
            "      <div class=\"col-xs-12 section-header\">" +
            "        <p class=\"font-10 bold\">CUSTOMER FEEDBACK</p>" +
            "      </div>" +
            "      <div class=\"col-xs-12\">" +
            "        <table class=\"table table-borderless\">" +
            "          <tbody class=\"font-9_5\">" +
            "            $feedbackSection" +
            "          </tbody>" +
            "        </table>" +
            "      </div>" +
            "    </div>" +

            "    $signatureSection" +

            "<!-- End Page Content -->" +
            "</div>" +
            "<!-- End of Content Wrapper -->" +
            "<!-- Scroll to Top Button-->" +
            "<!-- Bootstrap core JavaScript-->" +
            "<!-- <script src=\"/Users/pashatw/00 PROJECT/_LARAVEL/_PENTING/syspex-web/public/js/jquery-3.5.1.slim.min.js\"></script> -->" +
            "<!-- <script src=\"http://localhost:8001/js/popper.min.js\"></script> -->" +
            "<!-- <script src=\"http://localhost:8001/js/bootstrap.min.js\"></script> -->" +
            "<!-- script extra start -->" +
            "<script type=\"text/javascript\">" +
            "  window.onload = function(e) {" +
            "    document.getElementById('total_hour').innerHTML = '5'" +
            "  }" +
            "</script>" +
            "<!-- script extra end -->" +
            "</body></html>"

        webView.settings.layoutAlgorithm = WebSettings.LayoutAlgorithm.SINGLE_COLUMN
        webView.scrollBarStyle = WebView.SCROLLBARS_OUTSIDE_OVERLAY
        webView.fitsSystemWindows = true
        webView.settings.javaScriptEnabled = true

        webView.loadDataWithBaseURL(null, masterHtml , "text/html", "utf-8", null)
    }

}