package com.syspex.ui

import android.annotation.SuppressLint
import android.app.ProgressDialog
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.appcompat.app.AlertDialog
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.livinglifetechway.quickpermissions_kotlin.runWithPermissions

import com.syspex.R
import com.syspex.data.local.SharedPreferenceData
import com.syspex.data.local.database.account_contact.AccountContactEntity
import com.syspex.data.local.database.account_contact.AccountContactViewModel
import com.syspex.data.local.database.pcs_problem.ProblemEntity
import com.syspex.data.local.database.pcs_solution.SolutionEntity
import com.syspex.data.local.database.product_checklist_question.ChecklistQuestionEntity
import com.syspex.data.local.database.service_call.ServiceCallViewModel
import com.syspex.data.local.database.service_case.ServiceCaseViewModel
import com.syspex.data.local.database.service_report.ServiceReportViewModel
import com.syspex.data.local.database.service_report_equipment.ServiceReportEquipmentEntity
import com.syspex.data.local.database.service_report_equipment.ServiceReportEquipmentViewModel
import com.syspex.data.local.database.service_report_equipment_checklist.ServiceReportEquipmentChecklistEntity
import com.syspex.data.local.database.service_report_sparepart.ServiceReportSparepartEntity
import com.syspex.data.local.database.service_report_time_tracking.ServiceReportTimeTrackingViewModel
import com.syspex.data.model.SREquipmentModel
import com.syspex.data.retrofit.ApiClient
import com.syspex.ui.adapter.*
import com.syspex.ui.helper.*
import kotlinx.android.synthetic.main.service_report_preview_fragment.*
import kotlinx.android.synthetic.main.service_report_preview_fragment.account_pic_contact_name
import kotlinx.android.synthetic.main.service_report_preview_fragment.account_pic_contact_phone
import kotlinx.android.synthetic.main.service_report_preview_fragment.account_pic_contact_type
import kotlinx.android.synthetic.main.service_report_preview_fragment.btn_add_equipment
import kotlinx.android.synthetic.main.service_report_preview_fragment.btn_cancel
import kotlinx.android.synthetic.main.service_report_preview_fragment.btn_edit_contact
import kotlinx.android.synthetic.main.service_report_preview_fragment.btn_info_pdf
import kotlinx.android.synthetic.main.service_report_preview_fragment.btn_phone2
import kotlinx.android.synthetic.main.service_report_preview_fragment.btn_send_email
import kotlinx.android.synthetic.main.service_report_preview_fragment.btn_service_call
import kotlinx.android.synthetic.main.service_report_preview_fragment.btn_signature
import kotlinx.android.synthetic.main.service_report_preview_fragment.btn_time_tracking_add
import kotlinx.android.synthetic.main.service_report_preview_fragment.feedback_lay
import kotlinx.android.synthetic.main.service_report_preview_fragment.feedback_warning
import kotlinx.android.synthetic.main.service_report_preview_fragment.info_call_desc
import kotlinx.android.synthetic.main.service_report_preview_fragment.info_call_end
import kotlinx.android.synthetic.main.service_report_preview_fragment.info_call_start
import kotlinx.android.synthetic.main.service_report_preview_fragment.info_call_status
import kotlinx.android.synthetic.main.service_report_preview_fragment.info_call_subject
import kotlinx.android.synthetic.main.service_report_preview_fragment.info_case_type
import kotlinx.android.synthetic.main.service_report_preview_fragment.info_created_date_value
import kotlinx.android.synthetic.main.service_report_preview_fragment.info_created_value
import kotlinx.android.synthetic.main.service_report_preview_fragment.info_equipment_value
import kotlinx.android.synthetic.main.service_report_preview_fragment.info_lead_engineer
import kotlinx.android.synthetic.main.service_report_preview_fragment.info_service_value
import kotlinx.android.synthetic.main.service_report_preview_fragment.rating
import kotlinx.android.synthetic.main.service_report_preview_fragment.rating_text
import kotlinx.android.synthetic.main.service_report_preview_fragment.recycler_equipment
import kotlinx.android.synthetic.main.service_report_preview_fragment.recycler_time_tracking
import kotlinx.android.synthetic.main.service_report_preview_fragment.report_number
import kotlinx.android.synthetic.main.service_report_preview_fragment.report_status
import kotlinx.android.synthetic.main.service_report_preview_fragment.signature_date_value
import kotlinx.android.synthetic.main.service_report_preview_fragment.signature_pad
import kotlinx.android.synthetic.main.service_report_preview_fragment.signature_user
import kotlinx.android.synthetic.main.service_report_preview_fragment.time_tracking_lay
import kotlinx.android.synthetic.main.service_report_preview_fragment.tx_equipment_count
import kotlinx.android.synthetic.main.service_report_preview_fragment.tx_time_tracking_count
import kotlinx.android.synthetic.main.service_report_preview_fragment.warning_equipment
import kotlinx.android.synthetic.main.service_report_preview_fragment.warning_time_tracking
import okhttp3.ResponseBody
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.text.SimpleDateFormat
import java.util.*

class ServiceReportPreview : BottomSheetDialogFragment() {

    companion object {
        var reportId = ""
    }

    private lateinit var caseVM: ServiceCaseViewModel
    private lateinit var contactVM: AccountContactViewModel
    private lateinit var callVM: ServiceCallViewModel
    private lateinit var reportVM: ServiceReportViewModel
    private lateinit var reportEquipmentVM: ServiceReportEquipmentViewModel
    private lateinit var reportTimeTrackingVM: ServiceReportTimeTrackingViewModel

    private val equipmentAdapter = ServiceReportEquipmentDetailAdapter()
    private val timeTrackingAdapter = SRMyTimeTrackingAdapter()
    private val contactAdapter = SRContactAdapter()

    private var reportCompleteOrCancel = false
    private var hasChecklist = false
    private var canAddTimeTracking = false
    private var timeTrackingAdded = false
    private var problemAdded = false
    private var solutionAdded = false

    private var accountId = ""
    private var accountName = ""
    private var caseId = ""
    private var callId = ""
    private var callNumber = ""
    private var reportNumber = ""

    // Send email get from call
    private var accountContact = ""
    private var accountFieldPic = ""

    private val serviceByArray = ArrayList<String>()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.service_report_preview_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        caseVM = ViewModelProviders.of(this).get(ServiceCaseViewModel::class.java)
        contactVM = ViewModelProviders.of(this).get(AccountContactViewModel::class.java)
        callVM = ViewModelProviders.of(this).get(ServiceCallViewModel::class.java)
        reportVM = ViewModelProviders.of(this).get(ServiceReportViewModel::class.java)
        reportEquipmentVM = ViewModelProviders.of(this).get(ServiceReportEquipmentViewModel::class.java)
        reportTimeTrackingVM = ViewModelProviders.of(this).get(ServiceReportTimeTrackingViewModel::class.java)

        reportObserver()
        buttonListener()
        setupAdapter()
        timetrackingAdapterListener()
    }

    private fun buttonListener(){
        btn_service_call.setOnClickListener {
            val i = Intent(requireContext(), ServiceCallDetail::class.java)
            i.putExtra("id", callId)
            i.putExtra("number", callNumber)
            startActivity(i)
        }

        btn_update_report.setOnClickListener {
            val i = Intent(it.context, ServiceReportAdd::class.java)
            i.putExtra("accountId", accountId)
            i.putExtra("accountName", accountName)
            i.putExtra("reportId", reportId)
            i.putExtra("callNumber", callNumber)
            startActivity(i)
        }

        btn_phone2.setOnClickListener {
            FunHelper.phoneCall(it.context, account_pic_contact_phone.text.toString())
        }

        btn_edit_contact.setOnClickListener {
            editContactDialog()
        }

        btn_info_pdf.setOnClickListener {
            val linkPdf = "${FunHelper.BASE_URL}service-report-pdf/detail/$reportId"
            showPdf(linkPdf)
        }

        btn_add_equipment.setOnClickListener {
            val i = Intent(requireContext(), EquipmentAdd::class.java)
            i.putExtra("section", "report")
            i.putExtra("accountId", accountId)
            i.putExtra("caseId", caseId)
            i.putExtra("callId", callId)
            i.putExtra("reportId", reportId)
            startActivity(i)
        }

        btn_time_tracking_add.setOnClickListener {
            if (canAddTimeTracking) {
                TimeTrackingAdd(requireContext(), callId, reportId)
                    .dialogFormTimeTracking(false, "", "", "", "")
            } else {
                Toast.makeText(
                    requireContext(),
                    "Can't add time tracking from Mobile Apps after 7 day from signature date.",
                    Toast.LENGTH_LONG
                ).show()
            }
        }

        btn_signature.setOnClickListener {
            if (timeTrackingAdded && problemAdded && solutionAdded) {
                val i = Intent(it.context, FeedbackEdit::class.java)
                i.putExtra("reportId", reportId)
                i.putExtra("accountId", accountId)
                startActivity(i)
            } else if (!timeTrackingAdded) {
                Toast.makeText(requireContext(), "Insert time tracking first", Toast.LENGTH_LONG).show()
            } else if (!problemAdded) {
                Toast.makeText(
                    requireContext(),
                    "Insert problem for all equipment",
                    Toast.LENGTH_LONG
                ).show()
            } else if (!solutionAdded) {
                Toast.makeText(
                    requireContext(),
                    "Insert solution for all equipment",
                    Toast.LENGTH_LONG
                ).show()
            }
        }

        btn_cancel.setOnClickListener {
            dialogServiceReportCancel()
        }

        btn_send_email.setOnClickListener {
            val i = Intent(it.context, SendEmail::class.java)
            i.putExtra("callId", callId)
            i.putExtra("reportId", reportId)
            i.putExtra("accountContact", accountContact)
            i.putExtra("accountFieldPic", accountFieldPic)
            startActivity(i)
        }

    }

    private fun setupAdapter() {
        recycler_time_tracking?.layoutManager = LinearLayoutManager(
            requireContext(),
            RecyclerView.VERTICAL,
            false
        )
        recycler_time_tracking?.adapter = timeTrackingAdapter
    }

    private fun timetrackingAdapterListener() {
        timeTrackingAdapter.setEventHandler(object : SRMyTimeTrackingAdapter.RecyclerClickListener {
            override fun isEdit(id: String, date: String, start: String, end: String) {
                TimeTrackingAdd(requireContext(), callId, reportId)
                    .dialogFormTimeTracking(true, id, date, start, end)
            }

            override fun isDelete(id: String) {
                dialogDeleteTimetracking(id)
            }
        })
    }

    private fun dialogDeleteTimetracking(timetrackingId: String) {
        AlertDialog.Builder(requireContext())
            .setMessage("Delete time tracking ?")
            .setPositiveButton(android.R.string.yes) { dialog, whichButton ->
                reportVM.updateFlag(reportId, false)
                reportTimeTrackingVM.deleteById(timetrackingId)
                sendData()
            }
            .setNegativeButton(android.R.string.no, null).show()
    }

    @SuppressLint("SetTextI18n")
    private fun dialogServiceReportCancel() {
        val view = layoutInflater.inflate(R.layout.dialog_service_report_cancel, null)
        val dialog = BottomSheetDialog(requireContext())
        dialog.setContentView(view)
        val bottomSheetBehavior = BottomSheetBehavior.from<View>(view.parent as View)
        dialog.setOnShowListener { bottomSheetBehavior.state = BottomSheetBehavior.STATE_EXPANDED }

        val number = dialog.findViewById<TextView>(R.id.tx_title)
        val reasonValue = dialog.findViewById<EditText>(R.id.reason_value)
        val btnYes = dialog.findViewById<TextView>(R.id.btn_yes)
        val btnNo = dialog.findViewById<TextView>(R.id.btn_no)

        number?.text = "Cancel SR $reportNumber"

        btnYes?.setOnClickListener {
            val reason = reasonValue?.text ?: ""
            cancelReport(reason.toString())
        }

        btnNo?.setOnClickListener {
            dialog.dismiss()
        }

        dialog.show()
    }

    @SuppressLint("SetTextI18n")
    private fun reportObserver(){
        reportVM.getByReportId(reportId).observe(viewLifecycleOwner, { reportData ->
            if (reportData.isNotEmpty()) {

                // Service By Array
                serviceByArray.clear()
                if (reportData.first().servicedByUserName.isNotEmpty()) {
                    serviceByArray.add(reportData.first().servicedByUserName)
                }

                // PIC Contact
                getContact(
                    reportData.first().fieldAccountContactId,
                    account_pic_contact_name,
                    account_pic_contact_phone,
                    account_pic_contact_type,
                )

                reportNumber = reportData.first().serviceReportNumber.ifEmpty { "-" }
                val headerName = "Report : $reportNumber"
                report_status.text = reportData.first().reportStatusEnumText.ifEmpty { "-" }
                report_number.text = reportData.first().serviceReportNumber
                info_equipment_value.text = reportData.first().equipment?.size.toString()
                info_created_value.text = reportData.first().createdByUserName.ifEmpty { "-" }
                info_created_date_value.text = FunHelper.uiDateFormat(reportData.first().createdDate)

                caseId = reportData.first().caseId
                callId = reportData.first().callId

                // ================================================================================= time tracking when call end
                try {
                    val serverFormat = SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault())
                    val signatureDateString = reportData.first().accountPicSignatureAt

                    if (signatureDateString.isNotEmpty()) {
                        val signatureDate = serverFormat.parse(signatureDateString)
                        if (signatureDate != null) {
                            val callInstance = Calendar.getInstance()
                            callInstance.time = signatureDate
                            callInstance.add(Calendar.DATE, 7)
                            val callLimit = callInstance.time

                            canAddTimeTracking = callLimit.after(Date())
                        }
                    } else {
                        canAddTimeTracking = true
                    }
                } catch (e: Exception) { e.stackTrace }

                // ================================================================================= Feedback
                if (reportData.first().accountPicSignatureFileImage.isEmpty()) {
//                    btn_send_email.visibility = View.GONE
                    feedback_warning.visibility = View.VISIBLE
                    feedback_lay.visibility = View.GONE
                    btn_signature.visibility = View.VISIBLE
                }
                else {
//                    btn_send_email.visibility = View.VISIBLE
                    feedback_warning.visibility = View.GONE
                    feedback_lay.visibility = View.VISIBLE
                    btn_signature.visibility = View.GONE

                    signature_user.text = reportData.first().cpName.ifEmpty { "-" }
                    rating_text.text = reportData.first().feedback.ifEmpty { "-" }
                    signature_date_value.text =
                        FunHelper.uiDateFormat(reportData.first().accountPicSignatureAt)

                    Log.e("aim", "tanda tangan: ${reportData.first().accountPicSignatureFileImage}")

                    try {
                        Glide.with(requireContext()).load(reportData.first().accountPicSignatureFileImage).into(
                            signature_pad
                        )
                    } catch (e: Exception) {
                    }
                    val ratingFloat = reportData.first().customerRating
                    if (ratingFloat.isNotEmpty()) {
                        rating.rating = ratingFloat.toFloat()
                    } else {
                        rating.rating = 0f
                    }
                }

                val reportStatus = reportData.first().reportStatusEnumText
                // ================================================================================= Report completed
                if (reportStatus == "Completed" || reportStatus == "Customer Signed") {
                    reportCompleteOrCancel = true
                    btn_update_report.visibility = View.GONE
                    btn_edit_contact.visibility = View.GONE
                    btn_add_equipment.visibility = View.GONE
                    btn_signature.visibility = View.GONE
                    btn_cancel.visibility = View.GONE
                }

                // ================================================================================= Report canceled
                if (reportStatus == "Canceled") {
                    reportCompleteOrCancel = true
                    btn_update_report.visibility = View.GONE
                    btn_edit_contact.visibility = View.GONE
                    btn_info_pdf.visibility = View.GONE
                    btn_add_equipment.visibility = View.GONE
                    btn_time_tracking_add.visibility = View.GONE
                    btn_signature.visibility = View.GONE
                    btn_cancel.visibility = View.GONE
                    btn_send_email.visibility = View.GONE
                }

                // ================================================================================= My Report
                val engineerId = SharedPreferenceData.getString(requireContext(), 8, "")
                val itsMyReport = ArrayList<String>()
                reportData.first().engineer?.forEach {
                    if (it?.id ?: "" == engineerId) {
                        itsMyReport.add(it?.id ?: "")
                    }
                }

                Log.e("aim", "report engineer : $itsMyReport")
                if (itsMyReport.isEmpty()) {
                    btn_update_report.visibility = View.GONE
                    btn_service_call.visibility = View.GONE
                    btn_info_pdf.visibility = View.GONE
                    btn_add_equipment.visibility = View.GONE
                    btn_time_tracking_add.visibility = View.GONE
                    btn_signature.visibility = View.GONE
                    btn_cancel.visibility = View.GONE
                    btn_send_email.visibility = View.GONE
                }

                callObserver()
                caseObserver()
                timeTrackingObserver()

            }
//            else {
//                Toast.makeText(requireContext(), "Report not found", Toast.LENGTH_LONG).show()
//                onBackPressed()
//            }
        })
    }

    @SuppressLint("SetTextI18n")
    private fun callObserver() {
        callVM.getByCallId(callId).observe(viewLifecycleOwner, Observer { callData ->
            if (callData.isNotEmpty()) {

                accountId = callData.first().accountId
                accountName = callData.first().accountName
                callNumber = callData.first().serviceCallNumber

                accountContact = callData.first().accountContactEmail
                accountFieldPic = callData.first().fieldAccountContactEmail

                getContactList()

                btn_service_call.text =
                    "Service Call : ${callData.first().serviceCallNumber}"
                info_call_status.text = callData.first().callStatusEnumText.ifEmpty { "-" }
                info_call_start.text = FunHelper.uiDateFormat(callData.first().startDate)
                info_call_end.text = FunHelper.uiDateFormat(callData.first().endDate)
                info_call_subject.text = callData.first().serviceCallSubject.ifEmpty { "-" }
                info_call_desc.text = callData.first().serviceCallDescription.ifEmpty { "-" }
                info_lead_engineer.text = callData.first().leadEngineerName.ifEmpty { "-" }

            }
        })
    }

    private fun caseObserver() {
        caseVM.getByCaseId(caseId).observe(viewLifecycleOwner, Observer { caseData ->
            if (caseData.isNotEmpty()) {
                info_case_type.text = caseData.first().caseTypeEnumName.ifEmpty { "-" }
                if (caseData.first().caseTypeEnumName == "Installation" ||
                    caseData.first().caseTypeEnumName == "Maintenance") {

                    hasChecklist = true
                }

                reportEquipmentObserver()
            }
        })
    }

    @SuppressLint("SetTextI18n")
    private fun reportEquipmentObserver() {
        FunHelper.setUpAdapter(requireContext(), recycler_equipment).adapter = equipmentAdapter

        reportEquipmentVM.getEquipmentDetail(reportId).observe(viewLifecycleOwner,
            { reportEquipmentMain ->
                tx_equipment_count.text = ""
                equipmentAdapter.reset()

                Log.e("aim","equipment data : $reportEquipmentMain")

                if (reportEquipmentMain.isNotEmpty()) {

                    val equipmnetList = mutableListOf<ServiceReportEquipmentEntity>()
                    val problemList = mutableListOf<ProblemEntity>()
                    val solutionList = mutableListOf<SolutionEntity>()
                    val sparepartList = mutableListOf<ServiceReportSparepartEntity>()
                    val questionMaster = mutableListOf<ChecklistQuestionEntity>()
                    val questionReport = mutableListOf<ServiceReportEquipmentChecklistEntity>()

                    reportEquipmentMain.forEach { equipment ->

                        // ============================================================================= Report Equipment
                        val equipmentData = ServiceReportEquipmentEntity(
                            equipment.serviceReportEquipmentId ?: "",
                            equipment.serviceReportEquipmentCallId ?: "",
                            equipment.reportId ?: "",
                            equipment.equipmentId ?: "",
                            equipment.problemSymptom ?: "",
                            equipment.statusAfterServiceEnumId ?: "",
                            equipment.statusAfterServiceEnumName ?: "",
                            equipment.statusAfterServiceRemarks ?: "",
                            equipment.installationStartDate ?: "",
                            equipment.installationMatters ?: "",
                            equipment.checklistTemplateId ?: "",
                            "",
                            "",
                            "",
                            "",
                            equipment.equipmentName ?: "",
                            equipment.regionId ?: "",
                            equipment.serialNo ?: "",
                            equipment.brand ?: "",
                            equipment.warrantyStartDate ?: "",
                            equipment.warrantyEndDate ?: "",
                            equipment.equipmentRemarks ?: "",
                            equipment.deliveryAddressId ?: "",
                            equipment.salesOrderNo ?: "",
                            equipment.warrantyStatus ?: "",
                            equipment.accountId ?: "",
                            equipment.accountName ?: "",
                            equipment.agreementId ?: "",
                            equipment.agreementNumber ?: "",
                            equipment.agreementEnd ?: "",
                            equipment.statusTextEnumId ?: "",
                            equipment.statusTextEnumText ?: "",
                            equipment.product_id ?: "",
                            equipment.product_name,
                            false,
                            false,
                            false
                        )
                        if (!equipmnetList.contains(equipmentData)) {
                            equipmnetList.add(equipmentData)
                        }

                        // ============================================================================= Problem Image
                        val problemData = ProblemEntity(
                            equipment.reportEquipmentId_problem ?: "",
                            equipment.problemImage ?: emptyList()
                        )
                        if (!problemList.contains(problemData)) {
                            problemList.add(problemData)
                        }

                        // ============================================================================= Cause Solution
                        val solutionData = SolutionEntity(
                            equipment.serviceReportEquipmentSolutionId ?: "",
                            equipment.reportEquipmentId_solution ?: "",
                            equipment.subject ?: "",
                            equipment.problemCause ?: "",
                            equipment.solution ?: "",
                            "",
                            "",
                            "",
                            "",
                            equipment.causeImage ?: emptyList(),
                            equipment.solutionImage ?: emptyList(),
                            false,
                            false
                        )
                        if (!solutionList.contains(solutionData)) {
                            solutionList.add(solutionData)
                        }

                        // ============================================================================= Sparepart
                        val sparepartData = ServiceReportSparepartEntity(
                            equipment.service_report_sparepart_id ?: "",
                            equipment.reportEquipmentId_sparepart ?: "",
                            equipment.spare_part_image_number ?: "",
                            equipment.spare_part_number ?: "",
                            equipment.spare_part_description ?: "",
                            equipment.spare_part_memo ?: "",
                            equipment.enum_id ?: "",
                            equipment.enum_name ?: "",
                            equipment.part_id ?: "",
                            equipment.part_no ?: "",
                            equipment.part_name ?: "",
                            equipment.qty ?: "",
                            false,
                            false
                        )
                        if (!sparepartList.contains(sparepartData)) {
                            sparepartList.add(sparepartData)
                        }

                        // ============================================================================= Master Checklist Count
                        val questionDataMaster = ChecklistQuestionEntity(
                            equipment.questionId ?: "",
                            "",
                            "",
                            "",
                            "",
                            "",
                            "",
                            "",
                            "",
                            "",
                            "",
                            equipment.templateId ?: ""
                        )
                        if (!questionMaster.contains(questionDataMaster)) {
                            questionMaster.add(questionDataMaster)
                        }

                        // ============================================================================= Report Checklist Count
                        val questionReportData = ServiceReportEquipmentChecklistEntity(
                            "",
                            equipment.report_equipment_id ?: "",
                            "",
                            "",
                            "",
                            "",
                            "",
                            "",
                            equipment.checklist_status_id ?: "",
                            equipment.checklist_status_Name ?: "",
                            equipment.question_id ?: "",
                            "",
                            "",
                            "",
                            "",
                            "",
                            "",
                            "",
                            "",
                            "",
                            "",
                            "",
                            "",
                            emptyList(),
                            false
                        )
                        if (!questionReport.contains(questionReportData)) {
                            questionReport.add(questionReportData)
                        }

                    }

                    tx_equipment_count.text = "(${equipmnetList.size})"
                    recycler_equipment.visibility = View.VISIBLE
                    warning_equipment.visibility = View.GONE

                    equipmnetList.forEach { equipment ->

                        val problemFilter =
                            problemList.filter { it.reportEquipmentId_problem == equipment.serviceReportEquipmentId }
                        val solutionFilter =
                            solutionList.filter { it.reportEquipmentId_solution == equipment.serviceReportEquipmentId }
                        val sparepartFilter =
                            sparepartList.filter { it.reportEquipmentId_sparepart == equipment.serviceReportEquipmentId }
                        val checklistMasterFilter =
                            questionMaster.filter { it.templateId == equipment.checklistTemplateId }
                        val checklistReportFilter =
                            questionReport.filter { it.report_equipment_id == equipment.serviceReportEquipmentId }

                        equipmentAdapter.addData(
                            SREquipmentModel(
                                equipment.serviceReportEquipmentId,
                                equipment.reportId,
                                equipment.equipmentId,
                                equipment.problemSymptom,
                                equipment.statusAfterServiceEnumId,
                                equipment.statusAfterServiceEnumName,
                                equipment.statusAfterServiceRemarks,
                                equipment.installationStartDate,
                                equipment.installationMatters,
                                equipment.checklistTemplateId,
                                equipment.createdDate,
                                equipment.createdBy,
                                equipment.brand,
                                equipment.lastModifiedBy,
                                equipment.equipmentName,
                                equipment.regionId,
                                equipment.serialNo,
                                equipment.brand,
                                equipment.warrantyStartDate,
                                equipment.warrantyEndDate,
                                equipment.equipmentRemarks,
                                equipment.deliveryAddressId,
                                equipment.salesOrderNo,
                                equipment.warrantyStatus,
                                equipment.accountId,
                                equipment.accountName,
                                equipment.agreementId,
                                equipment.agreementNumber,
                                equipment.agreementEnd,
                                equipment.statusTextEnumId,
                                equipment.statusTextEnumText,
                                equipment.product_id,
                                equipment.product_name,
                                problemFilter as MutableList<ProblemEntity>,
                                solutionFilter as MutableList<SolutionEntity>,
                                sparepartFilter as MutableList<ServiceReportSparepartEntity>,
                                checklistMasterFilter as MutableList<ChecklistQuestionEntity>,
                                checklistReportFilter as MutableList<ServiceReportEquipmentChecklistEntity>,
                                false,
                                false
                            ),
                            reportCompleteOrCancel,
                            hasChecklist
                        )
                        Log.e("aim", "Checklist Report : $checklistReportFilter")
                    }

                    checkSolution()

                } else {
                    recycler_equipment.visibility = View.GONE
                    warning_equipment.visibility = View.VISIBLE
                }
            })
    }

    private fun timeTrackingObserver() {
        val engineerId = SharedPreferenceData.getString(requireContext(), 8, "")
        reportTimeTrackingVM.getByReportId(reportId).observe(viewLifecycleOwner, {
            tx_time_tracking_count.text = ""
            timeTrackingAdapter.clear()

            if (it.isNotEmpty()) {
                timeTrackingAdded = true
                val timeTrackingCount = "(${it.size})"
                tx_time_tracking_count.text = timeTrackingCount
                warning_time_tracking.visibility = View.GONE
                time_tracking_lay.visibility = View.VISIBLE

                if (reportCompleteOrCancel) {
                    timeTrackingAdapter.setList(it, engineerId, false)
                } else {
                    timeTrackingAdapter.setList(it, engineerId, true)
                }
            } else {
                timeTrackingAdded = false
                warning_time_tracking.visibility = View.VISIBLE
                time_tracking_lay.visibility = View.GONE
            }

            // ===================================================================================== Service By Array
            it.forEach { timeTrackingData ->
                if (!serviceByArray.contains(timeTrackingData.engineerName)) {
                    serviceByArray.add(timeTrackingData.engineerName)
                }
            }
            val serviceByString = serviceByArray.toString()
                .replace("[", "")
                .replace("]", "")
            info_service_value.text = serviceByString.ifEmpty { "-" }
        })
    }

    private fun checkSolution() {
        var problemCount = 0
        var solutionCount = 0
        equipmentAdapter.getAllItem().forEach {
            if (it.problemSymptom.isNotEmpty()) {
                problemCount++
            }

            if (it.solution.isNotEmpty()) {
                solutionCount++
            }
        }

        problemAdded = equipmentAdapter.itemCount == problemCount
        solutionAdded = equipmentAdapter.itemCount == solutionCount
    }

    private fun showPdf(fileUrl: String) = runWithPermissions(FunHelper.storageWritePermission) {
        DownloadPdf(requireContext(), fileUrl).downloadNow(true)
    }

    private fun getContactList() {
        contactVM.getByAccountId(accountId).observe(viewLifecycleOwner, {
            contactAdapter.setList(it)
        })
    }

    private fun getContact(contactId: String, name: TextView, phone: TextView, type: TextView) {
        contactVM.getById(contactId).observe(viewLifecycleOwner, {
            if (it.isNotEmpty()) {
                name.text = it.first().contact_person_name.ifEmpty { "-" }
                phone.text = it.first().contact_person_phone.ifEmpty { "-" }
                type.text = it.first().contact_type.ifEmpty { "-" }
            }
        })
    }

    private fun editContactDialog() {
        val view = layoutInflater.inflate(R.layout.dialog_list_filter,null)
        val dialog = BottomSheetDialog(requireContext())
        dialog.setContentView(view)
        val bottomSheetBehavior = BottomSheetBehavior.from<View>(view.parent as View)
        dialog.setOnShowListener { bottomSheetBehavior.state = BottomSheetBehavior.STATE_EXPANDED }

        val btnClear = dialog.findViewById<TextView>(R.id.btn_clear)
        btnClear?.visibility = View.GONE

        val recyclerFilter = dialog.findViewById<RecyclerView>(R.id.recycler_filter)
        recyclerFilter?.layoutManager = LinearLayoutManager(requireContext(), RecyclerView.VERTICAL, false)
        recyclerFilter?.adapter = contactAdapter

        contactAdapter.setEventHandler(object: SRContactAdapter.RecyclerClickListener {
            override fun isClicked(data: AccountContactEntity) {
                updateReportContact(
                    data.account_contact_id,
                    data.contact_person_name,
                    data.contact_person_phone,
                )

                dialog.dismiss()
            }
        })

        dialog.show()
    }

    private fun updateReportContact(id: String, name: String, phone: String) {
        reportVM.updateContact(
            reportId,
            id,
            name,
            phone,
            false
        )

        sendData()
    }

    private fun cancelReport(reasonValue: String) {
        val progressDialog = ProgressDialog(requireContext())
        progressDialog.setMessage("Cancel Report ...")
        progressDialog.setCancelable(false)

        val token = "Bearer "+ SharedPreferenceData.getString(requireContext(), 2, "")
        val userId = SharedPreferenceData.getString(requireContext(), 8, "")
        val params = HashMap<String, Any>()
        params["report_id"] = reportId
        params["cancellation_note"] = reasonValue

        ApiClient.instance.cancelReport(token, params).enqueue(object : Callback<ResponseBody> {
            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                progressDialog.dismiss()
                Toast.makeText(requireContext(), "Cancel report failed", Toast.LENGTH_LONG)
                    .show()
            }

            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                progressDialog.dismiss()
                if (!response.isSuccessful) {
                    if (response.code() == 401) {
                        FunHelper.clearAllData(requireContext())
                        Toast.makeText(context, "Session end", Toast.LENGTH_LONG).show()
                    } else {
                        FunHelper.firebaseApiLog(
                            userId ?: "",
                            response.raw().request().url().toString() ?: "",
                            response.raw().request().headers().toString() ?: "",
                            params.toString() ?: "",
                            response.raw().toString() ?: ""
                        )
                        Toast.makeText(requireContext(), response.raw().toString(), Toast.LENGTH_LONG).show()
                    }
                } else {
                    reportVM.deleteById(reportId)
                    reportEquipmentVM.deleteByReportId(reportId)
                    Toast.makeText(
                        requireContext(),
                        "Cancel report success",
                        Toast.LENGTH_LONG
                    ).show()

                    requireActivity().onBackPressed()
                }
            }
        })
    }

    private fun sendData() {
        val progressDialog = ProgressDialog(requireContext())
        progressDialog.setMessage("Send Data ...")
        progressDialog.setCancelable(false)
        progressDialog.show()

        UploadFile(requireContext()).startUploadFile()

        SendDataReport(requireContext()).getParam(reportId, "report", object : SendDataInterface {
            override fun sendDataCallback(isSuccess: Boolean) {
                requireActivity().runOnUiThread {
                    progressDialog.dismiss()

                    if (isSuccess) {
                        reportVM.getByReportId(reportId).observe(viewLifecycleOwner, {
                            if (it.isEmpty()) {
                                requireActivity().onBackPressed()
                            }
                        })
                    }
                }
            }
        })
    }

}
