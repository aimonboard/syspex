package com.syspex.ui

import android.annotation.SuppressLint
import android.app.ProgressDialog
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.core.widget.NestedScrollView
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.android.material.tabs.TabLayout
import com.livinglifetechway.quickpermissions_kotlin.runWithPermissions
import com.syspex.Menu2
import com.syspex.R
import com.syspex.data.local.SharedPreferenceData
import com.syspex.data.local.database.account_contact.AccountContactEntity
import com.syspex.data.local.database.account_contact.AccountContactViewModel
import com.syspex.data.local.database.pcs_problem.ProblemEntity
import com.syspex.data.local.database.pcs_solution.SolutionEntity
import com.syspex.data.local.database.product_checklist_question.ChecklistQuestionEntity
import com.syspex.data.local.database.service_call.ServiceCallViewModel
import com.syspex.data.local.database.service_case.ServiceCaseViewModel
import com.syspex.data.local.database.service_report.ServiceReportViewModel
import com.syspex.data.local.database.service_report_equipment.ServiceReportEquipmentEntity
import com.syspex.data.local.database.service_report_equipment.ServiceReportEquipmentViewModel
import com.syspex.data.local.database.service_report_equipment_checklist.ServiceReportEquipmentChecklistEntity
import com.syspex.data.local.database.service_report_sparepart.ServiceReportSparepartEntity
import com.syspex.data.local.database.service_report_time_tracking.ServiceReportTimeTrackingViewModel
import com.syspex.data.model.SREquipmentModel
import com.syspex.data.retrofit.ApiClient
import com.syspex.ui.adapter.SRContactAdapter
import com.syspex.ui.adapter.SRMyTimeTrackingAdapter
import com.syspex.ui.adapter.ServiceReportEquipmentDetailAdapter
import com.syspex.ui.helper.*
import kotlinx.android.synthetic.main.service_report_detail_activity.*
import kotlinx.android.synthetic.main.service_report_detail_activity.account_pic_contact_name
import kotlinx.android.synthetic.main.service_report_detail_activity.account_pic_contact_phone
import kotlinx.android.synthetic.main.service_report_detail_activity.account_pic_contact_type
import kotlinx.android.synthetic.main.service_report_detail_activity.scroll_lay
import kotlinx.android.synthetic.main.service_report_detail_activity.tx_time_tracking_count
import okhttp3.ResponseBody
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList
import kotlin.collections.HashMap

class ServiceReportDetail : AppCompatActivity() {

    private lateinit var caseVM: ServiceCaseViewModel
    private lateinit var contactVM: AccountContactViewModel
    private lateinit var callVM: ServiceCallViewModel
    private lateinit var reportVM: ServiceReportViewModel
    private lateinit var reportEquipmentVM: ServiceReportEquipmentViewModel
    private lateinit var reportTimeTrackingVM: ServiceReportTimeTrackingViewModel

    private val equipmentAdapter = ServiceReportEquipmentDetailAdapter()
    private val timeTrackingAdapter = SRMyTimeTrackingAdapter()
    private val contactAdapter = SRContactAdapter()

    private var reportCompleteOrCancel = false
    private var hasChecklist = false
    private var canAddTimeTracking = false
    private var timeTrackingAdded = false
    private var problemAdded = false
    private var solutionAdded = false
    private var isScrolling = false
    private var reportLocal = false

    private var accountId = ""
    private var accountName = ""
    private var caseId = ""
    private var callId = ""
    private var callNumber = ""
    private var reportId = ""
    private var reportNumber = ""

    // Send email get from call
    private var accountContact = ""
    private var accountFieldPic = ""

    private val serviceByArray = ArrayList<String>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.service_report_detail_activity)
        caseVM = ViewModelProviders.of(this).get(ServiceCaseViewModel::class.java)
        contactVM = ViewModelProviders.of(this).get(AccountContactViewModel::class.java)
        callVM = ViewModelProviders.of(this).get(ServiceCallViewModel::class.java)
        reportVM = ViewModelProviders.of(this).get(ServiceReportViewModel::class.java)
        reportEquipmentVM = ViewModelProviders.of(this).get(ServiceReportEquipmentViewModel::class.java)
        reportTimeTrackingVM = ViewModelProviders.of(this).get(ServiceReportTimeTrackingViewModel::class.java)

        generateView()
        buttonListener()
        setupAdapter()
        generateTab()
        tabListener()
        timetrackingAdapterListener()

    }

    override fun onResume() {
        super.onResume()
//        Log.e("aim", "on resume")
        sendData()
    }

    private fun generateView() {
        if (intent.hasExtra("id")) {
            reportId = intent.getStringExtra("id") ?: ""
            reportObserver()
        }
    }

    private fun buttonListener(){
        back_link.setOnClickListener {
            onBackPressed()
        }

        btn_refresh.setOnClickListener {
            sendData()
        }

        btn_home.setOnClickListener {
            val intent = Intent(this, Menu2::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_NEW_TASK)
            startActivity(intent)
        }

        btn_update_report_status.setOnClickListener {
            val i = Intent(this, ServiceReportAdd::class.java)
            i.putExtra("accountId", accountId)
            i.putExtra("accountName", accountName)
            i.putExtra("reportId", reportId)
            i.putExtra("callNumber", callNumber)
            startActivity(i)
        }

        btn_service_call.setOnClickListener {
            val i = Intent(this, ServiceCallDetail::class.java)
            i.putExtra("id", callId)
            i.putExtra("number", callNumber)
            startActivity(i)
        }

        btn_phone2.setOnClickListener {
            FunHelper.phoneCall(it.context, account_pic_contact_phone.text.toString())
        }

        btn_edit_contact.setOnClickListener {
            editContactDialog()
        }

        btn_info_pdf.setOnClickListener {
            if (reportLocal) {
                dialogReportLocal()
            } else {
                val linkPdf = "${FunHelper.BASE_URL}service-report-pdf/detail/$reportId"
                showPdf(linkPdf)
            }
        }

        btn_add_equipment.setOnClickListener {
            val i = Intent(this, EquipmentAdd::class.java)
            i.putExtra("section", "report")
            i.putExtra("accountId", accountId)
            i.putExtra("caseId", caseId)
            i.putExtra("callId", callId)
            i.putExtra("reportId", reportId)
            startActivity(i)
        }

        var allExpand = false
        btn_expand.setOnClickListener {
            try {
                allExpand = if (!allExpand) {
                    (recycler_equipment.adapter as ServiceReportEquipmentDetailAdapter).allExpand()
                    true
                } else {
                    (recycler_equipment.adapter as ServiceReportEquipmentDetailAdapter).allCollapse()
                    false
                }
            } catch (e: Exception) { e.stackTrace }
        }

        btn_time_tracking_add.setOnClickListener {
            if (canAddTimeTracking) {
            TimeTrackingAdd(this@ServiceReportDetail, callId, reportId)
                .dialogFormTimeTracking(false, "", "", "", "")
            } else {
                Toast.makeText(
                    this,
                    "Can't add time tracking from Mobile Apps after 7 day from signature date.",
                    Toast.LENGTH_LONG
                ).show()
            }
        }

        btn_signature.setOnClickListener {
            if (timeTrackingAdded && problemAdded && solutionAdded) {
                val i = Intent(it.context, FeedbackEdit::class.java)
                i.putExtra("reportId", reportId)
                i.putExtra("accountId", accountId)
                startActivity(i)
            } else if (!timeTrackingAdded) {
                Toast.makeText(this, "Insert time tracking first", Toast.LENGTH_LONG).show()
            } else if (!problemAdded) {
                Toast.makeText(
                    this,
                    "Insert problem for all equipment",
                    Toast.LENGTH_LONG
                ).show()
            } else if (!solutionAdded) {
                Toast.makeText(
                    this,
                    "Insert solution for all equipment",
                    Toast.LENGTH_LONG
                ).show()
            }
        }

        btn_cancel.setOnClickListener {
            dialogServiceReportCancel()
        }

        btn_send_email.setOnClickListener {
            if (reportLocal) {
                dialogReportLocal()
            } else {
                val i = Intent(this, SendEmail::class.java)
                i.putExtra("callId", callId)
                i.putExtra("reportId", reportId)
                i.putExtra("accountContact", accountContact)
                i.putExtra("accountFieldPic", accountFieldPic)
                startActivity(i)
            }
        }

    }

    private fun setupAdapter() {
        recycler_time_tracking?.layoutManager = LinearLayoutManager(
            this,
            RecyclerView.VERTICAL,
            false
        )
        recycler_time_tracking?.adapter = timeTrackingAdapter
    }

    private fun generateTab() {
        tab_lay.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
            override fun onTabUnselected(tab: TabLayout.Tab?) {}
            override fun onTabReselected(tab: TabLayout.Tab?) {}

            override fun onTabSelected(tab: TabLayout.Tab?) {
                if (!isScrolling) {
                    when (tab_lay.selectedTabPosition) {
                        0 -> scroll_lay.smoothScrollTo(0, head_information.top)
                        1 -> scroll_lay.smoothScrollTo(0, head_equipment.top)
                        2 -> scroll_lay.smoothScrollTo(0, head_time_tracking.top)
                        3 -> scroll_lay.smoothScrollTo(0, head_feedback.top)
                    }
                }
            }

        })
    }

    private fun tabListener() {
        scroll_lay.setOnScrollChangeListener { v: NestedScrollView?, x: Int, y: Int, oldX: Int, oldY: Int ->
             if (y >= head_information.top && y <= information_lay.bottom){
                isScrolling = true
                tab_lay.getTabAt(0)?.select()
                isScrolling = false
            }
            else if (y >= head_equipment.top && y <= recycler_equipment.bottom) {
                isScrolling = true
                tab_lay.getTabAt(1)?.select()
                isScrolling = false
            }
             else if (y >= head_time_tracking.top && y <= time_tracking_lay.bottom) {
                 isScrolling = true
                 tab_lay.getTabAt(2)?.select()
                 isScrolling = false
             }
             else if (y >= head_feedback.top && y <= feedback_lay.bottom) {
                 isScrolling = true
                 tab_lay.getTabAt(3)?.select()
                 isScrolling = false
             }
        }
    }

    private fun timetrackingAdapterListener() {
        timeTrackingAdapter.setEventHandler(object : SRMyTimeTrackingAdapter.RecyclerClickListener {
            override fun isEdit(id: String, date: String, start: String, end: String) {
                TimeTrackingAdd(this@ServiceReportDetail, callId, reportId)
                    .dialogFormTimeTracking(true, id, date, start, end)
            }

            override fun isDelete(id: String) {
                dialogDeleteTimetracking(id)
            }
        })
    }

    private fun dialogDeleteTimetracking(timetrackingId: String) {
        AlertDialog.Builder(this)
            .setMessage("Delete time tracking ?")
            .setPositiveButton(android.R.string.yes) { dialog, whichButton ->
                reportVM.updateFlag(reportId, false)
                reportTimeTrackingVM.deleteById(timetrackingId)
                sendData()
            }
            .setNegativeButton(android.R.string.no, null).show()
    }

    @SuppressLint("SetTextI18n")
    private fun dialogReportLocal() {
        // can't view report pdf and send email when report still in local android

        val view = layoutInflater.inflate(R.layout.dialog_confirmation,null)
        val dialog = BottomSheetDialog(this)
        dialog.setContentView(view)

        val txTitle = dialog.findViewById<TextView>(R.id.tx_title)
        val txBody = dialog.findViewById<TextView>(R.id.tx_body)
        val buttonYes = dialog.findViewById<TextView>(R.id.btn_yes)
        val buttonNo = dialog.findViewById<TextView>(R.id.btn_no)

        txTitle?.text = "Send Data"
        txBody?.text = "This report is still in local android storage, send this report to server?"
        buttonYes?.text = "Send"
        buttonNo?.text = "Cancel"

        buttonYes?.setOnClickListener {
            dialog.dismiss()
            sendData()
        }

        buttonNo?.setOnClickListener {
            dialog.dismiss()
        }

        dialog.show()
    }

    @SuppressLint("SetTextI18n")
    private fun dialogServiceReportCancel() {
        val view = layoutInflater.inflate(R.layout.dialog_service_report_cancel, null)
        val dialog = BottomSheetDialog(this)
        dialog.setContentView(view)
        val bottomSheetBehavior = BottomSheetBehavior.from<View>(view.parent as View)
        dialog.setOnShowListener { bottomSheetBehavior.state = BottomSheetBehavior.STATE_EXPANDED }

        val number = dialog.findViewById<TextView>(R.id.tx_title)
        val reasonValue = dialog.findViewById<EditText>(R.id.reason_value)
        val btnYes = dialog.findViewById<TextView>(R.id.btn_yes)
        val btnNo = dialog.findViewById<TextView>(R.id.btn_no)

        number?.text = "Cancel SR $reportNumber"

        btnYes?.setOnClickListener {
            val reason = reasonValue?.text ?: ""
            cancelReport(reason.toString())
        }

        btnNo?.setOnClickListener {
            dialog.dismiss()
        }

        dialog.show()
    }

    @SuppressLint("SetTextI18n")
    private fun reportObserver(){
        reportVM.getByReportId(reportId).observe(this, { reportData ->
            if (reportData.isNotEmpty()) {

                // Service By Array
                serviceByArray.clear()
                if (reportData.first().servicedByUserName.isNotEmpty()) {
                    serviceByArray.add(reportData.first().servicedByUserName)
                }

                // PIC Contact
                getContact(
                    reportData.first().fieldAccountContactId,
                    account_pic_contact_name,
                    account_pic_contact_phone,
                    account_pic_contact_type,
                )

                reportNumber = reportData.first().serviceReportNumber.ifEmpty { "-" }
                val headerName = "Report : $reportNumber"
                header_name?.text =  headerName
                report_status.text = reportData.first().reportStatusEnumText.ifEmpty { "-" }
                report_number.text = reportData.first().serviceReportNumber
                info_equipment_value.text = reportData.first().equipment?.size.toString()
                info_created_value.text = reportData.first().createdByUserName.ifEmpty { "-" }
                info_created_date_value.text =
                    FunHelper.uiDateFormat(reportData.first().createdDate)
                info_modified_by_value.text = reportData.first().modifiedByUserName.ifEmpty { "-" }
                info_modified_date_value.text =
                    FunHelper.uiDateFormat(reportData.first().lastModifiedDate)

                caseId = reportData.first().caseId
                callId = reportData.first().callId
                reportLocal = reportData.first().isLocal

                // ================================================================================= time tracking when call end
                try {
                    val serverFormat = SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault())
                    val signatureDateString = reportData.first().accountPicSignatureAt

                    if (signatureDateString.isNotEmpty()) {
                        val signatureDate = serverFormat.parse(signatureDateString)
                        if (signatureDate != null) {
                            val callInstance = Calendar.getInstance()
                            callInstance.time = signatureDate
                            callInstance.add(Calendar.DATE, 7)
                            val callLimit = callInstance.time

                            canAddTimeTracking = callLimit.after(Date())
                        }
                    } else {
                        canAddTimeTracking = true
                    }
                } catch (e: Exception) { e.stackTrace }

                // ================================================================================= Feedback
                if (reportData.first().accountPicSignatureFileImage.isEmpty()) {
//                    btn_send_email.visibility = View.GONE
                    feedback_warning.visibility = View.VISIBLE
                    feedback_lay.visibility = View.GONE
                    btn_signature.visibility = View.VISIBLE
                }
                else {
//                    btn_send_email.visibility = View.VISIBLE
                    feedback_warning.visibility = View.GONE
                    feedback_lay.visibility = View.VISIBLE
                    btn_signature.visibility = View.GONE

                    signature_user.text = reportData.first().cpName.ifEmpty { "-" }
                    rating_text.text = reportData.first().feedback.ifEmpty { "-" }
                    signature_date_value.text =
                        FunHelper.uiDateFormat(reportData.first().accountPicSignatureAt)

//                    Log.e("aim", "tanda tangan: ${reportData.first().accountPicSignatureFileImage}")

                    try {
                        Glide.with(this).load(reportData.first().accountPicSignatureFileImage).into(
                            signature_pad
                        )
                    } catch (e: Exception) {
                    }
                    val ratingFloat = reportData.first().customerRating
                    if (ratingFloat.isNotEmpty()) {
                        rating.rating = ratingFloat.toFloat()
                    } else {
                        rating.rating = 0f
                    }
                }

                val reportStatus = reportData.first().reportStatusEnumText
                // ================================================================================= Report completed
                if (reportStatus == "Completed") {
                    reportCompleteOrCancel = true
                    btn_update_report_status.visibility = View.GONE
                    btn_edit_contact.visibility = View.GONE
                    btn_add_equipment.visibility = View.GONE
                    btn_signature.visibility = View.GONE
                    btn_cancel.visibility = View.GONE
                }

                // ================================================================================= Report canceled
                if (reportStatus == "Canceled") {
                    reportCompleteOrCancel = true
                    btn_update_report_status.visibility = View.GONE
                    btn_edit_contact.visibility = View.GONE
                    btn_add_equipment.visibility = View.GONE
                    btn_time_tracking_add.visibility = View.GONE
                    btn_signature.visibility = View.GONE
                    btn_cancel.visibility = View.GONE
                    btn_send_email.visibility = View.GONE
                }

                // ================================================================================= My Report
                val engineerId = SharedPreferenceData.getString(this, 8, "")
                val itsMyReport = ArrayList<String>()
                reportData.first().engineer?.forEach {
                    if (it?.id ?: "" == engineerId) {
                        itsMyReport.add(it?.id ?: "")
                    }
                }

                Log.e("aim", "report engineer : $itsMyReport")
                if (itsMyReport.isEmpty()) {
                    btn_update_report_status.visibility = View.GONE
                    btn_service_call.visibility = View.GONE
                    btn_add_equipment.visibility = View.GONE
                    btn_expand.visibility = View.GONE
                    btn_time_tracking_add.visibility = View.GONE
                    btn_signature.visibility = View.GONE
                    btn_cancel.visibility = View.GONE
                    btn_send_email.visibility = View.GONE
                }

                callObserver()
                caseObserver()
                timeTrackingObserver()

            }
//            else {
//                Toast.makeText(this, "Report not found", Toast.LENGTH_LONG).show()
//                onBackPressed()
//            }
        })
    }

    @SuppressLint("SetTextI18n")
    private fun callObserver() {
        callVM.getByCallId(callId).observe(this, Observer { callData ->
            if (callData.isNotEmpty()) {

                accountId = callData.first().accountId
                accountName = callData.first().accountName
                callNumber = callData.first().serviceCallNumber

                accountContact = callData.first().accountContactEmail
                accountFieldPic = callData.first().fieldAccountContactEmail

                getContactList()

                btn_service_call.text =
                    "Service Call : ${callData.first().serviceCallNumber}"
                info_call_status.text = callData.first().callStatusEnumText.ifEmpty { "-" }
                info_call_start.text = FunHelper.uiDateFormat(callData.first().startDate)
                info_call_end.text = FunHelper.uiDateFormat(callData.first().endDate)
                info_call_subject.text = callData.first().serviceCallSubject.ifEmpty { "-" }
                info_call_desc.text = callData.first().serviceCallDescription.ifEmpty { "-" }
                info_lead_engineer.text = callData.first().leadEngineerName.ifEmpty { "-" }
                lead_engineer.text = callData.first().leadEngineerName.ifEmpty { "-" }

            }
        })
    }

    private fun caseObserver() {
        caseVM.getByCaseId(caseId).observe(this, Observer { caseData ->
            if (caseData.isNotEmpty()) {
                info_case_type.text = caseData.first().caseTypeEnumName.ifEmpty { "-" }
                if (caseData.first().caseTypeEnumName == "Installation" ||
                    caseData.first().caseTypeEnumName == "Maintenance") {

                    hasChecklist = true
                }

                reportEquipmentObserver()
            }
        })
    }

    @SuppressLint("SetTextI18n")
    private fun reportEquipmentObserver() {
        FunHelper.setUpAdapter(this, recycler_equipment).adapter = equipmentAdapter

        reportEquipmentVM.getEquipmentDetail(reportId).observe(this,
            { reportEquipmentMain ->
                tx_equipment_count.text = ""
                equipmentAdapter.reset()

//                Log.e("aim","equipment data : $reportEquipmentMain")

                if (reportEquipmentMain.isNotEmpty()) {

                    val equipmnetList = mutableListOf<ServiceReportEquipmentEntity>()
                    val problemList = mutableListOf<ProblemEntity>()
                    val solutionList = mutableListOf<SolutionEntity>()
                    val sparepartList = mutableListOf<ServiceReportSparepartEntity>()
                    val questionMaster = mutableListOf<ChecklistQuestionEntity>()
                    val questionReport = mutableListOf<ServiceReportEquipmentChecklistEntity>()

                    reportEquipmentMain.forEach { equipment ->

                        // ============================================================================= Report Equipment
                        val equipmentData = ServiceReportEquipmentEntity(
                            equipment.serviceReportEquipmentId ?: "",
                            equipment.serviceReportEquipmentCallId ?: "",
                            equipment.reportId ?: "",
                            equipment.equipmentId ?: "",
                            equipment.problemSymptom ?: "",
                            equipment.statusAfterServiceEnumId ?: "",
                            equipment.statusAfterServiceEnumName ?: "",
                            equipment.statusAfterServiceRemarks ?: "",
                            equipment.installationStartDate ?: "",
                            equipment.installationMatters ?: "",
                            equipment.checklistTemplateId ?: "",
                            "",
                            "",
                            "",
                            "",
                            equipment.equipmentName ?: "",
                            equipment.regionId ?: "",
                            equipment.serialNo ?: "",
                            equipment.brand ?: "",
                            equipment.warrantyStartDate ?: "",
                            equipment.warrantyEndDate ?: "",
                            equipment.equipmentRemarks ?: "",
                            equipment.deliveryAddressId ?: "",
                            equipment.salesOrderNo ?: "",
                            equipment.warrantyStatus ?: "",
                            equipment.accountId ?: "",
                            equipment.accountName ?: "",
                            equipment.agreementId ?: "",
                            equipment.agreementNumber ?: "",
                            equipment.agreementEnd ?: "",
                            equipment.statusTextEnumId ?: "",
                            equipment.statusTextEnumText ?: "",
                            equipment.product_id ?: "",
                            equipment.product_name,
                            false,
                            false,
                            false
                        )
                        if (!equipmnetList.contains(equipmentData)) {
                            equipmnetList.add(equipmentData)
                        }

                        // ============================================================================= Problem Image
                        val problemData = ProblemEntity(
                            equipment.reportEquipmentId_problem ?: "",
                            equipment.problemImage ?: emptyList()
                        )
                        if (!problemList.contains(problemData)) {
                            problemList.add(problemData)
                        }

                        // ============================================================================= Cause Solution
                        val solutionData = SolutionEntity(
                            equipment.serviceReportEquipmentSolutionId ?: "",
                            equipment.reportEquipmentId_solution ?: "",
                            equipment.subject ?: "",
                            equipment.problemCause ?: "",
                            equipment.solution ?: "",
                            "",
                            "",
                            "",
                            "",
                            equipment.causeImage ?: emptyList(),
                            equipment.solutionImage ?: emptyList(),
                            false,
                            false
                        )
                        if (!solutionList.contains(solutionData)) {
                            solutionList.add(solutionData)
                        }

                        // ============================================================================= Sparepart
                        val sparepartData = ServiceReportSparepartEntity(
                            equipment.service_report_sparepart_id ?: "",
                            equipment.reportEquipmentId_sparepart ?: "",
                            equipment.spare_part_image_number ?: "",
                            equipment.spare_part_number ?: "",
                            equipment.spare_part_description ?: "",
                            equipment.spare_part_memo ?: "",
                            equipment.enum_id ?: "",
                            equipment.enum_name ?: "",
                            equipment.part_id ?: "",
                            equipment.part_no ?: "",
                            equipment.part_name ?: "",
                            equipment.qty ?: "",
                            false,
                            false
                        )
                        if (!sparepartList.contains(sparepartData)) {
                            sparepartList.add(sparepartData)
                        }

                        // ============================================================================= Master Checklist Count
                        val questionDataMaster = ChecklistQuestionEntity(
                            equipment.questionId ?: "",
                            "",
                            "",
                            "",
                            "",
                            "",
                            "",
                            "",
                            "",
                            "",
                            "",
                            equipment.templateId ?: ""
                        )
                        if (!questionMaster.contains(questionDataMaster)) {
                            questionMaster.add(questionDataMaster)
                        }

                        // ============================================================================= Report Checklist Count
                        val questionReportData = ServiceReportEquipmentChecklistEntity(
                            "",
                            equipment.report_equipment_id ?: "",
                            "",
                            "",
                            "",
                            "",
                            "",
                            "",
                            equipment.checklist_status_id ?: "",
                            equipment.checklist_status_Name ?: "",
                            equipment.question_id ?: "",
                            "",
                            "",
                            "",
                            "",
                            "",
                            "",
                            "",
                            "",
                            "",
                            "",
                            "",
                            "",
                            emptyList(),
                            false
                        )
                        if (!questionReport.contains(questionReportData)) {
                            questionReport.add(questionReportData)
                        }

                    }

                    tx_equipment_count.text = "(${equipmnetList.size})"
                    recycler_equipment.visibility = View.VISIBLE
                    warning_equipment.visibility = View.GONE
                    btn_expand.visibility = View.VISIBLE

                    equipmnetList.forEach { equipment ->

                        val problemFilter =
                            problemList.filter { it.reportEquipmentId_problem == equipment.serviceReportEquipmentId }
                        val solutionFilter =
                            solutionList.filter { it.reportEquipmentId_solution == equipment.serviceReportEquipmentId }
                        val sparepartFilter =
                            sparepartList.filter { it.reportEquipmentId_sparepart == equipment.serviceReportEquipmentId }
                        val checklistMasterFilter =
                            questionMaster.filter { it.templateId == equipment.checklistTemplateId }
                        val checklistReportFilter =
                            questionReport.filter { it.report_equipment_id == equipment.serviceReportEquipmentId }

                        equipmentAdapter.addData(
                            SREquipmentModel(
                                equipment.serviceReportEquipmentId,
                                equipment.reportId,
                                equipment.equipmentId,
                                equipment.problemSymptom,
                                equipment.statusAfterServiceEnumId,
                                equipment.statusAfterServiceEnumName,
                                equipment.statusAfterServiceRemarks,
                                equipment.installationStartDate,
                                equipment.installationMatters,
                                equipment.checklistTemplateId,
                                equipment.createdDate,
                                equipment.createdBy,
                                equipment.brand,
                                equipment.lastModifiedBy,
                                equipment.equipmentName,
                                equipment.regionId,
                                equipment.serialNo,
                                equipment.brand,
                                equipment.warrantyStartDate,
                                equipment.warrantyEndDate,
                                equipment.equipmentRemarks,
                                equipment.deliveryAddressId,
                                equipment.salesOrderNo,
                                equipment.warrantyStatus,
                                equipment.accountId,
                                equipment.accountName,
                                equipment.agreementId,
                                equipment.agreementNumber,
                                equipment.agreementEnd,
                                equipment.statusTextEnumId,
                                equipment.statusTextEnumText,
                                equipment.product_id,
                                equipment.product_name,
                                problemFilter as MutableList<ProblemEntity>,
                                solutionFilter as MutableList<SolutionEntity>,
                                sparepartFilter as MutableList<ServiceReportSparepartEntity>,
                                checklistMasterFilter as MutableList<ChecklistQuestionEntity>,
                                checklistReportFilter as MutableList<ServiceReportEquipmentChecklistEntity>,
                                false,
                                false
                            ),
                            reportCompleteOrCancel,
                            hasChecklist
                        )
//                        Log.e("aim", "Checklist Report : $checklistReportFilter")
                    }

                    checkSolution()

                } else {
                    recycler_equipment.visibility = View.GONE
                    warning_equipment.visibility = View.VISIBLE
                    btn_expand.visibility = View.GONE
                }
            })
    }

    private fun timeTrackingObserver() {
        val engineerId = SharedPreferenceData.getString(this, 8, "")
        reportTimeTrackingVM.getByReportId(reportId).observe(this, {
            tx_time_tracking_count.text = ""
            timeTrackingAdapter.clear()

            if (it.isNotEmpty()) {
                timeTrackingAdded = true
                val timeTrackingCount = "(${it.size})"
                tx_time_tracking_count.text = timeTrackingCount
                warning_time_tracking.visibility = View.GONE
                time_tracking_lay.visibility = View.VISIBLE

                if (reportCompleteOrCancel) {
                    timeTrackingAdapter.setList(it, engineerId, false)
                } else {
                    timeTrackingAdapter.setList(it, engineerId, true)
                }
            } else {
                timeTrackingAdded = false
                warning_time_tracking.visibility = View.VISIBLE
                time_tracking_lay.visibility = View.GONE
            }

            // ===================================================================================== Service By Array
            it.forEach { timeTrackingData ->
                if (!serviceByArray.contains(timeTrackingData.engineerName)) {
                    serviceByArray.add(timeTrackingData.engineerName)
                }
            }
            val serviceByString = serviceByArray.toString()
                .replace("[", "")
                .replace("]", "")
            info_service_value.text = serviceByString.ifEmpty { "-" }
        })
    }

    private fun checkSolution() {
        var problemCount = 0
        var solutionCount = 0
        equipmentAdapter.getAllItem().forEach {
            if (it.problemSymptom.isNotEmpty()) {
                problemCount++
            }

            if (it.solution.isNotEmpty()) {
                solutionCount++
            }
        }

        problemAdded = equipmentAdapter.itemCount == problemCount
        solutionAdded = equipmentAdapter.itemCount == solutionCount
    }

    private fun showPdf(fileUrl: String) = runWithPermissions(FunHelper.storageWritePermission) {
        DownloadPdf(this, fileUrl).downloadNow(true)
    }

    private fun getContactList() {
        contactVM.getByAccountId(accountId).observe(this, {
            contactAdapter.setList(it)
        })
    }

    private fun getContact(contactId: String, name: TextView, phone: TextView, type: TextView) {
        contactVM.getById(contactId).observe(this, {
            if (it.isNotEmpty()) {
                name.text = it.first().contact_person_name.ifEmpty { "-" }
                phone.text = it.first().contact_person_phone.ifEmpty { "-" }
                type.text = it.first().contact_type.ifEmpty { "-" }
            }
        })
    }

    private fun editContactDialog() {
        val view = layoutInflater.inflate(R.layout.dialog_list_filter,null)
        val dialog = BottomSheetDialog(this)
        dialog.setContentView(view)
        val bottomSheetBehavior = BottomSheetBehavior.from<View>(view.parent as View)
        dialog.setOnShowListener { bottomSheetBehavior.state = BottomSheetBehavior.STATE_EXPANDED }

        val btnClear = dialog.findViewById<TextView>(R.id.btn_clear)
        btnClear?.visibility = View.GONE

        val recyclerFilter = dialog.findViewById<RecyclerView>(R.id.recycler_filter)
        recyclerFilter?.layoutManager = LinearLayoutManager(this, RecyclerView.VERTICAL, false)
        recyclerFilter?.adapter = contactAdapter

        contactAdapter.setEventHandler(object: SRContactAdapter.RecyclerClickListener {
            override fun isClicked(data: AccountContactEntity) {
                updateReportContact(
                    data.account_contact_id,
                    data.contact_person_name,
                    data.contact_person_phone,
                )

                dialog.dismiss()
            }
        })

        dialog.show()
    }

    private fun updateReportContact(id: String, name: String, phone: String) {
        reportVM.updateContact(
            reportId,
            id,
            name,
            phone,
            false
        )

        sendData()
    }

    private fun cancelReport(reasonValue: String) {
        val progressDialog = ProgressDialog(this)
        progressDialog.setMessage("Cancel Report ...")
        progressDialog.setCancelable(false)
        progressDialog.show()

        val token = "Bearer "+ SharedPreferenceData.getString(this, 2, "")
        val userId = SharedPreferenceData.getString(this, 8, "")
        val params = HashMap<String, Any>()
        params["report_id"] = reportId
        params["cancellation_note"] = reasonValue

        ApiClient.instance.cancelReport(token, params).enqueue(object : Callback<ResponseBody> {
            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                progressDialog.dismiss()
                Toast.makeText(this@ServiceReportDetail, "err ${t.message}, ${t.cause}", Toast.LENGTH_LONG).show()
            }

            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                progressDialog.dismiss()
                if (!response.isSuccessful) {
                    if (response.code() == 401) {
                        FunHelper.clearAllData(this@ServiceReportDetail)
                        Toast.makeText(this@ServiceReportDetail, "Session end", Toast.LENGTH_LONG).show()
                    } else {
                        FunHelper.firebaseApiLog(
                            userId ?: "",
                            response.raw().request().url().toString() ?: "",
                            response.raw().request().headers().toString() ?: "",
                            params.toString() ?: "",
                            response.raw().toString() ?: ""
                        )
                        Toast.makeText(this@ServiceReportDetail, response.raw().toString(), Toast.LENGTH_LONG).show()
                    }
                } else {
                    reportVM.deleteById(reportId)
                    reportEquipmentVM.deleteByReportId(reportId)
                    Toast.makeText(
                        this@ServiceReportDetail,
                        "Cancel report success",
                        Toast.LENGTH_LONG
                    ).show()

                    onBackPressed()
                }
            }
        })
    }

    private fun sendData() {
        val progressDialog = ProgressDialog(this)
        progressDialog.setMessage("Send Data ...")
        progressDialog.setCancelable(false)
        progressDialog.show()

        UploadFile(this).startUploadFile()

        SendDataReport(this).getParam(reportId, "report", object : SendDataInterface {
            override fun sendDataCallback(isSuccess: Boolean) {
                runOnUiThread {
                    progressDialog.dismiss()

                    if (isSuccess) {
                        reportVM.getByReportId(reportId).observe(this@ServiceReportDetail, {
                            if (it.isEmpty()) {
                                onBackPressed()
                            }
                        })
                    }

                }
            }
        })
    }

    private fun syncFcmData() {
        val progressDialog = ProgressDialog(this)
        progressDialog.setMessage("Get New Data ...")
        progressDialog.setCancelable(false)
        progressDialog.show()

        SyncDataFcm(this).getByCaseId(reportId,"report", object : SendDataInterface {
            override fun sendDataCallback(isSuccess: Boolean) {
                progressDialog.dismiss()

                if(isSuccess) {
                    reportObserver()
                }
            }
        })
    }

}
