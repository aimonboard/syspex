package com.syspex.ui

import android.annotation.SuppressLint
import android.app.ProgressDialog
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.core.widget.NestedScrollView
import androidx.lifecycle.ViewModelProviders
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.android.material.tabs.TabLayout
import com.livinglifetechway.quickpermissions_kotlin.runWithPermissions
import com.syspex.Menu2
import com.syspex.R
import com.syspex.data.local.database.account_attachment.AccountAttachmentViewModel
import com.syspex.data.local.database.account_equipment.AccountEquipmentEntity
import com.syspex.data.local.database.account_equipment.AccountEquipmentViewModel
import com.syspex.data.local.database.agreement.AgreementViewModel
import com.syspex.data.local.database.agreement_equipment.AgreementEquipmentEntity
import com.syspex.data.local.database.agreement_equipment.AgreementEquipmentViewModel
import com.syspex.data.local.database.service_call.ServiceCallViewModel
import com.syspex.data.local.database.service_case.ServiceCaseViewModel
import com.syspex.data.local.database.service_report.ServiceReportViewModel
import com.syspex.ui.adapter.*
import com.syspex.ui.helper.DownloadPdf
import com.syspex.ui.helper.FunHelper
import kotlinx.android.synthetic.main.activity_agreement_detail.*
import kotlinx.android.synthetic.main.item_agreement.view.*
import java.net.URL

class AgreementDetail : AppCompatActivity() {

    private lateinit var agreementVM: AgreementViewModel
    private lateinit var agreementEquipmentVM: AgreementEquipmentViewModel
    private lateinit var caseVM: ServiceCaseViewModel
    private lateinit var callVM: ServiceCallViewModel
    private lateinit var reportVM: ServiceReportViewModel
    private lateinit var attachmentVM: AccountAttachmentViewModel

    private val caseAdapter = CaseMyAdapter()
    private val callAdapter = SCMyCallAdapter()
    private val reportAdapter = SRMyReportAdapter()
    private val equipmentAdapter = EquipmentListAdapter()
    private val attachmentAdapter = AttachmentAdapter()

    private var isScrolling = false
    private var agreementId = ""
    private var accountId = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_agreement_detail)
        agreementVM = ViewModelProviders.of(this).get(AgreementViewModel::class.java)
        agreementEquipmentVM = ViewModelProviders.of(this).get(AgreementEquipmentViewModel::class.java)
        caseVM = ViewModelProviders.of(this).get(ServiceCaseViewModel::class.java)
        callVM = ViewModelProviders.of(this).get(ServiceCallViewModel::class.java)
        reportVM = ViewModelProviders.of(this).get(ServiceReportViewModel::class.java)
        attachmentVM = ViewModelProviders.of(this).get(AccountAttachmentViewModel::class.java)

        generateView()
        buttonListener()

        generateTab()
        scrollListener()

        setupAdapter()
        attachmentAdapterListener()

    }

    private fun generateView() {
        if (intent.hasExtra("id")) {
            agreementId = intent.getStringExtra("id") ?: ""
            val headerName = "Agreement : ${intent.getStringExtra("number")}"
            header_name?.text =  headerName

            agreementObserver()
            caseObserver()
            equipmentObserver()
        }
    }

    private fun buttonListener(){
        back_link.setOnClickListener {
            onBackPressed()
        }

//        btn_search.setOnClickListener {
//            val i = Intent(it.context, Search::class.java)
//            startActivity(i)
//        }

        btn_home.setOnClickListener {
            val intent = Intent(this, Menu2::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_NEW_TASK)
            startActivity(intent)
        }

        btn_add_attachment.setOnClickListener {
            val i = Intent(this, AttachmentAdd::class.java)
            i.putExtra("accountId", accountId)
            i.putExtra("sourceId", agreementId)
            i.putExtra("sourceTable","Agreement")
            startActivity(i)
        }
    }

    private fun generateTab() {
        tab_lay.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener{
            override fun onTabUnselected(tab: TabLayout.Tab?) { }
            override fun onTabReselected(tab: TabLayout.Tab?) { }

            override fun onTabSelected(tab: TabLayout.Tab?) {
                if (!isScrolling) {
                    when (tab_lay.selectedTabPosition) {
                        0 -> scroll_lay.smoothScrollTo(0, head_information.top)
                        1 -> scroll_lay.smoothScrollTo(0, head_service_case.top)
                        2 -> scroll_lay.smoothScrollTo(0, head_service_call.top)
                        3 -> scroll_lay.smoothScrollTo(0, head_service_report.top)
                        4 -> scroll_lay.smoothScrollTo(0, head_equipment.top)
                        5 -> scroll_lay.smoothScrollTo(0, head_attachment.top)
                    }
                }
            }

        })
    }

    private fun scrollListener() {
        scroll_lay.setOnScrollChangeListener { v: NestedScrollView?, x: Int, y: Int, oldX: Int, oldY: Int ->
            if (y >= head_information.top && y <= information_lay.bottom){
                isScrolling = true
                tab_lay.getTabAt(0)?.select()
                isScrolling = false
            }
            else if (y >= head_service_case.top && y < head_service_call.top) {
                isScrolling = true
                tab_lay.getTabAt(1)?.select()
                isScrolling = false
            }
            else if (y >= head_service_call.top && y < head_service_report.top) {
                isScrolling = true
                tab_lay.getTabAt(2)?.select()
                isScrolling = false
            }
            else if (y >= head_service_report.top && y < head_equipment.top) {
                isScrolling = true
                tab_lay.getTabAt(3)?.select()
                isScrolling = false
            }
            else if (y >= head_equipment.top && y < head_attachment.top) {
                isScrolling = true
                tab_lay.getTabAt(4)?.select()
                isScrolling = false
            }
            else if (y >= head_attachment.top) {
                isScrolling = true
                tab_lay.getTabAt(4)?.select()
                isScrolling = false
            }
        }
    }

    private fun setupAdapter() {
        FunHelper.setUpAdapter(this, recycler_service_case).adapter = caseAdapter
        FunHelper.setUpAdapter(this, recycler_service_call).adapter = callAdapter
        FunHelper.setUpAdapter(this, recycler_service_report).adapter = reportAdapter
        FunHelper.setUpAdapter(this, recycler_equipment).adapter = equipmentAdapter
        FunHelper.setUpAdapter(this, recycler_attachment).adapter = attachmentAdapter
    }

    private fun attachmentAdapterListener() {
        attachmentAdapter.setEventHandler(object : AttachmentAdapter.RecyclerClickListener {
            override fun isClicked(fileUrl: String) {
                val progressDialog = ProgressDialog(this@AgreementDetail)
                progressDialog.setMessage("Open Attachment ...")
                progressDialog.setCancelable(false)
                progressDialog.show()

                if (fileUrl.isNotEmpty()) {
                    val thread = object : Thread() {
                        override fun run() {
                            var fileExtension = ""
                            try {
                                val connection = URL(fileUrl).openConnection()
                                fileExtension = connection.getHeaderField("Content-Type")
                                Log.e("aim", "extention : $fileExtension")
                            } catch (e: Exception) { }

                            runOnUiThread {
                                if (fileExtension.contains("pdf")) {
                                    progressDialog.dismiss()
                                    showPdf(fileUrl)
                                } else if (fileExtension.contains("jpg") ||
                                    fileExtension.contains("jpeg") ||
                                    fileExtension.contains("png")) {

                                    progressDialog.dismiss()
                                    val i = Intent(this@AgreementDetail, ImagePreview::class.java)
                                    i.putExtra("image", fileUrl)
                                    startActivity(i)
                                } else {
                                    progressDialog.dismiss()
                                    Toast.makeText(
                                        this@AgreementDetail,
                                        "Cant open attachment file",
                                        Toast.LENGTH_LONG
                                    ).show()
                                }
                            }
                        }
                    }
                    thread.start()
                } else {
                    Toast.makeText(
                        this@AgreementDetail,
                        "Attachment file not available",
                        Toast.LENGTH_LONG
                    ).show()
                }
            }

        })
    }

    @SuppressLint("SetTextI18n")
    private fun agreementObserver() {
        agreementVM.getById(agreementId).observe(this, {
            if (it.isNotEmpty()) {

                agreement_status_value.text = it.first().agreementStatusEnumText.ifEmpty { "-" }
                agreement_type_value.text = it.first().agreementTypeEnumName.ifEmpty { "-" }
                agreement_number_value.text = it.first().agreementNumber.ifEmpty { "-" }

                if (!it.first().equipment.isNullOrEmpty()) {
                    agreement_equipment_value.text =  "(${it.first().equipment!!.size}) ${it.first().equipment!!.first()?.product?.productName}"
                } else {
                    agreement_type_value.text = "-"
                }

                val start = FunHelper.uiDateFormat(it.first().agreementStartDate)
                val end = FunHelper.uiDateFormat(it.first().agreementEndDate)
                agreement_period_value.text = "$start - $end"

                agreement_service_value.text = it.first().noOfService.ifEmpty { "-" }
                agreement_relate_id_value.text = it.first().agreementParentId.ifEmpty { "-" }
                agreement_account_value.text = it.first().accountName.ifEmpty { "-" }
                agreement_address_value.text = it.first().installationDeliveryAddressText.ifEmpty { "-" }
                agreement_contact_value.text = it.first().installationContactPicName.ifEmpty { "-" }
                agreement_created_value.text = it.first().createdBy.ifEmpty { "-" }
                agreement_modify_date_value.text = FunHelper.uiDateFormat(it.first().lastModifiedDate)
                agreement_created_date_value.text = FunHelper.uiDateFormat(it.first().createdDate)

                accountId = it.first().accountId
                attachmentObserver(accountId)
            }
        })
    }

    @SuppressLint("SetTextI18n")
    private fun caseObserver() {
        caseVM.getByAgreementId(agreementId).observe(this, {
            callAdapter.clear()
            reportAdapter.clear()
            tx_service_case_count.text = ""

            if (it.isNotEmpty()) {
                tx_service_case_count.text = "(${it.size})"
                warning_case.visibility = View.GONE
                caseAdapter.setList(it, true)

                it.forEach { caseData->
                    callObserver(caseData.caseId)
                    reportObserver(caseData.caseId)
                }

            } else {
                warning_case.visibility = View.VISIBLE
            }
        })
    }



    @SuppressLint("SetTextI18n")
    private fun callObserver(caseId: String) {
        val callLive = callVM.getByCaseId(caseId)
        callVM.getByCaseId(caseId).observe(this, {
            callLive.removeObservers(this)
            tx_service_call_count.text = ""

            if (it.isNotEmpty()) {
                callAdapter.setList(it, true)

                tx_service_call_count.text = "(${callAdapter.itemCount})"
                warning_call.visibility = View.GONE
            } else {
                warning_call.visibility = View.VISIBLE
            }
        })
    }

    @SuppressLint("SetTextI18n")
    private fun reportObserver(caseId: String) {
        val reportLive = reportVM.getByCaseId(caseId)
        reportLive.observe(this, {
            reportLive.removeObservers(this)
            tx_service_report_count.text = ""

            if (it.isNotEmpty()) {
                reportAdapter.setList(it, true)

                tx_service_report_count.text = "(${reportAdapter.itemCount})"
                warning_report.visibility = View.GONE
            } else {
                warning_report.visibility = View.VISIBLE
            }
        })
    }

    @SuppressLint("SetTextI18n")
    private fun equipmentObserver() {
        agreementEquipmentVM.getByAgreementId(agreementId).observe(this, {
            tx_equipment_count.text = ""

            if (it.isNotEmpty()) {
                tx_equipment_count.text = "(${it.size})"
                warning_equipment.visibility = View.GONE

                equipmentAdapter.setList(it, true)
            } else {
                warning_equipment.visibility = View.VISIBLE
            }
        })
    }

    @SuppressLint("SetTextI18n")
    private fun attachmentObserver(accountId: String) {
        attachmentVM.accountAttachmentSection(accountId, agreementId, "Agreement")
            .observe(this, { attachmentData ->
            tx_attachment_count.text = ""

            if (attachmentData.isNotEmpty()) {
                tx_attachment_count.text = "(${attachmentData.size})"
                recycler_attachment.visibility = View.VISIBLE
                warning_attachment.visibility = View.GONE

                attachmentAdapter.setList(attachmentData)
            } else {
                recycler_attachment.visibility = View.GONE
                warning_attachment.visibility = View.VISIBLE
            }
        })
    }

    private fun showPdf(fileUrl: String) = runWithPermissions(FunHelper.storageWritePermission) {
        DownloadPdf(this, fileUrl).downloadNow(false)
    }

}