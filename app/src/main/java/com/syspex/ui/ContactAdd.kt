package com.syspex.ui

import android.annotation.SuppressLint
import android.app.ProgressDialog
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.View
import android.widget.AdapterView
import android.widget.Toast
import androidx.lifecycle.ViewModelProviders
import com.syspex.R
import com.syspex.data.local.SharedPreferenceData
import com.syspex.data.local.database.account.AccountEntity
import com.syspex.data.local.database.account.AccountViewModel
import com.syspex.data.local.database.account_contact.AccountContactEntity
import com.syspex.data.local.database.account_contact.AccountContactViewModel
import com.syspex.data.remote.ContactRequest
import com.syspex.data.remote.GetDataRequest
import com.syspex.data.retrofit.ApiClient
import com.syspex.ui.custom.AutoTextAccountAdapter
import com.syspex.ui.helper.FunHelper
import kotlinx.android.synthetic.main.sub_header.*
import kotlinx.android.synthetic.main.activity_contact_add.*
import kotlinx.android.synthetic.main.activity_contact_add.btn_save
import kotlinx.android.synthetic.main.activity_contact_add.customer_value
import kotlinx.android.synthetic.main.sub_header.back_link
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.HashMap

class ContactAdd : AppCompatActivity() {

    private lateinit var accountVM: AccountViewModel
    private lateinit var contactVM: AccountContactViewModel

    private var accountData : AccountEntity? = null

    private var accountId = ""
    private var accountName = ""
    private var contactId = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_contact_add)
        accountVM = ViewModelProviders.of(this).get(AccountViewModel::class.java)
        contactVM = ViewModelProviders.of(this).get(AccountContactViewModel::class.java)

        generateView()
        accountObserver()
        buttonListener()
    }

    @SuppressLint("SetTextI18n")
    private fun generateView() {
        if (intent.hasExtra("accountId")) {
            accountId = intent.getStringExtra("accountId") ?: ""
            accountName = intent.getStringExtra("accountName") ?: ""
            contactId = intent.getStringExtra("contactId") ?: ""

            header_name?.text = "Edit Contact"
            customer_value_textview.visibility = View.VISIBLE
            customer_lay.visibility = View.GONE

            customer_value_textview.text = intent.getStringExtra("accountName") ?: ""
            name_value.setText(intent.getStringExtra("contactName") ?: "")
            phone_value.setText(intent.getStringExtra("contactPhone") ?: "")
            email_value.setText(intent.getStringExtra("contactEmail") ?: "")
            address_value.setText(intent.getStringExtra("contactAddress") ?: "")
            pos_value.setText(intent.getStringExtra("contactPos") ?: "")
        } else {
            header_name?.text = "Add Contact"
            customer_value_textview.visibility = View.GONE
            customer_lay.visibility = View.VISIBLE
        }
    }

    private fun buttonListener() {
        back_link.setOnClickListener {
            onBackPressed()
        }

        btn_save.setOnClickListener {
            saveContact()
        }
    }

    private fun accountObserver() {
        accountVM.getAll().observe(this, {
            autoTextAccount(it as ArrayList<AccountEntity>)
        })
    }

    private fun autoTextAccount(accountList: ArrayList<AccountEntity>) {
        val adapter = AutoTextAccountAdapter(this, R.layout.item_spinner_black, accountList)
        customer_value.setAdapter(adapter)
        customer_value.threshold = 1
        customer_value.onItemClickListener = AdapterView.OnItemClickListener{ parent, view, position, id->
            accountData = parent.getItemAtPosition(position) as AccountEntity
            accountId = accountData?.accountId ?: ""
            accountName = accountData?.account_name ?: ""
            customer_value.setText(accountData?.account_name ?: "")

        }
        customer_value.onFocusChangeListener = View.OnFocusChangeListener{ view, b ->
            if(b && customer_value.text.isEmpty()) { customer_value.showDropDown() }
        }
    }

    private fun saveContact() {
        val progressDialog = ProgressDialog(this)
        progressDialog.setMessage("Save Contact ...")
        progressDialog.setCancelable(false)
        progressDialog.show()

        val token = "Bearer "+ SharedPreferenceData.getString(this,2, "")
        val userId = SharedPreferenceData.getString(this,8, "")

        val params = HashMap<String, Any>()
        if (contactId.isNotEmpty()) params["account_contact_id"] = contactId
        params["customer_id"] = accountId
        params["contact_type"] = "Contact"
        params["contact_person_name"] = name_value.text.toString()
        params["contact_person_phone"] = phone_value.text.toString()
        params["contact_person_email"] = email_value.text.toString()
        params["contact_person_address"] = address_value.text.toString()
        params["contact_person_position"] = pos_value.text.toString()
        Log.e("aim","contact request : $params")

        ApiClient.instance.sendContact(token, params).enqueue(object: Callback<ContactRequest> {
            override fun onFailure(call: Call<ContactRequest>, t: Throwable) {
                progressDialog.dismiss()
                Toast.makeText(this@ContactAdd, "Save Contact failed", Toast.LENGTH_LONG).show()
            }
            override fun onResponse(call: Call<ContactRequest>, response: Response<ContactRequest>) {
                progressDialog.dismiss()
                if (!response.isSuccessful) {
                    if (response.code() == 401) {
                        FunHelper.clearAllData(this@ContactAdd)
                        Toast.makeText(this@ContactAdd, "Session end", Toast.LENGTH_LONG)
                            .show()
                    }
                    else {
                        FunHelper.firebaseApiLog(
                            userId ?: "",
                            response.raw().request().url().toString() ?: "",
                            response.raw().request().headers().toString() ?: "",
                            params.toString() ?: "",
                            response.raw().toString() ?: ""
                        )
                        Toast.makeText(this@ContactAdd, response.raw().toString(), Toast.LENGTH_LONG).show()
                    }
                } else {
                    contactVM.insert(AccountContactEntity(
                        response.body()?.data?.accountContact?.accountContactId ?: "",
                        response.body()?.data?.accountContact?.regionId ?: "",
                        response.body()?.data?.accountContact?.customerId ?: "",
                        accountName ?: "",
                        response.body()?.data?.accountContact?.contactPersonName ?: "",
                        response.body()?.data?.accountContact?.contactPersonPhone ?: "",
                        response.body()?.data?.accountContact?.contactPersonEmail ?: "",
                        response.body()?.data?.accountContact?.contactPersonAddress ?: "",
                        response.body()?.data?.accountContact?.contactPersonPosition ?: "",
                        response.body()?.data?.accountContact?.contactType ?: "",
                        response.body()?.data?.accountContact?.priority ?: "",
                        response.body()?.data?.accountContact?.createdDate ?: "",
                        response.body()?.data?.accountContact?.createdBy ?: "",
                        response.body()?.data?.accountContact?.lastModifiedDate ?: "",
                        response.body()?.data?.accountContact?.lastModifiedBy ?: ""
                    ))

                    onBackPressed()
                    Toast.makeText(this@ContactAdd, "Save Contact sent", Toast.LENGTH_LONG).show()
                }
            }
        })
    }

}