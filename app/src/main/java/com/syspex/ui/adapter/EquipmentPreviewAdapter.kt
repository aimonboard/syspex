package com.syspex.ui.adapter

import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.syspex.R
import com.syspex.data.local.database.agreement_equipment.AgreementEquipmentEntity
import com.syspex.ui.EquipmentDetail
import com.syspex.ui.helper.FunHelper
import kotlinx.android.synthetic.main.item_equipment_preview.view.*

class EquipmentPreviewAdapter : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private var vendorList = mutableListOf<AgreementEquipmentEntity>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return VendorListViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_equipment_preview, parent, false))
    }
    override fun getItemCount(): Int = vendorList.size

    override fun onBindViewHolder(viewHolder: RecyclerView.ViewHolder, position: Int) {
        val holder = viewHolder as VendorListViewHolder
        holder.bindView(vendorList[position])

        holder.itemView.setOnClickListener {
            val i = Intent(it.context, EquipmentDetail::class.java)
            i.putExtra("id", vendorList[position].equipmentId)
            i.putExtra("number", vendorList[position].serialNo)
            it.context.startActivity(i)
        }

        holder.itemView.report_number_value.setOnClickListener {
            // report detail
        }

        holder.itemView.btn_view_pdf.setOnClickListener {
            // report pdf
        }


    }

    fun setList(listOfVendor: List<AgreementEquipmentEntity>) {
        this.vendorList = listOfVendor.toMutableList()
        notifyDataSetChanged()
    }

    fun addData(data: AgreementEquipmentEntity) {
        this.vendorList.add(data)
        notifyDataSetChanged()
    }

    fun reset() {
        this.vendorList.clear()
        notifyDataSetChanged()
    }

    class VendorListViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bindView(vendorModel: AgreementEquipmentEntity) {
            itemView.status_header_value.text = vendorModel.statusTextEnumText.ifEmpty { "-" }
            itemView.number_value.text = vendorModel.serialNo.ifEmpty { "-" }
            itemView.product_value.text = vendorModel.product_name.ifEmpty { "-" }
            itemView.company_value.text = vendorModel.accountName.ifEmpty { "-" }
            itemView.Status_value.text = vendorModel.statusTextEnumText.ifEmpty { "-" }
            itemView.branch_value.text = "-"
            itemView.agreement_number_value.text  = vendorModel.agreementNumber.ifEmpty { "-" }
            itemView.agreement_start_value.text = "-"
            itemView.agreement_end_value.text = FunHelper.uiDateFormat(vendorModel.agreementEnd)
            itemView.warranty_start_value.text = FunHelper.uiDateFormat(vendorModel.warrantyStartDate)
            itemView.warranty_end_value.text = FunHelper.uiDateFormat(vendorModel.warrantyEndDate)
            itemView.report_number_value.text = "-"
        }
    }

}