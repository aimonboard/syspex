package com.syspex.ui.adapter

import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.syspex.R
import com.syspex.data.local.database.pcs_solution.SolutionEntity
import com.syspex.data.remote.GetDataRequest
import com.syspex.ui.CauseAdd
import kotlinx.android.synthetic.main.item_subject.view.*

class SRCauseSolutionAdapter : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private var vendorList = mutableListOf<SolutionEntity>()
    private var eventHandler: RecyclerClickListener? = null
    private var needEditDelete = false
    private var equipmentId = ""

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return VendorListViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_subject, parent, false))
    }
    override fun getItemCount(): Int = vendorList.size

    override fun onBindViewHolder(viewHolder: RecyclerView.ViewHolder, position: Int) {
        val holder = viewHolder as VendorListViewHolder
        holder.bindView(vendorList[position], needEditDelete)

        holder.itemView.btn_edit.setOnClickListener {
            val i = Intent(it.context, CauseAdd::class.java)
            i.putExtra("edit_reportEquipmentId", vendorList[position].reportEquipmentId_solution)
            i.putExtra("edit_equipmentId", equipmentId)
            i.putExtra("edit_solutionId", vendorList[position].serviceReportEquipmentSolutionId)
            it.context.startActivity(i)
        }

        holder.itemView.btn_delete.setOnClickListener {
            eventHandler?.isCauseSolutionDeleted(vendorList[position])
        }

        if (position == vendorList.lastIndex) {
            holder.itemView.divider.visibility = View.GONE
        } else {
            holder.itemView.divider.visibility = View.VISIBLE
        }

    }

    fun setList(listOfVendor: List<SolutionEntity>, equipmentId: String, needEditDelete: Boolean) {
        this.vendorList = listOfVendor.toMutableList()
        this.equipmentId = equipmentId
        this.needEditDelete = needEditDelete
        notifyDataSetChanged()
    }

    fun addData(data: SolutionEntity, equipmentId: String, needEditDelete: Boolean) {
        this.vendorList.add(data)
        this.equipmentId = equipmentId
        this.needEditDelete = needEditDelete
        notifyDataSetChanged()
    }

    fun clear() {
        this.vendorList.clear()
        notifyDataSetChanged()
    }

    class VendorListViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bindView(data: SolutionEntity, needEditDelete: Boolean) {
            val causeAdapter = SRImageCauseAdapter(false)
            val solutionAdapter = SRImageSolutionAdapter(false)

            itemView.subject_value.text = data.subject

            itemView.cause_value.text = data.problemCause
            itemView.recycler_cause.layoutManager = LinearLayoutManager(itemView.context, RecyclerView.HORIZONTAL, false)
            itemView.recycler_cause.adapter = causeAdapter

            causeAdapter.clear()
            data.causeImage?.forEach {
                causeAdapter.addData(it)
            }

            itemView.solution_value.text = data.solution
            itemView.recycler_solution.layoutManager = LinearLayoutManager(itemView.context, RecyclerView.HORIZONTAL, false)
            itemView.recycler_solution.adapter = solutionAdapter

            solutionAdapter.clear()
            data.solutionImage?.forEach {
                solutionAdapter.addData(it)
            }

            if (needEditDelete) {
                itemView.btn_edit.visibility = View.VISIBLE
                itemView.btn_delete.visibility = View.VISIBLE
            } else {
                itemView.btn_edit.visibility = View.GONE
                itemView.btn_delete.visibility = View.GONE
            }
        }
    }

    fun setEventHandler(eventHandler: RecyclerClickListener) {
        try {
            this.eventHandler = eventHandler
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    interface RecyclerClickListener {
        fun isCauseSolutionDeleted(data: SolutionEntity)
    }

}