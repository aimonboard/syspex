package com.syspex.ui.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.syspex.R
import com.syspex.data.model.PersonelModel
import kotlinx.android.synthetic.main.item_personel.view.*

class PersonelAdapter : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private var vendorList = mutableListOf<PersonelModel>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return VendorListViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_personel, parent, false))
    }
    override fun getItemCount(): Int = vendorList.size

    override fun onBindViewHolder(viewHolder: RecyclerView.ViewHolder, position: Int) {
        val holder = viewHolder as VendorListViewHolder
        holder.bindView(vendorList[position])

        holder.itemView.btn_delete.setOnClickListener {
            vendorList.removeAt(position)
            notifyItemRemoved(position)
        }
    }

    fun setList(listOfVendor: List<PersonelModel>) {
        this.vendorList = listOfVendor.toMutableList()
        notifyDataSetChanged()
    }

    fun addList(data: PersonelModel) {
        this.vendorList.add(data)
        notifyDataSetChanged()
    }

    fun getAllItem (): List<PersonelModel> {
        return this.vendorList
    }

    class VendorListViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bindView(vendorModel: PersonelModel) {
            itemView.tx_personel.text = vendorModel.personelName
            itemView.tx_designation.text = vendorModel.designation
        }
    }

}