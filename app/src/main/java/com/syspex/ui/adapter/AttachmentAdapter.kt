package com.syspex.ui.adapter

import android.annotation.SuppressLint
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.syspex.R
import com.syspex.data.local.database.account_attachment.AccountAttachmentEntity
import com.syspex.data.remote.GetDataRequest
import com.syspex.ui.AttachmentAdd
import com.syspex.ui.helper.FunHelper
import kotlinx.android.synthetic.main.item_attachment.view.*

class AttachmentAdapter : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private var vendorList = mutableListOf<AccountAttachmentEntity>()
    private var eventHandler: RecyclerClickListener? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return VendorListViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_attachment, parent, false))
    }
    override fun getItemCount(): Int = vendorList.size

    override fun onBindViewHolder(viewHolder: RecyclerView.ViewHolder, position: Int) {
        val holder = viewHolder as VendorListViewHolder
        holder.bindView(vendorList[position])

        holder.itemView.btn_view.setOnClickListener {
            eventHandler?.isClicked(vendorList[position].fileUrl ?: "")
        }

        holder.itemView.btn_edit.setOnClickListener {
            val i = Intent(it.context, AttachmentAdd::class.java)
            i.putExtra("accountId", vendorList[position].accountId)
            i.putExtra("attachmentId", vendorList[position].accountAttachmentId)
            i.putExtra("sourceId", vendorList[position].attachmentSourceId)
            i.putExtra("sourceTable",vendorList[position].attachmentSourceTable)
            i.putExtra("title",vendorList[position].attachmentTitle)
            it.context.startActivity(i)
        }

    }

    fun setList(listOfVendor: List<AccountAttachmentEntity>) {
        this.vendorList = listOfVendor.toMutableList()
        notifyDataSetChanged()
    }

    fun addList(data: AccountAttachmentEntity) {
        this.vendorList.add(data)
        notifyDataSetChanged()
    }

    fun clear() {
        this.vendorList.clear()
    }

    class VendorListViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        @SuppressLint("SetTextI18n")
        fun bindView(data: AccountAttachmentEntity) {
            itemView.attachment_body.text = data.attachmentTitle.ifEmpty { "-" }
            itemView.created_by_value.text = data.createdByName.ifEmpty { "-" }
            itemView.created_date_value.text = FunHelper.uiDateFormat(data.createdDate)

        }
    }

    fun setEventHandler(eventHandler: RecyclerClickListener) {
        try {
            this.eventHandler = eventHandler
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    interface RecyclerClickListener {
        fun isClicked(fileUrl: String)
    }

}