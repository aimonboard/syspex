package com.syspex.ui.adapter

import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.syspex.R
import com.syspex.data.local.database.service_report_sparepart.ServiceReportSparepartEntity
import com.syspex.data.model.SpinnerModel
import kotlinx.android.synthetic.main.item_sparepart_add.view.*

class SparePartAddAdapter : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private var vendorList = mutableListOf<ServiceReportSparepartEntity>()
    private var eventHandler: RecyclerClickListener? = null
    private var enumData = ArrayList<SpinnerModel>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return VendorListViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_sparepart_add, parent, false))
    }
    override fun getItemCount(): Int = vendorList.size

    override fun onBindViewHolder(viewHolder: RecyclerView.ViewHolder, position: Int) {
        val holder = viewHolder as VendorListViewHolder
        holder.bindView(vendorList[position], enumData)

        holder.itemView.btn_delete.setOnClickListener {
            eventHandler?.isDelete(position, vendorList[position])
        }

        holder.itemView.btn_edit.setOnClickListener {
            eventHandler?.isEdit(position, vendorList[position])
        }

    }

    fun setList(listOfVendor: List<ServiceReportSparepartEntity>, enumMaster: ArrayList<SpinnerModel>) {
        this.vendorList = listOfVendor.toMutableList()
        this.enumData = enumMaster
        notifyDataSetChanged()
    }

    fun addList(listOfVendor: ServiceReportSparepartEntity) {
        this.vendorList.add(listOfVendor)
        notifyDataSetChanged()
    }

    fun edit(postition: Int, listOfVendor: ServiceReportSparepartEntity) {
        this.vendorList[postition] = listOfVendor
        notifyDataSetChanged()
    }

    fun remove(postition: Int, listOfVendor: ServiceReportSparepartEntity) {
        this.vendorList[postition] = listOfVendor
        notifyDataSetChanged()
    }

    fun getAll(): MutableList<ServiceReportSparepartEntity> {
        return this.vendorList
    }

    class VendorListViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bindView(data: ServiceReportSparepartEntity, enumData: ArrayList<SpinnerModel>) {

            itemView.tx_title1.text = data.spare_part_number.ifEmpty { "-" }
            itemView.tx_title2.text = data.spare_part_description.ifEmpty { "-" }
            itemView.spinner_value.text = data.enum_name
            itemView.remark_value.text = data.spare_part_memo
            itemView.qty_value.text = data.qty

        }
    }

    fun setEventHandler(eventHandler: RecyclerClickListener) {
        try {
            this.eventHandler = eventHandler
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    interface RecyclerClickListener {
        fun isDelete(position: Int, data: ServiceReportSparepartEntity)
        fun isEdit(position: Int, data: ServiceReportSparepartEntity)
    }

}