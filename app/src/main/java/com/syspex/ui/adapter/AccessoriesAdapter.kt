package com.syspex.ui.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.syspex.R
import com.syspex.data.model.AccessoriesModel
import kotlinx.android.synthetic.main.item_accessories.view.*

class AccessoriesAdapter : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private var vendorList = mutableListOf<AccessoriesModel>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return VendorListViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_accessories, parent, false))
    }
    override fun getItemCount(): Int = vendorList.size

    override fun onBindViewHolder(viewHolder: RecyclerView.ViewHolder, position: Int) {
        val holder = viewHolder as VendorListViewHolder
        holder.bindView(vendorList[position])

        holder.itemView.btn_close.setOnClickListener {
            vendorList.removeAt(position)
            notifyItemRemoved(position)
        }

        if (position == vendorList.size-1) {
            holder.itemView.divider.visibility = View.GONE
        } else {
            holder.itemView.divider.visibility = View.VISIBLE
        }
    }

    fun setList(listOfVendor: List<AccessoriesModel>) {
        this.vendorList = listOfVendor.toMutableList()
        notifyDataSetChanged()
    }

    fun addList(data: AccessoriesModel) {
        this.vendorList.add(data)
        notifyDataSetChanged()
    }

    fun getAllItem (): List<AccessoriesModel> {
        return this.vendorList
    }

    class VendorListViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bindView(vendorModel: AccessoriesModel) {
            itemView.product_value.text = vendorModel.productNumber.ifEmpty { "-" }
            itemView.desc_value.text = vendorModel.description.ifEmpty { "-" }
            itemView.qty_value.text = vendorModel.quantity.ifEmpty { "0" }
        }
    }

}