package com.syspex.ui.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.syspex.R
import com.syspex.data.local.database.activity.ActivityEntity
import com.syspex.ui.helper.FunHelper
import kotlinx.android.synthetic.main.item_activity_list.view.*


class ActivityListAdapter : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private var vendorList = mutableListOf<ActivityEntity>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return VendorListViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_activity_list, parent, false))
    }
    override fun getItemCount(): Int = vendorList.size

    override fun onBindViewHolder(viewHolder: RecyclerView.ViewHolder, position: Int) {
        val holder = viewHolder as VendorListViewHolder
        holder.bindView(vendorList[position])

    }

    fun setList(listOfVendor: List<ActivityEntity>) {
        this.vendorList = listOfVendor.toMutableList()
        notifyDataSetChanged()
    }

    fun addData(data: ActivityEntity) {
        this.vendorList.add(data)
        notifyDataSetChanged()
    }

    fun clear() {
        this.vendorList.clear()
        notifyDataSetChanged()
    }

    class VendorListViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bindView(vendorModel: ActivityEntity) {

            itemView.activity_subject_value.text = vendorModel.activitySubject.ifEmpty { "-" }
            itemView.activity_type_value.text = vendorModel.activityTypeName.ifEmpty { "-" }
            itemView.activity_create_value.text = vendorModel.createdBy.ifEmpty { "-" }
            itemView.activity_start_value.text = FunHelper.uiDateFormat(vendorModel.activityStartTime)
            itemView.activity_end_value.text = FunHelper.uiDateFormat(vendorModel.activityEndTime)

        }
    }

}