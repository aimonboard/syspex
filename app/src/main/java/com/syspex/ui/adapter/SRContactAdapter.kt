package com.syspex.ui.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.syspex.R
import com.syspex.data.local.database.account_contact.AccountContactEntity
import kotlinx.android.synthetic.main.item_report_contact.view.*

class SRContactAdapter : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private var vendorList = mutableListOf<AccountContactEntity>()
    private var eventHandler: RecyclerClickListener? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return VendorListViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_report_contact, parent, false))
    }
    override fun getItemCount(): Int = vendorList.size

    override fun onBindViewHolder(viewHolder: RecyclerView.ViewHolder, position: Int) {
        val holder = viewHolder as VendorListViewHolder
        holder.bindView(vendorList[position])

        holder.itemView.btn_select_contact.setOnClickListener {
            eventHandler?.isClicked(vendorList[position])
        }

    }

    fun setList(listOfVendor: List<AccountContactEntity>) {
        this.vendorList = listOfVendor.toMutableList()
        notifyDataSetChanged()
    }

    fun addData(data: AccountContactEntity) {
        this.vendorList.add(data)
        notifyDataSetChanged()
    }

    fun clear() {
        this.vendorList.clear()
        notifyDataSetChanged()
    }

    class VendorListViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bindView(vendorModel: AccountContactEntity) {
            itemView.name_value.text = vendorModel.contact_person_name.ifEmpty { "-" }
            itemView.phone_value.text = vendorModel.contact_person_phone.ifEmpty { "-" }
            itemView.email_value.text = vendorModel.contact_person_email.ifEmpty { "-" }
            itemView.position_value.text = vendorModel.contact_person_position.ifEmpty { "-" }
            itemView.company_value.text = vendorModel.account_name.ifEmpty { "-" }
        }
    }

    fun setEventHandler(eventHandler: RecyclerClickListener) {
        try {
            this.eventHandler = eventHandler
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    interface RecyclerClickListener {
        fun isClicked(data: AccountContactEntity)
    }

}