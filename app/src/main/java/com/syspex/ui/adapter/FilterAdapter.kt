package com.syspex.ui.adapter

import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.syspex.R
import com.syspex.data.local.database.global_enum.EnumEntity
import kotlinx.android.synthetic.main.item_filter.view.*

class FilterAdapter : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private var vendorList = mutableListOf<EnumEntity>()
    private var eventHandler: RecyclerClickListener? = null

    private var itemStateArray = ArrayList<String>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return VendorListViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_filter, parent, false))
    }
    override fun getItemCount(): Int = vendorList.size

    override fun onBindViewHolder(viewHolder: RecyclerView.ViewHolder, position: Int) {
        val holder = viewHolder as VendorListViewHolder
        holder.bindView(vendorList[position], itemStateArray)

        holder.itemView.checkbox_filter.setOnCheckedChangeListener { compoundButton, b ->
            if (b) {
                if (!itemStateArray.contains(vendorList[position].enum_id)) {
                    itemStateArray.add(vendorList[position].enum_id)
                }
            } else {
                if (itemStateArray.contains(vendorList[position].enum_id)) {
                    val aIndex = itemStateArray.indexOf(vendorList[position].enum_id)
                    itemStateArray.removeAt(aIndex)
                }
            }

            if (itemStateArray.size == vendorList.size) {
                eventHandler?.isChecked(itemStateArray, true)
            } else {
                eventHandler?.isChecked(itemStateArray, false)
            }
        }

    }

    fun setList(listOfVendor: List<EnumEntity>) {
        this.vendorList = listOfVendor.toMutableList()
        listOfVendor.forEach {
            this.itemStateArray.add(it.enum_id)
        }
        notifyDataSetChanged()
    }

    fun addData(data: EnumEntity) {
        this.vendorList.add(data)
        this.itemStateArray.add(data.enum_id)
        notifyDataSetChanged()
    }

    fun restore(restoreData: ArrayList<String>) {
        this.itemStateArray = restoreData
        notifyDataSetChanged()
    }

    fun clear() {
        this.vendorList.clear()
        notifyDataSetChanged()
    }

    class VendorListViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bindView(vendorModel: EnumEntity, restoreData: ArrayList<String>) {
            itemView.checkbox_filter.text = vendorModel.enum_name
            itemView.checkbox_filter.isChecked = restoreData.contains(vendorModel.enum_id)
        }
    }

    fun setEventHandler(eventHandler: RecyclerClickListener) {
        try {
            this.eventHandler = eventHandler
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    interface RecyclerClickListener {
        fun isChecked(checkedId: ArrayList<String>, checkedAll: Boolean)
    }

}