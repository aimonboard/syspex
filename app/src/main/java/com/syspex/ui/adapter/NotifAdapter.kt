package com.syspex.ui.adapter

import android.annotation.SuppressLint
import android.content.Intent
import android.util.SparseBooleanArray
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.syspex.R
import com.syspex.data.remote.NotifRequest
import com.syspex.ui.*
import com.syspex.ui.helper.FunHelper
import kotlinx.android.synthetic.main.item_notif.view.*


class NotifAdapter : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private var vendorList = mutableListOf<NotifRequest.Data.Notification>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return VendorListViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_notif, parent, false))
    }
    override fun getItemCount(): Int = vendorList.size

    override fun onBindViewHolder(viewHolder: RecyclerView.ViewHolder, position: Int) {
        val holder = viewHolder as VendorListViewHolder
        holder.bindView(vendorList[position])

        holder.itemView.setOnClickListener {
            when (vendorList[position].notificationSource) {
                "Case" -> {
                    val i = Intent(it.context, CaseDetail::class.java)
                    i.putExtra("notif_case_id", vendorList[position].notificationSourceId)
                    it.context.startActivity(i)
                }
                "Call" -> {
                    val i = Intent(it.context, ServiceCallDetail::class.java)
                    i.putExtra("notif_call_id", vendorList[position].notificationSourceId)
                    it.context.startActivity(i)
                }
                "Report" -> {
                    val i = Intent(it.context, ServiceReportDetail::class.java)
                    i.putExtra("notif_report_id", vendorList[position].notificationSourceId)
                    it.context.startActivity(i)
                }
                "News" -> {
                    val i = Intent(it.context, NewsDetail::class.java)
                    i.putExtra("notif_news_id", vendorList[position].notificationSourceId)
                    it.context.startActivity(i)
                }
            }
        }

    }

    fun setList(listOfVendor: List<NotifRequest.Data.Notification>) {
        this.vendorList = listOfVendor.toMutableList()
        notifyDataSetChanged()
    }

    fun addData(data: NotifRequest.Data.Notification) {
        this.vendorList.add(data)
    }

    fun clear() {
        this.vendorList.clear()
        notifyDataSetChanged()
    }

    class VendorListViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        @SuppressLint("SetTextI18n")
        fun bindView(notifData: NotifRequest.Data.Notification) {
            itemView.tx_title.text = notifData.notificationTitle ?: "-"
            itemView.tx_body.text = notifData.notificationBody ?: "-"
            itemView.tx_date.text = FunHelper.uiDateFormat(notifData.createdDate ?: "")
        }
    }

}