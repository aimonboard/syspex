package com.syspex.ui.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.syspex.R
import com.syspex.data.local.database.service_report_sparepart.ServiceReportSparepartEntity
import kotlinx.android.synthetic.main.item_sparepart.view.*

class SRSparePartAdapter : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private var vendorList = mutableListOf<ServiceReportSparepartEntity>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return VendorListViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_sparepart, parent, false))
    }
    override fun getItemCount(): Int = vendorList.size

    override fun onBindViewHolder(viewHolder: RecyclerView.ViewHolder, position: Int) {
        val holder = viewHolder as VendorListViewHolder
        holder.bindView(vendorList[position])

        if (position+1 == itemCount) {
            holder.itemView.divider.visibility = View.GONE
        } else {
            holder.itemView.divider.visibility = View.VISIBLE
        }

    }

    fun setList(listOfVendor: List<ServiceReportSparepartEntity>) {
        this.vendorList = listOfVendor.toMutableList()
        notifyDataSetChanged()
    }

    fun addData(data: ServiceReportSparepartEntity) {
        this.vendorList.add(data)
        notifyDataSetChanged()
    }

    fun clear() {
        this.vendorList.clear()
        notifyDataSetChanged()
    }

    class VendorListViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bindView(data: ServiceReportSparepartEntity) {

            val imageAndPartNumber = "${data.spare_part_number.ifEmpty { "-" }} | ${data.spare_part_description.ifEmpty { "-" }}"
            itemView.number_value.text = imageAndPartNumber
            itemView.type_value.text = data.enum_name
            itemView.qty_value.text = data.qty

        }
    }

}