package com.syspex.ui.adapter

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.syspex.R
import com.syspex.data.remote.SearchEquipmentOnlineRequest
import com.syspex.ui.helper.FunHelper
import kotlinx.android.synthetic.main.item_equipment_online.view.*


class EquipmentSearchOnlineAdapter : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private var eventHandler: RecyclerClickListener? = null
    private var vendorList = mutableListOf<SearchEquipmentOnlineRequest.Data.Equipment>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return VendorListViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_equipment_online, parent, false))
    }
    override fun getItemCount(): Int = vendorList.size

    override fun onBindViewHolder(viewHolder: RecyclerView.ViewHolder, position: Int) {
        val holder = viewHolder as VendorListViewHolder
        holder.bindView(vendorList[position])

        holder.itemView.btn_download.setOnClickListener {
            eventHandler?.isClicked(
                vendorList[position].equipmentId ?: "",
                vendorList[position].accountId ?: ""
            )
        }

    }

    fun setList(listOfVendor: List<SearchEquipmentOnlineRequest.Data.Equipment>) {
        this.vendorList = listOfVendor.toMutableList()
        notifyDataSetChanged()
    }

    fun addData(data: SearchEquipmentOnlineRequest.Data.Equipment) {
        this.vendorList.add(data)
    }

    class VendorListViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        @SuppressLint("SetTextI18n")
        fun bindView(vendorModel: SearchEquipmentOnlineRequest.Data.Equipment) {
            val status = vendorModel.statusText?.enumName ?: "-"
            itemView.status_value.text = status.ifEmpty { "-" }

            val serialNo = vendorModel.serialNo ?: "-"
            val productName = vendorModel.product?.productName ?: "-"
            val accountName = vendorModel.account?.accountName ?: "-"
            val warrantyEnd = FunHelper.uiDateFormat(vendorModel.warrantyEnd ?: "")
            val agreementEnd = FunHelper.uiDateFormat("")

            itemView.number_value.text = "SN: ${serialNo.ifEmpty { "-" }}"
            itemView.product_value.text = productName.ifEmpty { "-" }
            itemView.company_value.text = accountName.ifEmpty { "-" }
            itemView.warranty_value.text = "Warranty End: ${warrantyEnd.ifEmpty { "-" }}"
            itemView.agreement_end.text = "Agreement End: ${agreementEnd.ifEmpty { "-" }}"
        }
    }

    fun setEventHandler(eventHandler: RecyclerClickListener) {
        try {
            this.eventHandler = eventHandler
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    interface RecyclerClickListener {
        fun isClicked(equipmentId: String, accountId: String)
    }

}