package com.syspex.ui.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.syspex.R
import com.syspex.data.local.database.account_equipment.AccountEquipmentEntity
import kotlinx.android.synthetic.main.item_handover_equipment.view.*

class CallAddEquipmentAdapter : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private var vendorList = mutableListOf<AccountEquipmentEntity>()
    private var eventHandler: RecyclerClickListener? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return VendorListViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_handover_equipment, parent, false))
    }
    override fun getItemCount(): Int = vendorList.size

    override fun onBindViewHolder(viewHolder: RecyclerView.ViewHolder, position: Int) {
        val holder = viewHolder as VendorListViewHolder
        holder.bindView(vendorList[position])

        holder.itemView.checkbox_equipment.setOnCheckedChangeListener { compoundButton, b ->
            if (b) {
                eventHandler?.isAdd(vendorList[position].equipmentId)
            } else {
                eventHandler?.isRemove(vendorList[position].equipmentId)
            }
        }
    }

        fun setList(listOfVendor: List<AccountEquipmentEntity>) {
        this.vendorList = listOfVendor.toMutableList()
        notifyDataSetChanged()
    }

        fun addData(data:AccountEquipmentEntity) {
        this.vendorList.add(data)
        notifyDataSetChanged()
    }

    fun clear() {
        this.vendorList.clear()
        notifyDataSetChanged()
    }

    class VendorListViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
            fun bindView(vendorModel:AccountEquipmentEntity) {
            itemView.serial_value.text = vendorModel.serialNo
            itemView.name_value.text = vendorModel.product_name
        }
    }

    fun setEventHandler(eventHandler: RecyclerClickListener) {
        try {
            this.eventHandler = eventHandler
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    interface RecyclerClickListener {
        fun isAdd(equipmentId: String)
        fun isRemove(equipmentId: String)
    }

}