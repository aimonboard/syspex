package com.syspex.ui.adapter

import android.annotation.SuppressLint
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.FragmentActivity
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.syspex.R
import com.syspex.data.model.EquipmentListModel
import com.syspex.ui.EquipmentDetail
import com.syspex.ui.EquipmentPreview
import com.syspex.ui.ServiceReportDetail
import com.syspex.ui.diffutil.EquipmentDiffUtil
import com.syspex.ui.helper.FunHelper
import kotlinx.android.synthetic.main.item_equipment.view.*

class EquipmentListAdapter : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private var vendorList = mutableListOf<EquipmentListModel>()
    private var withPreview = false

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return VendorListViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_equipment, parent, false))
    }
    override fun getItemCount(): Int = vendorList.size

    override fun onBindViewHolder(viewHolder: RecyclerView.ViewHolder, position: Int) {
        val holder = viewHolder as VendorListViewHolder
        holder.bindView(vendorList[position], withPreview)

        holder.itemView.setOnClickListener {
            val i = Intent(it.context, EquipmentDetail::class.java)
            i.putExtra("id", vendorList[position].equipmentId)
            i.putExtra("number", vendorList[position].serialNo)
            it.context.startActivity(i)
        }

        holder.itemView.btn_down.setOnClickListener {
            EquipmentPreview.equipmentId = vendorList[position].equipmentId ?: ""
            EquipmentPreview().show((it.context as FragmentActivity).supportFragmentManager,EquipmentPreview().tag)
        }

        holder.itemView.btn_pdf.setOnClickListener {
            val i = Intent(it.context, ServiceReportDetail::class.java)
            i.putExtra("id", vendorList[position].reportLocalId)
            it.context.startActivity(i)
        }

    }

    fun setList(listOfVendor: List<EquipmentListModel>, withPreview: Boolean) {
//        this.vendorList = listOfVendor.toMutableList()
//        notifyDataSetChanged()
        this.withPreview = withPreview
        val diffCallback = EquipmentDiffUtil(this.vendorList, listOfVendor)
        val diffResult = DiffUtil.calculateDiff(diffCallback)
        this.vendorList.clear()
        this.vendorList.addAll(listOfVendor)
        diffResult.dispatchUpdatesTo(this)
    }

    fun addData(data: EquipmentListModel, withPreview: Boolean) {
        this.withPreview = withPreview
        this.vendorList.add(data)
        notifyDataSetChanged()
    }

    fun reset() {
        this.vendorList.clear()
        notifyDataSetChanged()
    }

    class VendorListViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        @SuppressLint("SetTextI18n")
        fun bindView(vendorModel: EquipmentListModel, withPreview: Boolean) {
            val status = vendorModel.statusTextEnumText ?: "-"
            val type = vendorModel.callTypeEnumText ?: "-"
            itemView.status_value.text = status.ifEmpty { "-" }
            itemView.status2_value.text = type.ifEmpty { "-" }

            val serialNo = vendorModel.serialNo ?: "-"
            val productName = vendorModel.product_name ?: "-"
            val accountName = vendorModel.accountName ?: "-"
            val warrantyEnd = FunHelper.uiDateFormat(vendorModel.warrantyEndDate ?: "")
            val agreementEnd = FunHelper.uiDateFormat(vendorModel.agreementEnd ?: "")
            val lastReport = FunHelper.uiDateFormat(vendorModel.createdDate ?: "")

            itemView.number_value.text = "SN: ${serialNo.ifEmpty { "-" }}"
            itemView.product_value.text = productName.ifEmpty { "-" }
            itemView.company_value.text = accountName.ifEmpty { "-" }
            itemView.warranty_value.text = "Warranty End: ${warrantyEnd.ifEmpty { "-" }}"
            itemView.agreement_end.text = "Agreement End: ${agreementEnd.ifEmpty { "-" }}"
            itemView.btn_pdf.text = vendorModel.serviceReportNumber

            if (!vendorModel.servicedByUserName.isNullOrEmpty()) {
                itemView.engineer_value.visibility = View.VISIBLE
                itemView.engineer_value.text = vendorModel.servicedByUserName
            } else {
                itemView.engineer_value.visibility = View.INVISIBLE
            }

            val reportStatus = vendorModel.reportStatusEnumText ?: "-"
            val reportType = vendorModel.callTypeEnumText ?: "-"
            itemView.status3_value.text = reportStatus.ifEmpty { "-" }
            itemView.status4_value.text = reportType.ifEmpty { "-" }

            if (lastReport.isNotEmpty()) {
                itemView.divider.visibility = View.VISIBLE
                itemView.report_value.visibility = View.VISIBLE
                itemView.btn_pdf.visibility = View.VISIBLE
                itemView.engineer_value.visibility = View.VISIBLE
                itemView.status3_value.visibility = View.VISIBLE
                itemView.status4_value.visibility = View.VISIBLE
            } else {
                itemView.divider.visibility = View.GONE
                itemView.report_value.visibility = View.GONE
                itemView.btn_pdf.visibility = View.GONE
                itemView.engineer_value.visibility = View.GONE
                itemView.status3_value.visibility = View.GONE
                itemView.status4_value.visibility = View.GONE
            }

            if (!withPreview) {
                itemView.btn_down.visibility = View.GONE
            }

        }
    }

}