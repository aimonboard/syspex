package com.syspex.ui.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.syspex.R
import com.syspex.data.local.database.product_sparepart.SparepartEntity
import kotlinx.android.synthetic.main.item_sparepart_master.view.*

class SparePartMasterAdapter : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private var vendorList = mutableListOf<SparepartEntity>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return VendorListViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_sparepart_master, parent, false))
    }
    override fun getItemCount(): Int = vendorList.size

    override fun onBindViewHolder(viewHolder: RecyclerView.ViewHolder, position: Int) {
        val holder = viewHolder as VendorListViewHolder
        holder.bindView(vendorList[position])

        if (position+1 == itemCount) {
            holder.itemView.divider.visibility = View.GONE
        } else {
            holder.itemView.divider.visibility = View.VISIBLE
        }

    }

    fun setList(listOfVendor: List<SparepartEntity>) {
        this.vendorList = listOfVendor.toMutableList()
        notifyDataSetChanged()
    }

    fun addData(data: SparepartEntity) {
        this.vendorList.add(data)
        notifyDataSetChanged()
    }

    fun clear() {
        this.vendorList.clear()
        notifyDataSetChanged()
    }

    class VendorListViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bindView(data: SparepartEntity) {
            itemView.part_number_value.text = data.part_no.ifEmpty { "-" }
            itemView.part_name_value.text = data.part_name.ifEmpty { "-" }
            itemView.image_number_value.text = data.image_number.ifEmpty { "-" }
        }
    }

}