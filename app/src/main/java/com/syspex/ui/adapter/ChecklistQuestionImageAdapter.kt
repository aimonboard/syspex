package com.syspex.ui.adapter
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.syspex.R
import com.syspex.data.model.ConverterImageModel
import com.syspex.ui.helper.ChecklistInterface
import kotlinx.android.synthetic.main.item_image.view.*

class ChecklistQuestionImageAdapter(private var itemListener: ChecklistInterface) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private var questionId = ""
    private var vendorList = ArrayList<ConverterImageModel>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return VendorListViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_image, parent, false))
    }
    override fun getItemCount(): Int = vendorList.size

    override fun onBindViewHolder(viewHolder: RecyclerView.ViewHolder, position: Int) {
        val holder = viewHolder as VendorListViewHolder
        holder.bindView(vendorList[position])

        holder.itemView.btn_delete.setOnClickListener {
            vendorList.remove(vendorList[position])
            notifyDataSetChanged()

            itemListener.onQuestionDeleteImage(
                questionId = questionId,
                image = vendorList,
            )
        }

    }

    fun setList(questionId: String, image: ArrayList<ConverterImageModel>) {
        Log.e("aim","set list $image")
        this.vendorList.clear()
        this.questionId = questionId
        this.vendorList.addAll(image)
        notifyDataSetChanged()
    }

    fun getAllItem(): ArrayList<ConverterImageModel> {
        return vendorList
    }

    class VendorListViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bindView(data: ConverterImageModel) {

            try { Glide.with(itemView.context).load(data.fileUri).into(itemView.image_value) }
            catch (e: Exception) {}

            if (data.fileUri.contains("http")) {
                itemView.tx_disconnect.visibility = View.GONE
            } else {
                itemView.tx_disconnect.visibility = View.VISIBLE
            }

        }
    }

}