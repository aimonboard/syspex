package com.syspex.ui.adapter

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.syspex.R
import com.syspex.data.model.OutstandingModel
import kotlinx.android.synthetic.main.item_accessories.view.*

class OutstandingAdapter : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private var vendorList = mutableListOf<OutstandingModel>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return VendorListViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_accessories, parent, false))
    }
    override fun getItemCount(): Int = vendorList.size

    override fun onBindViewHolder(viewHolder: RecyclerView.ViewHolder, position: Int) {
        val holder = viewHolder as VendorListViewHolder
        holder.bindView(vendorList[position])

        holder.itemView.btn_close.setOnClickListener {
            vendorList.removeAt(position)
            notifyItemRemoved(position)
        }

        if (position == vendorList.size-1) {
            holder.itemView.divider.visibility = View.GONE
        } else {
            holder.itemView.divider.visibility = View.VISIBLE
        }
    }

    fun setList(listOfVendor: List<OutstandingModel>) {
        this.vendorList = listOfVendor.toMutableList()
        notifyDataSetChanged()
    }

    fun addList(data: OutstandingModel) {
        this.vendorList.add(data)
        notifyDataSetChanged()
    }

    fun getAllItem (): List<OutstandingModel> {
        return this.vendorList
    }

    class VendorListViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        @SuppressLint("SetTextI18n")
        fun bindView(vendorModel: OutstandingModel) {
            itemView.tx_product_number.text = "Things to be followed up"
            itemView.tx_desc.text = "Followed up by"
            itemView.tx_qty.text = "Target Date"

            itemView.product_value.text = vendorModel.things.ifEmpty { "-" }
            itemView.desc_value.text = vendorModel.follow.ifEmpty { "-" }
            itemView.qty_value.text = vendorModel.date.ifEmpty { "-" }
        }
    }

}