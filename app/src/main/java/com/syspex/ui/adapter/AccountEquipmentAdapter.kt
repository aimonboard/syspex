package com.syspex.ui.adapter

import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.syspex.R
import com.syspex.data.local.database.account_equipment.AccountEquipmentEntity
import kotlinx.android.synthetic.main.item_equipment_add.view.*

class AccountEquipmentAdapter : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private var vendorList = mutableListOf<AccountEquipmentEntity>()
    private var eventHandler: RecyclerClickListener? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return VendorListViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_equipment_add, parent, false))
    }
    override fun getItemCount(): Int = vendorList.size

    override fun onBindViewHolder(viewHolder: RecyclerView.ViewHolder, position: Int) {
        val holder = viewHolder as VendorListViewHolder
        holder.bindView(vendorList[position])

        holder.itemView.btn_add_remove.setOnClickListener {
            eventHandler?.attach(position, vendorList[position])
        }

    }

    fun setList(listOfVendor: List<AccountEquipmentEntity>) {
        this.vendorList = listOfVendor.toMutableList()
        notifyDataSetChanged()
    }

    fun addData(data: AccountEquipmentEntity) {
        this.vendorList.add(data)
        notifyDataSetChanged()
    }

    fun removeData(data: AccountEquipmentEntity) {
        this.vendorList.remove(data)
        notifyDataSetChanged()
    }

    fun clearData() {
        this.vendorList.clear()
        notifyDataSetChanged()
    }

    class VendorListViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bindView(data: AccountEquipmentEntity) {

            itemView.btn_add_remove.setTextColor(Color.parseColor("#1ABBB9"))
            itemView.btn_add_remove.setBackgroundResource(R.drawable.shape_frame_blue_light)
            itemView.btn_add_remove.text = "Add Equipment"

            itemView.title_value.text = data.serialNo.ifEmpty { "-" }
            itemView.status_value.text = data.statusTextEnumText.ifEmpty { "-" }
            itemView.number_value.text = data.product_name.ifEmpty { "-" }
            itemView.warranty_end_value.text = data.warrantyEndDate.ifEmpty { "-" }
            itemView.agremeent_number_value.text = data.agreementNumber.ifEmpty { "-" }
        }
    }

    fun setEventHandler(eventHandler: RecyclerClickListener) {
        try {
            this.eventHandler = eventHandler
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    interface RecyclerClickListener {
        fun attach(position: Int, data: AccountEquipmentEntity)
    }

}