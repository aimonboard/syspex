package com.syspex.ui.adapter

import android.annotation.SuppressLint
import android.content.Intent
import android.graphics.Color
import android.graphics.PorterDuff
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.FragmentActivity
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.syspex.R
import com.syspex.data.local.database.service_case.ServiceCaseEntity
import com.syspex.ui.CaseDetail
import com.syspex.ui.CasePreview
import com.syspex.ui.diffutil.CaseDiffUtil
import com.syspex.ui.helper.FunHelper
import kotlinx.android.synthetic.main.item_case.view.*


class CaseMyAdapter : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private var vendorList = mutableListOf<ServiceCaseEntity>()
    private var withPreview = false

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return VendorListViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_case, parent, false))
    }
    override fun getItemCount(): Int = vendorList.size

    override fun onBindViewHolder(viewHolder: RecyclerView.ViewHolder, position: Int) {
        val holder = viewHolder as VendorListViewHolder
        holder.bindView(vendorList[position], withPreview)

        holder.itemView.btn_down.setOnClickListener {
            CasePreview.caseId = vendorList[position].caseId
            CasePreview().show((it.context as FragmentActivity).supportFragmentManager,CasePreview().tag)
        }

        holder.itemView.setOnClickListener {
            val i = Intent(it.context,CaseDetail::class.java)
            i.putExtra("id",vendorList[position].caseId)
            i.putExtra("number",vendorList[position].caseNumber)
            it.context.startActivity(i)
        }


    }

    fun setList(listOfVendor: List<ServiceCaseEntity>, withPreview: Boolean) {
//        this.vendorList = listOfVendor.toMutableList()
//        notifyDataSetChanged()
        this.withPreview = withPreview
        val diffCallback = CaseDiffUtil(this.vendorList, listOfVendor)
        val diffResult = DiffUtil.calculateDiff(diffCallback)
        this.vendorList.clear()
        this.vendorList.addAll(listOfVendor)
        diffResult.dispatchUpdatesTo(this)
    }

    fun addData(data: ServiceCaseEntity) {
        this.vendorList.add(data)
    }

    fun clear() {
        this.vendorList.clear()
        notifyDataSetChanged()
    }

    class VendorListViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        @SuppressLint("SetTextI18n")
        fun bindView(vendorModel: ServiceCaseEntity, withPreview: Boolean) {
            itemView.number_value.text = vendorModel.caseNumber.ifEmpty { "-" }
            itemView.title_value.text = vendorModel.subject.ifEmpty { "-" }
            itemView.company_value.text = vendorModel.accountName.ifEmpty { "-" }
            itemView.status_value.text = vendorModel.caseStatusEnumName.ifEmpty { "-" }
            itemView.status2_value.text = vendorModel.caseTypeEnumName.ifEmpty { "-" }
            val onlyDate = vendorModel.createdDate.split(" ")
            itemView.date_value.text = "Created: ${FunHelper.uiDateFormat(onlyDate.first())}"

            if (!vendorModel.equipment.isNullOrEmpty()) {
                itemView.product_value.text =  "(${vendorModel.equipment.size}) ${vendorModel.equipment.first()?.equipment?.product?.productName}"
            } else {
                itemView.product_value.text = "-"
            }

            if (vendorModel.leadEngineerName.isNotEmpty()) {
                itemView.engineer_value.text = vendorModel.leadEngineerName
            } else {
                itemView.engineer_value.visibility = View.GONE
            }

            if (!vendorModel.colorBackground.isNullOrEmpty()) {
                itemView.parent_lay.setBackgroundColor(Color.parseColor(vendorModel.colorBackground))
            }

            if (!vendorModel.colorStatusLabel.isNullOrEmpty() && !vendorModel.colorStatusLabelText.isNullOrEmpty()) {
                itemView.status2_value.background.setColorFilter(Color.parseColor(vendorModel.colorStatusLabel), PorterDuff.Mode.SRC_ATOP)
                itemView.status2_value.setTextColor(Color.parseColor(vendorModel.colorStatusLabelText))
            }

            if (!withPreview) {
                itemView.btn_down.visibility = View.GONE
            }
        }
    }

}