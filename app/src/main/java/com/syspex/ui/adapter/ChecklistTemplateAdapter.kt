package com.syspex.ui.adapter

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.syspex.R
import com.syspex.data.model.ChecklistModel
import com.syspex.ui.helper.ChecklistInterface
import kotlinx.android.synthetic.main.item_checklist_template.view.*

class ChecklistTemplateAdapter(private var itemListener: ChecklistInterface): RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private var vendorList = mutableListOf<ChecklistModel>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return VendorListViewHolder(
            LayoutInflater.from(parent.context).inflate(R.layout.item_checklist_template, parent, false)
        )
    }
    override fun getItemCount(): Int = vendorList.size

    override fun onBindViewHolder(viewHolder: RecyclerView.ViewHolder, position: Int) {
        val holder = viewHolder as VendorListViewHolder
        holder.bindView(vendorList[position], itemListener)

        holder.itemView.setOnClickListener {
            if (holder.itemView.expand_lay.visibility == View.VISIBLE) {
                holder.itemView.btn_down.rotation = 0F
                holder.itemView.expand_lay.visibility = View.GONE
            } else {
                holder.itemView.btn_down.rotation = 180F
                holder.itemView.expand_lay.visibility = View.VISIBLE
            }
        }
    }

    fun setList(listOfVendor: List<ChecklistModel>) {
        this.vendorList = listOfVendor.toMutableList()
        notifyDataSetChanged()
    }

    fun addList(listOfVendor: ChecklistModel) {
        this.vendorList.add(listOfVendor)
        notifyDataSetChanged()
    }

    fun reset() {
        this.vendorList.clear()
    }

    class VendorListViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        @SuppressLint("SetTextI18n")
        fun bindView(vendorModel: ChecklistModel, itemListener: ChecklistInterface) {

            itemView.tx_name.text = vendorModel.template_description
            itemView.tx_total.text = "${vendorModel.question.size} checklist included"

            itemView.recycler_checklist_question.layoutManager = LinearLayoutManager(itemView.context, RecyclerView.VERTICAL, false)
            val adapter = ChecklistQuestionAdapter(itemListener)
            itemView.recycler_checklist_question.adapter = adapter
            adapter.setList(vendorModel.question, vendorModel.enumData)
        }
    }
}