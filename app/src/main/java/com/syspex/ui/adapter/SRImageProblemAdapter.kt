package com.syspex.ui.adapter
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.syspex.R
import com.syspex.data.model.ConverterImageModel
import kotlinx.android.synthetic.main.item_image.view.*
import kotlin.Exception

class SRImageProblemAdapter(private val isEdit: Boolean) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private var vendorList = ArrayList<ConverterImageModel>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return VendorListViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_image, parent, false))
    }
    override fun getItemCount(): Int = vendorList.size

    override fun onBindViewHolder(viewHolder: RecyclerView.ViewHolder, position: Int) {
        val holder = viewHolder as VendorListViewHolder
        holder.bindView(vendorList[position])

        holder.itemView.btn_delete.setOnClickListener {
            vendorList.remove(vendorList[position])
            notifyDataSetChanged()
        }

        if (isEdit) {
            holder.itemView.btn_delete.visibility = View.VISIBLE
        } else {
            holder.itemView.btn_delete.visibility = View.GONE
        }

    }

    fun setList(listOfVendor: ArrayList<ConverterImageModel>) {
        this.vendorList = listOfVendor
        notifyDataSetChanged()
    }

    fun addData(data: ConverterImageModel) {
        if (!this.vendorList.contains(data)) {
            this.vendorList.add(data)
            notifyDataSetChanged()
        }
    }

    fun getAllItem(): ArrayList<ConverterImageModel> {
        return vendorList
    }

    fun clear() {
        vendorList.clear()
    }

    class VendorListViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bindView(data: ConverterImageModel) {

            try {
                Glide.with(itemView.context)
                    .load(data.fileUri)
                    .placeholder(R.drawable.thumbnail)
                    .into(itemView.image_value)
            }
            catch (e: Exception) {}

            if (data.fileUri.contains("http")) {
                itemView.tx_disconnect.visibility = View.GONE
            } else {
                itemView.tx_disconnect.visibility = View.VISIBLE
            }

        }
    }

}