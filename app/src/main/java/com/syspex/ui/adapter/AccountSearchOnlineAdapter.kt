package com.syspex.ui.adapter

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.syspex.R
import com.syspex.data.remote.SearchAccountOnlineRequest
import kotlinx.android.synthetic.main.item_account_online.view.*


class AccountSearchOnlineAdapter : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private var eventHandler: RecyclerClickListener? = null
    private var vendorList = mutableListOf<SearchAccountOnlineRequest.Data.Account>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return VendorListViewHolder(
            LayoutInflater.from(parent.context).inflate(R.layout.item_account_online, parent, false)
        )
    }

    override fun getItemCount(): Int = vendorList.size

    override fun onBindViewHolder(viewHolder: RecyclerView.ViewHolder, position: Int) {
        val holder = viewHolder as VendorListViewHolder
        holder.bindView(vendorList[position])

        holder.itemView.btn_download.setOnClickListener {
            eventHandler?.isClicked(
                vendorList[position].accountId ?: "",
                vendorList[position].accountName ?: ""
            )
        }

    }

    fun setList(listOfVendor: List<SearchAccountOnlineRequest.Data.Account>) {
        this.vendorList = listOfVendor.toMutableList()
        notifyDataSetChanged()
    }

    fun addData(data: SearchAccountOnlineRequest.Data.Account) {
        this.vendorList.add(data)
    }

    class VendorListViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        @SuppressLint("SetTextI18n")
        fun bindView(vendorModel: SearchAccountOnlineRequest.Data.Account) {
            itemView.title_value.text = vendorModel.accountName ?: ""
            if (!vendorModel.address.isNullOrEmpty()) {
                val address = vendorModel.address.sortedByDescending { it?.priority }
                itemView.location_value.text = address.first()?.fullAddress
            } else {
                itemView.location_value.text = "-"
            }
        }
    }

    fun setEventHandler(eventHandler: RecyclerClickListener) {
        try {
            this.eventHandler = eventHandler
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    interface RecyclerClickListener {
        fun isClicked(accountId: String, accountName: String)
    }
}