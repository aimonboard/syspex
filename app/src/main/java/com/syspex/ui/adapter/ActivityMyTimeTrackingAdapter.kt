package com.syspex.ui.adapter

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.syspex.R
import com.syspex.data.local.database.service_call_equipment.ServiceCallEquipmentEntity
import com.syspex.data.local.database.service_report_time_tracking.ServiceReportTimeTrackingEntity
import com.syspex.ui.diffutil.ActivityDiffUtil
import com.syspex.ui.diffutil.TimeTrackingDiffUtil
import com.syspex.ui.helper.FunHelper
import kotlinx.android.synthetic.main.item_time_tracking.view.*
import kotlinx.android.synthetic.main.item_time_tracking.view.date_value
import kotlinx.android.synthetic.main.item_time_tracking.view.divider

class ActivityMyTimeTrackingAdapter : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private var vendorList = mutableListOf<ServiceReportTimeTrackingEntity>()
    private var eventHandler: RecyclerClickListener? = null

    private var engineerId = ""
    private var callNumber = ""
    private var withEdit = false

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return VendorListViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_time_tracking, parent, false))
    }
    override fun getItemCount(): Int = vendorList.size

    override fun onBindViewHolder(viewHolder: RecyclerView.ViewHolder, position: Int) {
        val holder = viewHolder as VendorListViewHolder
        holder.bindView(vendorList[position], callNumber)

        holder.itemView.btn_edit.setOnClickListener {
            val startEnd = holder.itemView.time_value.text.toString().split(" - ")

            eventHandler?.isEdit(
                vendorList[position].timeTrackingId,
                holder.itemView.date_value.text.toString(),
                startEnd[0],
                startEnd[1]
            )
        }

        holder.itemView.btn_delete.setOnClickListener {
            eventHandler?.isDelete(
                vendorList[position].timeTrackingId
            )
        }

        if (withEdit) {
            if (vendorList[position].engineerId == engineerId) {
                holder.itemView.btn_edit.visibility = View.VISIBLE
                holder.itemView.btn_delete.visibility = View.VISIBLE
            } else {
                holder.itemView.btn_edit.visibility = View.GONE
                holder.itemView.btn_delete.visibility = View.GONE
            }
        } else {
            holder.itemView.btn_edit.visibility = View.GONE
            holder.itemView.btn_delete.visibility = View.GONE
        }


        if (position+1 == vendorList.size) {
            holder.itemView.divider.visibility = View.GONE
        } else {
            holder.itemView.divider.visibility = View.VISIBLE
        }

    }

    fun setList(listOfVendor: List<ServiceReportTimeTrackingEntity>, engineerId: String, callNumber: String, withEdit: Boolean) {
        this.engineerId = engineerId
        this.callNumber = callNumber
        this.withEdit = withEdit
        this.vendorList = listOfVendor.toMutableList()
        notifyDataSetChanged()

//        val diffCallback = TimeTrackingDiffUtil(this.vendorList, listOfVendor)
//        val diffResult = DiffUtil.calculateDiff(diffCallback)
//        diffResult.dispatchUpdatesTo(this)
    }

    fun addData(data: ServiceReportTimeTrackingEntity, engineerId: String, callNumber: String, withEdit: Boolean) {
        this.vendorList.add(data)
        this.engineerId = engineerId
        this.callNumber = callNumber
        this.withEdit = withEdit
        notifyDataSetChanged()
    }

    fun clear() {
        this.vendorList.clear()
        notifyDataSetChanged()
    }

    class VendorListViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        @SuppressLint("SetTextI18n")
        fun bindView(data: ServiceReportTimeTrackingEntity, callNumber: String) {

            itemView.engineer_value.text = data.engineerName ?: "-"
            itemView.call_number_value.text = callNumber.ifEmpty { "-" }

            try {
                val startDate = data.start.split(" ")
                val endDate = data.end.split(" ")
                val timeStart = startDate[1].split(":")
                val timeEnd = endDate[1].split(":")
                itemView.date_value.text = FunHelper.uiDateFormat(startDate[0])
                itemView.time_value.text = "${timeStart[0]}:${timeStart[1]} - ${timeEnd[0]}:${timeEnd[1]}"
            } catch (e: Exception) {
                itemView.date_value.text = "-"
                itemView.time_value.text = "-"
            }

        }
    }

    fun setEventHandler(eventHandler: RecyclerClickListener) {
        try {
            this.eventHandler = eventHandler
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    interface RecyclerClickListener {
        fun isEdit(id: String, date: String, start: String, end: String)
        fun isDelete(id: String)
    }

}