package com.syspex.ui.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.syspex.R
import com.syspex.data.local.database.product_checklist_template.ChecklistTemplateEntity
import kotlinx.android.synthetic.main.item_checklist_master.view.*

class ChecklistMasterAdapter : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private var vendorList = mutableListOf<ChecklistTemplateEntity>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return VendorListViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_checklist_master, parent, false))
    }
    override fun getItemCount(): Int = vendorList.size

    override fun onBindViewHolder(viewHolder: RecyclerView.ViewHolder, position: Int) {
        val holder = viewHolder as VendorListViewHolder
        holder.bindView(vendorList[position])

        if (position+1 == itemCount) {
            holder.itemView.divider.visibility = View.GONE
        } else {
            holder.itemView.divider.visibility = View.VISIBLE
        }

    }

    fun setList(listOfVendor: List<ChecklistTemplateEntity>) {
        this.vendorList = listOfVendor.toMutableList()
        notifyDataSetChanged()
    }

    fun addData(data: ChecklistTemplateEntity) {
        this.vendorList.add(data)
        notifyDataSetChanged()
    }

    fun clear() {
        this.vendorList.clear()
        notifyDataSetChanged()
    }

    class VendorListViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bindView(data: ChecklistTemplateEntity) {
            itemView.desc_value.text = data.template_description.ifEmpty { "-" }
            itemView.type_value.text = data.checklistTypeTextEnumName.ifEmpty { "-" }
        }
    }

}