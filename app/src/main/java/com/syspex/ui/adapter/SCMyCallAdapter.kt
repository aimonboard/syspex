package com.syspex.ui.adapter

import android.annotation.SuppressLint
import android.content.Intent
import android.graphics.Color
import android.graphics.PorterDuff
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.FragmentActivity
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.syspex.R
import com.syspex.data.local.database.service_call.ServiceCallEntity
import com.syspex.ui.ServiceCallDetail
import com.syspex.ui.ServiceCallPreview
import com.syspex.ui.diffutil.CallDiffUtil
import com.syspex.ui.helper.FunHelper
import kotlinx.android.synthetic.main.item_service_call.view.*

class SCMyCallAdapter : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private var vendorList = mutableListOf<ServiceCallEntity>()
    private var withPreview = false

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return VendorListViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_service_call, parent, false))
    }
    override fun getItemCount(): Int = vendorList.size

    override fun onBindViewHolder(viewHolder: RecyclerView.ViewHolder, position: Int) {
        val holder = viewHolder as VendorListViewHolder
        holder.bindView(vendorList[position], withPreview)

        holder.itemView.btn_down.setOnClickListener {
            ServiceCallPreview.callId = vendorList[position].serviceCallId
            ServiceCallPreview().show((it.context as FragmentActivity).supportFragmentManager,ServiceCallPreview().tag)
        }

        holder.itemView.setOnClickListener {
            val i = Intent(it.context,ServiceCallDetail::class.java)
            i.putExtra("id",vendorList[position].serviceCallId)
            i.putExtra("number",vendorList[position].serviceCallNumber)
            it.context.startActivity(i)
        }
    }

    fun setList(listOfVendor: List<ServiceCallEntity>, withPreview: Boolean) {
        this.withPreview = withPreview
        this.vendorList = listOfVendor.toMutableList()
        notifyDataSetChanged()

//        val diffCallback = CallDiffUtil(this.vendorList, listOfVendor)
//        val diffResult = DiffUtil.calculateDiff(diffCallback)
//        this.vendorList.clear()
//        this.vendorList.addAll(listOfVendor)
//        diffResult.dispatchUpdatesTo(this)
    }

    fun addData(data: ServiceCallEntity) {
        this.vendorList.add(data)
    }

    fun clear() {
        this.vendorList.clear()
        notifyDataSetChanged()
    }

    class VendorListViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        @SuppressLint("SetTextI18n")
        fun bindView(vendorModel: ServiceCallEntity, withPreview: Boolean) {
            itemView.service_call_number.text = vendorModel.serviceCallNumber.ifEmpty { "-" }
            itemView.title_value.text = vendorModel.serviceCallSubject.ifEmpty { "-" }
            itemView.company_value.text = vendorModel.accountName.ifEmpty { "-" }
            itemView.created_value.text = FunHelper.uiDateFormat(vendorModel.startDate).ifEmpty { "-" }
            itemView.status_value.text = vendorModel.callStatusEnumText.ifEmpty { "-" }
            itemView.status2_value.text = vendorModel.callTypeEnumText.ifEmpty { "-" }

            if (!vendorModel.equipment.isNullOrEmpty()) {
                itemView.product_value.text =  "(${vendorModel.equipment.size}) ${vendorModel.equipment.first()?.equipment?.product?.productName}"
            } else {
                itemView.product_value.text = "-"
            }

            if (!vendorModel.engineer.isNullOrEmpty()) {
                when {
                    vendorModel.engineer.size == 1 -> {
                        itemView.engineer.text = vendorModel.leadEngineerName
                    }
                    vendorModel.engineer.size > 1 -> {
                        itemView.engineer.text = "${vendorModel.leadEngineerName} (+${vendorModel.engineer.size - 1})"
                    }
                }
            } else {
                itemView.engineer.visibility = View.GONE
            }

            if (!vendorModel.colorBackground.isNullOrEmpty()) {
                itemView.parent_lay.setBackgroundColor(Color.parseColor(vendorModel.colorBackground))
            }

            if (!vendorModel.colorStatusLabel.isNullOrEmpty() && !vendorModel.colorStatusLabelText.isNullOrEmpty()) {
                itemView.status2_value.background.setColorFilter(Color.parseColor(vendorModel.colorStatusLabel), PorterDuff.Mode.SRC_ATOP)
                itemView.status2_value.setTextColor(Color.parseColor(vendorModel.colorStatusLabelText))
            }

            if (!withPreview) {
                itemView.btn_down.visibility = View.GONE
            }

        }
    }

}