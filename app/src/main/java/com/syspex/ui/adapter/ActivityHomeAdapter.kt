package com.syspex.ui.adapter

import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.syspex.R
import com.syspex.data.local.database.activity.ActivityEntity
import com.syspex.ui.ActivityAdd
import com.syspex.ui.diffutil.ActivityDiffUtil
import com.syspex.ui.diffutil.CaseDiffUtil
import kotlinx.android.synthetic.main.item_activity_home.view.*
import java.text.SimpleDateFormat
import java.util.*


class ActivityHomeAdapter : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private var vendorList = mutableListOf<ActivityEntity>()
    private var selectedDate = Date()
    private var engineerName = ""

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return VendorListViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_activity_home, parent, false))
    }
    override fun getItemCount(): Int = vendorList.size

    override fun onBindViewHolder(viewHolder: RecyclerView.ViewHolder, position: Int) {
        val holder = viewHolder as VendorListViewHolder
        holder.bindView(selectedDate, engineerName, vendorList[position])

        holder.itemView.btn_edit.setOnClickListener {
            val i = Intent(it.context, ActivityAdd::class.java)
            i.putExtra("id", vendorList[position].activityId)
            it.context.startActivity(i)
        }

    }

    fun setList(mDate: Date, name: String, listOfVendor: List<ActivityEntity>) {
        this.selectedDate = mDate
        this.engineerName = name
        this.vendorList = listOfVendor.toMutableList()
        notifyDataSetChanged()
    }

    fun addData(mDate: Date, name: String, data: ActivityEntity) {
        this.selectedDate = mDate
        this.engineerName = name
        this.vendorList.add(data)
        notifyDataSetChanged()
    }

    fun clear() {
        this.vendorList.clear()
        notifyDataSetChanged()
    }

    class VendorListViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        private val uiFormat = SimpleDateFormat("dd MMM yyyy", Locale.getDefault())

        fun bindView(selectedDate: Date, name: String, vendorModel: ActivityEntity) {

            itemView.tx_activity.text = vendorModel.activitySubject.ifEmpty { "-" }
            itemView.activity_date_value.text = uiFormat.format(selectedDate)

            val startTime = vendorModel.activityStartTime.split(" ")
            val endTime = vendorModel.activityEndTime.split(" ")
            if (startTime.size == 2 && endTime.size == 2 ) {
                val finalTime = "${startTime[1].substring(0,5)} - ${endTime[1].substring(0,5)}"
                itemView.activity_time_value.text = finalTime
            } else {
                itemView.activity_time_value.text = "-"
            }

            itemView.activity_type_value.text = vendorModel.activityTypeName.ifEmpty { "-" }
            itemView.activity_name_value.text = name.ifEmpty { "-" }

        }
    }

}