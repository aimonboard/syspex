package com.syspex.ui.adapter

import android.annotation.SuppressLint
import android.content.Intent
import android.util.Log
import android.util.SparseBooleanArray
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.syspex.R
import com.syspex.data.model.ConverterImageModel
import com.syspex.data.model.SREquipmentModel
import com.syspex.ui.ChecklistConfirm
import com.syspex.ui.EquipmentEdit
import com.syspex.ui.ProductDocument
import com.syspex.ui.SparePartAdd
import com.syspex.ui.helper.FunHelper
import kotlinx.android.synthetic.main.item_service_report.view.btn_down
import kotlinx.android.synthetic.main.item_service_report_equipment_detail.view.*

class ServiceReportEquipmentDetailAdapter : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private var vendorList = mutableListOf<SREquipmentModel>()
    private var reportComplete = false
    private var hasChecklist = false
    private val itemStateArray = SparseBooleanArray()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return VendorListViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_service_report_equipment_detail, parent, false))
    }
    override fun getItemCount(): Int = vendorList.size

    override fun onBindViewHolder(viewHolder: RecyclerView.ViewHolder, position: Int) {
        val holder = viewHolder as VendorListViewHolder
        holder.bindView(vendorList[position], reportComplete, hasChecklist)

        if (!reportComplete) {
            holder.itemView.setOnClickListener {
                holder.itemView.btn_edit_equipment.performClick()
            }
        }

        holder.itemView.btn_down.setOnClickListener {
            if (holder.itemView.expand_lay.visibility == View.GONE) {
                holder.itemView.btn_down.rotation = 180F
                holder.itemView.preview_bar.visibility = View.GONE
                holder.itemView.expand_lay.visibility = View.VISIBLE
                itemStateArray.put(position,true)
            } else {
                holder.itemView.btn_down.rotation = 0F
                holder.itemView.preview_bar.visibility = View.VISIBLE
                holder.itemView.expand_lay.visibility = View.GONE
                itemStateArray.put(position,false)
            }
        }

        holder.itemView.btn_checklist.setOnClickListener {
            val i = Intent(it.context, ChecklistConfirm::class.java)
            i.putExtra("reportId", vendorList[position].reportId)
            i.putExtra("reportEquipmentId", vendorList[position].serviceReportEquipmentId)
            i.putExtra("equipmentId", vendorList[position].equipmentId)
            i.putExtra("productId", vendorList[position].productId)
            it.context.startActivity(i)
        }

        holder.itemView.btn_edit_equipment.setOnClickListener {
            val i = Intent(it.context, EquipmentEdit::class.java)
            i.putExtra("reportId", vendorList[position].reportId)
            i.putExtra("reportEquipmentId", vendorList[position].serviceReportEquipmentId)
            i.putExtra("equipmentId", vendorList[position].equipmentId)
            i.putExtra("productId", vendorList[position].productId)
            i.putExtra("checklistTemplateId", vendorList[position].checklistTemplateId)
            i.putExtra("hasChecklist", hasChecklist)
            it.context.startActivity(i)
        }

        holder.itemView.btn_edit_sparepart.setOnClickListener {
            val i = Intent(it.context, SparePartAdd::class.java)
            i.putExtra("reportId", vendorList[position].reportId)
            i.putExtra("reportEquipmentId", vendorList[position].serviceReportEquipmentId)
            i.putExtra("productId", vendorList[position].productId)
            it.context.startActivity(i)
        }

        holder.itemView.btn_pdf.setOnClickListener {
            val i = Intent(it.context, ProductDocument::class.java)
            i.putExtra("productId", vendorList[position].productId)
            it.context.startActivity(i)

            Log.e("aim", "product id : ${vendorList[position].productId}")
        }

        if (itemStateArray.get(position)) {
            holder.itemView.btn_down.rotation = 180F
            holder.itemView.preview_bar.visibility = View.GONE
            holder.itemView.expand_lay.visibility = View.VISIBLE
        } else {
            holder.itemView.btn_down.rotation = 0F
            holder.itemView.preview_bar.visibility = View.VISIBLE
            holder.itemView.expand_lay.visibility = View.GONE
        }
    }

    fun setList(listOfVendor: List<SREquipmentModel>, reportComplete: Boolean, hasChecklist: Boolean) {
        this.vendorList = listOfVendor.toMutableList()
        this.reportComplete = reportComplete
        this.hasChecklist = hasChecklist
        notifyDataSetChanged()
    }

    fun addData(data: SREquipmentModel, reportComplete: Boolean, hasChecklist: Boolean) {
        this.vendorList.add(data)
        this.reportComplete = reportComplete
        this.hasChecklist = hasChecklist
        notifyDataSetChanged()
    }

    fun reset() {
        this.vendorList.clear()
        notifyDataSetChanged()
    }

    fun getAllItem(): MutableList<SREquipmentModel> {
        return vendorList
    }

    fun allExpand() {
        for (i in 0 until itemCount) {
            itemStateArray.put(i, true)
        }
        notifyDataSetChanged()
    }

    fun allCollapse() {
        for (i in 0 until itemCount) {
            itemStateArray.put(i, false)
        }
        notifyDataSetChanged()
    }

    class VendorListViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        @SuppressLint("SetTextI18n")
        fun bindView(vendorModel: SREquipmentModel, reportComplete: Boolean, hasChecklist: Boolean) {

            // ===================================================================================== Report complete or cancel
            if (reportComplete) {
                itemView.btn_checklist.visibility = View.GONE
                itemView.btn_edit_sparepart.visibility = View.GONE
                itemView.btn_edit_equipment.visibility = View.GONE
            } else {
                itemView.btn_checklist.visibility = View.VISIBLE
                itemView.btn_edit_sparepart.visibility = View.VISIBLE
                itemView.btn_edit_equipment.visibility = View.VISIBLE
            }

            // ===================================================================================== Checklist
            if (hasChecklist) {
                itemView.tx_checklist.visibility = View.VISIBLE
                itemView.checklist_bar.visibility = View.VISIBLE
                itemView.header_checklist.visibility = View.VISIBLE
                itemView.checklist_lay.visibility = View.VISIBLE
            } else {
                itemView.tx_checklist.visibility = View.GONE
                itemView.checklist_bar.visibility = View.GONE
                itemView.header_checklist.visibility = View.GONE
                itemView.checklist_lay.visibility = View.GONE
            }

            itemView.status2_value.text = vendorModel.statusTextEnumText.ifEmpty { "-" }
            itemView.serial_number_value.text = "SN: ${vendorModel.serialNo.ifEmpty { "-" }}"
            itemView.desc_value.text = vendorModel.productName.ifEmpty { "-" }
            itemView.status_after_value.text = vendorModel.statusAfterServiceEnumName.ifEmpty { "-" }
            itemView.warranty_end_value1.text = FunHelper.uiDateFormat(vendorModel.warrantyEndDate)
            itemView.status_remark_value.text = vendorModel.statusAfterServiceRemarks.ifEmpty { "-" }
            itemView.agreement_end_value.text = FunHelper.uiDateFormat(vendorModel.agreementEnd)
            itemView.agreement_number_value.text = vendorModel.agreementNumber.ifEmpty { "-" }
            itemView.symptom_value.text = vendorModel.problemSymptom.ifEmpty { "-" }


            if (vendorModel.problemSymptom.isEmpty()) {
                itemView.problem_bar.setImageResource(R.drawable.ic_false)
            } else {
                itemView.problem_bar.setImageResource(R.drawable.ic_true)
            }


            if (vendorModel.problemImage.isNotEmpty()) {
                itemView.recycler_problem_image.layoutManager = LinearLayoutManager(itemView.context, RecyclerView.HORIZONTAL, false)
                val adapterSymptom = SRImageProblemAdapter(false)
                itemView.recycler_problem_image.adapter = adapterSymptom

                vendorModel.problemImage.forEach { problemData->
                    adapterSymptom.clear()
                    problemData.problemImage?.forEach {
                        adapterSymptom.addData(it)
                    }
                }
            }

            if (vendorModel.solution.isNotEmpty()) {
                itemView.cause_bar.setImageResource(R.drawable.ic_true)
                itemView.solution_bar.setImageResource(R.drawable.ic_true)

                itemView.recycler_cause_solution.layoutManager = LinearLayoutManager(itemView.context, RecyclerView.VERTICAL, false)
                val adapterSubject = SRCauseSolutionAdapter()
                itemView.recycler_cause_solution.adapter = adapterSubject

                adapterSubject.clear()
                vendorModel.solution.forEach {
                    adapterSubject.addData(it, vendorModel.equipmentId, false)
                }
            } else {
                itemView.cause_bar.setImageResource(R.drawable.ic_false)
                itemView.solution_bar.setImageResource(R.drawable.ic_false)
            }

            if (vendorModel.sparepart.isNotEmpty()) {
                itemView.recycler_sparepart.visibility = View.VISIBLE
                itemView.warning_sparepart.visibility = View.GONE

                itemView.recycler_sparepart.layoutManager = LinearLayoutManager(itemView.context, RecyclerView.VERTICAL, false)
                val adapterSparePart = SRSparePartAdapter()
                itemView.recycler_sparepart.adapter = adapterSparePart

                adapterSparePart.clear()
                adapterSparePart.setList(vendorModel.sparepart)
            } else {
                itemView.recycler_sparepart.visibility = View.GONE
                itemView.warning_sparepart.visibility = View.VISIBLE
            }

            val test = vendorModel.questionReport.filter { it.checklist_status_id.isNotEmpty() }
            itemView.checklist_value_text.text = "${test.size}/${vendorModel.questionReport.size} Checklist has been checked."
            itemView.checklist_bar.text = "${test.size}/${vendorModel.questionReport.size}"
        }
    }

}