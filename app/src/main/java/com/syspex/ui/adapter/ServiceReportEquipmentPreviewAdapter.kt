package com.syspex.ui.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.syspex.R
import com.syspex.data.model.SREquipmentModel
import com.syspex.ui.helper.FunHelper
import kotlinx.android.synthetic.main.item_service_report_equipment_preview.view.*

class ServiceReportEquipmentPreviewAdapter : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private var vendorList = mutableListOf<SREquipmentModel>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return VendorListViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_service_report_equipment_preview, parent, false))
    }
    override fun getItemCount(): Int = vendorList.size

    override fun onBindViewHolder(viewHolder: RecyclerView.ViewHolder, position: Int) {
        val holder = viewHolder as VendorListViewHolder
        holder.bindView(vendorList[position])

        holder.itemView.setOnClickListener {

        }
    }

    fun setList(listOfVendor: List<SREquipmentModel>) {
        this.vendorList = listOfVendor.toMutableList()
        notifyDataSetChanged()
    }

    fun addData(data: SREquipmentModel) {
        this.vendorList.add(data)
        notifyDataSetChanged()
    }

    fun reset() {
        this.vendorList.clear()
        notifyDataSetChanged()
    }


    class VendorListViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bindView(vendorModel: SREquipmentModel) {
            itemView.status2_value.text = vendorModel.statusTextEnumText.ifEmpty { "-" }
            itemView.serial_number_value.text = vendorModel.serialNo.ifEmpty { "-" }
            itemView.desc_value.text = vendorModel.equipmentName.ifEmpty { "-" }
            itemView.status_after_value.text = vendorModel.statusAfterServiceEnumName.ifEmpty { "-" }
            itemView.warranty_end_value1.text = FunHelper.uiDateFormat(vendorModel.warrantyEndDate)
            itemView.status_remark_value.text = vendorModel.statusAfterServiceRemarks.ifEmpty { "-" }
            itemView.agreement_end_value.text = FunHelper.uiDateFormat(vendorModel.agreementEnd)
            itemView.agreement_number_value.text = vendorModel.agreementNumber.ifEmpty { "-" }


            if (vendorModel.problemSymptom.isEmpty()) {
                itemView.problem_bar.setImageResource(R.drawable.ic_false)
            } else {
                itemView.problem_bar.setImageResource(R.drawable.ic_true)
            }

            if (vendorModel.solution.isNotEmpty()) {
                itemView.cause_bar.setImageResource(R.drawable.ic_true)
                itemView.solution_bar.setImageResource(R.drawable.ic_true)
            } else {
                itemView.cause_bar.setImageResource(R.drawable.ic_false)
                itemView.solution_bar.setImageResource(R.drawable.ic_false)
            }

            val test = vendorModel.questionReport.filter { it.checklist_status_id.isNotEmpty() }
            itemView.checklist_bar.text = "${test.size}/${vendorModel.questionMaster.size}"
        }
    }

}