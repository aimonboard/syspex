package com.syspex.ui.adapter

import android.content.Intent
import android.graphics.Color
import android.graphics.PorterDuff
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView
import com.syspex.R
import com.syspex.data.local.database.service_report.ServiceReportEntity
import com.syspex.ui.ServiceReportDetail
import com.syspex.ui.helper.FunHelper
import kotlinx.android.synthetic.main.item_service_report_preview.view.*

class ServiceReportPreviewAdapter : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private var reportData = mutableListOf<ServiceReportEntity>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return VendorListViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_service_report_preview, parent, false))
    }
    override fun getItemCount(): Int = reportData.size

    override fun onBindViewHolder(viewHolder: RecyclerView.ViewHolder, position: Int) {
        val holder = viewHolder as VendorListViewHolder
        holder.bindView(reportData[position])

        holder.itemView.setOnClickListener {
            val i = Intent(it.context,ServiceReportDetail::class.java)
            i.putExtra("id", reportData[position].reportLocalId)
            it.context.startActivity(i)
        }

        holder.itemView.btn_call.setOnClickListener {
            val data = holder.itemView.account_value.text.toString().split(" - ")
            if (data.size > 1) {
                FunHelper.phoneCall(it.context, data[1])
            } else {
                Toast.makeText(it.context,"Data not available", Toast.LENGTH_LONG).show()
            }
        }

        holder.itemView.btn_edit.setOnClickListener {
            Toast.makeText(it.context, "Edit", Toast.LENGTH_LONG).show()
        }
    }

    fun setList(reportData: List<ServiceReportEntity>) {
        this.reportData = reportData.toMutableList()
        notifyDataSetChanged()
    }

    fun clear() {
        this.reportData.clear()
    }

    class VendorListViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bindView(reportData: ServiceReportEntity) {
            val picName = reportData.fieldAccountContactName.ifEmpty { "-" }
            val picPhone = reportData.fieldAccountContactPhone.ifEmpty { "-" }
            itemView.account_value.text = "$picName - ($picPhone)"

            // Report
            itemView.service_id.text = reportData.serviceReportNumber.ifEmpty { "-" }
            itemView.status_value.text = reportData.reportStatusEnumText.ifEmpty { "-" }
            itemView.service_by_value.text = reportData.servicedByUserName.ifEmpty { "-" }
            itemView.equipment_value.text = reportData.equipment?.size.toString() ?: "-"
            itemView.created_by_value.text = reportData.createdByUserName.ifEmpty { "-" }
            itemView.created_date_value.text = FunHelper.uiDateFormat(reportData.createdDate)

            if (!reportData.colorBackground.isNullOrEmpty()) {
                itemView.parent_lay.setBackgroundColor(Color.parseColor(reportData.colorBackground))
            }

            if (!reportData.colorStatusLabel.isNullOrEmpty() && !reportData.colorStatusLabelText.isNullOrEmpty()) {
                itemView.status_value.background.setColorFilter(Color.parseColor(reportData.colorStatusLabel), PorterDuff.Mode.SRC_ATOP)
                itemView.status_value.setTextColor(Color.parseColor(reportData.colorStatusLabelText))
            }

        }
    }

}