package com.syspex.ui.adapter

import android.annotation.SuppressLint
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.syspex.R
import com.syspex.data.local.database.agreement.AgreementEntity
import com.syspex.ui.AgreementDetail
import com.syspex.ui.diffutil.AgreementDiffUtil
import com.syspex.ui.helper.FunHelper
import kotlinx.android.synthetic.main.item_agreement.view.*

class AgreementAdapter : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private var vendorList = mutableListOf<AgreementEntity>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return VendorListViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_agreement, parent, false))
    }
    override fun getItemCount(): Int = vendorList.size

    override fun onBindViewHolder(viewHolder: RecyclerView.ViewHolder, position: Int) {
        val holder = viewHolder as VendorListViewHolder
        holder.bindView(vendorList[position])

        holder.itemView.setOnClickListener {
            val i = Intent(it.context, AgreementDetail::class.java)
            i.putExtra("id", vendorList[position].agreementId)
            i.putExtra("number", vendorList[position].agreementNumber)
            it.context.startActivity(i)
        }

    }

    fun setList(listOfVendor: List<AgreementEntity>) {
//        this.vendorList = listOfVendor.toMutableList()
//        notifyDataSetChanged()

        val diffCallback = AgreementDiffUtil(this.vendorList, listOfVendor)
        val diffResult = DiffUtil.calculateDiff(diffCallback)
        this.vendorList.clear()
        this.vendorList.addAll(listOfVendor)
        diffResult.dispatchUpdatesTo(this)
    }

    fun addList(data: AgreementEntity) {
        this.vendorList.add(data)
        notifyDataSetChanged()
    }

    fun clear() {
        this.vendorList.clear()
    }

    class VendorListViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        @SuppressLint("SetTextI18n")
        fun bindView(vendorModel: AgreementEntity) {
            itemView.agreement_status_value.text = vendorModel.agreementStatusEnumText.ifEmpty { "-" }
            itemView.agreement_type_value.text = vendorModel.agreementTypeEnumName.ifEmpty { "-" }
            itemView.agreement_number_value.text = vendorModel.agreementNumber.ifEmpty { "-" }

            if (!vendorModel.equipment.isNullOrEmpty()) {
                itemView.agreement_equipment_value.text =  "(${vendorModel.equipment.size}) ${vendorModel.equipment.first()?.product?.productName}"
            } else {
                itemView.agreement_type_value.text = "-"
            }

            val start = FunHelper.uiDateFormat(vendorModel.agreementStartDate)
            val end = FunHelper.uiDateFormat(vendorModel.agreementEndDate)
            itemView.agreement_period_value.text = "$start - $end"


            itemView.agreement_service_value.text = vendorModel.noOfService.ifEmpty { "-" }
            itemView.agreement_contact_value.text = vendorModel.installationContactPicName.ifEmpty { "-" }
            itemView.agreement_created_value.text = vendorModel.createdBy.ifEmpty { "-" }

        }
    }

}