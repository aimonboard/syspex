package com.syspex.ui.adapter

import android.content.Intent
import android.net.Uri
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat.startActivity
import androidx.recyclerview.widget.RecyclerView
import com.syspex.R
import com.syspex.data.local.database.product_document.ProductDocumentEntity
import kotlinx.android.synthetic.main.item_technical_document.view.*


class TechnicalDocumentAdapter : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private var eventHandler: RecyclerClickListener? = null
    private var vendorList = mutableListOf<ProductDocumentEntity>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return VendorListViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_technical_document, parent, false))
    }
    override fun getItemCount(): Int = vendorList.size

    override fun onBindViewHolder(viewHolder: RecyclerView.ViewHolder, position: Int) {
        val holder = viewHolder as VendorListViewHolder
        holder.bindView(vendorList[position])

        holder.itemView.setOnClickListener {
            eventHandler?.isClicked(vendorList[position].url)

//            val browserIntent = Intent(Intent.ACTION_VIEW, Uri.parse(vendorList[position].url))
//            it.context.startActivity(browserIntent)
        }

        if (position == vendorList.lastIndex) {
            holder.itemView.divider.visibility = View.GONE
        } else {
            holder.itemView.divider.visibility = View.VISIBLE
        }

    }

    fun setList(listOfVendor: List<ProductDocumentEntity>) {
        this.vendorList = listOfVendor.toMutableList()
        notifyDataSetChanged()
    }

    fun addList(data: ProductDocumentEntity) {
        this.vendorList.add(data)
        notifyDataSetChanged()
    }

    fun clear() {
        this.vendorList.clear()
    }

    class VendorListViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bindView(vendorModel: ProductDocumentEntity) {

            itemView.attachment_title.text = vendorModel.title

        }
    }

    fun setEventHandler(eventHandler: RecyclerClickListener) {
        try {
            this.eventHandler = eventHandler
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    interface RecyclerClickListener {
        fun isClicked(fileUrl: String)
    }

}