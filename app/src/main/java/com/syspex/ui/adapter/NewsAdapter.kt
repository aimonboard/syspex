package com.syspex.ui.adapter

import android.annotation.SuppressLint
import android.content.Intent
import android.util.SparseBooleanArray
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.syspex.R
import com.syspex.data.remote.NewsRequestTest
import com.syspex.data.remote.NotifRequest
import com.syspex.ui.CaseDetail
import com.syspex.ui.NewsDetail
import com.syspex.ui.ServiceCallDetail
import com.syspex.ui.ServiceReportDetail
import com.syspex.ui.helper.FunHelper
import kotlinx.android.synthetic.main.item_news.view.*


class NewsAdapter : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private var vendorList = mutableListOf<NewsRequestTest.Data.News>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return VendorListViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_news, parent, false))
    }
    override fun getItemCount(): Int = vendorList.size

    override fun onBindViewHolder(viewHolder: RecyclerView.ViewHolder, position: Int) {
        val holder = viewHolder as VendorListViewHolder
        holder.bindView(vendorList[position])

        holder.itemView.setOnClickListener {
            val i = Intent(it.context,NewsDetail::class.java)
            i.putExtra("id", vendorList[position].id)
            i.putExtra("title", vendorList[position].title)
            it.context.startActivity(i)
        }

    }

    fun setList(listOfVendor: List<NewsRequestTest.Data.News>) {
        this.vendorList = listOfVendor.toMutableList()
        notifyDataSetChanged()
    }

    fun addData(data: NewsRequestTest.Data.News) {
        if (!this.vendorList.contains(data)) {
            this.vendorList.add(data)
            notifyDataSetChanged()
        }
    }

    fun clear() {
        this.vendorList.clear()
        notifyDataSetChanged()
    }

    class VendorListViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        @SuppressLint("SetTextI18n")
        fun bindView(newsData: NewsRequestTest.Data.News) {
            if (!newsData.thumnail.isNullOrEmpty()) {
                try {
                    Glide.with(itemView.context).load(newsData.thumnail).into(itemView.photo)
                } catch (e: Exception) { e.stackTrace }
            }

            itemView.tx_title.text = newsData.title ?: "-"
            itemView.tx_desc.text = newsData.plainContent ?: "-"
            itemView.tx_date.text = FunHelper.uiDateFormat(newsData.createdAt ?: "")
            itemView.tx_created_by.text = newsData.createdBy?.userFullname ?: "-"
        }
    }

}