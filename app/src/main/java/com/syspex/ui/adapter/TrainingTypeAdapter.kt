package com.syspex.ui.adapter

import android.content.Intent
import android.net.Uri
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat.startActivity
import androidx.recyclerview.widget.RecyclerView
import com.syspex.R
import com.syspex.data.local.database.global_enum.EnumEntity
import kotlinx.android.synthetic.main.item_training_type.view.*


class TrainingTypeAdapter : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private var eventHandler: RecyclerClickListener? = null
    private var vendorList = mutableListOf<EnumEntity>()
    private var selectedTypeId = ArrayList<String>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return VendorListViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_training_type, parent, false))
    }
    override fun getItemCount(): Int = vendorList.size

    override fun onBindViewHolder(viewHolder: RecyclerView.ViewHolder, position: Int) {
        val holder = viewHolder as VendorListViewHolder
        holder.bindView(vendorList[position])

        holder.itemView.checkbox.setOnCheckedChangeListener { compoundButton, b ->
            if (b) {
                selectedTypeId.add(vendorList[position].enum_id)
                eventHandler?.isClicked(selectedTypeId)
            } else {
                selectedTypeId.remove(vendorList[position].enum_id)
                eventHandler?.isClicked(selectedTypeId)
            }
        }
    }

    fun setList(listOfVendor: List<EnumEntity>) {
        this.vendorList = listOfVendor.toMutableList()
        notifyDataSetChanged()
    }

    fun addList(data: EnumEntity) {
        this.vendorList.add(data)
        notifyDataSetChanged()
    }

    fun clear() {
        this.vendorList.clear()
    }

    class VendorListViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bindView(vendorModel: EnumEntity) {

            itemView.checkbox.text = vendorModel.enum_name

        }
    }

    fun setEventHandler(eventHandler: RecyclerClickListener) {
        try {
            this.eventHandler = eventHandler
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    interface RecyclerClickListener {
        fun isClicked(typeEnumId: ArrayList<String>)
    }

}