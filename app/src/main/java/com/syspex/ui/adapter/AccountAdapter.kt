package com.syspex.ui.adapter

import android.annotation.SuppressLint
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.FragmentActivity
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.syspex.R
import com.syspex.data.model.AccountList
import com.syspex.ui.AccountDetail
import com.syspex.ui.AccountPreview
import com.syspex.ui.diffutil.AccountDiffUtil
import com.syspex.ui.helper.FunHelper
import kotlinx.android.synthetic.main.item_account.view.*
import kotlinx.android.synthetic.main.item_account.view.btn_down
import kotlinx.android.synthetic.main.item_account.view.title_value

class AccountAdapter : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private var accountList = mutableListOf<AccountList>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return VendorListViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_account, parent, false))
    }
    override fun getItemCount(): Int = accountList.size

    override fun onBindViewHolder(viewHolder: RecyclerView.ViewHolder, position: Int) {
        val holder = viewHolder as VendorListViewHolder
        holder.bindView(accountList[position])

        holder.itemView.setOnClickListener {
            val i = Intent(it.context,AccountDetail::class.java)
            i.putExtra("id", accountList[position].accountId)
            i.putExtra("name", accountList[position].account_name)
            it.context.startActivity(i)
        }

        holder.itemView.btn_down.setOnClickListener {
            AccountPreview.accountId = accountList[position].accountId ?: ""
            AccountPreview().show((it.context as FragmentActivity).supportFragmentManager,AccountPreview().tag)
        }

        holder.itemView.btn_location.setOnClickListener {
            FunHelper.direction(it.context, holder.itemView.location_value.text.toString())
        }

    }

    fun setList(listOfVendor: List<AccountList>) {
//        this.accountList = listOfVendor.toMutableList()
//        notifyDataSetChanged()

        val diffCallback = AccountDiffUtil(this.accountList, listOfVendor)
        val diffResult = DiffUtil.calculateDiff(diffCallback)
        this.accountList.clear()
        this.accountList.addAll(listOfVendor)
        diffResult.dispatchUpdatesTo(this)
    }

    class VendorListViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        @SuppressLint("SetTextI18n")
        fun bindView(accountList: AccountList) {
            itemView.title_value.text = accountList.account_name?.ifEmpty { "-" }
            if (!accountList.address.isNullOrEmpty()) {
                val address = accountList.address.sortedByDescending { it?.priority }
                itemView.location_value.text = address.first()?.fullAddress
            } else {
                itemView.location_value.text = "-"
            }

            if (!accountList.equipment.isNullOrEmpty()) {
                itemView.call_equipment.text =  "(${accountList.equipment.size}) ${accountList.equipment.first()?.equipment?.product?.productName}"
            } else {
                itemView.call_equipment.text = "-"
            }

            itemView.last_call.text = "Last Service Call : ${FunHelper.uiDateFormat(accountList.startDate ?: "")}"
            itemView.call_status.text = accountList.callStatusEnumText
            itemView.call_type.text = accountList.callTypeEnumText

        }
    }

}