package com.syspex.ui.adapter

import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.syspex.R
import com.syspex.data.local.database.account_contact.AccountContactEntity
import com.syspex.ui.ContactAdd
import com.syspex.ui.diffutil.ContactDiffUtil
import com.syspex.ui.helper.FunHelper
import kotlinx.android.synthetic.main.item_contact.view.*

class ContactAdapter : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private var vendorList = mutableListOf<AccountContactEntity>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return VendorListViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_contact, parent, false))
    }
    override fun getItemCount(): Int = vendorList.size

    override fun onBindViewHolder(viewHolder: RecyclerView.ViewHolder, position: Int) {
        val holder = viewHolder as VendorListViewHolder
        holder.bindView(vendorList[position])

        holder.itemView.btn_edit.setOnClickListener {
            val i = Intent(it.context, ContactAdd::class.java)
            i.putExtra("accountId",vendorList[position].account_id)
            i.putExtra("accountName",vendorList[position].account_name)
            i.putExtra("contactId",vendorList[position].account_contact_id)
            i.putExtra("accountName",vendorList[position].account_name)
            i.putExtra("contactName",vendorList[position].contact_person_name)
            i.putExtra("contactPhone",vendorList[position].contact_person_phone)
            i.putExtra("contactEmail",vendorList[position].contact_person_email)
            i.putExtra("contactAddress",vendorList[position].contact_person_address)
            i.putExtra("contactPos",vendorList[position].contact_person_position)
            it.context.startActivity(i)
        }

        holder.itemView.btn_call.setOnClickListener {
            val data = holder.itemView.phone_value.text.toString()
            if (data.isNotEmpty()) {
                FunHelper.phoneCall(it.context, data)
            } else {
                Toast.makeText(it.context,"Data not available", Toast.LENGTH_LONG).show()
            }
        }

    }

    fun setList(listOfVendor: List<AccountContactEntity>) {
//        this.vendorList = listOfVendor.toMutableList()
//        notifyDataSetChanged()

        val diffCallback = ContactDiffUtil(this.vendorList, listOfVendor)
        val diffResult = DiffUtil.calculateDiff(diffCallback)
        this.vendorList.clear()
        this.vendorList.addAll(listOfVendor)
        diffResult.dispatchUpdatesTo(this)
    }

    fun addData(data: AccountContactEntity) {
        this.vendorList.add(data)
        notifyDataSetChanged()
    }

    fun clear() {
        this.vendorList.clear()
        notifyDataSetChanged()
    }

    class VendorListViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bindView(vendorModel: AccountContactEntity) {
            itemView.name_value.text = vendorModel.contact_person_name.ifEmpty { "-" }
            itemView.phone_value.text = vendorModel.contact_person_phone.ifEmpty { "-" }
            itemView.email_value.text = vendorModel.contact_person_email.ifEmpty { "-" }
            itemView.position_value.text = vendorModel.contact_person_position.ifEmpty { "-" }
            itemView.company_value.text = vendorModel.account_name.ifEmpty { "-" }
        }
    }

}