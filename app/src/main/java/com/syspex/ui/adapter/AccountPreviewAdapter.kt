package com.syspex.ui.adapter

import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.syspex.R
import com.syspex.data.local.database.account.AccountEntity
import com.syspex.ui.AccountDetail
import com.syspex.ui.helper.FunHelper
import kotlinx.android.synthetic.main.item_account_preview.view.*

class AccountPreviewAdapter : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private var vendorList = mutableListOf<AccountEntity>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return VendorListViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_account_preview, parent, false))
    }
    override fun getItemCount(): Int = vendorList.size

    override fun onBindViewHolder(viewHolder: RecyclerView.ViewHolder, position: Int) {
        val holder = viewHolder as VendorListViewHolder
        holder.bindView(vendorList[position])

        holder.itemView.setOnClickListener {
            val i = Intent(it.context,AccountDetail::class.java)
            i.putExtra("id", vendorList[position].accountId)
            i.putExtra("name", vendorList[position].account_name)
            it.context.startActivity(i)
        }

        holder.itemView.btn_location.setOnClickListener {
            FunHelper.direction(it.context, "")
        }

    }

    fun setList(listOfVendor: List<AccountEntity>) {
        this.vendorList = listOfVendor.toMutableList()
        notifyDataSetChanged()
    }

    class VendorListViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bindView(vendorModel: AccountEntity) {

        }
    }

}