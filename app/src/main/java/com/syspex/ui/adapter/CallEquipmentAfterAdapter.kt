package com.syspex.ui.adapter

import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.syspex.R
import com.syspex.data.local.database.service_call_equipment.ServiceCallEquipmentEntity
import kotlinx.android.synthetic.main.item_equipment_add.view.*

class CallEquipmentAfterAdapter : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private var vendorList = mutableListOf<ServiceCallEquipmentEntity>()
    private var eventHandler: RecyclerClickListener? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return VendorListViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_equipment_add, parent, false))
    }
    override fun getItemCount(): Int = vendorList.size

    override fun onBindViewHolder(viewHolder: RecyclerView.ViewHolder, position: Int) {
        val holder = viewHolder as VendorListViewHolder
        holder.bindView(vendorList[position])

        holder.itemView.btn_add_remove.setOnClickListener {
            eventHandler?.detach(vendorList[position])
        }

    }

    fun setList(listOfVendor: List<ServiceCallEquipmentEntity>) {
        this.vendorList = listOfVendor.toMutableList()
        notifyDataSetChanged()
    }

    fun addData(data: ServiceCallEquipmentEntity) {
        this.vendorList.add(data)
        notifyDataSetChanged()
    }

    fun removeData(data: ServiceCallEquipmentEntity) {
        this.vendorList.remove(data)
        notifyDataSetChanged()
    }

    fun clearData() {
        this.vendorList.clear()
        notifyDataSetChanged()
    }

    class VendorListViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bindView(data: ServiceCallEquipmentEntity) {

            itemView.btn_add_remove.setTextColor(Color.parseColor("#FFFFFF"))
            itemView.btn_add_remove.setBackgroundResource(R.drawable.shape_solid_red)
            itemView.btn_add_remove.text = "Remove Equipment"

            itemView.title_value.text = data.serialNo.ifEmpty { "-" }
            itemView.status_value.text = data.statusTextEnumText.ifEmpty { "-" }
            itemView.number_value.text = data.product_name.ifEmpty { "-" }
            itemView.warranty_end_value.text = data.warrantyEndDate.ifEmpty { "-" }
            itemView.agremeent_number_value.text = data.agreementNumber.ifEmpty { "-" }

        }
    }

    fun setEventHandler(eventHandler: RecyclerClickListener) {
        try {
            this.eventHandler = eventHandler
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    interface RecyclerClickListener {
        fun detach(data: ServiceCallEquipmentEntity)
    }

}