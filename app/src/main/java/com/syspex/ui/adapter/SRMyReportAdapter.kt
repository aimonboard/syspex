package com.syspex.ui.adapter

import android.annotation.SuppressLint
import android.content.Intent
import android.graphics.Color
import android.graphics.PorterDuff
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.FragmentActivity
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.syspex.R
import com.syspex.data.local.database.service_report.ServiceReportEntity
import com.syspex.ui.ServiceReportDetail
import com.syspex.ui.ServiceReportPreview
import com.syspex.ui.diffutil.ReportDiffUtil
import com.syspex.ui.helper.FunHelper
import kotlinx.android.synthetic.main.item_service_report.view.*

class SRMyReportAdapter : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private var vendorList = mutableListOf<ServiceReportEntity>()
    private var withPreview = false

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return VendorListViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_service_report, parent, false))
    }
    override fun getItemCount(): Int = vendorList.size

    override fun onBindViewHolder(viewHolder: RecyclerView.ViewHolder, position: Int) {
        val holder = viewHolder as VendorListViewHolder
        holder.bindView(vendorList[position], withPreview)

        holder.itemView.btn_down.setOnClickListener {
            ServiceReportPreview.reportId = vendorList[position].reportLocalId
            ServiceReportPreview().show((it.context as FragmentActivity).supportFragmentManager,ServiceReportPreview().tag)
        }

        holder.itemView.setOnClickListener {
            val i = Intent(it.context,ServiceReportDetail::class.java)
            i.putExtra("id", vendorList[position].reportLocalId)
            it.context.startActivity(i)
        }
    }

    fun setList(listOfVendor: List<ServiceReportEntity>, withPreview: Boolean) {
//        this.vendorList = listOfVendor.toMutableList()
//        notifyDataSetChanged()
        this.withPreview = withPreview
        val diffCallback = ReportDiffUtil(this.vendorList, listOfVendor)
        val diffResult = DiffUtil.calculateDiff(diffCallback)
        this.vendorList.clear()
        this.vendorList.addAll(listOfVendor)
        diffResult.dispatchUpdatesTo(this)
    }

    fun addData(data: ServiceReportEntity) {
        this.vendorList.add(data)
    }

    fun clear() {
        this.vendorList.clear()
        notifyDataSetChanged()
    }

    class VendorListViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        @SuppressLint("SetTextI18n")
        fun bindView(vendorModel: ServiceReportEntity, withPreview: Boolean) {
            itemView.report_number.text = vendorModel.serviceReportNumber.ifEmpty { "-" }
            itemView.title_value.text = "-"
            itemView.company_value.text = vendorModel.accountName.ifEmpty { "-" }
            itemView.created_value.text = "Created: ${FunHelper.uiDateFormat(vendorModel.createdDate)}"
            itemView.status_value.text = vendorModel.reportStatusEnumText.ifEmpty { "-" }
            itemView.status2_value.text = vendorModel.reportTypeEnumText.ifEmpty { "-" }

            if (!vendorModel.equipment.isNullOrEmpty()) {
                itemView.product_value.text =  "(${vendorModel.equipment.size}) ${vendorModel.equipment.first()?.equipment?.product?.productName}"
            } else {
                itemView.product_value.text = "-"
            }

            if (vendorModel.servicedByUserName.isNotEmpty()) {
                itemView.engineer_value.text = vendorModel.servicedByUserName
            } else {
                itemView.engineer_value.visibility = View.GONE
            }

            if (!vendorModel.colorBackground.isNullOrEmpty()) {
                itemView.parent_lay.setBackgroundColor(Color.parseColor(vendorModel.colorBackground))
            }

            if (!vendorModel.colorStatusLabel.isNullOrEmpty() && !vendorModel.colorStatusLabelText.isNullOrEmpty()) {
                itemView.status2_value.background.setColorFilter(Color.parseColor(vendorModel.colorStatusLabel), PorterDuff.Mode.SRC_ATOP)
                itemView.status2_value.setTextColor(Color.parseColor(vendorModel.colorStatusLabelText))
            }

            if (!withPreview) {
                itemView.btn_down.visibility = View.GONE
            }
        }
    }

}