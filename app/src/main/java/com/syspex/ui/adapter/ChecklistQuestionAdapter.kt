package com.syspex.ui.adapter

import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import androidx.core.widget.doAfterTextChanged
import androidx.fragment.app.FragmentActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.syspex.R
import com.syspex.data.local.database.global_enum.EnumEntity
import com.syspex.data.local.database.service_report_equipment_checklist.ServiceReportEquipmentChecklistEntity
import com.syspex.data.model.ConverterImageModel
import com.syspex.ui.ChecklistConfirm
import com.syspex.ui.DialogImagePreview
import com.syspex.ui.helper.ChecklistInterface
import kotlinx.android.synthetic.main.item_checklist_question.view.*

class ChecklistQuestionAdapter(private var itemListener: ChecklistInterface): RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private var vendorList = mutableListOf<ServiceReportEquipmentChecklistEntity>()
    private var enumData = mutableListOf<EnumEntity>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return VendorListViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_checklist_question, parent, false))
    }
    override fun getItemCount(): Int = vendorList.size

    override fun onBindViewHolder(viewHolder: RecyclerView.ViewHolder, position: Int) {
        val holder = viewHolder as VendorListViewHolder
        holder.bindView(vendorList[position], enumData, itemListener)

        holder.itemView.btn_help.setOnClickListener {
            DialogImagePreview.helpText = vendorList[position].question_help_text
            DialogImagePreview.helpImage = vendorList[position].question_help_image_url
            DialogImagePreview().show((it.context as FragmentActivity).supportFragmentManager,DialogImagePreview().tag)
        }

        holder.itemView.btn_add_image.setOnClickListener {
            itemListener.onQuestionAddImage(
                questionId = vendorList[position].question_id,
                image = vendorList[position].checklistImage as ArrayList<ConverterImageModel>,
            )
        }

    }

    fun setList(listOfVendor: List<ServiceReportEquipmentChecklistEntity>, enumData: List<EnumEntity>) {
        this.vendorList = listOfVendor.toMutableList()
        this.enumData = enumData.toMutableList()
        notifyDataSetChanged()
    }



    class VendorListViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bindView(vendorModel: ServiceReportEquipmentChecklistEntity, enumData: List<EnumEntity>, itemListener: ChecklistInterface) {

            itemView.tx_title.text = vendorModel.question_desc
            itemView.note_value.setText(
                if (vendorModel.remarks.isNotEmpty()) vendorModel.remarks
                else vendorModel.question_default_remarks
            )

            val spinnerData = ArrayList<String>()
            spinnerData.add("")
            enumData.forEachIndexed { index, enumEntity ->
                spinnerData.add(enumEntity.enum_name)
            }

            var isFirstLoad = true
            val arrayAdapter = ArrayAdapter(itemView.context, R.layout.item_spinner_gray, spinnerData)
            itemView.spinner_value?.adapter = arrayAdapter
            itemView.spinner_value?.onItemSelectedListener = object : AdapterView.OnItemSelectedListener{
                override fun onNothingSelected(parent: AdapterView<*>?) { }
                override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                    if (!isFirstLoad) {
                        if (position == 0) {
                            itemListener.onQuestionStatusChange(
                                questionId = vendorModel.question_id,
                                enumId = "",
                                enumName = "",
                                remark = itemView.note_value.text.toString()
                            )
                        } else {
                            itemListener.onQuestionStatusChange(
                                questionId = vendorModel.question_id,
                                enumId = enumData[position-1].enum_id,
                                enumName = enumData[position-1].enum_name,
                                remark = itemView.note_value.text.toString()
                            )
                        }
                    }
                    isFirstLoad = false
                }
            }

            enumData.forEachIndexed { index, enumEntity ->
                if (vendorModel.checklist_status_id == enumEntity.enum_id) {
                    itemView.spinner_value.setSelection(index+1)
                }
            }

            itemView.note_value.addTextChangedListener(object : TextWatcher {
                override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}
                override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}
                override fun afterTextChanged(p0: Editable?) {
                    if (itemView.spinner_value.selectedItemPosition == 0) {
                        itemListener.onQuestionStatusChange(
                            questionId = vendorModel.question_id,
                            enumId = "",
                            enumName = "",
                            remark = p0.toString()
                        )
                    } else {
                        itemListener.onQuestionStatusChange(
                            questionId = vendorModel.question_id,
                            enumId = enumData[itemView.spinner_value.selectedItemPosition -1].enum_id,
                            enumName = enumData[itemView.spinner_value.selectedItemPosition -1].enum_name,
                            remark = p0.toString()
                        )
                    }
                }
            })

            val adapterSymptom = ChecklistQuestionImageAdapter(itemListener)
            itemView.recycler_image.layoutManager = LinearLayoutManager(itemView.context, RecyclerView.HORIZONTAL, false)
            itemView.recycler_image.adapter = adapterSymptom

            adapterSymptom.setList(
                questionId = vendorModel.question_id,
                image = vendorModel.checklistImage as ArrayList<ConverterImageModel>
            )
        }
    }

}