package com.syspex.ui.adapter

import android.content.Intent
import android.net.Uri
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat.startActivity
import androidx.recyclerview.widget.RecyclerView
import com.syspex.R
import com.syspex.data.local.database.service_call_completion.ServiceCallCompletionEntity
import com.syspex.data.remote.GetDataRequest
import com.syspex.ui.helper.FunHelper
import kotlinx.android.synthetic.main.item_technical_document.view.*


class CallCompletionAdapter : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private var eventHandler: RecyclerClickListener? = null
    private var vendorList = mutableListOf<ServiceCallCompletionEntity>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return VendorListViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_technical_document, parent, false))
    }
    override fun getItemCount(): Int = vendorList.size

    override fun onBindViewHolder(viewHolder: RecyclerView.ViewHolder, position: Int) {
        val holder = viewHolder as VendorListViewHolder
        holder.bindView(vendorList[position])

        holder.itemView.setOnClickListener {
            val url = "${FunHelper.BASE_URL}api/pdf/form-installation/${vendorList[position].handoverNumber}"
            eventHandler?.isClicked(url)
        }

        if (position == vendorList.lastIndex) {
            holder.itemView.divider.visibility = View.GONE
        } else {
            holder.itemView.divider.visibility = View.VISIBLE
        }

    }

    fun setList(listOfVendor: List<ServiceCallCompletionEntity>) {
        this.vendorList = listOfVendor.toMutableList()
        notifyDataSetChanged()
    }

    fun addList(data: ServiceCallCompletionEntity) {
        this.vendorList.add(data)
        notifyDataSetChanged()
    }

    fun clear() {
        this.vendorList.clear()
    }

    class VendorListViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bindView(vendorModel: ServiceCallCompletionEntity) {

            itemView.attachment_title.text = vendorModel.handoverNumber

        }
    }

    fun setEventHandler(eventHandler: RecyclerClickListener) {
        try {
            this.eventHandler = eventHandler
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    interface RecyclerClickListener {
        fun isClicked(fileUrl: String)
    }

}