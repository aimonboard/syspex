package com.syspex.ui.adapter

import android.annotation.SuppressLint
import android.content.Intent
import android.util.SparseBooleanArray
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.syspex.R
import com.syspex.data.local.database.product_group.ProductEntity
import com.syspex.ui.ProductDetail
import com.syspex.ui.diffutil.EquipmentDiffUtil
import com.syspex.ui.diffutil.ProductDiffUtil
import kotlinx.android.synthetic.main.item_product.view.*


class ProductAdapter : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private var vendorList = mutableListOf<ProductEntity>()
    private val itemStateArray = SparseBooleanArray()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return VendorListViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_product, parent, false))
    }
    override fun getItemCount(): Int = vendorList.size

    override fun onBindViewHolder(viewHolder: RecyclerView.ViewHolder, position: Int) {
        val holder = viewHolder as VendorListViewHolder
        holder.bindView(vendorList[position])

        holder.itemView.setOnClickListener {
            val i = Intent(it.context,ProductDetail::class.java)
            i.putExtra("productGroupId",vendorList[position].product_group_id)
            i.putExtra("productGroupName",vendorList[position].product_group_name)
            it.context.startActivity(i)
        }

    }

    fun setList(listOfVendor: List<ProductEntity>) {
//        this.vendorList = listOfVendor.toMutableList()
//        notifyDataSetChanged()

        val diffCallback = ProductDiffUtil(this.vendorList, listOfVendor)
        val diffResult = DiffUtil.calculateDiff(diffCallback)
        this.vendorList.clear()
        this.vendorList.addAll(listOfVendor)
        diffResult.dispatchUpdatesTo(this)
    }

    fun addData(data: ProductEntity) {
        this.vendorList.add(data)
        notifyDataSetChanged()
    }

    fun clear() {
        this.vendorList.clear()
        notifyDataSetChanged()
    }

    class VendorListViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        @SuppressLint("SetTextI18n")
        fun bindView(productData: ProductEntity) {
            itemView.technical_product_value.text = productData.product_group_name.ifEmpty { "-" }

            val voltage = productData.voltage.ifEmpty { "-" }
            val current = productData.current.ifEmpty { "-" }
            val power = productData.power.ifEmpty { "-" }
            val phase = productData.phase.ifEmpty { "-" }
            val frequency = productData.frequency.ifEmpty { "-" }
            itemView.technical_information_value.text = "$voltage | $current | $power | $phase | $frequency"

            itemView.technical_air_supply_value.text = productData.air_supply_needed.ifEmpty { "-" }
            itemView.technical_air_pressure_value.text = productData.air_pressure.ifEmpty { "-" }
            itemView.machine_brand_value.text = productData.machine_brand.ifEmpty { "-" }
            itemView.machine_model_value.text = productData.machine_model.ifEmpty { "-" }
            itemView.machine_class_value.text = productData.machine_class_category.ifEmpty { "-" }
            itemView.technical_supplier_value.text = productData.machine_supplier.ifEmpty { "-" }
            itemView.technical_tag_value.text = productData.product_group_tag_id.ifEmpty { "-" }
        }
    }

}