package com.syspex.ui

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.fragment.app.FragmentActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.syspex.R
import com.syspex.data.local.SharedPreferenceData
import com.syspex.data.local.database.pcs_solution.SolutionEntity
import com.syspex.data.local.database.pcs_solution.SolutionViewModel
import com.syspex.data.local.database.product_sparepart.SparepartViewModel
import com.syspex.data.model.ConverterImageModel
import com.syspex.ui.adapter.SRImageCauseAdapter
import com.syspex.ui.adapter.SRImageSolutionAdapter
import com.syspex.ui.helper.FunHelper
import kotlinx.android.synthetic.main.cause_add_activity.*
import kotlinx.android.synthetic.main.cause_add_activity.btn_save
import kotlinx.android.synthetic.main.cause_add_activity.divider_cause
import kotlinx.android.synthetic.main.sub_header.back_link
import kotlinx.android.synthetic.main.sub_header.header_name

class CauseAdd : AppCompatActivity() {

    companion object {
        var causeImage = ArrayList<ConverterImageModel>()
        var solutionImage = ArrayList<ConverterImageModel>()
    }

    private lateinit var reportSolution: SolutionViewModel

    private var causeAdapter = SRImageCauseAdapter(true)
    private var solutionAdapter = SRImageSolutionAdapter(true)

    private var isEdit = false
    private var reportEquipmentId = ""
    private var equipmentId = ""
    private var solutionId = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.cause_add_activity)
        header_name?.text = "Add Cause / Solution"

        reportSolution = ViewModelProviders.of(this).get(SolutionViewModel::class.java)

        generateView()
        buttonListener()

        setupAdapter()
        causeSolutionImageAdapterListener()
        solutionObserver()
    }

    // Return from dialog upload file
    override fun onResume() {
        super.onResume()
        causeImage.forEach {
            causeAdapter.addData(it)
        }
        causeImage.clear()


        solutionImage.forEach {
            solutionAdapter.addData(it)
        }
        solutionImage.clear()
    }

    override fun onBackPressed() {
        super.onBackPressed()
        if (isEdit) {
            updateCauseSolution()
        } else {
            insertCauseSolution()
        }

    }

    private fun generateView() {
        if (intent.hasExtra("reportEquipmentId")) {
            isEdit = false
            reportEquipmentId = intent.getStringExtra("reportEquipmentId")!!
            equipmentId = intent.getStringExtra("equipmentId")!!
        }

        if (intent.hasExtra("edit_reportEquipmentId")) {
            isEdit = true
            reportEquipmentId = intent.getStringExtra("edit_reportEquipmentId")!!
            equipmentId = intent.getStringExtra("edit_equipmentId")!!
            solutionId = intent.getStringExtra("edit_solutionId")!!
        }
    }

    private fun buttonListener() {
        back_link?.setOnClickListener {
            onBackPressed()
        }

        btn_cause_image.setOnClickListener {
            DialogUploadFile.fileSection = "cause"
            DialogUploadFile.equipmentId = equipmentId
            DialogUploadFile().show((it.context as FragmentActivity).supportFragmentManager,DialogUploadFile().tag)
        }

        btn_solution_image.setOnClickListener {
            DialogUploadFile.fileSection = "solution"
            DialogUploadFile.equipmentId = equipmentId
            DialogUploadFile().show((it.context as FragmentActivity).supportFragmentManager,DialogUploadFile().tag)
        }

        btn_save.setOnClickListener {
            onBackPressed()
        }
    }

    private fun setupAdapter() {
        recycler_cause.layoutManager = LinearLayoutManager(this, RecyclerView.HORIZONTAL, false)
        recycler_cause.adapter = causeAdapter

        recycler_solution.layoutManager = LinearLayoutManager(this, RecyclerView.HORIZONTAL, false)
        recycler_solution.adapter = solutionAdapter
    }

    private fun causeSolutionImageAdapterListener() {
        causeAdapter.registerAdapterDataObserver(object : RecyclerView.AdapterDataObserver() {
            override fun onChanged() {
                super.onChanged()
                if (causeAdapter.itemCount == 0) {
                    divider_cause.visibility = View.GONE
                } else {
                    divider_cause.visibility = View.VISIBLE
                }
            }
        })

        solutionAdapter.registerAdapterDataObserver(object : RecyclerView.AdapterDataObserver() {
            override fun onChanged() {
                super.onChanged()
                if (solutionAdapter.itemCount == 0) {
                    divider_solution.visibility = View.GONE
                } else {
                    divider_solution.visibility = View.VISIBLE
                }
            }
        })
    }

    private fun solutionObserver() {
        Log.e("aim", "solutionId: $solutionId")
        reportSolution.getBySolutionId(solutionId).observe(this, Observer { data->
            data.forEach {
                subject_value.setText(it.subject)
                cause_value.setText(it.problemCause)
                solution_value.setText(it.solution)

                causeAdapter.setList(it.causeImage as ArrayList<ConverterImageModel>)
                solutionAdapter.setList(it.solutionImage as ArrayList<ConverterImageModel>)
            }


        })
    }

    private fun insertCauseSolution() {
        if (subject_value.text.isNotEmpty() ||
            cause_value.text.isNotEmpty() || causeAdapter.getAllItem().isNotEmpty() ||
            solution_value.text.isNotEmpty() || solutionAdapter.getAllItem().isNotEmpty()) {

            reportSolution.insert(SolutionEntity(
                FunHelper.randomString("") ?: "",
                reportEquipmentId ?: "",
                subject_value.text.toString() ?: "",
                cause_value.text.toString() ?: "",
                solution_value.text.toString() ?: "",
                FunHelper.getDate(false) ?: "",
                SharedPreferenceData.getString(this, 8, "") ?: "",
                "",
                "",
                causeAdapter.getAllItem(),
                solutionAdapter.getAllItem(),
                false,
                true
            ))

            Toast.makeText(this, "Data saved", Toast.LENGTH_LONG).show()

        }
    }

    private fun updateCauseSolution() {
        if (subject_value.text.isNotEmpty() ||
            cause_value.text.isNotEmpty() || causeAdapter.getAllItem().isNotEmpty() ||
            solution_value.text.isNotEmpty() || solutionAdapter.getAllItem().isNotEmpty()) {

            reportSolution.update(SolutionEntity(
                solutionId ?: "",
                reportEquipmentId ?: "",
                subject_value.text.toString() ?: "",
                cause_value.text.toString() ?: "",
                solution_value.text.toString() ?: "",
                FunHelper.getDate(false) ?: "",
                SharedPreferenceData.getString(this, 8, "") ?: "",
                "",
                "",
                causeAdapter.getAllItem(),
                solutionAdapter.getAllItem(),
                false,
                true
            ))

            Toast.makeText(this, "Data saved", Toast.LENGTH_LONG).show()

        }
    }
}