package com.syspex.ui

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.syspex.R
import com.syspex.data.local.SharedPreferenceData
import com.syspex.data.remote.NotifRequest
import com.syspex.data.retrofit.ApiClient
import com.syspex.ui.adapter.NotifAdapter
import com.syspex.ui.helper.FunHelper
import kotlinx.android.synthetic.main.notif_activity.*
import kotlinx.android.synthetic.main.notif_activity.swipe_container
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class Notif : AppCompatActivity() {

    private val notifAdapter = NotifAdapter()

    private var page = 1
    private var lastPage = 1

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.notif_activity)

        setupAdapter()
        buttonListener()
        getNotif()
        recyclerScrollListener()

    }

    private fun buttonListener() {
        swipe_container.setOnRefreshListener {
            page = 1
            notifAdapter.clear()
            getNotif()
        }

        back_link.setOnClickListener {
            onBackPressed()
        }
    }

    private fun setupAdapter() {
        recycler_notif.layoutManager = LinearLayoutManager(this, RecyclerView.VERTICAL, false)
        recycler_notif.adapter = notifAdapter
    }

    private fun recyclerScrollListener() {
        recycler_notif?.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                val linearLayoutManager = recyclerView.layoutManager as LinearLayoutManager
                val countItem = linearLayoutManager.itemCount
                val lastVisiblePosition = linearLayoutManager.findLastCompletelyVisibleItemPosition()
                val isLastPosition = countItem.minus(1) == lastVisiblePosition

                swipe_container.isEnabled = linearLayoutManager.findFirstCompletelyVisibleItemPosition() == 0

                if(isLastPosition && page < lastPage && notifAdapter.itemCount > 0) {
                    page++
                    getNotif()
                }
            }
        })
    }

    private fun getNotif() {
        swipe_container.isRefreshing = true
        val token = "Bearer "+ SharedPreferenceData.getString(this,2, "")
        val userId = SharedPreferenceData.getString(this,8, "")

        ApiClient.instance.getNotif(token, page).enqueue(object: Callback<NotifRequest> {
            override fun onFailure(call: Call<NotifRequest>, t: Throwable) {
                swipe_container.isRefreshing = false
                Toast.makeText(this@Notif,"Connection failed", Toast.LENGTH_LONG).show()
            }
            override fun onResponse(call: Call<NotifRequest>, response: Response<NotifRequest>) {
                swipe_container.isRefreshing = false
                if (!response.isSuccessful) {
                    if (response.code() == 401) {
                        FunHelper.clearAllData(this@Notif)
                        Toast.makeText(this@Notif,"Session end", Toast.LENGTH_LONG).show()
                    } else {
                        FunHelper.firebaseApiLog(
                            userId ?: "",
                            response.raw().request().url().toString() ?: "",
                            response.raw().request().headers().toString() ?: "",
                            "",
                            response.raw().toString() ?: ""
                        )
                        Toast.makeText(this@Notif, response.raw().toString(), Toast.LENGTH_LONG).show()
                    }
                }
                else {

                    page = response.body()?.data?.page ?: 1
                    lastPage = response.body()?.data?.lastPage ?: 1

                    response.body()?.data?.notification?.forEach {
                        if (it != null) {
                            notifAdapter.addData(it)
                        }
                    }
                    notifAdapter.notifyDataSetChanged()

                }
            }
        })
    }

}
