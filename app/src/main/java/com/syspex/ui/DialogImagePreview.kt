package com.syspex.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.syspex.R
import kotlinx.android.synthetic.main.dialog_image_preview.*


class DialogImagePreview : BottomSheetDialogFragment() {

    companion object {
        var helpText = ""
        var helpImage = ""
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,savedInstanceState: Bundle?): View? {
        // Set dialog initial state when shown
        dialog?.setOnShowListener {
            val bottomSheetDialog = it as BottomSheetDialog
            val sheetInternal: View = bottomSheetDialog.findViewById(com.google.android.material.R.id.design_bottom_sheet)!!
            BottomSheetBehavior.from(sheetInternal).state = BottomSheetBehavior.STATE_EXPANDED
        }

        return inflater.inflate(R.layout.dialog_image_preview, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        try {
            help_text.text = helpText

            if (helpText.isEmpty()) {
                help_text.visibility = View.GONE
            } else {
                help_text.visibility = View.VISIBLE
            }

            Glide.with(this).load(helpImage).placeholder(R.drawable.thumbnail).into(help_image)
        } catch (e: Exception) { }

    }

}
