package com.syspex.ui

import android.content.Context
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CalendarView
import android.widget.TimePicker
import androidx.fragment.app.Fragment
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.syspex.R
import kotlinx.android.synthetic.main.date_time_picker_activity.*
import java.text.DecimalFormat
import java.text.SimpleDateFormat
import java.util.*

/**
 * A simple [Fragment] subclass.
 */
class DialogDateTime : BottomSheetDialogFragment() {

    private var eventHandler: OnFinishedListener? = null

    private var isDate = false
    private var isTimeStart = false
    private var isTimeEnd = false

    private var timeSelected = ""
    private var dateSelected = ""

    override fun onAttach(context: Context) {
        super.onAttach(context)
        try {
            eventHandler = context as OnFinishedListener
        } catch (e: Exception) { Log.d("aim", e.toString()) }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,savedInstanceState: Bundle?): View? {
        // Set dialog initial state when shown
        dialog?.setOnShowListener {
            val bottomSheetDialog = it as BottomSheetDialog
            val sheetInternal: View = bottomSheetDialog.findViewById(com.google.android.material.R.id.design_bottom_sheet)!!
            BottomSheetBehavior.from(sheetInternal).state = BottomSheetBehavior.STATE_EXPANDED
        }

        return inflater.inflate(R.layout.date_time_picker_activity, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        generateView()

        buttonListener()

    }

    private fun generateView() {
        tx_title.text = arguments?.getString("title", "")

        val dateOrTime = requireArguments().getString("date_time")!!
        when (dateOrTime) {
            "date" -> {
                isDate = true
                time_value.visibility = View.GONE
            }
            "time_start" -> {
                isTimeStart = true
                date_value.visibility = View.GONE
            }
            "time_end" -> {
                isTimeEnd = true
                date_value.visibility = View.GONE
            }
        }
    }

    private fun buttonListener() {
        time_value.setIs24HourView(true)
        time_value.setOnTimeChangedListener{ timePicker: TimePicker, hour: Int, minute: Int ->
            timeSelected = "${DecimalFormat("00").format(hour)}:${DecimalFormat("00").format(minute)}"
        }

        date_value.setOnDateChangeListener{ calendarView: CalendarView, year: Int, month: Int, date: Int ->
            dateSelected = "${DecimalFormat("00").format(date)}-${DecimalFormat("00").format(month+1)}-$year"
        }

        btn_set.setOnClickListener {

            val sdf = SimpleDateFormat("dd-MM-yyyy HH:mm", Locale.getDefault())
            val now = sdf.format(Date()).split(" ")

            when {
                isDate -> {
                    val data = dateSelected.ifBlank { now[0] }
                    eventHandler?.onFinished("date", data)
                }
                isTimeStart -> {
                    val data = timeSelected.ifBlank { now[1] }
                    eventHandler?.onFinished("time_start", data)
                }
                isTimeEnd -> {
                    val data = timeSelected.ifBlank { now[1] }
                    eventHandler?.onFinished("time_end", data)
                }
                else -> {
                    val data = "${dateSelected.ifBlank { now[0] }} ${timeSelected.ifBlank { now[1] }}"
                    eventHandler?.onFinished("datetime", data)
                }
            }

            dismiss()

        }
    }

    interface OnFinishedListener {
        fun onFinished(date_time: String, finalResult: String)
    }


}
