package com.syspex.ui

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import com.bumptech.glide.Glide
import com.syspex.R
import kotlinx.android.synthetic.main.activity_image_preview.*
import kotlinx.android.synthetic.main.sub_header.*

class ImagePreview : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_image_preview)
        header_name.text = "Image Preview"

        generateView()
        buttonListener()

    }

    private fun generateView() {
        if (intent.hasExtra("image")) {
            val image = intent.getStringExtra("image")!!
            Log.e("aim", "image receive : $image")
            try {
                Glide.with(this).load(image).into(image_lay)
            } catch (e: Exception) { }
        }
    }

    private fun buttonListener() {
        back_link.setOnClickListener {
            onBackPressed()
        }
    }
}
