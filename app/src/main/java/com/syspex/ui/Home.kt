package com.syspex.ui

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.CheckBox
import androidx.core.view.GravityCompat
import androidx.core.widget.NestedScrollView
import androidx.drawerlayout.widget.DrawerLayout
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.syspex.Menu2
import com.syspex.R
import com.syspex.data.local.SharedPreferenceData
import com.syspex.data.local.database.activity.ActivityEntity
import com.syspex.data.local.database.activity.ActivityViewModel
import com.syspex.data.local.database.service_call.ServiceCallEntity
import com.syspex.data.local.database.service_call.ServiceCallViewModel
import com.syspex.data.local.database.service_report.ServiceReportViewModel
import com.syspex.data.local.database.service_report_time_tracking.ServiceReportTimeTrackingEntity
import com.syspex.data.local.database.service_report_time_tracking.ServiceReportTimeTrackingViewModel
import com.syspex.ui.adapter.*
import com.syspex.ui.custom.OnSwipeTouchListener
import kotlinx.android.synthetic.main.calendar_layout.*
import kotlinx.android.synthetic.main.home_fragment.*
import kotlinx.android.synthetic.main.home_fragment.btn_filter
import kotlinx.android.synthetic.main.home_fragment.btn_menu
import kotlinx.android.synthetic.main.home_fragment.btn_search
import kotlinx.android.synthetic.main.home_fragment.scroll_lay
import kotlinx.android.synthetic.main.home_fragment.swipe_container
import kotlinx.android.synthetic.main.shimmer_layout.*
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList


class Home : Fragment() {

    private lateinit var callVM: ServiceCallViewModel
    private lateinit var activityVM: ActivityViewModel
    private lateinit var reportVM: ServiceReportViewModel
    private lateinit var timeTrackingVM: ServiceReportTimeTrackingViewModel

    private var activityAdapter = ActivityHomeAdapter()
    private var myCallAdapter = SCMyCallAdapter()
    private var teamCallAdapter = SCTeamCallAdapter()
    private val myTimeTrackingAdapter = ActivityMyTimeTrackingAdapter()
    private val teamTimeTrackingAdapter = ActivityTeamTimeTrackingAdapter()
    private val calendarObject = Calendar.getInstance()

    private val activityDataList = ArrayList<ActivityEntity>()
    private val myCallData = ArrayList<ServiceCallEntity>()
    private val teamCallData = ArrayList<ServiceCallEntity>()
    private val myTimeTrackingData = ArrayList<ServiceReportTimeTrackingEntity>()
    private val teamTimeTrackingData = ArrayList<ServiceReportTimeTrackingEntity>()

    private val calendarFormat = SimpleDateFormat("dd-MM-yyyy", Locale.getDefault())
    private val uiFormat = SimpleDateFormat("EEE, dd MMM yyyy", Locale.getDefault())
    private var selectedDate = Date()

    private val allEvent = ArrayList<String>()
    private var filterActivity = true
    private var filterCall = true
    private var filterTimeTracking = true

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.home_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        callVM = ViewModelProviders.of(this).get(ServiceCallViewModel::class.java)
        activityVM = ViewModelProviders.of(this).get(ActivityViewModel::class.java)
        reportVM = ViewModelProviders.of(this).get(ServiceReportViewModel::class.java)
        timeTrackingVM = ViewModelProviders.of(this).get(ServiceReportTimeTrackingViewModel::class.java)

        buttonListener()
        firstLoadCalendar()
        calendarListener()

        setupAdapter()
        syncObserver()
        scrollListener()

    }

    private fun buttonListener(){
        val drawerLayout: DrawerLayout = requireActivity().findViewById(R.id.drawer_layout)
        btn_menu?.setOnClickListener {
            if(!drawerLayout.isDrawerOpen(GravityCompat.START))
                drawerLayout.openDrawer(GravityCompat.START)
        }

        btn_search.setOnClickListener {
            val i = Intent(it.context, SearchLocal::class.java)
            startActivity(i)
        }

        btn_notif.setOnClickListener {
            val i = Intent(context, Notif::class.java)
            startActivity(i)
        }

        swipe_container.setOnRefreshListener {
            syncObserver()
        }

        btn_filter.setOnClickListener {
            dialogFilter()
        }

        btn_add_activity.setOnClickListener {
            val i = Intent(context, ActivityAdd::class.java)
            startActivity(i)
        }

        btn_add_activity2.setOnClickListener {
            val i = Intent(context, ActivityAdd::class.java)
            startActivity(i)
        }
    }

    private fun firstLoadCalendar() {
        btn_calendar.text = uiFormat.format(selectedDate)
        custom_calendar?.setUpCalendarAdapter(calendarObject,allEvent)
    }

    private fun calendarListener() {
        btn_calendar.setOnClickListener {
            calendar_container.isExpanded = !calendar_container.isExpanded
        }

        btn_left.setOnClickListener {
            if (calendar_container.isExpanded) {
                calendarObject.add(Calendar.MONTH, -1)
                btn_calendar.text = uiFormat.format(calendarObject.time)
                selectedDate = calendarObject.time

                custom_calendar?.setUpCalendarAdapter(calendarObject, allEvent)
            } else {
                calendarObject.add(Calendar.DATE, -1)
                btn_calendar.text = uiFormat.format(calendarObject.time)
                selectedDate = calendarObject.time
            }

            Log.e("aim","calendar - btn_left : ${calendarFormat.format(selectedDate)}")
            syncObserver()
        }

        btn_right.setOnClickListener {
            if (calendar_container.isExpanded) {
                calendarObject.add(Calendar.MONTH, 1)
                btn_calendar.text = uiFormat.format(calendarObject.time)
                selectedDate = calendarObject.time

                custom_calendar?.setUpCalendarAdapter(calendarObject, allEvent)
            } else {
                calendarObject.add(Calendar.DATE, 1)
                btn_calendar.text = uiFormat.format(calendarObject.time)
                selectedDate = calendarObject.time
            }

            Log.e("aim","calendar - btn_right : ${calendarFormat.format(selectedDate)}")
            syncObserver()
        }

        calendar_grid.onItemClickListener = AdapterView.OnItemClickListener {
                parent, view, position, id ->

            selectedDate = parent?.getItemAtPosition(position) as Date
            calendarObject.time = selectedDate
            btn_calendar.text = uiFormat.format(selectedDate)
            calendar_container.isExpanded = false

            Log.e("aim","calendar - click : ${calendarFormat.format(selectedDate)}")
            syncObserver()
        }

        calendar_grid.setOnTouchListener(object : OnSwipeTouchListener(requireContext()) {
            override fun onSwipeTop() {
                super.onSwipeTop()
                calendar_container.isExpanded = false
            }
            override fun onSwipeBottom() {
                super.onSwipeBottom()
                Log.d("aim","swipe bottom")
            }
            override fun onSwipeLeft() {
                super.onSwipeLeft()
                calendarObject.add(Calendar.MONTH, 1)
                btn_calendar.text = uiFormat.format(calendarObject.time)
                selectedDate = calendarObject.time
                custom_calendar?.setUpCalendarAdapter(calendarObject,allEvent)

                Log.e("aim","calendar - swipe_left : ${calendarFormat.format(selectedDate)}")
                syncObserver()
            }
            override fun onSwipeRight() {
                super.onSwipeRight()
                calendarObject.add(Calendar.MONTH, -1)
                btn_calendar.text = uiFormat.format(calendarObject.time)
                selectedDate = calendarObject.time
                custom_calendar?.setUpCalendarAdapter(calendarObject,allEvent)

                Log.e("aim","calendar - swipe_right : ${calendarFormat.format(selectedDate)}")
                syncObserver()
            }
        })
    }

    private fun dialogFilter() {
        val view = layoutInflater.inflate(R.layout.dialog_activity,null)
        val dialog = BottomSheetDialog(requireContext())
        dialog.setContentView(view)
        val bottomSheetBehavior = BottomSheetBehavior.from<View>(view.parent as View)
        dialog.setOnShowListener { bottomSheetBehavior.state = BottomSheetBehavior.STATE_EXPANDED }

        val checkboxActivity = dialog.findViewById<CheckBox>(R.id.checkbox_activity)
        val checkboxCall = dialog.findViewById<CheckBox>(R.id.checkbox_call)
        val checkboxTimeTracking = dialog.findViewById<CheckBox>(R.id.checkbox_timetracking)

        checkboxActivity?.isChecked = filterActivity
        checkboxCall?.isChecked = filterCall
        checkboxTimeTracking?.isChecked = filterTimeTracking

        checkboxActivity?.setOnCheckedChangeListener { compoundButton, b ->
            filterActivity = b
            if (b) recycler_activity.visibility = View.VISIBLE
            else recycler_activity.visibility = View.GONE
        }

        checkboxCall?.setOnCheckedChangeListener { compoundButton, b ->
            filterCall = b
            if (b) {
                recycler_my_call.visibility = View.VISIBLE
                recycler_team_call.visibility = View.VISIBLE
            } else {
                recycler_my_call.visibility = View.GONE
                recycler_team_call.visibility = View.GONE
            }
        }

        checkboxTimeTracking?.setOnCheckedChangeListener { compoundButton, b ->
            filterTimeTracking = b
            if (b) {
                recycler_my_time_tracking.visibility = View.VISIBLE
                recycler_team_time_tracking.visibility = View.VISIBLE
            } else {
                recycler_my_time_tracking.visibility = View.GONE
                recycler_team_time_tracking.visibility = View.GONE
            }
        }

        dialog.show()
    }

    private fun setupAdapter() {
        recycler_activity.layoutManager = LinearLayoutManager(requireContext(), RecyclerView.VERTICAL, false)
        recycler_activity.adapter = activityAdapter

        recycler_my_call?.layoutManager = LinearLayoutManager(requireContext(), RecyclerView.VERTICAL, false)
        recycler_my_call?.adapter = myCallAdapter

        recycler_team_call?.layoutManager = LinearLayoutManager(requireContext(), RecyclerView.VERTICAL, false)
        recycler_team_call?.adapter = teamCallAdapter

        recycler_my_time_tracking?.layoutManager = LinearLayoutManager(requireContext(), RecyclerView.VERTICAL, false)
        recycler_my_time_tracking?.adapter = myTimeTrackingAdapter

        recycler_team_time_tracking?.layoutManager = LinearLayoutManager(requireContext(), RecyclerView.VERTICAL, false)
        recycler_team_time_tracking?.adapter = teamTimeTrackingAdapter
    }

    private fun scrollListener() {
        scroll_lay.setOnScrollChangeListener { v: NestedScrollView?, x: Int, y: Int, oldX: Int, oldY: Int ->
            swipe_container.isEnabled = y == 0
        }
    }

    private fun syncObserver() {
        Menu2.isLoading.observe(viewLifecycleOwner, Observer {
            Log.e("aim", "sync proses $it")
            if (it == false) {
                activityObserver()
            }
        })
    }

    private fun activityObserver() {
        shimmer_container.visibility = View.VISIBLE
        val activityObserver = activityVM.getAll()
        activityObserver.observe(viewLifecycleOwner, Observer {
            activityObserver.removeObservers(viewLifecycleOwner)

            allEvent.clear()
            activityDataList.clear()
            myCallData.clear()
            teamCallData.clear()
            myTimeTrackingData.clear()
            teamTimeTrackingData.clear()

            var todayHasActivity = 0
            val engineerId = SharedPreferenceData.getString(requireContext(), 8, "")
            val engineerName = SharedPreferenceData.getString(requireContext(), 10, "")

            it.forEach { activityData ->
                val callId = activityData.serviceCallId
                val startDateTime = activityData.activityStartTime.split(" ")
                val endDateTime = activityData.activityEndTime.split(" ")

                if (endDateTime.size == 2 && endDateTime.size == 2) {
                    val startYear = startDateTime[0].split("-")[0]
                    val startMonth = startDateTime[0].split("-")[1]
                    val startDate = startDateTime[0].split("-")[2]
                    val finalStartDate = "$startDate-$startMonth-$startYear"

                    val endYear = endDateTime[0].split("-")[0]
                    val endMonth = endDateTime[0].split("-")[1]
                    val endDate = endDateTime[0].split("-")[2]
                    val finalEndDate = "$endDate-$endMonth-$endYear"

                    val rangeDate = getDates(finalStartDate, finalEndDate)

                    rangeDate.forEach { date ->
                        allEvent.add(date)
                        if (date == calendarFormat.format(selectedDate)) {
                            todayHasActivity += 1
                            if (activityData.userId == engineerId) {
                                activityDataList.add(activityData)
                                callObserver(callId, engineerId, true)
                            } else {
                                callObserver(callId, engineerId, false)
                            }

                        }
                    }
                }
            }

            Log.e("aim","calendar - set adapter : ${calendarFormat.format(selectedDate)}")
            activityAdapter.setList(selectedDate, engineerName, activityDataList)

            if (todayHasActivity == 0) {
                activity_warning.visibility = View.VISIBLE
            } else {
                activity_warning.visibility = View.GONE
            }

            swipe_container.isRefreshing = false
            shimmer_container.visibility = View.GONE
            custom_calendar?.setUpCalendarAdapter(calendarObject, allEvent)

        })
    }

    private fun callObserver(callId: String, engineerId: String, myCall: Boolean) {
        val observer = callVM.getByCallId(callId)
        observer.observe(viewLifecycleOwner, { callData ->
            observer.removeObservers(viewLifecycleOwner)

            if (callData.isNotEmpty()) {
                val callNumber = callData.first().serviceCallNumber

                if (myCall) {
                    val check = myCallData.filter { it.serviceCallId == callData.first().serviceCallId }
                    if (check.isEmpty()) {
                        myCallData.add(callData.first())
                        timeTrackingObserver(callId, engineerId, callNumber, myCall)
                    }
                } else {
                    val check = teamCallData.filter { it.serviceCallId == callData.first().serviceCallId }
                    if (check.isEmpty()) {
                        teamCallData.add(callData.first())
                        timeTrackingObserver(callId, engineerId, callNumber, myCall)
                    }
                }
            }

            Log.e("aim","calendar - my call data : ${myCallData.size}")
            myCallAdapter.setList(myCallData, true)
            if (activityAdapter.itemCount == 0 && myCallData.isEmpty()) {
                activity_warning_my.visibility = View.VISIBLE
                timetracking_container_my.visibility = View.GONE
            } else if (activityAdapter.itemCount == 0 && myCallData.isNotEmpty()) {
                activity_warning_my.visibility = View.GONE
                timetracking_container_my.visibility = View.VISIBLE
            } else if (activityAdapter.itemCount > 0 && myCallData.isEmpty()) {
                activity_warning_my.visibility = View.GONE
                timetracking_container_my.visibility = View.GONE
            } else if (activityAdapter.itemCount > 0 && myCallData.isNotEmpty()) {
                activity_warning_my.visibility = View.GONE
                timetracking_container_my.visibility = View.VISIBLE
            }

            teamCallAdapter.setList(teamCallData)
            if (teamCallData.isEmpty()) {
                activity_warning_team.visibility = View.VISIBLE
                timetracking_container_team.visibility = View.GONE
            } else {
                activity_warning_team.visibility = View.GONE
                timetracking_container_team.visibility = View.VISIBLE
            }

        })
    }

    private fun timeTrackingObserver(callId: String, engineerId: String, callNumber: String, myCall: Boolean) {
        val observer = timeTrackingVM.getByCallId(callId)
        observer.observe(viewLifecycleOwner, { timeTrackingData ->
            observer.removeObservers(viewLifecycleOwner)

            if (timeTrackingData.isNotEmpty()) {
                if (myCall) {
                    val check = myTimeTrackingData.filter { it.timeTrackingId == timeTrackingData.first().timeTrackingId }
                    if (check.isEmpty()) {
                        myTimeTrackingData.add(timeTrackingData.first())
                    }
                } else {
                    val check = teamTimeTrackingData.filter { it.timeTrackingId == timeTrackingData.first().timeTrackingId }
                    if (check.isEmpty()) {
                        teamTimeTrackingData.add(timeTrackingData.first())
                    }
                }
            }

            myTimeTrackingAdapter.setList(myTimeTrackingData, engineerId, callNumber, false)
            if (myTimeTrackingData.isEmpty()) {
                timetracking_container_my.visibility = View.GONE
            }

            teamTimeTrackingAdapter.setList(teamTimeTrackingData, engineerId, callNumber, false)
            if (teamTimeTrackingData.isEmpty()) {
                timetracking_container_team.visibility = View.GONE
            }

        })
    }

    private fun getDates(dateString1: String,dateString2: String): List<String> {
        val dates = ArrayList<String>()
        val date1: Date?
        val date2: Date?
        try {
            date1 = calendarFormat.parse(dateString1)
            date2 = calendarFormat.parse(dateString2)

            val cal1: Calendar = Calendar.getInstance()
            cal1.time = date1!!
            val cal2: Calendar = Calendar.getInstance()
            cal2.time = date2!!
            while (!cal1.after(cal2)) {
                dates.add(calendarFormat.format(cal1.time))
                cal1.add(Calendar.DATE, 1)
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
        return dates
    }

}
