package com.syspex.ui

import android.annotation.SuppressLint
import android.app.ProgressDialog
import android.graphics.Bitmap
import android.graphics.Canvas
import android.graphics.Color
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Environment
import android.provider.OpenableColumns
import android.util.Log
import android.view.View
import android.widget.*
import androidx.core.content.FileProvider
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.livinglifetechway.quickpermissions_kotlin.runWithPermissions
import com.syspex.R
import com.syspex.data.local.SharedPreferenceData
import com.syspex.data.local.database.account_contact.AccountContactEntity
import com.syspex.data.local.database.account_contact.AccountContactViewModel
import com.syspex.data.local.database.service_call.ServiceCallViewModel
import com.syspex.data.local.database.service_call_completion.ServiceCallCompletionEntity
import com.syspex.data.local.database.service_call_completion.ServiceCallCompletionViewModel
import com.syspex.data.local.database.service_call_equipment.ServiceCallEquipmentViewModel
import com.syspex.data.local.database.service_call_handover.ServiceCallHandoverEntity
import com.syspex.data.local.database.service_call_handover.ServiceCallHandoverViewModel
import com.syspex.data.model.AccessoriesModel
import com.syspex.data.model.ConverterImageModel
import com.syspex.data.model.OutstandingModel
import com.syspex.data.remote.GetDataRequest
import com.syspex.data.remote.HandoverAddRequest
import com.syspex.data.retrofit.ApiClient
import com.syspex.ui.adapter.AccessoriesAdapter
import com.syspex.ui.adapter.HandoverEquipmentAdapter
import com.syspex.ui.adapter.OutstandingAdapter
import com.syspex.ui.custom.AutoTextContactAdapter
import com.syspex.ui.custom.AutoTextEngineerAdapter
import com.syspex.ui.helper.FunHelper
import kotlinx.android.synthetic.main.handover_add_activity.*
import kotlinx.android.synthetic.main.handover_add_activity.btn_save
import kotlinx.android.synthetic.main.handover_add_activity.customer_name_err
import kotlinx.android.synthetic.main.handover_add_activity.customer_name_value
import kotlinx.android.synthetic.main.handover_add_activity.customer_signature_clear
import kotlinx.android.synthetic.main.handover_add_activity.customer_signature_edit
import kotlinx.android.synthetic.main.handover_add_activity.customer_signature_lay
import kotlinx.android.synthetic.main.handover_add_activity.customer_signature_pad
import kotlinx.android.synthetic.main.handover_add_activity.customer_signature_preview
import kotlinx.android.synthetic.main.handover_add_activity.customer_signature_priview_image
import kotlinx.android.synthetic.main.handover_add_activity.customer_signature_save
import kotlinx.android.synthetic.main.handover_add_activity.customer_value
import kotlinx.android.synthetic.main.handover_add_activity.desc_err
import kotlinx.android.synthetic.main.handover_add_activity.desc_value
import kotlinx.android.synthetic.main.handover_add_activity.location_value
import kotlinx.android.synthetic.main.sub_header.*
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import org.json.JSONArray
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.File
import java.io.FileOutputStream
import java.io.IOException
import java.io.OutputStream
import java.text.DecimalFormat
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList

class HandoverAdd : AppCompatActivity() {

    private lateinit var callVM: ServiceCallViewModel
    private lateinit var handoverVM: ServiceCallHandoverViewModel
    private lateinit var completionVM: ServiceCallCompletionViewModel
    private lateinit var equipmentVM: ServiceCallEquipmentViewModel
    private lateinit var contactVM: AccountContactViewModel

    private val equipmentAdapter = HandoverEquipmentAdapter()
    private val accessoriesAdapter = AccessoriesAdapter()
    private val outstandingAdapter = OutstandingAdapter()

    private val serverFormat = SimpleDateFormat("yyyy-MM-dd", Locale.getDefault())
    private val uiFormat = SimpleDateFormat("dd MMM yyyy", Locale.getDefault())
    private var startDate: Date? = null
    private var endDate: Date? = null // unused
    private var completionDate: Date? = null
    private var outstandingDate: Date? = null
    private var customerSignatureName = ""
    private var customerSignatureFile: File? = null
    private var equipmentData = ArrayList<String>()

    private var section = ""
    private var callId = ""
    private var accountId = ""
    private var accountName = ""
    private var addressId = ""
    private var addressText = ""
    private var engineerId = ""
    private var engineerName = ""
    private var contactId = ""
    private var contactName = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.handover_add_activity)

        callVM = ViewModelProviders.of(this).get(ServiceCallViewModel::class.java)
        handoverVM = ViewModelProviders.of(this).get(ServiceCallHandoverViewModel::class.java)
        completionVM = ViewModelProviders.of(this).get(ServiceCallCompletionViewModel::class.java)
        equipmentVM = ViewModelProviders.of(this).get(ServiceCallEquipmentViewModel::class.java)
        contactVM = ViewModelProviders.of(this).get(AccountContactViewModel::class.java)

        generateView()
        buttonListener()

        setupAdapter()
        equipmentAdapterListener()
        accessoriesAdapterListener()
        outstandingAdapterListener()

        equipmentObserver()
        engineerObserver()
        accountContactObserver()

    }

    @SuppressLint("SetTextI18n")
    private fun generateView() {
        if (intent.hasExtra("section")) {
            section = intent.getStringExtra("section") ?: ""
            callId = intent.getStringExtra("callId") ?: ""
            accountId = intent.getStringExtra("accountId") ?: ""
            accountName = intent.getStringExtra("accountName") ?: ""
            addressId = intent.getStringExtra("addressId") ?: ""
            addressText = intent.getStringExtra("addressText") ?: ""
            engineerId = intent.getStringExtra("engineerId") ?: ""
            engineerName = intent.getStringExtra("engineerName") ?: ""
            contactId = intent.getStringExtra("picId") ?: ""
            contactName = intent.getStringExtra("picName") ?: ""

            customer_value.text = accountName
            location_value.setText(addressText)

            contact_value.setText(contactName)
            contact_value.dismissDropDown()

            customer_name_value.setText(contactName)
            customer_name_value.dismissDropDown()

            po_value.setText(intent.getStringExtra("casePo"))
            po_value.dismissDropDown()

            install_by_value.setText(engineerName)
            install_by_value.dismissDropDown()

            if (section == "handover") {
                header_name.text = "Add Handover"
            } else {
                header_name.text = "Add Installation Completion Letter"
            }
        }
    }

    private fun buttonListener() {
        back_link.setOnClickListener {
            onBackPressed()
        }

        date_start_value.setOnClickListener {
            dialogDate("dateStart", date_start_value)
        }

        date_end_value.setOnClickListener {
            dialogDate("dateEnd", date_end_value)
        }

        completion_date_value.setOnClickListener {
            dialogDate("completionDate", completion_date_value)
        }

        btn_add_accessories.setOnClickListener {
            dialogAccessories()
        }

        btn_add_outstanding.setOnClickListener {
            dialogOutstanding()
        }

        // ========================================================================================= Signature
        customer_signature_edit.setOnClickListener {
            customer_signature_preview.visibility = View.GONE
            customer_signature_lay.visibility = View.VISIBLE
        }

        customer_signature_clear.setOnClickListener {
            customer_signature_pad.clear()
        }

        customer_signature_save.setOnClickListener {
            saveSignatureImage()
            customer_signature_preview.visibility = View.VISIBLE
            customer_signature_lay.visibility = View.GONE
        }

        btn_save.setOnClickListener {
            if (checkValue()) {
                sendHandoverForm()
            } else {
                FunHelper.snackBar(it, "Fill all required field")
            }
        }
    }

    private fun setupAdapter() {
        recycler_equipment.layoutManager = LinearLayoutManager(this, RecyclerView.VERTICAL, false)
        recycler_equipment.adapter = equipmentAdapter

        recycler_accessories.layoutManager = LinearLayoutManager(this, RecyclerView.VERTICAL, false)
        recycler_accessories.adapter = accessoriesAdapter

        recycler_outstanding.layoutManager = LinearLayoutManager(this, RecyclerView.VERTICAL, false)
        recycler_outstanding.adapter = outstandingAdapter
    }

    private fun equipmentAdapterListener() {
        equipmentAdapter.setEventHandler(object : HandoverEquipmentAdapter.RecyclerClickListener {
            override fun isAdd(equipmentId: String) {
                equipmentData.add(equipmentId)
            }

            override fun isRemove(equipmentId: String) {
                equipmentData.remove(equipmentId)
            }

        })
    }

    private fun accessoriesAdapterListener() {
        accessoriesAdapter.registerAdapterDataObserver(object : RecyclerView.AdapterDataObserver() {
            override fun onChanged() {
                if (accessoriesAdapter.getAllItem().isEmpty()) {
                    warning_accessories.visibility = View.VISIBLE
                    recycler_accessories.visibility = View.GONE
                } else {
                    warning_accessories.visibility = View.GONE
                    recycler_accessories.visibility = View.VISIBLE
                }
            }
        })
    }

    private fun outstandingAdapterListener() {
        outstandingAdapter.registerAdapterDataObserver(object : RecyclerView.AdapterDataObserver() {
            override fun onChanged() {
                if (outstandingAdapter.getAllItem().isEmpty()) {
                    warning_outstanding.visibility = View.VISIBLE
                    recycler_outstanding.visibility = View.GONE
                } else {
                    warning_outstanding.visibility = View.GONE
                    recycler_outstanding.visibility = View.VISIBLE
                }
            }
        })
    }

    private fun dialogDate (dateTimeStatus: String, mTextView: TextView?) {
        val view = layoutInflater.inflate(R.layout.date_time_picker_activity,null)
        val dialog = BottomSheetDialog(this)
        dialog.setContentView(view)

        val bottomSheetBehavior = BottomSheetBehavior.from<View>(view.parent as View)
        dialog.setOnShowListener { bottomSheetBehavior.state = BottomSheetBehavior.STATE_EXPANDED }

        val txTitle = dialog.findViewById<TextView>(R.id.tx_title)
        val timeValue = dialog.findViewById<TimePicker>(R.id.time_value)
        val dateValue = dialog.findViewById<CalendarView>(R.id.date_value)
        val btnSet = dialog.findViewById<TextView>(R.id.btn_set)
        txTitle?.text = "Choose Date"

        var dateSelected = ""

        when(dateTimeStatus) {
            "dateStart" -> timeValue?.visibility = View.GONE
            "dateEnd" -> timeValue?.visibility = View.GONE
            "completionDate" -> timeValue?.visibility = View.GONE
            "outstandingDate" -> timeValue?.visibility = View.GONE
        }

        dateValue?.setOnDateChangeListener{ calendarView: CalendarView, year: Int, month: Int, date: Int ->
            dateSelected = "$year-${DecimalFormat("00").format(month+1)}-${DecimalFormat("00").format(date)}"
        }

        btnSet?.setOnClickListener {
            val nowServer = serverFormat.format(Date())
            val nowUi = uiFormat.format(Date())

            val data = FunHelper.uiDateFormat(dateSelected).ifEmpty { nowUi }
            when (dateTimeStatus) {
                "dateStart" -> {
                    startDate =
                        serverFormat.parse(dateSelected.ifEmpty { nowServer })
                    mTextView?.text = data
                }
                "dateEnd" -> {
                    endDate =
                        serverFormat.parse(dateSelected.ifEmpty { nowServer })
                    mTextView?.text = data
                }
                "completionDate" -> {
                    completionDate =
                        serverFormat.parse(dateSelected.ifEmpty { nowServer })
                    mTextView?.text = data
                }
                "outstandingDate" -> {
                    outstandingDate =
                        serverFormat.parse(dateSelected.ifEmpty { nowServer })
                    mTextView?.text = data
                }
            }

            dialog.dismiss()

        }

        dialog.show()
    }

    @SuppressLint("SetTextI18n")
    private fun dialogAccessories() {
        val view = layoutInflater.inflate(R.layout.dialog_accessories_add, null)
        val dialog = BottomSheetDialog(this)
        dialog.setContentView(view)
        val bottomSheetBehavior = BottomSheetBehavior.from<View>(view.parent as View)
        dialog.setOnShowListener { bottomSheetBehavior.state = BottomSheetBehavior.STATE_EXPANDED }

        val brandErr = dialog.findViewById<TextView>(R.id.brand_err)
        val brandValue = dialog.findViewById<EditText>(R.id.brand_value)

        val qtyValue = dialog.findViewById<TextView>(R.id.qty_value)
        val btnPlus = dialog.findViewById<ImageView>(R.id.btn_plus)
        val btnMinus = dialog.findViewById<ImageView>(R.id.btn_minus)

        val descErr = dialog.findViewById<TextView>(R.id.desc_err)
        val descValue = dialog.findViewById<EditText>(R.id.desc_value)

        val btnSave = dialog.findViewById<TextView>(R.id.btn_ok)

        var qty = 1

        btnPlus?.setOnClickListener {
            qty++
            qtyValue?.text = qty.toString()
        }

        btnMinus?.setOnClickListener {
            if (qty > 1) {
                qty--
                qtyValue?.text = qty.toString()
            }
        }

        btnSave?.setOnClickListener {
            if (!brandValue?.text.isNullOrEmpty() && !descValue?.text.isNullOrEmpty()) {
                val data = AccessoriesModel(
                    brandValue?.text.toString() ?: "",
                    descValue?.text.toString() ?: "",
                    qtyValue?.text.toString() ?: "",
                )
                accessoriesAdapter.addList(data)

                dialog.dismiss()
            } else {
                brandErr?.text = "*Required"
                descErr?.text = "*Required"
            }
        }

        dialog.show()

    }

    @SuppressLint("SetTextI18n")
    private fun dialogOutstanding() {
        val view = layoutInflater.inflate(R.layout.dialog_outstanding_add, null)
        val dialog = BottomSheetDialog(this)
        dialog.setContentView(view)
        val bottomSheetBehavior = BottomSheetBehavior.from<View>(view.parent as View)
        dialog.setOnShowListener { bottomSheetBehavior.state = BottomSheetBehavior.STATE_EXPANDED }

        val thingsErr = dialog.findViewById<TextView>(R.id.things_err)
        val thingsValue = dialog.findViewById<EditText>(R.id.things_value)

        val followErr = dialog.findViewById<TextView>(R.id.follow_err)
        val followValue = dialog.findViewById<TextView>(R.id.follow_value)

        val dateErr = dialog.findViewById<TextView>(R.id.date_err)
        val dateValue = dialog.findViewById<TextView>(R.id.date_value)

        val btnSave = dialog.findViewById<TextView>(R.id.btn_ok)

        dateValue?.setOnClickListener {
            dialogDate("outstandingDate", dateValue)
        }

        btnSave?.setOnClickListener {
            if (!thingsValue?.text.isNullOrEmpty() && !followValue?.text.isNullOrEmpty() && !dateValue?.text.isNullOrEmpty()) {
                val data = OutstandingModel(
                    thingsValue?.text.toString() ?: "",
                    followValue?.text.toString() ?: "",
                    serverFormat.format(outstandingDate!!) ?: "",
                )
                outstandingAdapter.addList(data)

                dialog.dismiss()
            } else {
                thingsErr?.text = "*Required"
                followErr?.text = "*Required"
                dateErr?.text = "*Required"
            }
        }

        dialog.show()

    }

    private fun equipmentObserver() {
        equipmentVM.getAttachToCall(callId, true).observe(this, {
            if (it.isNotEmpty()) {
                recycler_equipment.visibility = View.VISIBLE
                warning_equipment.visibility = View.GONE

                equipmentAdapter.setList(it)
            } else {
                recycler_equipment.visibility = View.GONE
                warning_equipment.visibility = View.VISIBLE
            }
        })
    }

    private fun engineerObserver() {
        val engineerList = ArrayList<GetDataRequest.Engineer>()
        callVM.getByCallId(callId).observe(this, {
            if (it.isNotEmpty()) {
                val engineers = it.first().engineer
                engineers?.forEach { engineer ->
                    if (engineer != null) {
                        engineerList.add(engineer)
                    }
                }
                autoTextEngineer(engineerList)
            }
        })
    }

    private fun autoTextEngineer(engineerList: ArrayList<GetDataRequest.Engineer>) {
        val adapter = AutoTextEngineerAdapter(this, R.layout.item_spinner_black, engineerList)
        contact_value.setAdapter(adapter)
        contact_value.threshold = 1
        contact_value.onItemClickListener = AdapterView.OnItemClickListener{ parent, view, position, id->
            val engineerData = parent.getItemAtPosition(position) as GetDataRequest.Engineer
            engineerId = engineerData.id ?: ""
            contact_value.setText(engineerData.userFullname ?: "")
        }
        contact_value.onFocusChangeListener = View.OnFocusChangeListener{ view, b ->
            if(b && contact_value.text.isEmpty()) { contact_value.showDropDown() }
        }
    }

    private fun accountContactObserver() {
        val contactList = ArrayList<AccountContactEntity>()
        contactVM.getByAccountId(accountId).observe(this, {
            if (it.isNotEmpty()) {
                it.forEach { contact ->
                    contactList.add(contact)
                }
                autoTextCustomer(contactList)
            }
        })
    }

    private fun autoTextCustomer(contactList: ArrayList<AccountContactEntity>) {
        val adapter = AutoTextContactAdapter(this, R.layout.item_spinner_black, contactList)

        contact_value.setAdapter(adapter)
        contact_value.threshold = 1
        contact_value.onItemClickListener = AdapterView.OnItemClickListener{ parent, view, position, id->
            val contactData = parent.getItemAtPosition(position) as AccountContactEntity
            contactId = contactData.account_contact_id
            contact_value.setText(contactData.contact_person_name ?: "")

        }
        contact_value.onFocusChangeListener = View.OnFocusChangeListener{ view, b ->
            if(b && contact_value.text.isEmpty()) { contact_value.showDropDown() }
        }

        customer_name_value.setAdapter(adapter)
        customer_name_value.threshold = 1
        customer_name_value.onItemClickListener = AdapterView.OnItemClickListener{ parent, view, position, id->
            val contactData = parent.getItemAtPosition(position) as AccountContactEntity
            contactId = contactData.account_contact_id
            customer_name_value.setText(contactData.contact_person_name ?: "")

        }
        customer_name_value.onFocusChangeListener = View.OnFocusChangeListener{ view, b ->
            if(b && customer_name_value.text.isEmpty()) { customer_name_value.showDropDown() }
        }
    }

    private fun checkValue(): Boolean {
        var cek = 0

        if(desc_value.text.toString().isEmpty()){
            desc_err.text = "*Required"; cek++
        } else { desc_err.text = "" }

        // Installation
        if(date_start_value.text.toString().isEmpty()){
            date_start_err.text = "*Required"; cek++
        } else { date_start_err.text = "" }

//        if(date_end_value.text.toString().isEmpty()){
//            date_end_err.text = "*Required"; cek++
//        } else { date_end_err.text = "" }

        if(install_by_value.text.toString().isEmpty()){
            install_by_err.text = "*Required"; cek++
        } else { install_by_err.text = "" }

        // Supply Signature
//        if(supply_name_value.text.toString().isEmpty()){
//            supply_name_err.text = "*Required"; cek++
//        } else { supply_name_err.text = "" }
//
//        if (supply_pos_value.text.toString().isEmpty()) {
//            supply_pos_err.text = "*Required"; cek++
//        } else { supply_pos_err.text = "" }

        // Customer Signature
        if(customer_name_value.text.toString().isEmpty()){
            customer_name_err.text = "*Required"; cek++
        } else { customer_name_err.text = "" }

        if (cek == 0){
            return true
        }
        return false
    }

    private fun saveSignatureImage() = runWithPermissions(
        FunHelper.storageWritePermission,
        FunHelper.storageReadPermission
    ) {

        val signatureBitmap = customer_signature_pad.signatureBitmap

        if (addJpgSignatureToGallery(signatureBitmap)) {
            customer_signature_pad.clear()

            Toast.makeText(this, "Signature saved", Toast.LENGTH_SHORT).show()
        } else {
            Toast.makeText(this, "Signature save failed", Toast.LENGTH_SHORT).show()
        }
    }

    private fun addJpgSignatureToGallery(signature: Bitmap?): Boolean {
        var result = false
        try {

            val dataUri = ArrayList<ConverterImageModel>()
            customerSignatureFile = File(
                this.getExternalFilesDir(Environment.DIRECTORY_PICTURES),
                String.format("SRSIGN-%d.jpg", System.currentTimeMillis())
            )
            saveBitmapToJPG(signature!!, customerSignatureFile)

            val uri = FileProvider.getUriForFile(
                this,
                "com.syspex.fileprovider",
                customerSignatureFile!!
            )

            customerSignatureName = getFileName(uri)
            dataUri.add(ConverterImageModel(customerSignatureName, uri.toString()))

            try { Glide.with(this).load(uri.toString()).into(customer_signature_priview_image) }
            catch (e: Exception) { }

            result = true
        } catch (e: IOException) {
            e.printStackTrace()
        }
        return result
    }

    private fun getFileName(uri: Uri): String {
        var result: String? = null
        if (uri.scheme == "content") {
            val cursor = this.contentResolver?.query(uri, null, null, null, null)
            try {
                if (cursor != null && cursor.moveToFirst()) {
                    result = cursor.getString(cursor.getColumnIndex(OpenableColumns.DISPLAY_NAME))
                }
            } catch (e: Exception) { }
            cursor?.close()
        }
        if (result == null) {
            result = uri.path
            val cut = result!!.lastIndexOf('/')
            if (cut != -1) {
                result = result.substring(cut + 1)
            }
        }
        Log.e("aim", "signature name: $result")
        return result
    }

    @Throws(IOException::class)
    fun saveBitmapToJPG(bitmap: Bitmap, photo: File?) {
        try {
            val newBitmap = Bitmap.createBitmap(
                bitmap.width,
                bitmap.height,
                Bitmap.Config.ARGB_8888
            )
            val canvas = Canvas(newBitmap)
            canvas.drawColor(Color.WHITE)
            canvas.drawBitmap(bitmap, 0f, 0f, null)
            val stream: OutputStream = FileOutputStream(photo!!)
            newBitmap.compress(Bitmap.CompressFormat.JPEG, 80, stream)
            stream.close()
        } catch (e: Exception) { Log.e("aim", e.toString()) }
    }

    private fun sendHandoverForm() {
        val progressDialog = ProgressDialog(this)
        progressDialog.setMessage("Send $section form ...")
        progressDialog.setCancelable(false)
        progressDialog.show()

        val token = "Bearer "+ SharedPreferenceData.getString(this, 2, "")
        val userId = SharedPreferenceData.getString(this, 8, "")

        val accessoriesArray = JSONArray()
        accessoriesAdapter.getAllItem().forEach {
            val accessoriesObject = JSONObject()
            accessoriesObject.put("product_number", it.productNumber)
            accessoriesObject.put("product_description", it.description)
            accessoriesObject.put("qty", it.quantity)

            accessoriesArray.put(accessoriesObject)
        }

        val outstandingArray = JSONArray()
        outstandingAdapter.getAllItem().forEach {
            val outstandingObject = JSONObject()
            outstandingObject.put("thing_description", it.things)
            outstandingObject.put("followed_by", it.follow)
            outstandingObject.put("target_date", it.date)

            outstandingArray.put(outstandingObject)
        }

        val equipmentArray = JSONArray()
        equipmentData.forEach {
            equipmentArray.put(it)
        }

        val params = HashMap<String, RequestBody>()
        params["call_id"] = toRequestBody(callId)
        params["address_id"] = toRequestBody(addressId)
        params["account_contact_id"] = toRequestBody(contactId)
        params["engineer_id"] = toRequestBody(engineerId)
        params["start_date"] = toRequestBody(serverFormat.format(startDate!!))
//        params["end_date"] = toRequestBody(serverFormat.format(endDate!!))
        params["completion_date"] = toRequestBody(serverFormat.format(completionDate!!))
        params["po_ref_number"] = toRequestBody(po_value.text.toString())
//        params["customer_agreed"] = toRequestBody(agree_value.text.toString())
        params["outstanding_matter"] = toRequestBody(outstandingArray.toString())
//        params["supplier_pic_name"] = toRequestBody(supply_name_value.text.toString())
//        params["supplier_pic_position"] = toRequestBody(supply_pos_value.text.toString())
        params["customer_pic_name"] = toRequestBody(customer_name_value.text.toString())
//        params["customer_pic_position"] = toRequestBody(customer_pos_value.text.toString())
        params["equipments"] = toRequestBody(equipmentArray.toString())
        params["accessories"] = toRequestBody(accessoriesArray.toString())
        params["type"] = toRequestBody(section)

//        val signature1: RequestBody = RequestBody.create(MediaType.parse("image/*"), supplySignatureFile)
//        params["supplier_signature_url\"; filename=\"${supplySignatureFile.name}"] = signature1

        if (customerSignatureFile != null) {
            val signature2: RequestBody =
                RequestBody.create(MediaType.parse("image/*"), customerSignatureFile)
            params["customer_signature_url\"; filename=\"${customerSignatureFile!!.name}"] =
                signature2
        }
        Log.e("aim","params: $params")

        ApiClient.instance.sendHandover(token, params).enqueue(object :Callback<HandoverAddRequest> {
            override fun onFailure(call: Call<HandoverAddRequest>, t: Throwable) {
                progressDialog.dismiss()
                Toast.makeText(this@HandoverAdd, "Send $section form failed", Toast.LENGTH_LONG)
                    .show()
            }

            override fun onResponse(call: Call<HandoverAddRequest>, response: Response<HandoverAddRequest>) {
                progressDialog.dismiss()
                if (!response.isSuccessful) {
                    if (response.code() == 401) {
                        FunHelper.clearAllData(this@HandoverAdd)
                        Toast.makeText(this@HandoverAdd, "Session end", Toast.LENGTH_LONG)
                            .show()
                    }
                    else {
                        FunHelper.firebaseApiLog(
                            userId ?: "",
                            response.raw().request().url().toString() ?: "",
                            response.raw().request().headers().toString() ?: "",
                            params.toString() ?: "",
                            response.raw().toString() ?: ""
                        )
                        Toast.makeText(this@HandoverAdd, response.raw().toString(), Toast.LENGTH_LONG).show()
                    }
                } else {
                    if (section == "handover") {
                        handoverVM.insert(ServiceCallHandoverEntity(
                            response.body()?.data?.handover?.id ?: "",
                            response.body()?.data?.handover?.callId ?: "",
                            response.body()?.data?.handover?.addressId ?: "",
                            response.body()?.data?.handover?.accountContactId ?: "",
                            response.body()?.data?.handover?.engineerId ?: "",
                            response.body()?.data?.handover?.handoverNumber ?: "",
                            response.body()?.data?.handover?.handoverType ?: "",
                            response.body()?.data?.handover?.handoverPoRefNumber ?: "",
                            response.body()?.data?.handover?.handoverCustomerAgreed ?: "",
                            "",
                            response.body()?.data?.handover?.startDate ?: "",
                            response.body()?.data?.handover?.endDate ?: "",
                            response.body()?.data?.handover?.completionDate ?: "",
                            response.body()?.data?.handover?.supplierPicName ?: "",
                            response.body()?.data?.handover?.supplierPicPosition ?: "",
                            response.body()?.data?.handover?.supplierSignatureUrl ?: "",
                            response.body()?.data?.handover?.customerPicName ?: "",
                            response.body()?.data?.handover?.customerPicPosition ?: "",
                            response.body()?.data?.handover?.customerSignatureUrl ?: "",
                            response.body()?.data?.handover?.createdDate ?: "",
                            response.body()?.data?.handover?.createdBy ?: "",
                            response.body()?.data?.handover?.lastModifiedDate ?: "",
                            response.body()?.data?.handover?.lastModifiedBy ?: ""
                        ))
                    } else {
                        completionVM.insert(ServiceCallCompletionEntity(
                            response.body()?.data?.handover?.id ?: "",
                            response.body()?.data?.handover?.callId ?: "",
                            response.body()?.data?.handover?.addressId ?: "",
                            response.body()?.data?.handover?.accountContactId ?: "",
                            response.body()?.data?.handover?.engineerId ?: "",
                            response.body()?.data?.handover?.handoverNumber ?: "",
                            response.body()?.data?.handover?.handoverType ?: "",
                            response.body()?.data?.handover?.handoverPoRefNumber ?: "",
                            response.body()?.data?.handover?.handoverCustomerAgreed ?: "",
                            "",
                            response.body()?.data?.handover?.startDate ?: "",
                            response.body()?.data?.handover?.endDate ?: "",
                            response.body()?.data?.handover?.completionDate ?: "",
                            response.body()?.data?.handover?.supplierPicName ?: "",
                            response.body()?.data?.handover?.supplierPicPosition ?: "",
                            response.body()?.data?.handover?.supplierSignatureUrl ?: "",
                            response.body()?.data?.handover?.customerPicName ?: "",
                            response.body()?.data?.handover?.customerPicPosition ?: "",
                            response.body()?.data?.handover?.customerSignatureUrl ?: "",
                            response.body()?.data?.handover?.createdDate ?: "",
                            response.body()?.data?.handover?.createdBy ?: "",
                            response.body()?.data?.handover?.lastModifiedDate ?: "",
                            response.body()?.data?.handover?.lastModifiedBy ?: ""
                        ))
                    }

                    Toast.makeText(this@HandoverAdd, "$section form sent", Toast.LENGTH_LONG).show()
                    onBackPressed()
                }
            }
        })
    }

    private fun toRequestBody(value: String): RequestBody {
        return RequestBody.create(MultipartBody.FORM, value)
    }

}