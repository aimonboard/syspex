package com.syspex.ui

import android.content.Intent
import androidx.lifecycle.ViewModelProviders
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.GravityCompat
import androidx.drawerlayout.widget.DrawerLayout
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.syspex.Menu2
import com.syspex.R
import com.syspex.data.local.database.account.AccountViewModel
import com.syspex.ui.adapter.AccountAdapter
import com.syspex.ui.helper.KeyboardHelper
import kotlinx.android.synthetic.main.account_fragment.*
import kotlinx.android.synthetic.main.account_fragment.btn_menu
import kotlinx.android.synthetic.main.account_fragment.btn_search
import kotlinx.android.synthetic.main.account_fragment.swipe_container
import kotlinx.android.synthetic.main.shimmer_layout.*

class Account : Fragment() {
    private lateinit var accountVM: AccountViewModel
    private val adapter = AccountAdapter()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.account_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        accountVM = ViewModelProviders.of(this).get(AccountViewModel::class.java)

        buttonListener()

        setupAdapter()
        searchListener()
        recyclerScrollListener()
        syncObserver()

    }

    private fun buttonListener() {
        val drawerLayout: DrawerLayout = requireActivity().findViewById(R.id.drawer_layout)
        btn_menu?.setOnClickListener {
            if(!drawerLayout.isDrawerOpen(GravityCompat.START))
                drawerLayout.openDrawer(GravityCompat.START)
        }

        btn_search.setOnClickListener {
            if (header_name.visibility == View.VISIBLE) {
                header_name.visibility = View.GONE
                search_lay.visibility = View.VISIBLE
                btn_search.setImageResource(R.drawable.ic_close)

                KeyboardHelper.openSoftKeyboard(requireContext(),search_value)
            } else {
                header_name.visibility = View.VISIBLE
                search_lay.visibility = View.GONE
                btn_search.setImageResource(R.drawable.ic_search_small)

                KeyboardHelper.hideSoftKeyboard(requireContext(),search_value)

                search_value.setText("")
            }
        }

        btn_search_online.setOnClickListener {
            val i = Intent(requireContext(), SearchAccountOnline::class.java)
            startActivity(i)
        }

        swipe_container.setOnRefreshListener {
            syncObserver()
        }
    }

    private fun setupAdapter() {
        recycler_account?.layoutManager = LinearLayoutManager(context, RecyclerView.VERTICAL, false)
        recycler_account?.adapter = adapter
    }

    private fun searchListener() {
        search_value.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) { }
            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) { }

            override fun afterTextChanged(p0: Editable?) {
                if (p0 != null) {
                    syncObserver()
                }
            }
        })
    }

    private fun recyclerScrollListener() {
        recycler_account?.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                val linearLayoutManager = recyclerView.layoutManager as LinearLayoutManager
                swipe_container.isEnabled = linearLayoutManager.findFirstCompletelyVisibleItemPosition() == 0
            }
        })
    }

    private fun syncObserver(){
        Menu2.isLoading.observe(viewLifecycleOwner, {
            Log.e("aim", "sync proses $it")
            if (it == false) {
                accountObserver()
            }
        })
    }

    private fun accountObserver() {
        shimmer_container.visibility = View.VISIBLE
        val accountObserver = accountVM.getList(search_value.text.toString())
        accountObserver.observe(viewLifecycleOwner, {
            accountObserver.removeObservers(viewLifecycleOwner)

            swipe_container.isRefreshing = false
            shimmer_container.visibility = View.GONE
            adapter.setList(it)
        })
    }

}
