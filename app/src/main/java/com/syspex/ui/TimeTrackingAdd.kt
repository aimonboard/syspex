package com.syspex.ui

import android.annotation.SuppressLint
import android.app.ProgressDialog
import android.content.Context
import android.content.res.Resources
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.widget.*
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.syspex.R
import com.syspex.data.local.SharedPreferenceData
import com.syspex.data.local.database.AppDatabase
import com.syspex.data.local.database.service_report_time_tracking.ServiceReportTimeTrackingEntity
import com.syspex.ui.helper.FunHelper
import com.syspex.ui.helper.SendData
import com.syspex.ui.helper.SendDataInterface
import com.syspex.ui.helper.SendDataReport
import java.text.DecimalFormat
import java.text.SimpleDateFormat
import java.util.*

class TimeTrackingAdd(val context: Context, val callId: String, val reportId: String) {

    private val dao = AppDatabase.getInstance(context)!!.dao()
    private val mLayoutInflater = LayoutInflater.from(context)
    private var timeTrackingDate = ""

    @SuppressLint("SetTextI18n", "InflateParams")
    fun dialogFormTimeTracking(
        isEdit: Boolean,
        id: String,
        date: String,
        start: String,
        end: String
    ) {
        val view = mLayoutInflater.inflate(R.layout.dialog_time_tracking_add, null)
        val dialog = BottomSheetDialog(context)
        dialog.setContentView(view)
        val bottomSheetBehavior = BottomSheetBehavior.from<View>(view.parent as View)
        dialog.setOnShowListener { bottomSheetBehavior.state = BottomSheetBehavior.STATE_EXPANDED }

        val userName = dialog.findViewById<TextView>(R.id.username_value)
        val dateValue = dialog.findViewById<TextView>(R.id.date_value)
        val dateErr = dialog.findViewById<TextView>(R.id.date_err)
        val startValue = dialog.findViewById<TextView>(R.id.start_value)
        val startErr = dialog.findViewById<TextView>(R.id.start_err)
        val endValue = dialog.findViewById<TextView>(R.id.end_value)
        val endErr = dialog.findViewById<TextView>(R.id.end_err)
        val btnSave = dialog.findViewById<TextView>(R.id.btn_save)

        userName?.text = SharedPreferenceData.getString(view.context, 10, "")
        if (isEdit) {
            btnSave?.text = "Update Time Tracking"
            dateValue?.text = date
            startValue?.text = start
            endValue?.text = end

            try {
                var sdf = SimpleDateFormat("dd MMM yyyy", Locale.getDefault())
                val dateObjects = sdf.parse(date)
                sdf = SimpleDateFormat("yyyy-MM-dd", Locale.getDefault())
                timeTrackingDate = sdf.format(dateObjects ?: Date())
            } catch (e: Exception) { }
        }

        dateValue?.setOnClickListener {
            dialogDate(dateValue)
        }

        startValue?.setOnClickListener {
            dialogTime(startValue, "timeStart")
        }

        endValue?.setOnClickListener {
            dialogTime(endValue, "timeEnd")
        }

        btnSave?.setOnClickListener {

            Log.e("aim", "$timeTrackingDate, ${startValue?.text}, ${endValue?.text} ")

            var cek = 0
            if(dateValue?.text.toString() == ""){
                dateErr?.text = "*Required"; cek++
            } else { dateErr?.text = "" }
            if(startValue?.text.toString() == ""){
                startErr?.text = "*Required"; cek++
            } else { startErr?.text = "" }
            if(endValue?.text.toString() == ""){
                endErr?.text = "*Required"; cek++
            } else { endErr?.text = "" }

            if (cek == 0) {
                val format = SimpleDateFormat("HH:mm", Locale.getDefault())
                val startTime = format.parse(startValue?.text.toString())!!
                val endTime = format.parse(endValue?.text.toString())!!

                if (startTime.after(endTime)) {
                    Toast.makeText(
                        context,
                        "Start time must be earlier than end time",
                        Toast.LENGTH_LONG
                    ).show()
                }
                else {
                    if (isEdit) {
                        updateTimeTracking(
                            id,
                            timeTrackingDate,
                            startValue?.text.toString(),
                            endValue?.text.toString()
                        )
                    } else {
                        insertTimeTracking(
                            timeTrackingDate,
                            startValue?.text.toString(),
                            endValue?.text.toString()
                        )
                    }
                    dialog.dismiss()
                }
            }
        }

        dialog.show()
    }

    @SuppressLint("SetTextI18n", "InflateParams")
    fun dialogDate(mTextView: TextView) {
        val view = mLayoutInflater.inflate(R.layout.date_time_picker_activity, null)
        val dialog = BottomSheetDialog(context)
        dialog.setContentView(view)

        val bottomSheetBehavior = BottomSheetBehavior.from<View>(view.parent as View)
        dialog.setOnShowListener { bottomSheetBehavior.state = BottomSheetBehavior.STATE_EXPANDED }

        val txTitle = dialog.findViewById<TextView>(R.id.tx_title)
        val timeValue = dialog.findViewById<TimePicker>(R.id.time_value)
        val dateValue = dialog.findViewById<CalendarView>(R.id.date_value)
        val btnSet = dialog.findViewById<TextView>(R.id.btn_set)

        btnSet?.text = "Set Date"
        timeValue?.visibility = View.GONE
        txTitle?.text = "Date"
        val formater = DecimalFormat("00")

        dateValue?.setOnDateChangeListener{ calendarView: CalendarView, year: Int, month: Int, date: Int ->
            timeTrackingDate = "$year-${formater.format(month + 1)}-${formater.format(date)}"
        }

        btnSet?.setOnClickListener {
            if (timeTrackingDate.isEmpty()) {
                timeTrackingDate = FunHelper.getDate(true)
            }
            mTextView.text = FunHelper.uiDateFormat(timeTrackingDate)
            dialog.dismiss()
        }
        dialog.show()
    }

    @SuppressLint("SetTextI18n", "InflateParams")
    fun dialogTime(mTextView: TextView, dateTimeStatus: String) {
        val view = mLayoutInflater.inflate(R.layout.date_time_picker_activity, null)
        val dialog = BottomSheetDialog(context)
        dialog.setContentView(view)

        val bottomSheetBehavior = BottomSheetBehavior.from<View>(view.parent as View)
        dialog.setOnShowListener { bottomSheetBehavior.state = BottomSheetBehavior.STATE_EXPANDED }

        val txTitle = dialog.findViewById<TextView>(R.id.tx_title)
        val timeValue = dialog.findViewById<TimePicker>(R.id.time_value)
        val dateValue = dialog.findViewById<CalendarView>(R.id.date_value)
        val btnSet = dialog.findViewById<TextView>(R.id.btn_set)

        btnSet?.text = "Set Time"
        var timeSelected = ""

        when(dateTimeStatus) {
            "timeStart" -> {
                dateValue?.visibility = View.GONE
                txTitle?.text = "Time Start"
            }
            "timeEnd" -> {
                dateValue?.visibility = View.GONE
                txTitle?.text = "Time End"
            }
        }

        timeValue?.setIs24HourView(true)

        val interval = 15
        val formater = DecimalFormat("00")
        val numValues = 60 / interval
        val displayedValues = arrayOfNulls<String>(numValues)
        for (i in 0 until numValues) {
            displayedValues[i] = formater.format(i * interval)
        }

        var minutePicker: NumberPicker? = null
        val minuteNumberPicker = timeValue?.findViewById<NumberPicker>(
            Resources.getSystem().getIdentifier(
                "minute",
                "id",
                "android"
            )
        )
        if (minuteNumberPicker != null) {
            minutePicker = minuteNumberPicker
            minutePicker.minValue = 0
            minutePicker.maxValue = numValues - 1
            minutePicker.displayedValues = displayedValues
        }

        timeValue?.setOnTimeChangedListener{ timePicker: TimePicker, hour: Int, minute: Int ->
            if (minutePicker != null) {
                timeSelected = "${formater.format(hour)}:${formater.format(minutePicker.value * interval)}"
            }
        }

        btnSet?.setOnClickListener {
            val rightNow = Calendar.getInstance()
            val hour = rightNow[Calendar.HOUR_OF_DAY]

            if (minutePicker != null && timeSelected.isEmpty()) {
                timeSelected = "${formater.format(hour)}:${formater.format(minutePicker.value * interval)}"
            }
            mTextView.text = timeSelected
            dialog.dismiss()
        }
        dialog.show()
    }

    private fun insertTimeTracking(date: String, startTime: String, endTime: String) {
        val thread = object : Thread() {
            override fun run() {
                dao.reportTimeTrackingInsert(
                    ServiceReportTimeTrackingEntity(
                        FunHelper.randomString("report") ?: "",
                        callId ?: "",
                        reportId ?: "",
                        SharedPreferenceData.getString(context, 8, "") ?: "",
                        SharedPreferenceData.getString(context, 10, "") ?: "",
                        "0",
                        "$date $startTime",
                        "$date $endTime",
                        false,
                        true
                    )
                )

                dao.reportUpdateFlag(reportId, false)
            }
        }
        thread.start()

        //        SendData(context).getParam(false)
        sendData()
    }

    private fun updateTimeTracking(id: String, date: String, startTime: String, endTime: String) {
        val thread = object : Thread() {
            override fun run() {
                dao.reportTimeTrackingUpdate(
                    ServiceReportTimeTrackingEntity(
                        id ?: "",
                        callId ?: "",
                        reportId ?: "",
                        SharedPreferenceData.getString(context, 8, "") ?: "",
                        SharedPreferenceData.getString(context, 10, "") ?: "",
                        "0",
                        "$date $startTime",
                        "$date $endTime",
                        false,
                        true
                    )
                )

                dao.reportUpdateFlag(reportId, false)
            }
        }
        thread.start()

//        SendData(context).getParam(false)
        sendData()
    }

    private fun sendData() {
        val progressDialog = ProgressDialog(context)
        progressDialog.setMessage("Send Data ...")
        progressDialog.setCancelable(false)
        progressDialog.show()

        SendDataReport(context).getParam(callId, "call", object : SendDataInterface {
            override fun sendDataCallback(isSuccess: Boolean) {
                progressDialog.dismiss()
            }
        })
    }

}