package com.syspex.ui

import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.core.view.GravityCompat
import androidx.drawerlayout.widget.DrawerLayout
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.syspex.Menu2
import com.syspex.R
import com.syspex.data.local.SharedPreferenceData
import com.syspex.data.local.database.agreement.AgreementViewModel
import com.syspex.data.local.database.global_enum.EnumViewModel
import com.syspex.ui.adapter.AgreementAdapter
import com.syspex.ui.adapter.FilterAdapter
import com.syspex.ui.helper.KeyboardHelper
import kotlinx.android.synthetic.main.fragment_agreement.*
import kotlinx.android.synthetic.main.fragment_agreement.btn_filter
import kotlinx.android.synthetic.main.fragment_agreement.btn_menu
import kotlinx.android.synthetic.main.fragment_agreement.btn_search
import kotlinx.android.synthetic.main.fragment_agreement.header_name
import kotlinx.android.synthetic.main.fragment_agreement.search_lay
import kotlinx.android.synthetic.main.fragment_agreement.search_value
import kotlinx.android.synthetic.main.fragment_agreement.swipe_container
import kotlinx.android.synthetic.main.shimmer_layout.*

class Agreement : Fragment() {

    private lateinit var agreementVM: AgreementViewModel
    private lateinit var enumVM: EnumViewModel

    private val agreementAdapter = AgreementAdapter()
    private var filterAdapter = FilterAdapter()

    private var filterCheckedId = ArrayList<String>()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_agreement, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        agreementVM = ViewModelProviders.of(this).get(AgreementViewModel::class.java)
        enumVM = ViewModelProviders.of(this).get(EnumViewModel::class.java)

        setupAdapter()
        buttonListener()
        recyclerScrollListener()

        filterObserver()
        syncObserver()
        searchListener()
        filterListener()

    }

    private fun buttonListener() {
        val drawerLayout: DrawerLayout = requireActivity().findViewById(R.id.drawer_layout)
        btn_menu?.setOnClickListener {
            if(!drawerLayout.isDrawerOpen(GravityCompat.START))
                drawerLayout.openDrawer(GravityCompat.START)
        }

        btn_search?.setOnClickListener {
            if (header_name.visibility == View.VISIBLE) {
                header_name.visibility = View.GONE
                search_lay.visibility = View.VISIBLE
                btn_search.setImageResource(R.drawable.ic_close)

                KeyboardHelper.openSoftKeyboard(requireContext(),search_value)
            } else {
                header_name.visibility = View.VISIBLE
                search_lay.visibility = View.GONE
                btn_search.setImageResource(R.drawable.ic_search_small)

                KeyboardHelper.hideSoftKeyboard(requireContext(),search_value)

                search_value.setText("")
            }
        }

        btn_filter?.setOnClickListener {
            filterDialog()
        }

        swipe_container.setOnRefreshListener {
            syncObserver()
        }
    }

    private fun setupAdapter() {
        recycler_agreement?.layoutManager = LinearLayoutManager(activity, RecyclerView.VERTICAL, false)
        recycler_agreement?.adapter = agreementAdapter
    }

    private fun filterDialog() {
        val view = layoutInflater.inflate(R.layout.dialog_list_filter,null)
        val dialog = BottomSheetDialog(requireContext())
        dialog.setContentView(view)
        val bottomSheetBehavior = BottomSheetBehavior.from<View>(view.parent as View)
        dialog.setOnShowListener { bottomSheetBehavior.state = BottomSheetBehavior.STATE_EXPANDED }

        val recyclerFilter = dialog.findViewById<RecyclerView>(R.id.recycler_filter)
        val btnClear = dialog.findViewById<TextView>(R.id.btn_clear)

        recyclerFilter?.layoutManager = GridLayoutManager(activity,2)
        recyclerFilter?.adapter = filterAdapter
        filterAdapter.restore(filterCheckedId)

        btnClear?.setOnClickListener {
            filterCheckedId.clear()
            SharedPreferenceData.setArrayString(requireContext(),9994, emptySet())
            recyclerFilter?.layoutManager = GridLayoutManager(activity,2)
            recyclerFilter?.adapter = filterAdapter

            syncObserver()
            btn_filter.setImageResource(R.drawable.ic_filter_disable)
        }

        dialog.show()
    }

    private fun filterObserver() {
        val enumLive = enumVM.getByTableAndColumn("agreement","agreement_status")
        enumLive.observe(viewLifecycleOwner, {
            enumLive.removeObservers(viewLifecycleOwner)
            filterAdapter.clear()

            if (it.isNotEmpty()) {
                it.forEach { enumData->
                    filterAdapter.addData(enumData)
                }

                val savedFilter = restoreFilter()
                when {
                    it.size == savedFilter.size -> {
                        btn_filter.setImageResource(R.drawable.ic_filter_disable)
                    }
                    savedFilter.isEmpty() -> {
                        btn_filter.setImageResource(R.drawable.ic_filter_disable)
                    }
                    savedFilter.isNotEmpty() -> {
                        btn_filter.setImageResource(R.drawable.ic_filter_enable)
                    }
                }
            }
        })
    }

    private fun restoreFilter(): Set<String> {
        // Restore filter from shared preference
        val savedFilter = SharedPreferenceData.getArrayString(requireContext(),9994, emptySet())
        if (savedFilter.isNotEmpty()) {
            filterCheckedId.clear()
            savedFilter.forEach { restoreFilter->
                filterCheckedId.add(restoreFilter)
            }
        }
        return savedFilter
    }

    private fun filterListener() {
        filterAdapter.setEventHandler(object: FilterAdapter.RecyclerClickListener {
            override fun isChecked(checkedId: ArrayList<String>, checkedAll: Boolean) {
                filterCheckedId = checkedId
                SharedPreferenceData.setArrayString(requireContext(),9994,filterCheckedId.toSet())

                if (!checkedAll) {
                    btn_filter.setImageResource(R.drawable.ic_filter_enable)
                } else if (checkedAll) {
                    btn_filter.setImageResource(R.drawable.ic_filter_disable)
                }
                syncObserver()
            }
        })
    }

    private fun searchListener() {
        search_value.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) { }
            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) { }

            override fun afterTextChanged(p0: Editable?) {
                if (p0 != null) {
                    syncObserver()
                }
            }
        })
    }

    private fun recyclerScrollListener() {
        recycler_agreement?.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                val linearLayoutManager = recyclerView.layoutManager as LinearLayoutManager
                swipe_container.isEnabled = linearLayoutManager.findFirstCompletelyVisibleItemPosition() == 0
            }
        })
    }

    private fun syncObserver() {
        Menu2.isLoading.observe(viewLifecycleOwner, {
            Log.e("aim", "sync proses $it")
            if (it == false) {
                agreementObserver()
            }
        })
    }

    private fun agreementObserver() {
        restoreFilter()
        shimmer_container.visibility = View.VISIBLE
        val agreementObserver = agreementVM.getAll(search_value.text.toString(), filterCheckedId)
        agreementObserver.observe(viewLifecycleOwner, {
            agreementObserver.removeObservers(viewLifecycleOwner)

            swipe_container.isRefreshing = false
            shimmer_container.visibility = View.GONE
            agreementAdapter.setList(it)
        })
    }

}