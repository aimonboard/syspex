package com.syspex.ui

import android.annotation.SuppressLint
import android.app.ProgressDialog
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.widget.NestedScrollView
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.google.android.material.tabs.TabLayout
import com.livinglifetechway.quickpermissions_kotlin.runWithPermissions
import com.syspex.Menu2
import com.syspex.R
import com.syspex.data.local.SharedPreferenceData
import com.syspex.data.local.database.account_attachment.AccountAttachmentViewModel
import com.syspex.data.local.database.account_contact.AccountContactViewModel
import com.syspex.data.local.database.activity.ActivityViewModel
import com.syspex.data.local.database.agreement.AgreementViewModel
import com.syspex.data.local.database.service_call.ServiceCallViewModel
import com.syspex.data.local.database.service_case.ServiceCaseViewModel
import com.syspex.data.local.database.service_case_equipment.ServiceCaseEquipmentViewModel
import com.syspex.data.local.database.service_report.ServiceReportViewModel
import com.syspex.ui.adapter.*
import com.syspex.ui.helper.*
import kotlinx.android.synthetic.main.case_detail_activity.*
import kotlinx.android.synthetic.main.case_detail_activity.account_contact_name
import kotlinx.android.synthetic.main.case_detail_activity.account_contact_phone
import kotlinx.android.synthetic.main.case_detail_activity.account_contact_type
import kotlinx.android.synthetic.main.case_detail_activity.account_pic_contact_name
import kotlinx.android.synthetic.main.case_detail_activity.account_pic_contact_phone
import kotlinx.android.synthetic.main.case_detail_activity.account_pic_contact_type
import kotlinx.android.synthetic.main.case_detail_activity.account_sales_value
import kotlinx.android.synthetic.main.case_detail_activity.activity_count
import kotlinx.android.synthetic.main.case_detail_activity.back_link
import kotlinx.android.synthetic.main.case_detail_activity.btn_add_attachment
import kotlinx.android.synthetic.main.case_detail_activity.btn_edit_contact
import kotlinx.android.synthetic.main.case_detail_activity.btn_home
import kotlinx.android.synthetic.main.case_detail_activity.btn_location
import kotlinx.android.synthetic.main.case_detail_activity.btn_phone1
import kotlinx.android.synthetic.main.case_detail_activity.btn_phone2
import kotlinx.android.synthetic.main.case_detail_activity.case_desc_value
import kotlinx.android.synthetic.main.case_detail_activity.case_sales_value
import kotlinx.android.synthetic.main.case_detail_activity.case_status
import kotlinx.android.synthetic.main.case_detail_activity.case_subject_value
import kotlinx.android.synthetic.main.case_detail_activity.case_type_remark_value
import kotlinx.android.synthetic.main.case_detail_activity.company_value
import kotlinx.android.synthetic.main.case_detail_activity.created_by_value
import kotlinx.android.synthetic.main.case_detail_activity.created_date_value
import kotlinx.android.synthetic.main.case_detail_activity.head_activity
import kotlinx.android.synthetic.main.case_detail_activity.head_attachment
import kotlinx.android.synthetic.main.case_detail_activity.head_equipment
import kotlinx.android.synthetic.main.case_detail_activity.head_information
import kotlinx.android.synthetic.main.case_detail_activity.head_installation
import kotlinx.android.synthetic.main.case_detail_activity.head_service_report
import kotlinx.android.synthetic.main.case_detail_activity.header_name
import kotlinx.android.synthetic.main.case_detail_activity.information_lay
import kotlinx.android.synthetic.main.case_detail_activity.installation_air_supply_value
import kotlinx.android.synthetic.main.case_detail_activity.installation_customer_warranty_value
import kotlinx.android.synthetic.main.case_detail_activity.installation_delivery_value
import kotlinx.android.synthetic.main.case_detail_activity.installation_demo_value
import kotlinx.android.synthetic.main.case_detail_activity.installation_inspec_value
import kotlinx.android.synthetic.main.case_detail_activity.installation_install_value
import kotlinx.android.synthetic.main.case_detail_activity.installation_lay
import kotlinx.android.synthetic.main.case_detail_activity.installation_material_value
import kotlinx.android.synthetic.main.case_detail_activity.installation_po_value
import kotlinx.android.synthetic.main.case_detail_activity.installation_pro_value
import kotlinx.android.synthetic.main.case_detail_activity.installation_special_requirement_value
import kotlinx.android.synthetic.main.case_detail_activity.installation_supply_direct_value
import kotlinx.android.synthetic.main.case_detail_activity.installation_warranty_value
import kotlinx.android.synthetic.main.case_detail_activity.isntallation_free_value
import kotlinx.android.synthetic.main.case_detail_activity.modify_date_value
import kotlinx.android.synthetic.main.case_detail_activity.modify_value
import kotlinx.android.synthetic.main.case_detail_activity.recycler_activity
import kotlinx.android.synthetic.main.case_detail_activity.recycler_attachment
import kotlinx.android.synthetic.main.case_detail_activity.recycler_equipment
import kotlinx.android.synthetic.main.case_detail_activity.recycler_service_report
import kotlinx.android.synthetic.main.case_detail_activity.scroll_lay
import kotlinx.android.synthetic.main.case_detail_activity.sub_title
import kotlinx.android.synthetic.main.case_detail_activity.tab_lay
import kotlinx.android.synthetic.main.case_detail_activity.tx_attachment_count
import kotlinx.android.synthetic.main.case_detail_activity.tx_equipment_count
import kotlinx.android.synthetic.main.case_detail_activity.warning_activity
import kotlinx.android.synthetic.main.case_detail_activity.warning_attachment
import kotlinx.android.synthetic.main.case_detail_activity.warning_equipment
import kotlinx.android.synthetic.main.case_detail_activity.warning_installation
import kotlinx.android.synthetic.main.case_detail_activity.warning_report
import java.net.URL

class CaseDetail : AppCompatActivity() {

    private lateinit var caseVM: ServiceCaseViewModel
    private lateinit var caseEquipmentVM: ServiceCaseEquipmentViewModel
    private lateinit var accountContactVM: AccountContactViewModel
    private lateinit var attachmentVM: AccountAttachmentViewModel
    private lateinit var agreementVM: AgreementViewModel
    private lateinit var callVM: ServiceCallViewModel
    private lateinit var reportVM: ServiceReportViewModel
    private lateinit var activityVM: ActivityViewModel

    private val agreementAdapter = AgreementAdapter()
    private val serviceCallAdapter = SCMyCallAdapter()
    private val reportAdapter = SRMyReportAdapter()
    private val equipmentAdapter = EquipmentListAdapter()
    private val activityAdapter = ActivityListAdapter()
    private val attachmentAdapter = AttachmentAdapter()

    private var fcmStatus = false
    private var isScrolling = false
    private var caseId = ""
    private var accountId = ""
    private var accountName = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.case_detail_activity)
        caseVM = ViewModelProviders.of(this).get(ServiceCaseViewModel::class.java)
        caseEquipmentVM = ViewModelProviders.of(this).get(ServiceCaseEquipmentViewModel::class.java)
        accountContactVM = ViewModelProviders.of(this).get(AccountContactViewModel::class.java)
        attachmentVM = ViewModelProviders.of(this).get(AccountAttachmentViewModel::class.java)
        agreementVM = ViewModelProviders.of(this).get(AgreementViewModel::class.java)
        callVM = ViewModelProviders.of(this).get(ServiceCallViewModel::class.java)
        reportVM = ViewModelProviders.of(this).get(ServiceReportViewModel::class.java)
        activityVM = ViewModelProviders.of(this).get(ActivityViewModel::class.java)

        generateView()
        buttonListener()

        generateTab()
        scrollListener()

        setupAdapter()
        attachmentAdapterListener()
    }

    override fun onResume() {
        super.onResume()
        sendData(false)
    }

    override fun onBackPressed() {
        super.onBackPressed()
        if (fcmStatus) {
            val intent = Intent(this, Menu2::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_NEW_TASK)
            startActivity(intent)
            finish()
        }
    }

    private fun generateView() {
        when {
            // From fcm
            intent.hasExtra("case_id") -> {
                fcmStatus = true
                caseId = intent.getStringExtra("case_id") ?: ""
                sendData(true)
            }
            // From notif page
            intent.hasExtra("notif_case_id") -> {
                caseId = intent.getStringExtra("notif_case_id") ?: ""
                sendData(true)
            }
            // Normal flow
            intent.hasExtra("id") -> {
                caseId = intent.getStringExtra("id") ?: ""
                setupObserver()
            }
        }
    }

    private fun buttonListener(){
        back_link.setOnClickListener {
            onBackPressed()
        }

        btn_refresh.setOnClickListener {
            sendData(true)
        }

        btn_home.setOnClickListener {
            val intent = Intent(this, Menu2::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_NEW_TASK)
            startActivity(intent)
        }

        btn_location.setOnClickListener {
            FunHelper.direction(it.context, info_address.text.toString())
        }

        btn_phone1.setOnClickListener {
            FunHelper.phoneCall(this, account_contact_phone.text.toString())
        }

        btn_phone2.setOnClickListener {
            FunHelper.phoneCall(this, account_pic_contact_phone.text.toString())
        }

        btn_edit_contact.setOnClickListener {
            val i = Intent(this, ContactAccount::class.java)
            i.putExtra("accountId", accountId)
            i.putExtra("accountName", accountName)
            startActivity(i)
        }

        btn_add_call.setOnClickListener {
            val i = Intent(this, ServiceCallAddCase::class.java)
            i.putExtra("caseId", caseId) 
            i.putExtra("accountId", accountId)
            startActivity(i)
        }

        btn_add_attachment.setOnClickListener {
            val i = Intent(this, AttachmentAdd::class.java)
            i.putExtra("accountId",accountId)
            i.putExtra("sourceId", caseId)
            i.putExtra("sourceTable","Case")
            startActivity(i)
        }
    }

    private fun generateTab() {
        tab_lay.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
            override fun onTabUnselected(tab: TabLayout.Tab?) {}
            override fun onTabReselected(tab: TabLayout.Tab?) {}

            override fun onTabSelected(tab: TabLayout.Tab?) {
                if (!isScrolling) {
                    when (tab_lay.selectedTabPosition) {
                        0 -> scroll_lay.smoothScrollTo(0, head_information.top)
                        1 -> scroll_lay.smoothScrollTo(0, head_installation.top)
                        2 -> scroll_lay.smoothScrollTo(0, head_agreement.top)
                        3 -> scroll_lay.smoothScrollTo(0, head_service_call.top)
                        4 -> scroll_lay.smoothScrollTo(0, head_service_report.top)
                        5 -> scroll_lay.smoothScrollTo(0, head_equipment.top)
                        6 -> scroll_lay.smoothScrollTo(0, head_activity.top)
                        7 -> scroll_lay.smoothScrollTo(0, head_attachment.top)
                    }
                }
            }

        })
    }

    private fun scrollListener() {
        scroll_lay.setOnScrollChangeListener { v: NestedScrollView?, x: Int, y: Int, oldX: Int, oldY: Int ->
             if (y >= head_information.top && y <= information_lay.bottom){
                isScrolling = true
                tab_lay.getTabAt(0)?.select()
                isScrolling = false
            }
            if (y >= head_installation.top && y < head_agreement.top){
                isScrolling = true
                tab_lay.getTabAt(1)?.select()
                isScrolling = false
            }
            if (y >= head_agreement.top && y < head_service_call.top){
                isScrolling = true
                tab_lay.getTabAt(2)?.select()
                isScrolling = false
            }
            else if (y >= head_service_call.top && y < head_service_report.top) {
                isScrolling = true
                tab_lay.getTabAt(3)?.select()
                isScrolling = false
            }
            else if (y >= head_service_report.top && y < head_equipment.top) {
                isScrolling = true
                tab_lay.getTabAt(4)?.select()
                isScrolling = false
            }
            else if (y >= head_equipment.top && y < head_activity.top) {
                isScrolling = true
                tab_lay.getTabAt(5)?.select()
                isScrolling = false
            }
            else if (y >= head_activity.top && y < head_attachment.top) {
                isScrolling = true
                tab_lay.getTabAt(6)?.select()
                isScrolling = false
            }
            else if (y >= head_attachment.top) {
                isScrolling = true
                tab_lay.getTabAt(7)?.select()
                isScrolling = false
            }

        }
    }

    private fun setupAdapter() {
        FunHelper.setUpAdapter(this, recycler_agreement).adapter = agreementAdapter
        FunHelper.setUpAdapter(this, recycler_service_call).adapter = serviceCallAdapter
        FunHelper.setUpAdapter(this, recycler_service_report).adapter = reportAdapter
        FunHelper.setUpAdapter(this, recycler_equipment).adapter = equipmentAdapter
        FunHelper.setUpAdapter(this, recycler_activity).adapter = activityAdapter
        FunHelper.setUpAdapter(this, recycler_attachment).adapter = attachmentAdapter
    }

    private fun attachmentAdapterListener() {
        attachmentAdapter.setEventHandler(object : AttachmentAdapter.RecyclerClickListener {
            override fun isClicked(fileUrl: String) {
                val progressDialog = ProgressDialog(this@CaseDetail)
                progressDialog.setMessage("Open Attachment ...")
                progressDialog.setCancelable(false)
                progressDialog.show()

                if (fileUrl.isNotEmpty()) {
                    val thread = object : Thread() {
                        override fun run() {
                            var fileExtension = ""
                            try {
                                val connection = URL(fileUrl).openConnection()
                                fileExtension = connection.getHeaderField("Content-Type")
                                Log.e("aim", "extention : $fileExtension")
                            } catch (e: Exception) { }

                            runOnUiThread {
                                if (fileExtension.contains("pdf")) {
                                    progressDialog.dismiss()
                                    showPdf(fileUrl)
                                } else if (fileExtension.contains("jpg") ||
                                    fileExtension.contains("jpeg") ||
                                    fileExtension.contains("png")) {

                                    progressDialog.dismiss()
                                    val i = Intent(this@CaseDetail, ImagePreview::class.java)
                                    i.putExtra("image", fileUrl)
                                    startActivity(i)
                                } else {
                                    progressDialog.dismiss()
                                    Toast.makeText(
                                        this@CaseDetail,
                                        "Cant open attachment file",
                                        Toast.LENGTH_LONG
                                    ).show()
                                }
                            }
                        }
                    }
                    thread.start()
                } else {
                    Toast.makeText(
                        this@CaseDetail,
                        "Attachment file not available",
                        Toast.LENGTH_LONG
                    ).show()
                }
            }

        })
    }

    @SuppressLint("SetTextI18n")
    private fun setupObserver(){
        caseVM.getByCaseId(caseId).observe(this, {
            if (it.isNotEmpty()) {
                // Toolbar
                header_name?.text = "Case : ${it.first().caseNumber}"

                // header
                title_value.text = it.first().subject.ifEmpty { "-" }
                company_value.text = it.first().accountName.ifEmpty { "-" }
                status_value.text = it.first().caseStatusEnumName.ifEmpty { "-" }
                case_status.text = it.first().caseStatusEnumName.ifEmpty { "-" }

                //info
                info_account_name.text = it.first().accountName.ifEmpty { "-" }
                info_address.text = it.first().addressText.ifEmpty { "-" }

                // Account contact
                getContact(
                    it.first().caseAccountContactId,
                    account_contact_name,
                    account_contact_phone,
                    account_contact_type
                )

                // PIC contact
                getContact(
                    it.first().caseFieldAccountContactId,
                    account_pic_contact_name,
                    account_pic_contact_phone,
                    account_pic_contact_type
                )

                info_case_number.text = it.first().caseNumber.ifEmpty { "-" }
                sub_title.text = it.first().caseTypeEnumName.ifEmpty { "-" }

                case_subject_value.text = it.first().subject.ifEmpty { "-" }
                case_desc_value.text = it.first().description.ifEmpty { "-" }
                info_agreement_type.text = it.first().agreementType.ifEmpty { "-" }

                info_case_owner.text = it.first().caseOwnerName.ifEmpty { "-" }
                info_technical_pic.text = it.first().leadEngineerName.ifEmpty { "-" }
                account_sales_value.text = it.first().accountSalesName.ifEmpty { "-" }
                case_sales_value.text = it.first().caseSalesName.ifEmpty { "-" }

                case_type_remark_value.text = "-"

                created_by_value.text = it.first().createdBy.ifEmpty { "-" }
                modify_value.text = it.first().lastModifiedBy.ifEmpty { "-" }

                modify_date_value.text = FunHelper.uiDateFormat(it.first().lastModifiedDate)
                created_date_value.text = FunHelper.uiDateFormat(it.first().createdDate)

                // ================================================================================= Installation / Demo
                if (it.first().caseTypeEnumName == "Installation" || it.first().caseTypeEnumName == "Demo") {
                    installation_lay.visibility = View.VISIBLE
                    warning_installation.visibility = View.GONE

                    installation_delivery_value.text =
                        FunHelper.uiDateFormat(it.first().proposedDeliveryDate)
                    installation_install_value.text =
                        FunHelper.uiDateFormat(it.first().proposedInstallationDate)
                    installation_po_value.text = it.first().customerPo.ifEmpty { "-" }
                    installation_supply_direct_value.text =
                        if (it.first().directDelivery == "1") "Yes" else "No"
                    installation_material_value.text =
                        if (it.first().materialNeeded == "1") "Yes" else "No"
                    isntallation_free_value.text = it.first().noOfFreeService.ifEmpty { "-" }
                    installation_special_requirement_value.text =
                        if (it.first().specialRequirement == "1") "Yes" else "No"
                    installation_demo_value.text = it.first().loanDemoPeriod.ifEmpty { "-" }
                    installation_air_supply_value.text =
                        it.first().airUtilitySuppliedBy.ifEmpty { "-" }
                    installation_warranty_value.text =
                        it.first().warrantyPeriodSupplier.ifEmpty { "-" }
                    installation_customer_warranty_value.text =
                        it.first().warrantyPeriodCustomer.ifEmpty { "-" }
                    installation_pro_value.text =
                        if (it.first().professionalInspectionRequired == "1") "Yes" else "No"
                    installation_inspec_value.text = it.first().inspectionRemark.ifEmpty { "-" }
                } else {
                    installation_lay.visibility = View.GONE
                    warning_installation.visibility = View.VISIBLE
                }

                // ================================================================================= Attachment
                accountId = it.first().accountId
                accountName = it.first().accountName
                attachmnetObserver(accountId)

                // ================================================================================= Agreement
                val agreementId = it.first().agreementId
                agreementObserver(agreementId)

                // ================================================================================= My Case
                val engineerId = SharedPreferenceData.getString(this, 8, "")
                if (it.first().leadEngineerId == engineerId) {
                    btn_edit_contact.visibility = View.VISIBLE
                    btn_add_call.visibility = View.VISIBLE
                    btn_add_attachment.visibility = View.VISIBLE
                } else {
                    btn_edit_contact.visibility = View.GONE
                    btn_add_call.visibility = View.GONE
                    btn_add_attachment.visibility = View.GONE
                }

            }
//            else {
//                Toast.makeText(this, "Case not found", Toast.LENGTH_LONG).show()
//                onBackPressed()
//            }
        })

        callVM.getByCaseId(caseId).observe(this, {
            tx_service_call_count.text = ""
            serviceCallAdapter.clear()

            if (it.isNotEmpty()) {
                tx_service_call_count.text = "(${it.size})"
                recycler_service_call.visibility = View.VISIBLE
                warning_call.visibility = View.GONE

                serviceCallAdapter.setList(it, true)
            } else {
                recycler_service_call.visibility = View.GONE
                warning_call.visibility = View.VISIBLE
            }
        })

        reportVM.getByCaseId(caseId).observe(this, {
            tx_service_report_count.text = ""
            reportAdapter.clear()

            if (it.isNotEmpty()) {
                tx_service_report_count.text = "(${it.size})"
                recycler_service_report.visibility = View.VISIBLE
                warning_report.visibility = View.GONE

                reportAdapter.setList(it, true)
            } else {
                recycler_service_report.visibility = View.GONE
                warning_report.visibility = View.VISIBLE
            }
        })

        caseEquipmentVM.getByCaseId(caseId, true).observe(this, Observer {
            tx_equipment_count.text = ""
            equipmentAdapter.reset()

            if (it.isNotEmpty()) {
                tx_equipment_count.text = "(${it.size})"
                recycler_equipment.visibility = View.VISIBLE
                warning_equipment.visibility = View.GONE

                it.forEach { equipmentData ->
                    equipmentAdapter.addData(equipmentData, true)
                }
            } else {
                recycler_equipment.visibility = View.GONE
                warning_equipment.visibility = View.VISIBLE
            }
        })

        activityVM.getBycase(caseId).observe(this, {
            activity_count.text = ""

            if (it.isNotEmpty()) {
                activity_count.text = "(${it.size})"
                recycler_activity.visibility = View.VISIBLE
                warning_activity.visibility = View.GONE

                activityAdapter.setList(it)
            } else {
                recycler_activity.visibility = View.GONE
                warning_activity.visibility = View.VISIBLE
            }
        })
    }

    @SuppressLint("SetTextI18n")
    private fun agreementObserver(agreementId: String) {
        agreementVM.getById(agreementId).observe(this, { agreementData ->
            tx_agreement_count.text = ""

            if (agreementData.isNotEmpty()) {
                tx_agreement_count.text = "(${agreementData.size})"
                recycler_agreement.visibility = View.VISIBLE
                warning_agreement_1234.visibility = View.GONE

                agreementAdapter.setList(agreementData)
            } else {
                recycler_agreement.visibility = View.GONE
                warning_agreement_1234.visibility = View.VISIBLE
            }
        })
    }

    @SuppressLint("SetTextI18n")
    private fun attachmnetObserver(accountId: String) {
        attachmentVM.accountAttachmentSection(accountId, caseId, "Case")
            .observe(this, { attachmentData ->
            tx_attachment_count.text = ""

            if (attachmentData.isNotEmpty()) {
                tx_attachment_count.text = "(${attachmentData.size})"
                recycler_attachment.visibility = View.VISIBLE
                warning_attachment.visibility = View.GONE

                attachmentAdapter.setList(attachmentData)
            } else {
                recycler_attachment.visibility = View.GONE
                warning_attachment.visibility = View.VISIBLE
            }
        })
    }

    private fun getContact(contactId: String, name: TextView, phone: TextView, type: TextView) {
        accountContactVM.getById(contactId).observe(this, {
            if (it.isNotEmpty()) {
                name.text = it.first().contact_person_name.ifEmpty { "-" }
                phone.text = it.first().contact_person_phone.ifEmpty { "-" }
                type.text = it.first().contact_type.ifEmpty { "-" }
            }
        })
    }

    private fun showPdf(fileUrl: String) = runWithPermissions(FunHelper.storageWritePermission) {
        DownloadPdf(this, fileUrl).downloadNow(false)
    }

    private fun sendData(needRefresh: Boolean) {
        val progressDialog = ProgressDialog(this)
        progressDialog.setMessage("Send Data ...")
        progressDialog.setCancelable(false)
        progressDialog.show()

        SendData(this).getParam(object : SendDataInterface {
            override fun sendDataCallback(isSuccess: Boolean) {
                runOnUiThread {
                    progressDialog.dismiss()
                    if (needRefresh) {
                        syncFcmData()
                    }
                }
            }
        })
    }

    private fun syncFcmData() {
        val progressDialog = ProgressDialog(this)
        progressDialog.setMessage("Get New Data ...")
        progressDialog.setCancelable(false)
        progressDialog.show()

        SyncDataFcm(this).getByCaseId(caseId, "case", object : SendDataInterface {
            override fun sendDataCallback(isSuccess: Boolean) {
                progressDialog.dismiss()
                
                if (isSuccess) {
                    setupObserver()
                }
            }
        })
    }

}
