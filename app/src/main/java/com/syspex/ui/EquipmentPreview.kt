package com.syspex.ui

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.livinglifetechway.quickpermissions_kotlin.runWithPermissions

import com.syspex.R
import com.syspex.data.local.database.account.AccountViewModel
import com.syspex.data.local.database.account_equipment.AccountEquipmentViewModel
import com.syspex.data.local.database.agreement.AgreementViewModel
import com.syspex.data.local.database.product_checklist_template.ChecklistTemplateViewModel
import com.syspex.data.local.database.product_document.ProductDocumentViewModel
import com.syspex.data.local.database.product_group.ProductViewModel
import com.syspex.data.local.database.service_call.ServiceCallViewModel
import com.syspex.data.local.database.service_case.ServiceCaseViewModel
import com.syspex.data.local.database.service_report.ServiceReportViewModel
import com.syspex.ui.adapter.*
import com.syspex.ui.helper.DownloadPdf
import com.syspex.ui.helper.FunHelper
import kotlinx.android.synthetic.main.equipment_preview_fragment.*

class EquipmentPreview : BottomSheetDialogFragment() {

    companion object {
        var equipmentId = ""
    }

    private lateinit var caseVM: ServiceCaseViewModel
    private lateinit var accountVM: AccountViewModel
    private lateinit var accountEquipmentVM: AccountEquipmentViewModel
    private lateinit var agreementVM: AgreementViewModel
    private lateinit var callVM: ServiceCallViewModel
    private lateinit var reportVM: ServiceReportViewModel
    private lateinit var productVM: ProductViewModel
    private lateinit var checklistVM : ChecklistTemplateViewModel
    private lateinit var productDocumentVM: ProductDocumentViewModel

    private val agreementAdapter = AgreementAdapter()
    private val caseAdapter = CaseMyAdapter()
    private val serviceCallAdapter = SCMyCallAdapter()
    private val serviceReportAdapter = SRMyReportAdapter()
    private val checklistAdapter = ChecklistMasterAdapter()
    private val documentAdapter = TechnicalDocumentAdapter()

    // current report
    private var currentReportId = ""
    private var currentReportNumber = ""

    // last report
    private var lastReportId = ""
    private var lastReportNumber = ""

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.equipment_preview_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        caseVM = ViewModelProviders.of(this).get(ServiceCaseViewModel::class.java)
        accountVM = ViewModelProviders.of(this).get(AccountViewModel::class.java)
        accountEquipmentVM = ViewModelProviders.of(this).get(AccountEquipmentViewModel::class.java)
        agreementVM = ViewModelProviders.of(this).get(AgreementViewModel::class.java)
        callVM = ViewModelProviders.of(this).get(ServiceCallViewModel::class.java)
        reportVM = ViewModelProviders.of(this).get(ServiceReportViewModel::class.java)
        productVM = ViewModelProviders.of(this).get(ProductViewModel::class.java)
        checklistVM = ViewModelProviders.of(this).get(ChecklistTemplateViewModel::class.java)
        productDocumentVM = ViewModelProviders.of(this).get(ProductDocumentViewModel::class.java)

        buttonListener()
        setupAdapter()
        adapterListener()
        setupObserver()


    }

    private fun buttonListener(){
        btn_location.setOnClickListener {
            FunHelper.direction(requireContext(), info_address.text.toString())
        }

        start2_value.setOnClickListener {
            val i = Intent(requireContext(),ServiceReportDetail::class.java)
            i.putExtra("id", currentReportId)
            it.context.startActivity(i)
        }

        end2_value.setOnClickListener {
            val linkPdf = "${FunHelper.BASE_URL}service-report-pdf/detail/$lastReportId"
            showPdf(linkPdf)
        }

    }

    private fun setupAdapter() {
        FunHelper.setUpAdapter(requireContext(), recycler_agreement).adapter = agreementAdapter
        FunHelper.setUpAdapter(requireContext(), recycler_case).adapter = caseAdapter
        FunHelper.setUpAdapter(requireContext(), recycler_service_call).adapter = serviceCallAdapter
        FunHelper.setUpAdapter(requireContext(), recycler_service_report).adapter = serviceReportAdapter

        recycler_check_list.layoutManager = LinearLayoutManager(requireContext(), RecyclerView.VERTICAL, false)
        recycler_check_list.adapter = checklistAdapter

        recycler_document?.layoutManager = LinearLayoutManager(requireContext(), RecyclerView.VERTICAL, false)
        recycler_document?.adapter = documentAdapter
    }

    private fun adapterListener() {
        documentAdapter.setEventHandler(object : TechnicalDocumentAdapter.RecyclerClickListener {
            override fun isClicked(fileUrl: String) {
                showPdf(fileUrl)
            }
        })
    }

    @SuppressLint("SetTextI18n")
    private fun setupObserver(){
        accountEquipmentVM.getByEquipmentId(equipmentId).observe(this, {
            if (it.isNotEmpty()) {
                title_value.text = "SN : ${it.first().serialNo.ifEmpty { "-" }}"
                status_value.text = it.first().statusTextEnumText.ifEmpty { "-" }
                sub_title.text = it.first().product_name.ifEmpty { "-" }
                phone_value.text = it.first().accountName.ifEmpty { "-" }
                sales_value.text = it.first().statusTextEnumText.ifEmpty { "-" }
                remark_value.text = it.first().equipmentRemarks.ifEmpty { "-" }
                industry_value.text = it.first().agreementNumber.ifEmpty { "-" }
                start_value.text = it.first().warrantyStartDate.ifEmpty { "-" }
                end_value.text = it.first().warrantyEndDate.ifEmpty { "-" }
                start1_value.text = it.first().warrantyStartDate.ifEmpty { "-" }
                end1_value.text = it.first().warrantyEndDate.ifEmpty { "-" }

                val accountId = it.first().accountId
                accountObserver(accountId)

                val productId = it.first().product_id
                productObserver(productId)
            }
        })

        caseVM.caseEquipmentRelated(equipmentId).observe(this, {
            tx_case_count.text = ""

            if (it.isNotEmpty()) {
                tx_case_count.text = "(${it.size})"
                recycler_case.visibility = View.VISIBLE
                warning_case.visibility = View.GONE
                caseAdapter.setList(it, false)
            } else {
                recycler_case.visibility = View.GONE
                warning_case.visibility = View.VISIBLE
            }
        })

        callVM.callEquipmentRelated(equipmentId).observe(this, {
            tx_call_count.text = ""

            if (it.isNotEmpty()) {
                tx_call_count.text = "(${it.size})"
                recycler_service_call.visibility = View.VISIBLE
                warning_call.visibility - View.GONE
                serviceCallAdapter.setList(it, false)
            } else {
                recycler_service_call.visibility = View.GONE
                warning_call.visibility = View.VISIBLE
            }
        })

        reportVM.reportEquipmentRelated(equipmentId).observe(this, {
            tx_report_count.text = ""

            if (it.isNotEmpty()) {
                tx_report_count.text = "(${it.size})"
                recycler_service_report.visibility = View.VISIBLE
                warning_report.visibility = View.GONE
                serviceReportAdapter.setList(it, false)

                it.forEach { reportData->
                    if (reportData.reportStatusEnumText != "Completed") {
                        currentReportId = reportData.reportLocalId
                        currentReportNumber = reportData.serviceReportNumber

                        tx_start2.visibility = View.VISIBLE
                        start2_value.visibility = View.VISIBLE
                        start2_value.text = currentReportNumber
                    } else {
                        tx_start2.visibility = View.GONE
                        start2_value.visibility = View.GONE
                    }

                    if (reportData.reportStatusEnumText == "Completed") {
                        lastReportId = reportData.reportLocalId
                        lastReportNumber = reportData.serviceReportNumber

                        tx_end2.visibility = View.VISIBLE
                        end2_value.visibility = View.VISIBLE
                    } else {
                        tx_end2.visibility = View.GONE
                        end2_value.visibility = View.GONE
                    }

                }

            } else {
                recycler_service_report.visibility = View.GONE
                warning_report.visibility = View.VISIBLE

                // Current report button
                tx_start2.visibility = View.GONE
                start2_value.visibility = View.GONE

                // Last report button
                tx_end2.visibility = View.GONE
                end2_value.visibility = View.GONE
            }
        })

        agreementVM.getByEquipmentId(equipmentId).observe(this, { agreementData->
            tx_agreement_count.text = ""

            if (agreementData.isNotEmpty()) {
                tx_agreement_count.text = "(${agreementData.size})"
                recycler_agreement.visibility = View.VISIBLE
                warning_agreement.visibility = View.GONE

                agreementAdapter.setList(agreementData)
            } else {
                recycler_agreement.visibility = View.GONE
                warning_agreement.visibility = View.VISIBLE
            }
        })

    }

    private fun accountObserver(accountId: String) {
        accountVM.getByAccountId(accountId).observe(this, {
            if (it.isNotEmpty()) {
                info_address.text = it.first().address?.first()?.fullAddress ?: "-"
                account_sap_value.text = "-"
                account_sales_value.text = it.first().sales_name.ifEmpty { "-" }
                account_branch_value.text = it.first().region_id.ifEmpty { "-" }
                account_created_value.text = FunHelper.uiDateFormat(it.first().created_date ?: "")
                account_modify_value.text = FunHelper.uiDateFormat(it.first().last_modified_date ?: "")
            }
        })
    }

    // Technical Data
    @SuppressLint("SetTextI18n")
    private fun productObserver(productId: String) {
        productVM.productGroupJoinProduct(productId).observe(this, { productData->
            if (productData.isNotEmpty()) {
                technical_product_value.text = productData.first().product_group_name.ifEmpty { "-" }

                val voltage = productData.first().voltage.ifEmpty { "-" }
                val current = productData.first().current.ifEmpty { "-" }
                val power = productData.first().power.ifEmpty { "-" }
                val phase = productData.first().phase.ifEmpty { "-" }
                val frequency = productData.first().frequency.ifEmpty { "-" }
                technical_information_value.text = "$voltage | $current | $power | $phase | $frequency"

                technical_air_supply_value.text = productData.first().air_supply_needed.ifEmpty { "-" }
                technical_air_pressure_value.text = productData.first().air_pressure.ifEmpty { "-" }
                machine_brand_value.text = productData.first().machine_brand.ifEmpty { "-" }
                machine_model_value.text = productData.first().machine_model.ifEmpty { "-" }
                machine_class_value.text = productData.first().machine_class_category.ifEmpty { "-" }
                technical_supplier_value.text = productData.first().machine_supplier.ifEmpty { "-" }
                technical_tag_value.text = productData.first().product_group_tag_id.ifEmpty { "-" }

                val productGroupId = productData.first().product_group_id
                checklistObserver(productGroupId)
                productGroupObserver(productGroupId)
            }
            else {
                technical_information_value.text = "-"
                technical_air_supply_value.text = "-"
                technical_air_pressure_value.text = "-"
                machine_brand_value.text = "-"
                machine_model_value.text = "-"
                machine_class_value.text = "-"
                technical_supplier_value.text = "-"
                technical_tag_value.text = "-"

                checklist_lay.visibility = View.GONE
                warning_checklist.visibility = View.VISIBLE

                document_lay.visibility = View.GONE
                warning_document.visibility = View.VISIBLE
            }
        })
    }

    @SuppressLint("SetTextI18n")
    private fun checklistObserver(productGroupId: String) {
        checklistVM.getTemplateByProductGroupId(productGroupId).observe(this, {
            tx_checklist_count.text = ""

            if (it.isNotEmpty()) {
                tx_checklist_count.text = "(${it.size})"
                checklist_lay.visibility = View.VISIBLE
                warning_checklist.visibility = View.GONE

                checklistAdapter.setList(it)
            } else {
                checklist_lay.visibility = View.GONE
                warning_checklist.visibility = View.VISIBLE
            }
        })
    }

    // Document
    @SuppressLint("SetTextI18n")
    private fun productGroupObserver(productGroupId: String) {
        productDocumentVM.getByProductGroupId(productGroupId).observe(this, Observer { documentData->
            tx_document_count.text = ""

            if (documentData.isNotEmpty()) {
                tx_document_count.text = "(${documentData.size})"
                document_lay.visibility = View.VISIBLE
                warning_document.visibility = View.GONE

                documentAdapter.setList(documentData)
            } else {
                document_lay.visibility = View.GONE
                warning_document.visibility = View.VISIBLE
            }
        })
    }

    private fun showPdf(fileUrl: String) = runWithPermissions( FunHelper.storageWritePermission) {
        DownloadPdf(requireContext(), fileUrl).downloadNow(false)
    }

}
