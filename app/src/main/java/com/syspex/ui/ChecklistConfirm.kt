package com.syspex.ui

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.FragmentActivity
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.syspex.R
import com.syspex.data.local.database.global_enum.EnumEntity
import com.syspex.data.local.database.global_enum.EnumViewModel
import com.syspex.data.local.database.product_checklist_question.ChecklistQuestionViewModel
import com.syspex.data.local.database.product_checklist_template.ChecklistTemplateViewModel
import com.syspex.data.local.database.product_group_item.ProductGroupItemViewModel
import com.syspex.data.local.database.service_call.ServiceCallViewModel
import com.syspex.data.local.database.service_report.ServiceReportViewModel
import com.syspex.data.local.database.service_report_equipment.ServiceReportEquipmentViewModel
import com.syspex.data.local.database.service_report_equipment_checklist.ServiceReportEquipmentChecklistViewModel
import com.syspex.data.model.ChecklistModel
import com.syspex.data.model.ChecklistStatusModel
import com.syspex.data.model.ConverterImageModel
import com.syspex.ui.adapter.ChecklistQuestionImageAdapter
import com.syspex.ui.adapter.ChecklistTemplateAdapter
import com.syspex.ui.helper.ChecklistInterface
import kotlinx.android.synthetic.main.checklist_confirm_activity.*
import kotlinx.android.synthetic.main.item_checklist_template.view.*
import kotlinx.android.synthetic.main.sub_header.*

class ChecklistConfirm : AppCompatActivity(), ChecklistInterface {

    companion object {
        var checklistImage = ArrayList<ConverterImageModel>()
    }

    private lateinit var callVM: ServiceCallViewModel
    private lateinit var reportVM: ServiceReportViewModel
    private lateinit var reportEquipmentVM: ServiceReportEquipmentViewModel
    private lateinit var enumVM: EnumViewModel
    private lateinit var productGroupItemVM: ProductGroupItemViewModel
    private lateinit var checklistTemplateVM: ChecklistTemplateViewModel
    private lateinit var checklistQuestionVM: ChecklistQuestionViewModel
    private lateinit var reportChecklistVM: ServiceReportEquipmentChecklistViewModel

    private val templateAdapter = ChecklistTemplateAdapter(this)
    private lateinit var enumData: MutableList<EnumEntity>
    private var statusData = mutableListOf<ChecklistStatusModel>()

    private var returnQuestionId = ""
    private var reportId = ""
    private var reportEquipmentId = ""
    private var productId = ""
    private var equipmentId = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.checklist_confirm_activity)
        header_name.text = "Checklist"

        callVM = ViewModelProviders.of(this).get(ServiceCallViewModel::class.java)
        reportVM = ViewModelProviders.of(this).get(ServiceReportViewModel::class.java)
        reportEquipmentVM = ViewModelProviders.of(this).get(ServiceReportEquipmentViewModel::class.java)
        enumVM = ViewModelProviders.of(this).get(EnumViewModel::class.java)
        productGroupItemVM = ViewModelProviders.of(this).get(ProductGroupItemViewModel::class.java)
        checklistTemplateVM = ViewModelProviders.of(this).get(ChecklistTemplateViewModel::class.java)
        checklistQuestionVM = ViewModelProviders.of(this).get(ChecklistQuestionViewModel::class.java)
        reportChecklistVM = ViewModelProviders.of(this).get(ServiceReportEquipmentChecklistViewModel::class.java)

        buttonListener()
        generateView()

        setupAdapter()
    }

    // Return from dialog upload file
    override fun onResume() {
        super.onResume()
        if (checklistImage.isNotEmpty()) {
            updateQuestionStatus()
            updateQuestionImage(checklistImage)
        }
    }

    override fun onBackPressed() {
        super.onBackPressed()
        if (statusData.isNotEmpty()) {
            updateQuestionStatus()
        }
    }

    // ============================================================================================= Checklist Interface
    override fun onQuestionStatusChange(questionId: String, enumId: String, enumName: String, remark: String) {
        val check = statusData.find {it.questionId == questionId}
        if (check != null) {
            check.enumId = enumId
            check.enumText = enumName
            check.remark = remark
        } else {
            statusData.add( ChecklistStatusModel(
                questionId = questionId,
                enumId = enumId,
                enumText = enumName,
                remark = remark
            ))
        }
        Log.e("aim", "status data : $statusData")
    }

    override fun onQuestionAddImage(questionId: String, image: ArrayList<ConverterImageModel>) {
        returnQuestionId = questionId
        checklistImage.addAll(image)

        DialogUploadFile.fileSection = "checklist"
        DialogUploadFile.equipmentId = equipmentId
        DialogUploadFile().show((this@ChecklistConfirm as FragmentActivity).supportFragmentManager,DialogUploadFile().tag)
    }

    override fun onQuestionDeleteImage(questionId: String, image: ArrayList<ConverterImageModel>) {
        returnQuestionId = questionId
        checklistImage.addAll(image)
        updateQuestionImage(image)
    }

    private fun generateView() {
        if (intent.hasExtra("reportId")) {
            reportId = intent.getStringExtra("reportId") ?: ""
            reportEquipmentId = intent.getStringExtra("reportEquipmentId") ?: ""
            equipmentId = intent.getStringExtra("equipmentId") ?: ""
            productId = intent.getStringExtra("productId") ?: ""
            getEnumData()
        }
    }

    private fun buttonListener() {
        back_link.setOnClickListener {
            onBackPressed()
        }
    }

    private fun setupAdapter() {
        recycler_checklist?.layoutManager = LinearLayoutManager(this, RecyclerView.VERTICAL, false)
        recycler_checklist?.adapter = templateAdapter
    }

    private fun getEnumData() {
        enumVM.getByTableName("service_report_equipment_checklist").observe(this, {
            enumData = it as MutableList<EnumEntity>
            getReportChecklist()
        })
    }

    private fun getReportChecklist() {
        reportChecklistVM.getByReportEquipmentId(reportEquipmentId).observe(this, { templateData->
            templateAdapter.reset()
            val title = ArrayList<String>()

            templateData.forEach { questionData->
                if (!title.contains(questionData.template_desc)) {
                    title.add(questionData.template_desc)
                }
            }

            title.forEach { titleString->
                val questionData = templateData.filter { it.template_desc == titleString }
                    .sortedBy { it.question_order_no }
                    .toMutableList()

                templateAdapter.addList(
                    ChecklistModel(
                        "",
                        "",
                        "",
                        titleString ?: "",
                        "",
                        "",
                        questionData,
                        enumData
                    )
                )
            }

            checklistImage.clear()

        })
    }

    private fun updateQuestionStatus() {
        statusData.forEach {
            reportChecklistVM.updateStatus(
                questionId = it.questionId,
                enumId = it.enumId,
                enumName = it.enumText,
                remark = it.remark
            )
        }

        reportEquipmentVM.updateFlag(reportEquipmentId, false)
        reportVM.updateFlag(reportId, false)
    }

    private fun updateQuestionImage(image: ArrayList<ConverterImageModel>) {
        reportChecklistVM.updateImage(returnQuestionId, image)
        reportEquipmentVM.updateFlag(reportEquipmentId, false)
        reportVM.updateFlag(reportId, false)
    }

}