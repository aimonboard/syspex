package com.syspex.ui

import android.annotation.SuppressLint
import android.app.ProgressDialog
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.TextView
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.syspex.R
import com.syspex.data.local.database.activity.ActivityEntity
import com.syspex.data.local.database.activity.ActivityViewModel
import com.syspex.data.local.database.activity_type.ActivityTypeEntity
import com.syspex.data.local.database.activity_type.ActivityTypeViewModel
import com.syspex.ui.adapter.ActivityTypeAdapter
import com.syspex.ui.helper.FunHelper
import com.syspex.ui.helper.SendData
import com.syspex.ui.helper.SyncActivity
import com.syspex.ui.helper.SendDataInterface
import kotlinx.android.synthetic.main.activity_add.*
import kotlinx.android.synthetic.main.activity_add.btn_save
import kotlinx.android.synthetic.main.activity_add.name_value
import kotlinx.android.synthetic.main.sub_header.back_link
import kotlinx.android.synthetic.main.sub_header.header_name
import java.text.SimpleDateFormat
import java.util.*

class ActivityAdd : AppCompatActivity() {

    private lateinit var activityVM: ActivityViewModel
    private lateinit var activityTypeVM: ActivityTypeViewModel
    private lateinit var activityData : ActivityEntity

    private var activityTypeAdapter = ActivityTypeAdapter()

    private val dateStartRequest = 5552
    private val dateEndRequest = 5551

    private var isEdit = false
    private var activityId = ""
    private var activityTypeId = ""
    private var activityTypeCode = ""
    private var activityStart = ""
    private var activityEnd = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add)
        header_name.text = "Add New Activity"
        activityVM = ViewModelProviders.of(this).get(ActivityViewModel::class.java)
        activityTypeVM = ViewModelProviders.of(this).get(ActivityTypeViewModel::class.java)

        generateView()
        buttonListener()
        activityTypeObserver()
        activityObserver()

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        when (requestCode) {
            dateStartRequest -> {
                val resultData = data?.getStringExtra("dateTime")
                activityStart = resultData ?: ""
                btn_date_start.text = FunHelper.uiDateFormat(resultData ?: "")
            }

            dateEndRequest -> {
                val resultData = data?.getStringExtra("dateTime")
                activityEnd = resultData ?: ""
                btn_date_end.text = FunHelper.uiDateFormat(resultData ?: "")
            }
        }
    }

    @SuppressLint("SetTextI18n")
    private fun generateView() {
        if (intent.hasExtra("id")) {
            activityId = intent.getStringExtra("id") ?: ""
            header_name?.text = "Edit Activity"
            isEdit = true
            activityObserver()
        }
    }

    private fun buttonListener() {
        back_link.setOnClickListener {
            onBackPressed()
        }

        type_value.setOnClickListener {
            typeDialog()
        }

        btn_date_start.setOnClickListener {
            val i = Intent(this, DateTimePicker::class.java)
            i.putExtra("title", "Start Date Time")
            startActivityForResult(i, dateStartRequest)
        }

        btn_date_end.setOnClickListener {
            val i = Intent(this, DateTimePicker::class.java)
            i.putExtra("title", "End Date Time")
            startActivityForResult(i, dateEndRequest)
        }

        btn_save.setOnClickListener {
            if (check()) {
                val format = SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault())
                val startTime = format.parse(activityStart)!!
                val endTime = format.parse(activityEnd)!!
                if (startTime.after(endTime)) {
                    Toast.makeText(
                        this,
                        "Start time must be earlier than end time",
                        Toast.LENGTH_LONG
                    ).show()
                } else {
                    if (isEdit) {
                        activityEdit()
                    } else {
                        activityInsert()
                    }
                }
            } else {
                Toast.makeText(this, "Please fill required column", Toast.LENGTH_LONG).show()
            }
        }
    }

    private fun check(): Boolean{
        var cek = 0

        if(name_value.text.toString() == ""){
            name_err.text = "*Required"; cek++
        } else { name_err.text = "" }

        if(type_value.text.toString() == ""){
            type_err.text = "*Required"; cek++
        } else { type_err.text = "" }

        if(btn_date_start.text.toString() == ""){
            start_err.text = "*Required"; cek++
        } else { start_err.text = "" }

        if(btn_date_end.text.toString() == ""){
            end_err.text = "*Required"; cek++
        } else { end_err.text = "" }

        if(desc_value.text.toString() == ""){
            desc_err.text = "*Required"; cek++
        } else { desc_err.text = "" }

        if (cek == 0){
            return true
        }
        return false
    }

    private fun activityTypeObserver() {
        val typeLive = activityTypeVM.getAll()
        typeLive.observe(this, Observer {
            typeLive.removeObservers(this)

            it.forEach { activityData->
                if (activityData.activity_type_code != "AM" && activityData.activity_type_code != "RC") {
                    activityTypeAdapter.addData(activityData)
                }
            }
        })
    }

    private fun activityObserver() {
        val activityLive = activityVM.getById(activityId)
        activityLive.observe(this, {
            activityLive.removeObservers(this)
            if (it.isNotEmpty()) {
                activityData = it.first()
                activityTypeId = it.first().activityTypeId
                activityTypeCode = it.first().activityTypeCode
                activityStart = it.first().activityStartTime
                activityEnd = it.first().activityEndTime

                name_value.setText(it.first().activitySubject)
                type_value.text = it.first().activityTypeName
                btn_date_start.text = FunHelper.uiDateFormat(it.first().activityStartTime)
                btn_date_end.text = FunHelper.uiDateFormat(it.first().activityEndTime)
                desc_value.setText(it.first().activityDescription)
            }
        })
    }

    private fun typeDialog() {
        val view = layoutInflater.inflate(R.layout.dialog_list_filter,null)
        val dialog = BottomSheetDialog(this)
        dialog.setContentView(view)
        val bottomSheetBehavior = BottomSheetBehavior.from<View>(view.parent as View)
        dialog.setOnShowListener { bottomSheetBehavior.state = BottomSheetBehavior.STATE_EXPANDED }

        val btnClear = dialog.findViewById<TextView>(R.id.btn_clear)
        btnClear?.visibility = View.GONE

        val recyclerFilter = dialog.findViewById<RecyclerView>(R.id.recycler_filter)
        recyclerFilter?.layoutManager = LinearLayoutManager(this, RecyclerView.VERTICAL, false)
        recyclerFilter?.adapter = activityTypeAdapter

        activityTypeAdapter.setEventHandler(object: ActivityTypeAdapter.RecyclerClickListener {
            override fun isCLicked(data: ActivityTypeEntity) {
                activityTypeId = data.activity_type_id
                activityTypeCode = data.activity_type_code
                type_value.text = data.activity_type_name

                dialog.dismiss()
            }
        })

        dialog.show()
    }

    private fun activityInsert() {
        activityVM.insert(ActivityEntity(
            FunHelper.randomString("") ?: "",
            "",
            "",
            "",
            "",
            "",
            "",
            activityStart ?: "",
            activityEnd ?: "",
            name_value.text.toString() ?: "",
            desc_value.text.toString() ?: "",
            "",
            "",
            "",
            "",
            "",
            "",
            activityTypeId ?: "",
            activityTypeCode ?: "",
            type_value.text.toString() ?: "",
            "",
            true
        ))

        //        SendData(this).getParam(false)
        sendData()
    }

    private fun activityEdit() {
        activityVM.insert(ActivityEntity(
            activityData.activityId ?: "",
            activityData.caseId ?: "",
            activityData.accountId ?: "",
            activityData.serviceCallId ?: "",
            activityData.regionId ?: "",
            activityData.userId ?: "",
            activityData.activityType ?: "",
            activityStart ?: "",
            activityEnd ?: "",
            name_value.text.toString() ?: "",
            desc_value.text.toString() ?: "",
            activityData.activitySourceTable ?: "",
            activityData.activitySourceId ?: "",
            activityData.createdDate ?: "",
            activityData.createdBy ?: "",
            activityData.lastModifiedDate ?: "",
            activityData.lastModifiedDate ?: "",
            activityTypeId ?: "",
            activityTypeCode ?: "",
            type_value.text.toString() ?: "",
            activityData.activityTypeColor ?: "",
            true
        ))

//        SendData(this).getParam(false)
        sendData()
    }

    private fun sendData() {
        val progressDialog = ProgressDialog(this)
        progressDialog.setMessage("Send Data ...")
        progressDialog.setCancelable(false)
        progressDialog.show()

        SendData(this).getParam(object : SendDataInterface {
            override fun sendDataCallback(isSuccess: Boolean) {
                runOnUiThread {
                    progressDialog.dismiss()

                    if (isSuccess) {
                        syncActivity()
                    }
                }
            }
        })
    }

    private fun syncActivity() {
        val progressDialog = ProgressDialog(this)
        progressDialog.setMessage("Sync Activity ...")
        progressDialog.setCancelable(false)
        progressDialog.show()

        SyncActivity(this).startSyncActivity(object : SendDataInterface {
            override fun sendDataCallback(isSuccess: Boolean) {
                runOnUiThread {
                    progressDialog.dismiss()
                    onBackPressed()
                }
            }
        })
    }


}