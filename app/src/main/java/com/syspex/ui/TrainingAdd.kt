package com.syspex.ui

import android.annotation.SuppressLint
import android.app.ProgressDialog
import android.content.res.Resources
import android.graphics.Bitmap
import android.graphics.Canvas
import android.graphics.Color
import android.net.Uri
import android.os.Bundle
import android.os.Environment
import android.provider.OpenableColumns
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.View
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.FileProvider
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.livinglifetechway.quickpermissions_kotlin.runWithPermissions
import com.syspex.R
import com.syspex.data.local.SharedPreferenceData
import com.syspex.data.local.database.account_contact.AccountContactEntity
import com.syspex.data.local.database.account_contact.AccountContactViewModel
import com.syspex.data.local.database.global_enum.EnumViewModel
import com.syspex.data.local.database.service_call.ServiceCallViewModel
import com.syspex.data.local.database.service_call_equipment.ServiceCallEquipmentViewModel
import com.syspex.data.local.database.service_call_training.ServiceCallTrainingEntity
import com.syspex.data.local.database.service_call_training.ServiceCallTrainingViewModel
import com.syspex.data.model.ConverterImageModel
import com.syspex.data.model.EquipmentListModel
import com.syspex.data.model.PersonelModel
import com.syspex.data.remote.GetDataRequest
import com.syspex.data.remote.TrainingAddRequest
import com.syspex.data.retrofit.ApiClient
import com.syspex.ui.adapter.HandoverEquipmentAdapter
import com.syspex.ui.adapter.PersonelAdapter
import com.syspex.ui.adapter.TrainingTypeAdapter
import com.syspex.ui.custom.AutoTextContactAdapter
import com.syspex.ui.custom.AutoTextEngineerAdapter
import com.syspex.ui.helper.FunHelper
import kotlinx.android.synthetic.main.sub_header.*
import kotlinx.android.synthetic.main.training_add_activity.*
import kotlinx.android.synthetic.main.training_add_activity.btn_save
import kotlinx.android.synthetic.main.training_add_activity.customer_name_err
import kotlinx.android.synthetic.main.training_add_activity.customer_name_value
import kotlinx.android.synthetic.main.training_add_activity.customer_signature_clear
import kotlinx.android.synthetic.main.training_add_activity.customer_signature_edit
import kotlinx.android.synthetic.main.training_add_activity.customer_signature_err
import kotlinx.android.synthetic.main.training_add_activity.customer_signature_lay
import kotlinx.android.synthetic.main.training_add_activity.customer_signature_pad
import kotlinx.android.synthetic.main.training_add_activity.customer_signature_preview
import kotlinx.android.synthetic.main.training_add_activity.customer_signature_priview_image
import kotlinx.android.synthetic.main.training_add_activity.customer_signature_save
import kotlinx.android.synthetic.main.training_add_activity.customer_value
import kotlinx.android.synthetic.main.training_add_activity.desc_err
import kotlinx.android.synthetic.main.training_add_activity.desc_value
import kotlinx.android.synthetic.main.training_add_activity.location_value
import kotlinx.android.synthetic.main.training_add_activity.recycler_equipment
import kotlinx.android.synthetic.main.training_add_activity.warning_equipment
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import org.json.JSONArray
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.File
import java.io.FileOutputStream
import java.io.IOException
import java.io.OutputStream
import java.text.DecimalFormat
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList

class TrainingAdd : AppCompatActivity() {

    private lateinit var enumVM: EnumViewModel
    private lateinit var callVM: ServiceCallViewModel
    private lateinit var callTrainingVM: ServiceCallTrainingViewModel
    private lateinit var equipmentVM: ServiceCallEquipmentViewModel
    private lateinit var contactVM: AccountContactViewModel

    private val typeAdapter = TrainingTypeAdapter()
    private val equipmentAdapter = HandoverEquipmentAdapter()
    private val personelAdapter = PersonelAdapter()

    private val serverFormat = SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault())
    private var trainingStartDate: Date? = null
    private var trainingEndDate: Date? = null

    private var trainingTypeId = ArrayList<String>()
    private var equipmentData = ArrayList<String>()
    private var customerSignatureName = ""
    private var customerSignatureFile: File? = null

    private var callId = ""
    private var accountId = ""
    private var accountName = ""
    private var addressId = ""
    private var addressText = ""
    private var engineerId = ""
    private var engineerName = ""
    private var contactId = ""
    private var contactName = ""

    @SuppressLint("SetTextI18n")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.training_add_activity)
        header_name.text = "Add Training Attendance Form"

        enumVM = ViewModelProviders.of(this).get(EnumViewModel::class.java)
        callVM = ViewModelProviders.of(this).get(ServiceCallViewModel::class.java)
        callTrainingVM = ViewModelProviders.of(this).get(ServiceCallTrainingViewModel::class.java)
        equipmentVM = ViewModelProviders.of(this).get(ServiceCallEquipmentViewModel::class.java)
        contactVM = ViewModelProviders.of(this).get(AccountContactViewModel::class.java)

        generateView()
        buttonListener()
        spinnerPersonel()
        setupAdapter()

        typeAdapterListener()
        equipmentAdapterListener()
        personelAdapterListener()

        equipmentObserver()
        engineerObserver()
        customerObserver()

    }

    private fun generateView() {
        if (intent.hasExtra("callId")) {
            callId = intent.getStringExtra("callId") ?: ""
            accountId = intent.getStringExtra("accountId") ?: ""
            accountName = intent.getStringExtra("accountName") ?: ""
            addressId = intent.getStringExtra("addressId") ?: ""
            addressText = intent.getStringExtra("addressText") ?: ""
            engineerId = intent.getStringExtra("engineerId") ?: ""
            engineerName = intent.getStringExtra("engineerName") ?: ""
            contactId = intent.getStringExtra("picId") ?: ""
            contactName = intent.getStringExtra("picName") ?: ""

            customer_value.text = accountName
            location_value.setText(addressText)

            engineer_value.setText(engineerName)
            engineer_value.dismissDropDown()

            customer_name_value.setText(contactName)
            customer_name_value.dismissDropDown()

            typeObserver()
        }
    }

    private fun buttonListener() {
        back_link.setOnClickListener {
            onBackPressed()
        }

        start_value.setOnClickListener {
            dialogDateTime("start")
        }

        end_value.setOnClickListener {
            dialogDateTime("end")
        }

        btn_add_personel.setOnClickListener {
            addPersonel()
        }

        // ========================================================================================= Customer Signature
        customer_signature_edit.setOnClickListener {
            customer_signature_preview.visibility = View.GONE
            customer_signature_lay.visibility = View.VISIBLE
        }

        customer_signature_clear.setOnClickListener {
            customer_signature_pad.clear()
        }

        customer_signature_save.setOnClickListener {
            saveSignatureImage()
            customer_signature_preview.visibility = View.VISIBLE
            customer_signature_lay.visibility = View.GONE
        }

        btn_save.setOnClickListener {
            if (formValidation()) {
                sendTrainingForm()
            } else {
                Toast.makeText(this, "Please Fill all required columns", Toast.LENGTH_LONG).show()
            }
        }

    }

    @SuppressLint("SetTextI18n")
    private fun formValidation(): Boolean {
        var cek = 0

        // Information section
        if(desc_value.text.toString().isEmpty()){
            desc_err.text = "*Required"; cek++
        } else { desc_err.text = "" }

        if(start_value.text.toString().isEmpty()){
            start_err.text = "*Required"; cek++
        } else { start_err.text = "" }

        if(end_value.text.toString().isEmpty()){
            end_err.text = "*Required"; cek++
        } else { end_err.text = "" }

        if (trainingStartDate != null && trainingEndDate != null) {
            if (trainingStartDate!!.after(trainingEndDate)) {
                start_err.text = "*Start time must be earlier than end time"
                end_err.text = "*Start time must be earlier than end time"
                cek++
            }
        } else {
            start_err.text = "*Required"
            end_err.text = "*Required"
            cek++
        }

        if (trainingTypeId.isEmpty()) {
            checkbox_err.text = "*Required"; cek++
        } else {  checkbox_err.text = "" }

        // Personel
        if (personelAdapter.getAllItem().isEmpty()) {
            personel_err.text = "*Required"; cek++
        }

        if(engineer_value.text.toString().isEmpty()){
            engineer_err.text = "*Required"; cek++
        } else { engineer_err.text = "" }

        if(customer_name_value.text.toString().isEmpty()){
            customer_name_err.text = "*Required"; cek++
        } else { customer_name_err.text = "" }

//        if (customerSignatureName.isEmpty()) {
//            customer_signature_err.text = "*Required"; cek++
//        } else { customer_signature_err.text = "" }

        if (cek == 0){
            return true
        }
        return false
    }

    private fun setupAdapter() {
        recycler_training_type.layoutManager = GridLayoutManager(this, 2)
        recycler_training_type.adapter = typeAdapter

        recycler_equipment.layoutManager = LinearLayoutManager(this, RecyclerView.VERTICAL, false)
        recycler_equipment.adapter = equipmentAdapter

        recycler_personel.layoutManager = LinearLayoutManager(this, RecyclerView.VERTICAL, false)
        recycler_personel.adapter = personelAdapter
    }

    private fun typeAdapterListener() {
        typeAdapter.setEventHandler(object : TrainingTypeAdapter.RecyclerClickListener {
            override fun isClicked(typeEnumId: ArrayList<String>) {
                trainingTypeId = typeEnumId
            }
        })
    }

    private fun equipmentAdapterListener() {
        equipmentAdapter.setEventHandler(object : HandoverEquipmentAdapter.RecyclerClickListener {
            override fun isAdd(equipmentId: String) {
                equipmentData.add(equipmentId)
            }

            override fun isRemove(equipmentId: String) {
                equipmentData.remove(equipmentId)
            }

        })
    }

    private fun personelAdapterListener() {
        personelAdapter.registerAdapterDataObserver(object : RecyclerView.AdapterDataObserver() {
            override fun onChanged() {
                super.onChanged()
                if (personelAdapter.itemCount == 0) {
                    recycler_personel.visibility = View.GONE
                    warning_personel.visibility = View.VISIBLE
                } else {
                    recycler_personel.visibility = View.VISIBLE
                    warning_personel.visibility = View.GONE
                }
            }
        })
    }

    private fun addPersonel() {
        if (tx_personel.text.toString().isNotEmpty()) {
            personel_err.text = ""

            val data = PersonelModel(
                personel_value.text.toString(),
                spinner_lay.selectedItem.toString()
            )
            personelAdapter.addList(data)
            personel_value.setText("")
        } else {
            personel_err.text = "*Required"
        }
    }

    private fun typeObserver() {
        val enumLive = enumVM.getByTableAndColumn("service_call_training_form", "training_type")
        enumLive.observe(this, {
            enumLive.removeObservers(this)
            typeAdapter.setList(it)
        })
    }

    private fun equipmentObserver() {
        equipmentVM.getAttachToCall(callId, true).observe(this, {
            if (it.isNotEmpty()) {
                recycler_equipment.visibility = View.VISIBLE
                warning_equipment.visibility = View.GONE

                equipmentAdapter.setList(it)
            } else {
                recycler_equipment.visibility = View.GONE
                warning_equipment.visibility = View.VISIBLE
            }
        })
    }

    private fun engineerObserver() {
        val engineerList = ArrayList<GetDataRequest.Engineer>()
        callVM.getByCallId(callId).observe(this, {
            if (it.isNotEmpty()) {
                val engineers = it.first().engineer
                engineers?.forEach { engineer ->
                    if (engineer != null) {
                        engineerList.add(engineer)
                    }
                }
                autoTextEngineer(engineerList)
            }
        })
    }

    private fun autoTextEngineer(engineerList: ArrayList<GetDataRequest.Engineer>) {
        val adapter = AutoTextEngineerAdapter(this, R.layout.item_spinner_black, engineerList)
        engineer_value.setAdapter(adapter)
        engineer_value.threshold = 1
        engineer_value.onItemClickListener = AdapterView.OnItemClickListener{ parent, view, position, id->
            val engineerData = parent.getItemAtPosition(position) as GetDataRequest.Engineer
            engineerId = engineerData.id ?: ""
            engineer_value.setText(engineerData.userFullname ?: "")
        }
        engineer_value.onFocusChangeListener = View.OnFocusChangeListener{ view, b ->
            if(b && engineer_value.text.isEmpty()) { engineer_value.showDropDown() }
        }
    }

    private fun customerObserver() {
        val contactList = ArrayList<AccountContactEntity>()
        contactVM.getByAccountId(accountId).observe(this, {
            if (it.isNotEmpty()) {
                it.forEach { contact ->
                    contactList.add(contact)
                }
                autoTextCustomer(contactList)
            }
        })
    }

    private fun autoTextCustomer(contactList: ArrayList<AccountContactEntity>) {
        val adapter = AutoTextContactAdapter(this, R.layout.item_spinner_black, contactList)
        customer_name_value.setAdapter(adapter)
        customer_name_value.threshold = 1
        customer_name_value.onItemClickListener = AdapterView.OnItemClickListener{ parent, view, position, id->
            val contactData = parent.getItemAtPosition(position) as AccountContactEntity
            contactId = contactData.account_contact_id
            customer_name_value.setText(contactData.contact_person_name ?: "")

        }
        customer_name_value.onFocusChangeListener = View.OnFocusChangeListener{ view, b ->
            if(b && customer_name_value.text.isEmpty()) { customer_name_value.showDropDown() }
        }
    }

    private fun dialogDateTime(dateTimeStatus: String) {
        val view = layoutInflater.inflate(R.layout.date_time_picker_activity, null)
        val dialog = BottomSheetDialog(this)
        dialog.setContentView(view)

        val bottomSheetBehavior = BottomSheetBehavior.from<View>(view.parent as View)
        dialog.setOnShowListener { bottomSheetBehavior.state = BottomSheetBehavior.STATE_EXPANDED }

        val txTitle = dialog.findViewById<TextView>(R.id.tx_title)
        val timeValue = dialog.findViewById<TimePicker>(R.id.time_value)
        val dateValue = dialog.findViewById<CalendarView>(R.id.date_value)
        val btnSet = dialog.findViewById<TextView>(R.id.btn_set)

        var timeSelected = ""
        var dateSelected = ""

        when(dateTimeStatus) {
            "date" -> {
                timeValue?.visibility = View.GONE
                txTitle?.text = "Choose Date"
            }
            "timeStart" -> {
                dateValue?.visibility = View.GONE
                txTitle?.text = "Choose Time Start"
            }
            "timeEnd" -> {
                dateValue?.visibility = View.GONE
                txTitle?.text = "Choose Time End"
            }
        }

        timeValue?.setIs24HourView(true)

        val interval = 15
        val formater = DecimalFormat("00")
        val numValues = 60 / interval
        val displayedValues = arrayOfNulls<String>(numValues)
        for (i in 0 until numValues) {
            displayedValues[i] = formater.format(i * interval)
        }

        var minutePicker: NumberPicker? = null
        val minuteNumberPicker = timeValue?.findViewById<NumberPicker>(
            Resources.getSystem().getIdentifier(
                "minute",
                "id",
                "android"
            )
        )
        if (minuteNumberPicker != null) {
            minutePicker = minuteNumberPicker
            minutePicker.minValue = 0
            minutePicker.maxValue = numValues - 1
            minutePicker.displayedValues = displayedValues
        }

        timeValue?.setOnTimeChangedListener{ timePicker: TimePicker, hour: Int, minute: Int ->
            if (minutePicker != null) {
                timeSelected = "${formater.format(hour)}:${formater.format(minutePicker.value * interval)}:00"
            }
        }

        dateValue?.setOnDateChangeListener{ calendarView: CalendarView, year: Int, month: Int, date: Int ->
            dateSelected = "$year-${DecimalFormat("00").format(month+1)}-${DecimalFormat("00").format(date)}"
        }

        btnSet?.setOnClickListener {
            val nowServer = serverFormat.format(Date()).split(" ")
            val rightNow = Calendar.getInstance()
            val hour = rightNow[Calendar.HOUR_OF_DAY]

            if (minutePicker != null && timeSelected.isEmpty()) {
                timeSelected = "${formater.format(hour)}:${formater.format(minutePicker.value * interval)}:00"
            }

            val data = "${dateSelected.ifEmpty { nowServer[0] }} $timeSelected"
            when (dateTimeStatus) {
                "start" -> {
                    trainingStartDate =
                        serverFormat.parse("${dateSelected.ifEmpty { nowServer[0] }} $timeSelected")
                    start_value.text = FunHelper.uiDateFormat(data)
                }
                "end" -> {
                    trainingEndDate =
                        serverFormat.parse("${dateSelected.ifEmpty { nowServer[0] }} $timeSelected")
                    end_value.text = FunHelper.uiDateFormat(data)
                }
            }

            dialog.dismiss()

        }

        dialog.show()
    }

    private fun spinnerPersonel(){
        val spinnerData = arrayOf(
            "Operator",
            "Operator Supervisor",
            "Maintenance",
            "Maintenance Supervisor",
            "Operational Manager",
            "Maintenance Manager",
            "Other"
        )

        val arrayAdapter = ArrayAdapter(
            this,
            android.R.layout.simple_spinner_dropdown_item,
            spinnerData
        )
        spinner_lay?.adapter = arrayAdapter
    }

    private fun saveSignatureImage() = runWithPermissions(
        FunHelper.storageWritePermission,
        FunHelper.storageReadPermission
    ) {
        val signatureBitmap = customer_signature_pad.signatureBitmap

        if (addJpgSignatureToGallery(signatureBitmap)) {
            customer_signature_pad.clear()

            Toast.makeText(this, "Signature saved", Toast.LENGTH_SHORT).show()
        } else {
            Toast.makeText(this, "Signature save failed", Toast.LENGTH_SHORT).show()
        }
    }

    private fun addJpgSignatureToGallery(signature: Bitmap?): Boolean {
        var result = false
        try {

            val dataUri = ArrayList<ConverterImageModel>()
            customerSignatureFile = File(
                this.getExternalFilesDir(Environment.DIRECTORY_PICTURES),
                String.format("SRSIGN-%d.jpg", System.currentTimeMillis())
            )
            saveBitmapToJPG(signature!!, customerSignatureFile)

            val uri = FileProvider.getUriForFile(
                this,
                "com.syspex.fileprovider",
                customerSignatureFile!!
            )

            customerSignatureName = getFileName(uri)
            dataUri.add(ConverterImageModel(customerSignatureName, uri.toString()))

            try { Glide.with(this).load(uri.toString()).into(customer_signature_priview_image) }
            catch (e: Exception) { }

            result = true
        } catch (e: IOException) {
            e.printStackTrace()
        }
        return result
    }

    private fun getFileName(uri: Uri): String {
        var result: String? = null
        if (uri.scheme == "content") {
            val cursor = this.contentResolver?.query(uri, null, null, null, null)
            try {
                if (cursor != null && cursor.moveToFirst()) {
                    result = cursor.getString(cursor.getColumnIndex(OpenableColumns.DISPLAY_NAME))
                }
            } catch (e: Exception) { }
            cursor?.close()
        }
        if (result == null) {
            result = uri.path
            val cut = result!!.lastIndexOf('/')
            if (cut != -1) {
                result = result.substring(cut + 1)
            }
        }
        Log.e("aim", "signature name: $result")
        return result
    }

    @Throws(IOException::class)
    fun saveBitmapToJPG(bitmap: Bitmap, photo: File?) {
        try {
            val newBitmap = Bitmap.createBitmap(
                bitmap.width,
                bitmap.height,
                Bitmap.Config.ARGB_8888
            )
            val canvas = Canvas(newBitmap)
            canvas.drawColor(Color.WHITE)
            canvas.drawBitmap(bitmap, 0f, 0f, null)
            val stream: OutputStream = FileOutputStream(photo!!)
            newBitmap.compress(Bitmap.CompressFormat.JPEG, 80, stream)
            stream.close()
        } catch (e: Exception) { Log.e("aim", e.toString()) }
    }

    private fun sendTrainingForm() {
        val progressDialog = ProgressDialog(this)
        progressDialog.setMessage("Send Training Form ...")
        progressDialog.setCancelable(false)
        progressDialog.show()

        val token = "Bearer "+ SharedPreferenceData.getString(this, 2, "")
        val userId = SharedPreferenceData.getString(this, 8, "")

        val personelName = JSONArray()
        val designation = JSONArray()
        personelAdapter.getAllItem().forEach {
            personelName.put(it.personelName)
            designation.put(it.designation)
        }

        val equipmentArray = JSONArray()
        equipmentData.forEach {
            equipmentArray.put(it)
        }

        val typeString = trainingTypeId.toString()
            .replace("[","")
            .replace("]","")

        val params = HashMap<String, RequestBody>()
        params["call_id"] = toRequestBody(callId)
        params["training_personel_name"] = toRequestBody(personelName.toString())
        params["training_designation"] = toRequestBody(designation.toString())
        params["customer_id"] = toRequestBody(accountId)
        params["address_id"] = toRequestBody(addressId)
        params["training_start_date"] = toRequestBody(serverFormat.format(trainingStartDate!!))
        params["training_end_date"] = toRequestBody(serverFormat.format(trainingEndDate!!))
        params["training_type"] = toRequestBody(typeString)
        params["training_detail"] = toRequestBody(desc_value.text.toString())
        params["account_contact_id"] = toRequestBody(contactId)
        params["lead_engineer"] = toRequestBody(engineerId)
        params["equipments"] = toRequestBody(equipmentArray.toString())

        if (customerSignatureFile != null) {
            val signature2: RequestBody =
                RequestBody.create(MediaType.parse("image/*"), customerSignatureFile!!)
            params["customer_sign\"; filename=\"${customerSignatureFile!!.name}"] = signature2
        }

        Log.e("aim", "params: $params")

        ApiClient.instance.sendTraining(token, params).enqueue(object :
            Callback<TrainingAddRequest> {
            override fun onFailure(call: Call<TrainingAddRequest>, t: Throwable) {
                progressDialog.dismiss()
                Toast.makeText(this@TrainingAdd, "Send training form failed", Toast.LENGTH_LONG)
                    .show()
            }

            override fun onResponse(call: Call<TrainingAddRequest>, response: Response<TrainingAddRequest>) {
                progressDialog.dismiss()
                if (!response.isSuccessful) {
                    if (response.code() == 401) {
                        FunHelper.clearAllData(this@TrainingAdd)
                        Toast.makeText(this@TrainingAdd, "Session end", Toast.LENGTH_LONG)
                            .show()
                    }
                    else {
                        FunHelper.firebaseApiLog(
                            userId ?: "",
                            response.raw().request().url().toString() ?: "",
                            response.raw().request().headers().toString() ?: "",
                            params.toString() ?: "",
                            response.raw().toString() ?: ""
                        )
                        Toast.makeText(this@TrainingAdd, response.raw().toString(), Toast.LENGTH_LONG).show()
                    }
                } else {
                    callTrainingVM.insert(ServiceCallTrainingEntity(
                        response.body()?.data?.training?.id ?: "",
                        response.body()?.data?.training?.callId ?: "",
                        response.body()?.data?.training?.addressId ?: "",
                        response.body()?.data?.training?.trainingFormNumber ?: "",
                        response.body()?.data?.training?.trainingType ?: "",
                        response.body()?.data?.training?.trainingStartDate ?: "",
                        response.body()?.data?.training?.trainingDetail ?: "",
                        response.body()?.data?.training?.engineerId ?: "",
                        response.body()?.data?.training?.engineerSignatureUrl ?: "",
                        response.body()?.data?.training?.customerPicName ?: "",
                        response.body()?.data?.training?.customerPicPosition ?: "",
                        response.body()?.data?.training?.customerSignatureUrl ?: "",
                        response.body()?.data?.training?.officerName ?: "",
                        response.body()?.data?.training?.createdDate ?: "",
                        response.body()?.data?.training?.createdBy ?: "",
                        response.body()?.data?.training?.lastModifiedDate ?: "",
                        response.body()?.data?.training?.lastModifiedBy ?: "",
                        response.body()?.data?.training?.trainingEndDate ?: "",
                        response.body()?.data?.training?.accountContactId ?: ""
                    ))

                    Toast.makeText(this@TrainingAdd, "Training form sent", Toast.LENGTH_LONG).show()
                    onBackPressed()
                }
            }
        })
    }

    private fun toRequestBody(value: String): RequestBody {
        return RequestBody.create(MultipartBody.FORM, value)
    }

}