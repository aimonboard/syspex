package com.syspex.ui

import android.annotation.SuppressLint
import android.app.ProgressDialog
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.TextView
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.syspex.R
import com.syspex.data.local.SharedPreferenceData
import com.syspex.data.local.database.account_contact.AccountContactEntity
import com.syspex.data.local.database.account_contact.AccountContactViewModel
import com.syspex.data.local.database.chargeable_type.ChargeableEntity
import com.syspex.data.local.database.chargeable_type.ChargeableViewModel
import com.syspex.data.local.database.global_enum.EnumViewModel
import com.syspex.data.local.database.product_checklist_question.ChecklistQuestionViewModel
import com.syspex.data.local.database.product_checklist_template.ChecklistTemplateViewModel
import com.syspex.data.local.database.product_group_item.ProductGroupItemViewModel
import com.syspex.data.local.database.service_call.ServiceCallViewModel
import com.syspex.data.local.database.service_call_equipment.ServiceCallEquipmentEntity
import com.syspex.data.local.database.service_call_equipment.ServiceCallEquipmentViewModel
import com.syspex.data.local.database.service_case.ServiceCaseViewModel
import com.syspex.data.local.database.service_report.ServiceReportEntity
import com.syspex.data.local.database.service_report.ServiceReportViewModel
import com.syspex.data.local.database.service_report_equipment.ServiceReportEquipmentEntity
import com.syspex.data.local.database.service_report_equipment.ServiceReportEquipmentViewModel
import com.syspex.data.local.database.service_report_equipment_checklist.ServiceReportEquipmentChecklistEntity
import com.syspex.data.local.database.service_report_equipment_checklist.ServiceReportEquipmentChecklistViewModel
import com.syspex.data.remote.GetDataRequest
import com.syspex.ui.adapter.SRContactAdapter
import com.syspex.ui.helper.FunHelper
import kotlinx.android.synthetic.main.activity_add.*
import kotlinx.android.synthetic.main.service_report_add_activity.*
import kotlinx.android.synthetic.main.service_report_add_activity.account_pic_contact_name
import kotlinx.android.synthetic.main.service_report_add_activity.account_pic_contact_phone
import kotlinx.android.synthetic.main.service_report_add_activity.account_pic_contact_type
import kotlinx.android.synthetic.main.service_report_add_activity.btn_save
import kotlinx.android.synthetic.main.service_report_detail_activity.*
import kotlinx.android.synthetic.main.sub_header.*
import kotlinx.android.synthetic.main.sub_header.back_link
import kotlinx.android.synthetic.main.sub_header.header_name

class ServiceReportAdd : AppCompatActivity() {

    private lateinit var caseVM: ServiceCaseViewModel
    private lateinit var callVM: ServiceCallViewModel
    private lateinit var reportVM: ServiceReportViewModel
    private lateinit var contactVM: AccountContactViewModel
    private lateinit var enumVM: EnumViewModel
    private lateinit var chargeableVM: ChargeableViewModel

    private lateinit var callEquipmentVM: ServiceCallEquipmentViewModel
    private lateinit var reportEquipmentVM: ServiceReportEquipmentViewModel
    private lateinit var productGroupItemVM: ProductGroupItemViewModel
    private lateinit var checklistTemplateMaster: ChecklistTemplateViewModel
    private lateinit var checklistQuestionMaster: ChecklistQuestionViewModel
    private lateinit var reportChecklistVM: ServiceReportEquipmentChecklistViewModel

    private val contactAdapter = SRContactAdapter()

    // Spinner status data
    private val spinnerId = mutableListOf<String>()
    private val spinnerName = mutableListOf<String>()

    // Spinner chargeable data
    private val spinnerChargeableData = mutableListOf<ChargeableEntity>()
    private lateinit var selectedChargebleData: ChargeableEntity

    // Auto add equipment data
    private var equipmentData: ServiceReportEquipmentEntity? = null

    private var isEdit: Boolean = false
    private var userId = ""
    private var userName = ""
    private var userRegion = ""
    private var reportId = ""

    private var callId = ""
    private var caseId = ""
    private var accountId = ""
    private var accountName = ""
    private var fieldPicId = ""
    private var fieldPicName = ""
    private var fieldPicPhone = ""
    private var spinnerStatusId = ""
    private var spinnerStatusText = ""
    private var reportTypeId = ""
    private var reportTypeText = ""
    private var reportEquipmentId = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.service_report_add_activity)

        caseVM = ViewModelProviders.of(this).get(ServiceCaseViewModel::class.java)
        callVM = ViewModelProviders.of(this).get(ServiceCallViewModel::class.java)
        reportVM = ViewModelProviders.of(this).get(ServiceReportViewModel::class.java)
        contactVM = ViewModelProviders.of(this).get(AccountContactViewModel::class.java)
        enumVM = ViewModelProviders.of(this).get(EnumViewModel::class.java)
        chargeableVM = ViewModelProviders.of(this).get(ChargeableViewModel::class.java)

        callEquipmentVM = ViewModelProviders.of(this).get(ServiceCallEquipmentViewModel::class.java)
        reportEquipmentVM = ViewModelProviders.of(this).get(ServiceReportEquipmentViewModel::class.java)
        productGroupItemVM = ViewModelProviders.of(this).get(ProductGroupItemViewModel::class.java)
        checklistTemplateMaster = ViewModelProviders.of(this).get(ChecklistTemplateViewModel::class.java)
        checklistQuestionMaster = ViewModelProviders.of(this).get(ChecklistQuestionViewModel::class.java)
        reportChecklistVM = ViewModelProviders.of(this).get(ServiceReportEquipmentChecklistViewModel::class.java)

        spinnerCharge()
        spinnerReportStatus()

        generateView()
        getSupportData()
        buttonListener()

    }

    @SuppressLint("SetTextI18n")
    private fun generateView() {
        if (intent.hasExtra("callId")) {
            // insert report
            isEdit = false
            header_name?.text = "Add Master Service Report"
            head_contact.visibility = View.GONE
            contact_lay.visibility = View.GONE

            userId = SharedPreferenceData.getString(this,8,"")
            userName = SharedPreferenceData.getString(this,10,"")
            userRegion = SharedPreferenceData.getString(this,14,"")

            caseId = intent.getStringExtra("caseId") ?: ""
            callId = intent.getStringExtra("callId") ?: ""

            reportId = FunHelper.randomString("")
            reportEquipmentId = FunHelper.randomString("")

            val callNumber = intent.getStringExtra("callNumber") ?: ""
            service_report_value?.text = FunHelper.reportNumber(this, callNumber, userId)
            service_call_value?.text = intent.getStringExtra("callNumber")
            service_by_value?.text = SharedPreferenceData.getString(this, 10, "")
            date_value?.text = FunHelper.uiDateFormat(FunHelper.getDate(true))

            autoAddEquipment()

        } else {
            // Update report
            isEdit = true
            header_name?.text = "Edit Master Service Report"
            head_contact.visibility = View.VISIBLE
            contact_lay.visibility = View.VISIBLE
            accountId = intent.getStringExtra("accountId") ?: ""
            accountName = intent.getStringExtra("accountName") ?: ""
            reportId = intent.getStringExtra("reportId") ?: ""
            val callNumber = intent.getStringExtra("callNumber") ?: ""

            getDataForUpdate(callNumber)
            contactObserver()
        }
    }

    private fun buttonListener() {
        back_link.setOnClickListener {
            onBackPressed()
        }

        btn_add_contact.setOnClickListener {
            val i = Intent(it.context, ContactAdd::class.java)
            i.putExtra("accountId",accountId)
            i.putExtra("accountName",accountName)
            startActivity(i)
        }

        btn_update_contact.setOnClickListener {
            contactDialog()
        }

        btn_save.setOnClickListener {
            if (isEdit) {
                updateReport()
            } else {
                insertReport()
            }
        }
    }

    private fun spinnerReportStatus() {
        enumVM.getByTableName("service_report").observe(this, Observer { enum->
            if (enum.isNotEmpty()) {
                enum.forEach {
                    if (isEdit) {
                        spinnerId.add(it.enum_id)
                        spinnerName.add(it.enum_name)
                    } else {
                        if (it.enum_slug == "draft") {
                            spinnerId.add(it.enum_id)
                            spinnerName.add(it.enum_name)
                        }
                    }
                }

                val arrayAdapter = ArrayAdapter(this, R.layout.item_spinner_gray, spinnerName)
                spinner_status?.adapter = arrayAdapter

                spinner_status?.onItemSelectedListener = object : AdapterView.OnItemSelectedListener{
                    override fun onNothingSelected(parent: AdapterView<*>?) { }
                    override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                        spinnerStatusId = spinnerId[position]
                        spinnerStatusText = spinnerName[position]
                    }
                }
            }
        })
    }

    private fun spinnerCharge() {
        chargeableVM.getAll().observe(this, Observer{ chargeableData->
            if (chargeableData.isNotEmpty()) {
                val spinnerData = ArrayList<String>()
                spinnerChargeableData.clear()
                chargeableData.forEach {
                    if (it.isShow == "1") {
                        spinnerData.add(it.value)
                        spinnerChargeableData.add(it)
                    }
                }

                val arrayAdapter = ArrayAdapter(this, R.layout.item_spinner_black, spinnerData)
                spinner_charge?.adapter = arrayAdapter

                spinner_charge?.onItemSelectedListener = object : AdapterView.OnItemSelectedListener{
                    override fun onNothingSelected(parent: AdapterView<*>?) { }
                    override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                        selectedChargebleData = spinnerChargeableData[position]
                    }
                }
            }
        })
    }

    private fun getSupportData() {
        val dialog = ProgressDialog(this)
        dialog.setTitle("Load data ...")
        dialog.setCancelable(false)
        dialog.show()

        val callObserver = callVM.getByCallId(callId)
        callObserver.observe(this, Observer {
            callObserver.removeObservers(this)

            if (it.isNotEmpty()) {
                accountId = it.first().accountId
                accountName = it.first().accountName

                fieldPicId = it.first().fieldAccountContactId
                fieldPicName = it.first().fieldAccountContactName
                fieldPicPhone = it.first().fieldAccountContactPhone

                reportTypeId = it.first().callTypeEnumId
                reportTypeText = it.first().callTypeEnumText

                val isChargeable = it.first().isChargeable
                if (isChargeable.isNotEmpty()) {
                    spinnerChargeableData.forEachIndexed { index, chargeable ->
                        if (chargeable.key == isChargeable ) {
                            spinner_charge.setSelection(index)
                        }
                    }
                }
            }
            dialog.dismiss()
        })
    }

    private fun getDataForUpdate(callNumber: String) {
        val reportObserver = reportVM.getByReportId(reportId)
        reportObserver.observe(this, Observer { reportData->
            reportObserver.removeObservers(this)

            if (reportData.isNotEmpty()) {
                service_report_value?.text = reportData.first().serviceReportNumber
                service_call_value?.text = callNumber
                service_by_value?.text = reportData.first().servicedByUserName
                date_value?.text = reportData.first().createdDate
                memo_value?.setText(reportData.first().internalMemo)

                // PIC Contact
                getContact(
                    reportData.first().fieldAccountContactId,
                    account_pic_contact_name,
                    account_pic_contact_phone,
                    account_pic_contact_type,
                )

                // Spinner status
                spinnerId.forEachIndexed { index, it ->
                    if (it == reportData.first().reportStatusEnumId) {
                        spinner_status.setSelection(index)
                    }
                }

                // Spinner Charge
                val isChargeable = reportData.first().isChargeable
                if (isChargeable.isNotEmpty()) {
                    spinnerChargeableData.forEachIndexed { index, chargeable ->
                        if (chargeable.key == isChargeable ) {
                            spinner_charge.setSelection(index)
                        }
                    }
                }
            }
        })
    }

    private fun getContact(contactId: String, name: TextView, phone: TextView, type: TextView) {
        val contactLive = contactVM.getById(contactId)
        contactLive.observe(this, Observer {
            contactLive.removeObservers(this)

            if (it.isNotEmpty()) {
                name.text = it.first().contact_person_name.ifEmpty { "-" }
                phone.text = it.first().contact_person_phone.ifEmpty { "-" }
                type.text = it.first().contact_type.ifEmpty { "-" }
            }
        })
    }

    private fun contactObserver() {
        contactVM.getByAccountId(accountId).observe(this, Observer {
            contactAdapter.setList(it)
        })
    }

    private fun contactDialog() {
        val view = layoutInflater.inflate(R.layout.dialog_list_filter,null)
        val dialog = BottomSheetDialog(this)
        dialog.setContentView(view)
        val bottomSheetBehavior = BottomSheetBehavior.from<View>(view.parent as View)
        dialog.setOnShowListener { bottomSheetBehavior.state = BottomSheetBehavior.STATE_EXPANDED }

        val btnClear = dialog.findViewById<TextView>(R.id.btn_clear)
        btnClear?.visibility = View.GONE

        val recyclerFilter = dialog.findViewById<RecyclerView>(R.id.recycler_filter)
        recyclerFilter?.layoutManager = LinearLayoutManager(this, RecyclerView.VERTICAL, false)
        recyclerFilter?.adapter = contactAdapter

        contactAdapter.setEventHandler(object: SRContactAdapter.RecyclerClickListener {
            override fun isClicked(data: AccountContactEntity) {
                fieldPicId = data.account_contact_id
                fieldPicName = data.contact_person_name
                fieldPicPhone = data.contact_person_phone

                // PIC Contact
                getContact(
                    fieldPicId,
                    account_pic_contact_name,
                    account_pic_contact_phone,
                    account_pic_contact_type,
                )

                dialog.dismiss()
            }
        })

        dialog.show()
    }

    private fun autoAddEquipment() {
        val checkCall = callEquipmentVM.getAttachToCall(callId, true)
        checkCall.observe(this, Observer{ callEquipmentData ->
            checkCall.removeObservers(this@ServiceReportAdd)
            if (callEquipmentData.size == 1) {

                val checkReport = reportEquipmentVM.checkAttach(callId, callEquipmentData.first().equipmentId, true)
                checkReport.observe(this, Observer {
                    checkReport.removeObservers(this@ServiceReportAdd)
                    if (it.isEmpty()) {
                        equipmentData = callEquipmentData.first().toReportEquipment()
                    }
                })

            }
        })
    }

    private fun insertReport() {
        reportVM.insert(ServiceReportEntity(
            reportId ?: "",
            "",
            caseId ?: "",
            accountId ?: "",
            accountName ?: "",
            callId ?: "",
            service_report_value.text.toString() ?: "",
            FunHelper.getDate(true) ?: "",
            memo_value.text.toString() ?: "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            selectedChargebleData.key ?: "",
            "",
            "",
            FunHelper.getDate(false) ?: "",
            FunHelper.getDate(false) ?: "",
            service_report_value.text.toString() ?: "",
            "",
            "",
            fieldPicId ?: "",
            fieldPicName ?: "",
            fieldPicPhone ?: "",
            userId ?: "",
            userName ?: "",
            userId ?: "",
            userName ?: "",
            userId ?: "",
            userName ?: "",
            spinnerStatusId ?: "",
            spinnerStatusText ?: "",
            reportTypeId ?: "",
            reportTypeText ?: "",
            "",
            "",
            "",
            timeTracking = emptyList(),
            engineer = generateEngineer(),
            equipment = emptyList(),
            isUpload = false,
            isLocal = true
        ))

        if (equipmentData != null) {
            // Auto add equipment
            reportEquipmentVM.insert(equipmentData!!)

            // Auto add checklist
            getCallType(equipmentData!!.product_id, reportEquipmentId)
        } else {
            // Normal flow
            allProsesFinish()
        }
    }

    private fun updateReport() {
        reportVM.updateMaster(
            reportId ?: "",
            memo_value.text.toString() ?: "",
            selectedChargebleData.key ?: "",
            fieldPicId ?: "",
            fieldPicName ?: "",
            fieldPicPhone ?: "",
            false
        )

        onBackPressed()
    }

    // ============================================================================================= Insert Checklist
    private fun getCallType(productId: String, reportEquipmnetId: String) {
        Log.e("aim", "productId : $productId")

        callVM.getByCallId(callId).observe(this, Observer { callData->
            if (callData.isNotEmpty()) {
                if (callData.first().callTypeEnumText == "Installation" || callData.first().callTypeEnumText == "Maintenance") {
                    insertChecklist(productId, reportEquipmnetId, callData.first().callTypeEnumText)
                } else {
                    allProsesFinish()
                }
            }
        })
    }

    private fun insertChecklist(productId: String, reportEquipmentId: String, checklistTypeText: String) {
        productGroupItemVM.getByProductId(productId).observe(this, Observer {
            if (it.isNotEmpty()) {
                val productGroupId = it.first().product_group_id
                checklistTemplateMaster.getTemplateByProductGroupIdAndType(productGroupId, checklistTypeText).observe(this, Observer { templateArray ->
                    templateArray.forEach { templateData ->
                        if (userRegion == templateData.regionId) {

                            Log.e(
                                "aim",
                                "template type : ${templateData.checklistTypeTextEnumName}"
                            )

                            reportEquipmentVM.editTemplateId(
                                reportEquipmentId,
                                templateData.productGroupChecklistTemplateId
                            )

                            checklistQuestionMaster.getByTemplate(templateData.productGroupChecklistTemplateId)
                                .observe(this, Observer { questionArray ->

                                    questionArray.forEach { questionData ->

                                        Log.e(
                                            "aim",
                                            "question section : ${questionData.sectionTitle}"
                                        )

                                        reportChecklistVM.insert(
                                            ServiceReportEquipmentChecklistEntity(
                                                FunHelper.randomString("") ?: "",
                                                reportEquipmentId ?: "",
                                                "",
                                                "",
                                                FunHelper.getDate(false) ?: "",
                                                userId ?: "",
                                                "",
                                                "",
                                                "",
                                                "",
                                                questionData.questionId ?: "",
                                                questionData.orderNo ?: "",
                                                questionData.sectionTitle ?: "",
                                                questionData.question_description ?: "",
                                                questionData.defaultRemarks ?: "",
                                                questionData.helpText ?: "",
                                                questionData.helpImageUrl ?: "",
                                                questionData.templateId ?: "",
                                                templateData.productGroupChecklistTemplateId ?: "",
                                                templateData.regionId ?: "",
                                                templateData.productGroupId ?: "",
                                                templateData.checklistTypeTextEnumId ?: "",
                                                templateData.checklistTypeTextEnumName ?: "",
                                                emptyList(),
                                                true
                                            )
                                        )
                                    }
                                })

                        }
                    }
                })
            }

            allProsesFinish()
        })
    }

    private fun allProsesFinish() {
        val i = Intent(this, ServiceReportDetail::class.java)
        i.putExtra("id", reportId)
        startActivity(i)

        finish()
    }

    private fun generateEngineer(): ArrayList<GetDataRequest.Engineer> {
        val dataEngineer = ArrayList<GetDataRequest.Engineer>()
        dataEngineer.add(
            GetDataRequest.Engineer(
                id = userId,
                regionId = "",
                userCode = "",
                userFirstName = userName,
                userLastName = "",
                email = "",
                emailVerifiedAt = "",
                phone = "",
                roleId = "",
                createdDate = "",
                createdBy = "",
                lastModifiedDate = "",
                lastModifiedBy = "",
                userFullname = "",
                role = null,
                region = null,
                pivot = null
            )
        )

        return dataEngineer
    }

    private fun ServiceCallEquipmentEntity.toReportEquipment() = ServiceReportEquipmentEntity(
        serviceReportEquipmentId = reportEquipmentId,
        serviceReportEquipmentCallId = callId,
        reportId = reportId,
        equipmentId = equipmentId,
        problemSymptom = "",
        statusAfterServiceEnumId = "",
        statusAfterServiceEnumName = "",
        statusAfterServiceRemarks = "",
        installationStartDate = "",
        installationMatters = "",
        checklistTemplateId = "",
        createdDate = "",
        createdBy = "",
        lastModifiedDate = "",
        lastModifiedBy = "",
        equipmentName = "",
        regionId = regionId,
        serialNo = serialNo,
        brand = brand,
        warrantyStartDate = warrantyStartDate,
        warrantyEndDate = warrantyEndDate,
        equipmentRemarks = equipmentRemarks,
        deliveryAddressId = deliveryAddressId,
        salesOrderNo = salesOrderNo,
        warrantyStatus = warrantyStatus,
        accountId = accountId,
        accountName = accountName,
        agreementId = agreementId,
        agreementNumber = agreementNumber,
        agreementEnd = agreementEnd,
        statusTextEnumId = statusTextEnumId,
        statusTextEnumText = statusTextEnumText,
        product_id = product_id,
        product_name = product_name,
        isAttach = true,
        isUpload = false,
        isLocal = true
    )
}
