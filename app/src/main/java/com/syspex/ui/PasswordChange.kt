package com.syspex.ui

import android.annotation.SuppressLint
import android.app.ProgressDialog
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.android.material.textfield.TextInputLayout
import com.syspex.R
import com.syspex.data.local.SharedPreferenceData
import com.syspex.data.remote.LoginRequest
import com.syspex.data.retrofit.ApiClient
import com.syspex.ui.helper.FunHelper
import kotlinx.android.synthetic.main.password_change_activity.*
import kotlinx.android.synthetic.main.profile_fragment.*
import kotlinx.android.synthetic.main.sub_header.*
import okhttp3.ResponseBody
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class PasswordChange : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.password_change_activity)

        header_name?.text = "Change Password"
        buttonListener()

    }

    private fun buttonListener() {
        back_link.setOnClickListener {
            onBackPressed()
        }

        btn_forgot.setOnClickListener {
            val i = Intent(this, PasswordForgot::class.java)
            startActivity(i)
        }

        btn_save.setOnClickListener {
            if (check()) {
                dialogConfirmation()
            }
        }
    }

    private fun check(): Boolean{
        var cek = 0

        if(current_value.text.toString().isEmpty()){
            current_lay.error = "Please enter your current password"; cek++
        } else { current_lay.error = "" }

        if(new_value.text.toString().isEmpty()){
            new_lay.error = "Please enter your new password"; cek++
        } else { new_lay.error = "" }

        if(retype_value.text.toString() == ""){
            retype_lay.error = "Please retype password"; cek++
        } else {
            if(new_value.text.toString() != retype_value.text.toString()){
                retype_lay.error = "Password doesn’t match"; cek++
            } else { retype_lay.error = "" }
        }

        if (cek == 0){
            return true
        }
        return false
    }

    @SuppressLint("SetTextI18n")
    private fun dialogConfirmation() {
        val view = layoutInflater.inflate(R.layout.dialog_confirmation,null)
        val dialog = BottomSheetDialog(this)
        dialog.setContentView(view)

        val txTitle = dialog.findViewById<TextView>(R.id.tx_title)
        val txBody = dialog.findViewById<TextView>(R.id.tx_body)
        val buttonYes = dialog.findViewById<TextView>(R.id.btn_yes)
        val buttonNo = dialog.findViewById<TextView>(R.id.btn_no)

        txTitle?.text = "Change Password"
        txBody?.text = "Are you sure you want to save\nthis new password?"

        buttonYes?.setOnClickListener {
            dialog.dismiss()
            changePassword()
        }

        buttonNo?.setOnClickListener {
            dialog.dismiss()
        }

        dialog.show()
    }

    private fun changePassword() {
        val progressDialog = ProgressDialog(this)
        progressDialog.setMessage("Change password ...")
        progressDialog.setCancelable(false)
        progressDialog.show()

        val token = "Bearer "+ SharedPreferenceData.getString(this@PasswordChange,2, "")
        val userId = SharedPreferenceData.getString(this@PasswordChange,8, "")
        val params = HashMap<String, String>()
        params["old_password"] = current_value.text.toString()
        params["new_password"] = new_value.text.toString()
        params["new_password_confirmation"] = retype_value.text.toString()

        ApiClient.instance.updatePassword(token, params).enqueue(object: Callback<ResponseBody> {
            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                progressDialog.dismiss()
                Toast.makeText(this@PasswordChange, "Change password failed", Toast.LENGTH_LONG).show()
            }

            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                progressDialog.dismiss()
                if (response.isSuccessful) {
                    Toast.makeText(this@PasswordChange, "Change password done", Toast.LENGTH_LONG).show()
                    onBackPressed()
                }
                else {
                    if (response.code() == 401) {
                        FunHelper.clearAllData(this@PasswordChange)
                        Toast.makeText(this@PasswordChange, "Session end", Toast.LENGTH_LONG).show()
                    }
                    else {
                        FunHelper.firebaseApiLog(
                            userId ?: "",
                            response.raw().request().url().toString() ?: "",
                            response.raw().request().headers().toString() ?: "",
                            params.toString() ?: "",
                            response.raw().toString() ?: ""
                        )
                        Toast.makeText(this@PasswordChange, response.raw().toString(), Toast.LENGTH_LONG).show()
                    }
                }
            }
        })
    }


    private fun dialogDone(title: String, body: String) {
        val view = layoutInflater.inflate(R.layout.dialog_done,null)
        val dialog = BottomSheetDialog(this)
        dialog.setContentView(view)

        val txTitle = dialog.findViewById<TextView>(R.id.tx_title)
        val txBody = dialog.findViewById<TextView>(R.id.tx_body)
        val buttonOk = dialog.findViewById<TextView>(R.id.btn_ok)

        txTitle?.text = title
        txBody?.text = body

        buttonOk?.setOnClickListener {
            onBackPressed()
        }

        dialog.show()
    }

}