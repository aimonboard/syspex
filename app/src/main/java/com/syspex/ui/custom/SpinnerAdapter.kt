package com.syspex.ui.custom

import android.content.Context
import android.graphics.Color
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.BaseAdapter
import android.widget.TextView
import androidx.annotation.LayoutRes
import com.syspex.R
import com.syspex.data.model.SpinnerModel
import kotlinx.android.synthetic.main.item_spinner_gray.view.*


class SpinnerAdapter(context: Context,@LayoutRes private val layoutResource: Int,private val values: List<SpinnerModel>)
    : ArrayAdapter<SpinnerModel>(context, layoutResource, values) {

    override fun getItem(position: Int): SpinnerModel = values[position]

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        val view = createViewFromResource(convertView, parent, layoutResource)

        return bindData(getItem(position), view)
    }

    override fun getDropDownView(position: Int, convertView: View?, parent: ViewGroup): View {
        val view = createViewFromResource(convertView, parent, android.R.layout.simple_spinner_dropdown_item)

        return bindData(getItem(position), view)
    }

    private fun createViewFromResource(convertView: View?, parent: ViewGroup, layoutResource: Int): TextView {
        val context = parent.context
        val view = convertView ?: LayoutInflater.from(context).inflate(layoutResource, parent, false)
        return view as TextView
//        return try {
//            if (textViewResourceId == 0) view as TextView
//            else {
//                view.findViewById(textViewResourceId) ?:
//                throw RuntimeException("Failed to find view with ID " +
//                        "${context.resources.getResourceName(textViewResourceId)} in item layout")
//            }
//        } catch (ex: ClassCastException){
//            Log.e("CustomArrayAdapter", "You must supply a resource ID for a TextView")
//            throw IllegalStateException(
//                "ArrayAdapter requires the resource ID to be a TextView", ex)
//        }
    }

    private fun bindData(value: SpinnerModel, view: TextView): TextView {
        view.text = value.spinnerName
        return view
    }
}