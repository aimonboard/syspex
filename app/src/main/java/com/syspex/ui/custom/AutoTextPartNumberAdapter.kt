package com.syspex.ui.custom

import android.annotation.SuppressLint
import android.content.Context
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.Filter
import androidx.annotation.LayoutRes
import java.util.*


class AutoTextPartNumberAdapter(
    context: Context,
    @LayoutRes private val layoutResource: Int,
    private val movies: ArrayList<String>
) : ArrayAdapter<String>(context, layoutResource, movies) {

    var filteredMovies = movies

    override fun getCount(): Int = filteredMovies.size

    override fun getItem(position: Int): String = filteredMovies[position]

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        return super.getView(position, convertView, parent)
    }

    override fun getFilter(): Filter {
        return object : Filter() {
            override fun publishResults(charSequence: CharSequence?, filterResults: FilterResults) {
                @Suppress("UNCHECKED_CAST")
                filteredMovies = filterResults.values as ArrayList<String>
                notifyDataSetChanged()
            }

            override fun performFiltering(charSequence: CharSequence?): FilterResults {
                val queryString = charSequence?.toString()?.toLowerCase(Locale.getDefault())

                val filterResults = FilterResults()
                filterResults.values = if (queryString == null || queryString.isEmpty())
                    movies
                else
                    movies.filter {
                        it.toLowerCase(Locale.getDefault()).contains(queryString)
                    }
                return filterResults
            }
        }
    }
}