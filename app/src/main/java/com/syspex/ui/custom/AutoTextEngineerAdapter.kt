package com.syspex.ui.custom

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.Filter
import android.widget.TextView
import androidx.annotation.LayoutRes
import com.syspex.R
import com.syspex.data.remote.GetDataRequest
import java.util.*


class AutoTextEngineerAdapter(
    context: Context,
    @LayoutRes private val layoutResource: Int,
    private val movies: ArrayList<GetDataRequest.Engineer>
) : ArrayAdapter<GetDataRequest.Engineer>(context, layoutResource, movies) {

    var filteredMovies = movies

    override fun getCount(): Int = filteredMovies.size

    override fun getItem(position: Int): GetDataRequest.Engineer = filteredMovies[position]

    @SuppressLint("SetTextI18n")
    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        val view = super.getView(position, convertView, parent)
        val textView = view.findViewById<TextView>(R.id.text)

        textView.text = filteredMovies[position].userFullname

        return view
    }

    override fun getFilter(): Filter {
        return object : Filter() {
            override fun publishResults(charSequence: CharSequence?, filterResults: FilterResults) {
                @Suppress("UNCHECKED_CAST")
                filteredMovies = filterResults.values as ArrayList<GetDataRequest.Engineer>
                notifyDataSetChanged()
            }

            override fun performFiltering(charSequence: CharSequence?): FilterResults {
                val queryString = charSequence?.toString()?.toLowerCase(Locale.getDefault())

                val filterResults = FilterResults()
                filterResults.values = if (queryString == null || queryString.isEmpty())
                    movies
                else
                    movies.filter {
                        it.userFullname!!.toLowerCase(Locale.getDefault()).contains(queryString)
                    }
                return filterResults
            }
        }
    }
}