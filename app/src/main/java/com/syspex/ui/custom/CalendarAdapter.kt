package com.syspex.ui.custom

import android.annotation.SuppressLint
import android.widget.TextView
import android.content.Context
import android.graphics.Color
import android.util.Log
import android.view.ViewGroup
import android.view.LayoutInflater
import android.view.View
import android.widget.ArrayAdapter
import android.widget.LinearLayout
import com.syspex.R
import java.text.SimpleDateFormat
import java.util.*

class CalendarAdapter(context: Context) : ArrayAdapter<Date>(context, R.layout.calendar_layout_cell) {

    private lateinit var testCal: Calendar
    private lateinit var monthlyDates: List<Date>
    private lateinit var allEvents: ArrayList<String>

    private val mInflater: LayoutInflater = LayoutInflater.from(context)

    @SuppressLint("SimpleDateFormat")
    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        var mView = convertView

        // calendar by cell (include prev and next month date)
        val systemCalendar = Calendar.getInstance()
        val date = getItem(position)
        systemCalendar.time = date!!
        val systemDate = systemCalendar.get(Calendar.DATE)
        val systemMonth = systemCalendar.get(Calendar.MONTH)
        val systemYear = systemCalendar.get(Calendar.YEAR)

        // calendar by ui (only date in this month)
        val nowMonth = testCal.get(Calendar.MONTH)

        // today calendar
        val sdf = SimpleDateFormat("dd-MM-yyyy")
        val now = sdf.format(Date()).split("-")

        if (mView == null) {
            mView = mInflater.inflate(R.layout.calendar_layout_cell, parent, false)

        }

        // Inflate cell layout
        (mView as LinearLayout).setBackgroundResource(android.R.color.white)

        // Reset cell view
        val txTanggal = mView.findViewById<TextView>(R.id.calendar_date_id)
        val eventIndicator = mView.findViewById<TextView>(R.id.event_id)
        eventIndicator.visibility = View.INVISIBLE

        if (systemMonth == nowMonth) {
            // if this day is outside current month, grey it out
            txTanggal.setTextColor(Color.parseColor("#000000"))
        }

        if (systemDate == now[0].toInt() && systemMonth == now[1].toInt()-1 && systemYear == now[2].toInt()) {
            // if it is today, set it to blue/bold
            txTanggal.setBackgroundResource(R.drawable.shape_circle_solid_blue)
            txTanggal.setTextColor(Color.parseColor("#FFFFFF"))
        }

        // set text
        txTanggal.text = systemCalendar.get(Calendar.DATE).toString()

        //Add events to the calendar
        for (i in 0 until allEvents.size) {
            val event = allEvents[i].split("-")
            val eventDate = event[0].toInt()
            val eventMonth = event[1].toInt()
            val eventYear = event[2].toInt()

            if (systemDate == eventDate && systemMonth == eventMonth-1 && systemYear == eventYear) {
                eventIndicator.visibility = View.VISIBLE
            }
        }

        return mView
    }

    override fun getCount(): Int {
        return monthlyDates.size
    }

    override fun getItem(position: Int): Date? {
        return monthlyDates[position]
    }

    fun setCalendar(testCal: Calendar, monthlyDates: List<Date>, allEvents: ArrayList<String>) {
        this.testCal = testCal
        this.monthlyDates = monthlyDates
        this.allEvents = allEvents
    }

//    override fun getPosition(item: Any?): Int {
//        return monthlyDates.indexOf(item)
//    }
}