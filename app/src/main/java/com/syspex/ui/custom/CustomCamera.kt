package com.syspex.ui.custom

import android.app.Activity
import android.hardware.Camera
import android.hardware.Camera.PictureCallback
import android.os.Bundle
import android.os.Environment
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.FrameLayout
import com.syspex.R
import kotlinx.android.synthetic.main.activity_custom_camera.*
import java.io.File
import java.io.FileNotFoundException
import java.io.FileOutputStream
import java.io.IOException
import java.text.SimpleDateFormat
import java.util.*


class CustomCamera : Activity() {
    private var mCamera: Camera? = null
    private var mCameraPreview: CustomCameraPreview? = null

    /** Called when the activity is first created.  */
    public override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_custom_camera)
        mCamera = cameraInstance
        mCameraPreview = CustomCameraPreview(this, mCamera)
        val preview =
            findViewById<View>(R.id.camera_preview) as FrameLayout
        preview.addView(mCameraPreview)


        button_capture.setOnClickListener {
            mCamera?.takePicture(null, null, mPicture)
        }
    }

    /**
     * Helper method to access the camera returns null if it cannot get the
     * camera or does not exist
     *
     * @return
     */
    private val cameraInstance: Camera?
        get() {
            var camera: Camera? = null
            try {
                camera = Camera.open()
            } catch (e: Exception) {
                // cannot get camera or does not exist
            }
            return camera
        }

    var mPicture = PictureCallback { data, camera ->
        val pictureFile: File = outputMediaFile ?: return@PictureCallback
        try {
            val fos = FileOutputStream(pictureFile)
            fos.write(data)
            fos.close()
        } catch (e: FileNotFoundException) {
        } catch (e: IOException) {
        }
    }

    companion object {
        // Create a media file name
        private val outputMediaFile: File?
            get() {
                val mediaStorageDir = File(
                    Environment
                        .getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),
                    "MyCameraApp"
                )
                if (!mediaStorageDir.exists()) {
                    if (!mediaStorageDir.mkdirs()) {
                        Log.d("MyCameraApp", "failed to create directory")
                        return null
                    }
                }
                // Create a media file name
                val timeStamp: String = SimpleDateFormat("yyyyMMdd_HHmmss", Locale.getDefault())
                    .format(Date())
                val mediaFile: File
                mediaFile = File(
                    mediaStorageDir.getPath() + File.separator
                        .toString() + "IMG_" + timeStamp + ".jpg"
                )
                return mediaFile
            }
    }
}