package com.syspex.ui

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.syspex.R
import com.syspex.data.local.database.service_call_equipment.ServiceCallEquipmentEntity
import com.syspex.data.local.database.service_call_equipment.ServiceCallEquipmentViewModel
import com.syspex.data.local.database.service_report_equipment.ServiceReportEquipmentEntity
import com.syspex.data.local.database.service_report_equipment.ServiceReportEquipmentViewModel
import kotlinx.android.synthetic.main.dialog_equipment_add.*

class DialogEquipmentAdd : BottomSheetDialogFragment() {

    companion object {
        var reportId = ""
        var reportNumber = ""
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,savedInstanceState: Bundle?): View? {
        // Set dialog initial state when shown
        dialog?.setOnShowListener {
            val bottomSheetDialog = it as BottomSheetDialog
            val sheetInternal: View = bottomSheetDialog.findViewById(com.google.android.material.R.id.design_bottom_sheet)!!
            BottomSheetBehavior.from(sheetInternal).state = BottomSheetBehavior.STATE_EXPANDED
        }

        return inflater.inflate(R.layout.dialog_equipment_add, container, false)
    }

    @SuppressLint("SetTextI18n")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        tx_title.text = "This equipment used in service report. " +
                "Delete this equipment from service report first"

        btn_report.text = "Go to this Service Report $reportNumber"

        btn_report.setOnClickListener {
            if (reportId.isNotEmpty() && reportNumber.isNotEmpty()) {
                val i = Intent(it.context, ServiceReportDetail::class.java)
                i.putExtra("id", reportId)
                startActivity(i)
            }
        }

    }
}
