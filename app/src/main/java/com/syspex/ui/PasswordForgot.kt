package com.syspex.ui

import android.annotation.SuppressLint
import android.app.ProgressDialog
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import com.syspex.R
import com.syspex.data.local.SharedPreferenceData
import com.syspex.data.retrofit.ApiClient
import com.syspex.ui.helper.FunHelper
import kotlinx.android.synthetic.main.password_forgot_activity.*
import kotlinx.android.synthetic.main.password_forgot_activity.btn_forgot
import kotlinx.android.synthetic.main.sub_header.*
import okhttp3.ResponseBody
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class PasswordForgot : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.password_forgot_activity)

        generateView()
        buttonListener()

    }

    @SuppressLint("SetTextI18n")
    private fun generateView() {
        header_name.text = "Forgot Password"
        email_value.setText(SharedPreferenceData.getString(this,11, ""))
    }

    private fun buttonListener() {
        back_link.setOnClickListener {
            onBackPressed()
        }

        btn_forgot.setOnClickListener {
            if (check()) {
                forgotPass()
            }
        }
    }

    private fun check(): Boolean{
        var cek = 0

        if(email_value.text.toString() == ""){
            email_lay.error = "Please enter your email"; cek++
        } else { email_lay.error = "" }

        if (cek == 0){
            return true
        }
        return false
    }

    private fun forgotPass() {
        val progressDialog = ProgressDialog(this)
        progressDialog.setMessage("Reset password ...")
        progressDialog.setCancelable(false)
        progressDialog.show()

        val userId = SharedPreferenceData.getString(this@PasswordForgot,8, "")
        val params = HashMap<String, String>()
        params["email"] = email_value.text.toString()

        ApiClient.instance.forgotPassword(params).enqueue(object: Callback<ResponseBody> {
            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                progressDialog.dismiss()
                Toast.makeText(this@PasswordForgot, "Reset password failed", Toast.LENGTH_LONG).show()
            }

            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                progressDialog.dismiss()
                if (response.isSuccessful) {
                    Toast.makeText(this@PasswordForgot, "Reset password done, check your email", Toast.LENGTH_LONG).show()
                    onBackPressed()
                }
                else {
                    if (response.code() == 401) {
                        FunHelper.clearAllData(this@PasswordForgot)
                        Toast.makeText(this@PasswordForgot, "Session end", Toast.LENGTH_LONG).show()
                    }
                    else {
                        FunHelper.firebaseApiLog(
                            userId ?: "",
                            response.raw().request().url().toString() ?: "",
                            response.raw().request().headers().toString() ?: "",
                            params.toString() ?: "",
                            response.raw().toString() ?: ""
                        )
                        Toast.makeText(this@PasswordForgot, response.raw().toString(), Toast.LENGTH_LONG).show()
                    }
                }
            }
        })
    }
}
