package com.syspex.ui

import android.app.Activity
import android.app.ProgressDialog
import android.content.Intent
import android.content.res.Resources
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.core.view.GravityCompat
import androidx.drawerlayout.widget.DrawerLayout
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.syspex.R
import com.syspex.data.local.SharedPreferenceData
import com.syspex.data.local.database.account.AccountEntity
import com.syspex.data.local.database.account.AccountViewModel
import com.syspex.data.local.database.account_contact.AccountContactEntity
import com.syspex.data.local.database.account_contact.AccountContactViewModel
import com.syspex.data.local.database.account_equipment.AccountEquipmentViewModel
import com.syspex.data.local.database.global_enum.EnumViewModel
import com.syspex.data.retrofit.ApiClient
import com.syspex.ui.adapter.CallAddEquipmentAdapter
import com.syspex.ui.custom.AutoTextAccountAdapter
import com.syspex.ui.custom.AutoTextPicAdapter
import com.syspex.ui.helper.FunHelper
import kotlinx.android.synthetic.main.service_call_add_menu_fragment.*
import okhttp3.ResponseBody
import org.json.JSONArray
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.text.DecimalFormat
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList

class ServiceCallAddMenu : Fragment() {

    private lateinit var accountVM: AccountViewModel
    private lateinit var accountContactVM: AccountContactViewModel
    private lateinit var accountEquipmentVM: AccountEquipmentViewModel
    private lateinit var callTypeVM: EnumViewModel

    private val equipmentAdapter = CallAddEquipmentAdapter()
    private var equipmentData = ArrayList<String>()

    private val onlineResult = 24321
    private var accountId = ""
    private var accountPicId = ""
    private var callTypeId = ""
    private var selectedDate = ""

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.service_call_add_menu_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        accountVM = ViewModelProviders.of(this).get(AccountViewModel::class.java)
        accountContactVM = ViewModelProviders.of(this).get(AccountContactViewModel::class.java)
        accountEquipmentVM = ViewModelProviders.of(this).get(AccountEquipmentViewModel::class.java)
        callTypeVM = ViewModelProviders.of(this).get(EnumViewModel::class.java)

        buttonListener()
        setupAdapter()
        equipmentAdapterListener()

        accountObserver()
        callTypeObserver()

    }

    // SearchAccountOnline Result
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == onlineResult) {
            if (resultCode == Activity.RESULT_OK) {
                accountId = data?.getStringExtra("accountId") ?: ""
                customer_value.setText(data?.getStringExtra("accountName") ?: "")
                customer_value.dismissDropDown()

                accountPicObserver(accountId)
                accountEquipmentObserver(accountId)
            }
        }
    }

    private fun buttonListener() {
        val drawerLayout: DrawerLayout = requireActivity().findViewById(R.id.drawer_layout)
        btn_menu?.setOnClickListener {
            if(!drawerLayout.isDrawerOpen(GravityCompat.START))
                drawerLayout.openDrawer(GravityCompat.START)
        }

        btn_search_online.setOnClickListener {
            val i = Intent(requireContext(), SearchAccountOnline::class.java)
            startActivityForResult(i, onlineResult)
        }

        date_value.setOnClickListener {
            dialogDateTime("date")
        }
        time_start_value.setOnClickListener {
            dialogDateTime("timeStart")
        }
        time_end_value.setOnClickListener {
            dialogDateTime("timeEnd")
        }

        btn_save.setOnClickListener {
            if (checkValue()) {
                if (checkDate()) {
                    requestServiceCall()
                } else {
                    Toast.makeText(
                        context,
                        "Start time must be earlier than end time",
                        Toast.LENGTH_LONG
                    ).show()
                }
            } else {
                Toast.makeText(requireContext(), "Please fill all column", Toast.LENGTH_LONG).show()
            }
        }
    }

    private fun checkValue(): Boolean{
        var cek = 0

        if(customer_value.text.toString().isEmpty()){
            customer_err.text = "*Required"; cek++
        } else { customer_err.text = "" }

        if(subject_value.text.toString().isEmpty()){
            subject_err.text = "*Required"; cek++
        } else { subject_err.text = "" }

        if(date_value.text.toString().isEmpty()){
            date_err.text = "*Required"; cek++
        } else { date_err.text = "" }

        if(time_start_value.text.toString().isEmpty()){
            time_start_err.text = "*Required"; cek++
        } else { time_start_err.text = "" }

        if(time_end_value.text.toString().isEmpty()){
            time_end_err.text = "*Required"; cek++
        } else { time_end_err.text = "" }

        if(equipmentData.isEmpty()){
            serial_err.text = "*Required"; cek++
        } else { serial_err.text = "" }

        if(partner_value.text.toString().isEmpty()){
            partner_err.text = "*Required"; cek++
        } else { partner_err.text = "" }

        if(pic_value.text.toString().isEmpty()){
            pic_err.text = "*Required"; cek++
        } else { pic_err.text = "" }

        if (cek == 0){
            return true
        }
        return false
    }

    private fun checkDate(): Boolean {
        val startString = time_start_value.text.toString().split(":")
        val cStart = Calendar.getInstance()
        cStart[Calendar.HOUR_OF_DAY] = startString[0].toInt()
        cStart[Calendar.MINUTE] = startString[1].toInt()

        val endString = time_end_value.text.toString().split(":")
        val cEnd = Calendar.getInstance()
        cEnd[Calendar.HOUR_OF_DAY] = endString[0].toInt()
        cEnd[Calendar.MINUTE] = endString[1].toInt()

        if (cEnd.before(cStart)) {
            return false
        }
        return true
    }

    private fun setupAdapter() {
        recycler_equipment.layoutManager = LinearLayoutManager(
            requireContext(),
            RecyclerView.VERTICAL,
            false
        )
        recycler_equipment.adapter = equipmentAdapter
    }

    private fun equipmentAdapterListener() {
        equipmentAdapter.setEventHandler(object : CallAddEquipmentAdapter.RecyclerClickListener {
            override fun isAdd(equipmentId: String) {
                equipmentData.add(equipmentId)
            }

            override fun isRemove(equipmentId: String) {
                equipmentData.remove(equipmentId)
            }

        })
    }

    private fun accountObserver() {
        accountVM.getAll().observe(viewLifecycleOwner, {
            autoTextAccount(it as ArrayList<AccountEntity>)
        })
    }

    private fun autoTextAccount(accountList: ArrayList<AccountEntity>) {
        val adapter = AutoTextAccountAdapter(
            requireContext(),
            R.layout.item_spinner_black,
            accountList
        )
        customer_value.setAdapter(adapter)
        customer_value.threshold = 1
        customer_value.onItemClickListener = AdapterView.OnItemClickListener{ parent, view, position, id->
            val accountData = parent.getItemAtPosition(position) as AccountEntity
            accountId = accountData.accountId
            customer_value.setText(accountData.account_name ?: "")

            accountPicObserver(accountId)
            accountEquipmentObserver(accountId)

        }
        customer_value.onFocusChangeListener = View.OnFocusChangeListener{ view, b ->
            if(b && customer_value.text.isEmpty()) { customer_value.showDropDown() }
        }

        customer_value.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}
            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}

            override fun afterTextChanged(p0: Editable?) {
                if (p0 != null && p0.toString().isEmpty()) {
                    pic_value.setText("")
                    pic_value.dismissDropDown()
                }
            }
        })
    }

    private fun accountPicObserver(accountId: String) {
        val contactLive = accountContactVM.getByAccountId(accountId)
        contactLive.observe(viewLifecycleOwner, {
            autoTextAccountPic(it as ArrayList<AccountContactEntity>)
        })
    }

    private fun autoTextAccountPic(contactList: ArrayList<AccountContactEntity>) {
        val adapter = AutoTextPicAdapter(requireContext(), R.layout.item_spinner_black, contactList)
        pic_value.setAdapter(adapter)
        pic_value.threshold = 1
        pic_value.onItemClickListener = AdapterView.OnItemClickListener{ parent, view, position, id->
            val contactData = parent.getItemAtPosition(position) as AccountContactEntity
            accountPicId = contactData.account_contact_id
            pic_value.setText(contactData.contact_person_name ?: "")
        }
        pic_value.onFocusChangeListener = View.OnFocusChangeListener{ view, b ->
            if(b && pic_value.text.isEmpty()) { pic_value.showDropDown() }
        }
    }

    private fun accountEquipmentObserver(accountId: String) {
        val equipmentLive = accountEquipmentVM.getByAccount(accountId)
        equipmentLive.observe(viewLifecycleOwner, {
            equipmentLive.removeObservers(viewLifecycleOwner)

            equipmentAdapter.setList(it)
        })
    }

    private fun callTypeObserver() {
        val typeId = ArrayList<String>()
        val typeName = ArrayList<String>()
        callTypeVM.getByTableAndColumn("service_call", "call_type")
            .observe(viewLifecycleOwner, {
                typeId.clear()
                typeName.clear()

                it.forEach { typeData ->
                    typeId.add(typeData.enum_id)
                    typeName.add(typeData.enum_name)
                }
                spinnerTypeListener(typeId, typeName)
            })
    }

    private fun spinnerTypeListener(typeId: ArrayList<String>, typeName: ArrayList<String>){
        val arrayAdapter = ArrayAdapter(
            requireContext(),
            android.R.layout.simple_spinner_dropdown_item,
            typeName
        )
        spinner_value?.adapter = arrayAdapter

        spinner_value?.onItemSelectedListener = object : AdapterView.OnItemSelectedListener{
            override fun onNothingSelected(parent: AdapterView<*>?) { }
            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {
                callTypeId = typeId[position]
            }
        }
    }

    private fun dialogDateTime(dateTimeStatus: String) {
        val view = layoutInflater.inflate(R.layout.date_time_picker_activity, null)
        val dialog = BottomSheetDialog(requireContext())
        dialog.setContentView(view)

        val bottomSheetBehavior = BottomSheetBehavior.from<View>(view.parent as View)
        dialog.setOnShowListener { bottomSheetBehavior.state = BottomSheetBehavior.STATE_EXPANDED }

        val txTitle = dialog.findViewById<TextView>(R.id.tx_title)
        val timeValue = dialog.findViewById<TimePicker>(R.id.time_value)
        val dateValue = dialog.findViewById<CalendarView>(R.id.date_value)
        val btnSet = dialog.findViewById<TextView>(R.id.btn_set)

        var timeSelected = ""
        var dateSelected = ""

        when(dateTimeStatus) {
            "date" -> {
                timeValue?.visibility = View.GONE
                txTitle?.text = "Choose Date"
            }
            "timeStart" -> {
                dateValue?.visibility = View.GONE
                txTitle?.text = "Choose Time Start"
            }
            "timeEnd" -> {
                dateValue?.visibility = View.GONE
                txTitle?.text = "Choose Time End"
            }
        }

        timeValue?.setIs24HourView(true)
        val interval = 15
        val formater = DecimalFormat("00")
        val numValues = 60 / interval
        val displayedValues = arrayOfNulls<String>(numValues)
        for (i in 0 until numValues) {
            displayedValues[i] = formater.format(i * interval)
        }

        var minutePicker: NumberPicker? = null
        val minuteNumberPicker = timeValue?.findViewById<NumberPicker>(
            Resources.getSystem().getIdentifier(
                "minute",
                "id",
                "android"
            )
        )
        if (minuteNumberPicker != null) {
            minutePicker = minuteNumberPicker
            minutePicker.minValue = 0
            minutePicker.maxValue = numValues - 1
            minutePicker.displayedValues = displayedValues
        }


        timeValue?.setOnTimeChangedListener{ timePicker: TimePicker, hour: Int, minute: Int ->
            if (minutePicker != null) {
                timeSelected = "${formater.format(hour)}:${formater.format(minutePicker.value * interval)}"
            }
        }

        dateValue?.setOnDateChangeListener{ calendarView: CalendarView, year: Int, month: Int, date: Int ->
            dateSelected = "$year-${DecimalFormat("00").format(month + 1)}-${DecimalFormat("00").format(
                date
            )}"
        }

        btnSet?.setOnClickListener {

            val sdf = SimpleDateFormat("yyyy-MM-dd HH:mm", Locale.getDefault())
            val now = sdf.format(Date()).split(" ")

            when (dateTimeStatus) {
                "date" -> {
                    val data = dateSelected.ifBlank { now[0] }
                    selectedDate = data
                    date_value.text = FunHelper.uiDateFormat(data)
                }
                "timeStart" -> {
                    val data = timeSelected.ifBlank { now[1] }
                    time_start_value.text = data

                }
                "timeEnd" -> {
                    val data = timeSelected.ifBlank { now[1] }
                    time_end_value.text = data

                }
            }

            dialog.dismiss()

        }

        dialog.show()
    }

    private fun requestServiceCall() {
        val progressDialog = ProgressDialog(requireContext())
        progressDialog.setMessage("Call Request ...")
        progressDialog.setCancelable(false)
        progressDialog.show()

        val token = "Bearer "+ SharedPreferenceData.getString(requireContext(), 2, "")
        val userId = SharedPreferenceData.getString(requireContext(), 8, "")

        val equipmentArray = JSONArray()
        equipmentData.forEach {
            val equipmentObject = JSONObject()
            equipmentObject.put("equipment_id", it)
            equipmentObject.put("machine", "")
            equipmentObject.put("serial", "")
            equipmentArray.put(equipmentObject)
        }

        val params = HashMap<String, Any>()
        params["account_id"] = accountId
        params["engineer_id"] = userId
        params["account_pic_id"] = accountPicId
//        params["case_id"] = null
        params["call_type_id"] = callTypeId
        params["call_subject"] = subject_value.text.toString()
        params["call_date"] = selectedDate
        params["start_time"] = "${time_start_value.text}:00"
        params["end_time"] = "${time_end_value.text}:00"
        params["partner_name"] = partner_value.text.toString()
        params["equipment"] = equipmentArray
        Log.e("aim", "call request : $params")

        ApiClient.instance.callRequest(token, params).enqueue(object : Callback<ResponseBody> {
            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                progressDialog.dismiss()
                Toast.makeText(requireContext(), "Call request failed", Toast.LENGTH_LONG).show()
            }

            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                progressDialog.dismiss()
                if (!response.isSuccessful) {
                    if (response.code() == 401) {
                        FunHelper.clearAllData(requireContext())
                        Toast.makeText(context, "Session end", Toast.LENGTH_LONG).show()
                    } else {
                        FunHelper.firebaseApiLog(
                            userId ?: "",
                            response.raw().request().url().toString() ?: "",
                            response.raw().request().headers().toString() ?: "",
                            params.toString() ?: "",
                            response.raw().toString() ?: ""
                        )
                        Toast.makeText(context, response.raw().toString(), Toast.LENGTH_LONG).show()
                    }
                } else {
                    clearForm()
                    Toast.makeText(requireContext(), "Call request sent", Toast.LENGTH_LONG).show()
                }
            }
        })
    }

    private fun clearForm() {
        customer_value.text.clear()
        subject_value.text.clear()
        date_value.text = ""
        time_start_value.text = ""
        time_end_value.text = ""
        partner_value.text.clear()
        pic_value.text.clear()
    }
}
