package com.syspex.ui

import android.annotation.SuppressLint
import android.app.ProgressDialog
import android.graphics.Bitmap
import android.graphics.Canvas
import android.graphics.Color
import android.net.Uri
import android.os.Bundle
import android.os.Environment
import android.provider.OpenableColumns
import android.util.Log
import android.view.View
import android.widget.AdapterView
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.FileProvider
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.bumptech.glide.Glide
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.livinglifetechway.quickpermissions_kotlin.runWithPermissions
import com.syspex.R
import com.syspex.data.local.SharedPreferenceData
import com.syspex.data.local.database.account_contact.AccountContactEntity
import com.syspex.data.local.database.account_contact.AccountContactViewModel
import com.syspex.data.local.database.file.FileViewModel
import com.syspex.data.local.database.service_report.ServiceReportViewModel
import com.syspex.data.model.ConverterImageModel
import com.syspex.data.remote.SignAllRequest
import com.syspex.data.retrofit.ApiClient
import com.syspex.ui.custom.AutoTextPicAdapter
import com.syspex.ui.helper.*
import kotlinx.android.synthetic.main.feedback_edit_activity.*
import kotlinx.android.synthetic.main.feedback_edit_activity.btn_save
import kotlinx.android.synthetic.main.sub_header.*
import kotlinx.android.synthetic.main.sub_header.back_link
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.*

class FeedbackEdit : AppCompatActivity() {

    private lateinit var accountContactVM: AccountContactViewModel
    private lateinit var reportVM: ServiceReportViewModel
    private lateinit var fileVM: FileViewModel
    private lateinit var progressDialog: ProgressDialog

    private var accountContactData : AccountContactEntity? = null

    private var signAll = false
    private var signatureUri = ""
    private var signatureName = ""
    private var callId = ""
    private var reportId = ""
    private var accountId = ""

    @SuppressLint("SetTextI18n")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.feedback_edit_activity)

        accountContactVM = ViewModelProviders.of(this).get(AccountContactViewModel::class.java)
        reportVM = ViewModelProviders.of(this).get(ServiceReportViewModel::class.java)
        fileVM = ViewModelProviders.of(this).get(FileViewModel::class.java)

        progressDialog = ProgressDialog(this)
        progressDialog.setMessage("Sign All Report ...")
        progressDialog.setCancelable(false)

        generateView()
        buttonListener()

    }

    private fun generateView() {
        if (intent.hasExtra("reportId")) {
            header_name.text = "Customer Feedback"
            signAll = false
            reportId = intent.getStringExtra("reportId") ?: ""
            accountId = intent.getStringExtra("accountId") ?: ""

            getReport()
            accountPicObserver()
        } else {
            header_name.text = "Customer Feedback - All Reports"
            signAll = true
            callId = intent.getStringExtra("callId") ?: ""
            accountId = intent.getStringExtra("accountId") ?: ""

            pic_id_value.setText(intent.getStringExtra("picId"))
            pic_name_value.setText(intent.getStringExtra("picName"))

            preview_lay.visibility = View.GONE
            btn_preview.visibility = View.GONE

            accountPicObserver()
        }
    }

    private fun buttonListener() {
        back_link?.setOnClickListener {
            onBackPressed()
        }

        signature_edit.setOnClickListener {
            preview_lay.visibility = View.GONE
            signature_lay.visibility = View.VISIBLE
        }

        signature_clear.setOnClickListener {
            signature_pad.clear()
        }

        signature_save.setOnClickListener {
            saveSignatureImage()
            preview_lay.visibility = View.VISIBLE
            signature_lay.visibility = View.GONE
        }

        btn_preview?.setOnClickListener {
            val linkPdf = "http://54.254.15.142/service-report-pdf/detail/$reportId"
            showPdf(linkPdf)
        }

        btn_save?.setOnClickListener {
            if (formValidation()) {
                dialogConfirmation()
            } else {
                Toast.makeText(this, "Please fill all columns", Toast.LENGTH_LONG).show()
            }
        }
    }

    @SuppressLint("SetTextI18n")
    private fun dialogConfirmation() {
        val view = layoutInflater.inflate(R.layout.dialog_confirmation,null)
        val dialog = BottomSheetDialog(this)
        dialog.setContentView(view)

        val txTitle = dialog.findViewById<TextView>(R.id.tx_title)
        val txBody = dialog.findViewById<TextView>(R.id.tx_body)
        val buttonYes = dialog.findViewById<TextView>(R.id.btn_yes)
        val buttonNo = dialog.findViewById<TextView>(R.id.btn_no)

        txTitle?.text = "Customer Feedback"
        txBody?.text = "Are you sure you want to save\nthese changes to customer feedback?"

        buttonYes?.setOnClickListener {
            dialog.dismiss()
            if (signAll) {
                signAllReport()
            } else {
                insertFeedback()
            }
        }

        buttonNo?.setOnClickListener {
            dialog.dismiss()
        }

        dialog.show()
    }

//    private fun dialogDone() {
//        val view = layoutInflater.inflate(R.layout.dialog_done,null)
//        val dialog = BottomSheetDialog(this)
//        dialog.setContentView(view)
//
//        val txTitle = dialog.findViewById<TextView>(R.id.tx_title)
//        val txBody = dialog.findViewById<TextView>(R.id.tx_body)
//        val buttonOk = dialog.findViewById<TextView>(R.id.btn_ok)
//
//        txTitle?.text = "Customer Feedback Has Been Saved"
//        txBody?.text = "All changes has been saved to this customer feedback!"
//
//        buttonOk?.setOnClickListener {
//            onBackPressed()
//        }
//
//        dialog.show()
//    }

    private fun formValidation(): Boolean {
        if (rating_value.rating == 0.0f) {
            rating_err.visibility = View.VISIBLE
            return false
        } else rating_err.visibility = View.GONE

        if (signatureUri.isEmpty()) {
            signature_err.visibility = View.VISIBLE
            return false
        } else signature_err.visibility = View.GONE

        if (feedback.text.toString().isEmpty()) {
            feedback_err.visibility = View.VISIBLE
            return false
        } else feedback_err.visibility = View.GONE


        return true
    }

    private fun getReport() {
        reportVM.getByReportId(reportId).observe(this, Observer {
            pic_name_value.setText(it.first().fieldAccountContactName)
            pic_id_value.setText(it.first().fieldAccountContactId)
            feedback.setText(it.first().feedback)

            if (it.first().customerRating.isNotEmpty()) {
                val rating = it.first().customerRating.toFloat()
                rating_value.rating = rating
            } else {
                rating_value.rating = 0f
            }

            signatureUri = it.first().accountPicSignatureFileImage
            if (signatureUri.isNotEmpty()) {

                preview_lay.visibility = View.VISIBLE
                signature_lay.visibility = View.GONE

                try {
                    Glide.with(this).load(signatureUri).into(priview_image)
                } catch (e: Exception) { }


            } else if (signatureUri.isEmpty()) {

                preview_lay.visibility = View.GONE
                signature_lay.visibility = View.VISIBLE
            }

        })
    }

    private fun accountPicObserver() {
        val contactLive = accountContactVM.getByAccountId(accountId)
        contactLive.observe(this, {
            autoTextAccountPic(it as ArrayList<AccountContactEntity>)
        })
    }

    private fun autoTextAccountPic(contactList: ArrayList<AccountContactEntity>) {
        val adapter = AutoTextPicAdapter(this, R.layout.item_spinner_black, contactList)
        pic_name_value.setAdapter(adapter)
        pic_name_value.threshold = 1
        pic_name_value.onItemClickListener = AdapterView.OnItemClickListener{ parent, view, position, id->
            accountContactData = parent.getItemAtPosition(position) as AccountContactEntity
            pic_name_value.setText(accountContactData?.contact_person_name ?: "")
            pic_id_value.setText(accountContactData?.account_contact_id ?: "")
        }
        pic_name_value.onFocusChangeListener = View.OnFocusChangeListener{ view, b ->
            if(b && pic_name_value.text.isEmpty()) { pic_name_value.showDropDown() }
        }
    }

    private fun saveSignatureImage() = runWithPermissions(FunHelper.storageWritePermission, FunHelper.storageReadPermission) {
        val signatureBitmap: Bitmap = signature_pad.signatureBitmap
        if (addJpgSignatureToGallery(signatureBitmap)) {
            signature_pad.clear()
            Toast.makeText(this,"Signature saved", Toast.LENGTH_SHORT).show()
        } else {
            Toast.makeText(this, "Signature save failed", Toast.LENGTH_SHORT).show()
        }
    }

    private fun addJpgSignatureToGallery(signature: Bitmap?): Boolean {
        var result = false
        try {
            val photo = File(
                this.getExternalFilesDir(Environment.DIRECTORY_PICTURES),
                String.format("SRSIGN-%d.jpg", System.currentTimeMillis())
            )
            saveBitmapToJPG(signature!!, photo)

            val uri = FileProvider.getUriForFile(
                this,
                "com.syspex.fileprovider",
                photo
            )
            signatureUri = uri.toString()
            signatureName = getFileName(uri)

            Log.e("aim", "signature uri : $signatureUri")

            try {
                Glide.with(this).load(signatureUri).into(priview_image)
            } catch (e: Exception) { e.stackTrace }

            result = true
        } catch (e: IOException) {
            e.printStackTrace()
        }
        return result
    }

    private fun getFileName(uri: Uri): String {
        var result: String? = null
        if (uri.scheme == "content") {
            val cursor = this.contentResolver?.query(uri, null, null, null, null)
            try {
                if (cursor != null && cursor.moveToFirst()) {
                    result = cursor.getString(cursor.getColumnIndex(OpenableColumns.DISPLAY_NAME))
                }
            } catch (e: Exception) { }
            cursor?.close()
        }
        if (result == null) {
            result = uri.path
            val cut = result!!.lastIndexOf('/')
            if (cut != -1) {
                result = result.substring(cut + 1)
            }
        }
        Log.e("aim","signature name: $result")
        return result
    }

    @Throws(IOException::class)
    fun saveBitmapToJPG(bitmap: Bitmap, photo: File?) {
        try {
            val newBitmap = Bitmap.createBitmap(bitmap.width, bitmap.height, Bitmap.Config.ARGB_8888)
            val canvas = Canvas(newBitmap)
            canvas.drawColor(Color.WHITE)
            canvas.drawBitmap(bitmap, 0f, 0f, null)
            val stream: OutputStream = FileOutputStream(photo!!)
            newBitmap.compress(Bitmap.CompressFormat.JPEG, 80, stream)
            stream.close()
        } catch (e: Exception) { Log.e("aim", e.toString()) }
    }

    private fun showPdf(fileUrl: String) = runWithPermissions( FunHelper.storageWritePermission) {
        DownloadPdf(this, fileUrl).downloadNow(true)
    }

    private fun insertFeedback() {
        reportVM.updateFeedback(
            reportId ?: "",
            rating_value.rating.toInt().toString() ?: "",
            feedback.text.toString() ?: "",
            signatureName ?: "",
            signatureUri ?: "",
            pic_id_value.text.toString() ?: "",
            pic_name_value.text.toString() ?: "",
            false
        )

        uploadSignature()
        onBackPressed()
    }

    private fun signAllReport() {
        progressDialog.show()
        val token = "Bearer "+ SharedPreferenceData.getString(this, 2, "")
        val userId = SharedPreferenceData.getString(this, 8, "")

        val params = HashMap<String, String>()
        params["call_id"] = callId
        params["account_pic_signature"] = signatureName
        params["cp_number"] = pic_id_value.text.toString()
        params["cp_name"] = pic_name_value.text.toString()
        params["customer_rating"] = rating_value.rating.toInt().toString()
        params["feedback"] = feedback.text.toString()

        ApiClient.instance.signAllReport(token, params).enqueue(object: Callback<SignAllRequest> {
            override fun onFailure(call: Call<SignAllRequest>, t: Throwable) {
                progressDialog.dismiss()
                Toast.makeText(this@FeedbackEdit, "Sign All Report failed", Toast.LENGTH_LONG)
                    .show()
            }

            override fun onResponse(call: Call<SignAllRequest>, response: Response<SignAllRequest>) {
                if (!response.isSuccessful) {
                    progressDialog.dismiss()
                    if (response.code() == 401) {
                        FunHelper.clearAllData(this@FeedbackEdit)
                        Toast.makeText(this@FeedbackEdit, "Session end", Toast.LENGTH_LONG)
                            .show()
                    }
                    else {
                        FunHelper.firebaseApiLog(
                            userId ?: "",
                            response.raw().request().url().toString() ?: "",
                            response.raw().request().headers().toString() ?: "",
                            params.toString() ?: "",
                            response.raw().toString() ?: ""
                        )
                        Toast.makeText(this@FeedbackEdit, response.raw().toString(), Toast.LENGTH_LONG).show()
                    }
                } else {

                    uploadSignature()
                    SyncDataFcm(this@FeedbackEdit).getByCaseId(callId,"call", null)
                    Toast.makeText(this@FeedbackEdit, "Sign all success", Toast.LENGTH_LONG).show()
                    onBackPressed()
                }
            }
        })
    }

    private fun uploadSignature() {
        val dataUri = ArrayList<ConverterImageModel>()
        dataUri.add(ConverterImageModel(signatureName, signatureUri))
        val uploadService = UploadFile(this)
        uploadService.saveDB(dataUri, "signature")
    }

}