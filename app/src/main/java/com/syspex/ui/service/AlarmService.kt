package com.syspex.ui.service

import android.app.AlarmManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.os.Build
import com.syspex.ui.helper.FunHelper
import com.syspex.ui.receiver.AlarmReceiver
import com.syspex.ui.util.Constants
import com.syspex.ui.util.RandomUtil
import java.text.SimpleDateFormat
import java.util.*

class AlarmService(private val context: Context) {
    private val alarmManager: AlarmManager? = context.getSystemService(Context.ALARM_SERVICE) as AlarmManager?


    fun callAlarm(timeInMillis: Long, alarmRequest: Int, callId: String, callNumber: String, start: Date) {
        val sdf = SimpleDateFormat("hh:mm", Locale.getDefault())

        setAlarm(
            timeInMillis,
            callPendingIntent(
                getIntent().apply {
                    action = Constants.ACTION_SET_EXACT
                    putExtra(Constants.EXTRA_EXACT_ALARM_TIME, timeInMillis)
                    putExtra("callId", callId)
                    putExtra("callNumber", callNumber)
                    putExtra("start", sdf.format(start))
                },
                alarmRequest
            )
        )
    }

    //1 Week
    fun reportAlarm(timeInMillis: Long) {
        setAlarm(
            timeInMillis,
            reoprtPendingIntent(
                getIntent().apply {
                    action = Constants.ACTION_SET_REPETITIVE_EXACT
                    putExtra(Constants.EXTRA_EXACT_ALARM_TIME, timeInMillis)
                }
            )
        )
    }

    private fun callPendingIntent(intent: Intent, alarmRequest: Int) =
        PendingIntent.getBroadcast(
            context,
            alarmRequest,
            intent,
            PendingIntent.FLAG_UPDATE_CURRENT
        )

    private fun reoprtPendingIntent(intent: Intent) =
        PendingIntent.getBroadcast(
            context,
            FunHelper.alertReportRequest,
            intent,
            PendingIntent.FLAG_UPDATE_CURRENT
        )


    private fun setAlarm(timeInMillis: Long, pendingIntent: PendingIntent) {
        alarmManager?.let {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                alarmManager.setExactAndAllowWhileIdle(
                    AlarmManager.RTC_WAKEUP,
                    timeInMillis,
                    pendingIntent
                )
            } else {
                alarmManager.setExact(
                    AlarmManager.RTC_WAKEUP,
                    timeInMillis,
                    pendingIntent
                )
            }
        }
    }

    private fun getIntent() = Intent(context, AlarmReceiver::class.java)

    private fun getRandomRequestCode() = RandomUtil.getRandomInt()

}