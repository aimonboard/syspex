package com.syspex.ui

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.widget.CheckBox
import android.widget.ImageView
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.core.widget.NestedScrollView
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.android.material.tabs.TabLayout
import com.syspex.R
import com.syspex.data.local.database.account.AccountViewModel
import com.syspex.data.local.database.account_equipment.AccountEquipmentViewModel
import com.syspex.data.local.database.agreement.AgreementViewModel
import com.syspex.data.local.database.service_call.ServiceCallViewModel
import com.syspex.data.local.database.service_case.ServiceCaseViewModel
import com.syspex.data.local.database.service_report.ServiceReportViewModel
import com.syspex.ui.adapter.*
import com.syspex.ui.helper.FunHelper
import kotlinx.android.synthetic.main.search_local_activity.*

class SearchLocal : AppCompatActivity() {

    private lateinit var accountVM : AccountViewModel
    private lateinit var agreementVM: AgreementViewModel
    private lateinit var caseVM : ServiceCaseViewModel
    private lateinit var callVM : ServiceCallViewModel
    private lateinit var reportVM : ServiceReportViewModel
    private lateinit var equipmentVM : AccountEquipmentViewModel

    private val accountAdapter = AccountAdapter()
    private val agreementAdapter = AgreementAdapter()
    private val caseAdapter = CaseTeamAdapter()
    private val callAdapter = SCTeamCallAdapter()
    private val reportAdapter = SRTeamReportAdapter()
    private val equipmentAdapter = EquipmentListAdapter()

    private var isScrolling = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.search_local_activity)

        accountVM = ViewModelProviders.of(this).get(AccountViewModel::class.java)
        caseVM = ViewModelProviders.of(this).get(ServiceCaseViewModel::class.java)
        callVM = ViewModelProviders.of(this).get(ServiceCallViewModel::class.java)
        reportVM = ViewModelProviders.of(this).get(ServiceReportViewModel::class.java)
        agreementVM = ViewModelProviders.of(this).get(AgreementViewModel::class.java)
        equipmentVM = ViewModelProviders.of(this).get(AccountEquipmentViewModel::class.java)

        generateTab()
        tabListener()

        buttonListener()
        setupAdapter()
        searchListener()

        accountObserver()
        agreementObserver()
        caseObserver()
        callObserver()
        reportObserver()
        equipmentObserver()

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == FunHelper.scanRequest && data != null) {
            search_value.setText(if (data.hasExtra("scan_result")) {
                data.getStringExtra("scan_result") ?: ""
            } else {
                ""
            })
        }
    }

    private fun buttonListener() {
        back_link.setOnClickListener {
            onBackPressed()
        }

        btn_filter.setOnClickListener {
            dialogFilter()
        }

        btn_barcode?.setOnClickListener {
            val i = Intent(it.context, Scan::class.java)
            startActivityForResult(i, FunHelper.scanRequest)
        }


    }

    private fun generateTab() {
        tab_lay.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener{
            override fun onTabUnselected(tab: TabLayout.Tab?) { }
            override fun onTabReselected(tab: TabLayout.Tab?) { }

            override fun onTabSelected(tab: TabLayout.Tab?) {
                if (!isScrolling) {
                    when (tab_lay.selectedTabPosition) {
                        0 -> scroll_lay.smoothScrollTo(0, head_account.top)
                        1 -> scroll_lay.smoothScrollTo(0, head_agreement.top)
                        2 -> scroll_lay.smoothScrollTo(0, head_case.top)
                        3 -> scroll_lay.smoothScrollTo(0, head_call.top)
                        4 -> scroll_lay.smoothScrollTo(0, head_report.top)
                        5 -> scroll_lay.smoothScrollTo(0, head_equipment.top)
                    }
                }
            }

        })
    }

    private fun tabListener() {
        scroll_lay.setOnScrollChangeListener { v: NestedScrollView?, x: Int, y: Int, oldX: Int, oldY: Int ->
            if (y >= head_account.top && y <= recycler_account.bottom) {
                isScrolling = true
                tab_lay.getTabAt(0)?.select()
                isScrolling = false
            }
            else if (y >= head_agreement.top && y <= recycler_agreement.bottom) {
                isScrolling = true
                tab_lay.getTabAt(1)?.select()
                isScrolling = false
            }
            if (y >= head_case.top && y <= recycler_case.bottom){
                isScrolling = true
                tab_lay.getTabAt(2)?.select()
                isScrolling = false
            }
            else if (y >= head_call.top && y <= recycler_call.bottom) {
                isScrolling = true
                tab_lay.getTabAt(3)?.select()
                isScrolling = false
            }
            else if (y >= head_report.top && y <= recycler_report.bottom) {
                isScrolling = true
                tab_lay.getTabAt(4)?.select()
                isScrolling = false
            }
            else if (y >= head_equipment.top && y <= recycler_equipment.bottom) {
                isScrolling = true
                tab_lay.getTabAt(5)?.select()
                isScrolling = false
            }
        }
    }

    private fun setupAdapter() {
        recycler_account?.layoutManager = LinearLayoutManager(this, RecyclerView.VERTICAL, false)
        recycler_account?.adapter = accountAdapter

        recycler_agreement?.layoutManager = LinearLayoutManager(this, RecyclerView.VERTICAL, false)
        recycler_agreement?.adapter = agreementAdapter

        recycler_case?.layoutManager = LinearLayoutManager(this, RecyclerView.VERTICAL, false)
        recycler_case?.adapter = caseAdapter

        recycler_call?.layoutManager = LinearLayoutManager(this, RecyclerView.VERTICAL, false)
        recycler_call?.adapter = callAdapter

        recycler_report.layoutManager = LinearLayoutManager(this, RecyclerView.VERTICAL, false)
        recycler_report?.adapter = reportAdapter

        recycler_equipment.layoutManager = LinearLayoutManager(this, RecyclerView.VERTICAL, false)
        recycler_equipment?.adapter = equipmentAdapter
    }

    private fun dialogFilter() {
        val view = layoutInflater.inflate(R.layout.dialog_search_filter,null)
        val dialog = BottomSheetDialog(this)
        dialog.setContentView(view)

        val scLay = dialog.findViewById<CardView>(R.id.service_call_lay)
        val scExpand = dialog.findViewById<ImageView>(R.id.service_call_expand)
        val scCheckboxMain = dialog.findViewById<CheckBox>(R.id.service_call_checkbox)
        val scCheckboxProgres = dialog.findViewById<CheckBox>(R.id.sc_progres_checkbox)
        val scCheckboxComplete = dialog.findViewById<CheckBox>(R.id.sc_complete_checkbox)
        val scCheckboxAssigned = dialog.findViewById<CheckBox>(R.id.sc_assigned_checkbox)
        val scCheckboxSuspend = dialog.findViewById<CheckBox>(R.id.sc_suspend_checkbox)

        val btnSet = dialog.findViewById<TextView>(R.id.btn_set_filter)

        scExpand?.setOnClickListener {
            if (scLay?.visibility == View.GONE) {
                scExpand.rotation = 180f
                scLay.visibility = View.VISIBLE
            } else {
                scExpand.rotation = 0f
                scLay?.visibility = View.GONE
            }
        }

        dialog.show()
    }

    private fun searchListener() {
        search_value.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) { }
            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) { }

            override fun afterTextChanged(p0: Editable?) {
                if (p0 != null) {
                    accountObserver()
                    agreementObserver()
                    caseObserver()
                    callObserver()
                    reportObserver()
                    equipmentObserver()
                }
            }
        })
    }

    private fun accountObserver() {
        placeholder_account.visibility = View.VISIBLE
        warning_account.visibility = View.GONE

        val accountLive = accountVM.search(search_value.text.toString())
        accountLive.observe(this, Observer {
            placeholder_account.visibility = View.GONE
            accountLive.removeObservers(this)
            accountAdapter.setList(it)

            if (it.isNotEmpty()) {
                warning_account.visibility = View.GONE
            } else {
                warning_account.visibility = View.VISIBLE
            }
        })
    }

    private fun agreementObserver() {
        placeholder_agreement.visibility = View.VISIBLE
        warning_agreement.visibility = View.GONE

        val agreementLive = agreementVM.saearch(search_value.text.toString())
        agreementLive.observe(this, Observer {
            placeholder_agreement.visibility = View.GONE
            agreementLive.removeObservers(this)
            agreementAdapter.setList(it)

            if (it.isNotEmpty()) {
                warning_agreement.visibility = View.GONE
            } else {
                warning_agreement.visibility = View.VISIBLE
            }
        })
    }

    private fun caseObserver() {
        placeholder_case.visibility = View.VISIBLE
        warning_case.visibility = View.GONE

        val caseLive = caseVM.search(search_value.text.toString())
        caseLive.observe(this, Observer {
            placeholder_case.visibility = View.GONE
            caseLive.removeObservers(this)
            caseAdapter.setList(it)

            if (it.isNotEmpty()) {
                warning_case.visibility = View.GONE
            } else {
                warning_case.visibility = View.VISIBLE
            }
        })
    }

    private fun callObserver() {
        placeholder_call.visibility = View.VISIBLE
        warning_call.visibility = View.GONE

        val callLive = callVM.search(search_value.text.toString())
        callLive.observe(this, Observer {
            placeholder_call.visibility = View.GONE
            callLive.removeObservers(this)
            callAdapter.setList(it)

            if (it.isNotEmpty()) {
                warning_call.visibility = View.GONE
            } else {
                warning_call.visibility = View.VISIBLE
            }
        })
    }

    private fun reportObserver() {
        placeholder_report.visibility = View.VISIBLE
        warning_report.visibility = View.GONE

        val reportLive = reportVM.search(search_value.text.toString())
        reportLive.observe(this, Observer {
            placeholder_report.visibility = View.GONE
            reportLive.removeObservers(this)
            reportAdapter.setList(it)

            if (it.isNotEmpty()) {
                warning_report.visibility = View.GONE
            } else {
                warning_report.visibility = View.VISIBLE
            }
        })
    }

    private fun equipmentObserver() {
        placeholder_equipment.visibility = View.VISIBLE
        warning_equipment.visibility = View.GONE

        val equipmentLive = equipmentVM.search(search_value.text.toString())
        equipmentLive.observe(this, Observer {
            placeholder_equipment.visibility = View.GONE
            equipmentLive.removeObservers(this)
            equipmentAdapter.setList(it, true)

            if (it.isNotEmpty()) {
                warning_equipment.visibility = View.GONE
            } else {
                warning_equipment.visibility = View.VISIBLE
            }
        })
    }

}
