package com.syspex.ui

import android.app.ProgressDialog
import android.graphics.Bitmap
import android.graphics.Canvas
import android.graphics.Color
import android.os.Bundle
import android.os.Environment
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.FileProvider
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.livinglifetechway.quickpermissions_kotlin.runWithPermissions
import com.syspex.R
import com.syspex.data.local.SharedPreferenceData
import com.syspex.data.model.ConverterImageModel
import com.syspex.data.remote.LoginRequest
import com.syspex.data.retrofit.ApiClient
import com.syspex.ui.helper.FunHelper
import kotlinx.android.synthetic.main.activity_engineer_signature.*
import kotlinx.android.synthetic.main.sub_header.*
import net.gotev.uploadservice.protocols.multipart.MultipartUploadRequest
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.File
import java.io.FileOutputStream
import java.io.IOException
import java.io.OutputStream

class EngineerSignature : AppCompatActivity() {

    private lateinit var signatureFile : File
    private var signatureUri = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_engineer_signature)
        header_name.text = "Engineer Signature"

        generateView()
        generateListener()

    }

    private fun generateView() {
        val userSignature = SharedPreferenceData.getString(this, 1111, "")

        if (userSignature.isNotEmpty()) {
            signature_lay.visibility = View.GONE
            preview_lay.visibility = View.VISIBLE

            try {
                Glide.with(this).load(userSignature)
                    .diskCacheStrategy(DiskCacheStrategy.NONE)
                    .into(priview_image)
            } catch (e: Exception) { }
        } else {
            signature_lay.visibility = View.VISIBLE
            preview_lay.visibility = View.GONE
        }
    }

    private fun generateListener() {
        back_link?.setOnClickListener {
            onBackPressed()
        }

        signature_edit.setOnClickListener {
            preview_lay.visibility = View.GONE
            signature_lay.visibility = View.VISIBLE
        }

        signature_clear.setOnClickListener {
            signature_pad.clear()
        }

        signature_save.setOnClickListener {
            saveSignatureImage()
            preview_lay.visibility = View.VISIBLE
            signature_lay.visibility = View.GONE
        }

//        btn_save.setOnClickListener {
//            if (formValidation()) {
//                saveSignature()
//            } else {
//                Toast.makeText(this, "Please add signature", Toast.LENGTH_LONG).show()
//            }
//        }
    }

    private fun formValidation(): Boolean {
        if (signatureUri.isEmpty()) {
            signature_err.visibility = View.VISIBLE
            return false
        } else signature_err.visibility = View.GONE

        return true
    }

    private fun saveSignatureImage() = runWithPermissions(
        FunHelper.storageWritePermission,
        FunHelper.storageReadPermission
    ) {
        val signatureBitmap: Bitmap = signature_pad.signatureBitmap
        if (addJpgSignatureToGallery(signatureBitmap)) {
            signature_pad.clear()
            sendSignature()
        } else {
            Toast.makeText(this, "Signature save failed", Toast.LENGTH_SHORT).show()
        }
    }

    private fun addJpgSignatureToGallery(signature: Bitmap): Boolean {
        var result = false
        try {
            val id = SharedPreferenceData.getString(this, 8 , "")

            signatureFile = File(
                this.getExternalFilesDir(Environment.DIRECTORY_PICTURES),
                String.format("EngineerSignature-$id.jpg", System.currentTimeMillis())
            )
            saveBitmapToJPG(signature, signatureFile)

            val uri = FileProvider.getUriForFile(
                this,
                "com.syspex.fileprovider",
                signatureFile
            )
            signatureUri = uri.toString()

            Log.e("aim", "signature uri : $signatureUri")

            try {
                Glide.with(this).load(signature)
                    .diskCacheStrategy(DiskCacheStrategy.NONE)
                    .into(priview_image)
            } catch (e: Exception) { }

            result = true
        } catch (e: IOException) {
            e.printStackTrace()
        }
        return result
    }

    @Throws(IOException::class)
    private fun saveBitmapToJPG(bitmap: Bitmap, photo: File) {
        try {
            val newBitmap = Bitmap.createBitmap(
                bitmap.width,
                bitmap.height,
                Bitmap.Config.ARGB_8888
            )
            val canvas = Canvas(newBitmap)
            canvas.drawColor(Color.WHITE)
            canvas.drawBitmap(bitmap, 0f, 0f, null)
            val stream: OutputStream = FileOutputStream(photo)
            newBitmap.compress(Bitmap.CompressFormat.JPEG, 80, stream)
            stream.close()
        } catch (e: Exception) { Log.e("aim", e.toString()) }
    }

    private fun sendSignature() {
        val progressDialog = ProgressDialog(this)
        progressDialog.setMessage("Send signature ...")
        progressDialog.setCancelable(false)

        val header = "Bearer ${SharedPreferenceData.getString(this, 2, "")}"
        val userId = SharedPreferenceData.getString(this, 8, "")

        val reqFile = RequestBody.create(MediaType.parse("image/*"), signatureFile)
        val body = MultipartBody.Part.createFormData("myfile", signatureFile.name, reqFile)
        Log.e("aim", "multipart : $body")

        ApiClient.instance.uploadUserSignature(header, body).enqueue(object:Callback<LoginRequest> {
            override fun onFailure(call: Call<LoginRequest>, t: Throwable) {
                progressDialog.dismiss()
                Log.e("aim", "engineer signature : ${t.message}")
                Toast.makeText(this@EngineerSignature, "Send signature failed", Toast.LENGTH_LONG).show()
            }

            override fun onResponse(call: Call<LoginRequest>, response: Response<LoginRequest>) {
                progressDialog.dismiss()

                if (response.isSuccessful) {
                    Toast.makeText(this@EngineerSignature, "Signature sent", Toast.LENGTH_LONG)
                        .show()
                    val userSignature = response.body()?.data?.user?.userSignature ?: ""
                    SharedPreferenceData.setString(this@EngineerSignature, 1111, userSignature)

                    try {
                        Glide.with(this@EngineerSignature).load(userSignature)
                            .diskCacheStrategy(DiskCacheStrategy.NONE)
                            .into(priview_image)
                    } catch (e: Exception) { e.stackTrace }

                    onBackPressed()
                }
                else {
                    FunHelper.firebaseApiLog(
                        userId ?: "",
                        response.raw().request().url().toString() ?: "",
                        response.raw().request().headers().toString() ?: "",
                        body.toString() ?: "",
                        response.raw().toString() ?: ""
                    )
                    Toast.makeText(this@EngineerSignature, response.raw().toString(), Toast.LENGTH_LONG).show()
                }
            }
        })
    }
}