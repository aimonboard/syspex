package com.syspex.ui

import android.app.ProgressDialog
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.View
import android.view.inputmethod.EditorInfo
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.syspex.R
import com.syspex.data.local.SharedPreferenceData
import com.syspex.data.local.database.AppDatabase
import com.syspex.data.local.database.GlobalDao
import com.syspex.data.remote.SearchEquipmentOnlineRequest
import com.syspex.data.retrofit.ApiClient
import com.syspex.ui.adapter.EquipmentSearchOnlineAdapter
import com.syspex.ui.helper.*
import kotlinx.android.synthetic.main.search_equipment_online_activity.*
import kotlinx.android.synthetic.main.search_equipment_online_activity.btn_search
import kotlinx.android.synthetic.main.search_equipment_online_activity.swipe_container
import org.json.JSONObject
import org.w3c.dom.Text
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class SearchEquipmentOnline : AppCompatActivity() {

    lateinit var dao : GlobalDao
    private val equipmentAdapter = EquipmentSearchOnlineAdapter()

    private var page = 1
    private var lastPage = 1

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.search_equipment_online_activity)
        dao = AppDatabase.getInstance(this)!!.dao()

        setupAdapter()
        buttonListener()
        searchListener()
        recyclerScrollListener()
        adapterListener()
        getSearchEquipment(search_value.text.toString())

    }

    private fun buttonListener() {
        back_link.setOnClickListener {
            onBackPressed()
        }

        btn_search.setOnClickListener {
            if (header_name.visibility == View.VISIBLE) {
                header_name.visibility = View.GONE
                search_lay.visibility = View.VISIBLE
                btn_search.setImageResource(R.drawable.ic_close)

                KeyboardHelper.openSoftKeyboard(this,search_value)
            } else {
                header_name.visibility = View.VISIBLE
                search_lay.visibility = View.GONE
                btn_search.setImageResource(R.drawable.ic_search_small)

                KeyboardHelper.hideSoftKeyboard(this,search_value)

                search_value.setText("")
            }
        }

        swipe_container.setOnRefreshListener {
            page = 1
            getSearchEquipment(search_value.text.toString())
        }
    }

    private fun setupAdapter() {
        recycler_equipment.layoutManager = LinearLayoutManager(this, RecyclerView.VERTICAL,false)
        recycler_equipment.adapter = equipmentAdapter
    }

    private fun searchListener() {
        search_value.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}
            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}
            override fun afterTextChanged(p0: Editable?) {
                page = 1
                getSearchEquipment(p0.toString())
            }
        })

//        search_value.setOnEditorActionListener { textView, actionId, keyEvent ->
//            if(actionId == EditorInfo.IME_ACTION_SEARCH){
//                page = 1
//                getSearchEquipment(textView.text.toString())
//                KeyboardHelper.hideSoftKeyboard(this, search_value)
//                true
//            } else {
//                false
//            }
//        }
    }

    private fun recyclerScrollListener() {
        recycler_equipment.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                val linearLayoutManager = recyclerView.layoutManager as LinearLayoutManager
                val countItem = linearLayoutManager.itemCount
                val lastVisiblePosition = linearLayoutManager.findLastCompletelyVisibleItemPosition()
                val isLastPosition = countItem.minus(1) == lastVisiblePosition

                swipe_container.isEnabled = linearLayoutManager.findFirstCompletelyVisibleItemPosition() == 0


                if(isLastPosition && page < lastPage && equipmentAdapter.itemCount > 0) {
                    page++
                    getSearchEquipment(search_value.text.toString())
                }
            }
        })
    }

    private fun adapterListener() {
        equipmentAdapter.setEventHandler(object : EquipmentSearchOnlineAdapter.RecyclerClickListener {
            override fun isClicked(equipmentId: String, accountId: String) {
                startSyncEquipment(accountId, equipmentId)
            }
        })
    }

    private fun getSearchEquipment(searchText: String) {
        Log.e("aim", "request page : $page")
        swipe_container.isRefreshing = true
        val token = "Bearer "+ SharedPreferenceData.getString(this,2, "")
        val userId = SharedPreferenceData.getString(this,8, "")

        ApiClient.instance.searchEquipment(token, page, searchText).enqueue(object: Callback<SearchEquipmentOnlineRequest> {
            override fun onFailure(call: Call<SearchEquipmentOnlineRequest>, t: Throwable) {
                swipe_container.isRefreshing = false
                Log.e("aim","err : ${t.message}")
                Toast.makeText(this@SearchEquipmentOnline,"Connection failed", Toast.LENGTH_LONG).show()
            }
            override fun onResponse(call: Call<SearchEquipmentOnlineRequest>, response: Response<SearchEquipmentOnlineRequest>) {
                swipe_container.isRefreshing = false
                if (!response.isSuccessful) {
                    if (response.code() == 401) {
                        FunHelper.clearAllData(this@SearchEquipmentOnline)
                        Toast.makeText(this@SearchEquipmentOnline,"Session end", Toast.LENGTH_LONG).show()
                    } else {
                        FunHelper.firebaseApiLog(
                            userId ?: "",
                            response.raw().request().url().toString() ?: "",
                            response.raw().request().headers().toString() ?: "",
                            searchText ?: "",
                            response.raw().toString() ?: ""
                        )
                        Toast.makeText(this@SearchEquipmentOnline, response.raw().toString(), Toast.LENGTH_LONG).show()
                    }
                }
                else {
                    page = response.body()?.data?.page ?: 1
                    lastPage = response.body()?.data?.lastPage ?: 1
                    if (page == 1) {
                        equipmentAdapter.setList(response.body()?.data?.equipment ?: emptyList())
                    } else {
                        response.body()?.data?.equipment?.forEach {
                            equipmentAdapter.addData(it)
                        }
                        equipmentAdapter.notifyDataSetChanged()
                    }
                }
            }
        })
    }

    private fun startSyncEquipment(accountId: String, equipmentId: String) {
        val progressDialog = ProgressDialog(this)
        progressDialog.setMessage("Get Equipment Data ...")
        progressDialog.setCancelable(false)
        progressDialog.show()

        SyncSearchOnline(this).getEquipmentOnline(accountId, equipmentId,
            object : SyncSearchOnline.SyncSearchInterface {

            override fun syncSearchCallback(requestSuccess: Boolean) {
                runOnUiThread {
                    progressDialog.dismiss()
                    if (requestSuccess) {
                        val i = Intent(this@SearchEquipmentOnline, EquipmentDetail::class.java)
                        i.putExtra("id", equipmentId)
                        startActivity(i)
                    }
                }
            }
        })
    }
}